﻿using MainCloudFramework.Repositories;
using MainCloudFramework.Web.Helpers;
using SOLModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Repositories
{
    public class JobRepository : BaseRepository<Job, WeldDbContext>
    {
        public Job FindByJobName(string jobName)
        {
            if (!string.IsNullOrEmpty(jobName))
            {
                try
                {
                    return DBContext.Jobs.Where(j => j.JobName == jobName && j.ApplicationId == MultiTenantsHelper.ApplicationId).First();
                }
                
                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;
        }

        public List<Job> GetJobListByAsset(string assetId)
        {
            return DBContext.Jobs.Where(x => x.AssetId == assetId && x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
        }
    }
}