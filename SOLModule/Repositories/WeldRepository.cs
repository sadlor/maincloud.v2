﻿using MainCloudFramework.Repositories;
using SOLModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Repositories
{
    public class WeldRepository : BaseRepository<Weld, WeldDbContext>
    {
        public void InsertAll(IList<Weld> weldList)
        {
            DBContext.Welds.AddRange(weldList);
            //foreach (Weld item in weldList)
            //{
            //    try
            //    {
            //        DBContext.Welds.Add(item);
            //        DBContext.SaveChanges();
            //    }
            //    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            //    {
            //        foreach (var eve in ex.EntityValidationErrors)
            //        {
            //            System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //                eve.Entry.Entity.GetType().Name, eve.Entry.State);

            //            foreach (var ve in eve.ValidationErrors)
            //            {
            //                System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
            //                    ve.PropertyName,
            //                    eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
            //                    ve.ErrorMessage);
            //            }
            //        }
            //    }
            //}
        }
    }
}