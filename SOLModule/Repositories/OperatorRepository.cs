﻿using MainCloudFramework.Repositories;
using MainCloudFramework.Web.Helpers;
using SOLModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Repositories
{
    public class OperatorRepository : BaseRepository<Operator, WeldDbContext>
    {
        public Operator FindByName(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                try
                {
                    return DBContext.Operators.Where(o => o.Name == name && o.ApplicationId == MultiTenantsHelper.ApplicationId).First();
                }

                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;
        }

        public Operator FindByOperatorId(int id)
        {
            try
            {
                return DBContext.Operators.Where(o => o.OperatorId == id && o.ApplicationId == MultiTenantsHelper.ApplicationId).First();
            }

            catch (Exception ex)
            {
                return null;
            }
        }
    }
}