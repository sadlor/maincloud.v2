﻿using MainCloudFramework.Repositories;
using MainCloudFramework.Web.Helpers;
using SOLModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Repositories
{
    public class WPSRepository : BaseRepository<WeldingProcedureSpecification, WeldDbContext>
    {
        public WeldingProcedureSpecification FindByWPSName(string wpsName)
        {
            if (!string.IsNullOrEmpty(wpsName) && wpsName != "NOT-SET")
            {
                try
                {
                    return DBContext.WeldingProcedureSpecifications.Where(w => w.Name == wpsName && w.ApplicationId == MultiTenantsHelper.ApplicationId).First();
                }

                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;
        }
    }
}