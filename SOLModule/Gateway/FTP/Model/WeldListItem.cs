﻿using FTPModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Gateway.FTP.Model
{
    public class WeldListItem
    {
        public string MachineId { get; set; }
        public string JobId { get; set; }
        public string OperatorId { get; set; }
        public int RefNum { get; set; }
        public DateTime DateStart { get; set; }
        public decimal Current { get; set; }
        public decimal Voltage { get; set; }
        public decimal WireSpeed { get; set; }
        public decimal GasFlow { get; set; }
        public decimal Temp { get; set; }
        public decimal WeldTime { get; set; }
        public decimal Power { get; set; }
        public decimal HeatInput { get; set; }
        public decimal WeldLength { get; set; }
        public decimal WPSId { get; set; }
    }

    public class WeldListItemMap : CSVStructure<WeldListItem>
    {
        public WeldListItemMap() //TODO: da sistemare appena ho i tracciati
        {
            Map(m => m.MachineId).Name("machineId");
            Map(m => m.JobId).Name("jobId");
            Map(m => m.OperatorId).Name("operatorId");
            Map(m => m.RefNum).Name("refNum");
            Map(m => m.DateStart).Name("Date");
            Map(m => m.Current).Name("Current");
            Map(m => m.Voltage).Name("Voltage");
            Map(m => m.WireSpeed).Name("WireSpeed");
            Map(m => m.GasFlow).Name("GasFlow");
            Map(m => m.Temp).Name("Temp");
            Map(m => m.WeldTime).Name("WeldTime");
            Map(m => m.Power).Name("Power");
            Map(m => m.HeatInput).Name("HeatInput");
            Map(m => m.WeldLength).Name("WeldLength");
            Map(m => m.WPSId).Name("WPSId");
        }
    }
}