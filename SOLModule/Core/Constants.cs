﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOLModule.Core
{
    public class Constants
    {
        public const string ERR_MESSAGE = "ERR";
        public const string WARNING_MESSAGE = "WARN";
        public const string OK_MESSAGE = "OK";
    }
}