﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOLModule.Models
{
    public class WeldingProcedureSpecification
    {
        public WeldingProcedureSpecification()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        public decimal Current { get; set; }
        public decimal PctCurrentWrng { get; set; }
        public decimal PctCurrentErr { get; set; }

        public decimal Voltage { get; set; }
        public decimal PctVoltageWrng { get; set; }
        public decimal PctVoltageErr { get; set; }

        public decimal WireSpeed { get; set; }
        public decimal PctWireSpeedWrng { get; set; }
        public decimal PctWireSpeedErr { get; set; }

        public decimal GasFlow { get; set; }
        public decimal PctGasFlowWrng { get; set; }
        public decimal PctGasFlowErr { get; set; }

        public decimal Temperature { get; set; }
        public decimal PctTemperatureWrng { get; set; }
        public decimal PctTemperatureErr { get; set; }

        public string ApplicationId { get; set; }
    }
}