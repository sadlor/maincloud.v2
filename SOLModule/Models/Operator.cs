﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOLModule.Models
{
    public class Operator
    {
        public Operator()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        public int? OperatorId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Qualification { get; set; }
        public string WeldLicense { get; set; }

        public string ApplicationId { get; set; }

        //public virtual ICollection<Job> Jobs { get; set; }
    }
}