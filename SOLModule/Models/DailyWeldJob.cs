﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Models
{
    public class DailyWeldJob
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public int Id { get; set; }

        [Required]
        public string JobId { get; set; }

        [ForeignKey("JobId")]
        public Job Job { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public TimeSpan JobTime { get; set; }
        public TimeSpan WeldTime { get; set; }
        public double PctWeldTime { get; set; }
        public TimeSpan StopTime { get; set; }
        public double PctStopTime { get; set; }
        public decimal WireLength { get; set; }
        public decimal GasAmount { get; set; }
        public double AverageDepRate { get; set; }
        public decimal Energy { get; set; }
        public int WeldNum { get; set; }
        public int WeldWarningNum { get; set; }
        public int WeldErrorNum { get; set; }
    }
}