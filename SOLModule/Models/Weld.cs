﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Models
{
    public class Weld
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public int Id { get; set; }

        [Index("UniqueWeldIndex", 3, IsUnique = true)]
        public int Ref { get; set; }

        [Required]
        [Index("UniqueWeldIndex", 1, IsUnique = true)]
        public string JobId { get; set; }
        public string AnalyzerId { get; set; }  //AnalyzerId connected to the machine
        public string AssetId { get; set; }
        public string OperatorId { get; set; }

        [ForeignKey("JobId")]
        public Job Job { get; set; }

        [ForeignKey("OperatorId")]
        public Operator Operator { get; set; }

        public double SysDate { get; set; }

        [Index("UniqueWeldIndex", 2, IsUnique = true)]
        public DateTime Date { get; set; }     //DateStart
        public DateTime DateEnd { get; set; }

        public decimal? Current { get; set; }     //[A]
        public string Current_WPS { get; set; }
        public decimal? Volt { get; set; }     //[V]
        public string Volt_WPS { get; set; }
        public decimal? WireSpeed { get; set; }   //[m/min]
        public string WireSpeed_WPS { get; set; }
        public decimal? GasFlow { get; set; }     //[l/min]
        public string GasFlow_WPS { get; set; }
        public decimal? Temp { get; set; }        //[°C]
        public decimal? WeldTime { get; set; }    //[s]
        public decimal? Energy { get; set; }       //Power [kW]
        public decimal? HeatInput { get; set; }   //[kJ/mm]
        public decimal? WeldLen { get; set; }  //[cm]
        public string GasComp { get; set; }
        public string WpsName { get; set; }
        public string WPSId { get; set; }

        [ForeignKey("WPSId")]
        public WeldingProcedureSpecification WPS { get; set; }
    }
}