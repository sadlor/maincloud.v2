﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOLModule.Models
{
    public class Equipment
    {
        public Equipment()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        public string EquipName { get; set; }
        public string ChemBase { get; set; }
        public string ComBase { get; set; }
        public string LotBase { get; set; }
        public string ChemWire { get; set; }
        public string ComWire { get; set; }
        public string LotWire { get; set; }
        public string DiaWire { get; set; }
        public string ComProt { get; set; }
        public string LotProt { get; set; }
        public string ComTorch { get; set; }
        public string LotTorch { get; set; }
        public string DiaTorch { get; set; }
        public string ComGround { get; set; }
        public string LotGround { get; set; }
        public string DiaGround { get; set; }
        public string DiaBase { get; set; }
        public string Composition { get; set; }

        public string ApplicationId { get; set; }
    }
}