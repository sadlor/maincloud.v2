﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Models
{
    public class WeldSerie
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public int Id { get; set; }

        [Required]
        public string JobId { get; set; }
        public string OperatorId { get; set; }
        public string MachineId { get; set; }

        [ForeignKey("JobId")]
        public Job Job { get; set; }

        public DateTime Start { get; set; }
        public DateTime Stop { get; set; }

        public TimeSpan TotalSerieTime { get; set; }
        public TimeSpan WeldTime { get; set; }
        public TimeSpan StopWeldTime { get; set; }

        public decimal WireLength { get; set; }
        public decimal GasAmount { get; set; }
        public double AverageDepRate { get; set; }
        public decimal Energy { get; set; }
        public int WeldNum { get; set; }
        public int WeldWarningNum { get; set; }
        public int WeldErrorNum { get; set; }
        public int WeldClearNum { get; set; }  //Weld without errors or warnings
    }
}