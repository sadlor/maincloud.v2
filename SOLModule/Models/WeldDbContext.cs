﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Models
{
    public class WeldDbContext : DbContext
    {
        public DbSet<Weld> Welds { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Operator> Operators { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<WeldingProcedureSpecification> WeldingProcedureSpecifications { get; set; }
        public DbSet<DailyWeldJob> DailyWeldJobs { get; set; }
        public DbSet<WeldSerie> WeldSeries { get; set; }
        //public DbSet<Cause> Causes { get; set; }

        public WeldDbContext()
            : base("SOLWeldConnection")
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new WeldDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Weld>().Property(x => x.Current).HasPrecision(10, 2);
            modelBuilder.Entity<Weld>().Property(x => x.Volt).HasPrecision(10, 2);
            modelBuilder.Entity<Weld>().Property(x => x.GasFlow).HasPrecision(10, 2);
            modelBuilder.Entity<Weld>().Property(x => x.WireSpeed).HasPrecision(10, 2);
            modelBuilder.Entity<Weld>().Property(x => x.Temp).HasPrecision(10, 1);
            modelBuilder.Entity<Weld>().Property(x => x.WeldTime).HasPrecision(10, 2);
            modelBuilder.Entity<Weld>().Property(x => x.Energy).HasPrecision(10, 2);
            modelBuilder.Entity<Weld>().Property(x => x.HeatInput).HasPrecision(10, 2);
            modelBuilder.Entity<Weld>().Property(x => x.WeldLen).HasPrecision(10, 2);

            base.OnModelCreating(modelBuilder); // I had removed this
                                                /// Rest of on model creating here.
        }

        public static WeldDbContext Create()
        {
            return new WeldDbContext();
        }
    }
}