﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Models
{
    public class Job
    {
        public Job()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        public string Status { get; set; }

        [Index("UniqueJobIndex", 1, IsUnique = true)]
        [MaxLength(20)]
        public string JobId { get; set; }

        [Index("UniqueJobIndex", 2, IsUnique = true)]
        [Required]
        [MaxLength(50)]
        public string JobName { get; set; }

        [Index("UniqueJobIndex", 3, IsUnique = true)]
        [MaxLength(50)]
        public string MachineName { get; set; }
        public string Oper { get; set; }
        public string WpsName { get; set; }
        public string EquipeName { get; set; }
        public string ProcessType { get; set; }

        public decimal WireDiameter { get; set; }  //to manually insert  [mm]

        public string OperatorId { get; set; }

        [Index("UniqueJobIndex", 4, IsUnique = true)]
        [MaxLength(128)]
        public string AnalyzerId { get; set; } //AnalyzerId connected to the Machine
        public string AssetId { get; set; }    //MachineId

        public string EquipmentId { get; set; }

        public string WPSId { get; set; }

        public string ApplicationId { get; set; }

        [ForeignKey("OperatorId")]
        public Operator Operator { get; set; }

        [ForeignKey("EquipmentId")]
        public Equipment Equipment { get; set; }

        [ForeignKey("WPSId")]
        public WeldingProcedureSpecification WPS { get; set; }

        public string CalculatedData { get; set; }  //campo JSON
    }
}