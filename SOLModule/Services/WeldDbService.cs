﻿using MainCloudFramework.Services;
using SOLModule.Models;
using SOLModule.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Services
{
    public class WeldDbService : BaseService<WeldDbContext>
    {
        public List<Type> entitiesType
        {
            get
            {
                List<Type> entitiesType = new List<Type>();
                entitiesType.Add(typeof(Job));
                entitiesType.Add(typeof(Weld));
                entitiesType.Add(typeof(Operator));
                return entitiesType;
            }
        }
    }
}