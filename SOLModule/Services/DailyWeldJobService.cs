﻿using MainCloudFramework.Services;
using SOLModule.Core;
using SOLModule.Models;
using SOLModule.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Services
{
    public class DailyWeldJobService : BaseService<WeldDbContext>
    {
        public List<DailyWeldJob> GetListByJobId(string jobId)
        {
            List<DailyWeldJob> list = new List<DailyWeldJob>();
            if (!string.IsNullOrEmpty(jobId))
            {
                list = DBContext.DailyWeldJobs.Where(x => x.JobId == jobId).ToList();
                return list;
            }
            return list;
        }

        public List<DailyWeldJob> GetListByJobId(string jobId, DateTime start, DateTime end)
        {
            end = end.AddDays(1);
            List<DailyWeldJob> list = new List<DailyWeldJob>();
            if (!string.IsNullOrEmpty(jobId))
            {
                list = DBContext.DailyWeldJobs.Where(x => x.JobId == jobId && x.Date >= start && x.Date < end).ToList();
                return list;
            }
            return list;
        }

        public List<DailyWeldJob> GetListByJobList(List<string> jobList)
        {
            List<DailyWeldJob> returnList = new List<DailyWeldJob>();
            foreach (string jobId in jobList)
            {
                returnList.AddRange(GetListByJobId(jobId));
            }
            return returnList;
        }

        public List<DailyWeldJob> GetListByJobList(List<Job> jobList)
        {
            List<DailyWeldJob> returnList = new List<DailyWeldJob>();
            foreach (Job job in jobList)
            {
                returnList.AddRange(GetListByJobId(job.Id));
            }
            return returnList;
        }

        public List<DailyWeldJob> GetListByJobList(List<Job> jobList, DateTime start, DateTime end)
        {
            List<DailyWeldJob> returnList = new List<DailyWeldJob>();
            foreach (Job job in jobList)
            {
                returnList.AddRange(GetListByJobId(job.Id, start, end));
            }
            return returnList;
        }

        public void StorageDailySummary(string jobId, DateTime day)
        {
            DailyWeldJobRepository rep = new DailyWeldJobRepository();
            DateTime end = day.AddDays(1);
            List<WeldSerie> weldSerieInDay = DBContext.WeldSeries.Where(x => x.JobId == jobId && x.Start >= day && x.Start < end).OrderBy(x => x.Start).ToList();
            WeldSerieService WSS = new WeldSerieService();
            DailyWeldJob dailyWeld = DBContext.DailyWeldJobs.Where(x => x.JobId == jobId && x.Date == day).FirstOrDefault();
            bool update = true;
            if (dailyWeld == null)
            {
                dailyWeld = new DailyWeldJob();
                update = false;
            }
            if (weldSerieInDay.Count > 0)
            {
                dailyWeld.Date = day;
                dailyWeld.JobId = jobId;
                dailyWeld.JobTime = WSS.TotalTime(weldSerieInDay).Value;
                dailyWeld.WeldTime = WSS.WeldTime(weldSerieInDay).Value;
                dailyWeld.StopTime = dailyWeld.JobTime - dailyWeld.WeldTime;
                dailyWeld.PctWeldTime = Math.Round(dailyWeld.WeldTime.TotalSeconds / dailyWeld.JobTime.TotalSeconds * 100, 2);
                dailyWeld.PctStopTime = Math.Round(dailyWeld.StopTime.TotalSeconds / dailyWeld.JobTime.TotalSeconds * 100, 2);

                dailyWeld.WeldNum = WSS.WeldNumber(weldSerieInDay);
                dailyWeld.WeldWarningNum = WSS.WeldWarningsNum(weldSerieInDay);
                dailyWeld.WeldErrorNum = WSS.WeldErrorsNum(weldSerieInDay);

                if (dailyWeld.WeldTime.TotalSeconds > 0)
                {
                    //dwj.AverageSpeed = Math.Round(((weldList.Sum(x => x.WeldLen) / 100) / (decimal)dwj.WeldTime.TotalSeconds).Value, 3);
                    dailyWeld.Energy = WSS.Energy(weldSerieInDay);
                    dailyWeld.GasAmount = WSS.ShieldingGas(weldSerieInDay);
                    dailyWeld.WireLength = WSS.WireLength(weldSerieInDay);
                    dailyWeld.AverageDepRate = WSS.AverageDepRate(weldSerieInDay);
                }
                else
                {
                    dailyWeld.AverageDepRate = 0;
                    dailyWeld.Energy = 0;
                    dailyWeld.GasAmount = 0;
                    dailyWeld.WireLength = 0;
                }

                if (update)
                {
                    rep.Update(dailyWeld);
                }
                else
                {
                    rep.Insert(dailyWeld);
                }
                rep.SaveChanges();
            }
        }

        public void StorageDailySummary_Old(string jobId, DateTime day)
        {
            DailyWeldJobRepository rep = new DailyWeldJobRepository();

            DateTime end = day.AddDays(1);
            DailyWeldJob dwj = DBContext.DailyWeldJobs.Where(x => x.JobId == jobId && x.Date == day).FirstOrDefault();
            bool update = true;
            if (dwj == null)
            {
                dwj = new DailyWeldJob();
                update = false;
            }
            List<Weld> weldList = DBContext.Welds.Where(x => x.JobId == jobId && x.Date >= day && x.Date < end).ToList();
            if (weldList.Count > 0)
            {
                dwj.Date = day;
                dwj.JobId = jobId;
                dwj.JobTime = weldList.OrderByDescending(x => x.DateEnd).First().DateEnd - weldList.OrderBy(x => x.Date).First().Date;
                dwj.WeldTime = TimeSpan.FromSeconds((double)weldList.Sum(x => x.WeldTime).Value);
                dwj.StopTime = dwj.JobTime - dwj.WeldTime;
                //(weldList.OrderByDescending(x => x.DateEnd).First().DateEnd - weldList.OrderBy(x => x.DateStart).First().DateStart) -
                //TimeSpan.FromSeconds((double)weldList.Sum(x => x.WeldTime).Value);
                dwj.PctWeldTime = Math.Round(dwj.WeldTime.TotalSeconds / dwj.JobTime.TotalSeconds * 100, 2);
                dwj.PctStopTime = Math.Round(dwj.StopTime.TotalSeconds / dwj.JobTime.TotalSeconds * 100, 2);

                dwj.WeldNum = weldList.Count();
                dwj.WeldWarningNum = weldList.Where(x => x.Current_WPS == Constants.WARNING_MESSAGE || x.GasFlow_WPS == Constants.WARNING_MESSAGE || x.Volt_WPS == Constants.WARNING_MESSAGE || x.WireSpeed_WPS == Constants.WARNING_MESSAGE).Count();
                dwj.WeldErrorNum = weldList.Where(x => x.Current_WPS == Constants.ERR_MESSAGE || x.GasFlow_WPS == Constants.ERR_MESSAGE || x.Volt_WPS == Constants.ERR_MESSAGE || x.WireSpeed_WPS == Constants.ERR_MESSAGE).Count();

                if (dwj.WeldTime.TotalSeconds > 0)
                {
                    //dwj.AverageSpeed = Math.Round(((weldList.Sum(x => x.WeldLen) / 100) / (decimal)dwj.WeldTime.TotalSeconds).Value, 3);
                    dwj.Energy = Math.Round(weldList.Sum(x => x.Energy * x.WeldTime / 3600).Value, 2);
                    dwj.GasAmount = Math.Round(weldList.Sum(x => x.GasFlow * (x.WeldTime / 60)).Value, 2);
                    dwj.WireLength = Math.Round((weldList.Sum(x => x.WireSpeed * (x.WeldTime / 60))).Value, 2);

                    decimal wireDiameter = DBContext.Jobs.Where(x => x.Id == jobId).First().WireDiameter;
                    dwj.AverageDepRate = Math.Round(((double)Math.Round(weldList.Sum(x => x.WireSpeed * x.WeldTime / 60).Value, 2) *
                                         (Math.PI * Math.Pow((double)(wireDiameter / 1000) / 2, 2)) *
                                         7800) /
                                         (double)(weldList.Sum(x => x.WeldTime).Value / 3600), 2);
                }
                else
                {
                    dwj.AverageDepRate = 0;
                    dwj.Energy = 0;
                    dwj.GasAmount = 0;
                    dwj.WireLength = 0;
                }

                if (update)
                {
                    rep.Update(dwj);
                }
                else
                {
                    rep.Insert(dwj);
                }
                rep.SaveChanges();
            }
        }

        public void StorageDailySummary(string jobId)
        {
            List<Weld> weldList = DBContext.Welds.Where(x => x.JobId == jobId).ToList();
            DateTime start = weldList.Min(x => x.Date).Date;
            DateTime end = weldList.Max(x => x.Date).Date;
            for (DateTime day = start; day <= end; day = day.AddDays(1))
            {
                StorageDailySummary(jobId, day);
            }
        }
    }
}