﻿using MainCloudFramework.Services;
using SOLModule.Models;
using SOLModule.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Services
{
    public class WeldSerieService : BaseService<WeldDbContext>
    {
        public WeldSerie GetLastSerieByJobId(string jobId)
        {
            return DBContext.WeldSeries.Where(x => x.JobId == jobId).OrderByDescending(x => x.Start).FirstOrDefault();
        }

        public void CreateOrUpdateSerie(string jobId, List<Weld> weldList)
        {
            if (weldList.Count > 0)
            {
                bool save = true;
                bool update = false;
                WeldSerieRepository serieRepos = new WeldSerieRepository();
                WeldService WS = new WeldService();
                WeldSerie lastSerie = GetLastSerieByJobId(jobId);
                WeldSerie serie = new WeldSerie();

                if (lastSerie == null)
                {
                    //add new serie x jobId
                    serie.JobId = jobId;
                    serie.Start = weldList.Min(x => x.Date);
                }
                else
                {
                    if ((weldList.OrderBy(x => x.Date).First().Date - lastSerie.Stop).TotalMinutes >= 60)
                    {
                        //add new serie x jobId
                        serie.JobId = jobId;
                        serie.Start = weldList.Min(x => x.Date);
                    }
                    else
                    {
                        serie = lastSerie;
                        update = true;
                        WeldRepository repos = new WeldRepository();
                        weldList = new List<Weld>();
                        weldList.AddRange(repos.ReadAll(x => x.JobId == jobId && x.Date >= lastSerie.Start).OrderBy(x => x.Date).ToList());
                    }
                }

                Weld lastWeld = weldList.OrderBy(x => x.Date).First();
                List<Weld> weldListInSerie = new List<Weld>();
                foreach (Weld item in weldList.OrderBy(x => x.Date))
                {
                    if ((item.Date - lastWeld.DateEnd).TotalMinutes >= 60)
                    {
                        //close current serie and create new one
                        serie.AverageDepRate = WS.AverageDepRate(weldListInSerie, jobId);
                        serie.Energy = WS.Energy(weldListInSerie).Value;
                        serie.GasAmount = WS.ShieldingGas(weldListInSerie).Value;
                        serie.TotalSerieTime = WS.TotalTime(weldListInSerie).Value;
                        serie.WeldTime = TimeSpan.FromSeconds((double)WS.WeldTime(weldListInSerie).Value);
                        serie.StopWeldTime = serie.TotalSerieTime - serie.WeldTime;
                        serie.WeldClearNum = WS.WeldClearNum(weldListInSerie);
                        serie.WeldErrorNum = WS.WeldErrorsNum(weldListInSerie);
                        serie.WeldNum = WS.WeldNumber(weldListInSerie);
                        serie.WeldWarningNum = WS.WeldWarningsNum(weldListInSerie);
                        serie.WireLength = WS.WireLength(weldListInSerie).Value;
                        serie.Stop = weldListInSerie.OrderByDescending(x => x.Date).First().DateEnd;
                        if (update)
                        {
                            serieRepos.Update(serie);
                        }
                        else
                        {
                            serieRepos.Insert(serie);
                        }
                        serieRepos.SaveChanges();

                        CreateOrUpdateSerie(jobId, weldList.Where(x => !weldListInSerie.Contains(x)).ToList());

                        save = false;
                        break;
                    }
                    else
                    {
                        weldListInSerie.Add(item);
                        lastWeld = item;
                    }
                }

                if (save)
                {
                    //close current serie and create new one
                    serie.AverageDepRate = WS.AverageDepRate(weldListInSerie, jobId);
                    serie.Energy = WS.Energy(weldListInSerie).Value;
                    serie.GasAmount = WS.ShieldingGas(weldListInSerie).Value;
                    serie.TotalSerieTime = WS.TotalTime(weldListInSerie).Value;
                    serie.WeldTime = TimeSpan.FromSeconds((double)WS.WeldTime(weldListInSerie).Value);
                    serie.StopWeldTime = serie.TotalSerieTime - serie.WeldTime;
                    serie.WeldClearNum = WS.WeldClearNum(weldListInSerie);
                    serie.WeldErrorNum = WS.WeldErrorsNum(weldListInSerie);
                    serie.WeldNum = WS.WeldNumber(weldListInSerie);
                    serie.WeldWarningNum = WS.WeldWarningsNum(weldListInSerie);
                    serie.WireLength = WS.WireLength(weldListInSerie).Value;
                    serie.Stop = weldListInSerie.OrderByDescending(x => x.Date).First().DateEnd;
                    if (update)
                    {
                        serieRepos.Update(serie);
                    }
                    else
                    {
                        serieRepos.Insert(serie);
                    }
                    serieRepos.SaveChanges();
                }
            }
        }

        public TimeSpan? TotalTime(List<WeldSerie> weldSerieList)
        {
            TimeSpan total = weldSerieList.Aggregate(TimeSpan.Zero, (subtotal, x) => subtotal.Add(x.TotalSerieTime));
            return total;
        }

        public TimeSpan? WeldTime(List<WeldSerie> weldSerieList)
        {
            TimeSpan total = weldSerieList.Aggregate(TimeSpan.Zero, (subtotal, x) => subtotal.Add(x.WeldTime));
            return total;
        }

        public int WeldNumber(List<WeldSerie> weldSerieList)
        {
            return weldSerieList.Sum(x => x.WeldNum);
        }

        public int WeldErrorsNum(List<WeldSerie> weldSerieList)
        {
            return weldSerieList.Sum(x => x.WeldErrorNum);
        }

        public int WeldWarningsNum(List<WeldSerie> weldSerieList)
        {
            return weldSerieList.Sum(x => x.WeldWarningNum);
        }

        public int WeldClearNum(List<WeldSerie> weldSerieList)
        {
            return weldSerieList.Sum(x => x.WeldClearNum);
        }

        public decimal Energy(List<WeldSerie> weldSerieList)
        {
            return Math.Round(weldSerieList.Sum(x => x.Energy), 2);
        }

        public decimal ShieldingGas(List<WeldSerie> weldSerieList)
        {
            return Math.Round(weldSerieList.Sum(x => x.GasAmount), 2);
        }

        public decimal WireLength(List<WeldSerie> weldSerieList)
        {
            return Math.Round(weldSerieList.Sum(x => x.WireLength), 2);
        }

        public double AverageDepRate(List<WeldSerie> weldSerieList)
        {
            return Math.Round(weldSerieList.Average(x => x.AverageDepRate));
        }
    }
}