﻿using MainCloudFramework.Services;
using SOLModule.Core;
using SOLModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Services
{
    public class WeldService : BaseService<WeldDbContext>
    {
        public TimeSpan? TotalTime(List<Weld> weldList)
        {
            var first = weldList.OrderBy(x => x.Date).First();
            var last = weldList.OrderByDescending(x => x.Date).First();
            return last.DateEnd - first.Date;
        }

        public decimal? WeldTime(DateTime start, DateTime end, string jobId = null)
        {
            end = end.AddDays(1);
            if (string.IsNullOrEmpty(jobId))
            {
                return DBContext.Welds.Where(x => x.Date >= start && x.DateEnd <= end).Sum(x => x.WeldTime);
            }
            else
            {
                return DBContext.Welds.Where(x => x.JobId == jobId && x.Date >= start && x.DateEnd <= end).Sum(x => x.WeldTime);
            }
        }

        public decimal? WeldTime(List<Weld> weldList)
        {
            return weldList.Sum(x => x.WeldTime);
        }

        public int WeldNumber(DateTime start, DateTime end, string jobId = null)
        {
            end = end.AddDays(1);

            if (string.IsNullOrEmpty(jobId))
            {
                return DBContext.Welds.Where(x => x.Date >= start && x.DateEnd <= end).Count();
            }
            else
            {
                return DBContext.Welds.Where(x => x.JobId == jobId && x.Date >= start && x.DateEnd <= end).Count();
            }
        }

        public int WeldNumber(List<Weld> weldList)
        {
            return weldList.Count();
        }


        public decimal? Energy(DateTime start, DateTime end, string jobId = null)
        {
            end = end.AddDays(1);

            if (string.IsNullOrEmpty(jobId))
            {
                var item = DBContext.Welds.Where(x => x.Date >= start && x.DateEnd <= end).Sum(x => x.Energy * x.WeldTime / 3600);
                if (item.HasValue)
                {
                    item = Math.Round(item.Value, 2);
                }
                return item;
            }
            else
            {
                var item = DBContext.Welds.Where(x => x.JobId == jobId && x.Date >= start && x.DateEnd <= end).Sum(x => x.Energy * x.WeldTime / 3600);
                if (item.HasValue)
                {
                    item = Math.Round(item.Value, 2);
                }
                return item;
            }
        }

        public decimal? Energy(List<Weld> weldList)
        {
            var item = weldList.Sum(x => x.Energy * x.WeldTime / 3600);
            if (item.HasValue)
            {
                item = Math.Round(item.Value, 2);
            }
            return item;
        }

        public decimal? ShieldingGas(DateTime start, DateTime end, string jobId = null)
        {
            end = end.AddDays(1);

            if (string.IsNullOrEmpty(jobId))
            {
                var item = DBContext.Welds.Where(x => x.Date >= start && x.DateEnd <= end).Sum(x => x.GasFlow * (x.WeldTime / 60));
                if (item.HasValue)
                {
                    item = Math.Round(item.Value, 2);
                }
                return item;
            }
            else
            {
                var item = DBContext.Welds.Where(x => x.JobId == jobId && x.Date >= start && x.DateEnd <= end).Sum(x => x.GasFlow * (x.WeldTime / 60));
                if (item.HasValue)
                {
                    item = Math.Round(item.Value, 2);
                }
                return item;
            }
        }

        public decimal? ShieldingGas(List<Weld> weldList)
        {
            var item = weldList.Sum(x => x.GasFlow * (x.WeldTime / 60));
            if (item.HasValue)
            {
                item = Math.Round(item.Value, 2);
            }
            return item;
        }

        public decimal? WireLength(DateTime start, DateTime end, string jobId = null)
        {
            end = end.AddDays(1);

            if (string.IsNullOrEmpty(jobId))
            {
                var item = DBContext.Welds.Where(x => x.Date >= start && x.DateEnd <= end).Sum(x => x.WireSpeed * (x.WeldTime / 60));
                if (item.HasValue)
                {
                    item = Math.Round(item.Value, 2);
                }
                return item;
            }
            else
            {
                var item = DBContext.Welds.Where(x => x.JobId == jobId && x.Date >= start && x.DateEnd <= end).Sum(x => x.WireSpeed * (x.WeldTime / 60));
                if (item.HasValue)
                {
                    item = Math.Round(item.Value, 2);
                }
                return item;
            }
        }

        public decimal? WireLength(List<Weld> weldList)
        {
            var item = weldList.Sum(x => x.WireSpeed * (x.WeldTime / 60));
            if (item.HasValue)
            {
                item = Math.Round(item.Value, 2);
            }
            return item;
        }

        public int WeldErrorsNum(DateTime start, DateTime end, string jobId = null)
        {
            end = end.AddDays(1);

            if (string.IsNullOrEmpty(jobId))
            {
                return DBContext.Welds.Where(x => x.Date >= start && x.DateEnd <= end && (x.Current_WPS == Constants.ERR_MESSAGE || x.GasFlow_WPS == Constants.ERR_MESSAGE || x.Volt_WPS == Constants.ERR_MESSAGE || x.WireSpeed_WPS == Constants.ERR_MESSAGE)).Count();
            }
            else
            {
                return DBContext.Welds.Where(x => x.JobId == jobId && x.Date >= start && x.DateEnd <= end && (x.Current_WPS == Constants.ERR_MESSAGE || x.GasFlow_WPS == Constants.ERR_MESSAGE || x.Volt_WPS == Constants.ERR_MESSAGE || x.WireSpeed_WPS == Constants.ERR_MESSAGE)).Count();
            }
        }

        public int WeldErrorsNum(List<Weld> weldList)
        {
            return weldList.Where(x => x.Current_WPS == Constants.ERR_MESSAGE || x.GasFlow_WPS == Constants.ERR_MESSAGE || x.Volt_WPS == Constants.ERR_MESSAGE || x.WireSpeed_WPS == Constants.ERR_MESSAGE).Count();
        }

        public int WeldWarningsNum(DateTime start, DateTime end, string jobId = null)
        {
            end = end.AddDays(1);

            if (string.IsNullOrEmpty(jobId))
            {
                return DBContext.Welds.Where(x => x.Date >= start && x.DateEnd <= end && (x.Current_WPS == Constants.WARNING_MESSAGE || x.GasFlow_WPS == Constants.WARNING_MESSAGE || x.Volt_WPS == Constants.WARNING_MESSAGE || x.WireSpeed_WPS == Constants.WARNING_MESSAGE)).Count();
            }
            else
            {
                return DBContext.Welds.Where(x => x.JobId == jobId && x.Date >= start && x.DateEnd <= end && (x.Current_WPS == Constants.WARNING_MESSAGE || x.GasFlow_WPS == Constants.WARNING_MESSAGE || x.Volt_WPS == Constants.WARNING_MESSAGE || x.WireSpeed_WPS == Constants.WARNING_MESSAGE)).Count();
            }
        }

        public int WeldWarningsNum(List<Weld> weldList)
        {
            return weldList.Where(x => x.Current_WPS == Constants.WARNING_MESSAGE || x.GasFlow_WPS == Constants.WARNING_MESSAGE || x.Volt_WPS == Constants.WARNING_MESSAGE || x.WireSpeed_WPS == Constants.WARNING_MESSAGE).Count();
        }

        public int WeldClearNum(List<Weld> weldList)
        {
            return weldList.Where(x => x.Current_WPS == Constants.OK_MESSAGE && x.GasFlow_WPS == Constants.OK_MESSAGE && x.Volt_WPS == Constants.OK_MESSAGE && x.WireSpeed_WPS == Constants.OK_MESSAGE).Count();
        }

        public double AverageDepRate(List<Weld> weldList, string jobId)
        {
            decimal wireDiameter = DBContext.Jobs.Where(x => x.Id == jobId).First().WireDiameter;
            var returnValue = Math.Round(((double)Math.Round(weldList.Sum(x => x.WireSpeed * x.WeldTime / 60).Value, 2) *
                                          (Math.PI * Math.Pow((double)(wireDiameter / 1000) / 2, 2)) * 7800) /
                                         (double)(weldList.Sum(x => x.WeldTime).Value / 3600), 2);
            if (Double.IsNaN(returnValue))
            {
                return 0;
            }
            return returnValue;
        }
    }
}