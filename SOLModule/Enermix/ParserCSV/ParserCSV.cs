﻿using CsvHelper;
using Microsoft.VisualBasic.FileIO;
using SOLModule.Models;
using SOLModule.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Enermix.ParserCSV
{
    public class ParserCSV
    {
        public ParserCSV(string path)
        {
            //per debug
            //SOLModule.Enermix.ParserCSV.ParserCSV p = new SOLModule.Enermix.ParserCSV.ParserCSV(@"C:\Users\Mattia.MYCROS\Downloads\CSV Enermix\weld_20170223_123910.csv");
            ReadWeldingData(path);
        }

        private void ReadWeldingData(string fileName)
        {
            List<Weld> weldList = new List<Weld>();
            Job job = new Job();
            JobRepository jRep = new JobRepository();
            WeldRepository weldRep = new WeldRepository();

            using (TextFieldParser parser = new TextFieldParser(fileName))
            {
                //parser.CommentTokens = new string[] { "#" };
                parser.SetDelimiters(new string[] { ";" });
                parser.HasFieldsEnclosedInQuotes = true;
                // Skip over header line.
                //parser.ReadLine();

                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    if (!string.IsNullOrEmpty(fields[0]))
                    {
                        switch (fields[0].Split('=')[0].Trim())
                        {
                            case "Job Name":
                                job.JobName = fields[0].Split('=')[1].Trim();
                                job = jRep.FindByJobName(job.JobName) != null ? jRep.FindByJobName(job.JobName) : job;
                                break;
                            case "WPS":
                                if (fields[0].Split('=')[1].Trim() != "None")
                                {
                                    WPSRepository wpsRep = new WPSRepository();
                                    WeldingProcedureSpecification wps = new WeldingProcedureSpecification();
                                    wps.Name = fields[0].Split('=')[1].Trim();
                                    if (wpsRep.FindByWPSName(wps.Name) == null)
                                    {
                                        wps.Current = decimal.Parse(fields[1].Split('=')[1].Trim(), CultureInfo.CurrentCulture);
                                        wps.PctCurrentWrng = decimal.Parse(fields[2].Split('=')[1].Trim().Replace("%", ""), CultureInfo.CurrentCulture);
                                        wps.PctCurrentErr = decimal.Parse(fields[3].Split('=')[1].Trim().Replace("%", ""), CultureInfo.CurrentCulture);
                                        fields = parser.ReadFields();
                                        wps.Voltage = decimal.Parse(fields[1].Split('=')[1].Trim(), CultureInfo.CurrentCulture);
                                        wps.PctVoltageWrng = decimal.Parse(fields[2].Split('=')[1].Trim().Replace("%", ""), CultureInfo.CurrentCulture);
                                        wps.PctVoltageErr = decimal.Parse(fields[3].Split('=')[1].Trim().Replace("%", ""), CultureInfo.CurrentCulture);
                                        fields = parser.ReadFields();
                                        wps.WireSpeed = decimal.Parse(fields[1].Split('=')[1].Trim(), CultureInfo.CurrentCulture);
                                        wps.PctWireSpeedWrng = decimal.Parse(fields[2].Split('=')[1].Trim().Replace("%", ""), CultureInfo.CurrentCulture);
                                        wps.PctWireSpeedErr = decimal.Parse(fields[3].Split('=')[1].Trim().Replace("%", ""), CultureInfo.CurrentCulture);
                                        fields = parser.ReadFields();
                                        wps.GasFlow = decimal.Parse(fields[1].Split('=')[1].Trim(), CultureInfo.CurrentCulture);
                                        wps.PctGasFlowWrng = decimal.Parse(fields[2].Split('=')[1].Trim().Replace("%", ""), CultureInfo.CurrentCulture);
                                        wps.PctGasFlowErr = decimal.Parse(fields[3].Split('=')[1].Trim().Replace("%", ""), CultureInfo.CurrentCulture);
                                        fields = parser.ReadFields();
                                        wps.Temperature = decimal.Parse(fields[1].Split('=')[1].Trim(), CultureInfo.CurrentCulture);
                                        wps.PctTemperatureWrng = decimal.Parse(fields[2].Split('=')[1].Trim().Replace("%", ""), CultureInfo.CurrentCulture);
                                        wps.PctTemperatureErr = decimal.Parse(fields[3].Split('=')[1].Trim().Replace("%", ""), CultureInfo.CurrentCulture);

                                        wpsRep.Insert(wps);
                                        wpsRep.SaveChanges();
                                    }
                                    else
                                    {
                                        wps = wpsRep.FindByWPSName(wps.Name);
                                    }
                                    job.WPS = wps;
                                    //job.WPSId = wps.Id;
                                }
                                break;
                            case "Operator":
                                OperatorRepository opRep = new OperatorRepository();
                                Operator op = new Operator();
                                if (fields[2].Split('=')[1].Trim() != "(null)")
                                {
                                    if (opRep.FindByOperatorId(int.Parse(fields[2].Split('=')[1].Trim())) != null)
                                    {
                                        op = opRep.FindByOperatorId(int.Parse(fields[2].Split('=')[1].Trim()));
                                    }
                                    else
                                    {
                                        op.Name = fields[1].Split('=')[1].Trim();
                                        op.OperatorId = int.Parse(fields[2].Split('=')[1].Trim());
                                        op.Qualification = fields[3].Split('=')[1].Trim();
                                        op.WeldLicense = fields[4].Split('=')[1].Trim();
                                        opRep.Insert(op);
                                    }

                                }
                                else
                                {
                                    op.Name = fields[1].Split('=')[1].Trim();
                                    if (opRep.FindByName(op.Name) != null)
                                    {
                                        op = opRep.FindByName(op.Name);
                                    }
                                    else
                                    {
                                        op.OperatorId = null;
                                        op.Qualification = fields[3].Split('=')[1].Trim();
                                        op.WeldLicense = fields[4].Split('=')[1].Trim();
                                        opRep.Insert(op);
                                    }
                                }
                                opRep.SaveChanges();
                                job.Operator = op;
                                //job.OperatorId = op.Id;
                                break;
                            case "Machine Set":
                                AssetManagement.Repositories.AssetRepository aRep = new AssetManagement.Repositories.AssetRepository();
                                AssetManagement.Models.Asset asset = GetAssetByDescription(fields[0].Split('=')[1].Trim());
                                if (asset == null)
                                {
                                    asset = new AssetManagement.Models.Asset();
                                    asset.Code = "m";
                                    asset.Description = fields[0].Split('=')[1].Trim();
                                    asset.DtInstallation = DateTime.Today;
                                    aRep.Insert(asset);
                                    aRep.SaveChanges();
                                }
                                job.AssetId = asset.Id;
                                break;
                            case "ref":
                                parser.ReadToEnd();
                                break;
                        }
                    }
                }
            }

            //jRep.Insert(job);
            //jRep.SaveChanges();

            using (StreamReader sr = new StreamReader(fileName))
            {
                sr.BaseStream.Seek(0, SeekOrigin.Begin);
                while (!sr.EndOfStream)
                {
                    if (sr.ReadLine().StartsWith("Total Wire Used"))
                    {
                        var csv = new CsvReader(sr);
                        csv.Configuration.Delimiter = ";";
                        csv.Configuration.CultureInfo = CultureInfo.InvariantCulture;
                        csv.Configuration.RegisterClassMap<Enermix.Model.EnermixCSVMap>();

                        //List<Enermix.Model.EnermixCSV> recordList = new List<Enermix.Model.EnermixCSV>();
                        while (csv.Read())
                        {
                            try
                            {
                                //recordList.Add(csv.GetRecord<Enermix.Model.EnermixCSV>());
                                Enermix.Model.EnermixCSV record = csv.GetRecord<Enermix.Model.EnermixCSV>();
                                Weld weld = new Weld();
                                bool update = false;
                                try
                                {
                                    weld = weldRep.ReadAll(x => x.Date == record.Date && x.JobId == job.Id).First();
                                    update = true;   
                                }
                                catch (Exception ex)
                                {
                                    update = false;
                                }
                                weld.Job = job;
                                weld.JobId = job.Id;
                                weld.Operator = job.Operator;
                                weld.WPS = job.WPS;
                                weld.Current = record.Current;
                                weld.Current_WPS = record.WpsCurrent;
                                weld.Date = record.Date;
                                weld.WeldTime = record.WeldTime;
                                weld.DateEnd = record.Date.AddSeconds((double)record.WeldTime);
                                weld.Energy = record.Power;
                                weld.GasFlow = record.GasFlow;
                                weld.GasFlow_WPS = record.WpsGasFlow;
                                weld.HeatInput = record.HeatInput;
                                weld.Ref = record.Ref;
                                weld.SysDate = record.Date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                                weld.Temp = record.Temp;
                                weld.Volt = record.Voltage;
                                weld.Volt_WPS = record.WpsVoltage;
                                weld.WeldLen = record.WeldLength;
                                weld.WireSpeed = record.WireSpeed;
                                weld.WireSpeed_WPS = record.WpsWireSpeed;
                                if (update)
                                {
                                    weldRep.Update(weld);
                                }
                                else
                                {
                                    weldList.Add(weld);
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                    }
                }
            }
            weldRep.InsertAll(weldList);
            weldRep.SaveChanges();
        }

        private AssetManagement.Models.Asset GetAssetByDescription(string descr)
        {
            using (AssetManagement.Models.AssetManagementDbContext db = new AssetManagement.Models.AssetManagementDbContext())
            {
                try
                {
                    return db.Assets.Where(x => x.Description == descr).First();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
    }
}