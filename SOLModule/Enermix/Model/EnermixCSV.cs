﻿using FTPModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLModule.Enermix.Model
{
    public class EnermixCSV
    {
        public int Ref { get; set; }
        public DateTime Date { get; set; }
        public decimal Current { get; set; }
        public decimal Voltage { get; set; }
        public decimal WireSpeed { get; set; }
        public decimal GasFlow { get; set; }
        public decimal Temp { get; set; }
        public decimal WeldTime { get; set; }
        public decimal Power { get; set; }
        public decimal HeatInput { get; set; }
        public decimal WeldLength { get; set; }
        public string WpsName { get; set; }
        public string WpsCurrent { get; set; }
        public string WpsVoltage { get; set; }
        public string WpsWireSpeed { get; set; }
        public string WpsGasFlow { get; set; }
    }

    public class EnermixCSVMap : CSVStructure<EnermixCSV>
    {
        public EnermixCSVMap()
        {
            Map(m => m.Ref).Name("ref");
            Map(m => m.Date).Name("date");
            Map(m => m.Date).TypeConverterOption(System.Globalization.CultureInfo.CurrentCulture);
            Map(m => m.Current).Name("Current [A]");
            Map(m => m.Voltage).Name("Voltage [V]");
            Map(m => m.WireSpeed).Name("Wire Speed [m/min]");
            Map(m => m.GasFlow).Name("Gas Flow [l/min]");
            Map(m => m.Temp).Name("Temp [T]");
            Map(m => m.WeldTime).Name("Weld Time [s]");
            Map(m => m.Power).Name("Power [kW]");
            Map(m => m.HeatInput).Name("Heat Input [kJ/mm]");
            Map(m => m.WeldLength).Name("Weld Length [cm]");
            Map(m => m.WpsName).Name("WPS NAME");
            Map(m => m.WpsCurrent).Name(" Wps Current");
            Map(m => m.WpsVoltage).Name(" Wps Voltage");
            Map(m => m.WpsWireSpeed).Name(" WPS Wire Speed");
            Map(m => m.WpsGasFlow).Name(" WPS Gas Flow");
        }
    }
}