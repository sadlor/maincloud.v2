﻿using MainCloudFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TouchTagModule.Models;

namespace TouchTagModule.Repositories
{
    public class TouchTagMessageRepository : BaseRepository<TouchTagMessage, TouchTagDbContext>
    {
        public int GetTemperature(int messageId)
        {
            return DBContext.TouchTagMessages.Where(x => x.Id == messageId).Select(x => x.Temperature).FirstOrDefault();
        }

        public int GetTriggerCode(int messageId)
        {
            return DBContext.TouchTagMessages.Where(x => x.Id == messageId).Select(x => x.TriggerCode).FirstOrDefault();
        }

        public int GetPitch(int messageId)
        {
            return DBContext.TouchTagMessages.Where(x => x.Id == messageId).Select(x => x.Pitch).FirstOrDefault();
        }

        public int GetRoll(int messageId)
        {
            return DBContext.TouchTagMessages.Where(x => x.Id == messageId).Select(x => x.Roll).FirstOrDefault();
        }

        public DateTime GetData(int messageId)    //timestamp 
        {
            return DBContext.TouchTagMessages.Where(x => x.Id == messageId).Select(x => x.Time).FirstOrDefault();
        }

        public double GetGPSLat(int messageId)    //timestamp 
        {
            return DBContext.TouchTagMessages.Where(x => x.Id == messageId).Select(x => x.Lat).FirstOrDefault();
        }

        public double GetGPSLng(int messageId)    //timestamp 
        {
            return DBContext.TouchTagMessages.Where(x => x.Id == messageId).Select(x => x.Lng).FirstOrDefault();
        }
    }
}