﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TouchTagModule.Models
{
    public class TouchTagDbContext : DbContext
    {
        public DbSet<TouchTag> TouchTags { get; set; }
        public DbSet<TouchTagMessage> TouchTagMessages { get; set; }

        public TouchTagDbContext() : base("TouchTagConnection")
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new TouchTagInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public static TouchTagDbContext Create()
        {
            return new TouchTagDbContext();
        }
    }
}