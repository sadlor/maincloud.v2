﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TouchTagModule.Models
{
    public class TouchTag
    {
        [Key]
        [Required]
        public string Id { get; set; }
        public string AssetId { get; set; }
    }
}
