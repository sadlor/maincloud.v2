﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TouchTagModule.Models
{
    public class TouchTagInitializer : CreateDatabaseIfNotExists<TouchTagDbContext>
    {
        protected override void Seed(TouchTagDbContext context)
        {
            base.Seed(context);
        }
    }
}