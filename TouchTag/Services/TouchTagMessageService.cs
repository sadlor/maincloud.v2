﻿using MainCloudFramework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TouchTagModule.Models;

namespace TouchTagModule.Services
{
    public class TouchTagMessageService : BaseService<TouchTagDbContext>
    {
        public int GetLastTriggerCounter(string touchTagCode, int triggerCode)
        {
            return DBContext.TouchTagMessages.Where(x => x.Device == touchTagCode && x.TriggerCode == triggerCode).Select(x => x.TriggerCounter).DefaultIfEmpty().Max();
        }

        public int GetCounterUp(string touchTagCode)
        {
            return DBContext.TouchTagMessages.Where(x => x.Device == touchTagCode).Select(x => x.Counter_up).DefaultIfEmpty().Max();
        }

        public string GetTouchTagCode(string assetId)
        {
            return DBContext.TouchTags.Where(x => x.AssetId == assetId).Select(x => x.Id).FirstOrDefault();
        }

        public string GetAssetId(string touchTagCode)
        {
            return DBContext.TouchTags.Where(x => x.Id == touchTagCode).Select(x => x.AssetId).FirstOrDefault();
        }
    }
}