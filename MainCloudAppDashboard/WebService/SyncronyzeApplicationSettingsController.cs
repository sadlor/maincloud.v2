﻿using MainCloudAppDashboard.Models;
using MainCloudAppDashboard.Repositories;
using MainCloudAppDashboard.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace MainCloudAppDashboard.WebService
{
    public class SyncronyzeApplicationSettingsController : ApiController
    {
        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        [HttpPost]
        [AcceptVerbs("POST")]
        public Dictionary<string, object> Post([FromBody]Dictionary<string, object> value)
        {
            if (!value.ContainsKey(ApplicationSettingsKey.ApplicationId.ToString()) || value[ApplicationSettingsKey.ApplicationId.ToString()] == null)
            {
                return null;
            }

            string applicationId = value["ApplicationId"] as string;
            string authInfo = "main_api_rest_user" + ":" + "main_api_rest_password";
            InstalledApplicationRepository applicationInstalledRepository = new InstalledApplicationRepository();
            InstalledApplication applicationInstalled = applicationInstalledRepository.findByApplicationId(applicationId);
            if (applicationInstalled == null)
            {
                applicationInstalled = new InstalledApplication();
                applicationInstalled.ApplicationId = applicationId;
                applicationInstalled.SettingsMap = value;
                applicationInstalled.ApplicationRegistrationDate = DateTime.Now;
                applicationInstalledRepository.Insert(applicationInstalled);
            }
            else
            {
                authInfo = applicationInstalled.SettingsMap[ApplicationSettingsKey.BasicAuthUser.ToString()] 
                    + ":" 
                    + applicationInstalled.SettingsMap[ApplicationSettingsKey.BasicAuthPassword.ToString()];
            }

            string authorization = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            if (Request.Headers.Authorization.Parameter != authorization)
            {
                return null;
            }

            applicationInstalled.LastDataSyncronize = DateTime.Now;
            applicationInstalled.LastLicenseCheck = DateTime.Now;

            ApplicationSettingsHelper.UpdateSettingsKeys(applicationInstalled.SettingsMap);
            applicationInstalledRepository.SaveChanges();
            return applicationInstalled.SettingsMap;
        }

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}