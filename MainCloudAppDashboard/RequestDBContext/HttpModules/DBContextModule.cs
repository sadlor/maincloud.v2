﻿using MainCloudAppDashboard.Models;
using System;

namespace MainCloudAppDashboard.RequestDBContext.HttpModules
{
    public class DBContextModule : BaseDBContextModule
    {
        // Lista dei db context da rendere disponibili
        override protected Type[] DbContextTypesList
        {
            get
            {
                return new Type[]
                {
                    typeof(ApplicationDbContext)
                };
            }
        }
    }
}
