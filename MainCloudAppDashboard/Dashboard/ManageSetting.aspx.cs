﻿using MainCloudAppDashboard.Models;
using MainCloudAppDashboard.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Host
{
    public partial class ManageSetting : Page
    {
        private InstalledApplicationRepository applicationInstalledRepository = new InstalledApplicationRepository();
        private InstalledApplication applicationInstalled;

        private void GridViewDataBind()
        {
            DgvSettings.DataSource = applicationInstalled != null ? applicationInstalled.SettingsMap : null;
            DgvSettings.DataBind();

            DgvSettings.UseAccessibleHeader = true;
            if (DgvSettings.HeaderRow != null)
            {
                DgvSettings.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            //ddlCustomer.DataSource = mainCustomerRepository.FindAll().ToList();
            //var customerId = aspNetApplicationService.GetCustomerId();
            //if (customerId != null)
            //{
            //    ddlCustomer.SelectedValue = customerId;
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            applicationInstalled = applicationInstalledRepository.FindByID(Request.QueryString["id"]);
            GridViewDataBind();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            GridViewDataBind();
        }

        protected void DgvRoles_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
        }

        protected void DgvRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        //    var usersContext = new ApplicationDbContext();
        //    string id = e.Keys["ID"] as string;
        //    var role = usersContext.Roles.FirstOrDefault(u => u.Id == id);
        //    if (role != null)
        //    {
        //        usersContext.Roles.Remove(role);
        //        //usersContext.Entry(user).State = EntityState.Deleted;
        //        usersContext.SaveChanges();
        //    }
        //    GridViewDataBind();
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                applicationInstalled.SettingsMap.Add(txtNewKey.Text, txtNewValue.Text);
                applicationInstalledRepository.SaveChanges();
                txtNewKey.Text = "";
                txtNewValue.Text = "";
                GridViewDataBind();
            }
            catch(Exception ex)
            {
                AlertMessage.Text = string.Join(",", ex.Message);
            }
        }

        protected void gridAction_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                string key = btn.CommandArgument ?? null;
                if (!string.IsNullOrEmpty(key))
                {
                    switch (btn.CommandName)
                    {
                        case "EditConfig":
                            DgvSettings.Visible = InsertPanel.Visible = false;
                            EditPanel.Visible = true;
                            txtEditKey.Text = key;
                            txtEditVal.Text = "" + applicationInstalled.SettingsMap[key];
                            break;
                        case "DeleteConfig":
                            //                        ApplicationSettingsHelper.UpdateSettings()
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                AlertMessage.Text = string.Join(",", ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DgvSettings.Visible = InsertPanel.Visible = true;
                EditPanel.Visible = false;
                if (applicationInstalled.SettingsMap.ContainsKey(txtEditKey.Text))
                {
                    applicationInstalled.SettingsMap[txtEditKey.Text] = txtEditVal.Text;
                }
                else
                {
                    applicationInstalled.SettingsMap.Add(txtEditKey.Text, txtEditVal.Text);
                }
                applicationInstalledRepository.SaveChanges();
            }
            catch (Exception ex)
            {
                AlertMessage.Text = string.Join(",", ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            DgvSettings.Visible = InsertPanel.Visible = true;
            EditPanel.Visible = false;
        }
    }
}