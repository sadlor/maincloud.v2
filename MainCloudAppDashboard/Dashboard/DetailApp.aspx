﻿<%@ Page Title="Gestione installazione" Language="C#" MasterPageFile="~/Dashboard/Dashboard.master" AutoEventWireup="true" CodeBehind="DetailApp.aspx.cs" Inherits="MainCloudAppDashboard.Dashboard.DetailApp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">    
    <asp:HiddenField ID="IdHiddenField" runat="server"/>
    <div class="form-horizontal">
        <div class="form-group">
            <label for="ApplicationId" class="col-sm-3 control-label">Application Id</label>
            <div class="col-sm-9">
                <asp:TextBox ID="ApplicationId" runat="server" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <label for="OwnerCustomer" class="col-sm-3 control-label">Owner Customer</label>
            <div class="col-sm-9">
                <telerik:RadComboBox RenderMode="Lightweight" ID="OwnerCustomer" runat="server" class="form-control"
                        Filter="Contains" MarkFirstMatch="true" ChangeTextOnKeyBoardNavigation="false" Width="100%"
                        AllowCustomText="true" DataTextField="RagSoc" DataValueField="Id">
                </telerik:RadComboBox>
            </div>
        </div>
        <div class="form-group">
            <label for="Description" class="col-sm-3 control-label">Description</label>
            <div class="col-sm-9">
                <asp:TextBox ID="Description" runat="server" class="form-control" Width="100%" />
            </div>
        </div>
        <div class="form-group">
            <label for="ITContact" class="col-sm-3 control-label">IT Contact</label>
            <div class="col-sm-9">
                <asp:TextBox ID="ITContact" runat="server" class="form-control" Width="100%"/>
            </div>
        </div>
        
        <div class="form-group">
            <label for="ApplicationRegistrationDate" class="col-sm-3 control-label">Application Registration Date</label>
            <div class="col-sm-9">
                <telerik:RadDatePicker ID="ApplicationRegistrationDate" runat="server" class="form-control" Enabled="false" />
            </div>
        </div>
        
        <div class="form-group">
            <label for="InstalledModuleEnabled" class="col-sm-3 control-label">Installed Module Enabled</label>
            <div class="col-sm-9">
                <asp:CheckBoxList ID="InstalledModuleEnabled" runat="server" Width="100%" RepeatLayout="Table"
                    DataTextField="Name" DataValueField="Code">
                </asp:CheckBoxList>
            </div>
        </div>

        <div class="form-group">
            <label for="DataSyncronizeIntervalMinutes" class="col-sm-3 control-label">DataBase Syncronize Interval Minutes</label>
            <div class="col-sm-9">
                <telerik:RadNumericTextBox ID="DataSyncronizeIntervalMinutes" runat="server" class="form-control" NumberFormat-DecimalDigits="0"/>
            </div>
        </div>
        <div class="form-group">
            <label for="LastDataSyncronize" class="col-sm-3 control-label">Last Data Syncronize</label>
            <div class="col-sm-9">
                <telerik:RadDatePicker ID="LastDataSyncronize" runat="server" class="form-control" />
            </div>
        </div>

        <hr />

        <div class="form-group">
            <label for="LicenseExpiration" class="col-sm-3 control-label">License Expiration</label>
            <div class="col-sm-9">
                <telerik:RadDatePicker ID="LicenseExpiration" runat="server" class="form-control" />
            </div>
        </div>
        <div class="form-group">
            <label for="DaysWaitingBeforeLicenseLock" class="col-sm-3 control-label">Days Waiting Before License Lock</label>
            <div class="col-sm-9">
                <telerik:RadNumericTextBox ID="DaysWaitingBeforeLicenseLock" runat="server" class="form-control" NumberFormat-DecimalDigits="0"/>
            </div>
        </div>
        <div class="form-group">
            <label for="LicenseExpirationContactEmails" class="col-sm-3 control-label">License Expiration Contact Emails</label>
            <div class="col-sm-9">
                <asp:TextBox ID="LicenseExpirationContactEmails" runat="server" class="form-control" Width="100%"/>
            </div>
        </div>
        <div class="form-group">
            <label for="LastLicenseCheck" class="col-sm-3 control-label">Last License Check</label>
            <div class="col-sm-9">
                <telerik:RadDatePicker ID="LastLicenseCheck" runat="server" class="form-control" />
            </div>
        </div>
        <hr />
        <div class="form-group">
            <label for="BasicAuthUser" class="col-sm-3 control-label">Basic Auth User</label>
            <div class="col-sm-9">
                <asp:TextBox ID="BasicAuthUser" runat="server" class="form-control" Width="100%"/>
            </div>
        </div>
        <div class="form-group">
            <label for="BasicAuthPassword" class="col-sm-3 control-label">Basic Auth Password</label>
            <div class="col-sm-9">
                <asp:TextBox ID="BasicAuthPassword" runat="server" class="form-control" Width="100%"/>
            </div>
        </div>

        <asp:Button ID="SaveButton" runat="server" CssClass="btn btn-primary" Text="Salva" OnClick="SaveButton_Click" />
        <asp:Button ID="DeleteButton" runat="server" CssClass="btn btn-danger" Text="Elimina" OnClick="DeleteButton_Click" />
        <asp:HyperLink ID="CancelButton" runat="server" CssClass="btn btn-default" Text="Annulla"  NavigateUrl="~/Dashboard/ListApp"/>
    </div>
</asp:Content>