﻿<%@ Page Title="Application configuration" Language="C#" MasterPageFile="~/Dashboard/Dashboard.master" AutoEventWireup="true" CodeBehind="ManageSetting.aspx.cs" Inherits="MainCloud.Host.ManageSetting" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label runat="server" id="AlertMessage" />

    <div class="panel panel-default" id="InsertPanel" runat="server" visible="true">
        <div class="panel-body">
            <div class="form-inline">
                <div class="form-group">
                    <asp:Label runat="server" Text="Key" AssociatedControlID="txtNewKey" />
                    <asp:TextBox ID="txtNewKey" runat="server"  />
                    <asp:Label runat="server" Text="Value" AssociatedControlID="txtNewValue" />
                    <asp:TextBox ID="txtNewValue" runat="server"  />
                </div>
                <asp:Button runat="server" ID="BtnAdd" class="btn btn-primary" Text="Add Config" OnClick="BtnAdd_Click" />
            </div>
        </div>
    </div>

    <asp:GridView ID="DgvSettings" runat="server" DataKeyNames="Key"
        CssClass="table table-hover table-striped" GridLines="None" 
        AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="Key" HeaderText="Name" />
            <asp:BoundField DataField="Value" HeaderText="Value" />
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton runat="server" CausesValidation="False" CommandArgument='<%#Eval("Key") %>' CommandName="EditConfig" OnClick="gridAction_Click" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit</asp:LinkButton>
<%--                    <asp:LinkButton runat="server" CausesValidation="False" CommandArgument='<%#Eval("Key") %>' CommandName="DeleteConfig" OnClick="gridAction_Click" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</asp:LinkButton>--%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="cursor-pointer" />
    </asp:GridView>

    <div class="panel panel-default" id="EditPanel" runat="server" visible="false">
        <div class="panel-body">
            <div class="form-inline">
                <div class="form-group">
                    <asp:Label runat="server" Text="Key" AssociatedControlID="txtEditKey" />
                    <asp:TextBox ID="txtEditKey" runat="server" Enabled="false" />
                    <asp:Label runat="server" Text="Value" AssociatedControlID="txtEditVal" />
                    <asp:TextBox ID="txtEditVal" runat="server"/>
                </div>
                <asp:Button runat="server" ID="btnSave" class="btn btn-primary" Text="Save" OnClick="btnSave_Click" />
                <asp:Button runat="server" ID="btnCancel" class="btn btn-default" Text="Cancel" OnClick="btnCancel_Click" />
            </div>
        </div>
    </div>

</asp:Content>
