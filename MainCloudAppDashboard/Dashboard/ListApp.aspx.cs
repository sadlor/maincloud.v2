﻿using MainCloudAppDashboard.Helpers;
using MainCloudAppDashboard.Models;
using MainCloudAppDashboard.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloudAppDashboard.Dashboard
{
    public partial class ListApp : System.Web.UI.Page
    {
        private InstalledApplicationRepository applicationInstalledRepository = new InstalledApplicationRepository();

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            //    AppInstalledGrid.DataBind();
            //}
        }

        protected void AppInstalledGrid_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                AppInstalledGrid.DataSource = applicationInstalledRepository.FindAll().ToList();
            }
        }
    }
}