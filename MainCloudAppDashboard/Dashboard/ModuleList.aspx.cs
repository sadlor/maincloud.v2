﻿using MainCloudAppDashboard.Models;
using MainCloudAppDashboard.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloudAppDashboard.Dashboard
{
    public partial class ModuleList : System.Web.UI.Page
    {
        private AvailableModuleRepository availableModuleRepository = new AvailableModuleRepository();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ModulesGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                ModulesGrid.DataSource = availableModuleRepository.FindAll().ToList();
            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            availableModuleRepository.Insert(new AvailableModule() { Name = txtName.Text, Code = txtCode.Text });
            availableModuleRepository.SaveChanges();
            ModulesGrid.Rebind();
        }

        protected void ModulesGrid_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                string id = ((Telerik.Web.UI.GridEditableItem)e.Item).GetDataKeyValue("Id").ToString();
                availableModuleRepository.Delete(availableModuleRepository.FindByID(id));
                availableModuleRepository.SaveChanges();
                ModulesGrid.Rebind();
            }
        }
    }
}