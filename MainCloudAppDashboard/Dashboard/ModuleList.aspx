﻿<%@ Page Title="Lista Moduli" Language="C#" MasterPageFile="~/Dashboard/Dashboard.master" AutoEventWireup="true" CodeBehind="ModuleList.aspx.cs" Inherits="MainCloudAppDashboard.Dashboard.ModuleList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="row">
            <div class="col">

                 <div class="panel panel-default" id="InsertPanel" runat="server" visible="true">
                    <div class="panel-body">
                        <div class="form-inline">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Key" AssociatedControlID="txtCode" />
                                <asp:TextBox ID="txtCode" runat="server"  />
                                <asp:Label runat="server" Text="Value" AssociatedControlID="txtName" />
                                <asp:TextBox ID="txtName" runat="server"  />
                            </div>
                            <asp:Button runat="server" ID="BtnAdd" class="btn btn-primary" Text="Add Module" OnClick="BtnAdd_Click" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <br />
        <div class="row">
            <div class="col">
                <telerik:RadGrid RenderMode="Lightweight" ID="ModulesGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                    OnNeedDataSource="ModulesGrid_NeedDataSource" OnItemCommand="ModulesGrid_ItemCommand">
                    <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Code" DataField="Code">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Name" DataField="Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ButtonCssClass="btn btn-danger" Text="Delete" ButtonType="PushButton" CommandName="Delete">  
                            </telerik:GridButtonColumn> 
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
</asp:Content>
