﻿using MainCloudAppDashboard.Models;
using MainCloudAppDashboard.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloudAppDashboard.Dashboard
{
    public partial class DetailApp : System.Web.UI.Page
    {
        private InstalledApplicationRepository applicationInstalledRepository = new InstalledApplicationRepository();
        private CustomerRepository customerRepository = new CustomerRepository();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] == null)
                {
                    setFrom(new InstalledApplication());
                }
                else
                {
                    InstalledApplication applicationInstalled = applicationInstalledRepository.FindByID(Request.QueryString["id"]);
                    setFrom(applicationInstalled);
                }
            }
        }

        private void setFrom(InstalledApplication applicationInstalled)
        {
            IdHiddenField.Value = applicationInstalled.Id;
            ApplicationId.Text = applicationInstalled.ApplicationId;
            Description.Text = applicationInstalled.Description;
            ITContact.Text = applicationInstalled.ITContact;
            DaysWaitingBeforeLicenseLock.Value = applicationInstalled.DaysWaitingBeforeLicenseLock;
            DataSyncronizeIntervalMinutes.Value = applicationInstalled.DataSyncronizeIntervalMinutes;
            LastDataSyncronize.SelectedDate = applicationInstalled.LastDataSyncronize;
            LastLicenseCheck.SelectedDate = applicationInstalled.LastLicenseCheck;
            LicenseExpiration.SelectedDate = applicationInstalled.LicenseExpirationDate;


            ApplicationRegistrationDate.SelectedDate = applicationInstalled.ApplicationRegistrationDate;
            LicenseExpirationContactEmails.Text = applicationInstalled.LicenseExpirationContactEmails;

            AvailableModuleRepository availableModuleRepository = new AvailableModuleRepository();
            InstalledModuleEnabled.DataSource = availableModuleRepository.FindAll().ToList();
            InstalledModuleEnabled.DataBind();
            foreach (var item in applicationInstalled.InstalledModuleEnabled)
            {
                var i = InstalledModuleEnabled.Items.FindByValue(item.ToString());
                if (i != null)
                {
                    i.Selected = true;
                }
            }

            OwnerCustomer.DataSource = customerRepository.FindAll().ToList();
            OwnerCustomer.SelectedValue = applicationInstalled.OwnerCustomer == null ? null : applicationInstalled.OwnerCustomer.Id;
            OwnerCustomer.DataBind();

            BasicAuthUser.Text = applicationInstalled.BasicAuthUser;
            BasicAuthPassword.Text = applicationInstalled.BasicAuthPassword;
        }

        private InstalledApplication readForm()
        {
            InstalledApplication applicationInstalled = applicationInstalledRepository.FindByID(IdHiddenField.Value);
            if (applicationInstalled == null)
            {
                applicationInstalled = new InstalledApplication();
            }
            applicationInstalled.Id = IdHiddenField.Value;
            applicationInstalled.ApplicationId = ApplicationId.Text;
            applicationInstalled.Description = Description.Text;
            applicationInstalled.ITContact = ITContact.Text;
            applicationInstalled.DaysWaitingBeforeLicenseLock = (int)(DaysWaitingBeforeLicenseLock.Value ?? 0);
            applicationInstalled.DataSyncronizeIntervalMinutes = (int)(DataSyncronizeIntervalMinutes.Value ?? 0);
            applicationInstalled.LastDataSyncronize = LastDataSyncronize.SelectedDate.HasValue ? new DateTime?(LastDataSyncronize.SelectedDate.Value.Date) : null;
            applicationInstalled.LastLicenseCheck = LastLicenseCheck.SelectedDate.HasValue ? new DateTime?(LastLicenseCheck.SelectedDate.Value.Date) : null;
            applicationInstalled.LicenseExpirationDate = LicenseExpiration.SelectedDate ?? LicenseExpiration.SelectedDate.Value.Date;
            applicationInstalled.LicenseExpirationContactEmails = LicenseExpirationContactEmails.Text;

            applicationInstalled.InstalledModuleEnabled.Clear();
            foreach (ListItem item in InstalledModuleEnabled.Items)
            {
                if (item.Selected)
                {
                    applicationInstalled.InstalledModuleEnabled.Add(item.Value);
                }
            }


            Customer customer = customerRepository.FindByID(OwnerCustomer.SelectedValue);
            if (customer != null)
            {
//                applicationInstalled.OwnerCustomer = customer;
                applicationInstalled.OwnerCustomerId = customer.Id;
            }
            else if (OwnerCustomer.Text != null)
            {
                customer = new Customer();
                customer.RagSoc = OwnerCustomer.Text;
                applicationInstalled.OwnerCustomer = customer;
                applicationInstalled.OwnerCustomerId = customer.Id;
            }

            applicationInstalled.BasicAuthUser = BasicAuthUser.Text;
            applicationInstalled.BasicAuthPassword = BasicAuthPassword.Text;
            return applicationInstalled;
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            InstalledApplication applicationInstalled = readForm();
            if (Request.QueryString["id"] == null)
            {
                applicationInstalledRepository.Insert(applicationInstalled);
            }
            else
            {
                applicationInstalledRepository.Update(applicationInstalled);
            }
            applicationInstalledRepository.SaveChanges();
            Response.Redirect("~/Dashboard/ListApp");
        }

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            InstalledApplication applicationInstalled = readForm();
            applicationInstalled.OwnerCustomer = null;
            applicationInstalled.OwnerCustomerId = null;
            applicationInstalledRepository.Delete(applicationInstalled);
            applicationInstalledRepository.SaveChanges();
            Response.Redirect("~/Dashboard/ListApp");
        }
    }
}