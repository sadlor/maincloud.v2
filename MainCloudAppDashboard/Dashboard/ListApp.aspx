﻿<%@ Page Title="Lista Installazioni" Language="C#" MasterPageFile="~/Dashboard/Dashboard.master" AutoEventWireup="true" CodeBehind="ListApp.aspx.cs" Inherits="MainCloudAppDashboard.Dashboard.ListApp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
    <div class="row">
        <div class="col">
       <asp:HyperLink runat="server" ID="RadLinkButton1" CssClass="btn btn-primary" ToolTip="Censisci nuova installazione" NavigateUrl="~/Dashboard/DetailApp">
           <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Censisci nuova installazione
        </asp:HyperLink>
    </div>
    </div>
    <br />
    <div class="row">
    <div class="col">
       <telerik:RadGrid RenderMode="Lightweight" ID="AppInstalledGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
            PageSize="7" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
            OnNeedDataSource="AppInstalledGrid_NeedDataSource">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView DataKeyNames="ApplicationId" AllowMultiColumnSorting="True">
                <Columns>
                    <telerik:GridBoundColumn SortExpression="Description" HeaderText="Description" HeaderButtonType="TextButton" DataField="Description">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="OwnerCustomer" HeaderText="Customer" HeaderButtonType="TextButton" DataField="OwnerCustomer">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="LicenseExpirationDate" HeaderText="License Expiration Date" HeaderButtonType="TextButton" DataField="LicenseExpirationDate">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="LastLicenseCheck" HeaderText="Last License Check" HeaderButtonType="TextButton" DataField="LastLicenseCheck">
                    </telerik:GridBoundColumn>
                    <telerik:GridHyperLinkColumn DataTextFormatString="Dettaglio" DataNavigateUrlFields="Id"
                        UniqueName="Id" DataNavigateUrlFormatString="~/Dashboard/DetailApp?id={0}" DataTextField="ApplicationId" >  
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridHyperLinkColumn DataTextFormatString="All Settings" DataNavigateUrlFields="Id"
                        UniqueName="Id" DataNavigateUrlFormatString="~/Dashboard/ManageSetting?id={0}" DataTextField="ApplicationId" >  
                    </telerik:GridHyperLinkColumn> 
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </div>
        </div>
            </div>
</asp:Content>
