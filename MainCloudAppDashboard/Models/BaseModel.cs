﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudAppDashboard.Models
{
    public class BaseModel
    {
        public BaseModel() : base()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}