﻿using MainCloudAppDashboard.Repositories;
using MainCloudAppDashboard.RequestDBContext;
using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using MainCloudAppDashboard.Models;
using System.Runtime.Remoting.Contexts;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MainCloudAppDashboard.Models
{
    public class ApplicationDbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            InitializeIdentityForEF(context);
            base.Seed(context);
        }

        public void InitializeIdentityForEF(ApplicationDbContext db)
        {
            // Se non è presente nessun utente inizializza i dati nel DB
            if (!db.Users.Any())
            {
                string userName = "host@host.it";
                string password = "Password1!";

                DbContextLocator<ApplicationDbContext>.DbContextForce = db;
                // Create first user


                var userStore = new UserStore<ApplicationUser>(db);
                var manager = new UserManager<ApplicationUser>(userStore);

//                var manager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = new ApplicationUser() { UserName = userName, Email = userName };
                IdentityResult result = manager.Create(user, password);
            }
        }

    }
}