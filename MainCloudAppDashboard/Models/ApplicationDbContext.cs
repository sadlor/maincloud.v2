﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MainCloudAppDashboard.Models
{

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Customer> Customers { get; set; }

        public DbSet<InstalledApplication> InstalledApplication { get; set; }

        public DbSet<AvailableModule> AvailableModule { get; set; }

        public DbSet<Log> Logs { get; set; }


        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new ApplicationDbInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            var currentUsername = HttpContext.Current != null && HttpContext.Current.User != null
           ? HttpContext.Current.User.Identity.Name
           : "Anonymous";

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Added && x.Entity.CreatedAt == default(DateTime)))
            {
                entry.Entity.CreatedAt = DateTime.Now;
            }

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Added && x.Entity.CreatedBy == null))
            {
                entry.Entity.CreatedBy = currentUsername;
            }

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Modified))
            {
                //        string name = ReflectionUtility.GetPropertyName(() => model.CreatedAt);
                //        entry.Property(name).IsModified = false;

                entry.Entity.ModifiedAt = DateTime.Now;
                entry.Entity.ModifiedBy = currentUsername;
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges()
        {
            var currentUsername = HttpContext.Current != null && HttpContext.Current.User != null
                        ? HttpContext.Current.User.Identity.Name
                        : "Anonymous";

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Added && x.Entity.CreatedAt == default(DateTime)))
            {
                entry.Entity.CreatedAt = DateTime.Now;
            }

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Added && x.Entity.CreatedBy == null))
            {
                entry.Entity.CreatedBy = currentUsername;
            }

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Modified))
            {
                //        string name = ReflectionUtility.GetPropertyName(() => model.CreatedAt);
                //        entry.Property(name).IsModified = false;

                entry.Entity.ModifiedAt = DateTime.Now;
                entry.Entity.ModifiedBy = currentUsername;
            }

            return base.SaveChanges();
        }
    }
}