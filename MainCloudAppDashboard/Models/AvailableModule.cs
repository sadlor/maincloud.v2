﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MainCloudAppDashboard.Models
{
    public class AvailableModule : BaseModel
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}