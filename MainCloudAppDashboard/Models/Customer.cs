﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MainCloudAppDashboard.Models
{
    public class Customer : BaseModel
    {
        public string RagSoc { get; set; }

        [InverseProperty("OwnerCustomer")]
        public virtual ICollection<InstalledApplication> ApplicationaInstalled { get; set; }

        public override string ToString()
        {
            return RagSoc;
        }
    }
}