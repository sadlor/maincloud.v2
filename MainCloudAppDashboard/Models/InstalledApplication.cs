﻿using MainCloudAppDashboard.Web.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MainCloudAppDashboard.Models
{
    public class InstalledApplication : BaseModel
    {
        public InstalledApplication() : base()
        {
            LicenseExpirationDate = DateTime.Now;
            DaysWaitingBeforeLicenseLock = 15;
            LastLicenseCheck = null;
            DataSyncronizeIntervalMinutes = 0;
            LastDataSyncronize = null;
            ApplicationRegistrationDate = null;
            LicenseExpirationContactEmails = null;
            InstalledModuleEnabled = new JArray();
        }

        [Required]
        [MaxLength(128)]
        public string ApplicationId { get; set; }

        public string Description { get; set; }
        public string ITContact { get; set; }

        public DateTime? LastLicenseCheck { get; set; }

        public DateTime? LastDataSyncronize { get; set; }

        [MaxLength(128)]
        public string OwnerCustomerId { get; set; }

        [ForeignKey("OwnerCustomerId")]
        public virtual Customer OwnerCustomer { get; set; }

        private string settings;
        public string Settings
        {
            get
            {
                if (settingsMap != null)
                {
                    return JsonConvert.SerializeObject(settingsMap);
                }
                else
                {
                    return settings;
                }
            }
            set
            {
                settings = value;
                settingsMap = null;
            }
        }

        [NotMapped]
        private Dictionary<string, object> settingsMap;
        public Dictionary<string, object> SettingsMap
        {
            get
            {
                if (settingsMap == null)
                {
                    settingsMap = Settings == null ? 
                        new Dictionary<string, object>() :
                        JsonConvert.DeserializeObject<Dictionary<string, object>>(Settings);
                }
                return settingsMap;
            }
            set
            {
                settingsMap = value;
            }
        }

        [NotMapped]
        public int DaysWaitingBeforeLicenseLock
        {
            get { return SettingsMap[ApplicationSettingsKey.DaysWaitingBeforeLicenseLock.ToString()] as int? ?? 0; }
            set { SettingsMap[ApplicationSettingsKey.DaysWaitingBeforeLicenseLock.ToString()] = value; }
        }

        [NotMapped]
        public int DataSyncronizeIntervalMinutes
        {
            get { return SettingsMap[ApplicationSettingsKey.DataSyncronizeIntervalMinutes.ToString()] as int? ?? 0; }
            set { SettingsMap[ApplicationSettingsKey.DataSyncronizeIntervalMinutes.ToString()] = value; }
        }

        [NotMapped]
        public DateTime LicenseExpirationDate
        {
            get { return SettingsMap[ApplicationSettingsKey.LicenseExpirationDate.ToString()] as DateTime? ?? DateTime.Now; }
            set { SettingsMap[ApplicationSettingsKey.LicenseExpirationDate.ToString()] = value; }
        }

        [NotMapped]
        public DateTime? ApplicationRegistrationDate
        {
            get { return SettingsMap[ApplicationSettingsKey.ApplicationRegistrationDate.ToString()] as DateTime?; }
            set { SettingsMap[ApplicationSettingsKey.ApplicationRegistrationDate.ToString()] = value; }
        }

        [NotMapped]
        public string LicenseExpirationContactEmails
        {
            get { return SettingsMap[ApplicationSettingsKey.LicenseExpirationContactEmails.ToString()] as string; }
            set { SettingsMap[ApplicationSettingsKey.LicenseExpirationContactEmails.ToString()] = value; }
        }

        [NotMapped]
        public JArray InstalledModuleEnabled
        {
            get {
                JArray val = SettingsMap[ApplicationSettingsKey.InstalledModuleEnabled.ToString()] as JArray;
                if (val == null)
                {
                    SettingsMap[ApplicationSettingsKey.InstalledModuleEnabled.ToString()] = new JArray();
                    return SettingsMap[ApplicationSettingsKey.InstalledModuleEnabled.ToString()] as JArray;
                }
                return val;
            }
            set { SettingsMap[ApplicationSettingsKey.InstalledModuleEnabled.ToString()] = value; }
        }

        [NotMapped]
        public string BasicAuthUser
        {
            get { return SettingsMap[ApplicationSettingsKey.BasicAuthUser.ToString()] as string; }
            set { SettingsMap[ApplicationSettingsKey.BasicAuthUser.ToString()] = value; }
        }

        [NotMapped]
        public string BasicAuthPassword
        {
            get { return SettingsMap[ApplicationSettingsKey.BasicAuthPassword.ToString()] as string; }
            set { SettingsMap[ApplicationSettingsKey.BasicAuthPassword.ToString()] = value; }
        }
    }
}