﻿using MainCloudAppDashboard.Helpers;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MainCloudAppDashboard.Startup))]
namespace MainCloudAppDashboard
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);

            LicenseVerifierHelper.StartLicenseCheckAsync();
        }
    }
}
