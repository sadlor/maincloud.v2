﻿using MainCloudAppDashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloudAppDashboard.Repositories
{
    public class InstalledApplicationRepository : BaseRepository<InstalledApplication, ApplicationDbContext>
    {
        public InstalledApplication findByApplicationId(string applicationId)
        {
            return DBContext.InstalledApplication.Where(x => x.ApplicationId == applicationId).FirstOrDefault();
        }

    }
}