﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using MainCloudAppDashboard.RequestDBContext;

namespace MainCloudAppDashboard.Repositories
{
    public class BaseRepository<T, D>
        where T : class 
        where D : DbContext
    {
        protected D DBContext
        {
            get
            {
                return new DbContextLocator<D>().DbContext;
            }
        }

        public IEnumerable<T> FindAll()
        {
            return DBContext.Set<T>();
        }

        public T FindByID(object id)
        {
            return DBContext.Set<T>().Find(id);
        }

        public void Insert(T entity)
        {
            DBContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            DBContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public T Delete(T entity)
        {
            if (entity != null)
            {
                DBContext.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
            }
            return entity;
        }

        /// <summary>
        /// Reads all objects in which all of the predicates return true.
        /// </summary>
        public virtual IQueryable<T> ReadAll(params Expression<Func<T, Boolean>>[] predicates)
        {
            IQueryable<T> results = DBContext.Set<T>();
            if (predicates == null || predicates.Length <= 0) { return results; }

            foreach (var predicate in predicates)
            {
                results = results.Where(predicate);
            }

            return results;
        }

        public void SaveChanges()
        {
            DBContext.SaveChanges();
        }

        public void DeleteAll ()
        {
            DBContext.Set<T>().RemoveRange(DBContext.Set<T>());
        }
    }
}
