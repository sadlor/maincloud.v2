﻿using MainCloudAppDashboard.Models;
using MainCloudAppDashboard.Repositories;
using MainCloudAppDashboard.RequestDBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MainCloudAppDashboard.Helpers
{
    public static class LicenseVerifierHelper
    {
        private static Timer TIMER;

        public static void CheckLicense()
        {
            DbContextLocator<ApplicationDbContext>.DbContextForce = new ApplicationDbContext();
            InstalledApplicationRepository applicationInstalledRepository = new InstalledApplicationRepository();

            /// Restituisce tutte le applicazioni dove la data di scadenza - dayBeforeExpiration &lt;= Now
            /// Invia una email al giorno
            List<InstalledApplication> apps = applicationInstalledRepository.FindAll().ToList();
            var dayBeforeExpiration = 15; // 15 gg prima della scadenza
            var compareDate = DateTime.Now.AddDays(dayBeforeExpiration);
            apps.ForEach(application =>
            {
                try
                {
                    // Se non è già stata fatta la verifica nello stesso giorno e se mancano "dayBeforeExpiration" giorni alla scadenza
                    if ((application.LastLicenseCheck == null || DateTime.Now.Date != application.LastLicenseCheck.Value.Date) && compareDate > application.LicenseExpirationDate)
                    {
                        SendNoticeEmail.SendEmailLicenseExpiration(application);
                    }
                    application.LastLicenseCheck = DateTime.Now;
                    applicationInstalledRepository.SaveChanges();
                }
                catch (Exception ex)
                {
                    
                }
            });
        }

        public static void StartLicenseCheckAsync()
        {
            var startTimeSpan = TimeSpan.Zero;
            //var periodTimeSpan = TimeSpan.FromMilliseconds(10000); // Per debug e test
            var periodTimeSpan = TimeSpan.FromHours(24);

            TIMER = new Timer((e) =>
            {
                CheckLicense();
            }, null, startTimeSpan, periodTimeSpan);
        }

    }
}