﻿using MainCloudAppDashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace MainCloudAppDashboard.Helpers
{
    public class SendNoticeEmail
    {

        public static void SendEmailLicenseExpiration(InstalledApplication application)
        {
            string title = "Avviso automatico di licenza in scadenza";
            string body = string.Format("Avviso AUTOMATICO<br/><br/>" +
                "La licenza per l'installazione {0} è in scadenza, si prega di provvedere al rinnovo.<br/><br/><br/>" +
                "<strong>Data di scadenza: {1:dd/MM/yyyy}</strong><br/>" +
                "<strong>Id installazione {2}</strong><br/>",
                application.Description,
                application.LicenseExpirationDate,
                application.ApplicationId);

            SendEmail(application.LicenseExpirationContactEmails, title, body);
        }

        public static void SendEmail(string toAddress, string subject, string body)
        {
            const string accountEmail = "main.notice1@gmail.com";
            const string fromPassword = "951.3y*7";
            var fromAddress = accountEmail;

            SmtpClient client = new SmtpClient()
            {
                Port = 587,
                UseDefaultCredentials = false,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(accountEmail, fromPassword),
                Timeout = 20000                
            };

            MailMessage mail = new MailMessage(fromAddress, toAddress);
            mail.Subject = subject;
            mail.IsBodyHtml = true;
            mail.Body = body;

            client.Send(mail);
        }
    }
}