﻿using MainCloudAppDashboard.Models;
using MainCloudAppDashboard.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudAppDashboard.Web.Helpers
{
    public enum ApplicationSettingsKey
    {
        LicenseMaxUser,
        EnergyParameterFilter,
        LicenseMaxAnalyzer,
        LicenseMaxGateway,
        LicenzaMaxPlant,
        LicenseWidgetAdvanced,
        LicenseWidgetAggregator,
        //OperatorDefaultApplicationUID,

        ApplicationId,

        InstalledModuleEnabled,
        RemoteServerUrl,
        BasicAuthUser,
        BasicAuthPassword,

        DaysWaitingBeforeLicenseLock,
        DataSyncronizeIntervalMinutes,
        LicenseExpirationDate,
        ApplicationRegistrationDate,
        LicenseExpirationContactEmails
    }

    public static class ApplicationSettingsHelper
    {
        public static Dictionary<string, object> UpdateSettingsKeys(Dictionary<string, object> settings)
        {
            foreach (ApplicationSettingsKey applicationSettingsKey in Enum.GetValues(typeof(ApplicationSettingsKey)))
            {
                if (!settings.ContainsKey(applicationSettingsKey.ToString())) {
                    settings.Add(applicationSettingsKey.ToString(), null);
                }
            }
            return settings;
        }

        public static T GetConfiguration<T>(ApplicationSettingsKey key, string applicationId)
        {
            return GetConfiguration<T>(key.ToString(), applicationId);
        }

        public static object GetConfiguration(ApplicationSettingsKey key, string applicationId)
        {
            return GetConfiguration(key.ToString(), applicationId);
        }

        public static Dictionary<string, object> GetConfigurations(string applicationId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                try
                {
                    InstalledApplicationRepository applicationInstalledRepository = new InstalledApplicationRepository();
                    string settings = applicationInstalledRepository.FindByID(applicationId).Settings;
                    if (settings != null || settings != string.Empty)
                    {
                        Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(settings);
                        return config;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static T GetConfiguration<T>(string key, string applicationId)
        {
            try
            {
                InstalledApplicationRepository applicationInstalledRepository = new InstalledApplicationRepository();
                string settings = applicationInstalledRepository.FindByID(applicationId).Settings;
                if (settings != null || settings != string.Empty)
                {
                    Dictionary<string, T> config = JsonConvert.DeserializeObject<Dictionary<string, T>>(settings);
                    return config[key];
                }
                else
                {
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        public static object GetConfiguration(string key, string applicationId)
        {
            try
            {
                InstalledApplicationRepository applicationInstalledRepository = new InstalledApplicationRepository();
                string settings = applicationInstalledRepository.FindByID(applicationId).Settings;
                if (settings != null || settings != string.Empty)
                {
                    Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(settings);
                    return config[key];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static void UpdateSettings(ApplicationSettingsKey key, object obj, string applicationId)
        {
            UpdateSettings(key.ToString(), obj, applicationId);
        }

        public static void UpdateSettings(string key, object obj, string applicationId)
        {
            InstalledApplicationRepository applicationInstalledRepository = new InstalledApplicationRepository();
            var application = applicationInstalledRepository.FindByID(applicationId);
            if (!string.IsNullOrEmpty(application.Settings))
            {
                Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(application.Settings);
                if (config.ContainsKey(key))
                {
                    config[key] = obj;
                    application.Settings = JsonConvert.SerializeObject(config);
                }
                else
                {
                    config.Add(key, obj);
                    application.Settings = JsonConvert.SerializeObject(config);
                }
            }
            else
            {
                Dictionary<string, object> config = new Dictionary<string, object>();
                config.Add(key, obj);
                application.Settings = JsonConvert.SerializeObject(config);
            }
            applicationInstalledRepository.SaveChanges();
        }

        public static void UpdateSettings(Dictionary<string, object> _params, string applicationId)
        {
            InstalledApplicationRepository applicationInstalledRepository = new InstalledApplicationRepository();
            var application = applicationInstalledRepository.FindByID(applicationId);
            if (application.Settings != null && application.Settings != string.Empty)
            {
                Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(application.Settings);
                foreach (var param in _params)
                {
                    if (config.ContainsKey(param.Key))
                    {
                        config[param.Key] = param.Value;
                    }
                    else
                    {
                        config.Add(param.Key, param.Value);
                    }
                }
                application.Settings = JsonConvert.SerializeObject(config);
            }
            else
            {
                application.Settings = JsonConvert.SerializeObject(_params);
            }
            applicationInstalledRepository.SaveChanges();
        }
    }
}