using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace MainCloudAppDashboard
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Permanent;
            routes.EnableFriendlyUrls(settings);

//            routes.Ignore("{*allaxd}", new { allaxd = @".*\.axd(/.*)?" });
//            routes.Ignore("{*svc}", new { alljs = @".*\.svc(/.*)?" });
        }
    }
}
