﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TouchTagModule;
using TouchTagModule.Models;
using TouchTagModule.Repositories;

namespace MainCloud.TouchTag
{
    public partial class TouchTagHttpAdapter : System.Web.UI.Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("HMILogFile");

        protected void Page_Load(object sender, EventArgs e)
        {
            log.Debug("è arrivata una richiesta da everynet");
            StreamReader reader = new StreamReader(HttpContext.Current.Request.InputStream);
            string data = reader.ReadToEnd();
            log.Debug("ho letto il messaggio - " + data);
            //File.WriteAllText("C:\\WebSites\\Test\\TouchTag\\log.txt", data);
            if (data != "")
            {
                try
                {
                    TouchTagRootObject receivedData = JsonConvert.DeserializeObject<TouchTagRootObject>(data);

                    TouchTagMessage message = new TouchTagMessage();
                    try
                    {
                        if (receivedData.@params.radio.hardware.gps == null)
                        {
                            message.Alt = 0;
                            message.Lat = 0;
                            message.Lng = 0;
                        }
                        else
                        {
                            message.Alt = receivedData.@params.radio.hardware.gps.alt;
                            message.Lat = receivedData.@params.radio.hardware.gps.lat;
                            message.Lng = receivedData.@params.radio.hardware.gps.lng;
                        }
                        message.Application = receivedData.meta.application;
                        message.Bandwidth = receivedData.@params.radio.modulation.bandwidth;
                        message.Channel = receivedData.@params.radio.hardware.channel;
                        message.Counter_up = receivedData.@params.counter_up;
                        message.Device = receivedData.meta.device;
                        message.Device_addr = receivedData.meta.device_addr;
                        message.Freq = receivedData.@params.radio.freq;
                        message.Gateway = receivedData.meta.gateway;
                        message.Network = receivedData.meta.network;
                        message.Packet_id = receivedData.meta.packet_id;
                        message.Time = GetMessageDate(receivedData.meta.time);
                        message.Packet_hash = receivedData.meta.packet_hash;
                        message.Coderate = receivedData.@params.radio.modulation.coderate;
                        message.Counter_up = receivedData.@params.counter_up;
                        message.Chain = (int)receivedData.@params.radio.hardware.chain;
                        message.Datarate = receivedData.@params.radio.datarate;
                        message.Delay = receivedData.@params.radio.delay;
                        message.Duplicate = receivedData.@params.duplicate;
                        message.Freq = receivedData.@params.radio.freq;
                        message.Packet_id = receivedData.meta.packet_id;
                        message.Rssi = (int)receivedData.@params.radio.hardware.rssi;
                        message.Rx_time = receivedData.@params.rx_time;
                        message.Size = receivedData.@params.radio.size;
                        message.Snr = receivedData.@params.radio.hardware.snr;
                        message.Spreading = (int)receivedData.@params.radio.modulation.spreading;
                        message.Tmst = receivedData.@params.radio.hardware.tmst;

                        //deserializzo payload da messaggio

                        Message deserializedMessage = Unpack(receivedData.@params.payload);

                        message.ConfigVersion = deserializedMessage.ConfigVersion.ToString();
                        message.FwVersion = deserializedMessage.FwVersion.ToString();
                        message.Pitch = deserializedMessage.Pitch;
                        message.Roll = deserializedMessage.Roll;
                        message.Temperature = deserializedMessage.Temperature;
                        message.TriggerCode = (int)deserializedMessage.TriggerCode;
                        message.TriggerCounter = (int)deserializedMessage.TriggerCounter;

                        if (deserializedMessage.TriggerCode == 7)
                            message.Status = "Off";
                        else message.Status = "On";
                        message.Event = typeOfEvent(message.TriggerCode);

                        //leggo ultimo counter per evento

                        //TouchTagMessageService service = new TouchTagMessageService();
                        //message.Counter_up=service.GetLastCountByEvent(message.TriggerCode)+1;

                        //saving all data from post event Everynet


                        TouchTagMessageRepository repo = new TouchTagMessageRepository();
                        repo.Insert(message);
                        repo.SaveChanges();

                        UpdateTransactions(message);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message, ex);
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message, ex);
                }
            }
        }

        private void UpdateTransactions(TouchTagMessage message)
        {
            try
            {
                TouchTagModule.Services.TouchTagMessageService tms = new TouchTagModule.Services.TouchTagMessageService();
                string moldId = tms.GetAssetId(message.Device);

                MES.Repositories.TransactionMoldRepository tRepos = new MES.Repositories.TransactionMoldRepository();
                MES.Services.TransactionMoldService tService = new MES.Services.TransactionMoldService();
                MES.Models.TransactionMold lastT = tService.GetLastTransactionOpen(moldId);
                if (lastT != null)
                {
                    MES.Models.TransactionMold newT = lastT;
                    lastT.End = message.Time;
                    lastT.Duration = (decimal)(lastT.End - lastT.Start).Value.TotalSeconds;
                    lastT.Open = false;
                    lastT.LastUpdate = DateTime.Now;
                    if (lastT.CounterStart > message.Counter_up)
                    {
                        lastT.CounterEnd = lastT.CounterStart + 1;
                    }
                    else
                    {
                        lastT.CounterEnd = message.Counter_up;
                    }
                    lastT.PartialCounting = lastT.CounterEnd.Value - lastT.CounterStart;
                    tRepos.Update(lastT);
                    tRepos.SaveChanges();

                    newT.Start = message.Time;
                    newT.End = null;
                    newT.Duration = 0;
                    newT.Open = true;
                    newT.LastUpdate = DateTime.Now;
                    newT.CounterStart = lastT.CounterEnd.Value;
                    newT.CounterEnd = null;
                    newT.PartialCounting = 0;
                    tRepos.Insert(newT);
                    tRepos.SaveChanges();
                }
                else
                {
                    lastT = new MES.Models.TransactionMold();
                    lastT.MoldId = moldId;
                    lastT.Start = message.Time;
                    lastT.Open = true;
                    lastT.LastUpdate = DateTime.Now;
                    lastT.CounterStart = message.Counter_up;
                    lastT.CounterEnd = message.Counter_up;
                    lastT.PartialCounting = lastT.CounterEnd.Value - lastT.CounterStart;
                    tRepos.Insert(lastT);
                    tRepos.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }
        }

        static string typeOfEvent(int code)
        {
            switch (code)
            {
                case 0: return "Riavvio";
                case 1: return "Timer";
                case 2: return "Click singolo";
                case 3: return "Inizio movimento";
                case 4: return "Fine movimento";
                case 5: return "Caduta libera";
                case 6: return "Attivazione";
                case 7: return "Disattivazione";
                case 8: return "Doppio click";
                case 9: return "Click lungo";
                case 10: return "Temperatura massima";
                case 11: return "Temperatura minima";
            }
            return "";
        }

        public DateTime GetMessageDate(double millis)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(millis).ToLocalTime();
            return dtDateTime;
        }

        private string GetDocumentContents(System.Web.HttpRequestBase Request)
        {
            string documentContents;
            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }
            return documentContents;
        }

        protected Message Unpack(string payload)
        {
            byte[] data = Convert.FromBase64String(payload);
            if (data[0] != 0x01)
                return null;
            byte[] temp = data.Skip(2).ToArray();
            if (data[1] == 0x00)
                return Message.Parser.ParseFrom(temp);
            return null;
        }
    }

    #region MessageStructure
    public class Meta
    {
        public string network { get; set; }
        public string packet_hash { get; set; }
        public string application { get; set; }
        public string device_addr { get; set; }
        public double time { get; set; }
        public string device { get; set; }
        public string packet_id { get; set; }
        public string gateway { get; set; }
    }

    public class Gps
    {
        public double lat { get; set; }
        public double lng { get; set; }
        public double alt { get; set; }
    }

    public class Hardware
    {
        public double status { get; set; }
        public double chain { get; set; }
        public long tmst { get; set; }
        public double snr { get; set; }
        public float rssi { get; set; }
        public int channel { get; set; }
        public Gps gps { get; set; }
    }

    public class Modulation
    {
        public int bandwidth { get; set; }
        public string type { get; set; }
        public double spreading { get; set; }
        public string coderate { get; set; }
    }

    public class Radio
    {
        public Hardware hardware { get; set; }
        public int datarate { get; set; }
        public Modulation modulation { get; set; }
        public double delay { get; set; }
        public double time { get; set; }
        public double freq { get; set; }
        public int size { get; set; }
    }

    public class Header
    {
        public bool ack { get; set; }
        public bool adr { get; set; }
        public bool confirmed { get; set; }
        public int version { get; set; }
        public int type { get; set; }
    }

    public class Lora
    {
        public Header header { get; set; }
        public List<object> mac_commands { get; set; }
    }

    public class Params
    {
        public string payload { get; set; }
        public int port { get; set; }
        public bool duplicate { get; set; }
        public Radio radio { get; set; }
        public int counter_up { get; set; }
        public Lora lora { get; set; }
        public double rx_time { get; set; }
        public string encrypted_payload { get; set; }
    }

    public class TouchTagRootObject
    {
        public Meta meta { get; set; }
        public string type { get; set; }
        public Params @params { get; set; }
    }
    #endregion
}