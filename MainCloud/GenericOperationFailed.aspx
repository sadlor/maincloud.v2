﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenericOperationFailed.aspx.cs" Inherits="MainCloud.GenericOperationFailed" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <h4>Impossibile effettuare l'operazione richiesta, tornare all'elenco dell'applicazioni</h4>
        <a class="navbar-brand" runat="server" href="~/">Applications</a>
        <asp:Image ImageUrl="~/Content/Images/home_SOL.jpg" runat="server" Width="100%" />
    
    </div>
    </form>
</body>
</html>
