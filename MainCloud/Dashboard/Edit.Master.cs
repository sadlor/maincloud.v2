﻿using MainCloudFramework.Web.BasePages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard
{
    public partial class EditMaster : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string returnUrl = Request["returnurl"];
                if (string.IsNullOrEmpty(returnUrl))
                {   
                    // Try with UrlReferrer 
                    if (Request.UrlReferrer != null)
                    {
                        returnUrl = Request.UrlReferrer.ToString();
                    }
                }
                ViewState["ReturnUrl"] = returnUrl;
            }
        }

        protected void lnkBtnBack_Click(object sender, EventArgs e)
        {
            object returnUrl = ViewState["ReturnUrl"];
            if (returnUrl != null)
            {
                Response.Redirect((string)returnUrl);
            }
        }

        protected void PageEditScriptManager_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        {
            if (e.Exception.Data["ExtraInfo"] != null)
            {
                PageEditScriptManager.AsyncPostBackErrorMessage =
                    e.Exception.Message +
                    e.Exception.Data["ExtraInfo"].ToString();
            }
            else
            {
                PageEditScriptManager.AsyncPostBackErrorMessage =
                    "An unspecified error occurred.";
            }
        }
    }
}