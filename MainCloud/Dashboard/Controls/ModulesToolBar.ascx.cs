﻿using MainCloudFramework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.Dashboard.Admin.Controls
{
    public partial class ModulesToolBar : System.Web.UI.UserControl
    {
        protected WebPartManager _manager;
        private GrantService grantService = new GrantService();

        protected void Page_Load(object sender, EventArgs e)
        {
            _manager = WebPartManager.GetCurrentWebPartManager(Page);
            _manager.DisplayModeChanged += widgets_manager_DisplayModeChanged;

            widgetsCatalog.Visible = false;

            if (!_manager.Personalization.IsModifiable || !_manager.Personalization.CanEnterSharedScope)
            {
                Visible = false;
            }

            if (Visible == false)
            {
                _manager.DisplayMode = _manager.SupportedDisplayModes["Browse"];
            }

            if (_manager.DisplayMode == _manager.SupportedDisplayModes["Catalog"])
            {
                widgetsCatalog.Visible = true;
            }

//            widgetAdvanced.Visible = grantService.ShowWidgetAdvanced();
//            tabWidgetAdvanced.Visible = grantService.ShowWidgetAdvanced();
//            widgetAggregator.Visible = grantService.ShowWidgetAggregator();
//            tabWidgetAggregator.Visible = grantService.ShowWidgetAggregator();
        }

        private void widgets_manager_DisplayModeChanged(object sender, WebPartDisplayModeEventArgs e)
        {
            widgetsCatalog.Visible = _manager.DisplayMode == _manager.SupportedDisplayModes["Catalog"];
        }
    }
}