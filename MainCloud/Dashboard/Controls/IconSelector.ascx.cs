﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloudFramework.Dashboard.Admin.Controls
{
    public partial class IconSelector : System.Web.UI.UserControl
    {
        [Bindable(true)]
        public String SelectedIcon {
            get
            {
                return SelectedIconField.Text;
            }
            set
            {
                SelectedIconField.Text = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Page.ClientScript.IsClientScriptBlockRegistered(this.GetType(), "iconselector"))
            {
                String script = @"$(function() {
                    // This event is only triggered when the actual input value is changed by user interaction
                    $('.icp').on('iconpickerSelected', function (e) {
                        //var elem = $('.picker-target', e.target).get(0);
                        var selectedIcon = e.iconpickerInstance.getValue(e.iconpickerValue);
                        //elem.className = 'picker-target fa-2x ' + e.iconpickerInstance.options.iconBaseClass + ' ' + selectedIcon;
                        //$('#' + $(e.target).attr('data-field')).val(e.iconpickerInstance.options.iconBaseClass + ' ' + selectedIcon);
                    });

                    $('.icp-auto').iconpicker({
                        inputSearch: true,
                        hideOnSelect: true
                    });
                });";

                ScriptManager.RegisterStartupScript(this, this.GetType(), "iconselector", script, true);
            }

            base.OnPreRender(e);
        }

    }
}