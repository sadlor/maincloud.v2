﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DisplayManager.ascx.cs" Inherits="MainCloudFramework.Dashboard.Admin.Controls.DisplayManager" %>
<%--<div class="form-group">--%>
<%--    <asp:Label ID="Label2" runat="server" AssociatedControlID="DisplayModeDropdown">Visualizzazione</asp:Label>--%>
    <asp:DropDownList ID="DisplayModeDropdown" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="DisplayModeDropdown_SelectedIndexChanged" style="margin-top:8px"/>
    <asp:LinkButton ID="ResetStateLinkButton" runat="server" Visible="false" Text="Reset User State" ToolTip="Reset the current user's personalization data for the page." OnClick="ResetStateLinkButton_Click" />
<%--</div>--%>

<asp:Panel ID="Panel2" runat="server" GroupingText="Personalization Scope" Visible="false" >
    <asp:RadioButton ID="UserScopeRadioButton" runat="server" Text="User" AutoPostBack="true" GroupName="Scope" OnCheckedChanged="PersonalizationScope_CheckedChanged" />
    <asp:RadioButton ID="SharedScopeRadioButton" runat="server" Text="Shared" AutoPostBack="true" GroupName="Scope" OnCheckedChanged="PersonalizationScope_CheckedChanged" />
</asp:Panel>