﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.Dashboard.Admin.Controls
{
    public partial class DisplayManager : UserControl
    {
        protected WebPartManager _manager;

        protected void DisplayModeDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Change the page to the selected display mode.
            string selectedMode = DisplayModeDropdown.SelectedValue;
            WebPartDisplayMode mode = _manager.SupportedDisplayModes[selectedMode];

            if (mode != null)
            {
                _manager.DisplayMode = mode;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Set the selected item equal to the current display mode.
            DisplayModeDropdown.SelectedValue = _manager.DisplayMode.Name;
        }

        protected void PersonalizationScope_CheckedChanged(object sender, EventArgs e)
        {
            if (_manager.Personalization.Scope == PersonalizationScope.Shared)
            {
                // If not in User personalization scope, toggle into it.
                _manager.Personalization.ToggleScope();
            }
            else
            {
                // not in Shared scope, if user has the permission toggle the scope to shared.
                if (_manager.Personalization.CanEnterSharedScope)
                {
                    _manager.Personalization.ToggleScope();
                }
            }
        }

        protected void Page_Init(object sender, System.EventArgs e)
        {
            Page.InitComplete += Page_InitComplete;
        }

        void Page_InitComplete(object sender, EventArgs e)
        {
            _manager = WebPartManager.GetCurrentWebPartManager(Page);

            if (!_manager.Personalization.IsModifiable)
            {
                Visible = false;
                return;
            }

            if (Page.IsPostBack)
                return;

            _manager.DisplayMode = WebPartManager.BrowseDisplayMode;

            string selectedValue = _manager.DisplayMode.Name;

            DisplayModeDropdown.Items.Clear();

            // Fill the drop-down CauseList with the names of supported display modes.
            foreach (WebPartDisplayMode mode in _manager.SupportedDisplayModes)
            {
                string modeName = mode.Name;

                // Make sure a mode is enabled before adding it.
                if (mode.IsEnabled(_manager))
                {
                    ListItem item = new ListItem(modeName, modeName);

                    DisplayModeDropdown.Items.Add(item);
                }
            }

            DisplayModeDropdown.SelectedValue = selectedValue;

            // If Shared scope is allowed for this user, display the 
            // scope-switching UI and select the appropriate radio button 
            // for the current user scope.
            if (_manager.Personalization.CanEnterSharedScope)
            {
                //Panel2.Visible = True

                if (_manager.Personalization.Scope == PersonalizationScope.User)
                {
                    UserScopeRadioButton.Checked = true;
                }
                else
                {
                    SharedScopeRadioButton.Checked = true;
                }
            }
            else
            {
                Panel2.Visible = false;
            }
        }

        protected void ResetStateLinkButton_Click(object sender, System.EventArgs e)
        {
            // Reset all of a user's personalization data for the page.
            if (_manager.Personalization.IsModifiable)
            {
                _manager.Personalization.ResetPersonalizationState();
            }
        }
    }
}