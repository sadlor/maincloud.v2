﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IconSelector.ascx.cs" Inherits="MainCloudFramework.Dashboard.Admin.Controls.IconSelector" %>

<div class="input-group">
    <asp:TextBox ID="SelectedIconField" runat="server" data-placement="bottomRight" class="form-control icp icp-auto" value="fa-archive" type="text" />
    <span class="input-group-addon"></span>
</div>