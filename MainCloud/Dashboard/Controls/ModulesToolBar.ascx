﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModulesToolBar.ascx.cs" Inherits="MainCloudFramework.Dashboard.Admin.Controls.ModulesToolBar" %>

<asp:Panel ID="toolbar" runat="server">

    <div id="widgetsCatalog" runat="server">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#allWidgets" aria-controls="allWidgets" role="tab" data-toggle="tab">Catalogo Widgets</a></li>
<%-- 
            <li role="presentation" runat="server" id="tabWidgetAdvanced" ClientIDMode="Static"><a href="#widgetAdvanced" aria-controls="widgetAdvanced" role="tab" data-toggle="tab">Catalogo Widgets Advanced</a></li>
            <li role="presentation" runat="server" id="tabWidgetAggregator" ClientIDMode="Static"><a href="#widgetAggregator" aria-controls="widgetAggregator" role="tab" data-toggle="tab">Catalogo Widgets Aggregator</a></li>    
--%>
            <li role="presentation"><a href="#closedWidgets" aria-controls="closedWidgets" role="tab" data-toggle="tab">Widgets Chiusi</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="allWidgets" style="margin-bottom: 20px;">
                <mcf:ListCatalogZone ID="CatalogAllWidgets" runat="server" HeaderText="Inserisci Widget">
                    <ZoneTemplate>
                        <mcf:ListWidgetsCatalog ID="modulesCatalog1" runat="server" />
                    </ZoneTemplate>
                    <WidgetsCatalogHeaderTemplate>
                        <div class="wrap">
                            <ul class="pages"></ul>
                            <div class="frame" id="modulesCatalogScroll">
                                <ul class="clearfix" style="padding-right: 30px">
                    </WidgetsCatalogHeaderTemplate>
                    <WidgetsCatalogItemTemplate>
                        <li class="btn btn-default widget-all-select tooltipped" 
                            style="width: 100px; height: 100px; padding: 5px; white-space:normal"
                            aria-label="<%# DataBinder.Eval(Container, "DataItem.Title") %>" 
                            title="<%# DataBinder.Eval(Container, "DataItem.Description") %>"
                            onclick="$('#<%# CatalogAllWidgets.ModuleSelectorControlID.Replace("$", @"\\$") %>').val('<%# DataBinder.Eval(Container, "DataItem.Id") %>'); $('.widget-all-select').removeClass('active'); $(this).addClass('active');"
                            >
                            <i class="<%# DataBinder.Eval(Container, "DataItem.IconClass") %>" aria-hidden="true"></i>
                            <br />
                            <span style="width: 100px; word-wrap:break-word"><%# DataBinder.Eval(Container, "DataItem.Title") %></span>
                        </li>
                    </WidgetsCatalogItemTemplate>
                    <WidgetsCatalogFooterTemplate>
                                </ul>
                            </div>
			                <div class="scrollbar">
				                <div class="handle">
					                <div class="mousearea"></div>
				                </div>
			                </div>
                        </div>
                    </WidgetsCatalogFooterTemplate>
                </mcf:ListCatalogZone>
            </div>

            <div role="tabpanel" class="tab-pane" id="widgetAdvanced" runat="server" ClientIDMode="Static" style="margin-bottom: 20px;" visible="false">
                <mcf:ListCatalogZone ID="ListCatalogZone1" runat="server" HeaderText="Inserisci Widget Advanced">
                    <ZoneTemplate>
                        <mcf:ListWidgetsCatalog ID="ListWidgetsCatalog1" runat="server" />
                    </ZoneTemplate>
                    <WidgetsCatalogHeaderTemplate>
                        <div class="wrap">
                            <ul class="pages"></ul>
                            <div class="frame" id="widgetAdvancedCatalogScroll">
                                <ul class="clearfix">
                    </WidgetsCatalogHeaderTemplate>
                    <WidgetsCatalogItemTemplate>
                        <%--TODO--%>
                    </WidgetsCatalogItemTemplate>
                    <WidgetsCatalogFooterTemplate>
                                </ul>
                            </div>
			                <div class="scrollbar">
				                <div class="handle">
					                <div class="mousearea"></div>
				                </div>
			                </div>
                        </div>
                    </WidgetsCatalogFooterTemplate>
                </mcf:ListCatalogZone>
            </div>

            <div role="tabpanel" class="tab-pane" id="widgetAggregator" runat="server" ClientIDMode="Static" style="margin-bottom: 20px;" visible="false">
                <mcf:ListCatalogZone ID="ListCatalogZone2" runat="server" HeaderText="Inserisci Widget Aggregator">
                    <ZoneTemplate>
                        <mcf:ListWidgetsCatalog ID="ListWidgetsCatalog2" runat="server" />
                    </ZoneTemplate>
                    <WidgetsCatalogHeaderTemplate>
                        <div class="wrap">
                            <ul class="pages"></ul>
                            <div class="frame" id="widgetAggregatorCatalogScroll">
                                <ul class="clearfix">
                    </WidgetsCatalogHeaderTemplate>
                    <WidgetsCatalogItemTemplate>
                        <%--TODO--%>
                    </WidgetsCatalogItemTemplate>
                    <WidgetsCatalogFooterTemplate>
                                </ul>
                            </div>
			                <div class="scrollbar">
				                <div class="handle">
					                <div class="mousearea"></div>
				                </div>
			                </div>
                        </div>
                    </WidgetsCatalogFooterTemplate>
                </mcf:ListCatalogZone>
            </div>

            <div role="tabpanel" class="tab-pane" id="closedWidgets" style="margin-bottom: 20px;">
                <mcf:ListCatalogZone ID="CatalogClosedWidgets" runat="server" HeaderText="Widget chiusi">
                    <ZoneTemplate>
                        <asp:PageCatalogPart ID="PageCatalogPart2" runat="server" />
                    </ZoneTemplate>
                    <WidgetsCatalogHeaderTemplate>
                        <div class="wrap">
                            <ul class="pages"></ul>
                            <div class="frame" id="modulesClosedCatalogScroll">
                                <ul class="clearfix">
                    </WidgetsCatalogHeaderTemplate>
                    <WidgetsCatalogItemTemplate>
                        <li class="btn btn-default widget-all-select" aria-label="<%# DataBinder.Eval(Container, "DataItem.Title") %>"
                            onclick="$('#<%# CatalogClosedWidgets.ModuleSelectorControlID.Replace("$", @"\\$") %>').val('<%# DataBinder.Eval(Container, "DataItem.Id") %>'); $('.widget-closed-select').removeClass('active'); $(this).addClass('active');"
                            style="width: 100px; height: 100px; float: left;">
                            <i class="<%# DataBinder.Eval(Container, "DataItem.IconClass") %>" aria-hidden="true"></i>
                            <br />
                            <%# DataBinder.Eval(Container, "DataItem.Title") %> 
                        </li>
                    </WidgetsCatalogItemTemplate>
                    <WidgetsCatalogFooterTemplate>
                                </ul>
                            </div>
			                <div class="scrollbar">
				                <div class="handle">
					                <div class="mousearea"></div>
				                </div>
			                </div>                            
                        </div>
                    </WidgetsCatalogFooterTemplate>
                </mcf:ListCatalogZone>
            </div>
        </div>
    </div>

    <script>
        $(function () {

            function initScroll(idMainDiv) {

                var $frame = $(idMainDiv);
                var $slidee = $frame.children('ul').eq(0);
                var $wrap = $frame.parent();

                $frame.sly({
                    horizontal: 1,
                    itemNav: 'basic',
                    smart: 1,
                    activateOn: 'click',
                    mouseDragging: 1,
                    touchDragging: 1,
                    releaseSwing: 1,
                    //startAt: 3,
                    scrollBar: $wrap.find('.scrollbar'),
                    scrollBy: 1,
                    pagesBar: $wrap.find('.pages'),
                    activatePageOn: 'click',
                    speed: 300,
                    elasticBounds: 1,
                    easing: 'easeOutExpo',
                    dragHandle: 1,
                    dynamicHandle: 1,
                    clickBar: 1,

                    // Buttons
                    //forward: $wrap.find('.forward'),
                    //backward: $wrap.find('.backward'),
                    //prev: $wrap.find('.prev'),
                    //next: $wrap.find('.next'),
                    //prevPage: $wrap.find('.prevPage'),
                    //nextPage: $wrap.find('.nextPage')
                });

                //$(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                //    // console.log(e.target) // activated tab
                //    initScroll('#modulesCatalogScroll');
                //    initScroll('#widgetAdvancedCatalogScroll');
                //    initScroll('#widgetAggregatorCatalogScroll');
                //    initScroll('#modulesClosedCatalogScroll');
                //});
            }

            initScroll('#modulesCatalogScroll');
            initScroll('#widgetAdvancedCatalogScroll');
            initScroll('#widgetAggregatorCatalogScroll');
            initScroll('#modulesClosedCatalogScroll');
        });
    </script>
</asp:Panel>