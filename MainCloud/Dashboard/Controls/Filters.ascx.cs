﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Controls
{
    public partial class Filters : System.Web.UI.UserControl
    {
        public string EntityName { get; set; }
        public string ConnectionStringName { get; set; }
        public Type EntityNameType { get; set; }
        public string FilterContainerId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            EntityDataSource.EntitySetName = EntityName;
            EntityDataSource.ConnectionString = "name=" + ConnectionStringName;
            EntityDataSource.DefaultContainerName = ConnectionStringName;
            EntityDataSource.DataBind();
            Filter.FilterContainerID = FilterContainerId;
            if (!IsPostBack)
            {
                AddFieldEditor();
            }
            if (Filter.FieldEditors.Count == 0)
            {
                AddFieldEditor();
            }
        }

        private void AddFieldEditor()
        {
            //Assembly a = Assembly.GetAssembly(typeof(SOLModule.Models.Weld));
            //Type t = a.GetType(EntityName);

            PropertyInfo[] props = EntityNameType.GetProperties();
            foreach (var item in props)
            {
                switch (item.PropertyType.Name)
                {
                    case "Byte":
                    case "Sbyte":
                    case "Int":
                    case "Int32":
                    case "Uint":
                    case "Short":
                    case "Ushort":
                    case "Long":
                    case "Ulong":
                    case "Float":
                    case "Double":
                    case "Decimal":
                    case "Nullable`1":
                        RadFilterNumericFieldEditor nEditor = new RadFilterNumericFieldEditor();
                        Filter.FieldEditors.Add(nEditor);
                        nEditor.FieldName = item.Name;
                        nEditor.DataType = item.PropertyType;
                        break;
                    case "Char":
                    case "String":
                        RadFilterTextFieldEditor txtEditor = new RadFilterTextFieldEditor();
                        Filter.FieldEditors.Add(txtEditor);
                        txtEditor.FieldName = item.Name;
                        txtEditor.DataType = item.PropertyType;
                        break;
                    case "Bool":
                        RadFilterBooleanFieldEditor boolEditor = new RadFilterBooleanFieldEditor();
                        Filter.FieldEditors.Add(boolEditor);
                        boolEditor.FieldName = item.Name;
                        boolEditor.DataType = item.PropertyType;
                        break;
                    case "DateTime":
                        RadFilterDateFieldEditor dateEditor = new RadFilterDateFieldEditor();
                        Filter.FieldEditors.Add(dateEditor);
                        dateEditor.FieldName = item.Name;
                        dateEditor.DataType = item.PropertyType;
                        break;
                }
            }
        }

        public void GetDataSourceFiltered()
        {
        }
    }
}