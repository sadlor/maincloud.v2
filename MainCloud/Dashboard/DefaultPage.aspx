﻿<%@ Page Language="C#" MasterPageFile="~/Dashboard/MainDashboard.Master" AutoEventWireup="true" CodeBehind="DefaultPage.aspx.cs" Inherits="MainCloudFramework.Dashboard.Pages.DefaultPage" %>
<%@ Register src="~/Dashboard/Controls/ModulesToolBar.ascx" tagname="ModulesToolBar" tagprefix="mcf" %>
<%@ Register src="~/Dashboard/Controls/DisplayManager.ascx" tagname="DisplayManager" tagprefix="mcf" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="navBarContent" ContentPlaceHolderID="NavBarPlaceHolder" runat="Server">

    <mcf:WidgetWebPartManager ID="webPartManager" runat="server" Personalization-Enabled="true" OnSelectedWebPartChanging="webPartManager_SelectedWebPartChanging" OnSelectedWebPartChanged="webPartManager_SelectedWebPartChanged">
        <Personalization InitialScope="Shared" />
    </mcf:WidgetWebPartManager>
    <mcf:WebPartManagerExtender ID="WebPartManagerExtender" runat="server" TargetControlID="WebPartManager" />
    <mcf:DisplayManager ID="displayManager" runat="server"/>
</asp:Content>

<asp:Content ID="SubMenuContent" ContentPlaceHolderID="SubMenuContent" runat="Server">

    <mcf:ModulesToolBar ID="ModulesToolBar1" runat="server" />

    <mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Widget configuration">
        <Body>

            <mcf:TabViewEditorWidgetZone ID="EditorZone2" runat="server" HeaderText="Configurazione" CollapseBehavior="Collapse" CollapseStyle="ContractAll" InstructionText="Modifica aspetto e configurazione del modulo selezionato">
                <ApplyVerb Text="Applica" Description="Applica le modifiche" />
                <OKVerb Text="Salva" Description="Salva le modifiche" />
                <CancelVerb Text="Annulla" Description="Annulla le modifiche effettuate" />
                <ZoneTemplate>
                    <asp:AppearanceEditorPart ID="AppearanceEditorPart1" runat="server" />
                    <asp:BehaviorEditorPart ID="BehaviorEditorPart1" runat="server" />
                    <asp:LayoutEditorPart ID="LayoutEditorPart1" runat="server" />
                    <asp:PropertyGridEditorPart ID="PropertyGridEditorPart1" runat="server" />
                </ZoneTemplate>
            </mcf:TabViewEditorWidgetZone>

        </Body>
    </mcf:PopUpDialog>

</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ConnectionsZone ID="connectionsZone1" runat="server">
    </asp:ConnectionsZone>
    <asp:PlaceHolder ID="pagelayout" runat="server">Nessun template associato a questa pagina.</asp:PlaceHolder>
    
    <%--<mcf:Alert runat="server" id="AlertMessagePopup" />--%>

    <mcf:PopUpDialog runat="server" ID="AlertPopup" Title="Avviso">
        <Body>
            <asp:label id="messaggio" runat="server"/>
            <%--<asp:Button Text="Ok" runat="server" ID="btnRedirectOkLink" OnClick="btnRedirectOkLink_Click" />--%>
        </Body>
    </mcf:PopUpDialog>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>