﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace MainCloudFramework.Dashboard.Pages {
    
    
    public partial class DefaultPage {
        
        /// <summary>
        /// webPartManager control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MainCloudFramework.Web.UI.WebControls.WebParts.WidgetWebPartManager webPartManager;
        
        /// <summary>
        /// WebPartManagerExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MainCloudFramework.Web.UI.WebControls.WebParts.WebPartManagerExtender WebPartManagerExtender;
        
        /// <summary>
        /// displayManager control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MainCloudFramework.Dashboard.Admin.Controls.DisplayManager displayManager;
        
        /// <summary>
        /// ModulesToolBar1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MainCloudFramework.Dashboard.Admin.Controls.ModulesToolBar ModulesToolBar1;
        
        /// <summary>
        /// dlgWidgetConfig control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MainCloudFramework.Web.PopUpDialog dlgWidgetConfig;
        
        /// <summary>
        /// EditorZone2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MainCloudFramework.UI.Containers.Editor.TabViewEditorWidgetZone EditorZone2;
        
        /// <summary>
        /// AppearanceEditorPart1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.WebParts.AppearanceEditorPart AppearanceEditorPart1;
        
        /// <summary>
        /// BehaviorEditorPart1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.WebParts.BehaviorEditorPart BehaviorEditorPart1;
        
        /// <summary>
        /// LayoutEditorPart1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.WebParts.LayoutEditorPart LayoutEditorPart1;
        
        /// <summary>
        /// PropertyGridEditorPart1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.WebParts.PropertyGridEditorPart PropertyGridEditorPart1;
        
        /// <summary>
        /// connectionsZone1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.WebParts.ConnectionsZone connectionsZone1;
        
        /// <summary>
        /// pagelayout control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder pagelayout;
        
        /// <summary>
        /// AlertPopup control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MainCloudFramework.Web.PopUpDialog AlertPopup;
        
        /// <summary>
        /// messaggio control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label messaggio;
    }
}
