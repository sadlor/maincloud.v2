﻿function SetDate(startPickId, endPickId, checkId) {
    var startPicker = $find(startPickId);
    var endPicker = $find(endPickId);
    var radioBox = document.getElementById(checkId);
    var items = radioBox.getElementsByTagName('input');
    for (var i = 0; i < items.length; i++) {
        if (items[i].checked) {
            switch (items[i].value) {
                case "LastMonth":
                    var date = new Date();
                    var start = new Date(date.getFullYear(), date.getMonth() - 1, 1);
                    startPicker.set_selectedDate(start);
                    var end = new Date(date.getFullYear(), date.getMonth(), 0);
                    endPicker.set_selectedDate(end);
                    break;
                case "LastDay":
                    var date = new Date();
                    var start = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1);
                    startPicker.set_selectedDate(start);
                    endPicker.set_selectedDate(start);
                    break;
                case "LastYear":
                    var date = new Date();
                    var start = new Date(date.getFullYear() - 1, 0, 1);
                    startPicker.set_selectedDate(start);
                    var end = new Date(date.getFullYear() - 1, 11, 31);
                    endPicker.set_selectedDate(end);
                    break;
                case "CurrentMonth":
                    var date = new Date();
                    var start = new Date(date.getFullYear(), date.getMonth(), 1);
                    startPicker.set_selectedDate(start);
                    var end = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                    endPicker.set_selectedDate(end);
                    break;
                case "CurrentYear":
                    var date = new Date();
                    var start = new Date(date.getFullYear(), 0, 1);
                    startPicker.set_selectedDate(start);
                    var end = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                    endPicker.set_selectedDate(end);
                    break;
            }
        }
    }
}

function ClearRadioButtonListDate(objId)
{
    var radioList = document.getElementById(objId);
    var items = radioList.getElementsByTagName('input');
    for (var i = 0; i < items.length; i++) {
        items[i].checked = false;
    }
}

function SaveSelection(hiddenId, labelId, value, text) {
    document.getElementById(hiddenId).value = value;
    $('#' + labelId).text(text);
}

//function ClosePopup(popupId) {
//    $('#' + popupId).modal('hide');
//}