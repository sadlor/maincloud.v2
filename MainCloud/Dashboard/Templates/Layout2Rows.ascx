﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Layout2Rows.ascx.cs" Inherits="MainCloudFramework.Dashboard.Templates.Layout1" %>
<%--<link href="/Areas/Layout/BaseLayoutStyle.css" rel="stylesheet">--%>

<div class="container-fluid container_layout">
    <div class="row row_layout">
        <div class="col-xs-12 col-md-12">
            <mcf:WidgetWebPartZone ID="zone_1" runat="server">
                <ZoneTemplate>
                </ZoneTemplate>
            </mcf:WidgetWebPartZone>
        </div>
    </div>
    <div class="row row_layout">
        <div class="col-xs-12 col-md-12">
            <mcf:WidgetWebPartZone ID="zone_2" runat="server">
                <ZoneTemplate>
                </ZoneTemplate>
            </mcf:WidgetWebPartZone>
        </div>
    </div>
</div>