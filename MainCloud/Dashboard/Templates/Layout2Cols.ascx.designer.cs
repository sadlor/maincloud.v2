﻿//------------------------------------------------------------------------------
// <generato automaticamente>
//     Codice generato da uno strumento.
//
//     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
//     il codice viene rigenerato. 
// </generato automaticamente>
//------------------------------------------------------------------------------

namespace MainCloudFramework.Dashboard.Templates {
    
    
    public partial class Layout2Cols {
        
        /// <summary>
        /// Controllo zone_1.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MainCloudFramework.Web.UI.WebControls.WebParts.WidgetWebPartZone zone_1;
        
        /// <summary>
        /// Controllo zone_2.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MainCloudFramework.Web.UI.WebControls.WebParts.WidgetWebPartZone zone_2;
    }
}
