﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Layout2Rows5Cols.ascx.cs" Inherits="MainCloud.Dashboard.Templates.Layout2Rows5Cols" %>

<div class="container-fluid container_layout">
    <div class="row row_layout">
        <div class="col-xs-12 col-md-4">
            <mcf:WidgetWebPartZone ID="zone_1" runat="server">
                <ZoneTemplate>
                </ZoneTemplate>
            </mcf:WidgetWebPartZone>
        </div>
        <div class="col-xs-12 col-md-4">
            <mcf:WidgetWebPartZone ID="zone_2" runat="server">
                <ZoneTemplate>
                </ZoneTemplate>
            </mcf:WidgetWebPartZone>
        </div>
        <div class="col-xs-12 col-md-4">
            <mcf:WidgetWebPartZone ID="zone_3" runat="server">
                <ZoneTemplate>
                </ZoneTemplate>
            </mcf:WidgetWebPartZone>
        </div>
    </div>
    <div class="row row_layout">
        <div class="col-xs-12 col-md-6">
            <mcf:WidgetWebPartZone ID="zone_4" runat="server">
                <ZoneTemplate>
                </ZoneTemplate>
            </mcf:WidgetWebPartZone>
        </div>
        <div class="col-xs-12 col-md-6">
            <mcf:WidgetWebPartZone ID="zone_5" runat="server">
                <ZoneTemplate>
                </ZoneTemplate>
            </mcf:WidgetWebPartZone>
        </div>
    </div>
</div>