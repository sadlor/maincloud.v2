﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Layout2Cols.ascx.cs" Inherits="MainCloudFramework.Dashboard.Templates.Layout2Cols" %>

<div class="container-fluid container_layout">
    <div class="row row_layout">
        <div class="col-xs-6 col-md-6">
            <mcf:WidgetWebPartZone ID="zone_1" runat="server">
                <ZoneTemplate>
                </ZoneTemplate>
            </mcf:WidgetWebPartZone>
        </div>
        <div class="col-xs-6 col-md-6">
            <mcf:WidgetWebPartZone ID="zone_2" runat="server">
                <ZoneTemplate>
                </ZoneTemplate>
            </mcf:WidgetWebPartZone>
        </div>
    </div>
</div>