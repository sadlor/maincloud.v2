﻿using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Services;
using MainCloudFramework.UI.Containers;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard
{
    public partial class PageEdit : Page
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    string editControlPath = Request[WidgetWebPart.PARAM_EDIT_CONTROL];
        //    string moduleId = Request[WidgetWebPart.PARAM_MODULE_ID];

        //    string moduleFullPath = WidgetWebPart.getModuleControlPath(editControlPath) + ".ascx";
        //    WidgetControl<object> module = Page.LoadControl(moduleFullPath) as WidgetControl<object>;
        //    module.ID = moduleId;

        //    ctrlToEdit.Controls.Add(module);
        //    DataBind();
        //}

        //public string GetApplicationId()
        //{
        //    string returnUrl = Request["returnurl"];
        //    string applicationName = returnUrl.Split('/')[2];  // /App/Application/
        //    using (ApplicationDbContext db = new ApplicationDbContext())
        //    {
        //        var appId = from application in db.AspNetApplications
        //                    where application.Name == applicationName
        //                    select application.Id;
        //        return appId.SingleOrDefault();
        //    }
        //}

        private string applicationId;

        protected void Page_Load(object sender, EventArgs e)
        {
            WidgetService widgetService = new WidgetService();
            WidgetsRepository widgetsRepository = new WidgetsRepository();
            applicationId = MultiTenantsHelper.ApplicationId;

            string editControlPath = Request[WidgetWebPart.PARAM_EDIT_CONTROL];
            string widgetId = Request[WidgetWebPart.PARAM_WIDGET_ID];
            string moduleFullPath = widgetService.GetModuleControlPath(editControlPath) + ".ascx";
            Widget widget = widgetsRepository.FindByWidgetId(widgetId);

            WidgetControl<object> module = Page.LoadControl(moduleFullPath) as WidgetControl<object>;
            module.WidgetData = widget;
            module.WidgetId = widgetId;

            ctrlToEdit.Controls.Add(module);
            //DataBind();
        }

        public string GetApplicationId()
        {
            return applicationId;
        }
    }
}