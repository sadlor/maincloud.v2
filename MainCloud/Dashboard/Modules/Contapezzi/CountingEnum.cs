﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Contapezzi
{
    public static class CountingEnum
    {
        public enum Causale
        {
            Cambio_Lavoro,
            Cambio_Materiale,
            Manutenzione_Macchina,
            Produzione,
            Pulizia
        }
    }
}