﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContapezziDemo_Edit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Contapezzi.ContapezziDemo.ContapezziDemo_Edit" %>

<telerik:RadGrid runat="server" ID="GridCause" RenderMode="Lightweight" AutoGenerateColumns="false" AllowAutomaticUpdates="false"
    OnNeedDataSource="GridCause_NeedDataSource" OnUpdateCommand="GridCause_UpdateCommand" OnInsertCommand="GridCause_InsertCommand" OnDeleteCommand="GridCause_DeleteCommand"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView DataKeyNames="Id" Caption="Causali" EditMode="PopUp" CommandItemDisplay="Top"
        InsertItemPageIndexAction="ShowItemOnCurrentPage" FilterExpression="">
        <CommandItemSettings AddNewRecordText="Add" ShowRefreshButton="true" />
        <EditFormSettings InsertCaption="Inserisci Causale">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="Description" HeaderText="Description">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <ClientEvents OnPopUpShowing="PopUpShowing" />
    </ClientSettings>
</telerik:RadGrid>

<br />

<telerik:RadGrid runat="server" ID="GridJob" RenderMode="Lightweight" AutoGenerateColumns="false"
    OnNeedDataSource="GridJob_NeedDataSource" OnUpdateCommand="GridJob_UpdateCommand" OnInsertCommand="GridJob_InsertCommand" OnDeleteCommand="GridJob_DeleteCommand"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView DataKeyNames="Id" Caption="Jobs" EditMode="PopUp" CommandItemDisplay="Top" FilterExpression="">
        <CommandItemSettings AddNewRecordText="Add" ShowRefreshButton="true" />
        <EditFormSettings InsertCaption="Inserisci Job">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="Code" HeaderText="Code">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <ClientEvents OnPopUpShowing="PopUpShowing" />
    </ClientSettings>
</telerik:RadGrid>

<br />

<telerik:RadGrid runat="server" ID="GridOperator" RenderMode="Lightweight" AutoGenerateColumns="false"
    OnNeedDataSource="GridOperator_NeedDataSource" OnUpdateCommand="GridOperator_UpdateCommand" OnInsertCommand="GridOperator_InsertCommand" OnDeleteCommand="GridOperator_DeleteCommand"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView DataKeyNames="Id" Caption="Operatori" EditMode="PopUp" CommandItemDisplay="Top" FilterExpression="">
        <CommandItemSettings AddNewRecordText="Add" ShowRefreshButton="true" />
        <EditFormSettings InsertCaption="Inserisci Operatore">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="Name" HeaderText="Name">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <ClientEvents OnPopUpShowing="PopUpShowing" />
    </ClientSettings>
</telerik:RadGrid>

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
    <Body>
        <asp:label id="messaggio" runat="server"/>
    </Body>
</mcf:PopUpDialog>

<br />

<asp:Label Text="Data inizio:" runat="server" AssociatedControlID="dtPickStart" />
<telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickStart" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true"
    DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />

<asp:Label Text="Data fine:" runat="server" AssociatedControlID="dtPickEnd" />
<telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickEnd" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true"
    DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />

<telerik:RadDropDownList runat="server" ID="ddlSelectJob" DataSourceID="edsJob"
    DataTextField="Code" DataValueField="Id" />
<telerik:RadDropDownList runat="server" ID="ddlSelectAsset" DataSourceID="edsAsset"
    DataTextField="Description" DataValueField="Id" />

<ef:EntityDataSource ID="edsJob" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Jobs"
    OrderBy="it.Code" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsAsset" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />

<asp:Button Text="Genera demo" runat="server" ID="btnGenerateDemo" OnClick="btnGenerateDemo_Click" />

<script type="text/javascript">
    function PopUpShowing(sender, eventArgs) {
        var popUp = eventArgs.get_popUp();
        var scrollPosition = $('#<%= Page.Master.FindControl("ScrollPosition").ClientID %>').val();
        popUp.style.top = scrollPosition + "px";
    }
</script>