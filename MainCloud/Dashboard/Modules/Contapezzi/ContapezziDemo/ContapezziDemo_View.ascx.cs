﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.HtmlChart.Enums;
using MES.Services;
using MES.Models;
using MES.Repositories;
using MainCloudFramework.Web.Helpers;
using MoldID.Repositories;

namespace MainCloud.Dashboard.Modules.Contapezzi.ContapezziDemo
{
    public partial class ContapezziDemo_View : DataWidget<ContapezziDemo_View>
    {
        public ContapezziDemo_View() : base(typeof(ContapezziDemo_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMachine.WhereParameters.Clear();
            edsMachine.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);

            if (!IsPostBack)
            {
                if (Context.User.Identity.IsAuthenticated)
                {
                    txtUserName.Text = Context.User.Identity.Name;
                }
                AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
                ddlMachine.DataSource = AS.GetAssetList();
                ddlMachine.DataBind();
                ddlMachine.AllowCustomText = true;

                ProgressiveChart.Visible = false;
                StepChart.Visible = false;
            }
            //CreateDay();

            //CountingService cs = new CountingService();
            //cs.CreateDemo();
        }

        protected void CreateChart(DateTime start, DateTime end)
        {
            if (end.Date >= DateTime.Now.Date)
            {
                if (start.Date > DateTime.Now.Date)
                {
                    return;
                }
                else
                {
                    end = DateTime.Now;
                }
            }
            else
            {
                end = end.AddDays(1);
            }
            CountingDataRepository repos = new CountingDataRepository();
            List<CountingData> dataSource = null;
            if (!string.IsNullOrEmpty(ddlMachine.SelectedValue))
            {
                dataSource = repos.ReadAll(x => x.Start >= start && x.End <= end && x.AssetId == ddlMachine.SelectedValue).OrderBy(x => x.Start).ToList();
            }
            else
            {
                dataSource = repos.ReadAll(x => x.Start >= start && x.End <= end).OrderBy(x => x.Start).ToList();
            }

            if (dataSource.Count > 0)
            {
                GridCounting.DataSource = dataSource;
                GridCounting.DataBind();

                DateTime epoch = new DateTime(1970, 1, 1);
                DateTime dtStart = new DateTime(dataSource.First().Start.Year, dataSource.First().Start.Month, dataSource.First().Start.Day).ToUniversalTime();
                DateTime dtEnd = new DateTime(dataSource.Last().Start.Year, dataSource.Last().Start.Month, dataSource.Last().Start.Day).ToUniversalTime();

                //Set min and max date for a datetime x-axis type
                ProgressiveChart.PlotArea.XAxis.MinValue = (decimal)dtStart.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                ProgressiveChart.PlotArea.XAxis.MaxValue = (decimal)dtEnd.AddDays(1).Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                ScatterLineSeries progressiveSerie = ProgressiveChart.PlotArea.Series[0] as ScatterLineSeries;//new ScatterLineSeries();
                progressiveSerie.MarkersAppearance.Visible = false;
                progressiveSerie.LabelsAppearance.Visible = false;

                foreach (var item in dataSource)
                {
                    progressiveSerie.SeriesItems.Add((decimal)item.Start.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.ProgressiveCounting);                    
                }
                //ProgressiveChart.PlotArea.Series.Add(progressiveSerie);

                int? frequency = null;
                ScatterLineSeries frequencySerie = StepChart.PlotArea.Series[1] as ScatterLineSeries;
                string moldId = dataSource.Where(x => !string.IsNullOrEmpty(x.CountingAnalyzerId)).Select(x => x.CountingAnalyzerId).First();
                if (!string.IsNullOrEmpty(moldId))
                {
                    MoldRepository moldRepos = new MoldRepository();
                    try
                    {
                        frequency = moldRepos.ReadAll(x => x.Id == moldId && x.ApplicationId == MultiTenantsHelper.ApplicationId).First().Frequency;
                    }
                    catch (Exception)
                    { }
                }

                StepChart.PlotArea.XAxis.MinValue = (decimal)dtStart.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                StepChart.PlotArea.XAxis.MaxValue = (decimal)dtEnd.AddDays(1).Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

                //add static line
                if (frequency.HasValue)
                {
                    frequencySerie.Name = "Cadenza oraria prevista = " + frequency;
                    frequencySerie.SeriesItems.Add((decimal)dtStart.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, frequency.Value);
                    frequencySerie.SeriesItems.Add((decimal)dtEnd.AddDays(1).Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, frequency.Value);
                }

                ScatterLineSeries stepSerie = StepChart.PlotArea.Series[0] as ScatterLineSeries;
                stepSerie.MarkersAppearance.Visible = false;
                stepSerie.LabelsAppearance.Visible = false;
                stepSerie.LineAppearance.LineStyle = LineStyle.Smooth;//ExtendedLineStyle.Step;
                foreach (var item in dataSource)
                {
                    if (item == dataSource.First())
                    {
                        stepSerie.SeriesItems.Add((decimal)item.Start.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, 0);
                    }
                    stepSerie.SeriesItems.Add((decimal)item.Start.AddSeconds(1).ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.AverageCounting);
                    stepSerie.SeriesItems.Add((decimal)item.End.Value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.AverageCounting);
                }

                //asse x
                end = end.AddDays(-1);
                if ((end - start).Days < 7)
                {
                    ProgressiveChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Hours;
                    ProgressiveChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "HH";
                    StepChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Hours;
                    StepChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "HH";
                    switch ((end - start).Days)
                    {
                        case 0:
                            ProgressiveChart.PlotArea.XAxis.Step = 1;
                            StepChart.PlotArea.XAxis.Step = 1;
                            break;
                        case 1:
                            ProgressiveChart.PlotArea.XAxis.Step = 2;
                            StepChart.PlotArea.XAxis.Step = 2;
                            break;
                        case 2:
                            ProgressiveChart.PlotArea.XAxis.Step = 3;
                            StepChart.PlotArea.XAxis.Step = 3;
                            break;
                        case 3:
                        case 4:
                            ProgressiveChart.PlotArea.XAxis.Step = 4;
                            StepChart.PlotArea.XAxis.Step = 4;
                            break;
                        case 5:
                        case 6:
                        case 7:
                            ProgressiveChart.PlotArea.XAxis.Step = 5;
                            StepChart.PlotArea.XAxis.Step = 5;
                            break;
                    }
                }
                else
                {
                    ProgressiveChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Days;
                    ProgressiveChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "dd";
                    StepChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Days;
                    StepChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "dd";
                }
            }
            else
            {
                GridCounting.DataSource = dataSource;
                GridCounting.DataBind();
            }
        }

        protected void CreateDay()
        {
            //1 turno 06:00 - 14:00
            //2 turno 14:00 - 22:00

            string macchinaId = "1022c77b-5c53-47ef-b46b-27e9d7d4ead5";
            string jobId = "20161215";

            List<CountingData> dataList = new List<CountingData>();
            Random r = new Random();
            DateTime tempo = DateTime.Today;
            int dayStart = DateTime.Today.Day;
            int lavoro = 0;
            int pausa = 0;
            int progressivo = 0;
            int datoRandom = 0;
            while (tempo.Day == dayStart)
            {
                if ((tempo.Hour >= 6 && tempo.Hour <= 14) || (tempo.Hour >= 14 && tempo.Hour < 22))
                {
                    datoRandom = r.Next(25, 50);
                    lavoro = r.Next(4, 9);
                    pausa = r.Next(1, 3);
                    for (int i = 0; i < lavoro; i++)
                    {
                        if (tempo.Hour < 22)
                        {
                            int tRand = r.Next(1, 15);
                            CountingData cd = new CountingData();
                            cd.AssetId = macchinaId;
                            cd.Cause = new Cause() { Description = CountingEnum.Causale.Produzione.ToString() };
                            cd.Start = tempo;
                            cd.End = tempo.AddMinutes(tRand);
                            cd.ProgressiveCounting = progressivo;
                            cd.PartialCounting = datoRandom / lavoro;
                            dataList.Add(cd);
                            tempo = tempo.AddMinutes(tRand);
                            progressivo += datoRandom / lavoro;
                        }
                        else
                        {
                            break;
                        }
                    }
                    for (int i = 0; i < pausa; i++)
                    {
                        if (tempo.Hour < 22)
                        {
                            int tRand = r.Next(1, 15);
                            CountingData cd = new CountingData();
                            cd.AssetId = macchinaId;
                            cd.Cause = new Cause();
                            cd.Start = tempo;
                            cd.End = tempo.AddMinutes(tRand);
                            cd.ProgressiveCounting = progressivo;
                            cd.PartialCounting = 0;
                            dataList.Add(cd);
                            tempo = tempo.AddMinutes(tRand);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    //produzione = 0
                    CountingData cd = new CountingData();
                    cd.AssetId = macchinaId;
                    cd.Cause = new Cause();
                    cd.Start = tempo;
                    cd.ProgressiveCounting = progressivo;
                    cd.PartialCounting = 0;
                    dataList.Add(cd);
                    tempo = tempo.AddMinutes(15);
                }
            }

            //for (int i = 0 ; i <= 96; i++) //asse x
            //{
            //    if (i % 4 == 0)
            //    {
            //        ProgressiveChart.PlotArea.XAxis.Items.Add((i / 4).ToString());   //24 ore
            //        //StepChart.PlotArea.XAxis.Items.Add((i / 4).ToString());  //24 ore
            //    }
            //    else
            //    {
            //        ProgressiveChart.PlotArea.XAxis.Items.Add("");
            //        //StepChart.PlotArea.XAxis.Items.Add("");
            //    }
            //}

            //LineSeries lineSerie = new LineSeries();
            //lineSerie.MarkersAppearance.Visible = false;
            //lineSerie.LabelsAppearance.Visible = false;
            //foreach (var item in dataList)
            //{
            //    lineSerie.SeriesItems.Add(item.ProgressiveCounting);
            //}
            //ProgressiveChart.PlotArea.Series.Add(lineSerie);

            List<CountingData> DataSource = new List<CountingData>();
            CountingData inizio = null;
            CountingData fine = new CountingData();
            for (int i = 0; i < dataList.Count - 1; i++)
            {
                if (dataList[i].Cause.Description != dataList[i + 1].Cause.Description)  //cambio Evento
                {
                    if (inizio == null)
                    {
                        inizio = new CountingData();
                        inizio = dataList[i + 1];
                    }
                    else
                    {
                        fine = dataList[i + 1];
                        inizio.End = fine.Start;
                        inizio.Duration = (int)(inizio.End.Value - inizio.Start).TotalMinutes;
                        inizio.PartialCounting = fine.ProgressiveCounting - inizio.ProgressiveCounting;
                        inizio.AverageCounting = inizio.PartialCounting / (decimal)TimeSpan.FromMinutes(inizio.Duration).TotalHours;
                        DataSource.Add(inizio);
                        inizio = fine;
                    }
                }
            }
            GridCounting.DataSource = DataSource;
            GridCounting.DataBind();

            DateTime epoch = new DateTime(1970, 1, 1);
            DateTime dt = new DateTime(DataSource.First().Start.Year, DataSource.First().Start.Month, DataSource.First().Start.Day).ToUniversalTime();

            //Set min and max date for a datetime x-axis type
            ProgressiveChart.PlotArea.XAxis.MinValue = (decimal)dt.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
            ProgressiveChart.PlotArea.XAxis.MaxValue = (decimal)dt.AddDays(1).Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
            ScatterLineSeries progressiveSerie = ProgressiveChart.PlotArea.Series[0] as ScatterLineSeries;//new ScatterLineSeries();
            progressiveSerie.MarkersAppearance.Visible = false;
            progressiveSerie.LabelsAppearance.Visible = false;
            foreach (var item in DataSource)
            {
                progressiveSerie.SeriesItems.Add((decimal)item.Start.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.ProgressiveCounting);
            }
            //ProgressiveChart.PlotArea.Series.Add(progressiveSerie);

            StepChart.PlotArea.XAxis.MinValue = (decimal)dt.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
            StepChart.PlotArea.XAxis.MaxValue = (decimal)dt.AddDays(1).Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
            
            //LineSeries stepSerie = new LineSeries();
            ScatterLineSeries stepSerie = StepChart.PlotArea.Series[0] as ScatterLineSeries;
            stepSerie.MarkersAppearance.Visible = false;
            stepSerie.LabelsAppearance.Visible = false;
            stepSerie.LineAppearance.LineStyle = LineStyle.Smooth;//ExtendedLineStyle.Step;
            foreach (var item in DataSource)
            {
                if (item == DataSource.First())
                {
                    stepSerie.SeriesItems.Add((decimal)item.Start.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, 0);
                }
                stepSerie.SeriesItems.Add((decimal)item.Start.AddSeconds(1).ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.AverageCounting);
                stepSerie.SeriesItems.Add((decimal)item.End.Value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.AverageCounting);
            }
            //StepChart.PlotArea.Series.Add(stepSerie);
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (dtPickStart.SelectedDate != null && dtPickEnd.SelectedDate != null)
            {
                ProgressiveChart.PlotArea.Series[0].Items.Clear();
                ProgressiveChart.PlotArea.XAxis.Items.Clear();
                StepChart.PlotArea.Series[0].Items.Clear();
                StepChart.PlotArea.XAxis.Items.Clear();
                ProgressiveChart.Visible = true;
                StepChart.Visible = true;
                CreateChart(dtPickStart.SelectedDate.Value, dtPickEnd.SelectedDate.Value);
            }
        }

        protected void GridCounting_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (dtPickStart.SelectedDate.HasValue && dtPickEnd.SelectedDate.HasValue)
            {
                DateTime start = dtPickStart.SelectedDate.Value;
                DateTime end = dtPickEnd.SelectedDate.Value;

                if (end.Date >= DateTime.Now.Date)
                {
                    if (start.Date > DateTime.Now.Date)
                    {
                        return;
                    }
                    else
                    {
                        end = DateTime.Now;
                    }
                }
                else
                {
                    end = end.AddDays(1);
                }
                CountingDataRepository repos = new CountingDataRepository();
                List<CountingData> dataSource = null;
                if (!string.IsNullOrEmpty(ddlMachine.SelectedValue))
                {
                    dataSource = repos.ReadAll(x => x.Start >= start && x.End <= end && x.AssetId == ddlMachine.SelectedValue).OrderBy(x => x.Start).ToList();
                }
                else
                {
                    dataSource = repos.ReadAll(x => x.Start >= start && x.End <= end).OrderBy(x => x.Start).ToList();
                }

                if (dataSource.Count > 0)
                {
                    GridCounting.DataSource = dataSource;
                }
            }
        }

        protected void GridCounting_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                if (((CountingData)((GridItem)(dataItem)).DataItem).Cause == null)
                {
                    ((Literal)dataItem["ddlCause"].Controls[0]).Text = "";
                }
                else
                {
                    ((Literal)dataItem["ddlCause"].Controls[0]).Text = ((CountingData)((GridItem)(dataItem)).DataItem).Cause.Description;
                }

                if (((CountingData)((GridItem)(dataItem)).DataItem).Job == null)
                {
                    ((Literal)dataItem["ddlJob"].Controls[0]).Text = "";
                }
                else
                {
                    ((Literal)dataItem["ddlJob"].Controls[0]).Text = ((CountingData)((GridItem)(dataItem)).DataItem).Job.Code;
                }

                if (((CountingData)((GridItem)(dataItem)).DataItem).Operator == null)
                {
                    ((Literal)dataItem["ddlOperator"].Controls[0]).Text = "";
                }
                else
                {
                    ((Literal)dataItem["ddlOperator"].Controls[0]).Text = ((CountingData)((GridItem)(dataItem)).DataItem).Operator.UserName;
                }

                if (string.IsNullOrEmpty(((CountingData)((GridItem)(dataItem)).DataItem).AssetId))
                {
                    ((Literal)dataItem["ddlMachine"].Controls[0]).Text = "";
                }
                else
                {
                    AssetManagement.Repositories.AssetRepository AR = new AssetManagement.Repositories.AssetRepository();
                    ((Literal)dataItem["ddlMachine"].Controls[0]).Text = AR.FindByID(((CountingData)((GridItem)(dataItem)).DataItem).AssetId).Description;
                }
            }
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                CountingService CS = new CountingService();
                AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
                GridEditableItem item = e.Item as GridEditableItem;
                
                RadComboBox causeList = item["ddlCause"].Controls[0] as RadComboBox;
                causeList.DataSource = CS.CauseList();
                causeList.DataTextField = "Description";
                causeList.DataValueField = "Id";
                causeList.DataBind();
                if ((((CountingData)((GridItem)(item)).DataItem)).Cause != null)
                {
                    causeList.SelectedValue = (((CountingData)((GridItem)(item)).DataItem)).Cause.Id.ToString();
                }
                else
                {
                    causeList.AllowCustomText = true;
                }

                RadComboBox jobList = item["ddlJob"].Controls[0] as RadComboBox;
                jobList.DataSource = CS.JobList();
                jobList.DataTextField = "Code";
                jobList.DataValueField = "Id";
                jobList.DataBind();
                if ((((CountingData)((GridItem)(item)).DataItem)).Job != null)
                {
                    jobList.SelectedValue = (((CountingData)((GridItem)(item)).DataItem)).Job.Id.ToString();
                }
                else
                {
                    jobList.AllowCustomText = true;
                }

                RadComboBox operatorList = item["ddlOperator"].Controls[0] as RadComboBox;
                operatorList.DataSource = CS.OperatorList();
                operatorList.DataTextField = "Name";
                operatorList.DataValueField = "Id";
                operatorList.DataBind();
                if ((((CountingData)((GridItem)(item)).DataItem)).Operator != null)
                {
                    operatorList.SelectedValue = (((CountingData)((GridItem)(item)).DataItem)).Operator.Id;
                }
                else
                {
                    operatorList.AllowCustomText = true;
                }

                RadComboBox assetList = item["ddlMachine"].Controls[0] as RadComboBox;
                assetList.DataSource = AS.GetAssetList();
                assetList.DataTextField = "Description";
                assetList.DataValueField = "Id";
                assetList.DataBind();
                if (!string.IsNullOrEmpty((((CountingData)((GridItem)(item)).DataItem)).AssetId))
                {
                    assetList.SelectedValue = (((CountingData)((GridItem)(item)).DataItem)).AssetId;
                }
                else
                {
                    assetList.AllowCustomText = true;
                }
            }
        }

        protected void GridCounting_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            CountingDataRepository repos = new CountingDataRepository();
            
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var countingDataId = editableItem.GetDataKeyValue("Id");

            CountingData data = repos.FindByID(countingDataId);
            if (data != null)
            {
                //editableItem.UpdateValues(data);

                data.Start = Convert.ToDateTime(values["Start"]);
                data.End = Convert.ToDateTime(values["End"]);
                data.Duration = Convert.ToInt32(values["Duration"]);
                if (!string.IsNullOrEmpty((editableItem["ddlCause"].Controls[0] as RadComboBox).SelectedValue))
                {
                    data.CauseId = (editableItem["ddlCause"].Controls[0] as RadComboBox).SelectedValue;
                }
                else
                {
                    data.CauseId = null;
                }
                if (!string.IsNullOrEmpty((editableItem["ddlMachine"].Controls[0] as RadComboBox).SelectedValue))
                {
                    data.AssetId = (editableItem["ddlMachine"].Controls[0] as RadComboBox).SelectedValue;
                }
                else
                {
                    data.AssetId = null;
                }
                if (!string.IsNullOrEmpty((editableItem["ddlOperator"].Controls[0] as RadComboBox).SelectedValue))
                {
                    data.OperatorId = (editableItem["ddlOperator"].Controls[0] as RadComboBox).SelectedValue;
                }
                else
                {
                    data.OperatorId = null;
                }
                if (!string.IsNullOrEmpty((editableItem["ddlJob"].Controls[0] as RadComboBox).SelectedValue))
                {
                    data.JobId = (editableItem["ddlJob"].Controls[0] as RadComboBox).SelectedValue;
                }
                else
                {
                    data.JobId = null;
                }
                data.ProgressiveCounting = Convert.ToDecimal(values["ProgressiveCounting"]);
                data.PartialCounting = Convert.ToDecimal(values["PartialCounting"]);
                data.AverageCounting = Convert.ToDecimal(values["AverageCounting"]);
                data.ProductionWaste = Convert.ToDecimal(values["ProductionWaste"]);
                data.ProductionComply = data.PartialCounting - data.ProductionWaste;

                repos.Update(data);
                repos.SaveChanges();

                if (!string.IsNullOrEmpty(data.OperatorId) || !string.IsNullOrEmpty(data.JobId))
                {
                    CountingService CS = new CountingService();
                    CS.ChangeRecordOperatorJob(data);
                }
            }
        }
    }
}