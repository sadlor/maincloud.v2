﻿using MainCloudFramework.UI.Modules;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Contapezzi.ContapezziDemo
{
    public partial class ContapezziDemo_Edit : WidgetControl<object>
    {
        public ContapezziDemo_Edit() : base(typeof(ContapezziDemo_Edit)) {}

        CountingService countingService = new CountingService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridCause.DataSource = countingService.CauseList();
                GridCause.DataBind();

                GridJob.DataSource = countingService.JobList();
                GridJob.DataBind();

                GridOperator.DataSource = countingService.OperatorList();
                GridOperator.DataBind();

                edsJob.WhereParameters.Clear();
                edsJob.WhereParameters.Add("ApplicationId", (Page as PageEdit).GetApplicationId());
                edsAsset.WhereParameters.Clear();
                edsAsset.WhereParameters.Add("ApplicationId", (Page as PageEdit).GetApplicationId());
            }
        }

        #region NeedDataSource
        protected void GridCause_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            GridCause.DataSource = countingService.CauseList();
        }

        protected void GridJob_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            GridJob.DataSource = countingService.JobList();
        }

        protected void GridOperator_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            GridOperator.DataSource = countingService.OperatorList();
        }
        #endregion

        #region UpdateCommand
        protected void GridCause_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            var causeId = editableItem.GetDataKeyValue("Id");

            CauseRepository repos = new CauseRepository();
            Cause data = repos.FindByID(causeId);
            if (data != null)
            {
                editableItem.UpdateValues(data);
                repos.Update(data);
                repos.SaveChanges();
            }
        }

        protected void GridJob_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            var jobId = editableItem.GetDataKeyValue("Id");

            JobRepository repos = new JobRepository();
            Job data = repos.FindByID(jobId);
            if (data != null)
            {
                editableItem.UpdateValues(data);
                repos.Update(data);
                repos.SaveChanges();
            }
        }

        protected void GridOperator_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            var operatorId = editableItem.GetDataKeyValue("Id");

            OperatorRepository repos = new OperatorRepository();
            Operator data = repos.FindByID(operatorId);
            if (data != null)
            {
                editableItem.UpdateValues(data);
                repos.Update(data);
                repos.SaveChanges();
            }
        }
        #endregion

        #region InsertCommand
        protected void GridCause_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            Cause cause = new Cause();
            cause.Description = (string)values["Description"];
            cause.ApplicationId = (Page as PageEdit).GetApplicationId();

            CauseRepository repos = new CauseRepository();
            repos.Insert(cause);
            repos.SaveChanges();
        }

        protected void GridJob_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            Job job = new Job();
            job.Code = (string)values["Code"];
            job.ApplicationId = (Page as PageEdit).GetApplicationId();

            JobRepository repos = new JobRepository();
            repos.Insert(job);
            repos.SaveChanges();
        }
        
        protected void GridOperator_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            Operator op = new Operator();
            op.UserName = (string)values["Name"];
            op.ApplicationId = (Page as PageEdit).GetApplicationId();

            OperatorRepository repos = new OperatorRepository();
            repos.Insert(op);
            repos.SaveChanges();
        }
        #endregion

        #region DeleteCommand
        protected void GridCause_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var causeId = ((GridDataItem)e.Item).GetDataKeyValue("Id");

            CauseRepository repos = new CauseRepository();
            Cause cause = repos.FindByID(causeId);
            if (cause != null)
            {
                try
                {
                    repos.Delete(cause);
                    repos.SaveChanges();
                }
                catch (Exception ex)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }

        protected void GridJob_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var jobId = ((GridDataItem)e.Item).GetDataKeyValue("Id");

            JobRepository repos = new JobRepository();
            Job job = repos.FindByID(jobId);
            if (job != null)
            {
                try
                {
                    repos.Delete(job);
                    repos.SaveChanges();
                }
                catch (Exception ex)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }

        protected void GridOperator_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var opId = ((GridDataItem)e.Item).GetDataKeyValue("Id");

            OperatorRepository repos = new OperatorRepository();
            Operator op = repos.FindByID(opId);
            if (op != null)
            {
                try
                {
                    repos.Delete(op);
                    repos.SaveChanges();
                }
                catch (Exception ex)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }
        #endregion

        protected void btnGenerateDemo_Click(object sender, EventArgs e)
        {
            if (dtPickStart.SelectedDate.HasValue && dtPickEnd.SelectedDate.HasValue && ddlSelectJob.SelectedValue != null && ddlSelectAsset.SelectedValue != null)
            {
                countingService.CreateDemo(dtPickStart.SelectedDate.Value, dtPickEnd.SelectedDate.Value, ddlSelectJob.SelectedValue, ddlSelectAsset.SelectedValue);
                messaggio.Text = "Dati demo caricati";
                dlgWidgetConfig.OpenDialog();
            }
            else
            {
                messaggio.Text = "Impossibile caricare dati demo";
                dlgWidgetConfig.OpenDialog();
            }
        }
    }
}