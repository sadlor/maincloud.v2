﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContapezziDemo_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Contapezzi.ContapezziDemo.ContapezziDemo_View" %>

<link href="/Dashboard/WidgetStyle.css" rel="stylesheet">


<div class="row" style="width:100%;">
    <div class="col-xs-4 col-sm-3 col-md-2 col-lg-2 pull-left">
        <asp:Image ImageUrl="~/Dashboard/Modules/Contapezzi/ContapezziDemo/Images/operator-icon.jpg" runat="server" />
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div class="row">
            <asp:Label Text="Operator" runat="server" AssociatedControlID="txtUserName" Font-Bold="true" Font-Size="Large" />
            <br />
            <asp:TextBox ID="txtUserName" runat="server" ReadOnly="true" BorderStyle="None" />
        </div>
        <br />
        <div class="row">
            <asp:Label Text="Machine" runat="server" AssociatedControlID="ddlMachine" Font-Bold="true" Font-Size="Large" />
            <br />
            <telerik:RadComboBox ID="ddlMachine" runat="server" DataValueField="Id" DataTextField="Description" />
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <%--<telerik:RadDockZone ID="dockZoneSettings" runat="server" Orientation="Vertical" BorderStyle="None">
            <telerik:RadDock RenderMode="Auto" ID="dockSettings" runat="server" Title="Settings"
                EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false" Resizable="true" Width="100%">
                <ContentTemplate>--%>
                    <div class="row" style="width:100%;">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <%--<asp:Label Text="Data inizio:" runat="server" AssociatedControlID="dtPickStart" />--%>
                            <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickStart" runat="server" ShowPopupOnFocus="true" Width="100%" style="min-width:200px;max-width:220px;"
                                DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" DateInput-Label="Data inizio:" />
                        </div>
                    </div>
                    <div class="row" style="width:100%;">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <%--<asp:Label Text="Data fine:  " runat="server" AssociatedControlID="dtPickEnd" />--%>
                            <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickEnd" runat="server" ShowPopupOnFocus="true" Width="100%" style="min-width:200px;max-width:220px;"
                                DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" DateInput-Label="Data fine:&nbsp;&nbsp;" />
                        </div>
                    </div>

                    <br />

                    <div class="row" style="width:100%;">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                            <%--<br />--%>
                            <asp:LinkButton ID="btnConfirm" Text="Confirm <i class='fa fa-check-circle'></i>" runat="server" OnClick="btnConfirm_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;" />
                        </div>
                    </div>
                <%--</ContentTemplate>
            </telerik:RadDock>
        </telerik:RadDockZone>--%>
    </div>
</div>


<telerik:RadHtmlChart runat="server" ID="ProgressiveChart" Skin="Silk">
    <PlotArea>
        <Series>
            <telerik:ScatterLineSeries>
                <TooltipsAppearance>
                    <ClientTemplate>
                        #= kendo.format(\'{0:HH:mm}\', new Date(value.x)) #,  #= value.y #
                    </ClientTemplate>
                </TooltipsAppearance>
            </telerik:ScatterLineSeries>
        </Series>
        <YAxis>
            <TitleAppearance Text="Qtà pezzi" />
            <%--<MajorGridLines Visible="false" />--%>
            <%--<MinorGridLines Visible="false" />--%>
        </YAxis>
        <XAxis Type="Date">
            <%--<MajorGridLines Visible="false" />--%>
            <%--<MinorGridLines Visible="false" />--%>
        </XAxis>
    </PlotArea>
</telerik:RadHtmlChart>

<telerik:RadHtmlChart runat="server" ID="StepChart" Skin="Silk">
    <Legend>
        <Appearance Position="Bottom"></Appearance>
    </Legend>
    <PlotArea>
        <Series>
            <telerik:ScatterLineSeries>
                <TooltipsAppearance>
                    <ClientTemplate>
                        #= kendo.format(\'{0:HH:mm}\', new Date(value.x)) #,  #= value.y #
                    </ClientTemplate>
                </TooltipsAppearance>
            </telerik:ScatterLineSeries>
            <telerik:ScatterLineSeries>
                <TooltipsAppearance>
                    <ClientTemplate>
                        Cadenza oraria prevista = #= value.y #
                    </ClientTemplate>
                </TooltipsAppearance>
                <MarkersAppearance Size="0" />
                <LabelsAppearance Visible="false"></LabelsAppearance>
                <Appearance>
                    <FillStyle BackgroundColor="#2dbbff" />
                </Appearance>
            </telerik:ScatterLineSeries>
        </Series>
        <YAxis>
            <TitleAppearance Text="Qtà pezzi / h">
                <TextStyle Margin="9" />
            </TitleAppearance>
        </YAxis>
        <XAxis Type="Date">
        </XAxis>
    </PlotArea>
</telerik:RadHtmlChart>

<br />

<asp:UpdatePanel runat="server">
    <ContentTemplate>
    
<telerik:RadGrid runat="server" ID="GridCounting" RenderMode="Lightweight" AutoGenerateColumns="false" AllowAutomaticUpdates="false"
     OnNeedDataSource="GridCounting_NeedDataSource" OnItemDataBound="GridCounting_ItemDataBound" OnUpdateCommand="GridCounting_UpdateCommand">
    <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" EditMode="PopUp">
        <EditFormSettings InsertCaption="Modify row">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridDateTimeColumn DataField="Start" HeaderText="Inizio" PickerType="DateTimePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="End" HeaderText="Fine" PickerType="DateTimePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn DataField="Duration" HeaderText="Durata">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridDropDownColumn UniqueName="ddlCause" HeaderText="Causale" DataField="Cause"
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="ddlMachine" HeaderText="Macchina" DataField="AssetId"
                DataSourceID="edsMachine" ListTextField="Description" ListValueField="Id"
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>
            <%--<telerik:GridBoundColumn DataField="Asset.Description" HeaderText="Macchina">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>--%>
            <telerik:GridDropDownColumn UniqueName="ddlOperator" HeaderText="Operatore" DataField="Operator"
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="ddlJob" HeaderText="Job" DataField="Job"
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="ProgressiveCounting" HeaderText="Progressivo">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="PartialCounting" HeaderText="Parziale">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AverageCounting" HeaderText="Media">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ProductionWaste" HeaderText="Scarti">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <ClientEvents OnPopUpShowing="PopUpShowing" />
    </ClientSettings>
</telerik:RadGrid>

    </ContentTemplate>
</asp:UpdatePanel>

<ef:EntityDataSource runat="server" ID="edsMachine" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets" AutoGenerateWhereClause="true">
</ef:EntityDataSource>

<script type="text/javascript">
    function PopUpShowing(sender, eventArgs) {
        var popUp = eventArgs.get_popUp();
        var scrollPosition = $('#<%= Page.Master.FindControl("ScrollPosition").ClientID %>').val();
        popUp.style.top = scrollPosition + "px";
    }
</script>