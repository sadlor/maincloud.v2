﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaintenancePlansStats_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MaintenancePlansStats.MaintenancePlansStats_View" %>

<link href="/Dashboard/Modules/MaintenancePlansStats/MaintenancePlansStats.css" rel="stylesheet" type="text/css" />

<script type="text/javscript">
    $(document).ready(function() {
        bindEvents();   
    });

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function(evt, args) {
        bindEvents();
    });
</script>

<script type="text/javascript">
    var limit = 5;
    function OnClientDropDownOpening(sender, args) {
        if (sender.get_entries().get_count() == limit) {
            args.set_cancel(true);
        }
    }

    function OnClientEntryAdding(sender, args) {
        var entries = sender.get_entries();
        for (var i = 0; i < entries.get_count() ; i++) {
            if (entries.getEntry(i).get_value() == args.get_entry().get_value()) {
                args.set_cancel(true);
            }
        }
    }

    function selectActivity(index) {
        if (index > -1) {
            $("#<%=mpdTable.ClientID%> .rgMasterTable tbody tr").each(function(i) {
                if (i == index) {
                    if (!$(this).hasClass("rgSelectedRow"))
                        $(this).addClass("rgSelectedRow");
                } else {
                    $(this).removeClass("rgSelectedRow");
                }
            });
        }
    }

    function OnFilterLoad() {
        if ($("#<%= submitFlag.ClientID %>").val() == 1) {
            $("#<%= submitFlag.ClientID %>").val(0);

            $("#mpstatsDiv").css("display", "inherit");
            $("#mpdTableContainer").css("display", "inherit");
            $("#mpdeTableContainer").css("display", "none");
        }

        if ($("#<%= selectedPlanId.ClientID %>").val() != "") {
            selectActivity($("#<%= selectedPlanIdIndex.ClientID %>").val());
            $("#<%= operatorFilter.ClientID %> .racTextToken").text($("#<%= operatorFilterVal.ClientID %>").val());
            $("#<%= selectedPlanId.ClientID %>").val("");
            $("#mpstatsDiv").css("display", "inherit");
            $("#mpdTableContainer").css("display", "inherit");

            $(".resultLabel").each(function () {
                $(this).parent().css("background-color", $(this).attr("data-bg"));
            });

            $("#mpdeTableContainer").css("display", "inherit");
        }
    }

    function OnMoldFilterLoad() {
        OnFilterLoad();
        OnConfirmBtnLoad();

        if ($("#mpstatsDiv .headerDiv div:first-child .mpeBkTitolo").text() == "---") {
	        $("#mpstatsDiv .headerDiv div").eq(0).attr("style", "display: none");
	        $("#mpstatsDiv .headerDiv div").eq(1).attr("style", "display: none");
	        $("#mpstatsDiv .headerDiv div").eq(2).attr("style", "width: 10%");
	        $("#mpstatsDiv .headerDiv div").eq(3).attr("style", "width: 90%");
        }
        else
        {
	        $("#mpstatsDiv .headerDiv div").eq(0).attr("style", "width: 6%");
	        $("#mpstatsDiv .headerDiv div").eq(1).attr("style", "width: 25%");
	        $("#mpstatsDiv .headerDiv div").eq(2).attr("style", "width: 10%");
	        $("#mpstatsDiv .headerDiv div").eq(3).attr("style", "width: 20%");
        }
    }

    function printSelectedEntries() {
        var autoCompleteBox = $find("<%= operatorFilter.ClientID %>");
        
        var entriesCount = autoCompleteBox.get_entries().get_count();
        if (entriesCount > 0) {
            console.dir(autoCompleteBox.get_entries().getEntry(0));
        }
    }

    function OnConfirmBtnLoad() {
        $("#btnConfirmSelection").off("click");
        $("#btnConfirmSelection").click(function () {
            printSelectedEntries();
            var ofv = $("#<%= moldFilter.ClientID %> .racTextToken").text(), opfv = $("#<%= operatorFilter.ClientID %> .racTextToken").text();

            if (ofv.length > 0) {
                var mv = $("#<%= moldFilter.ClientID %>_ClientState").attr("value");
                mv = mv.substring(mv.indexOf('"value":"') + 9, mv.indexOf('"}}]}'));

                var dstart = $("#<%= dtPickStart.ClientID%>_dateInput_ClientState").val();
                dstart = dstart.substring(dstart.indexOf('AsString"') + 11, dstart.indexOf('minDateStr') - 3).replace("-00-00-00", "");
                var dend = $("#<%= dtPickEnd.ClientID%>_dateInput_ClientState").val();
                dend = dend.substring(dend.indexOf('AsString"') + 11, dend.indexOf('minDateStr') - 3).replace("-00-00-00", "");

                $("#<%= moldIdFilterVal.ClientID %>").val(mv);
                $("#<%= moldFilterVal.ClientID %>").val(ofv);
                $("#<%= operatorFilterVal.ClientID %>").val(opfv);
                $("#<%= submitFlag.ClientID %>").val(1);
                $("#<%= selectedDateStart.ClientID %>").val(dstart);
                $("#<%= selectedDateEnd.ClientID %>").val(dend);

                $("#mpstatsDiv").css("display", "none");
                $("#mpdTableContainer").css("display", "none");
                $("#mpdeTableContainer").css("display", "none");
                __doPostBack('<%=moldFilter.ClientID %>', "submit");
            } else {
                alert("Compilare tutti i campi.");
            }
        });
    }

    function RowSelected(sender, eventArgs) {
        var currentRow = $(sender)[0]._selection._masterTable.children[$(sender)[0]._selectedIndexes[0]];
        var planId = $(currentRow).find(".descr").attr("data-index");

        selectActivity($(sender)[0]._selectedIndexes[0]);
        $("#<%= selectedPlanIdIndex.ClientID %>").val($(sender)[0]._selectedIndexes[0]);
        $("#<%= selectedPlanId.ClientID %>").val(planId);
        $("#mpdeTableContainer").css("display", "none");
        __doPostBack('<%=moldFilter.ClientID %>', "selectActivity");
    }
</script>

<asp:UpdateProgress runat="server" ID="progressPnl">
    <ProgressTemplate>
        <div class="loading">Loading&#8230;</div>
    </ProgressTemplate>
</asp:UpdateProgress>

<div class="panel panel-info">
    <div class="panel-body">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="moldFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="135px" Width="280px"
                            TextSettings-SelectionMode="Single" MaxResultCount="10" MinFilterLength="0" DataSourceID="edsMolds" DataTextField="Name" DataValueField="Id" InputType="Text" Filter="StartsWith" Label="Mold: " OnClientLoad="OnMoldFilterLoad" OnClientDropDownOpening="OnClientDropDownOpening" OnClientEntryAdding="OnClientEntryAdding">
                        </telerik:RadAutoCompleteBox>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>
                </div>
                <div class="row" style="margin-top: 10px">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="operatorFilter" EmptyMessage="Seleziona" EnableClientFiltering="false" LabelWidth="135px" Width="280px"
                            TextSettings-SelectionMode="Single" InputType="Text" MaxResultCount="10" MinFilterLength="0" DataSourceID="edsOperator" DataTextField="Badge" DataValueField="Badge" Label="Badge Operatore: " OnClientDropDownOpening="OnClientDropDownOpening" OnClientEntryAdding="OnClientEntryAdding">
                            <DropDownItemTemplate>
                                <table>
                                    <tr>
                                        <td style="padding-right: 10px"><%# DataBinder.Eval(Container.DataItem, "Badge")%></td>
                                        <td><%# DataBinder.Eval(Container.DataItem, "Username")%></td>
                                    </tr>
                                </table>
                            </DropDownItemTemplate>
                        </telerik:RadAutoCompleteBox>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="text-align:right;">
            <button ID="btnConfirmSelection" class="btn btn-primary" onclick="return false;">Applica</button>
        </div>
    </div>
</div>

<asp:HiddenField runat="server" ID="moldIdFilterVal"/>
<asp:HiddenField runat="server" ID="moldFilterVal"/>
<asp:HiddenField runat="server" ID="operatorFilterVal"/>
<asp:HiddenField runat="server" ID="submitFlag"/>
<asp:HiddenField runat="server" ID="updateFlag"/>
<asp:HiddenField runat="server" ID="selectedPlanId" Value=""/>
<asp:HiddenField runat="server" ID="selectedPlanIdIndex" Value="-1"/>
<asp:HiddenField runat="server" ID="selectedDateStart" Value=""/>
<asp:HiddenField runat="server" ID="selectedDateEnd" Value=""/>

<ef:EntityDataSource ID="edsMolds" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    OrderBy="it.Description" AutoGenerateWhereClause="false" />
<ef:EntityDataSource ID="edsOperator" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Operators"
    AutoGenerateWhereClause="false" />

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div id="mpstatsDiv" class="panel" style="display: none">
            <div class="panel-body">
                <div class="headerDiv">
                    <div style="width: 6%;">
                        <asp:Label runat="server" Text="Cod." />
                        <asp:Label class="mpeBkTitolo " ID="lblOpCode" Width="100%" runat="server" Text="---" />
                    </div>
                    <div style="width: 25%;">
                        <asp:Label runat="server" Text="Operatore" />
                        <asp:Label class="mpeBkTitolo " ID="lblOpUserName" Width="100%" runat="server" Text="---" />
                    </div>
                    <div style="width: 10%;">
                        <asp:Label runat="server" Text="Cod. Mold" />
                        <asp:Label class="mpeBkTitolo " ID="lblMold" Width="100%" runat="server" Text="---" />
                    </div>
                    <div style="width: 20%;">
                        <asp:Label runat="server" Text="Descr. Piano di Manutenzione" />
                        <asp:Label class="mpeBkTitolo " ID="lblmpDescription" Width="100%" runat="server" Text="---" />
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<div style="max-height: 302px">
    <div id="mpdTableContainer" style="display: none;" class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
        <asp:Label runat="server">Selezionare il piano di manutenzione da analizzare:</asp:Label><br /><br />
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <telerik:RadGrid runat="server" ID="mpdTable" AutoGenerateColumns="false"
                    OnNeedDataSource="mpdTable_NeedDataSource" Width="100%" Height="300px"  >
                    <MasterTableView FilterExpression="" Caption="" DataKeyNames="PlanId" NoMasterRecordsText="Nessun piano disponibile.">
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Operatore" ItemStyle-Width="400px" ItemStyle-Wrap="true" UniqueName="OperatorUserName">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label runat="server" CssClass="control-label descr" Text='<%# Eval("OperatorUserName") %>' data-index='<%# Eval("PlanId") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Data" ItemStyle-Width="140px" ItemStyle-Wrap="true">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" CssClass="control-label descr" Text='<%# Eval("ExecutionDate") %>' data-index='<%# Eval("PlanId") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Num. interventi" ItemStyle-Width="60px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" CssClass="control-label ctype" Text='<%# Eval("ActivitiesCount") %>' data-index='<%# Eval("PlanId") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <ClientEvents OnRowSelected="RowSelected" />
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="true" EnableColumnClientFreeze="true" FrozenColumnsCount="1" UseStaticHeaders="false" />
                    </ClientSettings>
                </telerik:RadGrid>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="mpdeTableContainer" style="display: none;" class="col-xs-8 col-sm-8 col-md-8 col-lg-9">
        <asp:Label runat="server">Lista interventi:</asp:Label><br /><br />
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <telerik:RadGrid runat="server" ID="mpdeTable" AutoGenerateColumns="false"
                    OnNeedDataSource="mpdeTable_NeedDataSource" Width="100%" Height="300px" OnPreRender="mpdeTable_PreRender">
                        <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" NoMasterRecordsText="Nessun rilevazione disponibile.">
                            <Columns>
                                 <telerik:GridBoundColumn DataField="Description" HeaderText="Descrizione" ItemStyle-Width="280px" MaxLength="40" ItemStyle-Wrap="true">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ItemCode" HeaderText="Cod. Particolare" ItemStyle-Width="80px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ToolCode" HeaderText="Cod. Strumento" ItemStyle-Width="80px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DetailNotes" HeaderText="Note" MaxLength="2000" ItemStyle-Wrap="true" ItemStyle-Width="250px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="extraMaintenanceString" HeaderText="Aggiuntivo" ItemStyle-Wrap="true" ItemStyle-Width="70px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Result" HeaderText="Completamento" ItemStyle-Wrap="true" ItemStyle-Width="70px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="" ItemStyle-Wrap="true" ItemStyle-Width="30px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" CssClass="control-label resultLabel" data-bg='<%# Eval("ResultValueColor") %>' Text='<%# Eval("ResultValueString") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="true" EnableColumnClientFreeze="true" FrozenColumnsCount="1" UseStaticHeaders="false" />
                    </ClientSettings>
                </telerik:RadGrid>

                <div runat="server" class="row" style="margin-top: 10px; padding-left: 5px;">
                    <div runat="server" class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label runat="server">Effettuati:</asp:Label><br />
                        <asp:Label ID="lblECount" Font-Bold="true" runat="server"></asp:Label>
                    </div>
                    <div runat="server" class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label runat="server">Non effettuabili:</asp:Label><br />
                        <asp:Label ID="lblNECount" Font-Bold="true" runat="server"></asp:Label>
                    </div>
                    <div runat="server" class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label runat="server">Sospesi:</asp:Label><br />
                        <asp:Label ID="lblSOCount" Font-Bold="true" runat="server"></asp:Label>
                    </div>
                    <div runat="server" class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label runat="server">Saltati:</asp:Label><br />
                        <asp:Label ID="lblSACount" Font-Bold="true" runat="server"></asp:Label>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
