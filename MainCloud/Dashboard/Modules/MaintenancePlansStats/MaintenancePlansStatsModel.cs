﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Seneca.MaintenancePlansStats
{
    public class MaintenancePlansStatsModel
    {
        public string OperatorUserName { get; set; }
        public DateTime ExecutionDate { get; set; }
        public int ActivitiesCount { get; set; }
        public int? PlanId { get; set; }

        public MaintenancePlansStatsModel(string OperatorUserName, DateTime ExecutionDate, int ActivitiesCount, int? PlanId)
        {
            this.OperatorUserName = OperatorUserName;
            this.ExecutionDate = ExecutionDate;
            this.ActivitiesCount = ActivitiesCount;
            this.PlanId = PlanId;
        }
    }
}
