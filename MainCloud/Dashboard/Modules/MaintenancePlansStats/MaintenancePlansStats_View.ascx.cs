﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloud.Dashboard.Modules.Seneca.MaintenancePlansStats;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.MaintenancePlansStats
{
    public partial class MaintenancePlansStats_View : DataWidget<MaintenancePlansStats_View>
    {
        public MaintenancePlansStats_View() : base(typeof(MaintenancePlansStats_View)) { }

        private object moldId, moldCode, operatorId, operatorCode, operatorBadge, operatorUserName, mpoId, mpId, mpDescription, planId;

        private List<ControlPlanDetail> mpd;

        protected void Page_Load(object sender, EventArgs e)
        {
            var r = Request["__EVENTARGUMENT"];
            if (IsPostBack)
            {
                if (r == "submit")
                {
                    getActivityList();
                }
                else if (r == "selectActivity")
                {
                    getActivityList();
                    planId = Request.Form[selectedPlanId.UniqueID].Trim();
                    mpdeTable.Rebind();
                }
            }
        }

        protected void getActivityList()
        {
            moldId = Request.Form[moldIdFilterVal.UniqueID].Trim();
            moldCode = Request.Form[moldFilterVal.UniqueID].Trim();
            operatorBadge = Request.Form[operatorFilterVal.UniqueID].Trim();

            loadData();

            lblOpCode.Text = (string)operatorCode;
            lblOpUserName.Text = (string)operatorUserName;
            lblMold.Text = (string)moldCode;
            lblmpDescription.Text = (string)mpDescription;

            if (lblOpCode.Text == "") lblOpCode.Text = "---";
            if (lblOpUserName.Text == "") lblOpUserName.Text = "---";
            if (lblMold.Text == "") lblMold.Text = "---";
            if (lblmpDescription.Text == "") lblmpDescription.Text = "---";

            mpdTable.Rebind();
        }

        protected void loadData()
        {
            // Get Operator
            MesDbContext DBContext = new MesDbContext();
            if (operatorBadge.ToString().Trim().Length > 0)
            {
                Operator op = DBContext.Operators.Where(x => x.Badge == (string)operatorBadge).FirstOrDefault();
                if (op != null)
                {
                    operatorId = op.Id;
                    operatorCode = op.Code;
                    operatorUserName = op.UserName;
                }
            }

            // Get Maintenance Plan
            MaintenanceRepository mRep = new MaintenanceRepository();
            Maintenance m = mRep.FindByMoldId((string)moldId);

            if (m != null && m.MaintenancePlanId != null)
            {
                MaintenancePlanOperatorRepository mpoRep = new MaintenancePlanOperatorRepository();
                MaintenancePlanOperator mpo = mpoRep.GetByOperatorMaintenanceId((string)operatorId, m.Id);
                if (mpo != null) mpoId = mpo.Id;

                MaintenancePlanRepository mpRep = new MaintenancePlanRepository();
                MaintenancePlan mp = mpRep.FindByID(m.MaintenancePlanId);

                if (mp != null)
                {
                    // Get Maintenance Plan Id
                    mpId = mp.Id;

                    // Get Maintenance Plan Description
                    mpDescription = mp.Description;
                }
            }
        }

        protected void mpdTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            string dstart = Request.Form[selectedDateStart.UniqueID],
                   dend = Request.Form[selectedDateEnd.UniqueID];

            DateTime startDate = DateTime.MinValue,
                     endDate = DateTime.MaxValue;

            if (dstart.Length == 10 && dend.Length == 10)
            {
                // "2018-11-07"
                startDate = new DateTime(Convert.ToInt32(dstart.Substring(0, 4)), Convert.ToInt32(dstart.Substring(5, 2)), Convert.ToInt32(dstart.Substring(8, 2)));
                endDate = new DateTime(Convert.ToInt32(dend.Substring(0, 4)), Convert.ToInt32(dend.Substring(5, 2)), Convert.ToInt32(dend.Substring(8, 2)));
            }

            MaintenancePlanDetailExecutedRepository mpDERep = new MaintenancePlanDetailExecutedRepository();
            IQueryable<MaintenancePlanDetailExecuted> mpdQ = (operatorId != null) ? mpDERep.GetAllByDateAndMoldAndOperatorQuery(startDate, endDate, (string)operatorId, (string)moldId) : mpDERep.GetAllByDateAndMoldQuery(startDate, endDate, (string)moldId);

            var a = mpdQ.Select(x => new { x.PlanId, x.ExecutionDate }).GroupBy(x => x.PlanId).ToList();
            List<MaintenancePlansStatsModel> mpsList = mpdQ.GroupBy(x => x.PlanId).ToList().Select(x => new MaintenancePlansStatsModel((operatorUserName != null) ? (string)operatorUserName : "", new DateTime(), x.Count(), x.Key)).ToList();

            foreach (MaintenancePlansStatsModel mps in mpsList)
            {
                if (mps.OperatorUserName == "")
                    mps.OperatorUserName = mpDERep.GetOperatorUserNameByPlanId(mps.PlanId);
                mps.ExecutionDate = mpDERep.GetExecutionDateByPlanId(mps.PlanId);
            }

            mpdTable.DataSource = mpsList;

            mpdTable.MasterTableView.GetColumn("OperatorUserName").Visible = (operatorId == null);
        }

        protected void mpdeTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            MaintenancePlanDetailExecutedRepository mpDERep = new MaintenancePlanDetailExecutedRepository();
            List<MaintenancePlanDetailExecuted> mpDEList = mpDERep.GetAllByPlanId(Convert.ToInt32(planId));
            mpdeTable.DataSource = mpDEList;
        }

        protected void mpdeTable_PreRender(object sender, EventArgs e)
        {
            List<MaintenancePlanDetailExecuted> mpdeData = (List<MaintenancePlanDetailExecuted>)mpdeTable.DataSource;

            if (mpdeData != null && mpdeData.Count > 0)
            {

                decimal eCount = 0, neCount = 0, soCount = 0, saCount = 0;
                foreach (MaintenancePlanDetailExecuted mpde in mpdeData)
                {
                    switch (mpde.Result.ToLower())
                    {
                        case "effettuato":
                            eCount++;
                            break;
                        case "non effettuabile":
                            neCount++;
                            break;
                        case "sospeso":
                            soCount++;
                            break;
                        case "saltato":
                            saCount++;
                            break;
                    }
                }

                lblECount.Text = eCount.ToString();
                lblNECount.Text = neCount.ToString();
                lblSOCount.Text = soCount.ToString();
                lblSACount.Text = saCount.ToString();
            }
        }
    }
}