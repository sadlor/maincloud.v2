﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssetList_View.ascx.cs" Inherits="MainCloudFramework.Dashboard.Modules.AssetList.AssetList_View" %>

<link href="/Dashboard/Modules/AssetList/Style.css" rel="stylesheet">

<asp:UpdatePanel ID="pnlTree" runat="server">
    <ContentTemplate>
        <%--<div class="row">
            <div class="col-xs-6">--%>
                <telerik:RadTreeView runat="Server" ID="AssetTreeView" EnableViewState="true" EnableDragAndDrop="true" EnableDragAndDropBetweenNodes="false"
                    OnNodeClick="AssetTreeView_NodeClick" OnClientMouseOver="TreeMouseOver">
                    </telerik:RadTreeView>
            <%--</div>
            <div class="col-xs-6">
                <telerik:RadTreeView runat="Server" ID="AssetImageTreeView" EnableViewState="true" EnableDragAndDrop="true" EnableDragAndDropBetweenNodes="false"
                    OnNodeClick="AssetTreeView_NodeClick" ShowLineImages="false" OnClientMouseOver="TreeMouseOver" />
            </div>
        </div>--%>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="AssetTreeView" />
    </Triggers>
</asp:UpdatePanel>

<script type="text/javascript">

    var lastNodeSelected_<%=AssetTreeView.ClientID %> = null;

    function TreeMouseOver(sender, eventArgs) {
        var node = eventArgs.get_node();
        if (lastNodeSelected_<%=AssetTreeView.ClientID %> != node.get_value()) {
            lastNodeSelected_<%=AssetTreeView.ClientID %> = node.get_value();
            __doPostBack('<%=AssetTreeView.ClientID %>', node.get_value()); //"wp429903510"
        }
    }

    function ExpandCollapse(button)
    {
        if (button.Text == "Expand all")
        {
            treeExpandAllNodes();
            button.Text = "Collapse all";
            button.switchClass("fa-chevron-down", "fa-chevron-up", 1000);
        }
        else
        {
            treeCollapseAllNodes();
        }
    }

    function treeExpandAllNodes() {
        var treeView = $find("<%= AssetTreeView.ClientID %>");
        var nodes = treeView.get_allNodes();
        for (var i = 0; i < nodes.length; i++) {

            if (nodes[i].get_nodes() != null) {
                nodes[i].expand();
            }
        }
    }

    function treeCollapseAllNodes() {
        var treeView = $find("<%= AssetTreeView.ClientID %>");
        var nodes = treeView.get_allNodes();
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].get_nodes() != null) {
                nodes[i].collapse();
            }
        }
    }

</script>