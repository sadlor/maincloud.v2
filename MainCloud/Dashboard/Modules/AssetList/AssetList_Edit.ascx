﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssetList_Edit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.AssetList.AssetList_Edit" %>
<%@ Register src="~/Controls/ImageManager/ImageManager.ascx" tagname="ImageManager" tagprefix="mcf" %>

<%--<asp:ScriptManagerProxy runat="server" />--%>

<%--<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>--%>


<%--<asp:UpdatePanel runat="server">
    <ContentTemplate>--%>
        <telerik:RadGrid RenderMode="Lightweight" runat="server" ID="gridPlant" AutoGenerateColumns="false" AllowPaging="true" DataSourceID="edsPlant"
            AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false" 
            OnInsertCommand="gridPlant_InsertCommand" OnUpdateCommand="gridPlant_UpdateCommand" OnDeleteCommand="gridPlant_CancelCommand"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <MasterTableView DataKeyNames="Id" Caption="Plants" FilterExpression=""
                CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
                <EditFormSettings InsertCaption="Inserisci Sito">
                    <PopUpSettings Modal="true" />
                </EditFormSettings>
                <Columns>
                    <telerik:GridEditCommandColumn />
                    <telerik:GridBoundColumn DataField="Code" HeaderText="Code">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Description">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Address" HeaderText="Address">
                        <%--<ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>--%>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="City" HeaderText="City">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Country" HeaderText="Country">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="PathImg" HeaderText="Image" UniqueName="Image">
                        <ItemTemplate>
                            <asp:Image ImageUrl='<%# string.Format("~/{0}", Eval("PathImg"))%>' runat="server" Width="50%" />
                            <%--<asp:Image ImageUrl=<%# string.Format("~/Dashboard/Modules/AssetDetails/AssetImages/{0}", Eval("PathImg"))%> runat="server" Width="50%" />--%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <mcf:ImageManager runat="server" ID="ImageManager" Path='<%# string.Format("/Uploads/{0}/Widgets/Shared", MainCloudFramework.Web.Helpers.MultiTenantsHelper.ApplicationId)%>' />
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridDropDownColumn UniqueName="ddlAnalyzer" ListTextField="Description"
                        ListValueField="Id" DataSourceID="edsAnalyzer" HeaderText="Analyzer" DataField="AnalyzerId" 
                        DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                    </telerik:GridDropDownColumn>
                    <telerik:GridBoundColumn DataField="POD" HeaderText="POD"></telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="Tensione" HeaderText="Tensione" DataField="Voltage">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Voltage")%>' />
                        </ItemTemplate>
                        <EditItemTemplate> 
                            <telerik:RadComboBox ID="VoltageItems" runat="server" AppendDataBoundItems="true">  
                                <Items> 
                                    <telerik:RadComboBoxItem Text="BT" Value="BT"/>
                                    <telerik:RadComboBoxItem Text="MT" Value="MT"/>
                                    <telerik:RadComboBoxItem Text="AT" Value="AT"/>
                                </Items>
                            </telerik:RadComboBox>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="EnablePower" HeaderText="Potenza disponibile"></telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Coordinate" UniqueName="Coords">
                        <EditItemTemplate>
                            <a href="http://it.mygeoposition.com/" target="_blank">GeoPosition</a>
                            <br />
                            <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtLatitude" Label="Latitude"
                                DisplayText='<%# Eval("Latitude")%>' DbValue='<%# Eval("Latitude")%>' Width="100%">
                                <NumberFormat DecimalSeparator="." DecimalDigits="15" GroupSeparator=" " />
                            </telerik:RadNumericTextBox>
                            <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtLongitude" Label="Longitude"
                                DisplayText='<%# Eval("Longitude")%>' DbValue='<%# Eval("Longitude")%>' Width="100%">
                                    <NumberFormat DecimalSeparator="." DecimalDigits="15" GroupSeparator=" " />
                            </telerik:RadNumericTextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:TextBox Text='<%# Eval("Latitude") + "; " + Eval("Longitude")%>' runat="server" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="PartProgramPath" HeaderText="Part program">
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnPopUpShowing="PopUpShowing" />
            </ClientSettings>
        </telerik:RadGrid>
      <%-- </ContentTemplate>
</asp:UpdatePanel> --%>
        
<br/><br/>

<%-- <asp:UpdatePanel runat="server">
    <ContentTemplate> --%>
        <telerik:RadGrid RenderMode="Lightweight" runat="server" ID="gridDepartment" AutoGenerateColumns="false" AllowPaging="true" DataSourceID="edsDepartment"
            AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false"
            OnInsertCommand="gridDepartment_InsertCommand" OnDeleteCommand="gridDepartment_CancelCommand" OnUpdateCommand="gridDepartment_UpdateCommand" 
            LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <MasterTableView DataKeyNames="Id" Caption="Departments" FilterExpression=""
                CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
                <EditFormSettings InsertCaption="Inserisci Reparto"><PopUpSettings Modal="true" />
                </EditFormSettings>
                <Columns>
                    <telerik:GridEditCommandColumn />
                    <telerik:GridBoundColumn DataField="Code" HeaderText="Code">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Description">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridBoundColumn>
                    <telerik:GridDropDownColumn UniqueName="ddlPlant" ListTextField="Description"
                        ListValueField="Id" DataSourceID="edsPlant" HeaderText="Plant" DataField="PlantId" 
                        DropDownControlType="DropDownList" AllowSorting="true">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required" ControlToValidate="ddlPlant"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridDropDownColumn>
                    <telerik:GridTemplateColumn DataField="PathImg" HeaderText="Image" UniqueName="Image">
                        <ItemTemplate>
                            <asp:Image ImageUrl='<%# string.Format("~/{0}", Eval("PathImg"))%>' runat="server" Width="40%" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <mcf:ImageManager runat="server" ID="ImageManager" Path='<%# string.Format("/Uploads/{0}/Widgets/Shared", MainCloudFramework.Web.Helpers.MultiTenantsHelper.ApplicationId)%>' />
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridDropDownColumn UniqueName="ddlAnalyzer" ListTextField="Description"
                        ListValueField="Id" DataSourceID="edsAnalyzer" HeaderText="Analyzer" DataField="AnalyzerId" 
                        DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                    </telerik:GridDropDownColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnPopUpShowing="PopUpShowing" />
            </ClientSettings>
        </telerik:RadGrid>
    <%-- </ContentTemplate>
</asp:UpdatePanel> --%>
        
<br/><br/>

<%-- <asp:UpdatePanel runat="server">
    <ContentTemplate> --%>
        <telerik:RadGrid RenderMode="Lightweight" runat="server" ID="gridAsset" AutoGenerateColumns="false" AllowPaging="true" DataSourceID="edsAsset"
            AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false" AllowFilteringByColumn="true" AllowSorting="true"
            OnInsertCommand="gridAsset_InsertCommand" OnDeleteCommand="gridAsset_CancelCommand" OnUpdateCommand="gridAsset_UpdateCommand"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
            <MasterTableView DataKeyNames="Id" Caption="Assets" PageSize="20" FilterExpression=""
                CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
                <EditFormSettings InsertCaption="Inserisci Asset"><PopUpSettings Modal="true" />
                </EditFormSettings>
                <Columns>
                    <telerik:GridEditCommandColumn />
                    <telerik:GridBoundColumn DataField="Code" HeaderText="Code" AllowFiltering="true" AllowSorting="true"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Description" AllowFiltering="true" AllowSorting="true"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IdNumber" HeaderText="SerialCode" AllowSorting="false" AllowFiltering="false" />
                    <telerik:GridBoundColumn DataField="Brand" HeaderText="Brand" AllowSorting="false" AllowFiltering="false" />
                    <telerik:GridBoundColumn DataField="Model" HeaderText="Model" AllowSorting="false" AllowFiltering="false" />
                    <telerik:GridDateTimeColumn DataField="DtInstallation" HeaderText="Installation Date" UniqueName="DtInstallation" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy}"
                        AllowSorting="false" AllowFiltering="false" DataType="System.DateTime">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn DataField="DtEndGuarantee" HeaderText="End Guarantee Date" UniqueName="DtEndGuarantee" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy}"
                        AllowSorting="false" AllowFiltering="false">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true">
                        </ColumnValidationSettings>
                    </telerik:GridDateTimeColumn>
                    <telerik:GridTemplateColumn DataField="PathImg" HeaderText="Image" UniqueName="Image" AllowSorting="false" AllowFiltering="false">
                        <ItemTemplate>
                            <asp:Image ImageUrl='<%# string.Format("~/{0}", Eval("PathImg"))%>' runat="server" Width="80%" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <mcf:ImageManager runat="server" ID="ImageManager" Path='<%# string.Format("/Uploads/{0}/Widgets/Shared", MainCloudFramework.Web.Helpers.MultiTenantsHelper.ApplicationId)%>' />
                            <%-- <telerik:RadAsyncUpload RenderMode="Lightweight" Visible="false" runat="server" ID="UploadImg"
                                AllowedFileExtensions="jpg,jpeg,png,gif" MaxFileSize="1048576" TargetFolder="~/Dashboard/Modules/AssetDetails/AssetImages/">
                            </telerik:RadAsyncUpload> --%>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridDropDownColumn UniqueName="ddlDepartment" ListTextField="Description"
                        ListValueField="Id" DataSourceID="edsDepartment" HeaderText="Department" DataField="DepartmentId" 
                        DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                        AllowSorting="true" AllowFiltering="false">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="false">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required" ControlToValidate="ddlDepartment"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridDropDownColumn>
                    <telerik:GridDropDownColumn UniqueName="ddlZone" ListTextField="Description"
                        ListValueField="Id" DataSourceID="edsZone" HeaderText="Zone" DataField="ZoneId" 
                        DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                        AllowSorting="true" AllowFiltering="false">
                        <%--<ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required" ControlToValidate="ddlZone"></RequiredFieldValidator>
                        </ColumnValidationSettings>--%>
                    </telerik:GridDropDownColumn>
                    <telerik:GridNumericColumn DataField="StandardRate" HeaderText="Produzione oraria" AllowFiltering="false" DataType="System.Decimal"
                        MinValue="0" DecimalDigits="2">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridNumericColumn>
                    <telerik:GridNumericColumn DataField="NumShotTimeout" HeaderText="Numero cicli attesa" AllowFiltering="false" DataType="System.Decimal"
                        MinValue="0" DecimalDigits="0" DefaultInsertValue="2">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridNumericColumn>
                    <telerik:GridNumericColumn DataField="Timeout" HeaderText="Timeout[s]" AllowFiltering="false" DataType="System.Decimal"
                        MinValue="0" DecimalDigits="0" DefaultInsertValue="120">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridNumericColumn>
                    <%--<telerik:GridBoundColumn DataField="NumShotTimeout" HeaderText="Numero cicli attesa" AllowFiltering="false" DataType="System.Int32">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Timeout" HeaderText="Timeout[s]" AllowFiltering="false" DataType="System.Int32">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>--%>
                    <telerik:GridDropDownColumn UniqueName="ddlAnalyzer" ListTextField="Description"
                        ListValueField="Id" DataSourceID="edsAnalyzer" HeaderText="AnalyzerID" DataField="AnalyzerId" 
                        DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                        AllowSorting="true" AllowFiltering="false">
                    </telerik:GridDropDownColumn>
                    <telerik:GridDropDownColumn UniqueName="ddlAssetGroup" ListTextField="Description"
                        ListValueField="Id" DataSourceID="edsGroupAsset" HeaderText="Asset Group" DataField="AssetGroupId" 
                        DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                        AllowSorting="true" AllowFiltering="false">
                    </telerik:GridDropDownColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
                </Columns>
            </MasterTableView>
            <GroupingSettings CaseSensitive="false" />
            <ClientSettings>
                <ClientEvents OnPopUpShowing="PopUpShowing" />
            </ClientSettings>
            <PagerStyle Mode="NextPrevAndNumeric" />
        </telerik:RadGrid>
  <%--  </ContentTemplate>
</asp:UpdatePanel> --%>
       
<br/><br/>
 
<telerik:RadGrid RenderMode="Lightweight" runat="server" ID="gridZone" AutoGenerateColumns="false" AllowPaging="true" DataSourceID="edsZone"
    AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false"
    OnItemCommand="gridZone_ItemCommand" OnDeleteCommand="gridZone_DeleteCommand"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <MasterTableView DataKeyNames="Id" Caption="Zone" FilterExpression=""
        CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
        <EditFormSettings InsertCaption="Inserisci Zona"><PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="Code" HeaderText="Code" ColumnValidationSettings-EnableRequiredFieldValidation="true" ReadOnly="true" />
            <telerik:GridBoundColumn DataField="Description" HeaderText="Description" ColumnValidationSettings-EnableRequiredFieldValidation="true" />
            <telerik:GridDropDownColumn UniqueName="ddlDepartment" ListTextField="Description"
                ListValueField="Id" DataSourceID="edsDepartment" HeaderText="Department" DataField="DepartmentId" 
                DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                AllowSorting="true" AllowFiltering="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="false">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required" ControlToValidate="ddlDepartment"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridDropDownColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <ClientEvents OnPopUpShowing="PopUpShowing" />
    </ClientSettings>
</telerik:RadGrid>

<br/><br/>

<%-- <asp:UpdatePanel runat="server">
    <ContentTemplate> --%>
        <telerik:RadGrid RenderMode="Lightweight" runat="server" ID="gridGroupAsset" AutoGenerateColumns="false" AllowPaging="true" DataSourceID="edsGroupAsset"
            AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false"
            OnInsertCommand="gridGroupAsset_InsertCommand" OnDeleteCommand="gridGroupAsset_CancelCommand" OnUpdateCommand="gridGroupAsset_UpdateCommand"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <MasterTableView DataKeyNames="Id" Caption="Asset Groups" FilterExpression=""
                CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
                <EditFormSettings InsertCaption="Inserisci Gruppo Asset"><PopUpSettings Modal="true" />
                </EditFormSettings>
                <Columns>
                    <telerik:GridEditCommandColumn />
                    <telerik:GridBoundColumn DataField="Code" HeaderText="Code" ColumnValidationSettings-EnableRequiredFieldValidation="true" />
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Description" ColumnValidationSettings-EnableRequiredFieldValidation="true" />
                    <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnPopUpShowing="PopUpShowing" />
            </ClientSettings>
        </telerik:RadGrid>

        <br/><br/>

        <telerik:RadGrid RenderMode="Lightweight" runat="server" ID="gridGateway" AutoGenerateColumns="false" DataSourceID="edsGateway"
            AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false"
            OnInsertCommand="GridGateway_InsertCommand" OnUpdateCommand="GridGateway_UpdateCommand" OnDeleteCommand="GridGateway_DeleteCommand"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <MasterTableView DataKeyNames="Id" Caption="Gateways" FilterExpression=""
                CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
                <EditFormSettings InsertCaption="Inserisci Analyzer">
                    <PopUpSettings Modal="true" />
                </EditFormSettings>
                <Columns>
                    <telerik:GridEditCommandColumn />
                    <telerik:GridBoundColumn DataField="SerialCode" HeaderText="SerialCode" ColumnValidationSettings-EnableRequiredFieldValidation="true" />
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Description" ColumnValidationSettings-EnableRequiredFieldValidation="true" />
                    <telerik:GridBoundColumn DataField="NetworkAddress" HeaderText="Network Address" />
                    <telerik:GridBoundColumn DataField="DNSName" HeaderText="DNS Name" />
                    <telerik:GridTemplateColumn DataField="PathImg" HeaderText="Image" UniqueName="Image">
                        <ItemTemplate>
                            <asp:Image ImageUrl='<%# string.Format("~/{0}", Eval("PathImg"))%>' runat="server" Width="10%" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <mcf:ImageManager runat="server" ID="ImageManager" Path='<%# string.Format("/Uploads/{0}/Widgets/Shared", MainCloudFramework.Web.Helpers.MultiTenantsHelper.ApplicationId)%>' />
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>

        <br/><br/>

        <telerik:RadGrid RenderMode="Lightweight" runat="server" ID="gridAnalyzer" AutoGenerateColumns="false" AllowPaging="true" DataSourceID="edsAnalyzer"
            AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false" 
            OnInsertCommand="gridAnalyzer_InsertCommand" OnDeleteCommand="gridAnalyzer_CancelCommand" OnUpdateCommand="gridAnalyzer_UpdateCommand"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
            <MasterTableView DataKeyNames="Id" Caption="Analyzers" PageSize="20" FilterExpression=""
                CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
                <EditFormSettings InsertCaption="Inserisci Analyzer">
                    <PopUpSettings Modal="true" />
                </EditFormSettings>
                <Columns>
                    <telerik:GridEditCommandColumn />
                    <telerik:GridBoundColumn DataField="SerialCode" HeaderText="SerialCode" ColumnValidationSettings-EnableRequiredFieldValidation="true" />
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Description" ColumnValidationSettings-EnableRequiredFieldValidation="true" />
                    <telerik:GridBoundColumn DataField="NetworkAddress" HeaderText="Network Address" />
                     <telerik:GridDropDownColumn UniqueName="ddlGateway" ListTextField="Description"
                        ListValueField="Id" DataSourceID="edsGateway" HeaderText="GatewayID" DataField="GatewayId" 
                        DropDownControlType="RadComboBox" AllowSorting="true" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                    </telerik:GridDropDownColumn>
                    <telerik:GridTemplateColumn DataField="PathImg" HeaderText="Image" UniqueName="Image">
                        <ItemTemplate>
                            <asp:Image ImageUrl='<%# string.Format("~/{0}", Eval("PathImg"))%>' runat="server" Width="10%" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <mcf:ImageManager runat="server" ID="ImageManager" Path='<%# string.Format("/Uploads/{0}/Widgets/Shared", MainCloudFramework.Web.Helpers.MultiTenantsHelper.ApplicationId)%>' />
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnPopUpShowing="PopUpShowing" />
            </ClientSettings>
            <PagerStyle Mode="NextPrevAndNumeric" />
        </telerik:RadGrid>
        <asp:Label ID="lblAnalyzerCounter" runat="server" CssClass="label label-info"></asp:Label>

        <mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
            <Body>
                <asp:label id="messaggio" runat="server"/>
            </Body>
        </mcf:PopUpDialog>

        <mcf:PopUpDialog ID="dlgTable" runat="server" Title="Avviso">
            <Body>
                <asp:HiddenField runat="server" ID="hiddenGatewayValue" />
                <asp:label id="lblAvvisoTable" runat="server"/>
                <div class="modal-footer">
                    <div class="rgEditForm  RadGrid_Bootstrap rgEditPopup" style="width: 100%; z-index: 2500;" tabindex="0">
                        <div class="rgHeader" id="" style="height:20px;cursor:move;">
						    <div style="float:left;">
							    Inserisci Analyzer
						    </div>
                            <div style="float:right;">
                                <a href="javascript:__doPostBack('<%= this.ClientID %>','')" style="text-decoration:none;"><img title="Close" src="/WebResource.axd?d=EJ5TU3KBIxMV9V41DLVyrTj9HSwh6aHFvNe60T-6plxnNpO4ZQr5TK46CneQICmveCNV2gbKI6fiAK5hOoOkFERCsbrFwexIrS4xc50tOazs7WZ_8obTgxykPq-0C2VKBEJW7I0KpP53gN_CTC2M-Q2&amp;amp;t=636029710493406735" alt="Close" style="border:0;cursor:pointer;float:left;"></a>
						    </div>
                            <div style="clear:both;display:none;">
						    </div>
					    </div>
                        <div id="EditFormContainer" class="rgEditFormContainer" style="width: 100%;">
                            <table>
                                <thead>
                                    <tr style="display:none;">
                                        <th scope="col">Analyzers</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Label runat="server" Text="SerialCode:" AssociatedControlID="InsertAnalyzerSerialCode" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="InsertAnalyzerSerialCode" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label runat="server" Text="Description:" AssociatedControlID="InsertAnalyzerDescription" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="InsertAnalyzerDescription" />
                                        </td>
                                    </tr>
                                </tbody>		
                            </table>
                            <br />
                            <asp:LinkButton runat="server" ID="PerformInsertButton" CssClass="rgActionButton rgUpdate" OnClick="PerformInsertButton_Click" ToolTip="InsertText">
                                <span class="rgIcon rgUpdateIcon"></span>
                            </asp:LinkButton>
                            <button type="button" onclick="javascript:__doPostBack('<%= this.ClientID %>', '')" class="rgActionButton rgCancel"
                                id="CancelButton" name="CancelButton" value="CancelText" title="CancelText">
                                <span class="rgIcon rgCancelIcon"></span>
                            </button>
                        </div>    
                    </div>
                </div>
            </Body>
        </mcf:PopUpDialog>

         <%--<telerik:RadGrid RenderMode="Lightweight" ID="GridGateway" runat="server" ShowStatusBar="true" AutoGenerateColumns="False" 
             AllowSorting="True" AllowPaging="True" AllowAutomaticDeletes="false" AllowAutomaticInserts="false" AllowAutomaticUpdates="false"
             EnableLinqExpressions="false" DataSourceID="edsGateway" OnDetailTableDataBind="GridGateway_DetailTableDataBind" OnItemCommand="GridGateway_ItemCommand">
             <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                 EditMode="PopUp" Name="Gateways" Caption="Gateways" HierarchyDefaultExpanded="false">
                 <EditFormSettings InsertCaption="Inserisci Gateway">
                     <PopUpSettings Modal="true" />
                 </EditFormSettings>
                 <DetailTables>
                     <telerik:GridTableView DataKeyNames="Id" Width="100%" runat="server" CommandItemDisplay="Top"
                         Name="Analyzers" Caption="Analyzers">
                         <CommandItemTemplate>
                             <asp:Button runat="server" ID="btnOpenInsertForm" Text="Insert" OnClick="btnOpenInsertForm_Click" CommandName="InitInsert" />
                         </CommandItemTemplate>
                         <EditFormSettings InsertCaption="Inserisci Analyzer">
                             <PopUpSettings Modal="true" />
                         </EditFormSettings>
                         <ParentTableRelation>
                             <telerik:GridRelationFields DetailKeyField="Id" MasterKeyField="Id"></telerik:GridRelationFields>
                         </ParentTableRelation>
                         <Columns>
                             <telerik:GridEditCommandColumn UniqueName="EditColumnAnalyzer" />
                             <telerik:GridBoundColumn SortExpression="SerialCode" HeaderText="SerialCode" DataField="SerialCode"
                                 UniqueName="AnalyzerSerialCode" ColumnValidationSettings-EnableRequiredFieldValidation="true" />
                             <telerik:GridBoundColumn SortExpression="Description" HeaderText="Description" DataField="Description"
                                 UniqueName="AnalyzerDescription" ColumnValidationSettings-EnableRequiredFieldValidation="true" />
                             <telerik:GridButtonColumn ConfirmText="Delete this product?"
                                 CommandName="Delete" Text="Delete" UniqueName="DeleteColumnAnalyzer">
                             </telerik:GridButtonColumn>
                         </Columns>
                    </telerik:GridTableView>
                </DetailTables>
                <Columns>
                    <telerik:GridEditCommandColumn UniqueName="EditColumnGateway" />
                    <telerik:GridBoundColumn SortExpression="SerialCode" HeaderText="SerialCode" DataField="SerialCode" UniqueName="GatewaySerialCode" />
                    <telerik:GridBoundColumn SortExpression="Description" HeaderText="Description" DataField="Description" UniqueName="GatewayDescription" />
                    <telerik:GridButtonColumn ConfirmText="Delete this customer?"
                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumnGateway">
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>--%>

    <%--</ContentTemplate>
</asp:UpdatePanel>--%>


<ef:EntityDataSource ID="edsAsset" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    OrderBy="it.Code" />
<ef:EntityDataSource ID="edsZone" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Zones"
    OrderBy="it.Code" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsGroupAsset" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="AssetGroups"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsPlant" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Plants"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsDepartment" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Departments"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsAnalyzer" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Analyzers"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsGateway" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Gateways"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />


<%--<asp:UpdatePanel runat="server">
    <ContentTemplate>--%>

<%--<telerik:RadGrid RenderMode="Lightweight" ID="RadGrid1" runat="server" AutoGenerateColumns="False" 
    AllowAutomaticDeletes="true" AllowAutomaticInserts="false" AllowAutomaticUpdates="false"
    EnableLinqExpressions="false" OnDetailTableDataBind="RadGrid1_DetailTableDataBind">
    <MasterTableView DataKeyNames="Id" Width="100%" CommandItemDisplay="Top"
        Name="Plants" Caption="Plants" HierarchyDefaultExpanded="true" EditMode="PopUp">
        <EditFormSettings InsertCaption="Inserisci Plant">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <DetailTables>
            <telerik:GridTableView DataKeyNames="Id" Width="100%" runat="server"
                CommandItemDisplay="Top" Name="Departments" Caption="Departments" RetainExpandStateOnRebind="true"
                AllowAutomaticInserts="false" AllowAutomaticUpdates="false" EditMode="PopUp">
                <EditFormSettings InsertCaption="Inserisci Department">
                    <PopUpSettings Modal="true" />
                </EditFormSettings>
                <ParentTableRelation>
                    <telerik:GridRelationFields DetailKeyField="PlantId" MasterKeyField="Id"></telerik:GridRelationFields>
                </ParentTableRelation>
                <Columns>
                   <telerik:GridEditCommandColumn />
                    <telerik:GridBoundColumn DataField="Code" HeaderText="Code">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Description">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />                    
                </Columns>    
            </telerik:GridTableView>
        </DetailTables>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="Code" HeaderText="Code">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Description" HeaderText="Description">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
</telerik:RadGrid>--%>
    
   <%-- </ContentTemplate>
</asp:UpdatePanel>--%>

<script type="text/javascript">
    function PopUpShowing(sender, eventArgs) {
        var popUp = eventArgs.get_popUp();
        var scrollPosition = $('#<%= Page.Master.FindControl("ScrollPosition").ClientID %>').val();
        popUp.style.top = scrollPosition + "px";//((gridHeight - popUpHeight) / 2 + sender.get_element().offsetTop).toString() + "px";
        //var gridWidth = sender.get_element().offsetWidth;
        //var gridHeight = sender.get_element().offsetHeight;
        //var popUpWidth = popUp.style.width.substr(0, popUp.style.width.indexOf("px"));
        //var popUpHeight = popUp.style.height.substr(0, popUp.style.height.indexOf("px"));
        //popUp.style.left = ((gridWidth - popUpWidth) / 2 + sender.get_element().offsetLeft).toString() + "px";
    }
</script>
