﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.AssetList
{
    public partial class AssetList_Edit : WidgetControl<object>
    {
        public AssetList_Edit() : base(typeof(AssetList_Edit)) {}

        public string ApplicationId
        {
            get
            {
                return (Page as PageEdit).GetApplicationId();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            edsAsset.Selecting += new EventHandler<Microsoft.AspNet.EntityDataSource.EntityDataSourceSelectingEventArgs>(edsAsset_Selecting);
        }

        protected void edsAsset_Selecting(object sender, Microsoft.AspNet.EntityDataSource.EntityDataSourceSelectingEventArgs e)
        {
            if (e.DataSource.Where != string.Empty)
            {
                e.DataSource.Where += " AND ";
            }

            e.DataSource.Where += string.Format("it.ApplicationId == '{0}'", ApplicationId);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //ApplicationId = (Page as PageEdit).GetApplicationId();

            //edsAsset.WhereParameters.Clear();
            //edsAsset.AutoGenerateWhereClause = true;
            //edsAsset.WhereParameters.Add("ApplicationId", ApplicationId);

            edsAnalyzer.WhereParameters.Clear();
            edsAnalyzer.WhereParameters.Add("ApplicationId", ApplicationId);

            edsGroupAsset.WhereParameters.Clear();
            edsGroupAsset.WhereParameters.Add("ApplicationId", ApplicationId);

            edsPlant.WhereParameters.Clear();
            edsPlant.WhereParameters.Add("ApplicationId", ApplicationId);

            edsDepartment.WhereParameters.Clear();
            edsDepartment.WhereParameters.Add("ApplicationId", ApplicationId);

            edsGateway.WhereParameters.Clear();
            edsGateway.WhereParameters.Add("ApplicationId", ApplicationId);

            edsZone.WhereParameters.Clear();
            edsZone.WhereParameters.Add("ApplicationId", ApplicationId);

            //using (EnergyDbContext db = new EnergyDbContext())
            //{
            //    List<Gateway> dataSource = (from g in db.Gateways
            //                                where g.ApplicationId == ApplicationId
            //                                select g).ToList();
            //    GridGateway.MasterTableView.DataSource = dataSource;
            //    GridGateway.DataBind();

            //List<Plant> dataSource = (from p in db.Plants
            //                          where p.ApplicationId == ApplicationId
            //                          select p).ToList();
            //RadGrid1.MasterTableView.DataSource = dataSource;
            //RadGrid1.DataBind();
            //}
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                //string applicationId = (Page as PageEdit).GetApplicationId();
                if (!db.Plants.Where(p => p.ApplicationId == ApplicationId).Any())
                {
                    gridDepartment.Visible = false;
                }
                else
                {
                    gridDepartment.Visible = true;
                }
                if (!db.Departments.Where(d => d.ApplicationId == ApplicationId).Any())
                {
                    gridAsset.Visible = false;
                }
                else
                {
                    gridAsset.Visible = true;
                }
                if (db.Analyzers.Count(x => x.ApplicationId == ApplicationId) >= Convert.ToInt32(ApplicationSettingsHelper.GetConfiguration(ApplicationSettingsKey.LicenseMaxAnalyzer, ApplicationId)))
                {
                    gridAnalyzer.MasterTableView.CommandItemSettings.ShowAddNewRecordButton = false;
                    lblAnalyzerCounter.Visible = false;
                }
                else
                {
                    gridAnalyzer.MasterTableView.CommandItemSettings.ShowAddNewRecordButton = true;
                    lblAnalyzerCounter.Visible = true;
                    lblAnalyzerCounter.Text =
                        "È possibile aggiungere ancora " +
                        (Convert.ToInt32(ApplicationSettingsHelper.GetConfiguration(ApplicationSettingsKey.LicenseMaxAnalyzer, ApplicationId)) - db.Analyzers.Count(x => x.ApplicationId == ApplicationId)).ToString() +
                        " analizzatori";
                }
            }
        }

        #region InsertCommand

        protected void gridPlant_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                Hashtable values = new Hashtable();
                editableItem.ExtractValues(values);
                
                var plant = new Plant();
                plant.ApplicationId = ApplicationId;
                plant.Code = (string)values["Code"];
                plant.Description = (string)values["Description"];
                plant.Address = (string)values["Address"];
                plant.City = (string)values["City"];
                plant.Country = (string)values["Country"];
                plant.AnalyzerId = (string)values["AnalyzerId"];
                plant.POD = (string)values["POD"];
                plant.EnablePower = (string)values["EnablePower"];
                
                plant.Voltage = (editableItem["Tensione"].FindControl("VoltageItems") as RadComboBox).SelectedValue;

                if (!string.IsNullOrEmpty((editableItem["Coords"].FindControl("txtLatitude") as RadNumericTextBox).DisplayText))
                {
                    plant.Latitude = Convert.ToDouble((editableItem["Coords"].FindControl("txtLatitude") as RadNumericTextBox).DisplayText, CultureInfo.InvariantCulture);
                }
                if (!string.IsNullOrEmpty((editableItem["Coords"].FindControl("txtLongitude") as RadNumericTextBox).DisplayText))
                {
                    plant.Longitude = Convert.ToDouble((editableItem["Coords"].FindControl("txtLongitude") as RadNumericTextBox).DisplayText, CultureInfo.InvariantCulture);
                }

                var img = editableItem["Image"].FindControl("ImageManager");
                string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                if (!string.IsNullOrEmpty(pathImg))
                {
                    plant.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                }

                db.Plants.Add(plant);
                db.SaveChanges();
            }
        }

        protected void gridDepartment_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                Hashtable values = new Hashtable();
                editableItem.ExtractValues(values);

                var department = new Department();
                department.ApplicationId = ApplicationId;
                department.Code = (string)values["Code"];
                department.Description = (string)values["Description"];
                department.AnalyzerId = (string)values["AnalyzerId"];
                department.PlantId = (string)values["PlantId"];

                var img = editableItem["Image"].FindControl("ImageManager");
                string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                if (!string.IsNullOrEmpty(pathImg))
                {
                    department.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                }

                db.Departments.Add(department);
                db.SaveChanges();
            }
        }

        protected void gridAsset_InsertCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                Hashtable values = new Hashtable();
                editableItem.ExtractValues(values);

                var asset = new Asset();
                asset.ApplicationId = ApplicationId;
                asset.Code = (string)values["Code"];
                asset.Description = (string)values["Description"];
                asset.DepartmentId = (string)values["DepartmentId"];
                asset.ZoneId = (string)values["ZoneId"];
                asset.DtInstallation = Convert.ToDateTime(values["DtInstallation"]);
                asset.StandardRate = Convert.ToDecimal(values["StandardRate"]);
                asset.NumShotTimeout = Convert.ToInt32(values["NumShotTimeout"]);
                asset.Timeout = Convert.ToDecimal(values["Timeout"]);
                if (!string.IsNullOrEmpty((string)values["PartProgramPath"]))
                {
                    asset.PartProgramPath = (string)values["PartProgramPath"];
                }
                //if (values["NumShotTimeout"] != null)
                //{
                //    asset.NumShotTimeout = Convert.ToInt32(values["NumShotTimeout"]);
                //}
                //else
                //{
                //    asset.NumShotTimeout = 2;
                //}
                //if (values["Timeout"] != null)
                //{
                //    asset.Timeout = Convert.ToDecimal(values["Timeout"]);
                //}
                //else
                //{
                //    asset.Timeout = 120; //2 minuti
                //}

                asset.Brand = (string)values["Brand"];
                asset.IdNumber = (string)values["IdNumber"];
                asset.Model = (string)values["Model"];
                asset.AnalyzerId = (string)values["AnalyzerId"];
                asset.AssetGroupId = (string)values["AssetGroupId"];
                if (!string.IsNullOrEmpty((string)values["DtEndGuarantee"]))
                {
                    asset.DtEndGuarantee = Convert.ToDateTime(values["DtEndGuarantee"]);
                }

                var img = editableItem["Image"].FindControl("ImageManager");
                string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                if (!string.IsNullOrEmpty(pathImg))
                {
                    asset.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                }
                //RadAsyncUpload radAsyncUpload = editableItem["Image"].FindControl("UploadImg") as RadAsyncUpload;
                //if (radAsyncUpload.UploadedFiles.Count > 0)
                //{
                //    UploadedFile file = radAsyncUpload.UploadedFiles[0];
                //    asset.PathImg = file.GetName();
                //}

                db.Assets.Add(asset);
                db.SaveChanges();
            }
        }

        protected void gridGroupAsset_InsertCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                Hashtable values = new Hashtable();
                editableItem.ExtractValues(values);

                var assetGroup = new AssetGroup();
                assetGroup.ApplicationId = ApplicationId;
                assetGroup.Code = (string)values["Code"];
                assetGroup.Description = (string)values["Description"];

                db.AssetGroups.Add(assetGroup);
                db.SaveChanges();
            }
        }

        protected void gridAnalyzer_InsertCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                Hashtable values = new Hashtable();
                editableItem.ExtractValues(values);

                var analyzer = new Analyzer();
                analyzer.ApplicationId = ApplicationId;
                analyzer.SerialCode = (string)values["SerialCode"];
                analyzer.Description = (string)values["Description"];
                analyzer.NetworkAddress = (string)values["NetworkAddress"];
                analyzer.GatewayId = (string)values["GatewayId"];

                var img = editableItem["Image"].FindControl("ImageManager");
                string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                if (!string.IsNullOrEmpty(pathImg))
                {
                    analyzer.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                }

                db.Analyzers.Add(analyzer);
                db.SaveChanges();
            }
        }

        protected void GridGateway_InsertCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                Hashtable values = new Hashtable();
                editableItem.ExtractValues(values);

                var gateway = new Gateway();
                gateway.ApplicationId = ApplicationId;
                gateway.SerialCode = (string)values["SerialCode"];
                gateway.Description = (string)values["Description"];
                gateway.NetworkAddress = (string)values["NetworkAddress"];
                gateway.DNSName = (string)values["DNSName"];

                var img = editableItem["Image"].FindControl("ImageManager");
                string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                if (!string.IsNullOrEmpty(pathImg))
                {
                    gateway.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                }

                db.Gateways.Add(gateway);
                db.SaveChanges();
            }
            //switch (e.Item.OwnerTableView.Name)
            //{
            //    case "Analyzers":
            //        using (AssetManagementDbContext db = new AssetManagementDbContext())
            //        {
            //            var editableItem = ((GridEditableItem)e.Item);
            //            Hashtable values = new Hashtable();
            //            editableItem.ExtractValues(values);

            //            var analyzer = new Analyzer();
            //            analyzer.ApplicationId = ApplicationId;
            //            analyzer.SerialCode = (string)values["SerialCode"];
            //            analyzer.Description = (string)values["Description"];
            //            //analyzer.GatewayId = 

            //            db.Analyzers.Add(analyzer);
            //            db.SaveChanges();
            //        }
            //        break;
            //    case "Gateways":
            //        using (AssetManagementDbContext db = new AssetManagementDbContext())
            //        {
            //            var editableItem = ((GridEditableItem)e.Item);
            //            Hashtable values = new Hashtable();
            //            editableItem.ExtractValues(values);

            //            var gateway = new Gateway();
            //            gateway.ApplicationId = ApplicationId;
            //            gateway.SerialCode = (string)values["SerialCode"];
            //            gateway.Description = (string)values["Description"];

            //            db.Gateways.Add(gateway);
            //            db.SaveChanges();
            //        }
            //        break;
            //}
        }

        #endregion

        #region UpdateCommand

        protected void gridPlant_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                var plantId = (string)editableItem.GetDataKeyValue("Id");

                var plant = db.Plants.Where(p => p.Id == plantId).FirstOrDefault();
                if (plant != null)
                {
                    editableItem.UpdateValues(plant);

                    var img = editableItem["Image"].FindControl("ImageManager");
                    string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                    if (!string.IsNullOrEmpty(pathImg))
                    {
                        plant.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                    }
                    
                    RadComboBox comboBox = editableItem["Tensione"].FindControl("VoltageItems") as RadComboBox;
                    if (!string.IsNullOrEmpty(comboBox.SelectedValue))
                    {
                        plant.Voltage = comboBox.SelectedValue;
                    }

                    if (!string.IsNullOrEmpty((editableItem["Coords"].FindControl("txtLatitude") as RadNumericTextBox).DisplayText))
                    {
                        plant.Latitude = Convert.ToDouble((editableItem["Coords"].FindControl("txtLatitude") as RadNumericTextBox).DisplayText, CultureInfo.InvariantCulture);
                    }
                    if (!string.IsNullOrEmpty((editableItem["Coords"].FindControl("txtLongitude") as RadNumericTextBox).DisplayText))
                    {
                        plant.Longitude = Convert.ToDouble((editableItem["Coords"].FindControl("txtLongitude") as RadNumericTextBox).DisplayText, CultureInfo.InvariantCulture);
                    }

                    if (string.IsNullOrEmpty(plant.AnalyzerId))
                    {
                        plant.AnalyzerId = null;
                    }
                    db.SaveChanges();
                }
            }
        }

        protected void gridGroupAsset_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                var assetGroupId = (string)editableItem.GetDataKeyValue("Id");

                var assetGroup = db.AssetGroups.Where(a => a.Id == assetGroupId).FirstOrDefault();
                if (assetGroup != null)
                {
                    editableItem.UpdateValues(assetGroup);
                    db.SaveChanges();
                }
            }
        }

        protected void gridAsset_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                var assetId = (string)editableItem.GetDataKeyValue("Id");

                var asset = db.Assets.Where(a => a.Id == assetId).FirstOrDefault();
                if (asset != null)
                {
                    editableItem.UpdateValues(asset);

                    var img = editableItem["Image"].FindControl("ImageManager");
                    string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                    if (!string.IsNullOrEmpty(pathImg))
                    {
                        asset.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                    }
                    //RadAsyncUpload radAsyncUpload = editableItem["Image"].FindControl("UploadImg") as RadAsyncUpload;
                    //if (radAsyncUpload.UploadedFiles.Count > 0)
                    //{
                    //    UploadedFile file = radAsyncUpload.UploadedFiles[0];
                    //    asset.PathImg = file.GetName();
                    //}

                    if (string.IsNullOrEmpty(asset.ZoneId))
                    {
                        asset.ZoneId = null;
                    }
                    if (string.IsNullOrEmpty(asset.DepartmentId))
                    {
                        asset.DepartmentId = null;
                    }
                    if (string.IsNullOrEmpty(asset.AnalyzerId))
                    {
                        asset.AnalyzerId = null;
                    }
                    if (string.IsNullOrEmpty(asset.AssetGroupId))
                    {
                        asset.AssetGroupId = null;
                    }
                    db.SaveChanges();
                }
            }
        }

        protected void gridDepartment_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                var departmentId = (string)editableItem.GetDataKeyValue("Id");

                var department = db.Departments.Where(d => d.Id == departmentId).FirstOrDefault();
                if (department != null)
                {
                    editableItem.UpdateValues(department);

                    var img = editableItem["Image"].FindControl("ImageManager");
                    string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                    if (!string.IsNullOrEmpty(pathImg))
                    {
                        department.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                    }

                    if (department.AnalyzerId == string.Empty)
                    {
                        department.AnalyzerId = null;
                    }
                    db.SaveChanges();
                }
            }
        }

        protected void gridAnalyzer_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                var analyzerId = (string)editableItem.GetDataKeyValue("Id");

                var analyzer = db.Analyzers.Where(a => a.Id == analyzerId).FirstOrDefault();
                if (analyzer != null)
                {
                    editableItem.UpdateValues(analyzer);
                    
                    var img = editableItem["Image"].FindControl("ImageManager");
                    string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                    if (!string.IsNullOrEmpty(pathImg))
                    {
                        analyzer.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                    }
                    if (string.IsNullOrEmpty(analyzer.GatewayId))
                    {
                        analyzer.GatewayId = null;
                    }
                    db.SaveChanges();
                }
            }
        }

        protected void GridGateway_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                var gatewayId = (string)editableItem.GetDataKeyValue("Id");

                var gateway = db.Gateways.Where(a => a.Id == gatewayId).FirstOrDefault();
                if (gateway != null)
                {
                    editableItem.UpdateValues(gateway);

                    var img = editableItem["Image"].FindControl("ImageManager");
                    string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                    if (!string.IsNullOrEmpty(pathImg))
                    {
                        gateway.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                    }
                    db.SaveChanges();
                }
            }
            //switch (e.Item.OwnerTableView.Name)
            //{
            //    case "Analyzers":
            //        using (AssetManagementDbContext db = new AssetManagementDbContext())
            //        {
            //            var editableItem = ((GridEditableItem)e.Item);
            //            var analyzerId = (string)editableItem.GetDataKeyValue("Id");

            //            var analyzer = db.Analyzers.Where(a => a.Id == analyzerId).FirstOrDefault();
            //            if (analyzer != null)
            //            {
            //                editableItem.UpdateValues(analyzer);
            //                db.SaveChanges();
            //            }
            //        }
            //        break;
            //    case "Gateways":
            //        using (AssetManagementDbContext db = new AssetManagementDbContext())
            //        {
            //            var editableItem = ((GridEditableItem)e.Item);
            //            var gatewayId = (string)editableItem.GetDataKeyValue("Id");

            //            var gateway = db.Gateways.Where(g => g.Id == gatewayId).FirstOrDefault();
            //            if (gateway != null)
            //            {
            //                editableItem.UpdateValues(gateway);
            //                db.SaveChanges();
            //            }
            //        }
            //        break;
            //}
        }

        #endregion

        #region CancelCommand

        protected void gridAsset_CancelCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var assetId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");

                var asset = db.Assets.Where(a => a.Id == assetId).FirstOrDefault();
                if (asset != null)
                {
                    db.Assets.Remove(asset);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        messaggio.Text = "Impossibile rimuovere il record";
                        dlgWidgetConfig.OpenDialog();
                    }
                }
            }
        }

        protected void gridDepartment_CancelCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var departmentId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");

                var department = db.Departments.Where(a => a.Id == departmentId).FirstOrDefault();
                if (department != null)
                {
                    db.Departments.Remove(department);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        messaggio.Text = "Impossibile rimuovere il record";
                        dlgWidgetConfig.OpenDialog();
                    }
                }
            }
        }

        protected void gridPlant_CancelCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var plantId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");

                var plant = db.Plants.Where(a => a.Id == plantId).FirstOrDefault();
                if (plant != null)
                {
                    db.Plants.Remove(plant);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        messaggio.Text = "Impossibile rimuovere il record";
                        dlgWidgetConfig.OpenDialog();
                    }
                }
            }
        }

        protected void gridGroupAsset_CancelCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var assetGroupId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");

                //retrive entity form the Db
                var assetGroup = db.AssetGroups.Where(a => a.Id == assetGroupId).FirstOrDefault();
                if (assetGroup != null)
                {
                    //add the product for deletion
                    db.AssetGroups.Remove(assetGroup);
                    try
                    {
                        //save chanages to Db
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        messaggio.Text = "Impossibile rimuovere il record";
                        dlgWidgetConfig.OpenDialog();
                    }
                }
            }
        }

        protected void gridAnalyzer_CancelCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var analyzerId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");

                var analyzer = db.Analyzers.Where(a => a.Id == analyzerId).FirstOrDefault();
                if (analyzer != null)
                {
                    db.Analyzers.Remove(analyzer);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        messaggio.Text = "Impossibile rimuovere il record";
                        dlgWidgetConfig.OpenDialog();
                    }
                }
            }
        }

        protected void GridGateway_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var gatewayId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");

                var gateway = db.Gateways.Where(a => a.Id == gatewayId).FirstOrDefault();
                if (gateway != null)
                {
                    db.Gateways.Remove(gateway);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        messaggio.Text = "Impossibile rimuovere il record";
                        dlgWidgetConfig.OpenDialog();
                    }
                }
            }
        }

        #endregion

        //protected void gridDepartment_ItemCommand(object sender, GridCommandEventArgs e)
        //{
        //    if (e.CommandName == "InitInsert")
        //    {
        //        string applicationId = (Page as PageEdit).GetApplicationId();
        //        using (EnergyDbContext db = new EnergyDbContext())
        //        {
        //            if (!db.Plants.Where(p => p.ApplicationId == applicationId).Any())
        //            {
        //                e.Canceled = true;
        //                lblAvvisoTable.Text = "Non è possibile inserire Reparti senza nessun Sito. Inserire prima un nuovo Sito";
        //                dlgTable.OpenDialog();
        //                InsertButton.CommandArgument = gridPlant.ID;
        //            }
        //        }
        //    }
        //}

        //protected void InsertButton_Command(object sender, CommandEventArgs e)
        //{
        //    //mette in evidenza la tabella
        //    //apre la modalità inserimento

        //    RadGrid table = FindControl((string)e.CommandArgument) as RadGrid;
        //    table.MasterTableView.IsItemInserted = true;
        //    table.MasterTableView.Rebind();
        //}

        //protected void GridGateway_ItemCommand(object sender, GridCommandEventArgs e)
        //{
        //    if (e.CommandName == RadGrid.ExpandCollapseCommandName)
        //    {
        //        int i = 0;
        //    }
        //    if (e.CommandName == RadGrid.InitInsertCommandName)
        //    {
        //        if (e.Item.OwnerTableView.Name == "Analyzers")
        //        {
        //            //e.Item.FireCommandEvent("PerformInsert", String.Empty);
        //            //hiddenGatewayValue.Value = e.Item.OwnerTableView.ParentItem;
        //        }
        //    }
        //}

        //protected void GridGateway_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        //{
        //    GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
        //    switch (e.DetailTableView.Name)
        //    {
        //        case "Analyzers":
        //            {
        //                string gatewayId = dataItem.GetDataKeyValue("Id").ToString();
        //                using (AssetManagementDbContext db = new AssetManagementDbContext())
        //                {
        //                    List<Analyzer> dataSource = (from a in db.Analyzers
        //                                                 where a.GatewayId == gatewayId && a.ApplicationId == ApplicationId
        //                                                 select a).ToList();
        //                    e.DetailTableView.DataSource = dataSource;
        //                }
        //                break;
        //            }
        //    }
        //}

        //protected void RadGrid1_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        //{
        //    GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
        //    switch (e.DetailTableView.Name)
        //    {
        //        case "Departments":
        //            {
        //                string plantId = dataItem.GetDataKeyValue("Id").ToString();
        //                using (AssetManagementDbContext db = new AssetManagementDbContext())
        //                {
        //                    List<Department> dataSource = (from d in db.Departments
        //                                                   where d.PlantId == plantId && d.ApplicationId == ApplicationId
        //                                                   select d).ToList();
        //                    e.DetailTableView.DataSource = dataSource;
        //                }
        //                break;
        //            }

        //    }
        //}

        protected void btnOpenInsertForm_Click(object sender, EventArgs e)
        {
            dlgTable.OpenDialog();
        }

        protected void PerformInsertButton_Click(object sender, EventArgs e)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                var analyzer = new Analyzer();
                analyzer.ApplicationId = ApplicationId;
                analyzer.SerialCode = InsertAnalyzerSerialCode.Text;
                analyzer.Description = InsertAnalyzerDescription.Text;
                analyzer.GatewayId = "";


                db.Analyzers.Add(analyzer);
                db.SaveChanges();
            }
        }

        protected void gridZone_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                ZoneRepository zoneRep = new ZoneRepository();
                zoneRep.InsertNewZone();
                zoneRep.SaveChanges();
                e.Canceled = true;
                gridZone.Rebind();
            }
            if (e.CommandName == RadGrid.UpdateCommandName)
            {
                using (AssetManagementDbContext db = new AssetManagementDbContext())
                {
                    var editableItem = ((GridEditableItem)e.Item);
                    var zoneId = (string)editableItem.GetDataKeyValue("Id");

                    var zone = db.Zones.Where(a => a.Id == zoneId).FirstOrDefault();
                    if (zone != null)
                    {
                        editableItem.UpdateValues(zone);

                        if (string.IsNullOrEmpty(zone.DepartmentId))
                        {
                            zone.DepartmentId = null;
                        }

                        db.SaveChanges();
                    }
                }
            }
        }

        protected void gridZone_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            ZoneRepository zoneRep = new ZoneRepository();
            var zoneId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");

            var zone = zoneRep.FindByID(zoneId);
            if (zone != null)
            {
                zoneRep.Delete(zone);
                try
                {
                    zoneRep.SaveChanges();
                }
                catch (Exception ex)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }
    }
}