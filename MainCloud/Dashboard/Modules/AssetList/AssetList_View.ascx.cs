﻿using AssetManagement.Core;
using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule.Core;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web.Multitenants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Telerik.Web.UI;

namespace MainCloudFramework.Dashboard.Modules.AssetList
{
    public partial class AssetList_View : WidgetControl<object>
    {
        public AssetList_View() : base(typeof(AssetList_View)) { }

        public enum BehaviourKey
        {
            Redirect, //salva la selezione nell'application tenant e fa la redirect in RealTime
            Area //la selezione comporta modifiche nella sola area in cui è istanziato il widget
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadAssetTreeView();
            }
        }

        protected void LoadAssetTreeView()
        {
            this.AssetTreeView.Nodes.Clear();

            //AnagraphicsService service = new AnagraphicsService();
            PlantService service = new PlantService();
            var plants = service.PlantList();
            foreach (Plant p in plants.OrderBy(x => x.Description))
            {
                //Node Plant
                RadTreeNode nodeP = new RadTreeNode(p.Description);
                if (string.IsNullOrEmpty(p.AnalyzerId))
                {
                    nodeP.PostBack = false;
                    nodeP.ForeColor = Color.Red;
                }
                else
                {
                    nodeP.PostBack = true;
                }
                nodeP.Value = JsonConvert.SerializeObject(new SelectedContext(p.Id, AssetManagementEnum.AnalyzerContext.Plant));
                nodeP.Expanded = true;
                nodeP.Width = Unit.Percentage(66);
                nodeP.Height = Unit.Percentage(30);
                foreach (Department d in p.Department.OrderBy(x => x.Description))
                {
                    //Node Department
                    RadTreeNode nodeD = new RadTreeNode(d.Description);
                    if (string.IsNullOrEmpty(d.AnalyzerId))
                    {
                        nodeD.PostBack = false;
                        nodeD.ForeColor = Color.Red;
                    }
                    else
                    {
                        nodeD.PostBack = true;
                    }
                    nodeD.Value = JsonConvert.SerializeObject(new SelectedContext(d.Id, AssetManagementEnum.AnalyzerContext.Department));
                    nodeD.Expanded = true;
                    nodeD.Width = Unit.Percentage(66);
                    nodeD.Height = Unit.Percentage(30);
                    foreach (Asset a in d.Asset.OrderBy(x => x.Description))
                    {
                        //Node Asset
                        RadTreeNode nodeA = new RadTreeNode(a.Description);
                        if (!string.IsNullOrEmpty(a.ZoneId))
                        {
                            nodeA.Text += " - " + a.Zone.Description;
                        }
                        if (string.IsNullOrEmpty(a.AnalyzerId))
                        {
                            nodeA.PostBack = false;
                            nodeA.ForeColor = Color.Red;
                        }
                        else
                        {
                            nodeA.PostBack = true;
                        }
                        nodeA.Value = JsonConvert.SerializeObject(new SelectedContext(a.Id, AssetManagementEnum.AnalyzerContext.Asset));
                        nodeA.Expanded = true;
                        nodeA.Width = Unit.Percentage(66);
                        nodeA.Height = Unit.Percentage(30);

                        nodeD.Nodes.Add(nodeA);
                    }
                    nodeP.Nodes.Add(nodeD);
                }
                AssetTreeView.Nodes.Add(nodeP);
            }
        }

        protected void AssetTreeView_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            if (("" + CustomSettings.Find("Behaviour")).Trim().ToLower() == "redirect")
            {
                //if (Convert.ToInt64(e.Node.Value) == 18)
                //{
                //    Response.Redirect("~/Area/Fine_Linea");
                //}
                //if (Convert.ToInt64(e.Node.Value) == 19)
                //{
                //    Response.Redirect("~/Area/Handling");
                //}

                //Pagina di selezione asset, salvo in sessione e vado in real time
                SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT] = e.Node.Value;
                SessionMultitenant[EMConstants.START_ACTIVEENERGY_COUNTER] = null;
                SessionMultitenant[EMConstants.START_REACTIVEENERGY_COUNTER] = null;
                
                string returnUrl = Request["returnurl"];
                if (string.IsNullOrEmpty(returnUrl))
                {
                    string destinationAreaId = CustomSettings.Find("settings-destination-area-id") as string;
                    if (!string.IsNullOrEmpty(destinationAreaId))
                    {
                        WidgetAreaRepository areaService = new WidgetAreaRepository();
                        WidgetArea wa = areaService.FindByID(destinationAreaId);

                        Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
                    }
                    //Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl("RealTime"));
                    // eliminare se funziona la riga sopra -- Response.Redirect("~/App/" + MultiTenantsHelper.ApplicationName + "/Area/RealTime");
                }
                else
                {
                    Response.Redirect("~" + returnUrl);
                }
            }
        }

    }
}