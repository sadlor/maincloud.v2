﻿using MainCloudFramework.Web.Multitenants;
using MoldID.Core;
using MoldID.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace MainCloud.Dashboard.Modules.MoldID.Efficiency
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Efficiency
    {
        // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
        // To create an operation that returns XML,
        //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     and include the following line in the operation body:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        [WebGet]
        public decimal GetValue()
        {
            var tenantName = HttpContext.Current.Request.QueryString["MultiTenant"];
            HttpMultitenantsSessionState SessionMultitenant = HttpContext.Current.Session.SessionMultitenant(tenantName);
            if (SessionMultitenant.ContainsKey(MoldIDConstants.SELECTED_MOLD_ID))
            {
                string moldId = (string)SessionMultitenant[MoldIDConstants.SELECTED_MOLD_ID];
                MoldDataService MDS = new MoldDataService();
                return MDS.Efficiency(moldId) * 100;
            }
            else
            {
                return -1;
            }
        }
    }
}