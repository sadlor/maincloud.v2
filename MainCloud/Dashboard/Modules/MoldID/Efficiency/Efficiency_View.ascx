﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Efficiency_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MoldID.Efficiency.Efficiency_View" %>

<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <telerik:RadRadialGauge runat="server" ID="MeterEfficiency" Width="100%">
            <Pointer Value="0">
                <Cap Size="0.1" />
            </Pointer>
            <Scale Min="0" Max="100" MajorUnit="20">
                <Labels Format="{0}" />
                <Ranges>
                    <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                    <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                    <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                    <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                </Ranges>
            </Scale>
        </telerik:RadRadialGauge>
    </ContentTemplate>
</asp:UpdatePanel>

<script>

$(function () {
        UpdateMeter();
        setInterval(function () {
            UpdateMeter();
        }, 10000);
    });

function UpdateMeter() {
    var request = $.ajax({
        url: '<%= MainCloudFramework.Web.Helpers.MultiTenantsHelper.MountAndResolveMultiTenantUrl("/Dashboard/Modules/MoldID/Efficiency/Efficiency.svc/GetValue") %>',
        method: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json'
    });

    request.done(function (response) {  //decimal
        if (response.d != -1) {
            $find("<%=MeterEfficiency.ClientID %>").set_value(response.d);
        }
    });

    request.fail(function (jqXHR, textStatus) {
        //alert("Request failed: " + textStatus);
    });
}

 </script>