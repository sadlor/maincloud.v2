﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MoldID.Core;
using MoldID.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.MoldID.Efficiency
{
    public partial class Efficiency_View : DataWidget<Efficiency_View>
    {
        public Efficiency_View() : base(typeof(Efficiency_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}