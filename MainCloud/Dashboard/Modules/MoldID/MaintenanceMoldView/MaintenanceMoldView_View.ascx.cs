﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MoldID.Core;
using MoldID.Models;
using MoldID.Repositories;
using MoldID.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.MoldID.MaintenanceMoldView
{
    public partial class MaintenanceMoldView_View : DataWidget<MaintenanceMoldView_View>
    {
        public MaintenanceMoldView_View() : base(typeof(MaintenanceMoldView_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionMultitenant.ContainsKey(MoldIDConstants.SELECTED_MOLD_ID))
            {
                string moldId = (string)SessionMultitenant[MoldIDConstants.SELECTED_MOLD_ID];
                MoldRepository MR = new MoldRepository();
                Mold mold = MR.FindByID(moldId);

                Warning1ProgressBar.Style.Clear();
                Warning1ProgressBar.Style.Add(HtmlTextWriterStyle.Left, (Math.Round((decimal)mold.Warning1 / (decimal)mold.Maximum * 100, 0) - 3).ToString() + "%");
                //Warning1ProgressBar.Style.Add(HtmlTextWriterStyle.Left, (Math.Round((decimal)mold.Warning1 / (decimal)mold.Maximum * 100, 0) -
                //                                                         Math.Round(58 / Convert.ToDecimal(ProgressBar.Style[HtmlTextWriterStyle.Width].Replace("px", "")) * 100, 0)).ToString() + "%");
                Warning2ProgressBar.Style.Clear();
                Warning2ProgressBar.Style.Add(HtmlTextWriterStyle.Left, (Math.Round((decimal)mold.Warning2 / (decimal)mold.Maximum * 100, 0) - 3).ToString() + "%");

                MoldDataService MDS = new MoldDataService();
                ValueProgressBar.Style.Clear();
                ValueProgressBar.Style.Add(HtmlTextWriterStyle.Width, (Math.Round((decimal)MDS.GetCurrentShots(moldId) / (decimal)mold.Maximum * 100, 0)).ToString() + "%");

                //to change color
                if (MDS.GetCurrentShots(moldId) > mold.Warning1)
                {
                    if (MDS.GetCurrentShots(moldId) >= mold.Maximum)
                    {
                        ValueProgressBar.Attributes["class"] = "progress-bar progress-bar-danger";
                    }
                    else
                    {
                        ValueProgressBar.Attributes["class"] = "progress-bar progress-bar-warning";
                    }
                }

                CreateTable();
            }
        }

        protected void CreateTable()
        {
            if (SessionMultitenant.ContainsKey(MoldIDConstants.SELECTED_MOLD_ID))
            {
                string moldId = (string)SessionMultitenant[MoldIDConstants.SELECTED_MOLD_ID];
                MoldRepository MR = new MoldRepository();
                Mold mold = MR.FindByID(moldId);
                MoldDataService MDS = new MoldDataService();

                int currentShots = MDS.GetCurrentShots(moldId);
                int totalShots = MDS.GetTotalShots(moldId);
                var duration = MDS.GetCurrentShotsDuration(moldId);
                var pezziOra = Math.Round(currentShots / duration.TotalHours, 2);

                TotalShotsLastField.InnerText = MDS.GetTotalShotsLastMaintenance(moldId).ToString();
                TotalShotsLastDateField.InnerText = MDS.GetLastMaintenance(moldId).ToString("dd/MM/yyyy HH:mm");
                TotalShotsField.InnerText = totalShots.ToString();
                TotalShotsDateField.InnerText = MDS.GetTotalShotsLastUpdate(moldId).ToString("dd/MM/yyyy HH:mm");
                CurrentShotsField.InnerText = (totalShots - MDS.GetTotalShotsLastMaintenance(moldId)).ToString();
                DurationCurrentShotsTeoricoField.InnerText = (Math.Round((decimal)currentShots / (decimal)mold.Frequency, 2)).ToString();
                DurationCurrentShotsField.InnerText = Math.Round(duration.TotalHours, 2).ToString();
                FrequencyTeoricoField.InnerText = mold.Frequency.ToString();
                FrequencyEffettivoField.InnerText = pezziOra.ToString();
                EfficiencyField.InnerText = (MDS.Efficiency(moldId) * 100).ToString() + "%";
                MaximumField.InnerText = mold.Maximum.ToString();
                NextMaintenanceRemainingShotsField.InnerText = (mold.Maximum - currentShots).ToString();
                Warning1Field.InnerText = mold.Warning1.ToString();
                Warning2Field.InnerText = mold.Warning2.ToString();
                var warning1Remain = Math.Round((mold.Warning1 - currentShots) / pezziOra, 2);
                if (warning1Remain < 0)
                {
                    Warning1RemainingField.InnerText = "0.00";
                }
                else
                {
                    Warning1RemainingField.InnerText = warning1Remain.ToString();
                }
                var warning2Remain = Math.Round((mold.Warning2 - currentShots) / pezziOra, 2);
                if (warning2Remain < 0)
                {
                    Warning2RemainingField.InnerText = "0.00";
                }
                else
                {
                    Warning2RemainingField.InnerText = Math.Round((mold.Warning2 - currentShots) / pezziOra, 2).ToString();
                }
                MaximumField2.InnerText = mold.Maximum.ToString();
                MaximumRemainingField.InnerText = Math.Round((mold.Maximum - currentShots) / pezziOra, 2).ToString();
            }
        }
    }
}