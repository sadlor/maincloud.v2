﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaintenanceMoldView_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MoldID.MaintenanceMoldView.MaintenanceMoldView_View" %>

<style type="text/css">
    .bar-step-up {
        position: absolute;
        margin-top: -20px;
        z-index: 1;
        font-size: 12px;
    }

    .bar-step-down {
        position: absolute;
        margin-top: 20px;
        z-index: 1;
        font-size: 12px;
    }

    .label-txt {
        float: left;
    }

    .label-line {
        float: right;
        background: #000;
        height: 40px;
        width: 1px;
        margin-left: 5px;
    }

    .label-line2 {
        float: right;
        background: #000;
        height: 40px;
        width: 1px;
        margin-left: 5px;
        margin-top: -40px;
    }
</style>

<asp:UpdatePanel ID="UpdatePanelProgress" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        <div class="panel panel-default">
            <table class="table table-bordered table-condensed info" style="width:100%;">
                <tr>
                    <td></td>
                    <td></td>
                    <td style="background-color:#428bca; color:white; text-align:center"><b>Pezzi</b></td>
                    <td style="background-color:#428bca; color:white; text-align:center"><b>Teorico</b></td>
                    <td style="background-color:#428bca; color:white; text-align:center"><b>Effettivo</b></td>
                </tr>
                <tr>
                    <td><b>Total shots last maintenance</b></td>
                    <td></td>
                    <td runat="server" id="TotalShotsLastField" style="text-align: right"></td>
                    <td></td>
                    <td runat="server" id="TotalShotsLastDateField" style="text-align: right"></td>
                </tr>
                <tr>
                    <td><b>Total shots</b></td>
                    <td></td>
                    <td runat="server" id="TotalShotsField" style="text-align: right"></td>
                    <td></td>
                    <td runat="server" id="TotalShotsDateField" style="text-align: right"></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>Current shots</b></td>
                    <td></td>
                    <td runat="server" id="CurrentShotsField" style="text-align: right"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>Ore lavorate</b></td>
                    <td></td>
                    <td></td>
                    <td runat="server" id="DurationCurrentShotsTeoricoField" style="text-align: right"></td>
                    <td runat="server" id="DurationCurrentShotsField" style="text-align: right"></td>
                </tr>
                <tr>
                    <td><b>Cadenza oraria media</b></td>
                    <td>pezzi/ora</td>
                    <td></td>
                    <td runat="server" id="FrequencyTeoricoField" style="text-align: right"></td>
                    <td runat="server" id="FrequencyEffettivoField" style="text-align: right"></td>
                </tr>
                <tr>
                    <td><b>Efficiency</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td runat="server" id="EfficiencyField" style="text-align: right"></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>Maximum</b></td>
                    <td></td>
                    <td runat="server" id="MaximumField" style="text-align: right"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>Q.tà next maintenance</b></td>
                    <td></td>
                    <td runat="server" id="NextMaintenanceRemainingShotsField" style="text-align: right"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>Warning1</b></td>
                    <td></td>
                    <td runat="server" id="Warning1Field" style="text-align: right"></td>
                    <td>fra ore</td>
                    <td runat="server" id="Warning1RemainingField" style="text-align: right"></td>
                </tr>
                <tr>
                    <td><b>Warning2</b></td>
                    <td></td>
                    <td runat="server" id="Warning2Field" style="text-align: right"></td>
                    <td>fra ore</td>
                    <td runat="server" id="Warning2RemainingField" style="text-align: right"></td>
                </tr>
                <tr>
                    <td><b>Maximum</b></td>
                    <td></td>
                    <td runat="server" id="MaximumField2" style="text-align: right"></td>
                    <td>fra ore</td>
                    <td runat="server" id="MaximumRemainingField" style="text-align: right"></td>
                </tr>
            </table>
        </div>

        <br />

        <div class="container" style="width:100%;">
            <div class="row"><br />
	            <div class="col-xs-12">
    	            <div runat="server" id="ProgressBar" class="progress" style="width:100%;">
                        <div runat="server" id="Warning1ProgressBar" class="bar-step-up">
                            <div class="label-txt">Warning1</div>
                            <div class="label-line"></div>
                        </div>
                        <div runat="server" id="Warning2ProgressBar" class="bar-step-down">
                            <div class="label-txt">Warning2</div>
                            <div class="label-line2"></div>
                        </div>
  			            <div runat="server" id="ValueProgressBar" class="progress-bar progress-bar-success"></div>
		            </div>
	            </div>
            </div>
        </div>

    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">

$(function () {
        ResfreshUpdatePanelMaintenance();
        setInterval(function () {
            ResfreshUpdatePanelMaintenance();
        }, 5000);
    });

function ResfreshUpdatePanelMaintenance() {
    var updatePanel = '<%=UpdatePanelProgress.ClientID %>';
    if (updatePanel != null)
    {
        __doPostBack(updatePanel, '');
    }
}

 </script>