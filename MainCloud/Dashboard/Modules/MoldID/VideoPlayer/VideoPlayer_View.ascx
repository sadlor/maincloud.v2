﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoPlayer_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MoldID.VideoPlayer.VideoPlayer_View" %>

<asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
    <ContentTemplate>

        <telerik:RadMediaPlayer RenderMode="Lightweight" ID="MediaPlayer1" runat="server" AutoPlay="true" 
            Source="https://www.youtube.com/watch?v=b6r-QoF-T8U" Width="100%">
        </telerik:RadMediaPlayer>

    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">

$(function () {
        ResfreshUpdatePanel();
        setInterval(function () {
            ResfreshUpdatePanel();
        }, 180000);
    });

function ResfreshUpdatePanel() {
    var updatePanel = '<%=UpdatePanel1.ClientID %>';
    if (updatePanel != null)
    {
        __doPostBack(updatePanel, '');
    }
}

 </script>