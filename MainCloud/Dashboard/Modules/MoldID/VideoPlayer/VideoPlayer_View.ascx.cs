﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.MoldID.VideoPlayer
{
    public partial class VideoPlayer_View : DataWidget<VideoPlayer_View>
    {
        public VideoPlayer_View() : base(typeof(VideoPlayer_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}