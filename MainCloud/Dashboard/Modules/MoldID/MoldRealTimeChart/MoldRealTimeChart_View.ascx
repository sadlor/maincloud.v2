﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoldRealTimeChart_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MoldID.MoldRealTimeChart.MoldRealTimeChart_View" %>

<asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        <telerik:RadHtmlChart runat="server" ID="ProgressiveChart" Skin="Silk">
            <PlotArea>
                <Series>
                    <telerik:ScatterLineSeries>
                        <LineAppearance Width="5px" />
                        <TooltipsAppearance>
                            <ClientTemplate>
                                #= kendo.format(\'{0:HH:mm}\', new Date(value.x)) #,  #= value.y #
                            </ClientTemplate>
                        </TooltipsAppearance>
                    </telerik:ScatterLineSeries>
                </Series>
                <YAxis>
                    <TitleAppearance Text="Qtà pezzi">
                        <TextStyle Margin="9" />
                    </TitleAppearance>
                </YAxis>
                <XAxis Type="Date" BaseUnit="Minutes" Step="60">
                    <TitleAppearance Text="Time (h)">
                    </TitleAppearance>
                </XAxis>
            </PlotArea>
        </telerik:RadHtmlChart>

        <%--<asp:Label Text="LastUpdate: " runat="server" AssociatedControlID="txtLastUpdate" />
        <asp:TextBox runat="server" ID="txtLastUpdate" />--%>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">

$(function () {
        ResfreshUpdatePanelRealTimeChart();
        setInterval(function () {
            ResfreshUpdatePanelRealTimeChart();
        }, 10000);
    });

function ResfreshUpdatePanelRealTimeChart() {
    var updatePanel = '<%=UpdatePanel1.ClientID %>';
    if (updatePanel != null)
    {
        __doPostBack(updatePanel, '');
    }
}

 </script>