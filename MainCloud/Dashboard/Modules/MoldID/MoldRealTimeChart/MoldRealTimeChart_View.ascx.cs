﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MoldID.Core;
using MoldID.Models;
using MoldID.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.MoldID.MoldRealTimeChart
{
    public partial class MoldRealTimeChart_View : DataWidget<MoldRealTimeChart_View>
    {
        public MoldRealTimeChart_View() : base(typeof(MoldRealTimeChart_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            CreateChart();
        }

        protected void CreateChart()
        {
            //ProgressiveChart.PlotArea.Series.Clear();
            ProgressiveChart.PlotArea.XAxis.Items.Clear();

            DateTime start = DateTime.Today;
            DateTime end = DateTime.Now;

            if (SessionMultitenant.ContainsKey(MoldIDConstants.SELECTED_MOLD_ID))
            {
                string moldId = (string)SessionMultitenant[MoldIDConstants.SELECTED_MOLD_ID];
                MoldDataRepository repos = new MoldDataRepository();

                List<MoldData> dataSource = repos.ReadAll(x => x.CycleStart >= start && x.CycleEnd <= end && x.TagId == moldId).OrderBy(x => x.CycleStart).ToList();

                if (dataSource.Count > 0)
                {
                    //ProgressiveChart.DataSource = dataSource;
                    //ProgressiveChart.DataBind();

                    DateTime epoch = new DateTime(1970, 1, 1);
                    DateTime dtStart = dataSource.First().CycleStart.Date.ToUniversalTime();
                    DateTime dtEnd = dataSource.Last().CycleEnd.Date.AddDays(1).ToUniversalTime();

                    //Set min and max date for a datetime x-axis type
                    ProgressiveChart.PlotArea.XAxis.MinValue = (decimal)dtStart.Subtract(epoch).TotalMilliseconds;
                    ProgressiveChart.PlotArea.XAxis.MaxValue = (decimal)dtEnd.Subtract(epoch).TotalMilliseconds;

                    ScatterLineSeries progressiveSerie = ProgressiveChart.PlotArea.Series[0] as ScatterLineSeries;
                    //progressiveSerie.TooltipsAppearance.ClientTemplate = "#= kendo.format(\'{0:HH:mm}\', new Date(value.x)) #,  #= value.y #";
                    progressiveSerie.MarkersAppearance.Visible = false;
                    progressiveSerie.LabelsAppearance.Visible = false;
                    //progressiveSerie.SeriesItems.Clear();
                    foreach (var item in dataSource)
                    {
                        progressiveSerie.SeriesItems.Add((decimal)item.CycleStart.ToUniversalTime().Subtract(epoch).TotalMilliseconds, item.CurrentShots);
                    }
                    //ProgressiveChart.PlotArea.Series.Add(progressiveSerie);

                    //asse x
                    //ProgressiveChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Hours;
                    //ProgressiveChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "HH";
                    //ProgressiveChart.PlotArea.XAxis.Step = 1;
                }
            }
        }
    }
}