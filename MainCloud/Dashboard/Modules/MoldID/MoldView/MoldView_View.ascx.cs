﻿using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MoldID.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.MoldID.MoldView
{
    public partial class MoldView_View : DataWidget<MoldView_View>
    {
        public MoldView_View() : base(typeof(MoldView_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                edsMold.WhereParameters.Clear();
                edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            }
        }

        protected void btnRedirect_Click(object sender, EventArgs e)
        {
            SessionMultitenant[MoldIDConstants.SELECTED_MOLD_ID] = (sender as RadButton).Value;

            string returnUrl = Request["returnurl"];
            if (string.IsNullOrEmpty(returnUrl))
            {
                string destinationAreaId = CustomSettings.Find("settings-destination-area-id") as string;
                if (!string.IsNullOrEmpty(destinationAreaId))
                {
                    WidgetAreaRepository areaService = new WidgetAreaRepository();
                    WidgetArea wa = areaService.FindByID(destinationAreaId);

                    Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
                }
                //Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl("RealTime"));
                // eliminare se funziona la riga sopra -- Response.Redirect("~/App/" + MultiTenantsHelper.ApplicationName + "/Area/RealTime");
            }
            else
            {
                Response.Redirect("~" + returnUrl);
            }
        }
    }
}