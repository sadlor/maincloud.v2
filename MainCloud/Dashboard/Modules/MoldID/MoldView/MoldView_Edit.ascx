﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoldView_Edit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MoldID.MoldView.MoldView_Edit" %>
<%@ Register src="~/Controls/ImageManager/ImageManager.ascx" tagname="ImageManager" tagprefix="mcf" %>

<script type="text/javascript">
    function PopUpShowing(sender, eventArgs) {
        var popUp = eventArgs.get_popUp();
        var scrollPosition = $('#<%= Page.Master.FindControl("ScrollPosition").ClientID %>').val();
        popUp.style.top = scrollPosition + "px";//((gridHeight - popUpHeight) / 2 + sender.get_element().offsetTop).toString() + "px";
        //var gridWidth = sender.get_element().offsetWidth;
        //var gridHeight = sender.get_element().offsetHeight;
        //var popUpWidth = popUp.style.width.substr(0, popUp.style.width.indexOf("px"));
        //var popUpHeight = popUp.style.height.substr(0, popUp.style.height.indexOf("px"));
        //popUp.style.left = ((gridWidth - popUpWidth) / 2 + sender.get_element().offsetLeft).toString() + "px";
    }
</script>

<telerik:RadGrid RenderMode="Lightweight" runat="server" ID="gridMold" AutoGenerateColumns="false" AllowPaging="true" DataSourceID="edsMold"
    AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false"
    OnInsertCommand="gridMold_InsertCommand" OnUpdateCommand="gridMold_UpdateCommand" OnDeleteCommand="gridMold_DeleteCommand"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US" Width="100%">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView DataKeyNames="Id" Caption="Molds" PageSize="20" FilterExpression="" EnableHeaderContextMenu="true"
        CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
        <EditFormSettings InsertCaption="Insert Mold"><PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn Groupable="false" />
            <telerik:GridBoundColumn DataField="Id" HeaderText="TagId" Groupable="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Name" HeaderText="Name" Groupable="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Code" HeaderText="Code" AllowFiltering="true" AllowSorting="true" Groupable="false" />
            <telerik:GridBoundColumn DataField="Description" HeaderText="Description" AllowFiltering="false" AllowSorting="false" Groupable="false" />
            <telerik:GridBoundColumn DataField="IdNumber" HeaderText="SerialCode" Groupable="false" />
            <telerik:GridBoundColumn DataField="Brand" HeaderText="Brand" Groupable="false" />
            <telerik:GridBoundColumn DataField="Model" HeaderText="Model" Groupable="false" />
            <telerik:GridDateTimeColumn DataField="DtInstallation" HeaderText="Installation Date" UniqueName="DtInstallation" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy}" EditDataFormatString="dd/MM/yyyy" Groupable="false">
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="DtEndGuarantee" HeaderText="End Guarantee Date" UniqueName="DtEndGuarantee" PickerType="DatePicker" EditDataFormatString="dd/MM/yyyy" DataFormatString="{0:dd/MM/yyyy}" Groupable="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true">
                </ColumnValidationSettings>
            </telerik:GridDateTimeColumn>
            <telerik:GridTemplateColumn HeaderText="Warning1" UniqueName="Warning1" Groupable="false">
                <EditItemTemplate>
                    <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtWarning1"
                        DisplayText='<%# Eval("Warning1")%>' DbValue='<%# Eval("Warning1")%>' DataType="System.Int32">
                        <NumberFormat DecimalSeparator="." DecimalDigits="0" GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:TextBox Text='<%# Eval("Warning1")%>' Width="90px" runat="server" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Warning2" UniqueName="Warning2" Groupable="false">
                <EditItemTemplate>
                    <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtWarning2"
                        DisplayText='<%# Eval("Warning2")%>' DbValue='<%# Eval("Warning2")%>' DataType="System.Int32">
                        <NumberFormat DecimalSeparator="." DecimalDigits="0" GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:TextBox Text='<%# Eval("Warning2")%>' Width="90px" runat="server" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Maximum" UniqueName="Maximum" Groupable="false">
                <EditItemTemplate>
                    <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtMaximum"
                        DisplayText='<%# Eval("Maximum")%>' DbValue='<%# Eval("Maximum")%>' DataType="System.Int32">
                        <NumberFormat DecimalSeparator="." DecimalDigits="0" GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:TextBox Text='<%# Eval("Maximum")%>' Width="90px" runat="server" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <%--<telerik:GridBoundColumn DataField="Maximum" HeaderText="Maximum" DataType="System.Int32" />--%>
            <telerik:GridBoundColumn DataField="Frequency" HeaderText="Frequency" DataType="System.Int32" Groupable="false" />
            <telerik:GridTemplateColumn DataField="PathImg" HeaderText="Image" UniqueName="Image" Groupable="false">
                <ItemTemplate>
                    <asp:Image ImageUrl='<%# string.Format("~/{0}", Eval("PathImg"))%>' runat="server" Width="80%" />
                </ItemTemplate>
                <EditItemTemplate>
                    <mcf:ImageManager runat="server" ID="ImageManager" Path='<%# string.Format("/Uploads/{0}/Widgets/Shared", MainCloudFramework.Web.Helpers.MultiTenantsHelper.ApplicationId)%>'
                        LocalizationPath="~/App_GlobalResources/" Culture="en-US" />
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <Scrolling AllowScroll="true" EnableColumnClientFreeze="true" FrozenColumnsCount="3" UseStaticHeaders="true" />
        <ClientEvents OnPopUpShowing="PopUpShowing" />
    </ClientSettings>
</telerik:RadGrid>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="MoldID.Models.MoldDbContext" EntitySetName="Molds"
    OrderBy="it.Name" AutoGenerateWhereClause="true" />

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Warning">
    <Body>
        <asp:label id="messaggio" runat="server"/>
    </Body>
</mcf:PopUpDialog>