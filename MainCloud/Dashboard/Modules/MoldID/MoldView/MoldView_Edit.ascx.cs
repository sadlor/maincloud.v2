﻿using MainCloudFramework.UI.Modules;
using MoldID.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.MoldID.MoldView
{
    public partial class MoldView_Edit : WidgetControl<object>
    {
        public MoldView_Edit() : base(typeof(MoldView_Edit)) {}

        public string ApplicationId
        {
            get
            {
                return (Page as PageEdit).GetApplicationId();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMold.WhereParameters.Clear();
            //edsAsset.AutoGenerateWhereClause = true;
            edsMold.WhereParameters.Add("ApplicationId", ApplicationId);
        }

        protected void gridMold_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            using (MoldDbContext db = new MoldDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                Hashtable values = new Hashtable();
                editableItem.ExtractValues(values);

                var mold = new Mold();
                mold.ApplicationId = ApplicationId;
                mold.Id = (string)values["Id"];
                mold.Name = (string)values["Name"];
                mold.Code = (string)values["Code"];
                mold.Description = (string)values["Description"];
                mold.DtInstallation = Convert.ToDateTime(values["DtInstallation"]);
                mold.Brand = (string)values["Brand"];
                mold.IdNumber = (string)values["IdNumber"];
                mold.Model = (string)values["Model"];
                if (!string.IsNullOrEmpty((string)values["DtEndGuarantee"]))
                {
                    mold.DtEndGuarantee = Convert.ToDateTime(values["DtEndGuarantee"]);
                }
                if (!string.IsNullOrEmpty((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText))
                {
                    mold.Warning1 = Convert.ToInt32((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText);
                }
                if (!string.IsNullOrEmpty((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText))
                {
                    mold.Warning2 = Convert.ToInt32((editableItem["Warning2"].FindControl("txtWarning2") as RadNumericTextBox).DisplayText);
                }
                if (!string.IsNullOrEmpty((editableItem["Maximum"].FindControl("txtMaximum") as RadNumericTextBox).DisplayText))
                {
                    mold.Maximum = Convert.ToInt32((editableItem["Maximum"].FindControl("txtMaximum") as RadNumericTextBox).DisplayText);
                }
                //mold.Maximum = (int)values["Maximum"];
                if (values["Frequency"] != null)
                {
                    mold.Frequency = (int)values["Frequency"];
                }

                var img = editableItem["Image"].FindControl("ImageManager");
                string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                if (!string.IsNullOrEmpty(pathImg))
                {
                    mold.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                }

                db.Molds.Add(mold);
                db.SaveChanges();
            }
        }

        protected void gridMold_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            using (MoldDbContext db = new MoldDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                var moldId = (string)editableItem.GetDataKeyValue("Id");

                var mold = db.Molds.Where(m => m.Id == moldId).FirstOrDefault();
                if (mold != null)
                {
                    editableItem.UpdateValues(mold);

                    if (!string.IsNullOrEmpty((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText))
                    {
                        mold.Warning1 = Convert.ToInt32((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText);
                    }
                    if (!string.IsNullOrEmpty((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText))
                    {
                        mold.Warning2 = Convert.ToInt32((editableItem["Warning2"].FindControl("txtWarning2") as RadNumericTextBox).DisplayText);
                    }
                    if (!string.IsNullOrEmpty((editableItem["Maximum"].FindControl("txtMaximum") as RadNumericTextBox).DisplayText))
                    {
                        mold.Maximum = Convert.ToInt32((editableItem["Maximum"].FindControl("txtMaximum") as RadNumericTextBox).DisplayText);
                    }

                    var img = editableItem["Image"].FindControl("ImageManager");
                    string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                    if (!string.IsNullOrEmpty(pathImg))
                    {
                        mold.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                    }

                    db.SaveChanges();
                }
            }
        }

        protected void gridMold_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            using (MoldDbContext db = new MoldDbContext())
            {
                var moldId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");

                var mold = db.Molds.Where(m => m.Id == moldId).FirstOrDefault();
                if (mold != null)
                {
                    db.Molds.Remove(mold);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        messaggio.Text = "Impossibile rimuovere il record";
                        dlgWidgetConfig.OpenDialog();
                    }
                }
            }
        }
    }
}