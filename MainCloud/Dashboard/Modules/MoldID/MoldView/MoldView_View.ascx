﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoldView_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MoldID.MoldView.MoldView_View" %>

<link href="/Dashboard/Modules/MoldID/MoldView/Style.css" rel="stylesheet" type="text/css" />

<telerik:RadListView ID="MoldListView" DataSourceID="edsMold" runat="server" RenderMode="Lightweight"
    ItemPlaceholderID="MoldsContainer" DataKeyNames="Id" AllowPaging="true" Width="100%">
    <LayoutTemplate>
        <!-- Set the id of the wrapping container to match the CLIENT ID of the RadListView control to display the ajax loading panel
    In case the listview is embedded in another server control, you will need to append the id of that server control -->
        <fieldset id="FiledSet1" class="mainFieldset">
            <legend>Molds</legend>
            <table>
                <tr>
                    <td>
                        <telerik:RadDataPager RenderMode="Lightweight" ID="RadDataPager1" runat="server" PagedControlID="MoldListView"
                            PageSize="5" CssClass="pagerStyle">
                            <Fields>
                                <telerik:RadDataPagerButtonField FieldType="FirstPrev"></telerik:RadDataPagerButtonField>
                                <telerik:RadDataPagerButtonField FieldType="Numeric" PageButtonCount="4"></telerik:RadDataPagerButtonField>
                                <telerik:RadDataPagerButtonField FieldType="NextLast"></telerik:RadDataPagerButtonField>
                                <telerik:RadDataPagerPageSizeField PageSizeComboWidth="60" PageSizeText="Page size: "></telerik:RadDataPagerPageSizeField>
                                <telerik:RadDataPagerGoToPageField CurrentPageText="Page: " TotalPageText="of" SubmitButtonText="Go"></telerik:RadDataPagerGoToPageField>
                            </Fields>
                        </telerik:RadDataPager>
                    </td>
                </tr>
            </table>
            <div class="RadListView RadListView_<%# Container.Skin %>">
                <asp:PlaceHolder ID="MoldsContainer" runat="server"></asp:PlaceHolder>
            </div>
            <div class="clearFix">
            </div>
            <%--<table class="commandTable">
                <tr>
                    <td class="insertCell">
                        <telerik:RadButton RenderMode="Lightweight" ID="btnInitInsert" runat="server" Text="Add New Customer" OnClick="btnInitInsert_Click" CausesValidation="false"></telerik:RadButton>
                    </td>
                    <td class="sortCell">
                        <asp:Label ID="lblSort1" runat="server" AssociatedControlID="ddListSort" Text="Sort by:"
                            CssClass="sortLabel"></asp:Label>
                        <telerik:RadComboBox RenderMode="Lightweight" ID="ddListSort" Width="185px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddListSort_SelectedIndexChanged" CausesValidation="false">
                            <Items>
                                <telerik:RadComboBoxItem Text="-Select field to sort-" Value=""></telerik:RadComboBoxItem>
                                <telerik:RadComboBoxItem Text="Company name" Value="CompanyName"></telerik:RadComboBoxItem>
                                <telerik:RadComboBoxItem Text="Contact name" Value="ContactName"></telerik:RadComboBoxItem>
                                <telerik:RadComboBoxItem Text="Title" Value="ContactTitle"></telerik:RadComboBoxItem>
                                <telerik:RadComboBoxItem Text="City" Value="City"></telerik:RadComboBoxItem>
                                <telerik:RadComboBoxItem Text="Country" Value="Country"></telerik:RadComboBoxItem>
                                <telerik:RadComboBoxItem Text="Clear sort" Value="ClearSort"></telerik:RadComboBoxItem>
                            </Items>
                        </telerik:RadComboBox>
                        <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rblSort"
                            runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblSort_SelectedIndexChanged">
                            <asp:ListItem Text="Ascending" Value="ASC" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Descending" Value="DESC"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>--%>
        </fieldset>
    </LayoutTemplate>
    <ItemTemplate>
        <!--The widths/heights of the fieldset/outer tables in the item/edit/insert templates should match to avoid wrapping or visual discrepancies
        in the tiles layout-->
        <fieldset class="fieldset">
            <legend>Mold name:
                <%# Eval("Name") %></legend>
            
            <telerik:RadButton ID="btnRedirect" Text="View RealTime" runat="server" OnClick="btnRedirect_Click" Value='<%# Eval("Id") %>' />

            <table class="dataTable">
                <tr class="rlvI">
                    <td>
                        <table class="itemTable">
                            <tr>
                                <td>
                                    <table class="innerItemTable">
                                        <tr>
                                            <td class="itemCellLabel">Name:
                                            </td>
                                            <td class="itemCellInfo">
                                                <%#Eval("Name")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Code:
                                            </td>
                                            <td>
                                                <%#Eval("Code")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Description:
                                            </td>
                                            <td>
                                                <%# Eval("Description")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Brand:
                                            </td>
                                            <td>
                                                <%# Eval("Brand")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Model:
                                            </td>
                                            <td>
                                                <%#Eval("Model")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Builder:
                                            </td>
                                            <td>
                                                <%#Eval("Builder")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Frequency:
                                            </td>
                                            <td>
                                                <%#Eval("Frequency")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Warning1:
                                            </td>
                                            <td>
                                                <%#Eval("Warning1")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Warning2:
                                            </td>
                                            <td>
                                                <%#Eval("Warning2")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Maximum:
                                            </td>
                                            <td>
                                                <%#Eval("Maximum")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Last Maintenance:
                                            </td>
                                            <td>
                                                <%#Eval("LastMaintenance")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Next Maintenance:
                                            </td>
                                            <td>
                                                <%#Eval("NextMaintenance")%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="image">
                                    <asp:Image ImageUrl='<%# string.Format("~/{0}", Eval("PathImg"))%>' runat="server" Width="100%" AlternateText="Mold Image"
                                        ToolTip="Mold Image" /> <%--Width="90px" Height="110px"--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--<tr>
                    <td>
                        <telerik:RadButton RenderMode="Lightweight" ID="btnEdit" CssClass="btnEdit" runat="server" Text="Edit" CommandName="Edit" CausesValidation="false"></telerik:RadButton>
                        <telerik:RadButton RenderMode="Lightweight" ID="btnDelete" CssClass="btnDelete" runat="server" Text="Delete" CommandName="Delete" CausesValidation="false"></telerik:RadButton>
                    </td>
                </tr>--%>
            </table>
        </fieldset>
    </ItemTemplate>
    <EditItemTemplate>
        <!--The widths/heights of the fieldset/outer tables in the item/edit/insert templates should match to avoid wrapping or visual discrepancies
        in the tiles layout-->
        <%--<fieldset class="fieldset editFieldSet">
            <legend>
                <asp:Label runat="server" AssociatedControlID="txtBoxCompanyName">Company name:</asp:Label>
                <asp:TextBox ID="txtBoxCompanyName" runat="server" Text='<%#Bind("CompanyName")%>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvCompanyName" runat="server" ControlToValidate="txtBoxCompanyName"
                    ErrorMessage="Please enter company name" Display="Dynamic"></asp:RequiredFieldValidator>
            </legend>
            <table class="dataEditTable">
                <tr>
                    <td>
                        <table class="editTable">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="editItemCellLabel">
                                                <asp:Label runat="server" AssociatedControlID="txtBoxName">Name:</asp:Label>
                                            </td>
                                            <td class="editItemCellInfo">
                                                <asp:TextBox ID="txtBoxName" runat="server" Text='<%#Bind("ContactName")%>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rvName" runat="server" ControlToValidate="txtBoxName"
                                                    ErrorMessage="Please enter name" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" AssociatedControlID="txtBoxTitle">Title:</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBoxTitle" runat="server" Text='<%#Bind("ContactTitle")%>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rvTitle" runat="server" ControlToValidate="txtBoxTitle"
                                                    ErrorMessage="Please enter title" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" AssociatedControlID="txtBoxCity">City:</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBoxCity" runat="server" Text='<%#Bind("City")%>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rvCity" runat="server" ControlToValidate="txtBoxCity"
                                                    ErrorMessage="Please enter city" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" AssociatedControlID="txtBoxCountry">Country:</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBoxCountry" runat="server" Text='<%#Bind("Country")%>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rvCountry" runat="server" ControlToValidate="txtBoxCountry"
                                                    ErrorMessage="Please enter country" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" AssociatedControlID="txtBoxPhone">Phone:</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBoxPhone" runat="server" Text='<%#Bind("Phone")%>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rvPhone" runat="server" ControlToValidate="txtBoxPhone"
                                                    ErrorMessage="Please enter phone" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="editImage">
                                    <telerik:RadBinaryImage ID="RadBinaryImage1" runat="server" AlternateText="Contact Photo"
                                        ToolTip="Contact Photo" Width="90px" Height="110px" ResizeMode="Fit" DataValue='<%# Eval("Photo") == DBNull.Value? new System.Byte[0]: Eval("Photo") %>'></telerik:RadBinaryImage>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="asyncUpload">
                            <telerik:RadAsyncUpload RenderMode="Lightweight" ID="ruCustomerImage" runat="server" AllowedFileExtensions=".jpg,.jpeg,.gif,.png"
                                MaxFileSize="1000000" ControlObjectsVisibility="None" InputSize="12" Width="130px"
                                CssClass="radUploadStyle" OnFileUploaded="ruCustomerImage_FileUploaded">
                            </telerik:RadAsyncUpload>
                        </div>
                        <div class="clearFix">
                        </div>
                        <div class="commandButton">
                            <telerik:RadButton RenderMode="Lightweight" ID="btnUpdate" runat="server" Text="Update" CommandName="Update"></telerik:RadButton>
                            <telerik:RadButton RenderMode="Lightweight" ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CausesValidation="false"></telerik:RadButton>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>--%>
    </EditItemTemplate>
    <InsertItemTemplate>
        <!--The widths/heights of the fieldset/outer tables in the item/edit/insert templates should match to avoid wrapping or visual discrepancies
        in the tiles layout-->
        <%--<fieldset class="fieldset fieldsetInsert">
            <legend>
                <asp:Label runat="server" AssociatedControlID="txtBoxCompanyName">Company name:</asp:Label>
                <asp:TextBox ID="txtBoxCompanyName" runat="server" Text='<%# Bind("CompanyName") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvCompanyName" runat="server" ControlToValidate="txtBoxCompanyName"
                    ErrorMessage="Please enter company name" Display="Dynamic"></asp:RequiredFieldValidator>
            </legend>
            <table class="dataInsertTable">
                <tr>
                    <td>
                        <table class="editTable">
                            <tr>
                                <td class="editItemCellLabel">
                                    <asp:Label runat="server" AssociatedControlID="txtBoxName">Name:</asp:Label>
                                </td>
                                <td class="editItemCellInfo">
                                    <asp:TextBox ID="txtBoxName" runat="server" Text='<%# Bind("ContactName") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvName" runat="server" ControlToValidate="txtBoxName"
                                        ErrorMessage="Please enter name" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" AssociatedControlID="txtBoxTitle">Title:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBoxTitle" runat="server" Text='<%# Bind("ContactTitle") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvTitle" runat="server" ControlToValidate="txtBoxTitle"
                                        ErrorMessage="Please enter title" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" AssociatedControlID="txtBoxCity">City:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBoxCity" runat="server" Text='<%# Bind("City") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvCity" runat="server" ControlToValidate="txtBoxCity"
                                        ErrorMessage="Please enter city" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" AssociatedControlID="txtBoxCountry">Country:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBoxCountry" runat="server" Text='<%# Bind("Country") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvCountry" runat="server" ControlToValidate="txtBoxCountry"
                                        ErrorMessage="Please enter country" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" AssociatedControlID="txtBoxPhone">Phone:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBoxPhone" runat="server" Text='<%# Bind("Phone") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvPhone" runat="server" ControlToValidate="txtBoxPhone"
                                        ErrorMessage="Please enter phone" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="editImage">
                        <telerik:RadBinaryImage ID="RadBinaryImage1" runat="server" AlternateText="No Photo"
                            ToolTip="No Photo" Width="90px" Height="110px" ResizeMode="Fit" ImageUrl="~/ListView/Examples/Overview/images/EmtpyCategoryImage.jpg"></telerik:RadBinaryImage>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="insertAsyncUpload">
                            <telerik:RadAsyncUpload RenderMode="Lightweight" ID="ruCustomerImage" runat="server" AllowedFileExtensions=".jpg,.jpeg,.gif,.png"
                                MaxFileSize="1000000" ControlObjectsVisibility="None" InputSize="12" Width="160px"
                                CssClass="radUploadStyle">
                            </telerik:RadAsyncUpload>
                        </div>
                        <div class="clearFix">
                        </div>
                        <div class="insertCommandButton">
                            <telerik:RadButton RenderMode="Lightweight" ID="btnPerformInsert" runat="server" Text="Insert" CommandName="PerformInsert"></telerik:RadButton>
                            <telerik:RadButton RenderMode="Lightweight" ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CausesValidation="false"></telerik:RadButton>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>--%>
    </InsertItemTemplate>
    <EmptyDataTemplate>
        <fieldset class="noRecordsFieldset">
            <legend>Molds</legend>No records for molds available.
        </fieldset>
    </EmptyDataTemplate>
</telerik:RadListView>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="MoldID.Models.MoldDbContext" EntitySetName="Molds"
    OrderBy="it.Name" AutoGenerateWhereClause="true" />