﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Services;
using MoldID.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.MoldID.Summary
{
    public partial class Summary_View : DataWidget<Summary_View>
    {
        public Summary_View() : base(typeof(Summary_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (dtPickStart.SelectedDate != null && dtPickEnd.SelectedDate != null)
            {
                string moldId = ddlSelectMold.SelectedValue;
                DateTime start = dtPickStart.SelectedDate.Value;
                DateTime end = dtPickEnd.SelectedDate.Value;

                CountingService CS = new CountingService();
                TimeSpan totalTime = CS.TotalDuration(start, end, moldId);
                TimeSpan prodTime = CS.TotalProductionTime(start, end, moldId);
                TimeSpan stopTime = CS.TotalStopTime(start, end, moldId);

                TotTime.InnerText = string.Format("{0}:{1}:{2}", (int)totalTime.TotalHours, totalTime.Minutes, totalTime.Seconds);
                TotProdTime.InnerText = string.Format("{0}:{1}:{2}", (int)prodTime.TotalHours, prodTime.Minutes, prodTime.Seconds);
                TotStopTime.InnerText = string.Format("{0}:{1}:{2}", (int)stopTime.TotalHours, stopTime.Minutes, prodTime.Seconds);

                var pieChartDataSource = new List<ChartItem>();
                pieChartDataSource.Add(new ChartItem("Prod time", Math.Round(prodTime.TotalMinutes / totalTime.TotalMinutes * 100, 2), false));
                pieChartDataSource.Add(new ChartItem("Stop time", Math.Round(stopTime.TotalMinutes / totalTime.TotalMinutes * 100, 2), false));

                PieChartTime.DataSource = pieChartDataSource;
                PieChartTime.DataBind();

                MoldRepository repos = new MoldRepository();

                decimal partialCounting = CS.PartialCountingInPeriod(start, end, moldId);
                decimal pezziOra = Math.Round(partialCounting / (int)prodTime.TotalHours, 2);
                int frequency = repos.ReadAll(x => x.Id == moldId && x.ApplicationId == MultiTenantsHelper.ApplicationId).First().Frequency;

                PartialCountingField.InnerText = partialCounting.ToString();
                FrequencyField.InnerText = pezziOra.ToString();
                FrequencyTeoricaField.InnerText = frequency.ToString() + ",00";

                EfficiencyField.InnerText = Math.Round(pezziOra / frequency * 100, 2).ToString() + "%";

                MeterEfficiency.Pointer.Value = Math.Round(pezziOra / frequency * 100, 2);

                //PartialCountingField2.InnerText = partialCounting.ToString();
                decimal pezziOraSuTot = Math.Round(partialCounting / (int)totalTime.TotalHours, 2); ;
                FrequencyYeld.InnerText = pezziOraSuTot.ToString();
                MeterYeld.Pointer.Value = Math.Round(pezziOraSuTot / frequency * 100, 2);
                Yeld.InnerText = Math.Round(pezziOraSuTot / frequency * 100, 2).ToString() + "%";
            }
        }
    }

    public class ChartItem
    {
        public ChartItem(string name, double marketShare, bool isExploded)
        {
            _name = name;
            _marketShare = marketShare;
            _isExploded = isExploded;
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private double _marketShare;
        public double MarketShare
        {
            get { return _marketShare; }
            set { _marketShare = value; }
        }
        private bool _isExploded;
        public bool IsExploded
        {
            get { return _isExploded; }
            set { _isExploded = value; }
        }
    }
}