﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Summary_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MoldID.Summary.Summary_View" %>


<div class="row" style="width:100%;">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div class="row">
            <asp:Label Text="Mold" runat="server" AssociatedControlID="ddlSelectMold" Font-Bold="true" Font-Size="Large" />
            <br />
            <telerik:RadComboBox ID="ddlSelectMold" runat="server" DataSourceID="edsMold" DataValueField="Id" DataTextField="Name" />
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <div class="row" style="width:100%;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickStart" runat="server" ShowPopupOnFocus="true" Width="100%" style="min-width:200px;max-width:220px;"
                    DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" DateInput-Label="Data inizio:" />
            </div>
        </div>
        <div class="row" style="width:100%;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickEnd" runat="server" ShowPopupOnFocus="true" Width="100%" style="min-width:200px;max-width:220px;"
                    DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" DateInput-Label="Data fine:&nbsp;&nbsp;" />
            </div>
        </div>

        <br />

        <div class="row" style="width:100%;">
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                <asp:LinkButton ID="btnConfirm" Text="Confirm <i class='fa fa-check-circle'></i>" runat="server" OnClick="btnConfirm_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;" />
            </div>
        </div>
    </div>
</div>


<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnConfirm" EventName="Click" />
    </Triggers>
    <ContentTemplate>
        <div class="panel panel-default">
            <table class="table table-bordered table-condensed info" style="width:100%;">
                <tr>
                    <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <table class="table table-bordered table-condensed info" style="width:100%;">
                            <caption>Analisi tempi</caption>
                            <tr>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="background-color:#428bca; color:white;"><b>∑ Time</b></td>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" runat="server" id="TotTime" style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="background-color:#428bca; color:white;"><b>∑ Production time</b></td>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" runat="server" id="TotProdTime" style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="background-color:#428bca; color:white;"><b>∑ Stop time</b></td>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" runat="server" id="TotStopTime" style="text-align: right;"></td>
                            </tr>
                        </table>
                    </td>
                    <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <telerik:RadHtmlChart runat="server" ID="PieChartTime" Width="200px" Height="200px" Transitions="true" Skin="Silk" OnClientSeriesClicked="OnClientSeriesClicked">
                            <ChartTitle Text=''>
                                <Appearance Align="Center" Position="Top">
                                </Appearance>
                            </ChartTitle>
                            <Legend>
                                <Appearance Position="Top" Visible="true">
                                </Appearance>
                            </Legend>
                            <PlotArea>
                                <Series>
                                    <telerik:PieSeries DataFieldY="MarketShare" NameField="Name" ExplodeField="IsExploded" StartAngle="90">
                                        <LabelsAppearance Position="Center" DataFormatString="{0} %">
                                        </LabelsAppearance>
                                        <TooltipsAppearance Color="White" DataFormatString="{0} %"></TooltipsAppearance>
                                    </telerik:PieSeries>
                                </Series>
                                <YAxis>
                                </YAxis>
                            </PlotArea>
                        </telerik:RadHtmlChart>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <table class="table table-bordered table-condensed info" style="width:100%;">
                            <caption>Analisi efficienza su ore produttive</caption>
                            <tr>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="background-color:#428bca; color:white;"><b>Q.tà prodotta</b></td>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" runat="server" id="PartialCountingField" style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="background-color:#428bca; color:white;"><b>Cadenza oraria teorica (pezzi/ora)</b></td>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" runat="server" id="FrequencyTeoricaField" style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="background-color:#428bca; color:white;"><b>Cadenza oraria media (pezzi/ora)</b></td>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" runat="server" id="FrequencyField" style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="background-color:#428bca; color:white;"><b>Efficienza</b></td>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" runat="server" id="EfficiencyField" style="text-align: right;"></td>
                            </tr>
                        </table>
                    </td>
                    <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <telerik:RadRadialGauge runat="server" ID="MeterEfficiency" Width="100%">
                            <Pointer Value="0">
                                <Cap Size="0.1" />
                            </Pointer>
                            <Scale Min="0" Max="100" MajorUnit="20">
                                <Labels Format="{0}" />
                                <Ranges>
                                    <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                    <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                    <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                    <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                </Ranges>
                            </Scale>
                        </telerik:RadRadialGauge>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <table class="table table-bordered table-condensed info" style="width:100%;">
                            <caption>Analisi resa su ore totali</caption>
                            <%--<tr>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="background-color:#428bca; color:white;"><b>Q.tà prodotta</b></td>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" runat="server" id="PartialCountingField2" style="text-align: right;"></td>
                            </tr>--%>
                            <tr>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="background-color:#428bca; color:white;"><b>Cadenza oraria effettiva</b></td>
                                <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" runat="server" id="FrequencyYeld" style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td style="background-color:#428bca; color:white;"><b>Resa oraria</b></td>
                                <td runat="server" id="Yeld" style="text-align: right;"></td>
                            </tr>
                        </table>
                    </td>
                    <td class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <telerik:RadRadialGauge runat="server" ID="MeterYeld" Width="100%">
                            <Pointer Value="0">
                                <Cap Size="0.1" />
                            </Pointer>
                            <Scale Min="0" Max="100" MajorUnit="20">
                                <Labels Format="{0}" />
                                <Ranges>
                                    <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                    <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                    <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                    <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                </Ranges>
                            </Scale>
                        </telerik:RadRadialGauge>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="MoldID.Models.MoldDbContext" EntitySetName="Molds"
    OrderBy="it.Name" AutoGenerateWhereClause="true" />

<script type="text/javascript">
    function OnClientSeriesClicked(sender, args) {
        var theDataItem = args.get_dataItem();
        theDataItem.IsExploded = !theDataItem.IsExploded;
        sender.repaint();
    }
</script>