﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SmartLightView_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MoldID.SmartLightView.SmartLightView_View" %>

<style type="text/css">
    div.RadProgressBar .rpbStateSelected,
    div.RadProgressBar .rpbStateSelected:hover,
    div.RadProgressBar .rpbStateSelected:link,
    div.RadProgressBar .rpbStateSelected:visited
    {
        background-color: green;
    }

    /*div.RadProgressBar.rpbVertical {
        height: 280px;
    }*/

    #ValueProgressBar.RadProgressBar>.rpbChunksWrapper{
        height: 111%;
    }
</style>

<div>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <telerik:RadProgressBar runat="server" ID="ValueProgressBar" BarType="Chunk" Value="80" MinValue="0" MaxValue="100"
                    Orientation="Vertical" Height="50px" ShowLabel="false" />
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadProgressBar runat="server" ID="ChunkProgressBar" BarType="Chunk" Value="2" MinValue="0" MaxValue="5"
                    Orientation="Vertical" Height="200px" ShowLabel="false" />
            </td>
        </tr>
    </table>
</div>