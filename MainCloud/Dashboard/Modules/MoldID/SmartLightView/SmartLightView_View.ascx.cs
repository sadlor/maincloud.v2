﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.MoldID.SmartLightView
{
    public partial class SmartLightView_View : DataWidget<SmartLightView_View>
    {
        public SmartLightView_View() : base(typeof(SmartLightView_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}