﻿using MainCloudFramework.Web.Multitenants;
using MoldID.Core;
using MoldID.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace MainCloud.Dashboard.Modules.MoldID.Counter
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MoldCounter
    {
        // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
        // To create an operation that returns XML,
        //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     and include the following line in the operation body:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        [WebGet]
        public string GetValue()
        {
            var tenantName = HttpContext.Current.Request.QueryString["MultiTenant"];
            HttpMultitenantsSessionState SessionMultitenant = HttpContext.Current.Session.SessionMultitenant(tenantName);
            if (SessionMultitenant.ContainsKey(MoldIDConstants.SELECTED_MOLD_ID))
            {
                string moldId = (string)SessionMultitenant[MoldIDConstants.SELECTED_MOLD_ID];
                MoldDataService MDS = new MoldDataService();

                Dictionary<string, string> json = new Dictionary<string, string>();
                json.Add("ProgressiveCounter", MDS.GetTotalShots(moldId).ToString());
                json.Add("DailyCounter", MDS.GetDailyCurrentShots(moldId).ToString());
                json.Add("LastUpdate", MDS.GetTotalShotsLastUpdate(moldId).ToString("dd/MM/yyyy HH:mm"));
                
                return JsonConvert.SerializeObject(json);
            }
            else
            {
                return "";
            }
        }
    }
}
