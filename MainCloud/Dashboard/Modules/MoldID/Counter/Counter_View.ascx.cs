﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MoldID.Core;
using MoldID.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.MoldID.Counter
{
    public partial class Counter_View : DataWidget<Counter_View>
    {
        public Counter_View() : base (typeof(Counter_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}