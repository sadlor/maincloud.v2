﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Counter_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MoldID.Counter.Counter_View" %>

<asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
    <%--<Triggers>
        <asp:AsyncPostBackTrigger ControlID="UpdateTimer" EventName="Tick" />
    </Triggers>--%>
    <ContentTemplate>
        <%--<asp:Timer ID="UpdateTimer" OnTick="UpdateTimer_Tick" runat="server" Interval="5000">
        </asp:Timer>--%>

        <telerik:RadNumericTextBox runat="server" ID="ProgressiveCounter" Width="100%" Label="Progressive Counter:" LabelWidth="55%"
            style="background-color:rgb(145, 220, 243);text-align:right;" ReadOnly="true" />

        <telerik:RadNumericTextBox runat="server" ID="DailyCounter" Width="100%" Label="Daily Counter:" LabelWidth="55%"
            style="background-color:rgb(145, 220, 243);text-align:right;" ReadOnly="true" />

        <telerik:RadTextBox runat="server" ID="txtLastUpdate" Label="Last Update:" LabelWidth="55%"
            Width="100%" ReadOnly="true" style="text-align:right;" />
    </ContentTemplate>
</asp:UpdatePanel>

<%--<script type="text/javascript">

$(function () {
        ResfreshUpdatePanelCounter();
        setInterval(function () {
            ResfreshUpdatePanelCounter();
        }, 5000);
    });

function ResfreshUpdatePanelCounter() {
    var updatePanel = '<%=UpdatePanel1.ClientID %>';
    if (updatePanel != null)
    {
        __doPostBack(updatePanel, '');
    }
}

 </script>--%>

<script type="text/javascript">
$(function () {
        UpdateMoldCounter();
        setInterval(function () {
            UpdateMoldCounter();
        }, 5000);
    });

function UpdateMoldCounter() {
    var request = $.ajax({
        url: '<%= MainCloudFramework.Web.Helpers.MultiTenantsHelper.MountAndResolveMultiTenantUrl("/Dashboard/Modules/MoldID/Counter/MoldCounter.svc/GetValue") %>',
        method: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json'
    });

    request.done(function (response) {  //string
        if (response.d != "") {
            var objJSON = eval("(function(){return " + response.d + ";})()");

            $find("<%=ProgressiveCounter.ClientID %>").set_value(objJSON.ProgressiveCounter);
            $find("<%=DailyCounter.ClientID %>").set_value(objJSON.DailyCounter);
            $find("<%=txtLastUpdate.ClientID %>").set_value(objJSON.LastUpdate);
        }
    });

    request.fail(function (jqXHR, textStatus) {
        //alert("Request failed: " + textStatus);
    });
}

 </script>