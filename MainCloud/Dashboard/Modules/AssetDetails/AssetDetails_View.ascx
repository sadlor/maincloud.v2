﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssetDetails_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.AssetDetails.AssetDetails_View" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <asp:Image runat="server" ID="AssetImage" Width="100%" />
        
        <telerik:RadDockZone ID="zoneAssetRegistry" runat="server">
            <telerik:RadDock RenderMode="Lightweight" ID="dockAssetRegistry" runat="server" Title="Anagrafica"
                EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Code: " runat="server" AssociatedControlID="txtCode" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtCode" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Description: " runat="server" AssociatedControlID="txtDescr" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtDescr" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="SerialCode: " runat="server" AssociatedControlID="txtSerial" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtSerial" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Brand: " runat="server" AssociatedControlID="txtBrand" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtBrand" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Model: " runat="server" AssociatedControlID="txtModel" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtModel" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Installation Date: " runat="server" AssociatedControlID="txtDtI" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtDtI" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="End Guarantee Date: " runat="server" AssociatedControlID="txtDtE" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtDtE" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Department: " runat="server" AssociatedControlID="txtDepartment" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtDepartment" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="AnalyzerID: " runat="server" AssociatedControlID="txtAnalyzer" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtAnalyzer" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Asset Group: " runat="server" AssociatedControlID="txtGroup" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtGroup" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadDock>
        </telerik:RadDockZone>

        <telerik:RadDockZone ID="zonePlant" runat="server">
            <telerik:RadDock RenderMode="Lightweight" ID="dockPlant" runat="server" Title="Anagrafica"
                EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Code: " runat="server" AssociatedControlID="txtPlantCode" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtPlantCode" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Description: " runat="server" AssociatedControlID="txtPlantDescr" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtPlantDescr" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="POD: " runat="server" AssociatedControlID="txtPOD" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtPOD" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Voltage: " runat="server" AssociatedControlID="txtVoltage" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtVoltage" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    
                        <div class="col-xs-6">
                            <asp:Label Text="AnalyzerID: " runat="server" AssociatedControlID="txtPlantAnalyzer" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtPlantAnalyzer" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadDock>
        </telerik:RadDockZone>

        <telerik:RadDockZone ID="zoneDepartment" runat="server">
            <telerik:RadDock RenderMode="Lightweight" ID="dockDepartment" runat="server" Title="Anagrafica"
                EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Code: " runat="server" AssociatedControlID="txtDepartmentCode" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtDepartmentCode" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Description: " runat="server" AssociatedControlID="txtDepartmentDescr" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtDepartmentDescr" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="Plant: " runat="server" AssociatedControlID="txtPlant" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtPlant" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:Label Text="AnalyzerID: " runat="server" AssociatedControlID="txtDepartmentAnalyzer" />
                        </div>
                        <div class="col-xs-6">
                            <asp:TextBox ID="txtDepartmentAnalyzer" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadDock>
        </telerik:RadDockZone>

    </ContentTemplate>
</asp:UpdatePanel>