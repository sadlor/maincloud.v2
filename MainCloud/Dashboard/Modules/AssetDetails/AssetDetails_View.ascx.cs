﻿using AssetManagement.Core;
using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule.Core;
using MainCloudFramework.Services;
using MainCloudFramework.UI.Containers;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web;
using MainCloudFramework.Web.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.AssetDetails
{
    public partial class AssetDetails_View : WidgetControl<object>
    {
        public AssetDetails_View() : base(typeof(AssetDetails_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["__EVENTARGUMENT"]) && Request["__EVENTARGUMENT"].Contains("Id") && Request["__EVENTARGUMENT"].Contains("Type"))
            {
                SelectedContext parameter = JsonConvert.DeserializeObject<SelectedContext>(Request["__EVENTARGUMENT"]);   //da javascript
                try
                {
                    string pathImg = "";
                    switch (parameter.Type)
                    {
                        case AssetManagementEnum.AnalyzerContext.Asset:
                            AssetService AS = new AssetService();
                            Asset asset = AS.GetAssedById(parameter.Id);
                            pathImg = asset.PathImg;
                            CreateGridAsset(asset);
                            break;
                        case AssetManagementEnum.AnalyzerContext.Department:
                            DepartmentService DS = new DepartmentService();
                            Department department = DS.GetDepartmentById(parameter.Id);
                            pathImg = department.PathImg;
                            CreateGridDepartment(department);
                            break;
                        case AssetManagementEnum.AnalyzerContext.Plant:
                            PlantService PS = new PlantService();
                            Plant plant = PS.GetPlantById(parameter.Id);
                            pathImg = plant.PathImg;
                            CreateGridPlant(plant);
                            break;
                    }
                    this.AssetImage.ImageUrl = "~/" + pathImg;
                }
                catch (Exception ex)
                {
                    //non c'è immagine
                }
            }
            else
            {
                zoneAssetRegistry.Visible = false;
                zoneDepartment.Visible = false;
                zonePlant.Visible = false;
            }
        }

        protected void CreateGridAsset(Asset asset)
        {
            if (asset.Analyzer == null)
            {
                txtAnalyzer.Text = null;
            }
            else
            {
                txtAnalyzer.Text = asset.Analyzer.Description;
            }
            txtBrand.Text = asset.Brand;
            txtCode.Text = asset.Code;
            if (asset.Department == null)
            {
                txtDepartment.Text = "";
            }
            else
            {
                txtDepartment.Text = asset.Department.Description;
            }
            txtDescr.Text = asset.Description;
            txtDtE.Text = asset.DtEndGuarantee.ToString();
            txtDtI.Text = asset.DtInstallation.ToString();
            if (asset.AssetGroup == null)
            {
                txtGroup.Text = "";
            }
            else
            {
                txtGroup.Text = asset.AssetGroup.Description;
            }
            txtModel.Text = asset.Model;
            txtSerial.Text = asset.IdNumber;

            zoneAssetRegistry.Visible = true;
            zonePlant.Visible = false;
            zoneDepartment.Visible = false;
        }

        protected void CreateGridPlant(Plant plant)
        {
            if (plant.Analyzer == null)
            {
                txtPlantAnalyzer.Text = null;
            }
            else
            {
                txtPlantAnalyzer.Text = plant.Analyzer.Description;
            }
            txtPlantCode.Text = plant.Code;
            txtPlantDescr.Text = plant.Description;
            txtPOD.Text = plant.POD;
            txtVoltage.Text = plant.Voltage;

            zonePlant.Visible = true;
            zoneAssetRegistry.Visible = false;
            zoneDepartment.Visible = false;
        }

        protected void CreateGridDepartment(Department department)
        {
            if (department.Analyzer == null)
            {
                txtDepartmentAnalyzer.Text = null;
            }
            else
            {
                txtDepartmentAnalyzer.Text = department.Analyzer.Description;
            }
            txtDepartmentCode.Text = department.Code;
            if (department.Plant == null)
            {
                txtPlant.Text = "";
            }
            else
            {
                txtPlant.Text = department.Plant.Description;
            }
            txtDepartmentDescr.Text = department.Description;

            zoneDepartment.Visible = true;
            zoneAssetRegistry.Visible = false;
            zonePlant.Visible = false;
        }
    }
}