﻿using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule.ConsumptionStorage;
using EnergyModule.Models;
using EnergyModule.Services;
using MainCloud.Dashboard.Modules.Energy.Totalizer;
using MainCloudFramework.UI.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.AreaChart
{
    public partial class AreaChart_View : WidgetControl<object>
    {
        public AreaChart_View() : base(typeof(AreaChart_View)){}

        protected List<Plant> plantList = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            CreateTableDataSourceCost(new DateTime(2016, 01, 01), new DateTime(2016, 09, 30));
        }

        private void CreateTableDataSourceCost(DateTime start, DateTime end)
        {
            //string paramName = EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString();

            if (plantList == null)
            {
                PlantService plantService = new PlantService();
                plantList = plantService.PlantList();

                var plants = new SortedDictionary<string, TotalizerItemPlant>();
                var departments = new SortedDictionary<string, TotalizerItemDepartment>();
                var assets = new SortedDictionary<string, TotalizerItemAsset>();

                var energyDataSource = new List<TotalizerItemPlant>();

                //EnergyDataService energyDataService = new EnergyDataService();
                ConsumptionStorage storage = new ConsumptionStorage();

                foreach (Plant plant in plantList)
                {
                    MonthlyConsumption plantCost = null;
                    if (!string.IsNullOrEmpty(plant.AnalyzerId))
                    {
                        plantCost = storage.GetTotalizerCostConsumption(plant.AnalyzerId, start, end);

                    }
                    if (!plants.ContainsKey(plant.Id))
                    {
                        plants[plant.Id] = new TotalizerItemPlant()
                        {
                            Data = plant,
                            Departments = new List<TotalizerItemDepartment>(),
                            kWhCost = plantCost
                        };
                    }
                    foreach (Department dep in plant.Department.OrderBy(x => x.Description))
                    {
                        MonthlyConsumption depCost = null;
                        if (!string.IsNullOrEmpty(dep.AnalyzerId))
                        {
                            depCost = storage.GetTotalizerCostConsumption(dep.AnalyzerId, start, end);
                        }
                        if (!departments.ContainsKey(dep.Id))
                        {
                            departments[dep.Id] = new TotalizerItemDepartment()
                            {
                                Data = dep,
                                Assets = new List<TotalizerItemAsset>(),
                                kWhCost = depCost
                            };
                        }
                        foreach (Asset asset in dep.Asset.OrderBy(x => x.Description))
                        {
                            MonthlyConsumption assetCost = null;
                            if (!string.IsNullOrEmpty(asset.AnalyzerId))
                            {
                                assetCost = storage.GetTotalizerCostConsumption(asset.AnalyzerId, start, end);
                            }
                            if (!assets.ContainsKey(asset.Id))
                            {
                                assets[asset.Id] = new TotalizerItemAsset()
                                {
                                    Data = asset,
                                    kWhCost = assetCost
                                };
                                departments[dep.Id].Assets.Add(assets[asset.Id]);
                            }
                        }
                        plants[plant.Id].Departments.Add(departments[dep.Id]);
                    }
                    energyDataSource.Add(plants[plant.Id]);
                }
                
                TableDataSource = energyDataSource;
                grid.DataSource = TableDataSource;
                //grid.DataBind();
            }
        }

        public List<TotalizerItemPlant> TableDataSource { get; set; }

        protected void grid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            grid.DataSource = TableDataSource;
        }

        protected void grid_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem item = e.DetailTableView.ParentItem;

            string plantId = item.GetDataKeyValue("Data.Id").ToString();

            e.DetailTableView.DataSource = TableDataSource
                .First(x => x.Data.Id == plantId)
                .Departments;
        }

        protected void gridDep_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            string id = ((Telerik.Web.UI.GridEditableItem)(((Telerik.Web.UI.GridNestedViewItem)((sender as RadGrid).Parent.BindingContainer)).ParentItem)).KeyValues.Replace("{Data.Id:\"", "").Replace("\"}", "");
            (sender as RadGrid).DataSource = TableDataSource.First(x => x.Data.Id == id).Departments;
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e is GridExpandCommandEventArgs)
            {
                //e.Item.Expanded = true;
                string id = (e.Item.DataItem as TotalizerItemPlant).Data.Id;
                (((Telerik.Web.UI.GridDataItem)(e.Item)).ChildItem.FindControl("gridDep") as RadGrid).DataSource = TableDataSource.First(x => x.Data.Id == id).Departments;
                (((Telerik.Web.UI.GridDataItem)(e.Item)).ChildItem.FindControl("gridDep") as RadGrid).DataBind();
            }
        }

        protected void gridDep_PreRender(object sender, EventArgs e)
        {
            if ((sender as RadGrid).DataSource == null)
            {

            }
        }
    }
}