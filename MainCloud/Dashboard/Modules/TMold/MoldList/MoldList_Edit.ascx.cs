﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.UI.Modules;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.MoldList
{
    public partial class MoldList_Edit : WidgetControl<object>
    {
        public MoldList_Edit() : base(typeof(MoldList_Edit)) { }

        public string ApplicationId
        {
            get
            {
                return (Page as PageEdit).GetApplicationId();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", ApplicationId);

            edsAsset.WhereParameters.Clear();
            edsAsset.WhereParameters.Add("ApplicationId", ApplicationId);
            edsAssetGroup.WhereParameters.Clear();
            edsAssetGroup.WhereParameters.Add("ApplicationId", ApplicationId);

            edsDevice.WhereParameters.Clear();
            edsDevice.WhereParameters.Add("ApplicationId", ApplicationId);

            edsMaintenance.WhereParameters.Clear();
            edsMaintenance.WhereParameters.Add("ApplicationId", ApplicationId);
            edsControlPlan.WhereParameters.Clear();
            edsControlPlan.WhereParameters.Add("ApplicationId", ApplicationId);
        }

        protected void gridMold_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                //Add new button clicked
                e.Canceled = true;
                //Prepare an IDictionary with the predefined values
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();
                newValues["MaintenancePlan"] = "";
                newValues["ControlPlan"] = "";
                //Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }

        protected void gridMold_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            string b = null;
            int? a = Convert.ToInt32(b);

            MoldRepository moldRep = new MoldRepository();

            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var mold = new Mold();
            mold.ApplicationId = ApplicationId;
            //mold.Id = (string)values["Id"];
            mold.Name = (string)values["Name"];
            mold.Code = (string)values["Code"];
            mold.Description = (string)values["Description"];
            mold.DtInstallation = Convert.ToDateTime(values["DtInstallation"]);
            mold.Brand = (string)values["Brand"];
            mold.Builder = (string)values["Builder"];
            mold.IdNumber = (string)values["IdNumber"];
            mold.Model = (string)values["Model"];
            //mold.WorkFrequency = Convert.ToDecimal(values["WorkFrequency"]);
            //mold.ProductionCadency = Convert.ToDecimal(values["ProductionCadency"]);
            mold.CavityMoldNum = Convert.ToInt32(values["CavityMoldNum"]);
            if (mold.CavityMoldNum < 1)
            {
                mold.CavityMoldNum = 1;
            }
            if (!string.IsNullOrEmpty((string)values["DtEndGuarantee"]))
            {
                mold.DtEndGuarantee = Convert.ToDateTime(values["DtEndGuarantee"]);
            }

            mold.Warning1 = Convert.ToInt32(values["Warning1"]);
            mold.Warning2 = Convert.ToInt32(values["Warning2"]);
            mold.Maximum = Convert.ToInt32(values["Maximum"]);
            //if (!string.IsNullOrEmpty((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText))
            //{
            //    mold.Warning1 = Convert.ToInt32((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText);
            //}
            //if (!string.IsNullOrEmpty((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText))
            //{
            //    mold.Warning2 = Convert.ToInt32((editableItem["Warning2"].FindControl("txtWarning2") as RadNumericTextBox).DisplayText);
            //}
            //if (!string.IsNullOrEmpty((editableItem["Maximum"].FindControl("txtMaximum") as RadNumericTextBox).DisplayText))
            //{
            //    mold.Maximum = Convert.ToInt32((editableItem["Maximum"].FindControl("txtMaximum") as RadNumericTextBox).DisplayText);
            //}

            if (!string.IsNullOrEmpty((editableItem["ProductionCadency"].Controls[0] as TextBox).Text) && Convert.ToDecimal((editableItem["ProductionCadency"].Controls[0] as TextBox).Text) > 0)
            {
                mold.ProductionCadency = Convert.ToDecimal((editableItem["ProductionCadency"].Controls[0] as TextBox).Text);
                mold.WorkFrequency = (int)TimeSpan.FromHours((double)(1 / mold.ProductionCadency)).TotalSeconds;
            }
            else
            {
                if (!string.IsNullOrEmpty((editableItem["WorkFrequency"].Controls[0] as TextBox).Text) && Convert.ToDecimal((editableItem["WorkFrequency"].Controls[0] as TextBox).Text) > 0)
                {
                    mold.WorkFrequency = Convert.ToDecimal((editableItem["WorkFrequency"].Controls[0] as TextBox).Text);
                    mold.ProductionCadency = Math.Round(3600 / mold.WorkFrequency, 2);
                }
            }

            var img = editableItem["Image"].FindControl("ImageManager");
            string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
            if (!string.IsNullOrEmpty(pathImg))
            {
                mold.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
            }

            mold.StopGone = Convert.ToInt32(values["StopGone"]);
            mold.StopReturn = Convert.ToInt32(values["StopReturn"]);
            mold.MaxRawMaterialNum = Convert.ToInt32(values["MaxRawMaterialNum"]);
            if (mold.MaxRawMaterialNum == 0) { mold.MaxRawMaterialNum = 1; }
            mold.DefaultAssetId = (string)values["DefaultAssetId"];
            mold.DefaultAssetGroupId = (string)values["DefaultAssetGroupId"];

            moldRep.Insert(mold);
            moldRep.SaveChanges();

            // Associo i piani di controllo e di manutenzione (se almeno uno dei due è stato inserito) al Mold
            string moldId = mold.Id,
                   mpId = (string)values["MaintenancePlan"], //(editableItem["MaintenancePlan"].FindControl("txtMaintenancePlan") as RadNumericTextBox).Text.Trim(),
                   cpId = (string)values["ControlPlan"]; //(editableItem["ControlPlan"].FindControl("txtControlPlan") as RadNumericTextBox).Text.Trim();

            if (mpId != "" || cpId != "")
            {
                MaintenanceRepository mRep = new MaintenanceRepository();
                Maintenance m = new Maintenance();
                m.ObjectId = moldId;
                m.ObjectType = m.MoldType;
                m.MaintenancePlanId = (mpId != "") ? Convert.ToInt32(mpId) : (int?)null;
                m.ControlPlanId = (cpId != "") ? Convert.ToInt32(cpId) : (int?)null;
                m.CreationDate = DateTime.Now;
                m.UpdateDate = DateTime.Now;
                m.ApplicationId = ApplicationId;

                mRep.Insert(m);
                mRep.SaveChanges();

                // Aggiungo il Client Id
                Maintenance mN = mRep.FindByID(m.Id);
                mN.ClientId = m.Id;
                mRep.Update(mN);
                mRep.SaveChanges();
            }
        }

        protected void gridMold_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            MoldRepository moldRep = new MoldRepository();

            var editableItem = ((GridEditableItem)e.Item);
            var moldId = (string)editableItem.GetDataKeyValue("Id");

            var mold = moldRep.FindByID(moldId);
            if (mold != null)
            {
                editableItem.UpdateValues(mold);

                //if (!string.IsNullOrEmpty((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText))
                //{
                //    mold.Warning1 = Convert.ToInt32((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText);
                //}
                //if (!string.IsNullOrEmpty((editableItem["Warning1"].FindControl("txtWarning1") as RadNumericTextBox).DisplayText))
                //{
                //    mold.Warning2 = Convert.ToInt32((editableItem["Warning2"].FindControl("txtWarning2") as RadNumericTextBox).DisplayText);
                //}
                //if (!string.IsNullOrEmpty((editableItem["Maximum"].FindControl("txtMaximum") as RadNumericTextBox).DisplayText))
                //{
                //    mold.Maximum = Convert.ToInt32((editableItem["Maximum"].FindControl("txtMaximum") as RadNumericTextBox).DisplayText);
                //}

                if (!string.IsNullOrEmpty((editableItem["ProductionCadency"].Controls[0] as TextBox).Text) && Convert.ToDecimal(editableItem.SavedOldValues["ProductionCadency"]) != Convert.ToDecimal((editableItem["ProductionCadency"].Controls[0] as TextBox).Text))
                {
                    mold.ProductionCadency = Convert.ToDecimal((editableItem["ProductionCadency"].Controls[0] as TextBox).Text);
                    mold.WorkFrequency = (int)TimeSpan.FromHours((double)(1 / mold.ProductionCadency)).TotalSeconds;
                }
                else
                {
                    if (!string.IsNullOrEmpty((editableItem["WorkFrequency"].Controls[0] as TextBox).Text) && Convert.ToDecimal(editableItem.SavedOldValues["WorkFrequency"]) != Convert.ToDecimal((editableItem["WorkFrequency"].Controls[0] as TextBox).Text))
                    {
                        mold.WorkFrequency = Convert.ToDecimal((editableItem["WorkFrequency"].Controls[0] as TextBox).Text);
                        mold.ProductionCadency = Math.Round(3600 / mold.WorkFrequency, 2);
                    }
                }

                var img = editableItem["Image"].FindControl("ImageManager");
                string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                if (!string.IsNullOrEmpty(pathImg))
                {
                    mold.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                }

                if (string.IsNullOrEmpty(mold.DefaultAssetId))
                {
                    mold.DefaultAssetId = null;
                }
                if (string.IsNullOrEmpty(mold.DefaultAssetGroupId))
                {
                    mold.DefaultAssetGroupId = null;
                }

                moldRep.Update(mold);
                moldRep.SaveChanges();

                // Associazione Mold <-> Piani di Controllo e Manutenzione
                MaintenanceRepository mRep = new MaintenanceRepository();
                Maintenance m = mRep.FindByMoldId(mold.Id);
                string mpId = (editableItem["ddlMaintenancePlan"].Controls[0] as RadComboBox).SelectedValue, //(editableItem["MaintenancePlan"].FindControl("txtMaintenancePlan") as RadNumericTextBox).Text.Trim(),
                       cpId = (editableItem["ddlControlPlan"].Controls[0] as RadComboBox).SelectedValue; //(editableItem["ControlPlan"].FindControl("txtControlPlan") as RadNumericTextBox).Text.Trim();

                if (m != null)
                {
                    // Aggiorno se necessario
                    bool updateFlag = false;

                    if (m.MaintenancePlanId.ToString() != mpId)
                    {
                        m.MaintenancePlanId = (mpId != "") ? Convert.ToInt32(mpId) : (int?)null;
                        updateFlag = true;
                    }

                    if (m.ControlPlanId.ToString() != cpId)
                    {
                        m.ControlPlanId = (cpId != "") ? Convert.ToInt32(cpId) : (int?)null;
                        updateFlag = true;
                    }

                    if (updateFlag)
                    {
                        m.UpdateDate = DateTime.Now;

                        mRep.Update(m);
                        mRep.SaveChanges();
                    }
                }
                else
                {
                    // Creo nuova associazione se necessario
                    if (mpId != "" || cpId != "")
                    {
                        m = new Maintenance();
                        m.ObjectId = moldId;
                        m.ObjectType = m.MoldType;
                        m.MaintenancePlanId = (mpId != "") ? Convert.ToInt32(mpId) : (int?)null;
                        m.ControlPlanId = (cpId != "") ? Convert.ToInt32(cpId) : (int?)null;
                        m.CreationDate = DateTime.Now;
                        m.UpdateDate = DateTime.Now;
                        m.ApplicationId = ApplicationId;

                        mRep.Insert(m);
                        mRep.SaveChanges();

                        // Aggiungo il Client Id
                        Maintenance mN = mRep.FindByID(m.Id);
                        mN.ClientId = m.Id;
                        mRep.Update(mN);
                        mRep.SaveChanges();
                    }
                }
            }
        }

        protected void gridMold_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            MoldRepository moldRep = new MoldRepository();

            var moldId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");
            var mold = moldRep.FindByID(moldId);
            if (mold != null)
            {
                moldRep.Delete(mold);
                try
                {
                    moldRep.SaveChanges();

                    // Elimino eventuale associazione Mold <-> Piani di Controllo e Manutenzione
                    MaintenanceRepository mRep = new MaintenanceRepository();
                    Maintenance m = mRep.FindByMoldId(moldId);
                    if (m != null)
                    {
                        mRep.Delete(m);
                        mRep.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }
    }
}