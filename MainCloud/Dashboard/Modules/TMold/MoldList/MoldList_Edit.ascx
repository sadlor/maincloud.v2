﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoldList_Edit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.MoldList.MoldList_Edit" %>
<%@ Register src="~/Controls/ImageManager/ImageManager.ascx" tagname="ImageManager" tagprefix="mcf" %>

<link href="/Dashboard/Modules/TMold/MoldList/Style.css?<%#DateTime.Now %>" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    function PopUpShowing(sender, eventArgs) {
        var popUp = eventArgs.get_popUp();
        var scrollPosition = $('#<%= Page.Master.FindControl("ScrollPosition").ClientID %>').val();
        popUp.style.top = scrollPosition + "px";//((gridHeight - popUpHeight) / 2 + sender.get_element().offsetTop).toString() + "px";
        //var gridWidth = sender.get_element().offsetWidth;
        //var gridHeight = sender.get_element().offsetHeight;
        //var popUpWidth = popUp.style.width.substr(0, popUp.style.width.indexOf("px"));
        //var popUpHeight = popUp.style.height.substr(0, popUp.style.height.indexOf("px"));
        //popUp.style.left = ((gridWidth - popUpWidth) / 2 + sender.get_element().offsetLeft).toString() + "px";
    }

    function GridCreated(sender, args) {
        $('.rgDataDiv').removeAttr('style');
        $('.rgDataDiv').attr('style', 'overflow-x: scroll;');
    }
</script>

<telerik:RadGrid RenderMode="Lightweight" runat="server" ID="gridMold" AutoGenerateColumns="false" AllowPaging="true" DataSourceID="edsMold"
    AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false" OnItemCommand="gridMold_ItemCommand"
    OnInsertCommand="gridMold_InsertCommand" OnUpdateCommand="gridMold_UpdateCommand" OnDeleteCommand="gridMold_DeleteCommand"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US" Width="100%">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView DataKeyNames="Id" Caption="Molds" PageSize="20" FilterExpression="" EnableHeaderContextMenu="true"
        CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
        <EditFormSettings InsertCaption="Insert Mold">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="true" />
        <Columns>
            <telerik:GridEditCommandColumn Groupable="false" />
            <%--<telerik:GridBoundColumn DataField="Id" HeaderText="TagId" Groupable="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridBoundColumn>--%>
            <telerik:GridBoundColumn DataField="Name" HeaderText="Nome" Groupable="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Code" HeaderText="Code" AllowFiltering="true" AllowSorting="true" Groupable="false" />
            <telerik:GridBoundColumn DataField="Description" HeaderText="Descrizione" AllowFiltering="false" AllowSorting="false" Groupable="false" />
            <telerik:GridBoundColumn DataField="IdNumber" HeaderText="S/N" Groupable="false" />
            <telerik:GridBoundColumn DataField="Brand" HeaderText="Brand" Groupable="false" />
            <telerik:GridBoundColumn DataField="Builder" HeaderText="Costruttore" Groupable="false" />
            <telerik:GridBoundColumn DataField="Model" HeaderText="Modello" Groupable="false" />
            <telerik:GridDateTimeColumn DataField="DtInstallation" HeaderText="Data installazione" UniqueName="DtInstallation" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy}" EditDataFormatString="dd/MM/yyyy" Groupable="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="DtEndGuarantee" HeaderText="Fine garanzia" UniqueName="DtEndGuarantee" PickerType="DatePicker" EditDataFormatString="dd/MM/yyyy" DataFormatString="{0:dd/MM/yyyy}" Groupable="false">
                <%--<ColumnValidationSettings EnableModelErrorMessageValidation="true">
                </ColumnValidationSettings>--%>
            </telerik:GridDateTimeColumn>
            <telerik:GridNumericColumn DataField="Warning1" HeaderText="Warning1" DataType="System.Int32" Groupable="false" MinValue="0" DefaultInsertValue="0" DecimalDigits="0">
            </telerik:GridNumericColumn>
            <telerik:GridNumericColumn DataField="Warning2" HeaderText="Warning2" DataType="System.Int32" Groupable="false" MinValue="0" DefaultInsertValue="0" DecimalDigits="0">
            </telerik:GridNumericColumn>
            <telerik:GridNumericColumn DataField="Maximum" HeaderText="Allarme" DataType="System.Int32" Groupable="false" MinValue="0" DefaultInsertValue="0" DecimalDigits="0">
            </telerik:GridNumericColumn>
            <%--<telerik:GridBoundColumn DataField="Maximum" HeaderText="Maximum" DataType="System.Int32" />--%>
            <telerik:GridBoundColumn DataField="WorkFrequency" HeaderText="Tempo stampaggio [s]" DataType="System.Int32" Groupable="false" />
            <telerik:GridBoundColumn DataField="ProductionCadency" HeaderText="Colpi/h" Groupable="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridBoundColumn>
            <telerik:GridDropDownColumn DataField="MaintenancePlan" HeaderText="Piano di manutenzione" UniqueName="ddlMaintenancePlan" AllowFiltering="false"
                ListTextField="Description" ListValueField="Id" DataSourceID="edsMaintenance" DropDownControlType="RadComboBox"
                ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn DataField="ControlPlan" HeaderText="Piano di controllo" UniqueName="ddlControlPlan" AllowFiltering="false"
                ListTextField="Description" ListValueField="Id" DataSourceID="edsControlPlan" DropDownControlType="RadComboBox"
                ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
            </telerik:GridDropDownColumn>
            <%--<telerik:GridTemplateColumn HeaderText="Piano di Manutenzione" UniqueName="MaintenancePlan" ItemStyle-Width="90px" Groupable="false">
                <EditItemTemplate>
                    <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtMaintenancePlan"
                        DisplayText='<%# Eval("MaintenancePlan")%>' DbValue='<%# Eval("MaintenancePlan")%>' DataType="System.Int32">
                        <NumberFormat DecimalSeparator="." DecimalDigits="0" GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("MaintenancePlan")%>' Width="90px" runat="server" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Piano di Controllo" UniqueName="ControlPlan" ItemStyle-Width="90px" Groupable="false">
                <EditItemTemplate>
                    <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtControlPlan"
                        DisplayText='<%# Eval("ControlPlan")%>' DbValue='<%# Eval("ControlPlan")%>' DataType="System.Int32">
                        <NumberFormat DecimalSeparator="." DecimalDigits="0" GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("ControlPlan")%>' Width="90px" runat="server" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>--%>
            <telerik:GridBoundColumn DataField="CavityMoldNum" HeaderText="N° impronte" DataType="System.Int32" Groupable="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn DataField="PathImg" HeaderText="Image" UniqueName="Image" Groupable="false">
                <ItemTemplate>
                    <asp:Image ImageUrl='<%# string.Format("~/{0}", Eval("PathImg"))%>' runat="server" Width="80%" />
                </ItemTemplate>
                <EditItemTemplate>
                    <mcf:ImageManager runat="server" ID="ImageManager" Path='<%# string.Format("/Uploads/{0}/Widgets/Shared", MainCloudFramework.Web.Helpers.MultiTenantsHelper.ApplicationId)%>'
                        LocalizationPath="~/App_GlobalResources/" Culture="en-US" />
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridDropDownColumn UniqueName="ddlDefaultAsset" ListTextField="Description"
                ListValueField="Id" DataSourceID="edsAsset" HeaderText="Default<br/>Asset" DataField="DefaultAssetId" 
                DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                AllowSorting="true" AllowFiltering="false">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="ddlDefaultGroupAsset" ListTextField="Description"
                ListValueField="Id" DataSourceID="edsAssetGroup" HeaderText="Default<br/>Gruppo Asset" DataField="DefaultAssetGroupId"
                DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                AllowSorting="true" AllowFiltering="false">
            </telerik:GridDropDownColumn>
            <%--<telerik:GridDropDownColumn UniqueName="ddlDevice" ListTextField="SerialNumber"
                ListValueField="Id" DataSourceID="edsDevice" HeaderText="Device" DataField="DeviceId" 
                DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                AllowSorting="false" AllowFiltering="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="false">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required" ControlToValidate="ddlDepartment"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridDropDownColumn>--%>
            <telerik:GridNumericColumn DataField="StopGone" HeaderText="Fermate andata" DataType="System.Int32" Groupable="false" MinValue="0" DefaultInsertValue="0" DecimalDigits="0">
            </telerik:GridNumericColumn>
            <telerik:GridNumericColumn DataField="StopReturn" HeaderText="Fermate ritorno" DataType="System.Int32" Groupable="false" MinValue="0" DefaultInsertValue="0" DecimalDigits="0">
            </telerik:GridNumericColumn>
            <telerik:GridNumericColumn DataField="MaxRawMaterialNum" HeaderText="Max materie prime" DataType="System.Int32" Groupable="false" MinValue="1" DefaultInsertValue="1" DecimalDigits="0">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridNumericColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
    <ClientSettings AllowKeyboardNavigation="true">
        <Scrolling AllowScroll="true" SaveScrollPosition="true" />
        <ClientEvents OnPopUpShowing="PopUpShowing" OnGridCreated="GridCreated" />
        <KeyboardNavigationSettings AllowSubmitOnEnter="true" />
    </ClientSettings>
</telerik:RadGrid>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    OrderBy="it.Name" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsAsset" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    OrderBy="it.Code" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsAssetGroup" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="AssetGroups"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsDevice" runat="server" ContextTypeName="EverynetModule.Models.EverynetDbContext" EntitySetName="Devices"
    OrderBy="it.SerialNumber" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsMaintenance" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="MaintenancePlans"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsControlPlan" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="ControlPlans"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Warning">
    <Body>
        <asp:label id="messaggio" runat="server"/>
    </Body>
</mcf:PopUpDialog>