﻿using AssetManagement.Repositories;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.MoldList
{
    public partial class MoldList_View : DataWidget<MoldList_View>
    {
        public MoldList_View() : base(typeof(MoldList_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    edsMold.WhereParameters.Clear();
            //    edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            //}
        }

        protected void btnRedirect_Click(object sender, EventArgs e)
        {
        //    SessionMultitenant[MoldIDConstants.SELECTED_MOLD_ID] = (sender as RadButton).Value;

        //    string returnUrl = Request["returnurl"];
        //    if (string.IsNullOrEmpty(returnUrl))
        //    {
        //        string destinationAreaId = CustomSettings.Find("settings-destination-area-id") as string;
        //        if (!string.IsNullOrEmpty(destinationAreaId))
        //        {
        //            WidgetAreaRepository areaService = new WidgetAreaRepository();
        //            WidgetArea wa = areaService.FindByID(destinationAreaId);

        //            Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
        //        }
        //        //Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl("RealTime"));
        //        // eliminare se funziona la riga sopra -- Response.Redirect("~/App/" + MultiTenantsHelper.ApplicationName + "/Area/RealTime");
        //    }
        //    else
        //    {
        //        Response.Redirect("~" + returnUrl);
        //    }
        }

        protected void MoldListView_NeedDataSource(object sender, RadListViewNeedDataSourceEventArgs e)
        {
            //MoldRepository moldRepo = new MoldRepository();
            //MoldListView.DataSource = moldRepo.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
        }

        protected void gridMold_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            MoldRepository moldRepo = new MoldRepository();
            gridMold.DataSource = moldRepo.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Name).ToList();
        }
    }
}