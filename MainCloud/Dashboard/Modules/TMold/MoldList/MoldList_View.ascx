﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoldList_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.MoldList.MoldList_View" %>

<link href="/Dashboard/Modules/TMold/MoldList/Style.css?<%#DateTime.Now %>" rel="stylesheet" type="text/css" />

<telerik:RadGrid runat="server" ID="gridMold" RenderMode="Lightweight" AutoGenerateColumns="false"
    OnNeedDataSource="gridMold_NeedDataSource">
    <MasterTableView Caption="Stampi" DataKeyNames="Id" FilterExpression="">
        <Columns>
            <telerik:GridBoundColumn DataField="Name" HeaderText="Stampo">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
        </Columns>
        <NestedViewTemplate>
            <div>
                <fieldset class="fieldset">
                    <legend>Mold: <%# Eval("Name") %>
                        <asp:Image ImageUrl='<%# string.Format("~/{0}", Eval("PathImg"))%>' runat="server" Width="30%" AlternateText="Mold Image" ToolTip="Mold Image" />
                    </legend>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <table class="table">
                                <caption>Proprietà</caption>
                                <tr>
                                    <td><b>Code</b></td>
                                    <td>
                                        <%#Eval("Code")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Descrizione</b></td>
                                    <td>
                                        <%# Eval("Description")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Brand</b></td>
                                    <td>
                                        <%# Eval("Brand")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Modello</b></td>
                                    <td>
                                        <%#Eval("Model")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Costruttore</b></td>
                                    <td>
                                        <%#Eval("Builder")%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <table class="table">
                                <caption>Stampaggio</caption>
                                <tr>
                                    <td><b>Nr impronte</b></td>
                                    <td>
                                        <%#Eval("CavityMoldNum")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Cadenza</b></td>
                                    <td>
                                        <%#Eval("ProductionCadency")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Tempo stampaggio</b></td>
                                    <td>
                                        <%#Eval("WorkFrequency")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Max materie prime</b></td>
                                    <td>
                                        <%#Eval("MaxRawMaterialNum")%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <table class="table">
                                <caption>Manutenzione</caption>
                                <tr>
                                    <td><b>Warning1</b></td>
                                    <td>
                                        <%#Eval("Warning1")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Warning2</b></td>
                                    <td>
                                        <%#Eval("Warning2")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Allarme</b></td>
                                    <td>
                                        <%#Eval("Maximum")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Piano manutenzione</b></td>
                                    <td>
                                        <%#Eval("MaintenancePlan")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Piano controllo</b></td>
                                    <td>
                                        <%#Eval("ControlPlan")%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <table class="table">
                                <caption>Macchina</caption>
                                <tr>
                                    <td><b>Asset predefinito</b></td>
                                    <td>
                                        <%#Eval("DefaultAsset.Code")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Gruppo asset<br />predefinito</b></td>
                                    <td>
                                        <%#Eval("DefaultAssetGroup.Description")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Fermate andata</b></td>
                                    <td>
                                        <%#Eval("StopGone")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Fermate ritorno</b></td>
                                    <td>
                                        <%#Eval("StopReturn")%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </fieldset>
            </div>
        </NestedViewTemplate>
    </MasterTableView>
</telerik:RadGrid>