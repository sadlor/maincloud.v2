﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using MES.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.MoldArticle
{
    public partial class MoldArticle_View : DataWidget<MoldArticle_View>
    {
        public MoldArticle_View() : base(typeof(MoldArticle_View)) {}

        ArticleMoldService service = new ArticleMoldService();

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsArticle.WhereParameters.Clear();
            edsArticle.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        protected void gridMA_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            gridMA.DataSource = service.GetArticleMoldList();
        }

        protected void gridMA_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var articleMold = new ArticleMold();
            if (!string.IsNullOrEmpty((editableItem["Article"].FindControl("ddlArticle") as RadComboBox).SelectedValue))
            {
                articleMold.ArticleId = Convert.ToInt32((editableItem["Article"].FindControl("ddlArticle") as RadComboBox).SelectedValue);
            }
            articleMold.MoldId = (string)values["MoldId"];

            try
            {
                service.AddNew(articleMold);
            }
            catch (Exception)
            {
                messaggio.Text = "Associazione già esistente";
                dlgWidgetConfig.OpenDialog();
            }
        }

        protected void gridMA_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var moldId = (string)((GridDataItem)e.Item).GetDataKeyValue("MoldId");
            var articleId = Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("ArticleId"));
            var articleMold = service.FindArticleMold(moldId, articleId);
            if (articleMold != null)
            {
                try
                {
                    service.DeleteAssociation(articleMold);
                }
                catch (Exception)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }
    }
}