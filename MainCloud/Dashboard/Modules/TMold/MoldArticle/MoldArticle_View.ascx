﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoldArticle_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.MoldArticle.MoldArticle_View" %>

<telerik:RadGrid runat="server" RenderMode="Lightweight" ID="gridMA" AutoGenerateColumns="false" AllowFilteringByColumn="true" AllowSorting="true" AllowPaging="true"
    OnNeedDataSource="gridMA_NeedDataSource" OnInsertCommand="gridMA_InsertCommand" OnDeleteCommand="gridMA_DeleteCommand"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <GroupingSettings CaseSensitive="false" />
    <MasterTableView Caption="" CommandItemDisplay="Top" DataKeyNames="MoldId, ArticleId">
        <CommandItemSettings ShowAddNewRecordButton="true" ShowRefreshButton="true" />
        <Columns>
            <telerik:GridDropDownColumn UniqueName="ddlMold" ListTextField="Name"
                ListValueField="Id" DataSourceID="edsMold" HeaderText="Stampo" DataField="MoldId" 
                DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                AllowSorting="true" AllowFiltering="false">
            </telerik:GridDropDownColumn>
            <telerik:GridTemplateColumn DataField="ArticleId" HeaderText="Articolo" UniqueName="Article" AllowSorting="true"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Article.Code")%>
                </ItemTemplate>
                <InsertItemTemplate>
                    <telerik:RadComboBox RenderMode="Lightweight" ID="ddlArticle" runat="server" EmptyMessage="Articolo"
                        DataSourceID="edsArticle" DataTextField="Code" DataValueField="Id" MarkFirstMatch="true" AutoCompleteSeparator="">
                    </telerik:RadComboBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlArticle" ErrorMessage="*" ForeColor="Red" />
                </InsertItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridTemplateColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
    <ClientSettings AllowKeyboardNavigation="true">
        <KeyboardNavigationSettings AllowSubmitOnEnter="true" />
    </ClientSettings>
</telerik:RadGrid>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    OrderBy="it.Name" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsArticle" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Articles"
    OrderBy="it.Code" AutoGenerateWhereClause="true" />

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Warning">
    <Body>
        <asp:label id="messaggio" runat="server"/>
    </Body>
</mcf:PopUpDialog>