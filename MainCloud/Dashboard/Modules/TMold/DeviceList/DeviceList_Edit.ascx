﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeviceList_Edit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.DeviceList.DeviceList_Edit" %>

<telerik:RadGrid runat="server" ID="gridDevice" RenderMode="Lightweight" AutoGenerateColumns="false" AllowSorting="true" AllowFilteringByColumn="true"
    OnNeedDataSource="gridDevice_NeedDataSource" OnItemDataBound="gridDevice_ItemDataBound" OnInsertCommand="gridDevice_InsertCommand" OnUpdateCommand="gridDevice_UpdateCommand"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <GroupingSettings CaseSensitive="false" />
    <MasterTableView Caption="Devices" CommandItemDisplay="Top" DataKeyNames="Id">
        <CommandItemSettings ShowRefreshButton="false"/>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridTemplateColumn DataField="Id" HeaderText="DeviceId" UniqueName="DeviceId" AllowSorting="false" AllowFiltering="false">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Id")%>
                </ItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtDeviceId" runat="Server" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDeviceId" ErrorMessage="*" ForeColor="Red" />
                </InsertItemTemplate>
                <EditItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Id")%>
                </EditItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="SerialNumber" HeaderText="S/N" UniqueName="SerialNumber" AllowSorting="true"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "SerialNumber")%>
                </ItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtDeviceSN" runat="Server" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDeviceSN" ErrorMessage="*" ForeColor="Red" />
                </InsertItemTemplate>
                <EditItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "SerialNumber")%>
                </EditItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="Type" HeaderText="Tipo sensore" UniqueName="Type" AllowSorting="false" AllowFiltering="false">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Type")%>
                </ItemTemplate>
                <InsertItemTemplate>
                    <telerik:RadDropDownList runat="server" ID="ddlTypeDevice">
                    </telerik:RadDropDownList>
                </InsertItemTemplate>
                <EditItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Type")%>
                </EditItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn DataField="Producer" HeaderText="Produttore" AllowSorting="false" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Description" HeaderText="Descrizione" AllowSorting="false" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn DataField="Config" HeaderText="Configurazione" UniqueName="Config" AllowSorting="false" AllowFiltering="false">
                <ItemTemplate>
                    <%#Newtonsoft.Json.JsonConvert.SerializeObject(DataBinder.Eval(Container.DataItem, "Config"))%>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Label Text="Frequenza invio[m]" runat="server" />
                    <telerik:RadNumericTextBox ID="txtConfigCycle" runat="server" MinValue="1" MaxValue="255">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                    <asp:Label Text="Soglia accelerazione" runat="server" />
                    <telerik:RadNumericTextBox ID="txtConfigAcc" runat="server" MinValue="0">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                    <asp:Label Text="Asse" runat="server" AssociatedControlID="ddlConfigAxis" />
                    <telerik:RadDropDownList runat="server" ID="ddlConfigAxis">
                        <Items>
                            <telerik:DropDownListItem Text="x-" Selected="true" />
                            <telerik:DropDownListItem Text="x+" />
                            <telerik:DropDownListItem Text="y-" />
                            <telerik:DropDownListItem Text="y+" />
                            <telerik:DropDownListItem Text="z-" />
                            <telerik:DropDownListItem Text="z+" />
                        </Items>
                    </telerik:RadDropDownList>
                    <%--<asp:Label Text="Timeout" runat="server" />
                    <asp:TextBox ID="txtConfigTimeout" runat="server" />--%>
                </EditItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridTemplateColumn>
            <telerik:GridDropDownColumn UniqueName="ddlMold" ListTextField="Name"
                ListValueField="Id" DataSourceID="edsMold" HeaderText="Stampo" DataField="MoldId" 
                DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                AllowSorting="false" AllowFiltering="false">
            </telerik:GridDropDownColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    OrderBy="it.Name" AutoGenerateWhereClause="true" />