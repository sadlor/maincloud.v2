﻿using DeviceModule.Driver;
using EverynetModule.Repositories;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.DeviceList
{
    public partial class DeviceList_Edit : WidgetControl<object>
    {
        public DeviceList_Edit() : base(typeof(DeviceList_Edit)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        protected void gridDevice_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DeviceRepository repos = new DeviceRepository();
            gridDevice.DataSource = repos.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
        }

        protected void gridDevice_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item.IsInEditMode)
            {
                if (e.Item is IGridInsertItem)
                {
                    RadDropDownList ddl = e.Item.FindControl("ddlTypeDevice") as RadDropDownList;

                    Array itemValues = Enum.GetValues(typeof(TypeOfDevice));
                    Array itemNames = Enum.GetNames(typeof(TypeOfDevice));
                    for (int i = 0; i <= itemNames.Length - 1; i++)
                    {
                        DropDownListItem item = new DropDownListItem(itemNames.GetValue(i).ToString(), itemValues.GetValue(i).ToString());
                        ddl.Items.Add(item);
                    }
                    ddl.SelectedValue = TypeOfDevice.TMold.ToString();
                }
                else
                {
                    (e.Item.FindControl("txtConfigCycle") as RadNumericTextBox).Text = (e.Item.DataItem as EverynetModule.Models.Device).Config[DeviceConfigParam.Frequency.ToString()].ToString();
                    (e.Item.FindControl("txtConfigAcc") as RadNumericTextBox).Text = (e.Item.DataItem as EverynetModule.Models.Device).Config[DeviceConfigParam.Acceleration.ToString()].ToString();
                    //(e.Item.FindControl("txtConfigTimeout") as TextBox).Text = (e.Item.DataItem as EverynetModule.Models.Device).Config[DeviceConfigParam.Timeout.ToString()].ToString();
                    (e.Item.FindControl("ddlConfigAxis") as RadDropDownList).SelectedText = (e.Item.DataItem as EverynetModule.Models.Device).Config[DeviceConfigParam.Axis.ToString()].ToString();
                }
            }
        }

        protected void gridDevice_InsertCommand(object sender, GridCommandEventArgs e)
        {
            DeviceRepository deviceRepo = new DeviceRepository();

            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var device = new EverynetModule.Models.Device();
            device.ApplicationId = MultiTenantsHelper.ApplicationId;
            device.Id = (editableItem["DeviceId"].FindControl("txtDeviceId") as TextBox).Text;
            device.SerialNumber = (editableItem["SerialNumber"].FindControl("txtDeviceSN") as TextBox).Text;
            device.Producer = (string)values["Producer"];
            device.Description = (string)values["Description"];
            device.Type = (editableItem["Type"].FindControl("ddlTypedevice") as RadDropDownList).SelectedValue;
            device.Config = new Dictionary<string, object>()
            {
                { DeviceConfigParam.Frequency.ToString(), (editableItem["Config"].FindControl("txtConfigCycle") as RadNumericTextBox).Text },
                { DeviceConfigParam.Acceleration.ToString(), (editableItem["Config"].FindControl("txtConfigAcc") as RadNumericTextBox).Text },
                { DeviceConfigParam.Axis.ToString(), (editableItem["Config"].FindControl("ddlConfigAxis") as RadDropDownList).SelectedText }
                //{ DeviceConfigParam.CycleNum.ToString(), (editableItem["Config"].FindControl("txtConfigCycle") as TextBox).Text },
                //{ DeviceConfigParam.Timeout.ToString(), (editableItem["Config"].FindControl("txtConfigTimeout") as TextBox).Text }
            };

            device.MoldId = (string)values["MoldId"];

            deviceRepo.Insert(device);
            deviceRepo.SaveChanges();
        }

        protected void gridDevice_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            DeviceRepository deviceRepo = new DeviceRepository();
            var editableItem = ((GridEditableItem)e.Item);
            var deviceId = editableItem.GetDataKeyValue("Id").ToString();
            var device = deviceRepo.FindByID(deviceId);

            if (device != null)
            {
                editableItem.UpdateValues(device);

                device.Config = new Dictionary<string, object>()
                {
                    { DeviceConfigParam.Frequency.ToString(), (editableItem["Config"].FindControl("txtConfigCycle") as RadNumericTextBox).Text },
                    { DeviceConfigParam.Acceleration.ToString(), (editableItem["Config"].FindControl("txtConfigAcc") as RadNumericTextBox).Text },
                    { DeviceConfigParam.Axis.ToString(), (editableItem["Config"].FindControl("ddlConfigAxis") as RadDropDownList).SelectedText }
                    //{ DeviceConfigParam.Timeout.ToString(), (editableItem["Config"].FindControl("txtConfigTimeout") as TextBox).Text }
                };

                //if (!string.IsNullOrEmpty((editableItem["Config"].FindControl("txtConfigCycle") as TextBox).Text))
                //{
                //    device.Config[DeviceConfigParam.CycleNum.ToString()] = (editableItem["Config"].FindControl("txtConfigCycle") as TextBox).Text;
                //}
                //if (!string.IsNullOrEmpty((editableItem["Config"].FindControl("txtConfigAcc") as TextBox).Text))
                //{
                //    device.Config[DeviceConfigParam.Acceleration.ToString()] = (editableItem["Config"].FindControl("txtConfigAcc") as TextBox).Text;
                //}
                //if (!string.IsNullOrEmpty((editableItem["Config"].FindControl("txtConfigTimeout") as TextBox).Text))
                //{
                //    device.Config[DeviceConfigParam.Timeout.ToString()] = (editableItem["Config"].FindControl("txtConfigTimeout") as TextBox).Text;
                //}

                if (string.IsNullOrEmpty(device.MoldId))
                {
                    device.MoldId = null;
                }

                deviceRepo.Update(device);
                deviceRepo.SaveChanges();
            }
        }
    }
}