﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeviceList_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.DeviceList.DeviceList_View" %>

<telerik:RadGrid runat="server" ID="gridDevice" RenderMode="Lightweight" AutoGenerateColumns="false" AllowSorting="true" AllowFilteringByColumn="true"
    OnNeedDataSource="gridDevice_NeedDataSource"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <GroupingSettings CaseSensitive="false" />
    <MasterTableView Caption="Devices">
        <Columns>
            <telerik:GridBoundColumn DataField="Id" HeaderText="DeviceId" AllowSorting="false" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SerialNumber" HeaderText="S/N" AllowSorting="true"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Type" HeaderText="Tipo" AllowSorting="false" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn DataField="Config" HeaderText="Configurazione" UniqueName="Config" AllowSorting="false" AllowFiltering="false">
                <ItemTemplate>
                    <%#Newtonsoft.Json.JsonConvert.SerializeObject(DataBinder.Eval(Container.DataItem, "Config"))%>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridTemplateColumn>
            <telerik:GridDropDownColumn UniqueName="ddlMold" ListTextField="Name"
                ListValueField="Id" DataSourceID="edsMold" HeaderText="Stampo" DataField="MoldId" 
                DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                AllowSorting="false" AllowFiltering="false">
            </telerik:GridDropDownColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    OrderBy="it.Name" AutoGenerateWhereClause="true" />