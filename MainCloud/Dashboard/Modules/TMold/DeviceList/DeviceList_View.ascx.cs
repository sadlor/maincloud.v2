﻿using EverynetModule.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.TMold.DeviceList
{
    public partial class DeviceList_View : DataWidget<DeviceList_View>
    {
        public DeviceList_View() : base(typeof(DeviceList_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        protected void gridDevice_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DeviceRepository repos = new DeviceRepository();
            gridDevice.DataSource = repos.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.SerialNumber).ToList();
        }
    }
}