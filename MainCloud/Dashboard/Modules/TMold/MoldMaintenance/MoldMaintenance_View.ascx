﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoldMaintenance_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.MoldMaintenance.MoldMaintenance_View" %>

<telerik:RadGrid runat="server" ID="gridMold" RenderMode="Lightweight" AutoGenerateColumns="false"
    AllowFilteringByColumn="true" AllowSorting="true" AllowPaging="true"
    OnNeedDataSource="gridMold_NeedDataSource" OnItemDataBound="gridMold_ItemDataBound"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <%--<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />--%>
    <MasterTableView Caption="" DataKeyNames="Code, Value">
        <Columns>
            <telerik:GridBoundColumn DataField="Code" HeaderText="Stampo" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Machine" HeaderText="Macchina" CurrentFilterFunction="Contains" AllowSorting="false" AllowFiltering="false"
                ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Order" HeaderText="Commessa" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Article" HeaderText="Articolo" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Efficiency" HeaderText="Ore" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn UniqueName="ChartColumn" HeaderText="" AllowFiltering="false">
                <ItemTemplate>
                    <div style="max-width: 300px; height: 50px;">
                        <telerik:RadHtmlChart ID="RadHtmlChart1" runat="server" Width="100%" Height="100%">
                            <Legend>
                                <Appearance Visible="false">
                                </Appearance>
                            </Legend>
                            <PlotArea>
                                <Series>
                                    <telerik:BarSeries>
                                        <LabelsAppearance Visible="false">
                                        </LabelsAppearance>
                                        <TooltipsAppearance Color="White" />
                                    </telerik:BarSeries>
                                </Series>
                                <YAxis MinValue="0" MaxValue="5">
                                </YAxis>
                                <XAxis>
                                    <MinorGridLines Visible="false"></MinorGridLines>
                                    <MajorGridLines Visible="false"></MajorGridLines>
                                </XAxis>
                            </PlotArea>
                        </telerik:RadHtmlChart>
                    </div>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
            </telerik:GridTemplateColumn>
        </Columns>
    </MasterTableView>
    <GroupingSettings CaseSensitive="false" />
</telerik:RadGrid>
