﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.MoldMaintenance
{
    public partial class MoldMaintenance_View : DataWidget<MoldMaintenance_View>
    {
        public MoldMaintenance_View() : base(typeof(MoldMaintenance_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private class MoldRecord
        {
            public string Code { get; set; }
            public string Machine { get; set; }
            public string Article { get; set; }
            public string Order { get; set; }
            public decimal Efficiency { get; set; }
            public decimal Value { get; set; }

            public MoldRecord(string code, string machine, string article, string order, decimal value, decimal efficiency = 0)
            {
                Code = code;
                Machine = machine;
                Article = article;
                Order = order;
                Value = value;
                Efficiency = efficiency;
            }
        }

        protected void gridMold_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //Analisi per tutta la commessa in corso

            MoldRepository moldRepos = new MoldRepository();
            List<Mold> list = moldRepos.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();

            List<MoldRecord> dataSource = new List<MoldRecord>();

            AssetRepository ARep = new AssetRepository();
            TransactionMoldRepository TRep = new TransactionMoldRepository();
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

            TransactionMoldService tService = new TransactionMoldService();
            IndexService indexService = new IndexService();

            List<string> assetIdList = list.Select(x => x.AssetId).ToList();

            List<Asset> assetList = ARep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && assetIdList.Contains(x.Id)).ToList();
            //List<Asset> assetList = ARep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && !string.IsNullOrEmpty(x.ZoneId)).OrderBy(x => x.Code.Length).ThenBy(x => x.Code).ToList();
            //foreach (Asset item in assetList)
            foreach (Mold item in list)
            {
                if (tService.LastTransactionOpenIsInProduction(item.Id))
                {
                    TransactionMold lastT = tService.GetLastTransactionOpen(item.Id);
                    if (!string.IsNullOrEmpty(lastT.JobId))
                    {
                        string jobId = lastT.JobId;

                        List<TransactionMold> tList = new List<TransactionMold>();
                        tList = TRep.ReadAll(x => x.MoldId == item.Id && x.JobId == jobId).ToList();

                        //fine lavoro commessa -> quanto manca al termine della produzione
                        //tList.Sum(x => x.PartialCounting * x.)

                        TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                        TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                        TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                        decimal efficiency = 0;
                        decimal oreProdTeorico = 0;
                        decimal qtyShotProd = 0;
                        decimal qtyProd = 0;
                        decimal cadency = 0;

                        if (productionTime.TotalSeconds > 0)
                        {
                            qtyShotProd = tList.Sum(x => x.PartialCounting);
                            cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                            foreach (Job job in tList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct())
                            {
                                if (job != null)
                                {
                                    if (job.StandardRate > 0)
                                    {
                                        oreProdTeorico += Math.Round(tList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) / job.StandardRate, 2);
                                    }
                                    qtyProd += tList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1);
                                }
                            }
                        }
                        efficiency = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);

                        int lastShot = moldRepos.GetLastMaintenanceShot(item.Id);
                        decimal produced = (lastT.CounterEnd.HasValue ? lastT.CounterEnd.Value : lastT.CounterStart) - lastShot;
                        decimal left = item.Maximum - produced;
                        if (left < 0) { left = 0; }
                        TimeSpan time = TimeSpan.FromHours((double)(left / cadency));

                        decimal perc = (decimal)Math.Round(time.TotalSeconds / TimeSpan.FromHours(80).TotalSeconds * 100 / 20, 2);
                        dataSource.Add(
                            new MoldRecord(
                                item.Description,
                                ARep.GetCode(lastT.MachineId),
                                lastT.Job.Order.Article.Code,
                                lastT.Job.Order.CustomerOrder.OrderCode,
                                perc,
                                (int)time.TotalHours));
                    }
                }
            }

            gridMold.DataSource = dataSource;
        }

        protected void gridMold_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //ConfigurationService configService = new ConfigurationService();

            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                decimal d = Convert.ToDecimal(item.GetDataKeyValue("Value"));
                RadHtmlChart chart = item["ChartColumn"].FindControl("RadHtmlChart1") as RadHtmlChart;
                BarSeries s = chart.PlotArea.Series[0] as BarSeries;
                //s.Appearance.FillStyle.BackgroundColor = configService.GetColorByParameter(rbtnIndex.SelectedValue, d);
                s.Items.Clear();
                s.Items.Add(d);
                chart.DataBind();
            }
        }
    }
}