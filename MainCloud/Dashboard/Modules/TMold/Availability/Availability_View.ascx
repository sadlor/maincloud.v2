﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Availability_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.Availability.Availability_View" %>

<telerik:RadGrid runat="server" ID="gridMoldAvailable" AutoGenerateColumns="false" AllowSorting="true"
    OnNeedDataSource="gridMoldAvailable_NeedDataSource">
    <MasterTableView Caption="Stampi" FilterExpression="">
        <Columns>
            <telerik:GridBoundColumn DataField="Status" HeaderText="Stato" AllowSorting="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Mold" HeaderText="Stampo">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridBoundColumn DataField="Order" HeaderText="Commessa">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Article" HeaderText="Articolo">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>--%>
            <telerik:GridBoundColumn DataField="Alarm" HeaderText="Pz intervallo">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Produced" HeaderText="Pz prodotti" DataFormatString="{0:f0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Left" HeaderText="Pz possibili" DataFormatString="{0:f0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Cadency" HeaderText="Cadenza">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Theoretical" HeaderText="H teoriche" DataFormatString="{0:f2}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Efficiency" HeaderText="Efficienza" DataFormatString="{0:f2}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Actual" HeaderText="H effettive" DataFormatString="{0:f2}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>