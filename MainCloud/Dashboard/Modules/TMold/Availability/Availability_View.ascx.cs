﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.TMold.Availability
{
    public partial class Availability_View : DataWidget<Availability_View>
    {
        public Availability_View() : base(typeof(Availability_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            //TransactionMoldRepository moldTRep = new TransactionMoldRepository();
            //List<TransactionMold> tList = moldTRep.ReadAll(x => x.MoldId == "E008013453017AAC").OrderBy(x => x.Start).ToList();
            //decimal i = 0;
            //foreach (TransactionMold t in tList)
            //{
            //    t.ProgressiveCounter = i;
            //    if (t.PartialCounting > 0 && !t.Open)
            //    {
            //        t.ProgressiveCounterEnd = t.ProgressiveCounter + t.PartialCounting;
            //    }
            //    else
            //    {
            //        if (t.PartialCounting == 0)
            //        {
            //            t.ProgressiveCounterEnd = t.ProgressiveCounter;
            //        }
            //    }
            //    i = t.ProgressiveCounterEnd.Value;

            //    moldTRep.Update(t);
            //}
            //moldTRep.SaveChanges();
        }

        private class Record
        {
            public string Status { get; set; }
            public string Mold { get; set; }
            public string Order { get; set; }
            public string Article { get; set; }
            public decimal Alarm { get; set; }
            public decimal Produced { get; set; }
            public decimal Left { get; set; }
            public decimal Cadency { get; set; }
            /// <summary>
            /// Ore teoriche
            /// </summary>
            public decimal Theoretical { get; set; }
            public decimal Efficiency { get; set; }
            /// <summary>
            /// Ore effettive
            /// </summary>
            public decimal Actual { get; set; }

            public Record(string status, string mold, decimal alarm, decimal produced, decimal left,decimal cadency, decimal theoretical, decimal efficiency, decimal actual)
            {
                Status = status;
                this.Mold = mold;
                //this.Order = order;
                //this.Article = article;
                Alarm = alarm;
                Produced = produced;
                Left = left;
                Cadency = cadency;
                Theoretical = theoretical;
                Efficiency = efficiency;
                Actual = actual;
            }
        }

        protected void gridMoldAvailable_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            List<Record> dataSource = new List<Record>();

            TransactionMoldRepository TRep = new TransactionMoldRepository();
            TransactionMoldService service = new TransactionMoldService();
            MoldRepository rep = new MoldRepository();
            List<Mold> list = rep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();  //x.Status == "Disponibile" &&
            foreach (Mold item in list)
            {
                decimal produced = service.LastCounterMold(item.Id) - rep.GetLastMaintenanceShot(item.Id);
                decimal left = item.Maximum - produced < 0 ? 0 : item.Maximum - produced;

                string jobId = service.GetLastJob(item.Id);

                decimal cadency = 0;
                if (!string.IsNullOrEmpty(jobId))
                {
                    List<TransactionMold> tList = new List<TransactionMold>();
                    tList = TRep.ReadAll(x => x.MoldId == item.Id && x.JobId == jobId).ToList();
                    //tList = TRep.ReadAll(x => x.MachineId == AssetId && x.Start >= DateStart && x.Start <= DateEnd).ToList();

                    TimeSpan productionTime;
                    if (item.Id == "649ea07c-8fac-4e4b-817a-ee35e7deb744" || item.Id == "131ff7ef-2d61-4914-af46-b7b8d447b7e7")
                    {
                        productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString() || x.PartialCounting > 0).Sum(x => x.Duration));
                    }
                    else
                    {
                        productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                    }

                    decimal qtyShotProd = 0;

                    if (productionTime.TotalSeconds > 0)
                    {
                        qtyShotProd = tList.Sum(x => x.PartialCounting);
                        cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                    }
                }

                dataSource.Add(
                    new Record(
                        item.Status,
                        item.Description,
                        item.Maximum,
                        produced,
                        left,
                        item.ProductionCadency,
                        left / item.ProductionCadency,
                        cadency / item.ProductionCadency * 100,
                        cadency > 0 ? left / cadency : 0));
            }

            gridMoldAvailable.DataSource = dataSource.OrderBy(x => x.Mold);
        }
    }
}