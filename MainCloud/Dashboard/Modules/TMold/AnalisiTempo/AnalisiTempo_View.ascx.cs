﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using AssetManagement.Services;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Configuration;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.AnalisiTempo
{
    public partial class AnalisiTempo_View : DataWidget<AnalisiTempo_View>
    {
        public AnalisiTempo_View() : base(typeof(AnalisiTempo_View)) {}

        IndexService indexService = new IndexService();

        protected void Page_Load(object sender, EventArgs e)
        {
            edsDepartment.WhereParameters.Clear();
            edsDepartment.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);

            if (!IsPostBack)
            {
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }
        }

        private class MoldRecord
        {
            public string Code { get; set; }
            public string MachineOnTime { get; set; }
            public string Production { get; set; }
            public decimal OreProdTeorico { get; set; }
            public string Stop { get; set; }
            public decimal Index { get; set; }
            public decimal QtyShotProd { get; set; }
            public decimal QtyProd { get; set; }
            public decimal Cadency { get; set; }
            public decimal Waste { get; set; }
            public decimal StampateOraTeorico { get; set; }
            public decimal Availability { get; set; }
            public decimal Efficiency { get; set; }
            public decimal Quality { get; set; }
            public decimal OEE { get; set; }
            public decimal UnitTime { get; set; }

            public MoldRecord(string code, string machineOnTime, string productionTime, string stopTime, decimal index, decimal oreProdTeorico, decimal cadency, decimal qtyShotProd, decimal qtyProd, decimal waste)
            {
                Code = code;
                MachineOnTime = machineOnTime;
                Production = productionTime;
                Stop = stopTime;
                Index = index;
                OreProdTeorico = oreProdTeorico;
                Cadency = cadency;
                QtyShotProd = qtyShotProd;
                QtyProd = qtyProd;
                Waste = waste;
            }

            public MoldRecord(string code, string machineOnTime, string productionTime, string stopTime, decimal availability, decimal efficiency, decimal quality, decimal oreProdTeorico, decimal cadency, decimal qtyShotProd, decimal qtyProd, decimal waste)
            {
                Code = code;
                MachineOnTime = machineOnTime;
                Production = productionTime;
                Stop = stopTime;
                Availability = availability;
                Efficiency = efficiency;
                Quality = quality;
                OreProdTeorico = oreProdTeorico;
                Cadency = cadency;
                QtyShotProd = qtyShotProd;
                QtyProd = qtyProd;
                Waste = waste;
            }

            public MoldRecord(string code, string machineOnTime, string productionTime, string stopTime, decimal availability, decimal efficiency, decimal quality, decimal oreProdTeorico, decimal cadency, decimal unitTime, decimal qtyShotProd, decimal qtyProd, decimal waste)
            {
                Code = code;
                MachineOnTime = machineOnTime;
                Production = productionTime;
                Stop = stopTime;
                Availability = availability;
                Efficiency = efficiency;
                Quality = quality;
                OreProdTeorico = oreProdTeorico;
                Cadency = cadency;
                UnitTime = unitTime;
                QtyShotProd = qtyShotProd;
                QtyProd = qtyProd;
                Waste = waste;
            }
        }

        protected DateTime? DateStart
        {
            get
            {
                if (ViewState["IndexAnalysisDateStart"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["IndexAnalysisDateStart"]);
            }
            set
            {
                ViewState["IndexAnalysisDateStart"] = value;
            }
        }

        protected DateTime? DateEnd
        {
            get
            {
                if (ViewState["IndexAnalysisDateEnd"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["IndexAnalysisDateEnd"]);
            }
            set
            {
                ViewState["IndexAnalysisDateEnd"] = value;
            }
        }

        protected void gridMachine_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            List<MoldRecord> dataSource = new List<MoldRecord>();
            TransactionMoldRepository TRep = new TransactionMoldRepository();
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

            //string departmentId = departmentFilter.Entries.Count > 0 ? departmentFilter.Entries[0].Value : string.Empty;
            List<Mold> moldList = new List<Mold>();
            //if (!string.IsNullOrEmpty(departmentId))
            //{
                //AssetService AService = new AssetService();
                //moldList = AService.GetAssetsByDepartment(departmentId);
            //}
            //else
            //{
                //AssetRepository ARep = new AssetRepository();
                //moldList = ARep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && !string.IsNullOrEmpty(x.ZoneId)).OrderBy(x => x.Code.Length).ThenBy(x => x.Code).ToList();
                MoldRepository mRep = new MoldRepository();
                moldList = mRep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Name).ToList();
            //}
            foreach (Mold item in moldList)
            {
                List<TransactionMold> tList = new List<TransactionMold>();
                tList = TRep.ReadAll(x => x.MoldId == item.Id && x.Start >= DateStart && x.Start <= DateEnd).ToList();
                
                TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                decimal index = 0;
                decimal oreProdTeorico = 0;
                decimal qtyShotProd = 0;
                decimal waste = 0;
                decimal qtyProd = 0;
                decimal cadency = 0;

                if (productionTime.TotalSeconds > 0)
                {
                    qtyShotProd = tList.Sum(x => x.PartialCounting);
                    cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                    foreach (Job job in tList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct())
                    {
                        if (job != null)
                        {
                            if (job.StandardRate > 0)
                            {
                                oreProdTeorico += Math.Round(tList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) / job.StandardRate, 2);
                            }
                            qtyProd += tList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1);
                        }
                    }
                    try
                    {
                        waste = Math.Round(jpwRep.ReadAll(x => x.MoldId == item.Id && x.Date >= DateStart && x.Date <= DateEnd).Sum(x => x.QtyProductionWaste), 0);
                    }
                    catch (InvalidOperationException) { }
                }

                switch (rbtnIndex.SelectedValue)
                {
                    case "Availability":
                        index = indexService.Availability(productionTime, machineOn);
                        break;
                    case "Efficiency":
                        index = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
                        break;
                    case "Quality":
                        index = indexService.Quality(qtyProd, waste);
                        break;
                }

                dataSource.Add(new MoldRecord(
                    string.Format("{0}", item.Name.Trim()),
                    string.Format("{0}:{1:d2}:{2:d2}", (int)machineOn.TotalHours, machineOn.Minutes, machineOn.Seconds),
                    string.Format("{0}:{1:d2}:{2:d2}", (int)productionTime.TotalHours, productionTime.Minutes, productionTime.Seconds),
                    string.Format("{0}:{1:d2}:{2:d2}", (int)stopTime.TotalHours, stopTime.Minutes, stopTime.Seconds),
                    index,
                    oreProdTeorico,
                    cadency,
                    qtyShotProd,
                    qtyProd,
                    waste));
            }

            TimeSpan totMachineOn = TimeSpan.FromSeconds(dataSource.Sum(
                x => new TimeSpan((Convert.ToInt32(x.MachineOnTime.Split(':')[0])), Convert.ToInt32(x.MachineOnTime.Split(':')[1]), Convert.ToInt32(x.MachineOnTime.Split(':')[2])).TotalSeconds));
            TimeSpan totProd = TimeSpan.FromSeconds(dataSource.Sum(
                x => new TimeSpan((Convert.ToInt32(x.Production.Split(':')[0])), Convert.ToInt32(x.Production.Split(':')[1]), Convert.ToInt32(x.Production.Split(':')[2])).TotalSeconds));
            TimeSpan totStop = TimeSpan.FromSeconds(dataSource.Sum(
                x => new TimeSpan((Convert.ToInt32(x.Stop.Split(':')[0])), Convert.ToInt32(x.Stop.Split(':')[1]), Convert.ToInt32(x.Stop.Split(':')[2])).TotalSeconds));
            decimal indexTot = 0;
            decimal qtyTot = dataSource.Sum(x => x.QtyProd);
            decimal wasteTot = dataSource.Sum(x => x.Waste);
            decimal oreProdTeorTot = dataSource.Sum(x => x.OreProdTeorico);
            decimal cadencyTot = 0;
            if (totProd.TotalSeconds > 0)
            {
                cadencyTot = Math.Round(dataSource.Sum(x => x.QtyShotProd) / (decimal)totProd.TotalHours, 2);
            }
            switch (rbtnIndex.SelectedValue)
            {
                case "Availability":
                    indexTot = indexService.Availability(totProd, totMachineOn);
                    break;
                case "Efficiency":
                    if (dataSource.Where(x => x.OreProdTeorico > 0).Any())
                    {
                        //solo le ore di produzione delle macchine che avevano le stampateOra teoriche
                        TimeSpan totProdWithCadency = TimeSpan.FromSeconds(dataSource.Where(x => x.OreProdTeorico > 0).Sum(
                            x => new TimeSpan((Convert.ToInt32(x.Production.Split(':')[0])), Convert.ToInt32(x.Production.Split(':')[1]), Convert.ToInt32(x.Production.Split(':')[2])).TotalSeconds));
                        indexTot = indexService.Efficiency((decimal)totProdWithCadency.TotalHours, oreProdTeorTot);
                    }
                    break;
                case "Quality":
                    indexTot = indexService.Quality(qtyTot, wasteTot);
                    break;
            }
            dataSource.Insert(0, new MoldRecord(
                "MEDIA TOTALE",
                string.Format("{0}", (int)totMachineOn.TotalHours),// totMachineOn.Minutes, totMachineOn.Seconds),
                string.Format("{0}", (int)totProd.TotalHours),// totProd.Minutes, totProd.Seconds),
                string.Format("{0}", (int)totStop.TotalHours),// totStop.Minutes, totStop.Seconds),
                indexTot,
                oreProdTeorTot,
                cadencyTot,
                0,
                qtyTot,
                wasteTot));
            gridMachine.DataSource = dataSource;
        }

        protected void gridMachine_ItemDataBound(object sender, GridItemEventArgs e)
        {
            ConfigurationService configService = new ConfigurationService();

            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                if (item.GetDataKeyValue("Code").ToString() == "MEDIA TOTALE")
                {
                    item["Code"].Font.Bold = true;
                }

                decimal d = Convert.ToDecimal(item.GetDataKeyValue("Index"));
                RadHtmlChart chart = item["ChartColumn"].FindControl("RadHtmlChart1") as RadHtmlChart;
                BarSeries s = chart.PlotArea.Series[0] as BarSeries;
                s.Appearance.FillStyle.BackgroundColor = configService.GetColorByParameter(rbtnIndex.SelectedValue, d);
                s.Items.Clear();
                s.Items.Add(d);
                chart.DataBind();
            }
        }

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            DateStart = dtPickStart.SelectedDate;
            DateEnd = dtPickEnd.SelectedDate;
            if (DateEnd != null)
            {
                DateEnd = DateEnd.Value.AddDays(1);
            }
            switch (rbtnIndex.SelectedValue)
            {
                case "Availability":
                    gridOEE.Visible = false;
                    gridValue.Visible = false;
                    gridMachine.Visible = true;
                    gridMachine.MasterTableView.GetColumn("QtyProd").Display = false;
                    gridMachine.MasterTableView.GetColumn("Waste").Display = false;
                    gridMachine.MasterTableView.GetColumn("Cadency").Display = false;
                    gridMachine.MasterTableView.GetColumn("StampateOraTeorico").Display = false;
                    gridMachine.MasterTableView.GetColumn("OreProdTeorico").Display = false;
                    gridMachine.Rebind();
                    break;
                case "Efficiency":
                    gridOEE.Visible = false;
                    gridValue.Visible = false;
                    gridMachine.Visible = true;
                    gridMachine.MasterTableView.GetColumn("QtyProd").Display = false;
                    gridMachine.MasterTableView.GetColumn("Waste").Display = false;
                    gridMachine.MasterTableView.GetColumn("Cadency").Display = true;
                    //gridMachine.MasterTableView.GetColumn("StampateOraTeorico").Display = true;
                    gridMachine.MasterTableView.GetColumn("OreProdTeorico").Display = true;
                    gridMachine.Rebind();
                    break;
                case "Quality":
                    gridOEE.Visible = false;
                    gridValue.Visible = false;
                    gridMachine.Visible = true;
                    gridMachine.MasterTableView.GetColumn("QtyProd").Display = true;
                    gridMachine.MasterTableView.GetColumn("Waste").Display = true;
                    gridMachine.MasterTableView.GetColumn("Cadency").Display = false;
                    gridMachine.MasterTableView.GetColumn("StampateOraTeorico").Display = false;
                    gridMachine.MasterTableView.GetColumn("OreProdTeorico").Display = false;
                    gridMachine.Rebind();
                    break;
                case "OEE":
                    gridMachine.Visible = false;
                    gridValue.Visible = false;
                    gridOEE.Visible = true;
                    gridOEE.Rebind();
                    break;
                case "OEEvalue":
                    gridMachine.Visible = false;
                    gridOEE.Visible = false;
                    gridValue.Visible = true;
                    gridValue.Rebind();
                    break;
            }
        }

        protected void gridOEE_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            List<MoldRecord> dataSource = new List<MoldRecord>();
            TransactionMoldRepository TRep = new TransactionMoldRepository();
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

            //string departmentId = departmentFilter.Entries.Count > 0 ? departmentFilter.Entries[0].Value : string.Empty;
            List<Mold> moldList = new List<Mold>();
            //if (!string.IsNullOrEmpty(departmentId))
            //{
                //AssetService AService = new AssetService();
                //moldList = AService.GetAssetsByDepartment(departmentId);
            //}
            //else
            //{
                //AssetRepository ARep = new AssetRepository();
                //moldList = ARep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && !string.IsNullOrEmpty(x.ZoneId)).OrderBy(x => x.Code.Length).ThenBy(x => x.Code).ToList();
                MoldRepository mRep = new MoldRepository();
                moldList = mRep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Name).ToList();
            //}

            foreach (Mold item in moldList)
            {
                List<TransactionMold> tList = new List<TransactionMold>();
                tList = TRep.ReadAll(x => x.MoldId == item.Id && x.Start >= DateStart && x.Start <= DateEnd).ToList();

                TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                decimal oreProdTeorico = 0;
                decimal qtyShotProd = 0;
                decimal waste = 0;
                decimal qtyProd = 0;
                decimal cadency = 0;
                decimal availability = 0;
                decimal efficiency = 0;
                decimal quality = 0;

                if (productionTime.TotalSeconds > 0)
                {
                    qtyShotProd = tList.Sum(x => x.PartialCounting);
                    cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                    foreach (Job job in tList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct())
                    {
                        if (job != null)
                        {
                            if (job.StandardRate > 0)
                            {
                                oreProdTeorico += Math.Round(tList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) / job.StandardRate, 2);
                            }
                            qtyProd += tList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1);
                        }
                    }
                    try
                    {
                        waste = Math.Round(jpwRep.ReadAll(x => x.MoldId == item.Id && x.Date >= DateStart && x.Date <= DateEnd).Sum(x => x.QtyProductionWaste), 0);
                    }
                    catch (InvalidOperationException) { }
                }

                availability = indexService.Availability(productionTime, machineOn);
                efficiency = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
                quality = indexService.Quality(qtyProd, waste);

                dataSource.Add(new MoldRecord(
                    string.Format("{0}", item.Name.Trim()),
                    string.Format("{0}:{1:d2}:{2:d2}", (int)machineOn.TotalHours, machineOn.Minutes, machineOn.Seconds),
                    string.Format("{0}:{1:d2}:{2:d2}", (int)productionTime.TotalHours, productionTime.Minutes, productionTime.Seconds),
                    string.Format("{0}:{1:d2}:{2:d2}", (int)stopTime.TotalHours, stopTime.Minutes, stopTime.Seconds),
                    availability,
                    efficiency,
                    quality,
                    oreProdTeorico,
                    cadency,
                    qtyShotProd,
                    qtyProd,
                    waste));
            }

            TimeSpan totMachineOn = TimeSpan.FromSeconds(dataSource.Sum(
                x => new TimeSpan((Convert.ToInt32(x.MachineOnTime.Split(':')[0])), Convert.ToInt32(x.MachineOnTime.Split(':')[1]), Convert.ToInt32(x.MachineOnTime.Split(':')[2])).TotalSeconds));
            TimeSpan totProd = TimeSpan.FromSeconds(dataSource.Sum(
                x => new TimeSpan((Convert.ToInt32(x.Production.Split(':')[0])), Convert.ToInt32(x.Production.Split(':')[1]), Convert.ToInt32(x.Production.Split(':')[2])).TotalSeconds));
            TimeSpan totStop = TimeSpan.FromSeconds(dataSource.Sum(
                x => new TimeSpan((Convert.ToInt32(x.Stop.Split(':')[0])), Convert.ToInt32(x.Stop.Split(':')[1]), Convert.ToInt32(x.Stop.Split(':')[2])).TotalSeconds));
            decimal qtyTot = dataSource.Sum(x => x.QtyProd);
            decimal wasteTot = dataSource.Sum(x => x.Waste);
            decimal oreProdTeorTot = dataSource.Sum(x => x.OreProdTeorico);
            decimal cadencyTot = 0;
            if (totProd.TotalSeconds > 0)
            {
                cadencyTot = Math.Round(dataSource.Sum(x => x.QtyShotProd) / (decimal)totProd.TotalHours, 2);
            }

            decimal availabilityTot = indexService.Availability(totProd, totMachineOn);
            decimal efficiencyTot = 0;
            if (dataSource.Where(x => x.OreProdTeorico > 0).Any())
            {
                //solo le ore di produzione delle macchine che avevano le stampateOra teoriche
                TimeSpan totProdWithCadency = TimeSpan.FromSeconds(dataSource.Where(x => x.OreProdTeorico > 0).Sum(
                    x => new TimeSpan((Convert.ToInt32(x.Production.Split(':')[0])), Convert.ToInt32(x.Production.Split(':')[1]), Convert.ToInt32(x.Production.Split(':')[2])).TotalSeconds));
                efficiencyTot = indexService.Efficiency((decimal)totProdWithCadency.TotalHours, oreProdTeorTot);
            }
            decimal qualityTot = indexService.Quality(qtyTot, wasteTot);

            dataSource.Insert(0, new MoldRecord(
                "MEDIA TOTALE",
                string.Format("{0}:{1:d2}:{2:d2}", (int)totMachineOn.TotalHours, totMachineOn.Minutes, totMachineOn.Seconds),
                string.Format("{0}:{1:d2}:{2:d2}", (int)totProd.TotalHours, totProd.Minutes, totProd.Seconds),
                string.Format("{0}:{1:d2}:{2:d2}", (int)totStop.TotalHours, totStop.Minutes, totStop.Seconds),
                availabilityTot,
                efficiencyTot,
                qualityTot,
                oreProdTeorTot,
                cadencyTot,
                0,
                qtyTot,
                wasteTot));

            gridOEE.DataSource = dataSource;
        }

        protected void gridOEE_ItemDataBound(object sender, GridItemEventArgs e)
        {
            ConfigurationService configService = new ConfigurationService();

            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                if (item.GetDataKeyValue("Code").ToString() == "MEDIA TOTALE")
                {
                    item["Code"].Font.Bold = true;
                }

                decimal d = Convert.ToDecimal(item.GetDataKeyValue("Availability"));
                RadHtmlChart chart = item["DisponibilityColumn"].FindControl("DisponibilityChart") as RadHtmlChart;
                BarSeries s = chart.PlotArea.Series[0] as BarSeries;
                s.Appearance.FillStyle.BackgroundColor = configService.GetColorByParameter(ParamContext.Availability, d);
                s.Items.Clear();
                s.Items.Add(d);
                chart.DataBind();

                decimal eff = Convert.ToDecimal(item.GetDataKeyValue("Efficiency"));
                chart = item["EfficiencyColumn"].FindControl("EfficiencyChart") as RadHtmlChart;
                s = chart.PlotArea.Series[0] as BarSeries;
                s.Appearance.FillStyle.BackgroundColor = configService.GetColorByParameter(ParamContext.Efficiency, eff);
                s.Items.Clear();
                s.Items.Add(eff);
                chart.DataBind();

                decimal q = Convert.ToDecimal(item.GetDataKeyValue("Quality"));
                chart = item["QualityColumn"].FindControl("QualityChart") as RadHtmlChart;
                s = chart.PlotArea.Series[0] as BarSeries;
                s.Appearance.FillStyle.BackgroundColor = configService.GetColorByParameter(ParamContext.Quality, q);
                s.Items.Clear();
                s.Items.Add(q);
                chart.DataBind();

                decimal oee = Math.Round(((d / 100) * (eff / 100) * (q / 100)) * 100, 2);
                item["OEE"].Text = oee.ToString();
                chart = item["OEEColumn"].FindControl("OEEChart") as RadHtmlChart;
                s = chart.PlotArea.Series[0] as BarSeries;
                s.Appearance.FillStyle.BackgroundColor = configService.GetColorByParameter(ParamContext.OEE, oee);
                s.Items.Clear();
                s.Items.Add(oee);
                chart.DataBind();
            }
        }

        protected void gridValue_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            List<MoldRecord> dataSource = new List<MoldRecord>();
            TransactionMoldRepository TRep = new TransactionMoldRepository();
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

            //string departmentId = departmentFilter.Entries.Count > 0 ? departmentFilter.Entries[0].Value : string.Empty;
            List<Mold> moldList = new List<Mold>();
            //if (!string.IsNullOrEmpty(departmentId))
            //{
                //AssetService AService = new AssetService();
                //assetList = AService.GetAssetsByDepartment(departmentId);
            //}
            //else
            //{
                //AssetRepository ARep = new AssetRepository();
                //assetList = ARep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && !string.IsNullOrEmpty(x.ZoneId)).OrderBy(x => x.Code.Length).ThenBy(x => x.Code).ToList();
                MoldRepository mRep = new MoldRepository();
                moldList = mRep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Name).ToList();
            //}

            foreach (Mold item in moldList)
            {
                List<TransactionMold> tList = new List<TransactionMold>();
                tList = TRep.ReadAll(x => x.MoldId == item.Id && x.Start >= DateStart && x.Start <= DateEnd).ToList();

                TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                decimal oreProdTeorico = 0;
                decimal qtyShotProd = 0;
                decimal waste = 0;
                decimal qtyProd = 0;
                decimal cadency = 0;
                decimal unitTime = 0;

                if (productionTime.TotalSeconds > 0)
                {
                    qtyShotProd = tList.Sum(x => x.PartialCounting);
                    cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                    unitTime = Math.Round((decimal)productionTime.TotalHours / qtyShotProd, 2);
                    foreach (Job job in tList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct())
                    {
                        if (job != null)
                        {
                            if (job.StandardRate > 0)
                            {
                                oreProdTeorico += Math.Round(tList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) / job.StandardRate, 2);
                            }
                            qtyProd += tList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1);
                        }
                    }
                    try
                    {
                        waste = Math.Round(jpwRep.ReadAll(x => x.MoldId == item.Id && x.Date >= DateStart && x.Date <= DateEnd).Sum(x => x.QtyProductionWaste), 0);
                    }
                    catch (InvalidOperationException) { }
                }

                dataSource.Add(new MoldRecord(
                    string.Format("{0}", item.Name.Trim()),
                    string.Format("{0}:{1:d2}:{2:d2}", (int)machineOn.TotalHours, machineOn.Minutes, machineOn.Seconds),
                    string.Format("{0}:{1:d2}:{2:d2}", (int)productionTime.TotalHours, productionTime.Minutes, productionTime.Seconds),
                    string.Format("{0}:{1:d2}:{2:d2}", (int)stopTime.TotalHours, stopTime.Minutes, stopTime.Seconds),
                    indexService.Availability(productionTime, machineOn),
                    indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico),
                    indexService.Quality(qtyProd, waste),
                    oreProdTeorico,
                    cadency,
                    unitTime,
                    qtyShotProd,
                    qtyProd,
                    waste));
            }

            TimeSpan totMachineOn = TimeSpan.FromSeconds(dataSource.Sum(
                x => new TimeSpan((Convert.ToInt32(x.MachineOnTime.Split(':')[0])), Convert.ToInt32(x.MachineOnTime.Split(':')[1]), Convert.ToInt32(x.MachineOnTime.Split(':')[2])).TotalSeconds));
            TimeSpan totProd = TimeSpan.FromSeconds(dataSource.Sum(
                x => new TimeSpan((Convert.ToInt32(x.Production.Split(':')[0])), Convert.ToInt32(x.Production.Split(':')[1]), Convert.ToInt32(x.Production.Split(':')[2])).TotalSeconds));
            TimeSpan totStop = TimeSpan.FromSeconds(dataSource.Sum(
                x => new TimeSpan((Convert.ToInt32(x.Stop.Split(':')[0])), Convert.ToInt32(x.Stop.Split(':')[1]), Convert.ToInt32(x.Stop.Split(':')[2])).TotalSeconds));
            decimal qtyTot = dataSource.Sum(x => x.QtyProd);
            decimal wasteTot = dataSource.Sum(x => x.Waste);
            decimal oreProdTeorTot = dataSource.Sum(x => x.OreProdTeorico);
            decimal cadencyTot = 0;
            decimal unitTimeTot = 0;
            if (totProd.TotalSeconds > 0)
            {
                cadencyTot = Math.Round(dataSource.Sum(x => x.QtyShotProd) / (decimal)totProd.TotalHours, 2);
                unitTimeTot = Math.Round((decimal)totProd.TotalHours / dataSource.Sum(x => x.QtyShotProd), 2);
            }

            decimal availabilityTot = indexService.Availability(totProd, totMachineOn);
            decimal efficiencyTot = 0;
            if (dataSource.Where(x => x.OreProdTeorico > 0).Any())
            {
                //solo le ore di produzione delle macchine che avevano le stampateOra teoriche
                TimeSpan totProdWithCadency = TimeSpan.FromSeconds(dataSource.Where(x => x.OreProdTeorico > 0).Sum(
                    x => new TimeSpan((Convert.ToInt32(x.Production.Split(':')[0])), Convert.ToInt32(x.Production.Split(':')[1]), Convert.ToInt32(x.Production.Split(':')[2])).TotalSeconds));
                efficiencyTot = indexService.Efficiency((decimal)totProdWithCadency.TotalHours, oreProdTeorTot);
            }
            decimal qualityTot = indexService.Quality(qtyTot, wasteTot);

            dataSource.Insert(0, new MoldRecord(
                "MEDIA TOTALE",
                string.Format("{0}:{1:d2}:{2:d2}", (int)totMachineOn.TotalHours, totMachineOn.Minutes, totMachineOn.Seconds),
                string.Format("{0}:{1:d2}:{2:d2}", (int)totProd.TotalHours, totProd.Minutes, totProd.Seconds),
                string.Format("{0}:{1:d2}:{2:d2}", (int)totStop.TotalHours, totStop.Minutes, totStop.Seconds),
                availabilityTot,
                efficiencyTot,
                qualityTot,
                oreProdTeorTot,
                cadencyTot,
                unitTimeTot,
                0,
                qtyTot,
                wasteTot));

            gridValue.DataSource = dataSource;
        }

        protected void gridValue_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                if (item.GetDataKeyValue("Code").ToString() == "MEDIA TOTALE")
                {
                    item["Code"].Font.Bold = true;
                }
            }
        }
    }
}