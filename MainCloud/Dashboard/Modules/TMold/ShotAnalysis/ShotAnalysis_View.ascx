﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShotAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.ShotAnalysis.ShotAnalysis_View" %>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function PopUpShowing(sender, eventArgs) {
            var popUp = eventArgs.get_popUp();
            var scrollPosition = $('#<%= Page.Master.FindControl("ScrollPosition").ClientID %>').val();
            popUp.style.top = scrollPosition + "px";
        }
    </script>
</telerik:RadCodeBlock>

<style type="text/css">
    .RadPicker.RadDateTimePicker.RadPicker_Bootstrap.rfControl{
        width: 211px !important;
    }
</style>

<div class="panel panel-info">
    <%--<div class="panel-heading">Filtro selezione</div>--%>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="machineFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px"
                    TextSettings-SelectionMode="Single" DataSourceID="edsMold" DataTextField="Name" DataValueField="Id" InputType="Text" Filter="Contains" Label="Stampo: ">
                </telerik:RadAutoCompleteBox>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <div style="text-align:right">
            <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
        </div>
    </div>
</div>

<br />

<telerik:RadHtmlChart runat="server" ID="ProgressiveChart" Skin="Silk">
    <PlotArea>
        <Series>
            <telerik:ScatterLineSeries>
                <%--<LineAppearance Width="5" />--%>
                <TooltipsAppearance>
                    <ClientTemplate>
                        #= kendo.format(\'{0:HH:mm}\', new Date(value.x)) #,  #= value.y #
                    </ClientTemplate>
                </TooltipsAppearance>
            </telerik:ScatterLineSeries>
        </Series>
        <YAxis>
            <TitleAppearance Text="Qtà colpi" />
            <%--<MajorGridLines Visible="false" />--%>
            <%--<MinorGridLines Visible="false" />--%>
        </YAxis>
        <XAxis Type="Date">
            <%--<MajorGridLines Visible="false" />--%>
            <%--<MinorGridLines Visible="false" />--%>
        </XAxis>
    </PlotArea>
</telerik:RadHtmlChart>

<telerik:RadHtmlChart runat="server" ID="StepChart" Skin="Silk">
    <Legend>
        <Appearance Position="Bottom"></Appearance>
    </Legend>
    <PlotArea>
        <Series>
            <telerik:ScatterLineSeries>
                <TooltipsAppearance>
                    <ClientTemplate>
                        #= kendo.format(\'{0:HH:mm}\', new Date(value.x)) #,  #= value.y #
                    </ClientTemplate>
                </TooltipsAppearance>
            </telerik:ScatterLineSeries>
            <telerik:ScatterLineSeries>
                <TooltipsAppearance>
                    <ClientTemplate>
                        Cadenza oraria prevista = #= value.y #
                    </ClientTemplate>
                </TooltipsAppearance>
                <MarkersAppearance Size="0" />
                <LabelsAppearance Visible="false"></LabelsAppearance>
                <Appearance>
                    <FillStyle BackgroundColor="#e8481b" />  <%--#2dbbff--%>
                </Appearance>
            </telerik:ScatterLineSeries>
        </Series>
        <YAxis>
            <TitleAppearance Text="Produzione oraria">
                <TextStyle Margin="9" />
            </TitleAppearance>
        </YAxis>
        <XAxis Type="Date">
        </XAxis>
    </PlotArea>
</telerik:RadHtmlChart>

<br />
        
<asp:UpdatePanel runat="server">
    <ContentTemplate>
    
<telerik:RadGrid runat="server" ID="GridCounting" RenderMode="Lightweight" AutoGenerateColumns="false" AllowAutomaticUpdates="false" AllowPaging="true"
    OnItemDataBound="GridCounting_ItemDataBound" OnNeedDataSource="GridCounting_NeedDataSource"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" EditMode="PopUp">
        <Columns>
            <telerik:GridDropDownColumn DataField="MachineId" HeaderText="Macchina"
                ListValueField="Id" ListTextField="Description" DataSourceID="edsMachine">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>
            <telerik:GridDateTimeColumn DataField="Start" HeaderText="Inizio" PickerType="DateTimePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="End" HeaderText="Fine" PickerType="DateTimePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn DataField="Duration" HeaderText="Durata [m]">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Cause.Description" HeaderText="Causale">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridDropDownColumn UniqueName="ddlCause" HeaderText="Causale" DataField="CauseId"
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText=""
                ListTextField="Description" ListValueField="Id" DataSourceID="edsCause">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>--%>
            <%--<telerik:GridDropDownColumn UniqueName="ddlOperator" HeaderText="Operatore" DataField="OperatorId"
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText=""
                ListTextField="UserName" ListValueField="Id" DataSourceID="edsOperator">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>--%>
            <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="Operatore">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Job.Order.CustomerOrder.OrderCode" HeaderText="Commessa">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Job.Order.Article.Code" HeaderText="Articolo">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridDropDownColumn UniqueName="ddlArticle" HeaderText="Articolo" DataField="Job.Order.IdArticle"
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText=""
                ListTextField="Code" ListValueField="Id" DataSourceID="edsArticle">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>--%>
            <telerik:GridBoundColumn DataField="PartialCounting" HeaderText="Parziale" DataFormatString="{0:N0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CounterEnd" HeaderText="Progressivo" DataFormatString="{0:f0}" DataType="System.Int32">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="Media" HeaderText="Media">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <ClientEvents OnPopUpShowing="PopUpShowing" />
    </ClientSettings>
</telerik:RadGrid>

    </ContentTemplate>
</asp:UpdatePanel>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    AutoGenerateWhereClause="true" OrderBy="it.Name">
</ef:EntityDataSource>
<ef:EntityDataSource ID="edsMachine" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    AutoGenerateWhereClause="true" OrderBy="it.Code">
</ef:EntityDataSource>






<%--<link href="/bower_components/jsgrid-1.5.3/jsgrid.min.css" rel="stylesheet" type="text/css">
<link href="/bower_components/jsgrid-1.5.3/jsgrid-theme.min.css" rel="stylesheet" type="text/css" />
    
<script type="text/javascript" src="/bower_components/jsgrid-1.5.3/jsgrid.min.js"></script>

<script>
    //$(function () {
    function UpdateGrid() {
        var moldId = $find("<%= machineFilter.ClientID %>").get_entries().toArray()[0].get_value();
        var startDate = $find("<%= dtPickStart.ClientID %>").get_selectedDate().format("MM/dd/yyyy HH:mm");
        var endDate = $find("<%= dtPickEnd.ClientID %>").get_selectedDate().format("MM/dd/yyyy HH:mm");
        $("#jsGrid").jsGrid({
            height: "auto",
            width: "100%",

            filtering: false,
            inserting: false,
            editing: false,
            sorting: true,
            paging: true,
            autoload: true,

            pageSize: 10,
            pageButtonCount: 5,

            //deleteConfirm: "Do you really want to delete client?",

            controller: {
                loadData: function (filter) {
                    return $.ajax({
                        type: "GET",
                        url: "/api/TMold/Transaction?moldId=" + moldId + "&startDate=" + startDate + "&endDate=" + endDate,
                        data: filter,
                        dataType: "json"
                    });
                }

                //insertItem: function(item) {
                //    return $.ajax({
                //        type: "POST",
                //        url: "/api/data",
                //        data: item,
                //        dataType: "json"
                //    });
                //},

                //updateItem: function(item) {
                //    return $.ajax({
                //        type: "PUT",
                //        url: "/api/data/" + item.ID,
                //        data: item,
                //        dataType: "json"
                //    });
                //},

                //deleteItem: function(item) {
                //    return $.ajax({
                //        type: "DELETE",
                //        url: "/api/data/" + item.ID,
                //        dataType: "json"
                //    });
                //}
            },

            onRefreshed: function (args) {
                var items = args.grid.option("data");
                var total = { Name: "Tot", "PartialCounting": 0, IsTotal: true };

                items.forEach(function (item) {
                    total.PartialCounting += item.PartialCounting;
                    //total.FundforForum += item.FundforForum;
                });

                //args.grid._content.append("<tr><td colspan=\"2\" class=\"jsgrid-cell jsgrid-align-right\" style=\"width: 80px;\"><strong>Total</strong></td>"
                //+ "<td class=\"jsgrid-cell jsgrid-align-right\"><strong>" + total.NoOfCoveredUpazila + "</strong></td>"
                //+ "<td class=\"jsgrid-cell jsgrid-align-right\"><strong>" + total.FundforForum + "</strong></td>"
                //+ "<td class=\"jsgrid-cell jsgrid-align-right\"><strong>" + total.TotalPOContribution + "</strong></td>"
                //+ "<td class=\"jsgrid-cell jsgrid-align-right\"><strong>" + total.RemainingPOContribution + "</strong></td></tr>");

                var $totalRow = $("<tr>");//.addClass("total-row");

                args.grid._renderCells($totalRow, total);

                args.grid._content.append($totalRow);
            },

            fields: [
                { name: "Start", type: "text" },
                { name: "End", type: "text" },
                { name: "Duration", type: "number", filtering: false },
                { name: "PartialCounting", type: "number", title: "Parziale" },
                { name: "ProgressiveCounter", type: "number", title: "Progressivo", filtering: false }
                //{ type: "control" }
            ]
        });
    }
        //});
</script>

<script src="/Scripts/moment.js"></script>
<script src="/bower_components/chart.js-2.7.3/dist/Chart.js"></script>
<script type="text/javascript">
    
    var option = {
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                stacked: true,
                gridLines: {
                    display: true,
                    color: "rgba(255,99,132,0.2)"
                }
            }],
            xAxes: [{
                type: 'time',
                unit: 'day',
                time: {
                    displayFormats: {
                        'millisecond': 'dd',
                        'second': 'dd',
                        'minute': 'dd',
                        'hour': 'dd',
                        'day': 'dd',
                        'week': 'dd',
                        'month': 'dd',
                        'quarter': 'dd',
                        'year': 'dd',
                    }
                },
                distribution: 'series',
                gridLines: {
                    display: false
                }
            }]
        }
    };

    function ApplyClick() {
        UpdateChart();
        UpdateGrid();
    }

    function UpdateChart() {
        var moldId = $find("<%= machineFilter.ClientID %>").get_entries().toArray()[0].get_value();
        var startDate = $find("<%= dtPickStart.ClientID %>").get_selectedDate().format("MM/dd/yyyy HH:mm");
        var endDate = $find("<%= dtPickEnd.ClientID %>").get_selectedDate().format("MM/dd/yyyy HH:mm");
        $.ajax({
            type: "GET",
            url: "/api/TMold/Transaction?moldId=" + moldId + "&startDate=" + startDate + "&endDate=" + endDate,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                label = [];
                array = [];
                data.forEach(function (transaction) {
                    array.push(transaction.ProgressiveCounter);
                    label.push(new Date(transaction.Start).format("dd/MM/yyyy HH:mm"));
                });

                new Chart(document.getElementById("myChart"), {
                    type: 'line',
                    data: {
                        labels: label,
                        datasets: [
                            {
                                label: "Progressivo",
                                data: array,
                                fill: false,
                                borderColor: 'rgba(75,192,192,1)',
                                lineTension: 0.1,
                                borderWidth: 1,//3,
                                pointBackgroundColor: '#fff',
                                pointBorderWidth: 1,
                                pointHoverRadius: 5,
                                borderCapStyle: 'square',
                                borderJoinStyle: 'square',
                                pointHitRadius: 1,//20,
                                pointStyle: 'circle',
                            }
                        ]
                    },
                    options: {
                        legend: { display: false },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    min: 0
                                },
                                time: {
                                    unit: 'year'
                                }
                            }]
                        }
                    }
                    //options: option
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //$("#getPeopleResult").val(jqXHR.statusText);
            }
        });
    }
</script>

<asp:UpdatePanel runat="server">
    <ContentTemplate>

<canvas id="myChart"></canvas>
    </ContentTemplate>
</asp:UpdatePanel>
<div id="jsGrid"></div>--%>