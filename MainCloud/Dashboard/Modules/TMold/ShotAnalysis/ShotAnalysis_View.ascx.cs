﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.HtmlChart.Enums;

namespace MainCloud.Dashboard.Modules.TMold.ShotAnalysis
{
    public partial class ShotAnalysis_View : DataWidget<ShotAnalysis_View>
    {
        public ShotAnalysis_View() : base(typeof(ShotAnalysis_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }

            if (!IsPostBack || !string.IsNullOrEmpty(MoldId))
            {
                ProgressiveChart.Visible = false;
                StepChart.Visible = false;
                //GridCounting.Visible = false;
            }

            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsMachine.WhereParameters.Clear();
            edsMachine.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        protected string MoldId
        {
            get
            {
                if (string.IsNullOrEmpty((string)ViewState["ShotAnalysisMoldId"]))
                {
                    return null;
                }
                return ViewState["ShotAnalysisMoldId"].ToString();
            }
            set
            {
                ViewState["ShotAnalysisMoldId"] = value;
            }
        }

        protected DateTime? DateStart
        {
            get
            {
                if (ViewState["ShotAnalysisDateStart"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["ShotAnalysisDateStart"]);
            }
            set
            {
                ViewState["ShotAnalysisDateStart"] = value;
            }
        }

        protected DateTime? DateEnd
        {
            get
            {
                if (ViewState["ShotAnalysisDateEnd"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["ShotAnalysisDateEnd"]);
            }
            set
            {
                ViewState["ShotAnalysisDateEnd"] = value;
            }
        }

        protected void CreateChart(List<TransactionMold> dataSource, DateTime start, DateTime end)
        {
            if (dataSource.Count > 0)
            {
                GridCounting.DataSource = dataSource;
                GridCounting.DataBind();

                DateTime epoch = new DateTime(1970, 1, 1);
                DateTime dtStart = new DateTime(dataSource.First().Start.Year, dataSource.First().Start.Month, dataSource.First().Start.Day).ToUniversalTime();
                DateTime dtEnd = new DateTime(dataSource.Last().Start.Year, dataSource.Last().Start.Month, dataSource.Last().Start.Day).ToUniversalTime();

                //Set min and max date for a datetime x-axis type
                ProgressiveChart.PlotArea.XAxis.MinValue = (decimal)dtStart.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                ProgressiveChart.PlotArea.XAxis.MaxValue = (decimal)dtEnd.AddDays(1).Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                ScatterLineSeries progressiveSerie = ProgressiveChart.PlotArea.Series[0] as ScatterLineSeries;//new ScatterLineSeries();
                progressiveSerie.MarkersAppearance.Visible = false;
                progressiveSerie.LabelsAppearance.Visible = false;

                foreach (var item in dataSource)
                {
                    //progressiveSerie.SeriesItems.Add((decimal)item.Start.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.ProgressiveCounter);
                    progressiveSerie.SeriesItems.Add((decimal)item.Start.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.CounterStart);
                    if (item == dataSource.Last())
                    {
                        if (item.Duration > 0)
                        {
                            //progressiveSerie.SeriesItems.Add((decimal)item.Start.AddSeconds((double)item.Duration).ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.ProgressiveCounter + item.PartialCounting);
                            progressiveSerie.SeriesItems.Add((decimal)item.Start.AddSeconds((double)item.Duration).ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.CounterEnd);
                        }
                        else
                        {
                            //progressiveSerie.SeriesItems.Add((decimal)item.LastUpdate.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.ProgressiveCounter + item.PartialCounting);
                            progressiveSerie.SeriesItems.Add((decimal)item.LastUpdate.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.CounterEnd);
                        }
                    }
                }
                //ProgressiveChart.PlotArea.Series.Add(progressiveSerie);

                //int? frequency = null;
                ScatterLineSeries frequencySerie = StepChart.PlotArea.Series[1] as ScatterLineSeries;
                //string moldId = dataSource.Where(x => !string.IsNullOrEmpty(x.CountingAnalyzerId)).Select(x => x.CountingAnalyzerId).First();
                //if (!string.IsNullOrEmpty(moldId))
                //{
                //    MoldRepository moldRepos = new MoldRepository();
                //    try
                //    {
                //        frequency = moldRepos.ReadAll(x => x.Id == moldId && x.ApplicationId == MultiTenantsHelper.ApplicationId).First().Frequency;
                //    }
                //    catch (Exception)
                //    { }
                //}

                StepChart.PlotArea.XAxis.MinValue = (decimal)dtStart.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                StepChart.PlotArea.XAxis.MaxValue = (decimal)dtEnd.AddDays(1).Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

                //add static line
                //if (frequency.HasValue)
                //{
                //    frequencySerie.Name = "Cadenza oraria prevista = " + frequency;
                //    frequencySerie.SeriesItems.Add((decimal)dtStart.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, frequency.Value);
                //    frequencySerie.SeriesItems.Add((decimal)dtEnd.AddDays(1).Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, frequency.Value);
                //}

                ScatterLineSeries stepSerie = StepChart.PlotArea.Series[0] as ScatterLineSeries;
                stepSerie.MarkersAppearance.Visible = false;
                stepSerie.LabelsAppearance.Visible = false;
                stepSerie.LineAppearance.LineStyle = LineStyle.Smooth;//ExtendedLineStyle.Step;
                foreach (var item in dataSource)
                {
                    if (item == dataSource.First())
                    {
                        stepSerie.SeriesItems.Add((decimal)item.Start.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, 0);
                    }
                    if (!string.IsNullOrEmpty(item.JobId))
                    {
                        frequencySerie.SeriesItems.Add((decimal)item.Start.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, item.Job.StandardRate);
                    }
                    if (item.Duration == 0)
                    {
                        stepSerie.SeriesItems.Add((decimal)item.Start.AddSeconds(1).ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, 0);
                        if (!item.End.HasValue)
                        {
                            if (item.Duration > 0)
                            {
                                stepSerie.SeriesItems.Add((decimal)item.Start.AddSeconds((double)item.Duration).ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, 0);
                            }
                            else
                            {
                                stepSerie.SeriesItems.Add((decimal)item.LastUpdate.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, 0);
                            }
                        }
                        else
                        {
                            stepSerie.SeriesItems.Add((decimal)item.End.Value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, 0);
                        }
                    }
                    else
                    {
                        stepSerie.SeriesItems.Add((decimal)item.Start.AddSeconds(1).ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, Math.Round(item.PartialCounting / Convert.ToDecimal(TimeSpan.FromSeconds((double)item.Duration).TotalHours), 2));
                        if (!item.End.HasValue)
                        {
                            if (item.Duration > 0)
                            {
                                stepSerie.SeriesItems.Add((decimal)item.Start.AddSeconds((double)item.Duration).ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, Math.Round(item.PartialCounting / Convert.ToDecimal(TimeSpan.FromSeconds((double)item.Duration).TotalHours), 2));
                            }
                            else
                            {
                                stepSerie.SeriesItems.Add((decimal)item.LastUpdate.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, Math.Round(item.PartialCounting / Convert.ToDecimal(TimeSpan.FromSeconds((double)(item.LastUpdate - item.Start).TotalSeconds).TotalHours), 2));
                            }
                        }
                        else
                        {
                            stepSerie.SeriesItems.Add((decimal)item.End.Value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, Math.Round(item.PartialCounting / Convert.ToDecimal(TimeSpan.FromSeconds((double)item.Duration).TotalHours), 2));
                        }
                    }
                }

                //asse x
                end = end.AddDays(-1);
                if ((end - start).Days < 7)
                {
                    ProgressiveChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Hours;
                    ProgressiveChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "HH";
                    StepChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Hours;
                    StepChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "HH";
                    switch ((end - start).Days)
                    {
                        case 0:
                            ProgressiveChart.PlotArea.XAxis.Step = 1;
                            StepChart.PlotArea.XAxis.Step = 1;
                            break;
                        case 1:
                            ProgressiveChart.PlotArea.XAxis.Step = 2;
                            StepChart.PlotArea.XAxis.Step = 2;
                            break;
                        case 2:
                            ProgressiveChart.PlotArea.XAxis.Step = 3;
                            StepChart.PlotArea.XAxis.Step = 3;
                            break;
                        case 3:
                        case 4:
                            ProgressiveChart.PlotArea.XAxis.Step = 4;
                            StepChart.PlotArea.XAxis.Step = 4;
                            break;
                        case 5:
                        case 6:
                        case 7:
                            ProgressiveChart.PlotArea.XAxis.Step = 5;
                            StepChart.PlotArea.XAxis.Step = 5;
                            break;
                    }
                }
                else
                {
                    ProgressiveChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Days;
                    ProgressiveChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "dd";
                    StepChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Days;
                    StepChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "dd";
                }
            }
            else
            {
                GridCounting.DataSource = dataSource;
                GridCounting.DataBind();
            }
        }

        protected void GridCounting_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                dataItem["Duration"].Text = Math.Round(TimeSpan.FromSeconds(Convert.ToDouble(dataItem["Duration"].Text)).TotalMinutes, 0).ToString();
                if (dataItem["Duration"].Text != "0")
                {
                    dataItem["Media"].Text = Math.Round(Convert.ToDecimal(dataItem["PartialCounting"].Text) / Convert.ToDecimal(TimeSpan.FromMinutes(Convert.ToDouble(dataItem["Duration"].Text)).TotalHours), 2).ToString();
                }
                else
                {
                    dataItem["Media"].Text = "0";
                }
                if (dataItem["CounterEnd"].Text != "&nbsp;" && !string.IsNullOrEmpty(dataItem["CounterEnd"].Text))
                {
                    dataItem["CounterEnd"].Text = (Convert.ToDecimal(dataItem["CounterEnd"].Text) - (GridCounting.DataSource as List<TransactionMold>).First().CounterStart).ToString();
                }
            }
        }

        protected void GridCounting_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            TransactionMoldRepository TRep = new TransactionMoldRepository();
            IQueryable<TransactionMold> returnList = null;
            List<TransactionMold> dataSource = null;
            if (!string.IsNullOrEmpty(MoldId))
            {
                returnList = TRep.ReadAll(x => x.MoldId == MoldId).OrderBy(x => x.Start);
            }

            if (returnList != null && returnList.Count() > 0)
            {
                if (DateStart.HasValue)
                {
                    if (DateEnd.HasValue)
                    {
                        dataSource = returnList.Where(x => x.Start >= DateStart && x.Start <= DateEnd).ToList();
                    }
                    else
                    {
                        dataSource = returnList.Where(x => x.Start >= DateStart).ToList();
                    }
                }
                else
                {
                    dataSource = returnList.ToList();
                }
            }
            //else
            //{
            //    if (DateStart.HasValue)
            //    {
            //        if (DateEnd.HasValue)
            //        {
            //            dataSource = TRep.ReadAll(x => x.Start >= DateStart && x.Start <= DateEnd).ToList();
            //        }
            //        else
            //        {
            //            dataSource = TRep.ReadAll(x => x.Start >= DateStart).ToList();
            //        }
            //    }
            //}

            GridCounting.DataSource = dataSource;
        }

        #region Filter
        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            CreateFilter();
        }

        private void CreateFilter()
        {
            string moldId = machineFilter.Entries.Count > 0 ? machineFilter.Entries[0].Value : string.Empty;
            DateTime? start = dtPickStart.SelectedDate;
            DateTime? end = dtPickEnd.SelectedDate;
            if (end.HasValue)
            {
                end = end.Value.AddDays(1);
            }

            TransactionMoldRepository TRep = new TransactionMoldRepository();
            List<TransactionMold> dataSource = new List<TransactionMold>();
            IQueryable<TransactionMold> returnList = null;

            if (!string.IsNullOrEmpty(moldId))
            {
                returnList = TRep.ReadAll(x => x.MoldId == moldId).OrderBy(x => x.Start);
            }

            if (returnList != null && returnList.Count() > 0)
            {
                if (start.HasValue)
                {
                    if (end.HasValue)
                    {
                        if (end == DateTime.Today)
                        {
                            dataSource = returnList.Where(x => x.Start >= start).ToList();
                        }
                        else
                        {
                            dataSource = returnList.Where(x => x.Start >= start && x.Start <= end).ToList();
                        }
                    }
                    else
                    {
                        dataSource = returnList.Where(x => x.Start >= start).ToList();
                    }
                }
                else
                {
                    dataSource = returnList.ToList();
                }
            }
            //else
            //{
            //    if (start.HasValue)
            //    {
            //        if (end.HasValue)
            //        {
            //            if (end == DateTime.Today)
            //            {
            //                dataSource = TRep.ReadAll(x => x.Start >= start).ToList();
            //            }
            //            else
            //            {
            //                dataSource = TRep.ReadAll(x => x.Start >= start && x.Start <= end).ToList();
            //            }
            //        }
            //        else
            //        {
            //            dataSource = TRep.ReadAll(x => x.Start >= start).ToList();
            //        }
            //    }
            //}

            if (!string.IsNullOrEmpty(moldId))
            {
                ProgressiveChart.Visible = true;
                StepChart.Visible = true;
                //GridCounting.Visible = true;
                MoldId = moldId;
                if (start.HasValue)
                {
                    DateStart = start.Value;
                    DateEnd = end.Value;
                }
            }

            if (dataSource.Count > 0)
            {
                CreateChart(dataSource, dataSource.Min(x => x.Start), dataSource.Max(x => x.Start));
            }
            else
            {
                GridCounting.Rebind();
            }

            //var param = Expression.Parameter(typeof(Transaction), "t");
            //BinaryExpression body;

            //if (!string.IsNullOrEmpty(assetId))
            //{
            //    body = Expression..And(body, Expression.Equal(
            //            Expression.PropertyOrField(param, "MachineId"),
            //            Expression.Constant(assetId)));
            //}
            //if (!string.IsNullOrEmpty(operatorId))
            //{
            //    body = Expression.And(body, Expression.Equal(
            //            Expression.PropertyOrField(param, "OperatorId"),
            //            Expression.Constant(operatorId)));
            //}
            //var lambda = Expression.Lambda<Func<Transaction, bool>>(body, param);
            //returnList = TRep.ReadAll(lambda).ToList();

            //Expression<Func<Purchase, bool>> criteria1 = p => p.Price > 1000;
            //Expression<Func<Purchase, bool>> criteria2 = p => criteria1.Invoke(p)
            //                                                  || p.Description.Contains("a");

            //Console.WriteLine(criteria2.Expand().ToString());
        }
        #endregion
    }
}