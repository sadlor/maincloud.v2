﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaintenanceHistory_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.MaintenanceHistory.MaintenanceHistory_View" %>

<div class="panel panel-info">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="moldFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px"
                    TextSettings-SelectionMode="Single" DataSourceID="edsMold" DataTextField="Name" DataValueField="Id" InputType="Text" Filter="Contains" Label="Stampo: ">
                </telerik:RadAutoCompleteBox>
            </div>
        </div>
        <%--<br />--%>
        <div style="text-align:right;">
            <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
        </div>
    </div>
</div>

<telerik:RadGrid runat="server" ID="gridHistory" RenderMode="Lightweight" AutoGenerateColumns="false"
    OnNeedDataSource="gridHistory_NeedDataSource" OnItemDataBound="gridHistory_ItemDataBound">
    <MasterTableView Caption="Storico manutenzioni" FilterExpression="" NoMasterRecordsText="Non sono state fatte manutenzioni">
        <Columns>
            <telerik:GridBoundColumn DataField="MoldId" HeaderText="Stampo">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Date" HeaderText="Intervento del" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CounterStart" HeaderText="∑ pz prodotti">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Duration" HeaderText="Durata [m]">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Cause.Description" HeaderText="Causale">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="Operatore">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    AutoGenerateWhereClause="true" OrderBy="it.Name">
</ef:EntityDataSource>