﻿using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.MaintenanceHistory
{
    public partial class MaintenanceHistory_View : DataWidget<MaintenanceHistory_View>
    {
        public MaintenanceHistory_View() : base(typeof(MaintenanceHistory_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void gridHistory_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            MoldRepository repos = new MoldRepository();

            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                item["MoldId"].Text = repos.GetName(item["MoldId"].Text);

                item["Duration"].Text = (Convert.ToDecimal(item["Duration"].Text) / 60).ToString();
            }
        }

        protected void gridHistory_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            string moldId = moldFilter.Entries.Count > 0 ? moldFilter.Entries[0].Value : string.Empty;
            if (!string.IsNullOrEmpty(moldId))
            {
                MoldMaintenanceRepository rep = new MoldMaintenanceRepository();
                gridHistory.DataSource = rep.ReadAll(x => x.MoldId == moldId).ToList();
            }
        }

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            gridHistory.Rebind();
        }
    }
}