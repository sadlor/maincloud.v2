﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TransactionEdit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.TransactionList.TransactionEdit" %>

<style type="text/css">
    [id$='EditFormControl_gridRow_ctl00__0'] input{
        border-color:transparent;
        white-space:normal;
        /*background:none;*/
        /*width:100%;*/
        /*height:100%;*/
    }

    [id$=EditFormControl_ddlOrder] input{
        border: 0 !important;
        width:50% !important;
    }

    [id$=EditFormControl_ddlOrder] .racEmptyMessage{
        width:100% !important;
    }

    .tdButton{
        padding-left:3px !important;
        padding-top:1px !important;
        padding-right:3px !important;
        padding-bottom:1px !important;
        /*height:100%;*/
    }
</style>

<telerik:RadGrid runat="server" ID="gridRow" RenderMode="Lightweight" AutoGenerateColumns="false"
    OnNeedDataSource="gridRow_NeedDataSource" OnItemCommand="gridRow_ItemCommand" OnItemDataBound="gridRow_ItemDataBound">
    <MasterTableView FilterExpression="" Caption="">
        <Columns>
            <telerik:GridDropDownColumn DataField="MachineId" HeaderText="Macchina" ReadOnly="true"
                ListValueField="Id" ListTextField="Description" DataSourceID="edsMachine">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="240px" />
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="Status" HeaderText="Stato" ReadOnly="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn DataTextField="Operator.UserName" CommandName="Operator" ButtonType="PushButton" HeaderText="Operatore">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" CssClass="tdButton" Wrap="true" />
            </telerik:GridButtonColumn>
            <telerik:GridButtonColumn DataTextField="Cause.Description" CommandName="Cause" ButtonType="PushButton" HeaderText="Causale">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" CssClass="tdButton" Wrap="true" />
            </telerik:GridButtonColumn>
            <telerik:GridButtonColumn DataTextField="Job.Order.CustomerOrder.OrderCode" CommandName="Order" ButtonType="PushButton" HeaderText="Commessa">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" CssClass="tdButton" Wrap="true" />
            </telerik:GridButtonColumn>
            <%--<telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="Operatore">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Cause.Description" HeaderText="Causale">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Job.Order.CustomerOrder.OrderCode" HeaderText="Commessa">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>--%>
            <telerik:GridBoundColumn DataField="Job.Order.Article.Code" HeaderText="Articolo" ReadOnly="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridDateTimeColumn DataField="Start" HeaderText="Start" PickerType="DateTimePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" ReadOnly="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="End" HeaderText="End" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" ReadOnly="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn DataField="PartialCounting" HeaderText="Colpi" DataFormatString="{0:n0}" ReadOnly="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn UniqueName="CavityMoldNum" DataTextField="CavityMoldNum" CommandName="CavityMoldNum" ButtonType="PushButton" HeaderText="Impronte" DataTextFormatString="{0:n0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" CssClass="tdButton" Wrap="true" />
            </telerik:GridButtonColumn>
            <telerik:GridBoundColumn DataField="QtyProduced" HeaderText="Pezzi<br />prodotti" DataFormatString="{0:n0}" ReadOnly="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridBoundColumn DataField="QtyProductionWaste" HeaderText="Scarti" DataFormatString="{0:n0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>--%>
            <%--<telerik:GridButtonColumn UniqueName="Waste" DataTextField="QtyProductionWaste" CommandName="Waste" ButtonType="PushButton" HeaderText="Scarti" DataTextFormatString="{0:f0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" CssClass="tdButton" Wrap="true" />
            </telerik:GridButtonColumn>--%>
            <telerik:GridCheckBoxColumn DataField="Verified" HeaderText="Verificata">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridCheckBoxColumn>
        </Columns>
    </MasterTableView>
    <%--<ClientSettings>
        <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
    </ClientSettings>--%>
</telerik:RadGrid>
        
<br />
        
<asp:Panel runat="server" ID="pnlOperator" Visible="false">
    <asp:Label Text="Operatore:" runat="server" />
    <asp:TextBox ID="txtOperator" runat="server" ReadOnly="true" Text='<%# DataBinder.Eval(Container, "DataItem.Operator.UserName") %>'>' TabIndex="1">
    </asp:TextBox>
    <telerik:RadComboBox RenderMode="Lightweight" ID="ddlOperator" AllowCustomText="true" runat="server" Filter="Contains" AutoPostBack="true"
        DataSourceID="edsOperator" DataValueField="Id" DataTextField="UserName" EmptyMessage="Seleziona operatore"
        OnSelectedIndexChanged="ddlOperator_SelectedIndexChanged" OnClientDropDownClosed="OnClientDropDownClosedHandler">
    </telerik:RadComboBox>
</asp:Panel>
<asp:Panel runat="server" ID="pnlCause" Visible="false">
    <asp:Label Text="Causale:" runat="server" />
    <asp:TextBox ID="txtCause" runat="server" ReadOnly="true" Text='<%# DataBinder.Eval(Container, "DataItem.Cause.Description") %>'>' TabIndex="1">
    </asp:TextBox>
    <telerik:RadComboBox RenderMode="Lightweight" ID="ddlCause" AllowCustomText="true" runat="server" Filter="Contains" AutoPostBack="true"
        DataSourceID="edsCause" DataValueField="Id" DataTextField="Description" EmptyMessage="Seleziona causale"
        OnSelectedIndexChanged="ddlCause_SelectedIndexChanged" OnClientDropDownClosed="OnClientDropDownClosedHandler">
    </telerik:RadComboBox>
</asp:Panel>
<asp:Panel runat="server" ID="pnlOrder" Visible="false">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Label Text="Commessa:" runat="server" />
            <asp:TextBox ID="txtOrder" runat="server" ReadOnly="true" Text='<%# DataBinder.Eval(Container, "DataItem.Job.Order.CustomerOrder.OrderCode") %>'>' TabIndex="1">
            </asp:TextBox>
            <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="ddlOrder" EmptyMessage="Cerca commessa" EnableClientFiltering="true"
                InputType="Text" Filter="Contains" DataSourceID="edsOrder" DataTextField="OrderCode" DataValueField="Id"
                OnTextChanged="ddlOrder_TextChanged">
                <TextSettings SelectionMode="Single" />
            </telerik:RadAutoCompleteBox>
            <telerik:RadComboBox RenderMode="Lightweight" ID="ddlJob" AllowCustomText="true" runat="server" Filter="Contains" AutoPostBack="true"
                DataValueField="Id" DataTextField="Description" EmptyMessage="Seleziona fase"
                OnSelectedIndexChanged="ddlJob_SelectedIndexChanged" OnClientDropDownClosed="OnClientDropDownClosedHandler">
            </telerik:RadComboBox>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel runat="server" ID="pnlCavityMoldNum" Visible="false">
    <asp:Label Text="Impronte:" runat="server" />
    <asp:TextBox ID="txtCavityMoldNum" runat="server" ReadOnly="true" Text='<%# DataBinder.Eval(Container, "DataItem.CavityMoldNum") %>'>' TabIndex="1">
    </asp:TextBox>
    <telerik:RadNumericTextBox runat="server" ID="txtNewCavityMoldNum" EmptyMessage="Nuovo numero impronte">
        <IncrementSettings Step="0" />
        <NumberFormat DecimalDigits="0" ZeroPattern="1" />
    </telerik:RadNumericTextBox>
</asp:Panel>
<asp:Panel runat="server" ID="pnlWaste" Visible="false">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                    <asp:Label Text="Colpi a vuoto:" runat="server" Font-Bold="true" />
                    <telerik:RadGrid runat="server" ID="gridBlankShot" RenderMode="Lightweight" AutoGenerateColumns="false" ShowGroupFooter="true"
                        OnNeedDataSource="gridBlankShot_NeedDataSource" OnUpdateCommand="gridBlankShot_UpdateCommand">
                        <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId, NewValue" EditMode="Batch">
                            <BatchEditingSettings EditType="Cell" OpenEditingEvent="Click" />
                            <Columns>
                                <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridNumericColumn DataField="OperatorWaste" HeaderText="Operatore" DataFormatString="{0:f0}" ReadOnly="true">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridNumericColumn>
                                <telerik:GridNumericColumn DataField="NewValue" HeaderText="Rettifica" DataFormatString="{0:f0}" Aggregate="Sum">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridNumericColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnBatchEditCellValueChanged="BatchEditCellValueChanged"/>
                        </ClientSettings>
                    </telerik:RadGrid>
                    <asp:HiddenField ID="hiddenIsBlankShotModify" runat="server" />
                    <asp:HiddenField ID="hiddenIsModify" runat="server" />
                    <asp:Label Text="Scarti:" runat="server" Font-Bold="true" />
                    <telerik:RadGrid runat="server" ID="gridScarti" RenderMode="Lightweight" AutoGenerateColumns="false" ShowGroupFooter="true"
                        OnNeedDataSource="gridScarti_NeedDataSource" OnUpdateCommand="gridScarti_UpdateCommand">
                        <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId, NewValue" EditMode="Batch">
                            <BatchEditingSettings EditType="Cell" OpenEditingEvent="Click" />
                            <Columns>
                                <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridNumericColumn DataField="OperatorWaste" HeaderText="Operatore" DataFormatString="{0:f0}" ReadOnly="true">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridNumericColumn>
                                <telerik:GridNumericColumn DataField="Quality" HeaderText="Qualità" DataFormatString="{0:f0}" ReadOnly="true">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridNumericColumn>
                                <telerik:GridNumericColumn DataField="NewValue" HeaderText="Rettifica" DataFormatString="{0:f0}" Aggregate="Sum">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridNumericColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnBatchEditCellValueChanged="BatchEditCellValueChanged"/>
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>

<br />
<div style="text-align:right;">
    <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" CssClass="btn btn-success">
    </asp:Button>&nbsp;
    <asp:Button ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" CssClass="btn btn-danger">
    </asp:Button>
</div>
<br />