﻿using MainCloudFramework.Web.Helpers;
using MES.Models;
using MES.Repositories;
using MES.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.TransactionList
{
    public partial class TransactionEdit : System.Web.UI.UserControl
    {
        private const string TRANSACTION_TO_MODIFY = "EditFormTransactionMoldToModify";
        private const string REMOVE_OPERATOR = "RemoveOperator";

        private object _dataItem = null;

        private string TransToModify
        {
            get
            {
                if (this.ViewState[TRANSACTION_TO_MODIFY] != null)
                {
                    return this.ViewState[TRANSACTION_TO_MODIFY].ToString();
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.ViewState[TRANSACTION_TO_MODIFY] = value;
            }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            if (ddlOperator.Items.Count > 0 && !ddlOperator.Items.Any(x => x.Value == REMOVE_OPERATOR))
            {
                ddlOperator.Items.Add(new RadComboBoxItem("Rimuovi operatore", REMOVE_OPERATOR));
            }
        }

        public object DataItem
        {
            get
            {
                return this._dataItem;
            }
            set
            {
                this._dataItem = value;
            }
        }

        protected void gridRow_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (TransToModify == null)//if (string.IsNullOrEmpty(TransToModify))
            {
                GridEditableItem editedItem = this.Parent.NamingContainer as GridEditableItem;
                int transactionId = Convert.ToInt32(editedItem.GetDataKeyValue("Id"));
                TransactionMoldRepository repos = new TransactionMoldRepository();
                TransToModify = JsonConvert.SerializeObject(repos.FindByID(transactionId));
            }
            gridRow.DataSource = new List<TransactionMold>() { JsonConvert.DeserializeObject<TransactionMold>(TransToModify) };
        }

        protected void gridRow_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Operator":
                    pnlOperator.Visible = true;
                    pnlCause.Visible = false;
                    pnlOrder.Visible = false;
                    pnlCavityMoldNum.Visible = false;
                    pnlWaste.Visible = false;
                    break;
                case "Cause":
                    pnlOperator.Visible = false;
                    pnlCause.Visible = true;
                    pnlOrder.Visible = false;
                    pnlCavityMoldNum.Visible = false;
                    pnlWaste.Visible = false;
                    break;
                case "Order":
                    pnlOperator.Visible = false;
                    pnlCause.Visible = false;
                    pnlOrder.Visible = true;
                    pnlCavityMoldNum.Visible = false;
                    pnlWaste.Visible = false;
                    break;
                case "CavityMoldNum":
                    pnlOperator.Visible = false;
                    pnlCause.Visible = false;
                    pnlOrder.Visible = false;
                    pnlCavityMoldNum.Visible = true;
                    pnlWaste.Visible = false;
                    break;
                case "Waste":
                    pnlOperator.Visible = false;
                    pnlCause.Visible = false;
                    pnlOrder.Visible = false;
                    pnlCavityMoldNum.Visible = false;
                    pnlWaste.Visible = true;
                    gridScarti.Rebind();//InitializeWaste();
                    gridBlankShot.Rebind();
                    break;
            }
        }

        protected void ddlOperator_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            string operatorId = e.Value;
            TransactionMold t = JsonConvert.DeserializeObject<TransactionMold>(TransToModify);
            t.OperatorId = operatorId;
            OperatorRepository repos = new OperatorRepository();
            t.Operator = repos.FindByID(operatorId);
            TransToModify = JsonConvert.SerializeObject(t);
            gridRow.Rebind();
        }

        protected void ddlCause_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            string causeId = e.Value;
            TransactionMold t = JsonConvert.DeserializeObject<TransactionMold>(TransToModify);
            t.CauseId = causeId;
            CauseRepository repos = new CauseRepository();
            t.Cause = repos.FindByID(causeId);
            TransToModify = JsonConvert.SerializeObject(t);
            gridRow.Rebind();
        }

        protected void ddlOrder_TextChanged(object sender, AutoCompleteTextEventArgs e)
        {
            if (ddlOrder.Entries.Count > 0)
            {
                int orderId = Convert.ToInt32(ddlOrder.Entries[0].Value);

                JobService JService = new JobService();
                ddlJob.DataSource = JService.GetJobListByCustomerOrder(orderId);
                ddlJob.DataBind();
            }
        }

        protected void ddlJob_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            string jobId = e.Value;
            TransactionMold t = JsonConvert.DeserializeObject<TransactionMold>(TransToModify);
            t.JobId = jobId;
            JobRepository repos = new JobRepository();
            t.Job = repos.FindByID(jobId);
            TransToModify = JsonConvert.SerializeObject(t);
            gridRow.Rebind();
        }

        protected void gridRow_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                item["Status"].Text = item["Status"].Text == "False" ? "Off" : "On";

                if (string.IsNullOrEmpty((item.DataItem as TransactionMold).JobId))
                {
                    (item["CavityMoldNum"].Controls[0] as Button).Enabled = false;
                }
                else
                {
                    (item["CavityMoldNum"].Controls[0] as Button).Enabled = true;
                }
                //if (string.IsNullOrEmpty((item.DataItem as TransactionMold).JobId))
                //{
                //    (item["Waste"].Controls[0] as Button).Enabled = false;
                //}
                //else
                //{
                //    if ((item.DataItem as TransactionMold).PartialCounting > 0)
                //    {
                //        (item["Waste"].Controls[0] as Button).Enabled = true;
                //    }
                //    else
                //    {
                //        (item["Waste"].Controls[0] as Button).Enabled = false;
                //    }
                //}
            }
        }

        #region Scarti
        [Serializable]
        protected class ScartiRecord
        {
            public string CauseId { get; set; }
            public string Description { get; set; } // code - description

            /// <summary>
            /// Scarti dichiaarti dall'operatore corrente
            /// </summary>
            public decimal OperatorWaste { get; set; }

            /// <summary>
            /// Scarti dichiarati da un controllo qualità
            /// </summary>
            public decimal Quality { get; set; }

            public decimal NewValue { get; set; }

            public ScartiRecord(string id, string description)
            {
                CauseId = id;
                Description = description;
            }

            public ScartiRecord(string id, string description, decimal operatorWaste, decimal quality, decimal num = 0)
            {
                CauseId = id;
                Description = description;
                OperatorWaste = operatorWaste;
                Quality = quality;
                NewValue = num;
            }
        }

        const string JOB_PRODUCTIONWASTE_TO_MODIFY = "JobProductionWasteListToModify";
        protected List<ScartiRecord> JobWasteListToModify
        {
            get
            {
                if (this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] == null)
                {
                    this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] = new List<ScartiRecord>();
                }
                return (List<ScartiRecord>)(this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY]);
            }
            set
            {
                this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] = value;
            }
        }
        const string BLANK_SHOT_RECORD = "BlankShotRecord";
        protected List<ScartiRecord> BlankShotRecord
        {
            get
            {
                if (this.ViewState[BLANK_SHOT_RECORD] == null)
                {
                    this.ViewState[BLANK_SHOT_RECORD] = new List<ScartiRecord>();
                }
                return (List<ScartiRecord>)(this.ViewState[BLANK_SHOT_RECORD]);
            }
            set
            {
                this.ViewState[BLANK_SHOT_RECORD] = value;
            }
        }

        private void InitializeWaste()
        {
            OperatorService OS = new OperatorService();
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            TransactionMoldService transService = new TransactionMoldService();

            TransactionMold t = JsonConvert.DeserializeObject<TransactionMold>(TransToModify);
            string assetId = t.MachineId;

            List<JobProductionWaste> wasteWorkshift;
            JobProductionWasteService jpwService = new JobProductionWasteService();

            wasteWorkshift = jpwService.GetWasteListPerWorkshift(t.JobId, t.OperatorId, t.Start, t.End);

            CauseTypeRepository CTR = new CauseTypeRepository();
            List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
            List<ScartiRecord> dataSource = new List<ScartiRecord>();
            foreach (CauseType ct in causeList)
            {
                foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode))
                {
                    //mostro gli scarti dichiarati in precedenza
                    //if (wasteList.Where(x => x.CauseId == c.Id).Any())
                    if (wasteWorkshift.Where(x => x.CauseId == c.Id).Any())
                    {
                        //dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteList.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste)));
                        dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteWorkshift.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste), 0));
                    }
                    else
                    {
                        dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                    }
                }
            }
            gridScarti.DataSource = dataSource;
            gridScarti.DataBind();
        }

        protected void gridScarti_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            TransactionMold t = JsonConvert.DeserializeObject<TransactionMold>(TransToModify);

            JobProductionWasteService jpwService = new JobProductionWasteService();
            List<JobProductionWaste> wasteList = jpwService.GetWasteListPerTransaction(t.Id, t.JobId);
            //List<JobProductionWaste> wasteList = jpwService.GetWasteListPerWorkshift(t.JobId, t.OperatorId, t.Start, t.End);

            //update datasource
            CauseTypeRepository CTR = new CauseTypeRepository();
            List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Description).ToList();
            List<ScartiRecord> dataSource = new List<ScartiRecord>();
            foreach (CauseType ct in causeList)
            {
                foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode))
                {
                    //scarti in differita -> mostro gli scarti dichiarati in precedenza
                    if (wasteList.Where(x => x.CauseId == c.Id).Any())
                    {
                        dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, Math.Round(wasteList.Where(x => x.CauseId == c.Id && !x.Quality).Sum(x => x.QtyProductionWaste), 0), wasteList.Where(x => x.CauseId == c.Id && x.Quality).Sum(x => x.QtyProductionWaste)));
                    }
                    else
                    {
                        dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                    }
                }
            }

            foreach (ScartiRecord sr in JobWasteListToModify)
            {
                //modifico la colonna newValue
                dataSource.Where(x => x.CauseId == sr.CauseId).First().NewValue = sr.NewValue;
            }
            gridScarti.DataSource = dataSource;

            //txtQtyWasteTransaction.Text = dataSource.Sum(x => x.Num + x.Quality).ToString();

            //Aggiornamento quantità totale in corso
            //List<Transaction> currentJobTList = transService.GetTransactionListByCurrentJob(AssetId);
            //txtQtyProducedTransaction.Text = currentJobTList.Sum(x => x.PartialCounting).ToString();
        }

        protected void gridScarti_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (string.IsNullOrEmpty(hiddenIsModify.Value) || hiddenIsModify.Value != bool.TrueString)
            {
                hiddenIsModify.Value = bool.TrueString;
            }
            var editableItem = ((GridEditableItem)e.Item);
            var causeId = editableItem.GetDataKeyValue("CauseId").ToString();

            if (!JobWasteListToModify.Where(x => x.CauseId == causeId).Any())
            {
                JobWasteListToModify.Add(new ScartiRecord(causeId, "", 0, 0));
            }
            JobWasteListToModify.Where(x => x.CauseId == causeId).First().NewValue = Convert.ToInt32((e.CommandArgument as GridBatchEditingEventArgument).NewValues["NewValue"]);
        }

        protected void gridBlankShot_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            TransactionMold t = JsonConvert.DeserializeObject<TransactionMold>(TransToModify);

            if (BlankShotRecord.Count == 0)
            {
                gridBlankShot.DataSource = new List<ScartiRecord>(1) { new ScartiRecord(null, "Colpi a vuoto", t.BlankShot, 0) };
            }
            else
            {
                gridBlankShot.DataSource = new List<ScartiRecord>(1) { new ScartiRecord(null, "Colpi a vuoto", t.BlankShot, 0, BlankShotRecord.First().NewValue) };
            }
        }

        protected void gridBlankShot_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (string.IsNullOrEmpty(hiddenIsBlankShotModify.Value) || hiddenIsBlankShotModify.Value != bool.TrueString)
            {
                hiddenIsBlankShotModify.Value = bool.TrueString;
            }
            var editableItem = ((GridEditableItem)e.Item);

            if (BlankShotRecord.Count == 0)
            {
                BlankShotRecord.Add(new ScartiRecord(null, "Colpi a vuoto", 0, 0));
            }
            BlankShotRecord.First().NewValue = Convert.ToInt32((e.CommandArgument as GridBatchEditingEventArgument).NewValues["NewValue"]);
        }
        #endregion
    }
}