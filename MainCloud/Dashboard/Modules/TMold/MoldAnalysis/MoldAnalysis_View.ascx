﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoldAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.MoldAnalysis.MoldAnalysis_View" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="panel panel-info">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="moldFilter" EmptyMessage="Stampo" EnableClientFiltering="true"
                            LabelWidth="70px" Width="263px" TextSettings-SelectionMode="Single" InputType="Text" Filter="Contains" Label="Stampo: "
                            DataSourceID="edsMold" DataTextField="Name" DataValueField="Id">
                        </telerik:RadAutoCompleteBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>
                </div>
                <div style="text-align:right;">
                    <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
                </div>
            </div>
        </div>

        <telerik:RadGrid runat="server" ID="gridMold" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="false"
            OnNeedDataSource="gridMold_NeedDataSource" OnItemDataBound="gridMold_ItemDataBound">
            <MasterTableView Caption="" FilterExpression="" DataKeyNames="Code">
                <ColumnGroups>
                    <telerik:GridColumnGroup HeaderText="H" Name="Ore">
                        <HeaderStyle HorizontalAlign="Center" />
                    </telerik:GridColumnGroup>
                    <telerik:GridColumnGroup HeaderText="%" Name="Perc">
                        <HeaderStyle HorizontalAlign="Center" />
                    </telerik:GridColumnGroup>
                    <telerik:GridColumnGroup HeaderText="Colpi/ora" Name="Cadency">
                        <HeaderStyle HorizontalAlign="Center" />
                    </telerik:GridColumnGroup>
                </ColumnGroups>
                <Columns>
                    <telerik:GridBoundColumn DataField="Code" HeaderText="Macchina" CurrentFilterFunction="Contains" AllowSorting="false" AllowFiltering="false"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CustomerOrder" HeaderText="Commessa" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Article" HeaderText="Articolo" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OreProdTeorico" HeaderText="Assegnate" AllowFiltering="false" AllowSorting="false" ColumnGroupName="Ore">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MachineOnTime" HeaderText="Utilizzo" AllowFiltering="false" AllowSorting="false" ColumnGroupName="Ore">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Production" HeaderText="Prod" AllowFiltering="false" AllowSorting="false" ColumnGroupName="Ore">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Stop" HeaderText="Fermo" AllowFiltering="false" AllowSorting="false" ColumnGroupName="Ore">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Availability" HeaderText="Disp" AllowFiltering="false" AllowSorting="false" ColumnGroupName="Perc">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Efficiency" HeaderText="Eff" AllowFiltering="false" AllowSorting="false" ColumnGroupName="Perc">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Quality" HeaderText="Q" AllowFiltering="false" AllowSorting="false" ColumnGroupName="Perc">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridCalculatedColumn DataFields="Availability,Efficiency,Quality" HeaderText="OEE" Expression="(({0}/100) * ({1} / 100) * ({2} / 100)) * 100" 
                        AllowFiltering="false" AllowSorting="false" DataFormatString="{0:f2}" ColumnGroupName="Perc">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridCalculatedColumn>
                    <telerik:GridBoundColumn DataField="StampateOraTeorico" HeaderText="Teorici" AllowFiltering="false" AllowSorting="false" ColumnGroupName="Cadency">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Cadency" HeaderText="Effettivi" AllowFiltering="false" AllowSorting="false" ColumnGroupName="Cadency">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="UnitTime" HeaderText="Tempo<br/>unitario" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="QtyProd" HeaderText="Pezzi<br/>prodotti" AllowFiltering="false" AllowSorting="false" DataFormatString="{0:N0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Waste" HeaderText="Scarti" AllowFiltering="false" AllowSorting="false" DataFormatString="{0:f0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </ContentTemplate>
</asp:UpdatePanel>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    AutoGenerateWhereClause="true" OrderBy="it.Name">
</ef:EntityDataSource>