﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.MoldAnalysis
{
    public partial class MoldAnalysis_View : DataWidget<MoldAnalysis_View>
    {
        public MoldAnalysis_View() : base(typeof(MoldAnalysis_View)) {}

        private class TableRecord
        {
            public string Code { get; set; }
            public string CustomerOrder { get; set; }
            public string Article { get; set; }
            public string MachineOnTime { get; set; }
            public string Production { get; set; }
            public decimal OreProdTeorico { get; set; }
            public string Stop { get; set; }
            public decimal Index { get; set; }
            public decimal QtyShotProd { get; set; }
            public decimal QtyProd { get; set; }
            public decimal Cadency { get; set; }
            public decimal Waste { get; set; }
            public decimal StampateOraTeorico { get; set; }
            public decimal Availability { get; set; }
            public decimal Efficiency { get; set; }
            public decimal Quality { get; set; }
            public decimal OEE { get; set; }
            //public decimal UnitTime { get; set; }
            public TimeSpan UnitTime { get; set; }

            public TableRecord(string code, string customerOrder, string article, string machineOnTime, string productionTime, string stopTime, decimal availability, decimal efficiency, decimal quality, decimal oreProdTeorico, decimal stampateOra, decimal cadency, decimal unitTime, decimal qtyShotProd, decimal qtyProd, decimal waste)
            {
                Code = code;
                CustomerOrder = customerOrder;
                Article = article;
                MachineOnTime = machineOnTime;
                Production = productionTime;
                Stop = stopTime;
                Availability = availability;
                Efficiency = efficiency;
                Quality = quality;
                OreProdTeorico = oreProdTeorico;
                StampateOraTeorico = stampateOra;
                Cadency = cadency;
                UnitTime = TimeSpan.FromHours((double)unitTime);
                QtyShotProd = qtyShotProd;
                QtyProd = qtyProd;
                Waste = waste;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }

            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            gridMold.Visible = true;
            gridMold.Rebind();
        }

        protected void gridMold_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            string moldId = moldFilter.Entries.Count > 0 ? moldFilter.Entries[0].Value : string.Empty;
            if (!string.IsNullOrEmpty(moldId))
            {
                IndexService indexService = new IndexService();
                JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();
                List<TableRecord> dataSource = new List<TableRecord>();

                TransactionMoldRepository TRep = new TransactionMoldRepository();
                List<TransactionMold> tList = new List<TransactionMold>();
                DateTime end = dtPickEnd.SelectedDate.Value.AddDays(1);
                tList = TRep.ReadAll(x => x.MoldId == moldId && x.Start >= dtPickStart.SelectedDate && x.Start <= end).ToList();
                foreach (string assetId in tList.Select(x => x.MachineId).Distinct())
                {
                    AssetRepository assetRep = new AssetRepository();
                    Asset item = assetRep.FindByID(assetId);
                    List<TransactionMold> mtList = tList.Where(x => x.MachineId == assetId).ToList();

                    foreach (Job job in mtList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct())
                    {
                        if (job != null)
                        {
                            TimeSpan machineOn = TimeSpan.FromSeconds((double)mtList.Where(x => x.JobId == job.Id && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                            TimeSpan productionTime = TimeSpan.FromSeconds((double)mtList.Where(x => x.JobId == job.Id && x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                            TimeSpan stopTime = TimeSpan.FromSeconds((double)mtList.Where(x => x.JobId == job.Id && x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                            decimal oreProdTeorico = 0;
                            decimal qtyShotProd = 0;
                            decimal qtyProd = 0;
                            decimal waste = 0;
                            decimal cadency = 0;
                            decimal unitTime = 0;

                            if (productionTime.TotalSeconds > 0)
                            {
                                qtyShotProd = mtList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting);
                                cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                                unitTime = Math.Round((decimal)productionTime.TotalHours / qtyShotProd, 2);
                                //foreach (Job job in mtList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct())
                                //{
                                //    if (job != null)
                                //    {
                                if (job.StandardRate > 0)
                                {
                                    oreProdTeorico += Math.Round(mtList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) / job.StandardRate, 2);
                                }
                                qtyProd += mtList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1);

                                try
                                {
                                    waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == item.Id && x.JobId == job.Id && x.Date >= dtPickStart.SelectedDate && x.Date <= end).Sum(x => x.QtyProductionWaste), 0);
                                }
                                catch (InvalidOperationException) { }
                            }

                            dataSource.Add(new TableRecord(
                                string.Format("{0} - {1}", item.Code.Trim(), item.Description.Trim()),
                                job.Order.CustomerOrder.OrderCode,
                                job.Order.Article.Code,
                                string.Format("{0:n2}", machineOn.TotalHours),//string.Format("{0}:{1:d2}:{2:d2}", (int)machineOn.TotalHours, machineOn.Minutes, machineOn.Seconds),
                                string.Format("{0:n2}", productionTime.TotalHours),//string.Format("{0}:{1:d2}:{2:d2}", (int)productionTime.TotalHours, productionTime.Minutes, productionTime.Seconds),
                                string.Format("{0:n2}", stopTime.TotalHours),//string.Format("{0}:{1:d2}:{2:d2}", (int)stopTime.TotalHours, stopTime.Minutes, stopTime.Seconds),
                                indexService.Availability(productionTime, machineOn),
                                indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico),
                                indexService.Quality(qtyProd, waste),
                                oreProdTeorico,
                                job.StandardRate,
                                cadency,
                                unitTime,
                                qtyShotProd,
                                qtyProd,
                                waste));
                        }
                    }
                }

                TimeSpan totMachineOn = TimeSpan.FromHours(dataSource.Sum(x => Convert.ToDouble(x.MachineOnTime)));
                TimeSpan totProd = TimeSpan.FromHours(dataSource.Sum(x => Convert.ToDouble(x.Production)));
                TimeSpan totStop = TimeSpan.FromHours(dataSource.Sum(x => Convert.ToDouble(x.Stop)));
                decimal qtyTot = dataSource.Sum(x => x.QtyProd);
                decimal wasteTot = dataSource.Sum(x => x.Waste);
                decimal oreProdTeorTot = dataSource.Sum(x => x.OreProdTeorico);
                decimal cadencyTot = 0;
                decimal unitTimeTot = 0;
                if (totProd.TotalSeconds > 0)
                {
                    cadencyTot = Math.Round(dataSource.Sum(x => x.QtyShotProd) / (decimal)totProd.TotalHours, 2);
                    unitTimeTot = Math.Round((decimal)totProd.TotalHours / dataSource.Sum(x => x.QtyShotProd), 2);
                }

                decimal availabilityTot = indexService.Availability(totProd, totMachineOn);
                //decimal efficiencyTot = 0;
                //if (dataSource.Where(x => x.OreProdTeorico > 0).Any())
                //{
                //solo le ore di produzione delle macchine che avevano le stampateOra teoriche
                //TimeSpan totProdWithCadency = TimeSpan.FromHours(dataSource.Where(x => x.OreProdTeorico > 0).Sum(x => Convert.ToInt32(x.Production)));
                //efficiencyTot = indexService.Efficiency((decimal)totProdWithCadency.TotalHours, oreProdTeorTot);
                //}
                decimal efficiencyTot = indexService.Efficiency((decimal)totProd.TotalHours, oreProdTeorTot);
                decimal qualityTot = indexService.Quality(qtyTot, wasteTot);

                dataSource.Insert(0, new TableRecord(
                    "MEDIA TOTALE",
                    "",
                    "",
                    string.Format("{0:n2}", totMachineOn.TotalHours),
                    string.Format("{0:n2}", totProd.TotalHours),
                    string.Format("{0:n2}", totStop.TotalHours),
                    availabilityTot,
                    efficiencyTot,
                    qualityTot,
                    oreProdTeorTot,
                    0,
                    cadencyTot,
                    unitTimeTot,
                    0,
                    qtyTot,
                    wasteTot));

                gridMold.DataSource = dataSource;
            }
        }

        protected void gridMold_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                if (item.GetDataKeyValue("Code").ToString() == "MEDIA TOTALE")
                {
                    item["Code"].Font.Bold = true;
                }
            }
        }
    }
}