﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.StopTimeSummary
{
    public partial class StopTimeSummary_View : DataWidget<StopTimeSummary_View>
    {
        public StopTimeSummary_View() : base(typeof(StopTimeSummary_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);

            if (!IsPostBack)
            {
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }
        }

        protected string MoldId
        {
            get
            {
                if (string.IsNullOrEmpty((string)ViewState["StopSummaryMoldId"]))
                {
                    return null;
                }
                return ViewState["StopSummaryMoldId"].ToString();
            }
            set
            {
                ViewState["StopSummaryMoldId"] = value;
            }
        }

        protected DateTime? DateStart
        {
            get
            {
                if (ViewState["StopSummaryMoldDateStart"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["StopSummaryMoldDateStart"]);
            }
            set
            {
                ViewState["StopSummaryMoldDateStart"] = value;
            }
        }

        protected DateTime? DateEnd
        {
            get
            {
                if (ViewState["StopSummaryMoldDateEnd"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["StopSummaryMoldDateEnd"]);
            }
            set
            {
                ViewState["StopSummaryMoldDateEnd"] = value;
            }
        }

        #region Filter
        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            CreateFilter();
        }

        private void CreateFilter()
        {
            string moldId = moldFilter.Entries.Count > 0 ? moldFilter.Entries[0].Value : string.Empty;
            DateTime? start = dtPickStart.SelectedDate;
            DateTime? end = dtPickEnd.SelectedDate;

            TransactionMoldRepository TRep = new TransactionMoldRepository();
            List<TransactionMold> dataSource = new List<TransactionMold>();
            IQueryable<TransactionMold> returnList = null;

            if (!string.IsNullOrEmpty(moldId))
            {
                returnList = TRep.ReadAll(x => x.MoldId == moldId);
            }

            if (returnList != null && returnList.Count() > 0)
            {
                if (start.HasValue)
                {
                    if (end.HasValue)
                    {
                        dataSource = returnList.Where(x => x.Start >= start && x.Start <= end).ToList();
                    }
                    else
                    {
                        dataSource = returnList.Where(x => x.Start >= start).ToList();
                    }
                }
                else
                {
                    dataSource = returnList.ToList();
                }
            }
            else
            {
                if (start.HasValue)
                {
                    if (end.HasValue)
                    {
                        dataSource = TRep.ReadAll(x => x.Start >= start && x.Start <= end).ToList();
                    }
                    else
                    {
                        dataSource = TRep.ReadAll(x => x.Start >= start).ToList();
                    }
                }
            }

            if (!string.IsNullOrEmpty(moldId))
            {
                DownTimeChart.Visible = true;
                MoldId = moldId;
                if (start.HasValue)
                {
                    DateStart = start.Value;
                    DateEnd = end.Value;
                }
            }

            if (dataSource.Count > 0)
            {
                CreateChart(dataSource, dataSource.Min(x => x.Start), dataSource.Max(x => x.Start));
            }
        }
        #endregion

        protected void CreateChart(List<TransactionMold> tList, DateTime start, DateTime end)
        {
            if (tList.Where(x => x.Open).Any())
            {
                tList.Where(x => x.Open).First().Duration = (decimal)(DateTime.Now - tList.Where(x => x.Open).First().Start).TotalSeconds;
            }

            decimal useTime = Math.Round((decimal)TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration)).TotalMinutes, 2);
            decimal productionTime = Math.Round((decimal)TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration)).TotalMinutes, 2);
            decimal downTime = Math.Round((decimal)TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration)).TotalMinutes, 2);
            tdDisponibility.InnerText = TimeSpan.FromMinutes((double)useTime).ToString(@"d\ hh\:mm\:ss");
            tdProduction.InnerText = TimeSpan.FromMinutes((double)productionTime).ToString(@"d\ hh\:mm\:ss");
            tdDown.InnerText = TimeSpan.FromMinutes((double)downTime).ToString(@"d\ hh\:mm\:ss");
            tdPctDown.InnerText = Math.Round(downTime / useTime * 100, 2).ToString();
            //lblDisponibility.Text = string.Format("Ore disponibilità: {0}", TimeSpan.FromMinutes((double)useTime).ToString(@"hh\:mm\:ss"));
            //lblProduction.Text = string.Format("Ore produzione: {0}", TimeSpan.FromMinutes((double)productionTime).ToString(@"hh\:mm\:ss"));
            //lblDownTime.Text = string.Format("Ore fermo: {0}", TimeSpan.FromMinutes((double)downTime).ToString(@"hh\:mm\:ss"));
            //lblPctDownTime.Text = string.Format("Percentuale fermi: {0} %", Math.Round(downTime / useTime * 100, 2).ToString());

            Dictionary<string, decimal> dataSource = new Dictionary<string, decimal>();
            foreach(var t in tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()))
            {
                if (t.Cause != null)
                {
                    if (!dataSource.ContainsKey(string.Format("{1} - {0}", t.Cause.Description, t.Cause.ExternalCode)))
                    {
                        dataSource.Add(string.Format("{1} - {0}", t.Cause.Description, t.Cause.ExternalCode), 0);
                    }
                    dataSource[string.Format("{1} - {0}", t.Cause.Description, t.Cause.ExternalCode)] += Math.Round((decimal)TimeSpan.FromSeconds((double)t.Duration).TotalMinutes, 2);
                }
                else
                {
                    if (!dataSource.ContainsKey("Ingiustificati"))
                    {
                        dataSource.Add("Ingiustificati", 0);
                    }
                    dataSource["Ingiustificati"] += Math.Round((decimal)TimeSpan.FromSeconds((double)t.Duration).TotalMinutes, 2);
                }
            }

            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("label"));
            table.Columns.Add(new DataColumn("data", typeof(decimal)));
            table.Columns.Add(new DataColumn("pctValue", typeof(decimal)));
            foreach (var item in dataSource.OrderBy(x => x.Key.Split('-')[0].Trim().Length).ThenBy(x => x.Key.Split('-')[0].Trim()))
            {
                table.Rows.Add(new object[] { item.Key, item.Value, Math.Round(item.Value / downTime * 100, 2) });
            }

            //DownTimeChart.DataSource = dataSource.OrderBy(x => x.Key);
            DownTimeChart.DataSource = table;
            DownTimeChart.DataBind();
        }
    }
}