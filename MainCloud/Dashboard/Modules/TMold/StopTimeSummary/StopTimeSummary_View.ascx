﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StopTimeSummary_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.StopTimeSummary.StopTimeSummary_View" %>

<div class="panel panel-info">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="moldFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px"
                    TextSettings-SelectionMode="Single" DataSourceID="edsMold" DataTextField="Name" DataValueField="Id" InputType="Text" Filter="Contains" Label="Stampo: ">
                </telerik:RadAutoCompleteBox>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <%--<br />--%>
        <div style="text-align:right;">
            <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
        </div>
    </div>
</div>

<div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <table class="table table-bordered table-condensed info" style="width:100%;">
                    <tr>
                        <td colspan="3"></td>
                        <td style="background-color:#428bca; color:white; text-align:center"><b>Tempo</b></td>
                        <td style="background-color:#428bca; color:white; text-align:center"><b>%</b></td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>Disponibilità</b></td>
                        <td runat="server" id="tdDisponibility" style="text-align:right;"></td>
                        <td runat="server" style="text-align:right;"></td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>Produzione</b></td>
                        <td runat="server" id="tdProduction" style="text-align:right;"></td>
                        <td runat="server" style="text-align:right;"></td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>Fermi</b></td>
                        <td runat="server" id="tdDown" style="text-align:right;"></td>
                        <td runat="server" id="tdPctDown" style="text-align:right;"></td>
                    </tr>
                </table>
            </div>
            <asp:Label runat="server" ID="lblDisponibility" Font-Bold="true" /> &nbsp;&nbsp;&nbsp;
            <asp:Label runat="server" ID="lblProduction" Font-Bold="true" /> &nbsp;&nbsp;&nbsp;
            <asp:Label runat="server" ID="lblDownTime" Font-Bold="true" /> &nbsp;&nbsp;&nbsp;
            <asp:Label runat="server" ID="lblPctDownTime" Font-Bold="true" />
        </div>
    </div>
</div>

<div class="demo-container no-bg size-medium">
    <telerik:RadHtmlChart runat="server" ID="DownTimeChart">
        <PlotArea>
            <Series>
                <telerik:BarSeries DataFieldY="data">
                    <TooltipsAppearance Color="White" DataFormatString="{0}\'">
                    </TooltipsAppearance>
                    <LabelsAppearance>
                        <ClientTemplate>
                            #= dataItem.data #\' - #= dataItem.pctValue #%
                        </ClientTemplate>
                    </LabelsAppearance>
                </telerik:BarSeries>
            </Series>
            <XAxis DataLabelsField="label">
                <MinorGridLines Visible="false"></MinorGridLines>
                <MajorGridLines Visible="false"></MajorGridLines>
            </XAxis>
            <YAxis>
                <LabelsAppearance DataFormatString="{0}\'"></LabelsAppearance>
                <MinorGridLines Visible="false"></MinorGridLines>
            </YAxis>
        </PlotArea>
        <Legend>
            <Appearance Visible="false"></Appearance>
        </Legend>
        <ChartTitle Text="Riepilogo Fermi"></ChartTitle>
    </telerik:RadHtmlChart>
</div>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    AutoGenerateWhereClause="true" OrderBy="it.Name">
</ef:EntityDataSource>