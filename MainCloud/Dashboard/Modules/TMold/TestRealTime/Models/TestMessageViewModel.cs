﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.TMold.TestRealTime.Models
{
    public class TestMessageViewModel
    {
        public string DeviceId { get; set; }
        public string DeviceSN { get; set; }
        public int Counter { get; set; }

        public TestMessageViewModel(string deviceId, int counter)
        {
            DeviceId = deviceId;
            Counter = counter;
        }
    }
}