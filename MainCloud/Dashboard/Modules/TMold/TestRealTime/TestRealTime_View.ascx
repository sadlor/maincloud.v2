﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestRealTime_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.TestRealTime.TestRealTime_View" %>

<script type="text/javascript">
    function GetMessages(sender, eventArgs) {
        var deviceId = args.get_tableView().get_clientDataKeyNames()[0];
        $.ajax({
            url: 'api/TMold/' + deviceId,
            type: 'get',
            dataType: 'json',
            success: function (data, status) {
                alert('ok - ' + data);
                var tableView = sender.get_masterTableView();
                tableView.set_dataSource(data);
                tableView.dataBind();
            },
            error: function (data, msg, detail) {
                alert(data + '\n' + msg + '\n' + detail);
            }
        });
        return false;
    }

    function CreateGrid(sender, eventArgs)
    {
        var tableView = sender.get_detailTables()[0];
        tableView.set_dataSource(data);
        tableView.dataBind();
    }

    function GridCommand(sender, eventArgs)
    {
    }

    function requestStart(sender, args) {
        var type = args.get_type();
        var transport = sender.get_dataSourceObject().options.transport;
        switch (type) {
            case "read":
                var value = 'ok';
                transport.read.url = "api/products/" + value;
                break;
            default: break;
        }
    }
</script>

<asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>
        <telerik:RadGrid runat="server" ID="gridMold" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="true"
            OnNeedDataSource="gridMold_NeedDataSource" OnDetailTableDataBind="gridMold_DetailTableDataBind" OnItemCommand="gridMold_ItemCommand" OnPreRender="gridMold_PreRender"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <ClientSettings AllowExpandCollapse="true">
                <%--<ClientEvents OnHierarchyExpanding="GetMessages" OnGridCreated="CreateGrid" OnCommand="GridCommand" />--%>
            </ClientSettings>
            <MasterTableView Caption="Verifica contatori" FilterExpression="" DataKeyNames="DeviceId" ClientDataKeyNames="DeviceId" HierarchyDefaultExpanded="false">
                <Columns>
                    <telerik:GridBoundColumn DataField="DeviceId" HeaderText="EUI">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DeviceSN" HeaderText="Seriale">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Counter" HeaderText="Contatore">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Config" HeaderText="Configurazione">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                </Columns>
                <DetailTables>
                    <telerik:GridTableView runat="server" EnableHierarchyExpandAll="true" AllowPaging="false">
                        <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
                        <Columns>
                            <%--<telerik:GridBoundColumn DataField="Device" HeaderText="Sensore">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>--%>
                            <telerik:GridBoundColumn DataField="Gateway" HeaderText="Gateway">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn DataField="Time" HeaderText="Time" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn DataField="Counter" HeaderText="Contatore">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Temperature" HeaderText="Temperatura">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Battery" HeaderText="Batteria">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClickButton" HeaderText="Click bottone">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                </DetailTables>
            </MasterTableView>
        </telerik:RadGrid>

        <asp:Timer ID="RefreshTimer" runat="server" Interval="3000" OnTick="RefreshTimer_Tick" />
    </ContentTemplate>
</asp:UpdatePanel>

<telerik:RadClientDataSource runat="server" ID="clientDS">
    <ClientEvents OnRequestStart="requestStart" />
    <DataSource>
        <WebServiceDataSourceSettings>
            <Select Url="api/TMold" RequestType="Get" DataType="JSON" />
        </WebServiceDataSourceSettings>
    </DataSource>
    <Schema>
        <Model ID="Device">
            <telerik:ClientDataSourceModelField FieldName="Device" DataType="String" />
            <telerik:ClientDataSourceModelField FieldName="Time" DataType="Date" />
            <telerik:ClientDataSourceModelField FieldName="Gateway" DataType="String" />
            <telerik:ClientDataSourceModelField FieldName="Counter" DataType="Number" />
            <telerik:ClientDataSourceModelField FieldName="Temperature" DataType="Number" />
            <telerik:ClientDataSourceModelField FieldName="Battery" DataType="Number" />
            <telerik:ClientDataSourceModelField FieldName="ClickButton" DataType="Boolean" />
            <telerik:ClientDataSourceModelField FieldName="LowBattery" DataType="Boolean" />
        </Model>
    </Schema>
</telerik:RadClientDataSource>