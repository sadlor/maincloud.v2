﻿using DeviceModule.Models;
using EverynetModule.Repositories;
using MainCloud.Dashboard.Modules.TMold.TestRealTime.Models;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.TestRealTime
{
    public partial class TestRealTime_View : DataWidget<TestRealTime_View>
    {
        public TestRealTime_View() : base(typeof(TestRealTime_View)) {}

        private Hashtable _ordersExpandedState;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //reset expanded states
                this._ordersExpandedState = null;
                this.Session["_ordersExpandedState"] = null;
            }
        }

        #region Save hierarchy state
        //Save/load expanded states Hash from the viewstate
        private Hashtable ExpandedStates
        {
            get
            {
                if (this._ordersExpandedState == null)
                {
                    _ordersExpandedState = this.ViewState["_ordersExpandedState"] as Hashtable;
                    if (_ordersExpandedState == null)
                    {
                        _ordersExpandedState = new Hashtable();
                        this.ViewState["_ordersExpandedState"] = _ordersExpandedState;
                    }
                }

                return this._ordersExpandedState;
            }
        }

        //Clear the state for all expanded children if a parent item is collapsed
        private void ClearExpandedChildren(string parentHierarchicalIndex)
        {
            string[] indexes = new string[this.ExpandedStates.Keys.Count];
            this.ExpandedStates.Keys.CopyTo(indexes, 0);
            foreach (string index in indexes)
            {
                //all indexes of child items
                if (index.StartsWith(parentHierarchicalIndex + "_") ||
                    index.StartsWith(parentHierarchicalIndex + ":"))
                {
                    this.ExpandedStates.Remove(index);
                }
            }
        }
        #endregion

        protected void gridMold_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            TMoldMessageRepository repos = new TMoldMessageRepository();
            DeviceRepository devRepos = new DeviceRepository();
            var dataSource = repos.GetLastCounter().Select(x => new { DeviceId = x.Device, Counter = x.Counter, DeviceSN = devRepos.GetDeviceSN(x.Device), Config = devRepos.GetConfig(x.Device) });
            var list = devRepos.GetAll().Select(x => x.Id);
            gridMold.DataSource = dataSource.Where(x => list.Contains(x.DeviceId)).OrderBy(x => x.DeviceSN);
        }

        protected void RefreshTimer_Tick(object sender, EventArgs e)
        {
            gridMold.Rebind();
        }

        protected void gridMold_DetailTableDataBind(object sender, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            TMoldMessageRepository repos = new TMoldMessageRepository();
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            string deviceId = dataItem.GetDataKeyValue("DeviceId").ToString();
            var dataSource = repos.GetMessages(deviceId);
            e.DetailTableView.DataSource = dataSource;
        }

        protected void gridMold_ItemCommand(object sender, GridCommandEventArgs e)
        {
            //save the expanded/selected state in the session
            if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            {
                //Is the item about to be expanded or collapsed
                if (!e.Item.Expanded)
                {
                    //Save its unique index among all the items in the hierarchy
                    this.ExpandedStates[e.Item.ItemIndexHierarchical] = true;
                }
                else //collapsed
                {
                    this.ExpandedStates.Remove(e.Item.ItemIndexHierarchical);
                    this.ClearExpandedChildren(e.Item.ItemIndexHierarchical);
                }
            }
        }

        protected void gridMold_PreRender(object sender, EventArgs e)
        {
            if (gridMold.Items.Count > 0)
            {
                //Expand all items using our custom storage
                string[] indexes = new string[this.ExpandedStates.Keys.Count];
                this.ExpandedStates.Keys.CopyTo(indexes, 0);

                ArrayList arr = new ArrayList(indexes);
                //Sort so we can guarantee that a parent item is expanded before any of 
                //its children
                arr.Sort();

                foreach (string key in arr)
                {
                    bool value = (bool)this.ExpandedStates[key];
                    if (value)
                    {
                        gridMold.Items[key].Expanded = true;
                    }
                }
            }
        }
    }
}