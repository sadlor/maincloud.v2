﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.MoldIndexAnalysis
{
    public partial class MoldIndexAnalysis_View : DataWidget<MoldIndexAnalysis_View>
    {
        public MoldIndexAnalysis_View() : base(typeof(MoldIndexAnalysis_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }

            //if (!IsPostBack || !string.IsNullOrEmpty(AssetId))
            //{
            //    ProgressiveChart.Visible = false;
            //}

            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            //edsDepartment.WhereParameters.Clear();
            //edsDepartment.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        //protected DateTime? DateStart
        //{
        //    get
        //    {
        //        if (ViewState["MachineIndexAnalysisDateStart"] == null)
        //        {
        //            return null;
        //        }
        //        return Convert.ToDateTime(ViewState["MachineIndexAnalysisDateStart"]);
        //    }
        //    set
        //    {
        //        ViewState["MachineIndexAnalysisDateStart"] = value;
        //    }
        //}

        //protected DateTime? DateEnd
        //{
        //    get
        //    {
        //        if (ViewState["MachineIndexAnalysisDateEnd"] == null)
        //        {
        //            return null;
        //        }
        //        return Convert.ToDateTime(ViewState["MachineIndexAnalysisDateEnd"]);
        //    }
        //    set
        //    {
        //        ViewState["MachineIndexAnalysisDateEnd"] = value;
        //    }
        //}

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            IndexService service = new IndexService();
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

            string moldId = moldFilter.Entries.Count > 0 ? moldFilter.Entries[0].Value : string.Empty;
            if (!string.IsNullOrEmpty(moldId))
            {
                AssetManagement.Repositories.MoldRepository moldRepo = new AssetManagement.Repositories.MoldRepository();
                decimal stampateOra = moldRepo.GetStandardRate(moldId);

                TransactionMoldRepository TRep = new TransactionMoldRepository();
                List<TransactionMold> tList = new List<TransactionMold>();
                DateTime end = dtPickEnd.SelectedDate.Value.AddDays(1);
                tList = TRep.ReadAll(x => x.MoldId == moldId && x.Start >= dtPickStart.SelectedDate && x.Start <= end).ToList();

                if (tList.Count > 0)
                {
                    ScatterLineSeries availability = indexChart.PlotArea.Series[0] as ScatterLineSeries;
                    availability.Items.Clear();
                    ScatterLineSeries efficiency = indexChart.PlotArea.Series[1] as ScatterLineSeries;
                    efficiency.Items.Clear();
                    ScatterLineSeries quality = indexChart.PlotArea.Series[2] as ScatterLineSeries;
                    quality.Items.Clear();
                    ScatterLineSeries oee = indexChart.PlotArea.Series[3] as ScatterLineSeries;
                    oee.Items.Clear();

                    indexChart.PlotArea.XAxis.MinValue = (decimal)tList.Select(x => x.Start.Date).Min().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                    //indexChart.PlotArea.XAxis.MaxValue = (decimal)tList.Select(x => x.Start.Date).Max().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                    indexChart.PlotArea.XAxis.MaxValue = (decimal)tList.Select(x => x.Start.Date).Max().AddDays(1).AddMinutes(-1).Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

                    if ((tList.Select(x => x.Start.Date).Max() - tList.Select(x => x.Start.Date).Min()).Days < 3)
                    {
                        for (DateTime hour = tList.Select(x => x.Start.Date).Min(); hour.Date <= tList.Select(x => x.Start.Date).Max(); hour = hour.AddHours(1))
                        {
                            TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Start.Date == hour.Date && x.Start.Hour == hour.Hour && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                            TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Start.Date == hour.Date && x.Start.Hour == hour.Hour && x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                            //decimal stampateOra = 0;
                            decimal qtyShotProd = 0;
                            decimal waste = 0;
                            decimal qtyProd = 0;
                            if (machineOn.TotalSeconds > 0)
                            {
                                //stampateOra = tList.Where(x => x.Start.Date == hour.Date && x.Start.Hour == hour.Hour).Select(x => x.StandardRate).Distinct().Average();
                                qtyShotProd = tList.Where(x => x.Start.Date == hour.Date && x.Start.Hour == hour.Hour).Sum(x => x.PartialCounting);
                                qtyProd = tList.Where(x => x.Start.Date == hour.Date && x.Start.Hour == hour.Hour).Sum(x => x.PartialCounting * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                                List<JobProductionWaste> wasteList = jpwRep.ReadAll(x => x.AssetId == moldId && DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(hour)).ToList();
                                if (wasteList.Count > 0)
                                {
                                    waste = Math.Round(wasteList.Where(x => x.Date.Hour == hour.Hour).Sum(x => x.QtyProductionWaste), 0);
                                }
                                //waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == assetId && DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(day)).Sum(x => x.QtyProductionWaste), 0);
                            }

                            decimal av = service.Availability(productionTime, machineOn);
                            decimal eff = service.Efficiency(stampateOra, qtyShotProd, productionTime);
                            decimal q = service.Quality(qtyProd, waste);

                            availability.SeriesItems.Add((decimal)hour.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, av);
                            efficiency.SeriesItems.Add((decimal)hour.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, eff);
                            quality.SeriesItems.Add((decimal)hour.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, q);
                            oee.SeriesItems.Add((decimal)hour.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, service.OEE(av / 100, eff / 100, q / 100));
                        }
                    }
                    else
                    {
                        foreach (DateTime day in tList.Select(x => x.Start.Date).Distinct().OrderBy(x => x.Date))
                        {
                            TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Start.Date == day.Date && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                            TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Start.Date == day.Date && x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                            //decimal stampateOra = tList.Where(x => x.Start.Date == day.Date).Select(x => x.StandardRate).Distinct().Average();
                            decimal qtyShotProd = tList.Where(x => x.Start.Date == day.Date).Sum(x => x.PartialCounting);
                            decimal qtyProd = tList.Where(x => x.Start.Date == day.Date).Sum(x => x.PartialCounting * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                            decimal waste = 0;
                            try
                            {
                                waste = Math.Round(jpwRep.ReadAll(x => x.MoldId == moldId && DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(day)).Sum(x => x.QtyProductionWaste), 0);
                            }
                            catch (InvalidOperationException) { }

                            decimal av = service.Availability(productionTime, machineOn);
                            decimal eff = service.Efficiency(stampateOra, qtyShotProd, productionTime);
                            decimal q = service.Quality(qtyProd, waste);

                            availability.SeriesItems.Add((decimal)day.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, av);
                            efficiency.SeriesItems.Add((decimal)day.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, eff);
                            quality.SeriesItems.Add((decimal)day.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, q);
                            oee.SeriesItems.Add((decimal)day.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds, service.OEE(av / 100, eff / 100, q / 100));
                        }
                    }

                    //indexChart.PlotArea.Series.Add(availability);
                    //indexChart.PlotArea.Series.Add(efficiency);
                    //indexChart.PlotArea.Series.Add(quality);

                    //asse x
                    end = end.AddDays(-1);
                    DateTime start = dtPickStart.SelectedDate.Value;
                    if ((end - start).Days < 3)
                    {
                        indexChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Hours;
                        indexChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "HH";
                        switch ((end - start).Days)
                        {
                            case 0:
                                indexChart.PlotArea.XAxis.Step = 1;
                                break;
                            case 1:
                                indexChart.PlotArea.XAxis.Step = 2;
                                break;
                            case 2:
                                indexChart.PlotArea.XAxis.Step = 3;
                                break;
                        }
                    }
                    else
                    {
                        if ((end - start).Days < 60)
                        {
                            indexChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Days;
                            if ((end - start).Days < 31)
                            {
                                indexChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "dd";
                                indexChart.PlotArea.XAxis.Step = 1;
                            }
                            else
                            {
                                indexChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "dd/MM";
                                indexChart.PlotArea.XAxis.Step = 2;
                            }
                        }
                    }
                }
            }
        }
    }
}