﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.TMold.MaintenancePlanning.Models
{
    public class PlanningView
    {
        public string Mold { get; set; }
        public string Status { get; set; }
        public decimal Hour { get; set; }

        public PlanningView() {}

        public PlanningView(string mold, string status, decimal hour)
        {
            Mold = mold;
            Status = status;
            Hour = hour;
        }
    }
}