﻿using AssetManagement.Core;
using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloud.Dashboard.Modules.TMold.MaintenancePlanning.Models;
using MainCloudFramework.UI.Modules;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.MaintenancePlanning
{
    public partial class MaintenancePlanning_View : WidgetControl<MaintenancePlanning_View>
    {
        public MaintenancePlanning_View() : base(typeof(MaintenancePlanning_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void gridPlanning_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //Visualizzare lista di stampi con relativo stato (produzione, manutenzione, disponibile, ecc...)
            //Indicare il tempo alla prossima manutenzione in scala delle prossime 120 ore

            //Steps:
            //1. Lista stampi
            //2. Ultima manutenzione
            //3. Prossima manutenzione
            //4. Progressivo attuale e colpi rimanenti
            //5. Calcolo tempo rimanente alla prossima manutenzione

            List<PlanningView> dataSource = new List<PlanningView>();

            //1.
            MoldRepository moldRepo = new MoldRepository();
            TransactionMoldService tMoldService = new TransactionMoldService();
            List<Mold> moldList = moldRepo.ReadAll(x => x.Status == MoldStatus.Produzione.ToString() || x.Status == MoldStatus.Disponibile.ToString()).ToList();
            foreach (Mold mold in moldList)
            {
                //2.
                decimal startProgressive = mold.LastMaintenanceProgressiveCount;
                //3.
                decimal endProgressive = mold.LastMaintenanceProgressiveCount + mold.Maximum;
                //4.
                decimal lastProgressive = tMoldService.LastCounterMold(mold.Id);
                decimal hourLeft = Math.Round((endProgressive - lastProgressive) / mold.ProductionCadency, 2);
                dataSource.Add(new PlanningView(mold.Name, mold.Status, hourLeft));
            }
            gridPlanning.DataSource = dataSource.OrderBy(x => x.Mold);
        }

        protected void gridPlanning_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                decimal d = Convert.ToDecimal(item.GetDataKeyValue("Hour"));
                RadHtmlChart chart = item["ChartColumn"].FindControl("RadHtmlChart1") as RadHtmlChart;
                BarSeries s = chart.PlotArea.Series[0] as BarSeries;
                //s.Appearance.FillStyle.BackgroundColor = configService.GetColorByParameter(rbtnIndex.SelectedValue, d);
                s.Items.Clear();
                s.Items.Add(d);
                chart.DataBind();
            }
        }
    }
}