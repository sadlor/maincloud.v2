﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaintenancePlanning_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.TMold.MaintenancePlanning.MaintenancePlanning_View" %>

<telerik:RadGrid runat="server" ID="gridPlanning" RenderMode="Lightweight" AutoGenerateColumns="false" AllowSorting="true"
    OnNeedDataSource="gridPlanning_NeedDataSource" OnItemDataBound="gridPlanning_ItemDataBound">
    <MasterTableView Caption="" DataKeyNames="Hour" FilterExpression="">
        <Columns>
            <telerik:GridBoundColumn DataField="Mold" HeaderText="Stampo">
                <HeaderStyle HorizontalAlign="Center" Width="115px" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Status" HeaderText="Stato">
                <HeaderStyle HorizontalAlign="Center" Width="115px" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn UniqueName="ChartColumn" HeaderText="Tempo residuo [h]" AllowFiltering="false" DataField="Hour" SortExpression="Hour">
                <ItemTemplate>
                    <div style="height: 50px;">
                        <telerik:RadHtmlChart ID="RadHtmlChart1" runat="server" Width="100%" Height="100%">
                            <Legend>
                                <Appearance Visible="false">
                                </Appearance>
                            </Legend>
                            <PlotArea>
                                <Series>
                                    <telerik:BarSeries>
                                        <LabelsAppearance Visible="false">
                                        </LabelsAppearance>
                                        <TooltipsAppearance Color="White" />
                                        <Appearance FillStyle-BackgroundColor="#f0ad4e"></Appearance>
                                    </telerik:BarSeries>
                                </Series>
                                <YAxis MinValue="0" MaxValue="120">
                                </YAxis>
                                <XAxis>
                                    <MinorGridLines Visible="false"></MinorGridLines>
                                    <MajorGridLines Visible="false"></MajorGridLines>
                                </XAxis>
                            </PlotArea>
                        </telerik:RadHtmlChart>
                    </div>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
            </telerik:GridTemplateColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>