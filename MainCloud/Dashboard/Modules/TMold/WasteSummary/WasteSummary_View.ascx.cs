﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.TMold.WasteSummary
{
    public partial class WasteSummary_View : DataWidget<WasteSummary_View>
    {
        public WasteSummary_View() : base(typeof(WasteSummary_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);

            if (!IsPostBack)
            {
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }
        }

        protected string MoldId
        {
            get
            {
                if (string.IsNullOrEmpty((string)ViewState["WasteSummaryMoldId"]))
                {
                    return null;
                }
                return ViewState["WasteSummaryMoldId"].ToString();
            }
            set
            {
                ViewState["WasteSummaryMoldId"] = value;
            }
        }

        protected DateTime? DateStart
        {
            get
            {
                if (ViewState["WasteSummaryDateStart"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["WasteSummaryDateStart"]);
            }
            set
            {
                ViewState["WasteSummaryDateStart"] = value;
            }
        }

        protected DateTime? DateEnd
        {
            get
            {
                if (ViewState["WasteSummaryDateEnd"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["WasteSummaryDateEnd"]);
            }
            set
            {
                ViewState["WasteSummaryDateEnd"] = value;
            }
        }

        #region Filter
        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            CreateFilter();
        }

        private void CreateFilter()
        {
            string moldId = moldFilter.Entries.Count > 0 ? moldFilter.Entries[0].Value : string.Empty;
            DateTime? start = dtPickStart.SelectedDate;
            DateTime? end = dtPickEnd.SelectedDate;

            List<JobProductionWaste> dataSource = GetDataSource(moldId, start, end);

            if (!string.IsNullOrEmpty(moldId))
            {
                WasteChart.Visible = true;
                MoldId = moldId;
                if (start.HasValue)
                {
                    DateStart = start.Value;
                    DateEnd = end.Value;
                }
                else
                {
                    DateStart = null;
                    DateEnd = null;
                }
            }

            if (dataSource.Count > 0)
            {
                gridJob.Visible = true;
                WasteChart.Visible = true;
                BindJobTable(dataSource);
            }
            else
            {
                gridJob.Visible = true;
                gridJob.DataSource = new List<Job>();
                gridJob.DataBind();

                WasteChart.Visible = true;
                DataTable table = new DataTable();
                table.Columns.Add(new DataColumn("label"));
                table.Columns.Add(new DataColumn("data", typeof(decimal)));
                table.Columns.Add(new DataColumn("pctValue", typeof(decimal)));
                WasteChart.DataSource = table;
                WasteChart.DataBind();
            }
        }
        #endregion

        private void BindJobTable(List<JobProductionWaste> list)
        {
            List<Job> jobList = list.Select(x => x.Job).Distinct().ToList();

            TransactionMoldRepository rep = new TransactionMoldRepository();
            foreach (Job j in jobList)
            {
                List<TransactionMold> tList = rep.ReadAll(x => x.MoldId == MoldId && x.JobId == j.Id).ToList();
                j.Order.CustomerOrder.QtyProduced = tList.Sum(x => x.PartialCounting);
                j.Order.CustomerOrder.QtyProductionWaste = list.Where(x => x.JobId == j.Id).Sum(x => x.QtyProductionWaste);
            }

            gridJob.DataSource = jobList;
            gridJob.DataBind();

            gridJob.MasterTableView.Items[0].Selected = true;
            CreateChart();
        }

        protected void CreateChart()
        {
            GridDataItem row = (GridDataItem)gridJob.SelectedItems[0];//get selected row
            string jobId = row.GetDataKeyValue("Id").ToString();

            JobProductionWasteRepository rep = new JobProductionWasteRepository();
            IQueryable<JobProductionWaste> returnList = null;
            List<JobProductionWaste> list = null;

            //List<JobProductionWaste> dataSource = GetDataSource(AssetId, DateStart, DateEnd);

            returnList = rep.ReadAll(x => x.MoldId == MoldId && x.JobId == jobId);
            if (DateStart.HasValue)
            {
                if (DateEnd.HasValue)
                {
                    list = returnList.Where(x => x.Date >= DateStart && x.Date <= DateEnd).ToList();
                }
                else
                {
                    list = returnList.Where(x => x.Date >= DateStart).ToList();
                }
            }
            else
            {
                list = returnList.ToList();
            }

            decimal totWaste = list.Sum(x => x.QtyProductionWaste);
            //lblNumWaste.Text = string.Format("Totale scarti: {0:n0}", totWaste);
            //lblPctWaste.Text = string.Format("Percentuale scarti: {0} %", Math.Round(totWaste / tot * 100, 2).ToString());

            Dictionary<string, decimal> dataSource = new Dictionary<string, decimal>();
            foreach (var item in list)
            {
                if (!dataSource.ContainsKey(string.Format("{1} - {0}", item.Cause.Description, item.Cause.ExternalCode)))
                {
                    dataSource.Add(string.Format("{1} - {0}", item.Cause.Description, item.Cause.ExternalCode), 0);
                }
                dataSource[string.Format("{1} - {0}", item.Cause.Description, item.Cause.ExternalCode)] += item.QtyProductionWaste;
            }

            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("label"));
            table.Columns.Add(new DataColumn("data", typeof(decimal)));
            table.Columns.Add(new DataColumn("pctValue", typeof(decimal)));
            foreach (var item in dataSource.OrderBy(x => x.Key.Split('-')[0].Trim().Length).ThenBy(x => x.Key.Split('-')[0].Trim()))
            {
                table.Rows.Add(new object[] { item.Key, item.Value, Math.Round(item.Value / totWaste * 100, 2) });
            }

            WasteChart.DataSource = table;
            WasteChart.DataBind();
        }

        protected void gridJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreateChart();
        }

        private List<JobProductionWaste> GetDataSource(string moldId, DateTime? start, DateTime? end)
        {
            JobProductionWasteRepository rep = new JobProductionWasteRepository();
            List<JobProductionWaste> dataSource = new List<JobProductionWaste>();
            IQueryable<JobProductionWaste> returnList = null;

            if (!string.IsNullOrEmpty(moldId))
            {
                returnList = rep.ReadAll(x => x.MoldId == moldId);
            }

            if (returnList != null && returnList.Count() > 0)
            {
                if (start.HasValue)
                {
                    if (end.HasValue)
                    {
                        dataSource = returnList.Where(x => x.Date >= start && x.Date <= end).ToList();
                    }
                    else
                    {
                        dataSource = returnList.Where(x => x.Date >= start).ToList();
                    }
                }
                else
                {
                    dataSource = returnList.ToList();
                }
            }
            //else
            //{
            //    if (start.HasValue)
            //    {
            //        if (end.HasValue)
            //        {
            //            dataSource = rep.ReadAll(x => x.Date >= start && x.Date <= end).ToList();
            //        }
            //        else
            //        {
            //            dataSource = rep.ReadAll(x => x.Date >= start).ToList();
            //        }
            //    }
            //}

            return dataSource;
        }

        protected void gridJob_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (gridJob.Visible)
            {
                List<JobProductionWaste> list = GetDataSource(MoldId, DateStart, DateEnd);
                IEnumerable<Job> jobList = list.Select(x => x.Job).Distinct();

                TransactionMoldRepository rep = new TransactionMoldRepository();
                foreach (Job j in jobList)
                {
                    List<TransactionMold> tList = rep.ReadAll(x => x.MoldId == MoldId && x.JobId == j.Id).ToList();
                    j.Order.CustomerOrder.QtyProduced = tList.Sum(x => x.PartialCounting);
                    j.Order.CustomerOrder.QtyProductionWaste = list.Where(x => x.JobId == j.Id).Sum(x => x.QtyProductionWaste);
                }

                gridJob.DataSource = jobList;
            }
        }
    }
}