﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Mes.Availability.Models
{
    public class AvailabilityViewModel
    {
        public string Machine { get; set; }
        public string Mold { get; set; }
        public string Order { get; set; }
        public string Article { get; set; }
        public decimal ResidualTime { get; set; }

        public AvailabilityViewModel()
        {
        }

        public AvailabilityViewModel(string machine, string mold, string order, string article, decimal residual)
        {
            Machine = machine;
            Mold = mold;
            Order = order;
            Article = article;
            ResidualTime = residual;
        }
    }
}