﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Availability_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.Availability.Availability_View" %>

<asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>
        <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="gridMachine" AutoGenerateColumns="false" AllowSorting="true"
            OnNeedDataSource="gridMachine_NeedDataSource" OnItemDataBound="gridMachine_ItemDataBound">
            <MasterTableView Caption="" DataKeyNames="ResidualTime" FilterExpression="">
                <Columns>
                    <telerik:GridBoundColumn DataField="Machine" HeaderText="Macchina">
                        <HeaderStyle HorizontalAlign="Center" Width="115px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Mold" HeaderText="Stampo">
                        <HeaderStyle HorizontalAlign="Center" Width="115px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Order" HeaderText="Commessa">
                        <HeaderStyle HorizontalAlign="Center" Width="110px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Article" HeaderText="Articolo">
                        <HeaderStyle HorizontalAlign="Center" Width="200px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="ChartColumn" HeaderText="Tempo residuo [h]" AllowFiltering="false" DataField="ResidualTime" SortExpression="ResidualTime">
                        <ItemTemplate>
                            <div style="height: 50px;">
                                <telerik:RadHtmlChart ID="RadHtmlChart1" runat="server" Width="100%" Height="100%">
                                    <Legend>
                                        <Appearance Visible="false">
                                        </Appearance>
                                    </Legend>
                                    <PlotArea>
                                        <Series>
                                            <telerik:BarSeries>
                                                <LabelsAppearance Visible="false">
                                                </LabelsAppearance>
                                                <TooltipsAppearance Color="White" />
                                                <Appearance FillStyle-BackgroundColor="#f0ad4e"></Appearance>
                                            </telerik:BarSeries>
                                        </Series>
                                        <YAxis MinValue="0" MaxValue="120">
                                        </YAxis>
                                        <XAxis>
                                            <MinorGridLines Visible="false"></MinorGridLines>
                                            <MajorGridLines Visible="false"></MajorGridLines>
                                        </XAxis>
                                    </PlotArea>
                                </telerik:RadHtmlChart>
                            </div>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </ContentTemplate>
</asp:UpdatePanel>