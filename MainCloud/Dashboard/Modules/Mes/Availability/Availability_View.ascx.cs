﻿using AssetManagement.Repositories;
using MainCloud.Dashboard.Modules.Mes.Availability.Models;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Core;
using MES.Models;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Mes.Availability
{
    public partial class Availability_View : DataWidget<Availability_View>
    {
        public Availability_View() : base(typeof(Availability_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void gridMachine_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            MoldRepository moldRepo = new MoldRepository();
            var assetList = AS.GetAssetEntity().Select(x => new { x.Id, x.Code }).ToList();
            TransactionService tService = new TransactionService();
            var lastTransactions = tService.GetAllTransactionOpen(assetList.Select(x => x.Id));
            List<AvailabilityViewModel> dataSource = new List<AvailabilityViewModel>();
            lastTransactions.Where(x => !string.IsNullOrEmpty(x.JobId)).ToList().ForEach(x =>
            {
                List<Transaction> tList = tService.GetTransactionListByJobId(x.MachineId, x.JobId);
                TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(t => t.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(t => t.Duration));
                decimal productionHour = (decimal)productionTime.TotalHours;
                decimal qtyOrdered = x.Job.Order.QtyOrdered;
                decimal qtyProduced = tList.Sum(t => (t.PartialCounting - t.BlankShot) * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1));
                decimal qtyResidual = qtyOrdered - qtyProduced;
                decimal leftTime = Math.Round((productionHour * qtyResidual) / (qtyProduced > 0 ? qtyProduced : 1), 2);
                dataSource.Add(
                    new AvailabilityViewModel(
                        assetList.Where(a => a.Id == x.MachineId).Select(a => a.Code).FirstOrDefault(),
                        moldRepo.GetName(x.MoldId),
                        x.Job.Order.CustomerOrder.OrderCode,
                        x.Job.Order.Article.Code,
                        leftTime));
            });

            gridMachine.DataSource = dataSource;
        }

        protected void gridMachine_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                decimal d = Convert.ToDecimal(item.GetDataKeyValue("ResidualTime"));
                RadHtmlChart chart = item["ChartColumn"].FindControl("RadHtmlChart1") as RadHtmlChart;
                BarSeries s = chart.PlotArea.Series[0] as BarSeries;
                //s.Appearance.FillStyle.BackgroundColor = configService.GetColorByParameter(rbtnIndex.SelectedValue, d);
                s.Items.Clear();
                s.Items.Add(d);
                chart.DataBind();
            }
        }
    }
}