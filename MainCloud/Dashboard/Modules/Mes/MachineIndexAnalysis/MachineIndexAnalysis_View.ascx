﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MachineIndexAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.MachineIndexAnalysis.MachineIndexAnalysis_View" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="panel panel-info">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="machineFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px"
                            TextSettings-SelectionMode="Single" DataSourceID="edsMachine" DataTextField="Code" DataValueField="Id" InputType="Text" Filter="Contains" Label="Macchina: ">
                        </telerik:RadAutoCompleteBox>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="departmentFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px"
                            TextSettings-SelectionMode="Single" DataSourceID="edsDepartment" DataTextField="Description" DataValueField="Id" InputType="Text" Filter="Contains" Label="Reparto: ">
                        </telerik:RadAutoCompleteBox>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>
                </div>
                <div style="text-align:right;">
                    <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
                </div>
            </div>
        </div>

        <telerik:RadHtmlChart runat="server" ID="indexChart">
            <PlotArea>
                <Series>
                    <telerik:ScatterLineSeries Name="Availability">
                        <TooltipsAppearance>
                            <ClientTemplate>
                                #= kendo.format(\'{0:dd/MM}\', new Date(value.x)) #,  #= value.y #
                            </ClientTemplate>
                        </TooltipsAppearance>
                        <LabelsAppearance Visible="false"></LabelsAppearance>
                    </telerik:ScatterLineSeries>
                </Series>
                <Series>
                    <telerik:ScatterLineSeries Name="Efficiency">
                        <TooltipsAppearance>
                            <ClientTemplate>
                                #= kendo.format(\'{0:dd/MM}\', new Date(value.x)) #,  #= value.y #
                            </ClientTemplate>
                        </TooltipsAppearance>
                        <LabelsAppearance Visible="false"></LabelsAppearance>
                    </telerik:ScatterLineSeries>
                </Series>
                <Series>
                    <telerik:ScatterLineSeries Name="Quality">
                        <TooltipsAppearance>
                            <ClientTemplate>
                                #= kendo.format(\'{0:dd/MM}\', new Date(value.x)) #,  #= value.y #
                            </ClientTemplate>
                        </TooltipsAppearance>
                        <LabelsAppearance Visible="false"></LabelsAppearance>
                    </telerik:ScatterLineSeries>
                </Series>
                <Series>
                    <telerik:ScatterLineSeries Name="OEE">
                        <TooltipsAppearance>
                            <ClientTemplate>
                                #= kendo.format(\'{0:dd/MM}\', new Date(value.x)) #,  #= value.y #
                            </ClientTemplate>
                        </TooltipsAppearance>
                        <LabelsAppearance Visible="false"></LabelsAppearance>
                    </telerik:ScatterLineSeries>
                </Series>
                <YAxis>
                    <%--<TitleAppearance Text="Qtà colpi" />--%>
                    <%--<MajorGridLines Visible="false" />--%>
                    <%--<MinorGridLines Visible="false" />--%>
                </YAxis>
                <XAxis Type="Date">
                    <%--<MajorGridLines Visible="false" />--%>
                    <%--<MinorGridLines Visible="false" />--%>
                </XAxis>
            </PlotArea>
        </telerik:RadHtmlChart>
    </ContentTemplate>
</asp:UpdatePanel>

<ef:EntityDataSource ID="edsMachine" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    AutoGenerateWhereClause="true" OrderBy="it.Description">
</ef:EntityDataSource>
<ef:EntityDataSource ID="edsDepartment" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Departments"
    AutoGenerateWhereClause="true" OrderBy="it.Description">
</ef:EntityDataSource>