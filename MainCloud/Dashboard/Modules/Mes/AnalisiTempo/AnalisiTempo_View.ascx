﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnalisiTempo_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.AnalisiTempo.AnalisiTempo_View" %>

<div class="panel panel-info">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="departmentFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px"
                    TextSettings-SelectionMode="Single" DataSourceID="edsDepartment" DataTextField="Description" DataValueField="Id" InputType="Text" Filter="Contains" Label="Reparto: ">
                </telerik:RadAutoCompleteBox>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <asp:RadioButtonList runat="server" ID="rbtnIndex" AutoPostBack="false" RepeatDirection="Vertical">
                    <asp:ListItem Text="Disponibilità" Value="Availability" Selected="True" />
                    <asp:ListItem Text="Efficienza" Value="Efficiency" />
                    <asp:ListItem Text="Qualità" Value="Quality" />
                    <asp:ListItem Text="OEE" Value="OEE" />
                    <asp:ListItem Text="OEE valori" Value="OEEvalue" />
                </asp:RadioButtonList>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <%--<br />--%>
        <div style="text-align:right;">
            <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
        </div>
    </div>
</div>

<telerik:RadGrid runat="server" ID="gridMachine" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="false"
    AllowFilteringByColumn="true" AllowSorting="true" AllowPaging="true"
    OnNeedDataSource="gridMachine_NeedDataSource" OnItemDataBound="gridMachine_ItemDataBound"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <%--<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />--%>
    <MasterTableView Caption="" DataKeyNames="Code, Index">
        <Columns>
            <telerik:GridBoundColumn DataField="Code" HeaderText="Macchina" CurrentFilterFunction="Contains" AllowSorting="false" AllowFiltering="false"
                ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MachineOnTime" HeaderText="Ore utilizzo" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Production" HeaderText="Produzione" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Stop" HeaderText="Fermo" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Index" HeaderText="Indice %" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn UniqueName="ChartColumn" HeaderText="Indice" AllowFiltering="false">
                <ItemTemplate>
                    <div style="max-width: 300px; height: 50px;">
                        <telerik:RadHtmlChart ID="RadHtmlChart1" runat="server" Width="100%" Height="100%">
                            <Legend>
                                <Appearance Visible="false">
                                </Appearance>
                            </Legend>
                            <PlotArea>
                                <Series>
                                    <telerik:BarSeries>
                                        <LabelsAppearance Visible="false">
                                        </LabelsAppearance>
                                        <TooltipsAppearance Color="White" />
                                    </telerik:BarSeries>
                                </Series>
                                <YAxis MinValue="0" MaxValue="100">
                                </YAxis>
                                <XAxis>
                                    <MinorGridLines Visible="false"></MinorGridLines>
                                    <MajorGridLines Visible="false"></MajorGridLines>
                                </XAxis>
                            </PlotArea>
                        </telerik:RadHtmlChart>
                    </div>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn DataField="QtyProd" HeaderText="Qta Prodotta" AllowFiltering="false" AllowSorting="false" DataFormatString="{0:n0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Waste" HeaderText="Scarti" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Cadency" HeaderText="Cadenza<br/>effettiva" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="StampateOraTeorico" HeaderText="Cadenza teorica" AllowFiltering="false" AllowSorting="false" Display="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="OreProdTeorico" HeaderText="Ore<br/>assegnate" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <GroupingSettings CaseSensitive="false" />
</telerik:RadGrid>

<telerik:RadGrid runat="server" ID="gridOEE" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="false"
    AllowFilteringByColumn="true" AllowSorting="true" AllowPaging="true"
    OnNeedDataSource="gridOEE_NeedDataSource" OnItemDataBound="gridOEE_ItemDataBound"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <MasterTableView DataKeyNames="Code, Availability, Efficiency, Quality">
        <Columns>
            <telerik:GridBoundColumn DataField="Code" HeaderText="Macchina" CurrentFilterFunction="Contains" AllowSorting="false"
                ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn UniqueName="DisponibilityColumn" HeaderText="Disponibilità" AllowFiltering="false">
                <ItemTemplate>
                    <div style="max-width: 150px; height: 50px;">
                        <telerik:RadHtmlChart ID="DisponibilityChart" runat="server" Width="100%" Height="100%">
                            <Legend>
                                <Appearance Visible="false">
                                </Appearance>
                            </Legend>
                            <PlotArea>
                                <Series>
                                    <telerik:BarSeries>
                                        <LabelsAppearance Visible="false">
                                        </LabelsAppearance>
                                        <TooltipsAppearance Color="White" />
                                    </telerik:BarSeries>
                                </Series>
                                <YAxis MinValue="0" MaxValue="100">
                                </YAxis>
                                <XAxis>
                                    <MinorGridLines Visible="false"></MinorGridLines>
                                    <MajorGridLines Visible="false"></MajorGridLines>
                                </XAxis>
                            </PlotArea>
                        </telerik:RadHtmlChart>
                    </div>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="150px" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn UniqueName="EfficiencyColumn" HeaderText="Efficienza" AllowFiltering="false">
                <ItemTemplate>
                    <div style="max-width: 150px; height: 50px;">
                        <telerik:RadHtmlChart ID="EfficiencyChart" runat="server" Width="100%" Height="100%">
                            <Legend>
                                <Appearance Visible="false">
                                </Appearance>
                            </Legend>
                            <PlotArea>
                                <Series>
                                    <telerik:BarSeries>
                                        <LabelsAppearance Visible="false">
                                        </LabelsAppearance>
                                        <TooltipsAppearance Color="White" />
                                    </telerik:BarSeries>
                                </Series>
                                <YAxis MinValue="0" MaxValue="100">
                                </YAxis>
                                <XAxis>
                                    <MinorGridLines Visible="false"></MinorGridLines>
                                    <MajorGridLines Visible="false"></MajorGridLines>
                                </XAxis>
                            </PlotArea>
                        </telerik:RadHtmlChart>
                    </div>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="150px" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn UniqueName="QualityColumn" HeaderText="Qualità" AllowFiltering="false">
                <ItemTemplate>
                    <div style="max-width: 150px; height: 50px;">
                        <telerik:RadHtmlChart ID="QualityChart" runat="server" Width="100%" Height="100%">
                            <Legend>
                                <Appearance Visible="false">
                                </Appearance>
                            </Legend>
                            <PlotArea>
                                <Series>
                                    <telerik:BarSeries>
                                        <LabelsAppearance Visible="false">
                                        </LabelsAppearance>
                                        <TooltipsAppearance Color="White" />
                                    </telerik:BarSeries>
                                </Series>
                                <YAxis MinValue="0" MaxValue="100">
                                </YAxis>
                                <XAxis>
                                    <MinorGridLines Visible="false"></MinorGridLines>
                                    <MajorGridLines Visible="false"></MajorGridLines>
                                </XAxis>
                            </PlotArea>
                        </telerik:RadHtmlChart>
                    </div>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="150px" />
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn DataField="OEE" HeaderText="Indice %" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn UniqueName="OEEColumn" HeaderText="OEE" AllowFiltering="false">
                <ItemTemplate>
                    <div style="max-width: 150px; height: 50px;">
                        <telerik:RadHtmlChart ID="OEEChart" runat="server" Width="100%" Height="100%">
                            <Legend>
                                <Appearance Visible="false">
                                </Appearance>
                            </Legend>
                            <PlotArea>
                                <Series>
                                    <telerik:BarSeries>
                                        <LabelsAppearance Visible="false">
                                        </LabelsAppearance>
                                        <TooltipsAppearance Color="White" />
                                    </telerik:BarSeries>
                                </Series>
                                <YAxis MinValue="0" MaxValue="100">
                                </YAxis>
                                <XAxis>
                                    <MinorGridLines Visible="false"></MinorGridLines>
                                    <MajorGridLines Visible="false"></MajorGridLines>
                                </XAxis>
                            </PlotArea>
                        </telerik:RadHtmlChart>
                    </div>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="150px" />
            </telerik:GridTemplateColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<telerik:RadGrid runat="server" ID="gridValue" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="false"
    AllowFilteringByColumn="false" AllowSorting="true" AllowPaging="true"
    OnNeedDataSource="gridValue_NeedDataSource" OnItemDataBound="gridValue_ItemDataBound"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <%--<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />--%>
    <MasterTableView Caption="" DataKeyNames="Code">
        <ColumnGroups>
            <telerik:GridColumnGroup Name="Indici" HeaderText="Indici"></telerik:GridColumnGroup>
        </ColumnGroups>
        <Columns>
            <telerik:GridBoundColumn DataField="Code" HeaderText="Macchina" CurrentFilterFunction="Contains" AllowSorting="false" AllowFiltering="false"
                ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MachineOnTime" HeaderText="Ore utilizzo" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Production" HeaderText="Produzione" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Stop" HeaderText="Fermo" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Availability" HeaderText="Disp %" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Efficiency" HeaderText="Eff %" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Quality" HeaderText="Q %" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridCalculatedColumn DataFields="Availability,Efficiency,Quality" HeaderText="OEE %" Expression="(({0}/100) * ({1} / 100) * ({2} / 100)) * 100"
                AllowFiltering="false" AllowSorting="false" DataFormatString="{0:n2}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridCalculatedColumn>
            <telerik:GridBoundColumn DataField="OreProdTeorico" HeaderText="Ore<br/>assegnate" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Cadency" HeaderText="Cadenza<br/>effettiva" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="UnitTime" HeaderText="Tempo<br/>unitario" AllowFiltering="false" AllowSorting="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="QtyProd" HeaderText="Pezzi<br/>prodotti" AllowFiltering="false" AllowSorting="false" DataFormatString="{0:n0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Waste" HeaderText="Scarti" AllowFiltering="false" AllowSorting="false" DataFormatString="{0:n0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <GroupingSettings CaseSensitive="false" />
</telerik:RadGrid>

<ef:EntityDataSource ID="edsDepartment" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Departments"
    AutoGenerateWhereClause="true" OrderBy="it.Description">
</ef:EntityDataSource>