﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageOperatorRoles_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.ManageOperatorRoles.ManageOperatorRoles_View" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1"  MultiPageID="RadMultiPage1" SelectedIndex="0" Skin="Silk">
    <Tabs>
        <telerik:RadTab Text="Ruoli" Width="200px"></telerik:RadTab>
        <telerik:RadTab Text="Operatori" Width="200px"></telerik:RadTab>
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage runat="server" ID="RadMultiPage1"  SelectedIndex="0">
    <telerik:RadPageView runat="server" ID="RadPageView1">
        <br />
        <mcf:Alert runat="server" id="AlertMessageRole" />

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-inline">
                    <div class="form-group">
                        <asp:TextBox ID="txtNewRoleName" runat="server" />
                    </div>
                    <asp:Button runat="server" ID="BtnAdd" class="btn btn-default" Text="Aggiungi Ruolo" OnClick="BtnAdd_Click" />
                </div>
            </div>
        </div>

        <asp:GridView ID="DgvRoles" runat="server" DataKeyNames="Id"
            CssClass="table table-hover table-striped" GridLines="None"
            OnRowCancelingEdit="DgvRoles_RowCancelingEdit"
            OnRowDeleting="DgvRoles_RowDeleting"
            OnRowEditing="DgvRoles_RowEditing"
            OnRowUpdating="DgvRoles_RowUpdating"
            AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="ID" Visible="false" />
                <asp:BoundField DataField="Name" HeaderText="Nome" />

                <asp:CheckBoxField DataField="AllowSetup" HeaderText="Attrezzaggio" />
                <asp:CheckBoxField DataField="AllowProduction" HeaderText="Produzione" />
                <asp:CheckBoxField DataField="AllowMaintenance" HeaderText="Manutenzione" />
                <asp:CheckBoxField DataField="AllowQuality" HeaderText="Qualità" />
                <asp:CheckBoxField DataField="AllowWaste" HeaderText="Scarti" />
                <asp:CheckBoxField DataField="AllowSelectJob" HeaderText="Seleziona<br/>job" />
                <asp:CheckBoxField DataField="AllowSelectMold" HeaderText="Seleziona<br/>stampo" />
                <asp:CheckBoxField DataField="AllowSelectMP" HeaderText="Seleziona<br/>MP" />
                <asp:CheckBoxField DataField="AllowConfirmStartUpProduction" HeaderText="Seleziona<br/>benestare" />
                <asp:CheckBoxField DataField="AllowOEE" HeaderText="Visualizza<br/>OEE" />
                <asp:CheckBoxField DataField="AllowAnalysis" HeaderText="Visualizza<br/>Analisi" />

                <asp:TemplateField ShowHeader="False">
                    <EditItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Update" class="btn btn-success btn-xs" Visible='<%# ("Host Administrators".Contains((string)Eval("Name"))) ? false : true %>'><i class="fa fa-check"></i> Save</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" class="btn btn-danger btn-xs" Visible='<%# ("Host Administrators Guests".Contains((string)Eval("Name"))) ? false : true %>'><i class="fa fa-trash"></i> Cancel</asp:LinkButton>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" class="btn btn-primary btn-xs" Visible='<%# ("Host Administrators".Contains((string)Eval("Name"))) ? false : true %>'><i class="fa fa-pencil"></i> Edit</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" class="btn btn-danger btn-xs" Visible='<%# ("Host Administrators Guests".Contains((string)Eval("Name"))) ? false : true %>'><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="cursor-pointer" />
        </asp:GridView>
    </telerik:RadPageView>
    <telerik:RadPageView runat="server" ID="RadPageView2">
        <br />
        <mcf:Alert runat="server" id="AlertMessageOperator" />

        <%--<asp:UpdatePanel runat="server">
            <ContentTemplate>--%>

        <telerik:RadGrid runat="server" ID="gridOperator" RenderMode="Lightweight" AutoGenerateColumns="false" GridLines="None" AllowFilteringByColumn="true"
            OnNeedDataSource="gridOperator_NeedDataSource" OnItemDataBound="gridOperator_ItemDataBound"
            OnUpdateCommand="gridOperator_UpdateCommand" OnInsertCommand="gridOperator_InsertCommand" OnDeleteCommand="gridOperator_DeleteCommand"
            LocalizationPath="~/App_GlobalResources/" CssClass="table table-hover table-striped">
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView DataKeyNames="Id" CommandItemDisplay="Top" EditMode="InPlace">
                <CommandItemSettings AddNewRecordText="Nuovo operatore" ShowAddNewRecordButton="true" ShowRefreshButton="false" />
                <Columns>
                    <telerik:GridBoundColumn DataField="UserName" HeaderText="Nome" AllowFiltering="true"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                        <HeaderStyle Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Code" HeaderText="Matricola" AllowFiltering="true"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                        <HeaderStyle Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Badge" HeaderText="Badge" AllowFiltering="true"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                        <HeaderStyle Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Pin" UniqueName="PIN" DataField="PIN" AllowFiltering="false">
                        <ItemTemplate>
                            <asp:Label ID="lblpass" runat="server" Text='<%# Eval("PIN")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadTextBox ID="txtPIN" runat="server" Text='<%# Bind("PIN") %>' TextMode="Password" InputType="Number">
                            </telerik:RadTextBox>
                        </EditItemTemplate>
                        <HeaderStyle Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="Qualification" HeaderText="Qualifica" AllowFiltering="false">
                        <HeaderStyle Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Ruolo" UniqueName="columnRole" AllowFiltering="false">
                        <EditItemTemplate>
                            <asp:Label ID="lblRole" runat="server" Text='<%# GetUserRole(DataBinder.Eval(Container.DataItem, "Id") as string) %>' Visible="false" />
                            <asp:DropDownList ID="ddlRoles" runat="server" DataTextField="Name" DataValueField="Id">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRole" runat="server" Text='<%# GetUserRole(DataBinder.Eval(Container.DataItem, "Id") as string) %>' />
                        </ItemTemplate>
                        <HeaderStyle Font-Bold="true" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="" AllowFiltering="false">
                        <InsertItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="PerformInsert" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Save</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Cancel</asp:LinkButton>
                        </InsertItemTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Update" class="btn btn-success btn-xs" Visible='<%# ("Host Administrators".Contains((string)Eval("UserName"))) ? false : true %>'><i class="fa fa-check"></i> Save</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" class="btn btn-danger btn-xs" Visible='<%# ("Host Administrators Guests".Contains((string)Eval("UserName"))) ? false : true %>'><i class="fa fa-trash"></i> Cancel</asp:LinkButton>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" class="btn btn-primary btn-xs" Visible='<%# ("Host Administrators".Contains((string)Eval("UserName"))) ? false : true %>'><i class="fa fa-pencil"></i> Edit</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" class="btn btn-danger btn-xs" Visible='<%# ("Host Administrators Guests".Contains((string)Eval("UserName"))) ? false : true %>' OnClientClick="return confirm('Confermi l'eliminazione?')"><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>

            <%--</ContentTemplate>
        </asp:UpdatePanel>--%>

    </telerik:RadPageView>
</telerik:RadMultiPage>