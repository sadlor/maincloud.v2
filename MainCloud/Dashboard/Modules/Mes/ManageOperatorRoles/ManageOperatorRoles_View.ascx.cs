﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Mes.ManageOperatorRoles
{
    public partial class ManageOperatorRoles_View : DataWidget<ManageOperatorRoles_View>
    {
        public ManageOperatorRoles_View() : base(typeof(ManageOperatorRoles_View)) { }

        private OperatorRoleService operatorRoleService = new OperatorRoleService();

        private void GridRolesDataBind()
        {
            DgvRoles.DataSource = operatorRoleService.RolesInCurrentApplication();
            DgvRoles.DataBind();

            DgvRoles.UseAccessibleHeader = true;
            if (DgvRoles.HeaderRow != null)
            {
                DgvRoles.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridRolesDataBind();
            }
        }

        protected void DgvRoles_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
        }

        protected void DgvRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                var usersContext = new MesDbContext();
                string id = e.Keys["Id"] as string;
                var role = usersContext.MesRoles.FirstOrDefault(u => u.Id == id);
                if (role != null)
                {
                    usersContext.MesRoles.Remove(role);
                    //usersContext.Entry(user).State = EntityState.Deleted;
                    usersContext.SaveChanges();
                }
                GridRolesDataBind();
            }
            catch (Exception ex)
            {
                AlertMessageRole.ShowError(ex.InnerException.InnerException.Message);
            }
        }

        protected void DgvRoles_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //Set the edit index.
            DgvRoles.EditIndex = e.NewEditIndex;
            //Bind data to the GridView control.
            GridRolesDataBind();
        }

        protected void DgvRoles_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //Reset the edit index.
            DgvRoles.EditIndex = -1;
            //Bind data to the GridView control.
            GridRolesDataBind();
        }

        protected void DgvRoles_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                var usersContext = new MesDbContext();
                string id = e.Keys["Id"] as string;
                var role = usersContext.MesRoles.FirstOrDefault(u => u.Id == id);
                if (role != null)
                {
                    var row = DgvRoles.Rows[e.RowIndex];
                    role.Name = (row.Cells[1].Controls[0] as TextBox).Text;
                    role.Description = role.Name;
                    role.AllowSetup = (row.Cells[2].Controls[0] as CheckBox).Checked;
                    role.AllowProduction = (row.Cells[3].Controls[0] as CheckBox).Checked;
                    role.AllowMaintenance = (row.Cells[4].Controls[0] as CheckBox).Checked;
                    role.AllowQuality = (row.Cells[5].Controls[0] as CheckBox).Checked;
                    role.AllowWaste = (row.Cells[6].Controls[0] as CheckBox).Checked;
                    role.AllowSelectJob = (row.Cells[7].Controls[0] as CheckBox).Checked;
                    role.AllowSelectMold = (row.Cells[8].Controls[0] as CheckBox).Checked;
                    role.AllowSelectMP = (row.Cells[9].Controls[0] as CheckBox).Checked;
                    role.AllowConfirmStartUpProduction = (row.Cells[10].Controls[0] as CheckBox).Checked;
                    role.AllowOEE = (row.Cells[11].Controls[0] as CheckBox).Checked;
                    role.AllowAnalysis = (row.Cells[12].Controls[0] as CheckBox).Checked;
                    usersContext.Entry(role).State = System.Data.Entity.EntityState.Modified;
                    usersContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                AlertMessageRole.ShowError(ex.InnerException.InnerException.Message);
            }

            //Reset the edit index.
            DgvRoles.EditIndex = -1;
            GridRolesDataBind();
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string appId = MultiTenantsHelper.ApplicationId;
                IdentityResult roleresult;

                // TODO: Verificare se serve
                if (string.IsNullOrEmpty(appId))
                {
                    var roleManager = new MesRoleManager<MesRole>(new RoleStore<MesRole>(new MesDbContext()));
                    var newRole = new MesRole(txtNewRoleName.Text);
                    roleresult = roleManager.Create(newRole);
                }
                else
                {
                    var roleManager = new MesRoleManager<MesRole>(new MesRoleStore<MesRole>(appId, new MesDbContext()));
                    var newRole = new MesRole(txtNewRoleName.Text, txtNewRoleName.Text);
                    //newRole.ApplicationId = MultiTenantsHelper.ApplicationId;
                    roleresult = roleManager.Create(newRole);
                }

                if (roleresult.Succeeded)
                {
                    txtNewRoleName.Text = "";
                }
                else
                {
                    AlertMessageRole.ShowError(string.Join(",", roleresult.Errors));
                }
                GridRolesDataBind();
            }
            catch (Exception ex)
            {
                AlertMessageRole.ShowError(string.Join(",", ex.Message));
            }
        }

        public string GetUserRole(string userId)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                var r = operatorRoleService.FindUserRoleCurrentApp(userId);
                if (r != null)
                {
                    return r.Name;
                }
            }
            return "";
        }

        protected void gridOperator_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            OperatorRepository userRepository = new OperatorRepository();
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var operId = (string)editableItem.GetDataKeyValue("Id");
            var user = userRepository.FindByID(operId);
            if (user != null)
            {
                editableItem.UpdateValues(user);
                user.Badge = user.Badge.PadLeft(3, '0');
                userRepository.Update(user);
                userRepository.SaveChanges();

                //user.PIN = (string)values["PIN"];
                //user.Qualification = (string)values["Qualification"];
                //userRepository.Update(user);

                //try
                //{
                //    userRepository.SaveChanges();
                //}
                //catch (Exception ex)
                //{
                //    AlertMessageOperator.ShowError(ex.Message);
                //}

                var ddl = editableItem["columnRole"].FindControl("ddlRoles") as DropDownList;
                operatorRoleService.ChangeRoleAndUpdateUser(ddl.SelectedValue, user);
            }
        }
        
        protected void gridOperator_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            Operator user = new Operator((string)values["UserName"]);
            user.Code = (string)values["Code"];
            if (!string.IsNullOrEmpty((string)values["Badge"]))
            {
                user.Badge = (string)values["Badge"];
                user.Badge = user.Badge.PadLeft(3, '0');
            }
            user.Qualification = (string)values["Qualification"];
            user.PIN = (string)values["PIN"];
            //user.ApplicationId = MultiTenantsHelper.ApplicationId;

            IdentityResult result = operatorRoleService.CreateUserCurrentApplication(user);
            if (result.Succeeded)
            {
                var ddl = editableItem["columnRole"].FindControl("ddlRoles") as DropDownList;
                operatorRoleService.ChangeRoleAndUpdateUser(ddl.SelectedValue, user);
            }
            else
            {
                e.Canceled = true;
                AlertMessageOperator.ShowError(string.Join(",", result.Errors));
            }
        }

        protected void gridOperator_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            gridOperator.DataSource = operatorRoleService.OperatorsInCurrentApplication();
        }

        protected void gridOperator_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item.IsInEditMode && !(e.Item is IGridInsertItem))
            {
                //(e.Item as GridEditableItem)["UserName"].Enabled = false;
                (e.Item as GridEditableItem)["Code"].Enabled = false;
            }
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem item = e.Item as GridEditableItem;
                // access/modify the edit item template settings here
                DropDownList list = item.FindControl("ddlRoles") as DropDownList;
                list.DataSource = operatorRoleService.RolesInCurrentApplication();
                list.DataBind();

                //Select the role of operator in DropDownList
                string roleId = (item.FindControl("lblRole") as Label).Text;
                if (!string.IsNullOrEmpty(roleId))
                {
                    list.Items.FindByText(roleId).Selected = true;
                }
            }
        }

        protected void gridOperator_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            OperatorRepository opRep = new OperatorRepository();

            var operId = ((GridDataItem)e.Item).GetDataKeyValue("Id").ToString();
            var oper = opRep.FindByID(operId);
            if (oper != null)
            {
                try
                {
                    opRep.Delete(oper);
                    opRep.SaveChanges();
                }
                catch (Exception ex)
                {
                    AlertMessageOperator.ShowError(ex.Message);
                }
            }
        }
    }
}