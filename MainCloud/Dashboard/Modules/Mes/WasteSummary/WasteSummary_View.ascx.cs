﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Mes.WasteSummary
{
    public partial class WasteSummary_View : DataWidget<WasteSummary_View>
    {
        public WasteSummary_View() : base(typeof(WasteSummary_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMachine.WhereParameters.Clear();
            edsMachine.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);

            if (!IsPostBack)
            {
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }
        }

        protected string AssetId
        {
            get
            {
                if (string.IsNullOrEmpty((string)ViewState["ShotAnalysisAssetId"]))
                {
                    return null;
                }
                return ViewState["ShotAnalysisAssetId"].ToString();
            }
            set
            {
                ViewState["ShotAnalysisAssetId"] = value;
            }
        }

        protected DateTime? DateStart
        {
            get
            {
                if (ViewState["ShotAnalysisDateStart"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["ShotAnalysisDateStart"]);
            }
            set
            {
                ViewState["ShotAnalysisDateStart"] = value;
            }
        }

        protected DateTime? DateEnd
        {
            get
            {
                if (ViewState["ShotAnalysisDateEnd"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["ShotAnalysisDateEnd"]);
            }
            set
            {
                ViewState["ShotAnalysisDateEnd"] = value;
            }
        }

        #region Filter
        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            CreateFilter();
        }

        private void CreateFilter()
        {
            string assetId = machineFilter.Entries.Count > 0 ? machineFilter.Entries[0].Value : string.Empty;
            DateTime? start = dtPickStart.SelectedDate;
            DateTime? end = dtPickEnd.SelectedDate;

            List<JobProductionWaste> dataSource = GetDataSource(assetId, start, end);

            if (!string.IsNullOrEmpty(assetId))
            {
                WasteChart.Visible = true;
                AssetId = assetId;
                if (start.HasValue)
                {
                    DateStart = start.Value;
                    DateEnd = end.Value;
                }
                else
                {
                    DateStart = null;
                    DateEnd = null;
                }
            }

            if (dataSource.Count > 0)
            {
                gridJob.Visible = true;
                WasteChart.Visible = true;
                BindJobTable(dataSource);
            }
            else
            {
                gridJob.Visible = true;
                gridJob.DataSource = new List<Job>();
                gridJob.DataBind();

                WasteChart.Visible = true;
                DataTable table = new DataTable();
                table.Columns.Add(new DataColumn("label"));
                table.Columns.Add(new DataColumn("data", typeof(decimal)));
                table.Columns.Add(new DataColumn("pctValue", typeof(decimal)));
                WasteChart.DataSource = table;
                WasteChart.DataBind();
            }
        }
        #endregion

        private void BindJobTable(List<JobProductionWaste> list)
        {
            List<Job> jobList = list.Select(x => x.Job).Distinct().ToList();

            TransactionRepository rep = new TransactionRepository();
            foreach (Job j in jobList)
            {
                List<Transaction> tList = rep.ReadAll(x => x.MachineId == AssetId && x.JobId == j.Id).ToList();
                j.Order.CustomerOrder.QtyProduced = tList.Sum(x => x.PartialCounting);
                j.Order.CustomerOrder.QtyProductionWaste = list.Where(x => x.JobId == j.Id).Sum(x => x.QtyProductionWaste);
            }

            gridJob.DataSource = jobList;
            gridJob.DataBind();

            gridJob.MasterTableView.Items[0].Selected = true;
            CreateChart();
        }

        protected void CreateChart()
        {
            GridDataItem row = (GridDataItem)gridJob.SelectedItems[0];//get selected row
            string jobId = row.GetDataKeyValue("Id").ToString();

            JobProductionWasteRepository rep = new JobProductionWasteRepository();
            IQueryable<JobProductionWaste> returnList = null;
            List<JobProductionWaste> list = null;

            //List<JobProductionWaste> dataSource = GetDataSource(AssetId, DateStart, DateEnd);

            returnList = rep.ReadAll(x => x.AssetId == AssetId && x.JobId == jobId);
            if (DateStart.HasValue)
            {
                if (DateEnd.HasValue)
                {
                    list = returnList.Where(x => x.Date >= DateStart && x.Date <= DateEnd).ToList();
                }
                else
                {
                    list = returnList.Where(x => x.Date >= DateStart).ToList();
                }
            }
            else
            {
                list = returnList.ToList();
            }

            decimal totWaste = list.Sum(x => x.QtyProductionWaste);
            //lblNumWaste.Text = string.Format("Totale scarti: {0:n0}", totWaste);
            //lblPctWaste.Text = string.Format("Percentuale scarti: {0} %", Math.Round(totWaste / tot * 100, 2).ToString());

            Dictionary<string, decimal> dataSource = new Dictionary<string, decimal>();
            foreach (var item in list)
            {
                if (!dataSource.ContainsKey(string.Format("{1} - {0}", item.Cause.Description, item.Cause.ExternalCode)))
                {
                    dataSource.Add(string.Format("{1} - {0}", item.Cause.Description, item.Cause.ExternalCode), 0);
                }
                dataSource[string.Format("{1} - {0}", item.Cause.Description, item.Cause.ExternalCode)] += item.QtyProductionWaste;
            }

            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("label"));
            table.Columns.Add(new DataColumn("data", typeof(decimal)));
            table.Columns.Add(new DataColumn("pctValue", typeof(decimal)));
            foreach (var item in dataSource.OrderBy(x => x.Key.Split('-')[0].Trim().Length).ThenBy(x => x.Key.Split('-')[0].Trim()))
            {
                table.Rows.Add(new object[] { item.Key, item.Value, Math.Round(item.Value / totWaste * 100, 2) });
            }

            WasteChart.DataSource = table;
            WasteChart.DataBind();
        }

        protected void gridJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreateChart();
        }

        private List<JobProductionWaste> GetDataSource(string assetId, DateTime? start, DateTime? end)
        {
            JobProductionWasteRepository rep = new JobProductionWasteRepository();
            List<JobProductionWaste> dataSource = new List<JobProductionWaste>();
            IQueryable<JobProductionWaste> returnList = null;

            if (!string.IsNullOrEmpty(assetId))
            {
                returnList = rep.ReadAll(x => x.AssetId == assetId);
            }

            if (returnList != null && returnList.Count() > 0)
            {
                if (start.HasValue)
                {
                    if (end.HasValue)
                    {
                        dataSource = returnList.Where(x => x.Date >= start && x.Date <= end).ToList();
                    }
                    else
                    {
                        dataSource = returnList.Where(x => x.Date >= start).ToList();
                    }
                }
                else
                {
                    dataSource = returnList.ToList();
                }
            }
            //else
            //{
            //    if (start.HasValue)
            //    {
            //        if (end.HasValue)
            //        {
            //            dataSource = rep.ReadAll(x => x.Date >= start && x.Date <= end).ToList();
            //        }
            //        else
            //        {
            //            dataSource = rep.ReadAll(x => x.Date >= start).ToList();
            //        }
            //    }
            //}

            return dataSource;
        }

        protected void gridJob_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (gridJob.Visible)
            {
                List<JobProductionWaste> list = GetDataSource(AssetId, DateStart, DateEnd);
                IEnumerable<Job> jobList = list.Select(x => x.Job).Distinct();

                TransactionRepository rep = new TransactionRepository();
                foreach (Job j in jobList)
                {
                    List<Transaction> tList = rep.ReadAll(x => x.MachineId == AssetId && x.JobId == j.Id).ToList();
                    j.Order.CustomerOrder.QtyProduced = tList.Sum(x => x.PartialCounting);
                    j.Order.CustomerOrder.QtyProductionWaste = list.Where(x => x.JobId == j.Id).Sum(x => x.QtyProductionWaste);
                }

                gridJob.DataSource = jobList;
            }
        }
    }
}