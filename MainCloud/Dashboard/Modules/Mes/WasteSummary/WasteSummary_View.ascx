﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WasteSummary_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.WasteSummary.WasteSummary_View" %>

<div class="panel panel-info">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="machineFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px"
                    TextSettings-SelectionMode="Single" DataSourceID="edsMachine" DataTextField="Code" DataValueField="Id" InputType="Text" Filter="Contains" Label="Macchina: ">
                </telerik:RadAutoCompleteBox>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <div style="text-align:right;">
            <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
        </div>
    </div>
</div>

<%--<div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <asp:Label runat="server" ID="lblNumWaste" Font-Bold="true" /> &nbsp;&nbsp;&nbsp;
            <asp:Label runat="server" ID="lblPctWaste" Font-Bold="true" />
        </div>
    </div>
</div>--%>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <telerik:RadGrid RenderMode="Lightweight" ID="gridJob" runat="server" AutoGenerateColumns="False" AllowPaging="true"
            OnSelectedIndexChanged="gridJob_SelectedIndexChanged" OnNeedDataSource="gridJob_NeedDataSource" GridLines="None"
            AllowMultiRowSelection="false" Visible="false">
            <MasterTableView DataKeyNames="Id" Caption="Commesse" NoMasterRecordsText="Non ci sono commesse lavorate nel periodo" FilterExpression="">
                <Columns>
                    <telerik:GridBoundColumn DataField="Code" HeaderText="Fase" Display="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>" Display="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Order.CustomerOrder.OrderCode" HeaderText="<%$ Resources:Mes,Order %>" EmptyDataText="&amp;nbsp;">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Order.Article.Code" HeaderText="<%$ Resources:Mes,Article %>" EmptyDataText="&amp;nbsp;">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Order.CustomerOrder.QtyProduced" HeaderText="Qta prodotta"
                        EmptyDataText="&amp;nbsp;" DataFormatString="{0:n0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Order.CustomerOrder.QtyProductionWaste" HeaderText="Scarti"
                        EmptyDataText="&amp;nbsp;" DataFormatString="{0:n0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
                <%--<RowIndicatorColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>--%>
            </MasterTableView>
            <FilterMenu EnableTheming="True">
                <CollapseAnimation Duration="200" Type="OutQuint"></CollapseAnimation>
            </FilterMenu>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Selecting AllowRowSelect="true"></Selecting>
            </ClientSettings>
            <PagerStyle Mode="NumericPages"></PagerStyle>
        </telerik:RadGrid>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <telerik:RadHtmlChart runat="server" ID="WasteChart" Visible="false">
            <PlotArea>
                <Series>
                    <telerik:BarSeries DataFieldY="data">
                        <TooltipsAppearance Color="White">
                        </TooltipsAppearance>
                        <LabelsAppearance>
                            <ClientTemplate>
                                #= dataItem.data # - #= dataItem.pctValue #%
                            </ClientTemplate>
                        </LabelsAppearance>
                    </telerik:BarSeries>
                </Series>
                <XAxis DataLabelsField="label">
                    <MinorGridLines Visible="false"></MinorGridLines>
                    <MajorGridLines Visible="false"></MajorGridLines>
                </XAxis>
                <YAxis>
                    <%--<LabelsAppearance DataFormatString="{0}\'"></LabelsAppearance>--%>
                    <MinorGridLines Visible="false"></MinorGridLines>
                </YAxis>
            </PlotArea>
            <Legend>
                <Appearance Visible="false"></Appearance>
            </Legend>
            <ChartTitle Text="Dettaglio Scarti"></ChartTitle>
        </telerik:RadHtmlChart>
    </div>
    </ContentTemplate>
</asp:UpdatePanel>

<ef:EntityDataSource ID="edsMachine" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    AutoGenerateWhereClause="true" OrderBy="it.Description">
</ef:EntityDataSource>