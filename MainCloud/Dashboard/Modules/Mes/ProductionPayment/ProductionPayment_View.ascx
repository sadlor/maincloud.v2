﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductionPayment_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.ProductionPayment.ProductionPayment_View" %>

<style type="text/css">
    .btnTable{
        width: 120px;
        text-align: center;
    }
</style>

<script type="text/javascript">
    function ScrollToElement() {
        setTimeout(document.getElementById('<%= gridTransaction.ClientID %>').scrollIntoView(), 1);
    }

    function BatchEditCellValueChanged(sender, args) {
        var batchEditingManager = sender.get_batchEditingManager();
        batchEditingManager.saveAllChanges();
    }
</script>

<asp:UpdatePanel runat="server">
    <ContentTemplate>

        <telerik:RadGrid runat="server" ID="gridMachine" RenderMode="Lightweight" AutoGenerateColumns="false"
            OnNeedDataSource="gridMachine_NeedDataSource" OnItemCommand="gridMachine_ItemCommand">
            <MasterTableView DataKeyNames="MachineId" FilterExpression="" Caption="Macchine con versamenti">
                <Columns>
                    <telerik:GridDropDownColumn DataField="MachineId" HeaderText="Macchina"
                        ListValueField="Id" ListTextField="Description" DataSourceID="edsMachine">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDropDownColumn>
                    <telerik:GridBoundColumn DataField="QtyTot" HeaderText="Quantità prodotta" DataFormatString="{0:f0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn DataTextField="QtyToVerify" DataTextFormatString="{0:f0}" HeaderText="Quantità da verificare" CommandName="Modify" ButtonCssClass="btn btnTable">
                       <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridButtonColumn>
                    <telerik:GridBoundColumn DataField="QtyExport" HeaderText="Quantità da esportare" DataFormatString="{0:f0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridBoundColumn DataField="Qty" HeaderText="Quantità da verificare" DataFormatString="{0:n0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>--%>
                    <telerik:GridButtonColumn Text="Vedi dettaglio" CommandName="SelectMachine" ButtonCssClass="btn btnTable">
                        <ItemStyle HorizontalAlign="Center" />
                    </telerik:GridButtonColumn>
                    <telerik:GridButtonColumn Text="Esporta tutto" CommandName="ExportMachine" ButtonCssClass="btn btnTable">
                        <ItemStyle HorizontalAlign="Center" />
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>

        <asp:Panel runat="server" ID="pnlTransactions" Visible="false">
            <telerik:RadGrid runat="server" ID="gridTransaction" RenderMode="Lightweight" AutoGenerateColumns="false" ShowFooter="True"
                OnItemCommand="gridTransaction_ItemCommand" AllowMultiRowSelection="true" GroupingEnabled="true">
                <MasterTableView DataKeyNames="MachineId, OperatorId, JobId, Start" ShowGroupFooter="true" FilterExpression="" Caption="Dettaglio transazioni" NoMasterRecordsText="Non ci sono transazioni con commessa e/o produzione">
                    <Columns>
                        <%--<telerik:GridButtonColumn Text="Modifica" CommandName="Modify">
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridButtonColumn>--%>
                        <telerik:GridDropDownColumn DataField="MachineId" HeaderText="Macchina"
                            ListValueField="Id" ListTextField="Description" DataSourceID="edsMachine">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridDropDownColumn>
                        <%--<telerik:GridBoundColumn DataField="Status" HeaderText="Stato">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="Operatore">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <%--<telerik:GridBoundColumn DataField="Cause.Description" HeaderText="Causale">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridBoundColumn DataField="Job.Order.CustomerOrder.OrderCode" HeaderText="Commessa">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Job.Order.Article.Code" HeaderText="Articolo">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn DataField="Start" HeaderText="Start" PickerType="DateTimePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn DataField="End" HeaderText="End" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn DataField="Duration" HeaderText="Tempo produzione">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" />
                        </telerik:GridBoundColumn>
                        <%--<telerik:GridBoundColumn DataField="PartialCounting" HeaderText="Colpi" DataFormatString="{0:f0}" Aggregate="Sum" FooterText="Tot:">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign="Right" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CavityMoldNum" HeaderText="Impronte" DataFormatString="{0:f0}">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" />
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridBoundColumn DataField="QtyProduced" HeaderText="Pezzi" DataFormatString="{0:f0}" Aggregate="Sum" FooterText="Tot:">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign="Right" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="QtyProductionWaste" HeaderText="Scarti" DataFormatString="{0:f0}" Aggregate="Sum" FooterText="Tot:">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign="Right" />
                        </telerik:GridBoundColumn>
                        <telerik:GridClientSelectColumn>
                        </telerik:GridClientSelectColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
            </telerik:RadGrid>

            <div style="float:right;">
                <telerik:RadButton runat="server" ID="btnExport" CssClass="btn" style="background-color:#5cb85c!important;border-color:#4cae4c!important;color:#fff!important;" OnClick="btnExport_Click">
                    <ContentTemplate>
                        <center><i class="fa fa-sign-out" aria-hidden="true"></i> Esporta</center>
                    </ContentTemplate>
                </telerik:RadButton>
                <telerik:RadButton runat="server" ID="btnCancel" CssClass="btn" style="background-color:#d9534f!important;border-color:#d9534f!important;color:#fff!important;" OnClick="btnCancel_Click">
                    <ContentTemplate>
                        <center><i class="fa fa-times" aria-hidden="true"></i> Annulla</center>
                    </ContentTemplate>
                </telerik:RadButton>
            </div>
        </asp:Panel>

        <br /><br />

        <div class="panel panel-info">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="machineFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px"
                            TextSettings-SelectionMode="Single" DataSourceID="edsMachine" DataTextField="Description" DataValueField="Id" InputType="Text" Filter="Contains" Label="Macchina: ">
                        </telerik:RadAutoCompleteBox>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>
                </div>
                <%--<br />--%>
                <div style="text-align:right;">
                    <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
                </div>
            </div>
        </div>
        <telerik:RadGrid runat="server" ID="gridExport" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="false">
            <MasterTableView Caption="Esportazioni" FilterExpression="" NoMasterRecordsText="Non ci sono esportazioni">
                <Columns>
                    <telerik:GridBoundColumn DataField="MachineCode" HeaderText="Macchina">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OrderCode" HeaderText="Commessa">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="QtyProduced" HeaderText="Qta Prodotta" DataFormatString="{0:f0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="QtyProductionWaste" HeaderText="Qta Scarti" DataFormatString="{0:f0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn DataField="Start" HeaderText="Data Inizio Registrazione" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn DataField="Duration" HeaderText="Tempo Macchina [m]">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn DataField="Sent" HeaderText="Data Invio" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn DataField="IsOrderClose" HeaderText="Commessa chiusa">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>

    </ContentTemplate>
</asp:UpdatePanel>

<mcf:PopUpDialog ID="dlgOrderClose" runat="server" Title="Dichiarazione chiusura commesse">
    <Body>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:HiddenField runat="server" ID="hiddenMachineId" />
                        <telerik:RadGrid runat="server" ID="gridJobToExport" RenderMode="Lightweight" AutoGenerateColumns="false" AllowMultiRowEdit="true"
                            OnNeedDataSource="gridJobToExport_NeedDataSource">
                            <MasterTableView Caption="Commesse da esportare" DataKeyNames="JobId,IsOrderClose" FilterExpression="" EditMode="Batch" NoMasterRecordsText="Non è stato selezionato niente">
                                <BatchEditingSettings EditType="Cell" />
                                <Columns>
                                    <telerik:GridBoundColumn DataField="OrderCode" HeaderText="Commessa" ReadOnly="true">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn DataField="IsOrderClose" HeaderText="Commessa chiusa">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridCheckBoxColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                               <ClientEvents OnBatchEditCellValueChanged="BatchEditCellValueChanged"/>
                        </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div style="float:right;">
                            <telerik:RadButton runat="server" ID="btnConfirmExport" CssClass="btn" style="background-color:#5cb85c!important;border-color:#4cae4c!important;color:#fff!important;" OnClick="btnConfirmExport_Click">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton runat="server" ID="btnCancelExport" CssClass="btn" style="background-color:#d9534f!important;border-color:#d9534f!important;color:#fff!important;" OnClick="btnCancelExport_Click">
                                <ContentTemplate>
                                    <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Body>
</mcf:PopUpDialog>

<ef:EntityDataSource ID="edsMachine" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    AutoGenerateWhereClause="true" OrderBy="it.Description" />