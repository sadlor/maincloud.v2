﻿using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Mes.ProductionPayment
{
    public partial class ProductionPayment_View : DataWidget<ProductionPayment_View>
    {
        public ProductionPayment_View() : base(typeof(ProductionPayment_View)) {}

        ProductionPaymentService ppService = new ProductionPaymentService();

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMachine.WhereParameters.Clear();
            edsMachine.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);

            if (!IsPostBack)
            {
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }
        }

        protected void gridMachine_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var dataSource = ppService.MachineListWithProduction();
            gridMachine.DataSource = dataSource;
        }

        protected void gridMachine_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            string assetId = (e.Item as GridDataItem).GetDataKeyValue("MachineId").ToString();
            switch (e.CommandName)
            {
                case "Modify":
                    string destinationAreaId = CustomSettings.Find("settings-destination-area-id") as string;
                    if (!string.IsNullOrEmpty(destinationAreaId))
                    {
                        WidgetAreaRepository areaService = new WidgetAreaRepository();
                        WidgetArea wa = areaService.FindByID(destinationAreaId);

                        SessionMultitenant[MesConstants.ASSET_TO_MODIFY_TRANSACTIONS] = assetId;
                        Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
                    }
                    break;
                case "SelectMachine":
                    pnlTransactions.Visible = true;
                    //gridTransaction.DataSource = ppService.TransactionListPerMachine(assetId);
                    gridTransaction.DataSource = ppService.CreateViewToExport(assetId);
                    gridTransaction.DataBind();
                    break;
                case "ExportMachine":
                    hiddenMachineId.Value = assetId;
                    dlgOrderClose.OpenDialog();
                    gridJobToExport.Rebind();
                    break;
            }
        }

        protected void gridTransaction_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Modify")
            {
                int transId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));
                string destinationAreaId = CustomSettings.Find("settings-destination-area-id") as string;
                if (!string.IsNullOrEmpty(destinationAreaId))
                {
                    WidgetAreaRepository areaService = new WidgetAreaRepository();
                    WidgetArea wa = areaService.FindByID(destinationAreaId);

                    SessionMultitenant[MesConstants.TRANSACTION_ID_TO_MODIFY] = transId;
                    Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
                }
            }
        }

        protected void Export(string assetId = null)//(string methodExport, string assetId = null)
        {
            //switch (methodExport)
            //{
            //    case "NoExport":
            //    case "Aggregate":
            //        if (string.IsNullOrEmpty(assetId))
            //        {
            //            if (gridTransaction.SelectedItems.Count > 0)
            //            {
            //                List<Transaction> tList = new List<Transaction>();
            //                TransactionRepository transRepos = new TransactionRepository();
            //                foreach (GridDataItem item in gridTransaction.SelectedItems)
            //                {
            //                    tList.Add(transRepos.FindByID(Convert.ToInt32(item.GetDataKeyValue("Id"))));
            //                }
            //                if (methodExport == "NoExport")
            //                {
            //                    ppService.CreateProductionPaymentPerDay(tList);
            //                }
            //                else
            //                {
            //                    ppService.CreateProductionPaymentPerDay(tList, true);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            List<Transaction> tList = ppService.TransactionListPerMachine(assetId);
            //            if (methodExport == "NoExport")
            //            {
            //                ppService.CreateProductionPaymentPerDay(tList);
            //            }
            //            else
            //            {
            //                ppService.CreateProductionPaymentPerDay(tList, true);
            //            }
            //        }
            //        break;
            //    case "Single":
            //        if (string.IsNullOrEmpty(assetId))
            //        {
            //            if (gridTransaction.SelectedItems.Count > 0)
            //            {
            //                List<Transaction> tList = new List<Transaction>();
            //                TransactionRepository transRepos = new TransactionRepository();
            //                foreach (GridDataItem item in gridTransaction.SelectedItems)
            //                {
            //                    tList.Add(transRepos.FindByID(Convert.ToInt32(item.GetDataKeyValue("Id"))));
            //                }
            //                ppService.CreateProductionPayment(tList, true);
            //            }
            //        }
            //        else
            //        {
            //            List<Transaction> tList = ppService.TransactionListPerMachine(assetId);
            //            ppService.CreateProductionPayment(tList, true);
            //        }
            //        break;
            //}

            if (string.IsNullOrEmpty(assetId))
            {
                if (gridTransaction.SelectedItems.Count > 0)
                {
                    List<Transaction> tList = new List<Transaction>();
                    TransactionRepository transRepos = new TransactionRepository();
                    foreach (GridDataItem item in gridTransaction.SelectedItems)
                    {
                        //tList.Add(transRepos.FindByID(Convert.ToInt32(item.GetDataKeyValue("Id"))));
                        string jobId = item.GetDataKeyValue("JobId").ToString();
                        string operatorId = item.GetDataKeyValue("OperatorId") == null ? null : item.GetDataKeyValue("OperatorId").ToString();
                        string machineId = item.GetDataKeyValue("MachineId").ToString();
                        DateTime start = Convert.ToDateTime(item.GetDataKeyValue("Start"));
                        List<Transaction> l = transRepos.ReadAll(x => x.MachineId == machineId && (x.PartialCounting > 0 || x.QtyProductionWaste > 0) && !x.Exported && x.Verified && !string.IsNullOrEmpty(x.JobId) && !x.Open).ToList();
                        tList.AddRange(l.Where(x => x.JobId == jobId && x.OperatorId == operatorId && x.Start.Date == start.Date));
                    }
                    ppService.CreateProductionPaymentPerDay(tList, true);
                }
            }
            else
            {
                List<Transaction> tList = ppService.TransactionListPerMachine(assetId);
                ppService.CreateProductionPaymentPerDay(tList, true);
            }

            gridMachine.Rebind();
            pnlTransactions.Visible = false;
            hiddenMachineId.Value = null;

            ProductionPaymentRepository repos = new ProductionPaymentRepository();
            OperatorRepository opRepos = new OperatorRepository();
            List<MES.Models.ProductionPayment> list = ppService.GetAllVerifiedNotSent();
            //List<MES.Models.ProductionPayment> exportList = ppService.AggregateToExport();

            string connectionString = ConfigurationManager.ConnectionStrings["FluentisConnection"].ConnectionString;
            //string tableName = connectionString.Split(';').Where(x => x.StartsWith("initial catalog", StringComparison.OrdinalIgnoreCase)).First().Split('=')[1];
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlTransaction trans = conn.BeginTransaction();
                string sql = "INSERT INTO dbo.OP_SAT_MES_Segnalazioni (OPSMS_OPFS_Id, OPSMS_CICL_Cdl, OPSMS_QtaPezziConformi, OPSMS_QtaPezziScarti, OPSMS_OrdineChiuso, OPSMS_DataRegistrazione, OPSMS_TempoMacchina, OPSMS_TempoUomo, OPSMS_CIMC_Macchina, OPSMS_Operatore)" +
                    "VALUES (@param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10)";//, getdate())";

                SqlCommand cmd = new SqlCommand(sql, conn, trans);

                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                cmd.Parameters.AddWithValue("@param1", DbType.Int32);
                cmd.Parameters.AddWithValue("@param2", DbType.String);
                cmd.Parameters.AddWithValue("@param3", DbType.Decimal);
                cmd.Parameters.AddWithValue("@param4", DbType.Decimal);
                cmd.Parameters.AddWithValue("@param5", DbType.Boolean);
                cmd.Parameters.AddWithValue("@param6", DbType.DateTime);
                cmd.Parameters.AddWithValue("@param7", DbType.Int32);
                cmd.Parameters.AddWithValue("@param8", DbType.Int32);
                cmd.Parameters.AddWithValue("@param9", DbType.String);
                cmd.Parameters.AddWithValue("@param10", DbType.String);

                try
                {
                    foreach (var item in list)//exportList)
                    {
                        cmd.Parameters[0].Value = item.JobId;
                        cmd.Parameters[1].Value = item.MachineCode;
                        cmd.Parameters[2].Value = item.QtyOK; //item.QtyProduced;
                        cmd.Parameters[3].Value = item.QtyProductionWaste;
                        cmd.Parameters[4].Value = item.IsOrderClose;
                        cmd.Parameters[5].Value = item.Start;
                        cmd.Parameters[6].Value = (int)item.Duration;
                        cmd.Parameters[7].Value = (int)item.Duration;
                        cmd.Parameters[8].Value = item.MachineCode;
                        var op = opRepos.GetBadge(item.OperatorId);
                        if (string.IsNullOrEmpty(op))
                        {
                            op = string.Empty;
                        }
                        cmd.Parameters[9].Value = op;

                        cmd.ExecuteNonQuery();
                    }
                    trans.Commit();

                    foreach (var item in list)
                    {
                        item.Verified = true;
                        item.Sent = DateTime.Now;
                        repos.Update(item);
                    }
                    repos.SaveChanges();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                }
                finally
                {
                    //foreach (var item in list)
                    //{
                    //    item.Verified = true;
                    //    item.Sent = DateTime.Now;
                    //    repos.Update(item);
                    //}
                    //repos.SaveChanges();
                    conn.Close();
                }
            }



            // Create the thread object, passing in the Alpha.Beta method
            // via a ThreadStart delegate. This does not start the thread.
            //Thread oThread = new Thread(new ThreadStart(Export));

            // Start the thread
            //oThread.Start();

            // Spin for a while waiting for the started thread to become
            // alive:
            //while (!oThread.IsAlive) ;

            // Put the Main thread to sleep for 1 millisecond to allow oThread
            // to do some work:
            //Thread.Sleep(1);

            // Request that oThread be stopped
            //oThread.Abort();

            // Wait until oThread finishes. Join also has overloads
            // that take a millisecond interval or a TimeSpan object.
            //oThread.Join();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            dlgOrderClose.OpenDialog();
            gridJobToExport.Rebind();
        }

        protected void btnCancelExport_Click(object sender, EventArgs e)
        {
            dlgOrderClose.CloseDialog();
        }

        protected void btnConfirmExport_Click(object sender, EventArgs e)
        {
            gridJobToExport.Rebind(); //se premono subito update le modifiche non vengono salvate, quindi va ricaricata
            foreach (GridDataItem item in gridJobToExport.Items)
            {
                string jobId = item.GetDataKeyValue("JobId").ToString();
                bool isOrderClose = (bool)item.GetDataKeyValue("IsOrderClose");
                if (isOrderClose)
                {
                    //chiusura commessa
                    ppService.CloseOrder(jobId);
                }
            }

            dlgOrderClose.CloseDialog();
            //Export(chkSelectExportMethod.SelectedValue, hiddenMachineId.Value);
            Export(hiddenMachineId.Value);

            gridExport.Rebind();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlTransactions.Visible = false;
        }

        protected void gridExport_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ProductionPaymentRepository repos = new ProductionPaymentRepository();
            gridExport.DataSource = repos.ReadAll().ToList();
        }

        protected void gridJobToExport_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (dlgOrderClose.IsOpen())
            {
                List<MES.Models.ProductionPayment> dataSource = new List<MES.Models.ProductionPayment>();
                List<MES.Models.ProductionPayment> pList = new List<MES.Models.ProductionPayment>();
                TransactionRepository transRepos = new TransactionRepository();
                JobRepository jobRepos = new JobRepository();
                if (!string.IsNullOrEmpty(hiddenMachineId.Value))
                {
                    //tList = ppService.TransactionListPerMachine(hiddenMachineId.Value);
                    pList = ppService.CreateViewToExport(hiddenMachineId.Value);
                    foreach (Job job in pList.Select(x => x.Job).Distinct())
                    {
                        if (job != null)
                        {
                            var pp = new MES.Models.ProductionPayment();
                            pp.JobId = job.Id;
                            pp.OrderCode = job.Order.CustomerOrder.OrderCode;
                            dataSource.Add(pp);
                        }
                    }
                }
                else
                {
                    foreach (GridDataItem item in gridTransaction.SelectedItems)
                    {
                        //pList.Add(transRepos.FindByID(Convert.ToInt32(item.GetDataKeyValue("Id"))));
                        string jobId = item.GetDataKeyValue("JobId").ToString();
                        if (!string.IsNullOrEmpty(jobId) && !dataSource.Any(x => x.JobId == jobId))
                        {
                            var pp = new MES.Models.ProductionPayment();
                            pp.JobId = jobId;
                            pp.OrderCode = jobRepos.ReadAll(x => x.Id == jobId).First().Order.CustomerOrder.OrderCode;
                            dataSource.Add(pp);
                        }
                    }
                }
                //foreach (Job job in tList.Select(x => x.Job).Distinct())
                //{
                //    if (job != null)
                //    {
                //        var pp = new MES.Models.ProductionPayment();
                //        pp.JobId = job.Id;
                //        pp.OrderCode = job.Order.CustomerOrder.OrderCode;
                //        dataSource.Add(pp);
                //    }
                //}
                foreach (GridDataItem item in gridJobToExport.Items)
                {
                    if (dataSource.Where(x => x.JobId == item.GetDataKeyValue("JobId").ToString()).Any())
                    {
                        dataSource.Where(x => x.JobId == item.GetDataKeyValue("JobId").ToString()).First().IsOrderClose = (item.Controls[3].Controls[0] as CheckBox).Checked;
                    }
                }
                gridJobToExport.DataSource = dataSource;
            }
            else
            {
                gridJobToExport.DataSource = null;
            }
        }

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            gridExport.Visible = true;

            string assetId = machineFilter.Entries.Count > 0 ? machineFilter.Entries[0].Value : string.Empty;
            DateTime? start = dtPickStart.SelectedDate;
            DateTime? end = dtPickEnd.SelectedDate;
            if (end.HasValue)
            {
                end = end.Value.AddDays(1);
            }

            ProductionPaymentRepository repos = new ProductionPaymentRepository();
            gridExport.DataSource = repos.ReadAll(x => x.MachineId == assetId && x.Sent >= start && x.Sent <= end).ToList();
            gridExport.DataBind();
        }
    }
}