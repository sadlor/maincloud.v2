﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Mes.ArticleAnalysis
{
    public partial class ArticleAnalysis_View : DataWidget<ArticleAnalysis_View>
    {
        public ArticleAnalysis_View() : base(typeof(ArticleAnalysis_View)) {}

        private class TableRecord
        {
            public string Code { get; set; }
            public string CustomerOrder { get; set; }
            public string MachineOnTime { get; set; }
            public string Production { get; set; }
            public decimal OreProdTeorico { get; set; }
            public string Stop { get; set; }
            public decimal Index { get; set; }
            public decimal QtyShotProd { get; set; }
            public decimal QtyProd { get; set; }
            public decimal Cadency { get; set; }
            public decimal Waste { get; set; }
            public decimal StampateOraTeorico { get; set; }
            public decimal Availability { get; set; }
            public decimal Efficiency { get; set; }
            public decimal Quality { get; set; }
            public decimal OEE { get; set; }
            public decimal UnitTime { get; set; }

            public TableRecord(string code, string customerOrder, string machineOnTime, string productionTime, string stopTime, decimal availability, decimal efficiency, decimal quality, decimal oreProdTeorico, decimal stampateOra, decimal cadency, decimal unitTime, decimal qtyShotProd, decimal qtyProd, decimal waste)
            {
                Code = code;
                CustomerOrder = customerOrder;
                MachineOnTime = machineOnTime;
                Production = productionTime;
                Stop = stopTime;
                Availability = availability;
                Efficiency = efficiency;
                Quality = quality;
                OreProdTeorico = oreProdTeorico;
                StampateOraTeorico = stampateOra;
                Cadency = cadency;
                UnitTime = unitTime;
                QtyShotProd = qtyShotProd;
                QtyProd = qtyProd;
                Waste = waste;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }

            edsArticle.WhereParameters.Clear();
            edsArticle.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            gridArticle.Visible = true;
            gridArticle.Rebind();
        }

        protected void gridArticle_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            IndexService indexService = new IndexService();
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();
            List<TableRecord> dataSource = new List<TableRecord>();

            string article = articleFilter.Entries.Count > 0 ? articleFilter.Entries[0].Value : string.Empty;
            if (!string.IsNullOrEmpty(article))
            {
                int articleId = Convert.ToInt32(article);
                TransactionRepository TRep = new TransactionRepository();
                List<Transaction> tList = new List<Transaction>();
                DateTime end = dtPickEnd.SelectedDate.Value.AddDays(1);
                tList = TRep.ReadAll(x => x.Job.Order.IdArticle == articleId && x.Start >= dtPickStart.SelectedDate && x.Start <= end).ToList();

                foreach (string assetId in tList.Select(x => x.MachineId).Distinct())
                {
                    AssetRepository assetRep = new AssetRepository();
                    Asset item = assetRep.FindByID(assetId);
                    List<Transaction> mtList = tList.Where(x => x.MachineId == assetId).ToList();

                    foreach (Job job in mtList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct())
                    {
                        if (job != null)
                        {
                            TimeSpan machineOn = TimeSpan.FromSeconds((double)mtList.Where(x => x.JobId == job.Id && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                            TimeSpan productionTime = TimeSpan.FromSeconds((double)mtList.Where(x => x.JobId == job.Id && x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                            TimeSpan stopTime = TimeSpan.FromSeconds((double)mtList.Where(x => x.JobId == job.Id && x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                            decimal oreProdTeorico = 0;
                            decimal qtyShotProd = 0;
                            decimal waste = 0;
                            decimal qtyProd = 0;
                            decimal cadency = 0;
                            decimal unitTime = 0;

                            if (productionTime.TotalSeconds > 0)
                            {
                                qtyShotProd = mtList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting);
                                cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                                if (qtyShotProd > 0)
                                {
                                    unitTime = Math.Round((decimal)productionTime.TotalHours / qtyShotProd, 2);
                                }
                                //foreach (Job job in mtList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct())
                                //{
                                //    if (job != null)
                                //    {
                                if (job.StandardRate > 0)
                                {
                                    oreProdTeorico += Math.Round(mtList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) / job.StandardRate, 2);
                                }
                                qtyProd += mtList.Where(x => x.JobId == job.Id).Sum(x => x.PartialCounting) * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1);

                                try
                                {
                                    waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == item.Id && x.JobId == job.Id && x.Date >= dtPickStart.SelectedDate && x.Date <= end).Sum(x => x.QtyProductionWaste), 0);
                                }
                                catch (InvalidOperationException) { }
                                //}
                                //}
                            }

                            dataSource.Add(new TableRecord(
                                string.Format("{0} - {1}", item.Code.Trim(), item.Description.Trim()),
                                job.Order.CustomerOrder.OrderCode,
                                string.Format("{0:n2}", machineOn.TotalHours),//string.Format("{0}:{1:d2}:{2:d2}", (int)machineOn.TotalHours, machineOn.Minutes, machineOn.Seconds),
                                string.Format("{0:n2}", productionTime.TotalHours),//string.Format("{0}:{1:d2}:{2:d2}", (int)productionTime.TotalHours, productionTime.Minutes, productionTime.Seconds),
                                string.Format("{0:n2}", stopTime.TotalHours),//string.Format("{0}:{1:d2}:{2:d2}", (int)stopTime.TotalHours, stopTime.Minutes, stopTime.Seconds),
                                indexService.Availability(productionTime, machineOn),
                                indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico),
                                indexService.Quality(qtyProd, waste),
                                oreProdTeorico,
                                job.StandardRate,
                                cadency,
                                unitTime,
                                qtyShotProd,
                                qtyProd,
                                waste));
                        }
                    }
                }

                TimeSpan totMachineOn = TimeSpan.FromHours(dataSource.Sum(x => Convert.ToDouble(x.MachineOnTime)));
                TimeSpan totProd = TimeSpan.FromHours(dataSource.Sum(x => Convert.ToDouble(x.Production)));
                TimeSpan totStop = TimeSpan.FromHours(dataSource.Sum(x => Convert.ToDouble(x.Stop)));
                decimal qtyTot = dataSource.Sum(x => x.QtyProd);
                decimal wasteTot = dataSource.Sum(x => x.Waste);
                decimal oreProdTeorTot = dataSource.Sum(x => x.OreProdTeorico);
                decimal cadencyTot = 0;
                decimal unitTimeTot = 0;
                if (totProd.TotalSeconds > 0)
                {
                    cadencyTot = Math.Round(dataSource.Sum(x => x.QtyShotProd) / (decimal)totProd.TotalHours, 2);
                    if (qtyTot > 0)
                    {
                        unitTimeTot = Math.Round((decimal)totProd.TotalHours / dataSource.Sum(x => x.QtyShotProd), 2);
                    }
                }

                decimal availabilityTot = indexService.Availability(totProd, totMachineOn);
                //decimal efficiencyTot = 0;
                //if (dataSource.Where(x => x.OreProdTeorico > 0).Any())
                //{
                //solo le ore di produzione delle macchine che avevano le stampateOra teoriche
                //TimeSpan totProdWithCadency = TimeSpan.FromHours(dataSource.Where(x => x.OreProdTeorico > 0).Sum(x => Convert.ToInt32(x.Production)));
                //efficiencyTot = indexService.Efficiency((decimal)totProdWithCadency.TotalHours, oreProdTeorTot);
                //}
                decimal efficiencyTot = indexService.Efficiency((decimal)totProd.TotalHours, oreProdTeorTot);
                decimal qualityTot = indexService.Quality(qtyTot, wasteTot);

                dataSource.Insert(0, new TableRecord(
                    "MEDIA TOTALE",
                    "",
                    string.Format("{0:n2}", totMachineOn.TotalHours),
                    string.Format("{0:n2}", totProd.TotalHours),
                    string.Format("{0:n2}", totStop.TotalHours),
                    availabilityTot,
                    efficiencyTot,
                    qualityTot,
                    oreProdTeorTot,
                    0,
                    cadencyTot,
                    unitTimeTot,
                    0,
                    qtyTot,
                    wasteTot));

                gridArticle.DataSource = dataSource;
            }
        }

        protected void gridArticle_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                if (item.GetDataKeyValue("Code").ToString() == "MEDIA TOTALE")
                {
                    item["Code"].Font.Bold = true;
                }
            }
        }

        protected void articleFilter_TextChanged(object sender, AutoCompleteTextEventArgs e)
        {
            string article = articleFilter.Entries.Count > 0 ? articleFilter.Entries[0].Value : string.Empty;
            if (!string.IsNullOrEmpty(article))
            {
                int articleId = Convert.ToInt32(article);
                ArticleRepository rep = new ArticleRepository();
                articleDescrFilter.Entries.Add(new AutoCompleteBoxEntry(rep.FindByID(articleId).Description, articleId.ToString()));
            }
        }

        protected void articleDescrFilter_TextChanged(object sender, AutoCompleteTextEventArgs e)
        {
            string article = articleDescrFilter.Entries.Count > 0 ? articleDescrFilter.Entries[0].Value : string.Empty;
            if (!string.IsNullOrEmpty(article))
            {
                int articleId = Convert.ToInt32(article);
                ArticleRepository rep = new ArticleRepository();
                articleFilter.Entries.Add(new AutoCompleteBoxEntry(rep.FindByID(articleId).Code, articleId.ToString()));
            }
        }
    }
}