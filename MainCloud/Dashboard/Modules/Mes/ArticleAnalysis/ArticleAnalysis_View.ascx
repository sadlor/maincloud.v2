﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArticleAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.ArticleAnalysis.ArticleAnalysis_View" %>

<script type="text/javascript">
    function dropDownClosed(sender, eventArgs) {
        setTimeout(function () {
            document.activeElement.blur();
        }, 15);
    }
</script>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="panel panel-info">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="articleFilter" EmptyMessage="Codice" EnableClientFiltering="true"
                            LabelWidth="70px" Width="263px" TextSettings-SelectionMode="Single" InputType="Text" Filter="Contains" Label="Articolo: "
                            DataSourceID="edsArticle" DataTextField="Code" DataValueField="Id" OnTextChanged="articleFilter_TextChanged" OnClientDropDownClosed="dropDownClosed">
                        </telerik:RadAutoCompleteBox>
                        <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="articleDescrFilter" EmptyMessage="Descrizione" EnableClientFiltering="true"
                            Width="370px" TextSettings-SelectionMode="Single" InputType="Text" Filter="Contains"
                            DataSourceID="edsArticle" DataTextField="Description" DataValueField="Id" OnTextChanged="articleDescrFilter_TextChanged" OnClientDropDownClosed="dropDownClosed">
                        </telerik:RadAutoCompleteBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>
                </div>
                <div style="text-align:right;">
                    <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
                </div>
            </div>
        </div>

        <telerik:RadGrid runat="server" ID="gridArticle" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="false"
            AllowFilteringByColumn="false" AllowSorting="true" AllowPaging="true"
            OnNeedDataSource="gridArticle_NeedDataSource" OnItemDataBound="gridArticle_ItemDataBound"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <%--<PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />--%>
            <MasterTableView Caption="" DataKeyNames="Code">
                <Columns>
                    <telerik:GridBoundColumn DataField="Code" HeaderText="Macchina" CurrentFilterFunction="Contains" AllowSorting="false" AllowFiltering="false"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CustomerOrder" HeaderText="Commessa" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MachineOnTime" HeaderText="H utilizzo" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Production" HeaderText="Prod" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Stop" HeaderText="Fermo" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Availability" HeaderText="Disp %" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Efficiency" HeaderText="Eff %" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Quality" HeaderText="Q %" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridCalculatedColumn DataFields="Availability,Efficiency,Quality" HeaderText="OEE %" Expression="(({0}/100) * ({1} / 100) * ({2} / 100)) * 100" 
                        AllowFiltering="false" AllowSorting="false" DataFormatString="{0:n2}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridCalculatedColumn>
                    <telerik:GridBoundColumn DataField="OreProdTeorico" HeaderText="Ore<br/>assegnate" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="StampateOraTeorico" HeaderText="Cadenza<br/>teorica" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Cadency" HeaderText="Cadenza<br/>effettiva" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="UnitTime" HeaderText="Tempo<br/>unitario" AllowFiltering="false" AllowSorting="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="QtyProd" HeaderText="Pezzi<br/>prodotti" AllowFiltering="false" AllowSorting="false" DataFormatString="{0:n0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Waste" HeaderText="Scarti" AllowFiltering="false" AllowSorting="false" DataFormatString="{0:n0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
    </ContentTemplate>
</asp:UpdatePanel>

<ef:EntityDataSource ID="edsArticle" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Articles"
    AutoGenerateWhereClause="true" OrderBy="it.Code">
</ef:EntityDataSource>