﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Configurator_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.Configurator.Configurator_View" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <telerik:RadComboBox runat="server" ID="ddlModuleSelector" Label="Seleziona modulo:" EmptyMessage="" OnSelectedIndexChanged="ddlModuleSelector_SelectedIndexChanged"
            AutoPostBack="true">
            <Items>
                <telerik:RadComboBoxItem Text="Asset" Value="Asset" />
                <telerik:RadComboBoxItem Text="MES" Value="MES" />
            </Items>
        </telerik:RadComboBox>

        <telerik:RadGrid runat="server" ID="gridConfigurator" RenderMode="Lightweight" AutoGenerateColumns="false"
            OnNeedDataSource="gridConfigurator_NeedDataSource" OnUpdateCommand="gridConfigurator_UpdateCommand">
            <MasterTableView NoMasterRecordsText="Non ci sono parametri per il modulo selezionato" Caption="Configurazione"
                EditMode="InPlace" FilterExpression="" DataKeyNames="Id">
                <Columns>
                    <telerik:GridEditCommandColumn />
                    <telerik:GridBoundColumn DataField="ParamContext" HeaderText="Parametro" ReadOnly="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ParamName" HeaderText="Parametro" ReadOnly="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Value" HeaderText="Valore">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Module" HeaderText="Modulo" ReadOnly="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </ContentTemplate>
</asp:UpdatePanel>