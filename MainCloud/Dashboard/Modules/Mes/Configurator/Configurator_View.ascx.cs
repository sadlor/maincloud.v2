﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Mes.Configurator
{
    public partial class Configurator_View : DataWidget<Configurator_View>
    {
        public Configurator_View() : base(typeof(Configurator_View)) {}

        ConfigurationService service = new ConfigurationService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                gridConfigurator.Visible = false;
            }
            //service.InsertDefault();
        }

        protected void ddlModuleSelector_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            gridConfigurator.Visible = true;
            switch(e.Value)
            {
                case "Asset":
                    gridConfigurator.DataSource = null;
                    gridConfigurator.DataBind();
                    break;
                case "MES":
                    gridConfigurator.DataSource = service.GetAllParameter();
                    gridConfigurator.DataBind();
                    break;
            }
        }

        protected void gridConfigurator_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            switch (ddlModuleSelector.SelectedValue)
            {
                case "Asset":
                    gridConfigurator.DataSource = null;
                    break;
                case "MES":
                    gridConfigurator.DataSource = service.GetAllParameter();
                    break;
            }
        }

        protected void gridConfigurator_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            var paramId = Convert.ToInt32(editableItem.GetDataKeyValue("Id"));

            ModuleConfiguration param = service.GetByID(paramId);
            if (param != null)
            {
                editableItem.UpdateValues(param);
                service.ModifyParameter(param);
            }
        }
    }
}