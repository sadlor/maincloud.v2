﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Mes.ProductionWeight.Models
{
    public class WeightViewModel
    {
        /// <summary>
        /// Commessa
        /// </summary>
        public string Order { get; set; }
        /// <summary>
        /// Peso teorico
        /// </summary>
        public decimal Theoretical { get; set; }
        /// <summary>
        /// Peso Prelevato
        /// </summary>
        public decimal Up { get; set; }
        /// <summary>
        /// Peso reso
        /// </summary>
        public decimal Down { get; set; }
        /// <summary>
        /// Peso calcolato
        /// </summary>
        public decimal Calculated { get; set; }
        /// <summary>
        /// PEso scarto
        /// </summary>
        public decimal Waste { get; set; }

        public WeightViewModel(string order, decimal theoretical, decimal up, decimal down, decimal calculated, decimal waste)
        {
            Order = order;
            Theoretical = theoretical;
            Up = up;
            Down = down;
            Calculated = calculated;
            Waste = waste;
        }
    }
}