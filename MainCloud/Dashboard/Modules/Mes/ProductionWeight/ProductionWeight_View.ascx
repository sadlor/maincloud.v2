﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductionWeight_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.ProductionWeight.ProductionWeight_View" %>

<div class="panel panel-info">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="width:87px!important;">
                <asp:Label Text="Macchina: " runat="server" AssociatedControlID="machineFilter" />
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="machineFilter" EmptyMessage="Seleziona" EnableClientFiltering="true"
                    TextSettings-SelectionMode="Single" DataSourceID="edsMachine" DataTextField="Code" DataValueField="Id" InputType="Text" Filter="Contains">
                </telerik:RadAutoCompleteBox>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="width:87px!important;">
                <asp:Label Text="Inizio: " runat="server" AssociatedControlID="dtPickStart" />
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="187px">
                    <DateInput runat="server" DisplayDateFormat="dd/MM/yyyy" />
                    <%--<ClientEvents OnDateSelected="OpenEndCalendar" />--%>
                </telerik:RadDatePicker>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="width:87px!important;">
                <asp:Label Text="Fine: " runat="server" AssociatedControlID="dtPickEnd" />
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="187px">
                    <DateInput runat="server" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <div style="text-align:right">
            <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
        </div>
    </div>
</div>

<telerik:RadGrid runat="server" ID="gridWeight" RenderMode="Lightweight" AutoGenerateColumns="false"
    OnNeedDataSource="gridWeight_NeedDataSource">
    <MasterTableView Caption="Pesi" FilterExpression="">
        <Columns>
            <telerik:GridBoundColumn DataField="Order">
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Theoretical" HeaderText="Teorico" DataFormatString="{0:f0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Up" HeaderText="Prelevato<br/>da lotto" DataFormatString="{0:f0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Down" HeaderText="Reso<br/>pesato" DataFormatString="{0:f0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Calculated" HeaderText="Utilizzato<br/>calcolato" DataFormatString="{0:f0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Waste" HeaderText="Scarto<br/>differenza" DataFormatString="{0:f0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridCalculatedColumn DataFields="Waste, Calculated" HeaderText="Percentuale<br/>su utilizzato" Expression="iif(({1} > 0), ({0}/{1}), 0)" DataFormatString="{0:f2}" DataType="System.Decimal">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridCalculatedColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<ef:EntityDataSource ID="edsMachine" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    AutoGenerateWhereClause="true" OrderBy="it.Code">
</ef:EntityDataSource>