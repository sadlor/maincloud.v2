﻿using MainCloud.Dashboard.Modules.Mes.ProductionWeight.Models;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.Mes.ProductionWeight
{
    public partial class ProductionWeight_View : DataWidget<ProductionWeight_View>
    {
        public ProductionWeight_View() : base(typeof(ProductionWeight_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }
        }

        private void CreateTable(string assetId, DateTime start, DateTime end)
        {
            List<WeightViewModel> dataSource = new List<WeightViewModel>();

            JobBatchRepository batchRepo = new JobBatchRepository();
            JobRepository jobRepo = new JobRepository();
            TransactionRepository tRepos = new TransactionRepository();
            var tList = tRepos.ReadAll(x => x.MachineId == assetId && x.Start >= start && x.Start <= end).ToList();
            foreach (var jobId in tList.Select(x => x.JobId).Distinct())
            {
                decimal calculatedWeight = tList.Where(x => x.JobId == jobId).Sum(x => x.PartialCounting * x.CavityMoldNum);
                List<JobBatch> batchList = batchRepo.ReadAll(x => x.MachineId == assetId && x.JobId == jobId && x.Start >= start && x.Start <= end).ToList();
                decimal upWeight = batchList.Where(x => x.Quantity > 0).Sum(x => x.Quantity);
                decimal downWeight = batchList.Where(x => x.Quantity < 0).Sum(x => x.Quantity);
                JobService jobService = new JobService();
                decimal theoreticalWeight = 0;
                try
                {
                    theoreticalWeight = jobService.GetBOM(jobId).FirstOrDefault().Value.Sum(x => x.Quantity);
                }
                catch (Exception) {}
                decimal waste = upWeight - calculatedWeight + downWeight;
                dataSource.Add(new WeightViewModel(jobRepo.GetCustomerOrderCode(jobId), theoreticalWeight, upWeight, downWeight, calculatedWeight, waste));
            }
            gridWeight.DataSource = dataSource;
        }

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            gridWeight.Rebind();
        }

        protected void gridWeight_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            string assetId = machineFilter.Entries.Count > 0 ? machineFilter.Entries[0].Value : string.Empty;
            DateTime? start = dtPickStart.SelectedDate;
            DateTime? end = dtPickEnd.SelectedDate;
            if (!string.IsNullOrEmpty(assetId) && start.HasValue && end.HasValue)
            {
                CreateTable(assetId, start.Value, end.Value.AddDays(1));
            }
        }
    }
}