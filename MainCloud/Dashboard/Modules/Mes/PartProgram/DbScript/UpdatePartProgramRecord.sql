  USE [MES]
  
  /*Insert article without partprogram*/
  insert into PartPrograms(ArticleId)
  select distinct IdArticle
  from dbo.Orders
  where ApplicationId = @ApplicationId and IdArticle not in
  (SELECT ArticleId FROM PartPrograms INTERSECT SELECT IdArticle FROM Orders)
  
  /*insert article type without partprogram*/
  insert into PartPrograms (ArticleType)
  select distinct ArticleClass
  from Articles
  where ApplicationId = @ApplicationId and ISNULL(ArticleClass, '') <> '' and ArticleClass not in
  (select ArticleType from PartPrograms where ISNULL(ArticleType, '') <> '' and ISNULL(ArticleId, '') = '')
  
  /*insert asset without partprogram*/
  insert into PartPrograms(AssetId)
  select Id
  from AssetManagement.dbo.Assets
  where ApplicationId = @ApplicationId and Id not in
  (select AssetId from PartPrograms where ISNULL(ArticleType, '') = '' and ISNULL(ArticleId, '') = '' and ISNULL(AssetId, '') <> '')
  
  /*insert asset group without partprogram*/
  insert into PartPrograms(AssetGroupId)
  select Id
  from AssetManagement.dbo.AssetGroups
  where ApplicationId = @ApplicationId and Id not in
  (select AssetGroupId from PartPrograms where ISNULL(ArticleType, '') = '' and ISNULL(ArticleId, '') = '' and ISNULL(AssetId, '') = '' and ISNULL(AssetGroupId, '') <> '')