﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PartProgram_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.PartProgram.PartProgram_View" %>

<telerik:RadGrid runat="server" ID="gridPartProgram" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="true" AllowSorting="true" AllowFilteringByColumn="true"
    OnNeedDataSource="gridPartProgram_NeedDataSource" OnItemCommand="gridPartProgram_ItemCommand"
    LocalizationPath="~/App_GlobalResources/" EnableLinqExpressions="false">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <GroupingSettings CaseSensitive="false" />
    <MasterTableView Caption="PartProgram" DataKeyNames="Id" NoMasterRecordsText="Non ci sono articoli senza ricetta" CommandItemDisplay="Top">
        <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add" ShowRefreshButton="false" />
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridDropDownColumn UniqueName="ddlAssetGroup" ListTextField="Description"
                ListValueField="Id" DataSourceID="edsAssetGroup" HeaderText="Gruppo<br/>Macchina" DataField="AssetGroupId" 
                DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                AllowSorting="true" AllowFiltering="false" ReadOnly="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="ddlAsset" ListTextField="Code"
                ListValueField="Id" DataSourceID="edsAsset" HeaderText="Macchina" DataField="AssetId" 
                DropDownControlType="RadComboBox" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                AllowSorting="true" AllowFiltering="false" ReadOnly="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="Article.ArticleClass" HeaderText="Tipologia<br/>Articolo" ReadOnly="true" AllowSorting="true"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Article.Code" HeaderText="Articolo" ReadOnly="true" AllowSorting="true"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="PartProgramCode" HeaderText="PartProgram" AllowSorting="false" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridBoundColumn DataField="AssetGroupId" HeaderText="Macchina">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AssetId" HeaderText="Macchina">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            --%><%--<telerik:GridBoundColumn DataField="CycleLength" HeaderText="Lunghezza ciclo">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>--%>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>


<ef:EntityDataSource ID="edsAsset" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    AutoGenerateWhereClause="true" OrderBy="it.Description">
</ef:EntityDataSource>
<ef:EntityDataSource ID="edsAssetGroup" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="AssetGroups"
    AutoGenerateWhereClause="true" OrderBy="it.Description">
</ef:EntityDataSource>