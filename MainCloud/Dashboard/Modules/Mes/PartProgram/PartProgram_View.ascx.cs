﻿using AssetManagement.Models;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Mes.PartProgram
{
    public partial class PartProgram_View : DataWidget<PartProgram_View>
    {
        PartProgramService ppService = new PartProgramService();

        public PartProgram_View() : base(typeof(PartProgram_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void gridPartProgram_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //gridPartProgram.DataSource = ppService.GetAllArticleWithoutPP();
            gridPartProgram.DataSource = ppService.GetAll();
        }

        protected void gridPartProgram_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                var query = File.ReadAllText(Server.MapPath(Path.Combine("~/", TemplateSourceDirectory, "DbScript/UpdatePartProgramRecord.sql")));
                ppService.AddRecordToTable(query);
                e.Canceled = true;
                gridPartProgram.Rebind();
            }
            if (e.CommandName == RadGrid.UpdateCommandName)
            {
                var editableItem = ((GridEditableItem)e.Item);
                var rowId = Convert.ToInt32(editableItem.GetDataKeyValue("Id"));

                PartProgramRepository repo = new PartProgramRepository();
                var row = repo.FindByID(rowId);
                if (row != null)
                {
                    editableItem.UpdateValues(row);
                    repo.Update(row);
                    repo.SaveChanges();
                }
            }
        }

        private void UpdateFromExcel()
        {
            string connString = ConfigurationManager.ConnectionStrings["xlsx"].ConnectionString;
            // Create the connection object
            OleDbConnection oledbConn = new OleDbConnection(connString);
            try
            {
                // Open connection
                oledbConn.Open();

                AssetManagement.Repositories.AssetRepository repo = new AssetManagement.Repositories.AssetRepository();
                List<Asset> assetList = repo.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                foreach (Asset asset in assetList)
                {

                }

                // Create OleDbCommand object and select data from worksheet Sheet1
                OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", oledbConn);

                // Create new OleDbDataAdapter
                OleDbDataAdapter oleda = new OleDbDataAdapter();

                oleda.SelectCommand = cmd;

                // Create a DataSet which will hold the data extracted from the worksheet.
                DataSet ds = new DataSet();

                // Fill the DataSet from the data extracted from the worksheet.
                oleda.Fill(ds, "Employees");

            }
            catch
            {
            }
            finally
            {
                // Close connection
                oledbConn.Close();
            }
        }
    }
}