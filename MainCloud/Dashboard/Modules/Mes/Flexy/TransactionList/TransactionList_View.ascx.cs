﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.Mes.Flexy.TransactionList
{
    public partial class TransactionList_View : DataWidget<TransactionList_View>
    {
        public TransactionList_View() : base(typeof(TransactionList_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void transactionTable_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            using (FlexyModule.Models.FlexyDbContext db = new FlexyModule.Models.FlexyDbContext())
            {
                transactionTable.DataSource = db.stato.ToList();
            }
        }
    }
}