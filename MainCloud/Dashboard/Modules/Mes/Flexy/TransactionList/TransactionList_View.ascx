﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TransactionList_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.Flexy.TransactionList.TransactionList_View" %>

<telerik:RadGrid runat="server" ID="transactionTable" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="true" AllowAutomaticUpdates="false"
    OnNeedDataSource="transactionTable_NeedDataSource"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US" PageSize="25">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id">
        <Columns>
            <%--<telerik:GridDropDownColumn DataField="MachineId" HeaderText="Macchina"
                ListValueField="Id" ListTextField="Description" DataSourceID="edsMachine">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="200px" />
            </telerik:GridDropDownColumn>--%>
            <telerik:GridBoundColumn DataField="stato" HeaderText="Stato">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="descrizione" HeaderText="Descrizione">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridDateTimeColumn DataField="inizio" HeaderText="Inizio" PickerType="DateTimePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn DataField="durata" HeaderText="Durata</br>[m]" DataFormatString="{0:n0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="spessore" HeaderText="Spessore</br>[mm]" DataFormatString="{0:n0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="larghezza" HeaderText="Larghezza</br>[mm]" DataFormatString="{0:n0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="km" HeaderText="Km" DataFormatString="{0:n3}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="kmTot" HeaderText="KmTot" DataFormatString="{0:n3}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="deltaKm" HeaderText="Delta km" DataFormatString="{0:n3}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>