﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Mes.MachineProgress.Models
{
    public class ProgressViewModel
    {
        public string Machine { get; set; }
        public string Mold { get; set; }
        public string Order { get; set; }
        public string Article { get; set; }
        public decimal TheoreticalTime { get; set; }
        public decimal EstimatedTime { get; set; }
        public decimal ProgressTime { get; set; }
        public decimal LeftTime { get; set; }

        public ProgressViewModel()
        {}

        public ProgressViewModel(string machine, string mold, string order, string article, decimal theoreticalTime, decimal estimatedTime, decimal progressTime, decimal leftTime)
        {
            Machine = machine;
            Mold = mold;
            Order = order;
            Article = article;
            TheoreticalTime = theoreticalTime;
            EstimatedTime = estimatedTime;
            ProgressTime = progressTime;
            LeftTime = leftTime;
        }
    }
}