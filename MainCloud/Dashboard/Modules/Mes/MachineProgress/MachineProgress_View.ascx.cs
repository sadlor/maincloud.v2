﻿using AssetManagement.Repositories;
using MainCloud.Dashboard.Modules.Mes.MachineProgress.Models;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Mes.MachineProgress
{
    public partial class MachineProgress_View : DataWidget<MachineProgress_View>
    {
        public MachineProgress_View() :base(typeof(MachineProgress_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void gridMachine_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            var assetList = AS.GetAssetEntity().Select(x => new { x.Id, x.Code }).ToList();
            TransactionService tService = new TransactionService();
            var lastTransactions = tService.GetAllTransactionOpen(assetList.Select(x => x.Id));
            List<ProgressViewModel> dataSource = new List<ProgressViewModel>();
            JobRepository jobRep = new JobRepository();
            IndexService indexService = new IndexService();
            MoldRepository moldRepo = new MoldRepository();
            lastTransactions.Where(x => !string.IsNullOrEmpty(x.JobId)).ToList().ForEach(x =>
            {
                List<Transaction> tList = tService.GetTransactionListByJobId(x.MachineId, x.JobId);
                TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(t => t.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(t => t.Duration));
                decimal productionHour = (decimal)productionTime.TotalHours;
                decimal qtyShotProd = tList.Sum(t => t.PartialCounting - t.BlankShot);
                decimal qtyOrdered = x.Job.Order.QtyOrdered;
                decimal qtyProduced = tList.Sum(t => (t.PartialCounting - t.BlankShot) * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1));
                decimal qtyResidual = qtyOrdered - qtyProduced;
                Job job = jobRep.FindByID(x.JobId);
                decimal tempoAssegnato = 0;
                decimal tempoPrevisto = 0;
                decimal oreProdTeorico = 0;
                if (job.StandardRate > 0)
                {
                    tempoAssegnato = Math.Round(qtyOrdered / (job.StandardRate * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1)), 2);
                    oreProdTeorico += Math.Round(qtyShotProd / job.StandardRate, 2);
                }
                decimal efficiency = indexService.Efficiency(productionHour, oreProdTeorico);
                if (efficiency > 0)
                {
                    tempoPrevisto = Math.Round(tempoAssegnato / efficiency * 100, 2);
                }
                else
                {
                    tempoPrevisto = tempoAssegnato;
                }

                decimal tempoPrevistoProd = 0;
                decimal leftTime = 0;
                //decimal leftTime = (productionHour * qtyResidual) / (qtyProduced > 0 ? qtyProduced : 1);
                if (qtyProduced > 0)
                {
                    if (productionHour > 0)
                    {
                        leftTime = (productionHour * qtyResidual / qtyProduced);
                        tempoPrevistoProd = productionHour + leftTime;
                    }
                }

                dataSource.Add(
                new ProgressViewModel(
                    assetList.Where(a => a.Id == x.MachineId).Select(a => a.Code).FirstOrDefault(),
                    moldRepo.GetName(x.MoldId),
                    x.Job.Order.CustomerOrder.OrderCode,
                    x.Job.Order.Article.Code,
                    tempoAssegnato,
                    tempoPrevisto,
                    productionHour,
                    leftTime));
            });

            gridMachine.DataSource = dataSource;
        }

        protected void gridMachine_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                HtmlGenericControl barTheoreticalTime = item["ChartColumn"].FindControl("barTheoreticalTime") as HtmlGenericControl;
                HtmlGenericControl textEstimatedTime = item["ChartColumn"].FindControl("textEstimatedTime") as HtmlGenericControl;
                HtmlGenericControl barProducedTime = item["ChartColumn"].FindControl("barProducedTime") as HtmlGenericControl;
                HtmlGenericControl barLeftTime = item["ChartColumn"].FindControl("barLeftTime") as HtmlGenericControl;
                decimal tempoAssegnato = Convert.ToDecimal(item.GetDataKeyValue("TheoreticalTime"));
                decimal tempoPrevisto = Convert.ToDecimal(item.GetDataKeyValue("EstimatedTime"));
                decimal productionTime = Convert.ToDecimal(item.GetDataKeyValue("ProgressTime"));
                decimal leftTime = Convert.ToDecimal(item.GetDataKeyValue("LeftTime"));

                if (tempoPrevisto > 0)
                {
                    barTheoreticalTime.Attributes["aria-valuenow"] = Math.Round(tempoAssegnato / tempoPrevisto * 100, 2).ToString().Replace(",", ".");
                    barTheoreticalTime.Attributes["style"] = string.Format("z-index:1000; font-size: large; font-weight: bold; width:{0}%", Math.Round(tempoAssegnato / tempoPrevisto * 100, 2)).Replace(",", ".");
                    barTheoreticalTime.InnerText = string.Format("{0:n1} h", tempoAssegnato);

                    textEstimatedTime.Attributes["style"] = string.Format("z-index:1000; font-size: large; font-weight: bold;  text-align:right; width:{0}%", 100 - Math.Round(tempoAssegnato / tempoPrevisto * 100, 2)).Replace(",", ".");
                    textEstimatedTime.InnerText = string.Format("{0:n1} h", tempoPrevisto - tempoAssegnato);
                }

                if (productionTime > 0)
                {
                    decimal tempoPrevistoProd = productionTime + leftTime;
                    barProducedTime.Attributes["aria-valuenow"] = Math.Round((decimal)productionTime / tempoPrevistoProd * 100, 2).ToString().Replace(",", ".");
                    barProducedTime.Attributes["style"] = string.Format("font-size: large; font-weight: bold; text-align:center; float:left; width:{0}%", Math.Round((decimal)productionTime / tempoPrevistoProd * 100, 2)).Replace(",", ".");
                    barProducedTime.InnerText = string.Format("{0} h", Math.Round(productionTime, 1));

                    //decimal leftTime = tempoPrevisto - (decimal)productionTime.TotalHours;
                    barLeftTime.Attributes["aria-valuenow"] = Math.Round(leftTime / tempoPrevistoProd * 100, 2).ToString().Replace(",", ".");
                    barLeftTime.Attributes["style"] = string.Format("background-color:#f5f5f5 !important; font-size: large; font-weight: bold; color: black; text-align:right; float:left; width:{0}%", Math.Round(leftTime / tempoPrevistoProd * 100, 2)).Replace(",", ".");
                    barLeftTime.InnerText = string.Format("{0:n1} h", leftTime);
                }
            }
        }
    }
}