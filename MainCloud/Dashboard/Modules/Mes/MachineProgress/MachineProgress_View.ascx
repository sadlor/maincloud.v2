﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MachineProgress_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.MachineProgress.MachineProgress_View" %>

<asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>
        <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="gridMachine" AutoGenerateColumns="false" AllowSorting="true"
            OnNeedDataSource="gridMachine_NeedDataSource" OnItemDataBound="gridMachine_ItemDataBound">
            <MasterTableView Caption="" DataKeyNames="TheoreticalTime, EstimatedTime, ProgressTime, LeftTime" FilterExpression="">
                <Columns>
                    <telerik:GridBoundColumn DataField="Machine" HeaderText="Macchina">
                        <HeaderStyle HorizontalAlign="Center" Width="115px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Mold" HeaderText="Stampo">
                        <HeaderStyle HorizontalAlign="Center" Width="115px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Order" HeaderText="Commessa">
                        <HeaderStyle HorizontalAlign="Center" Width="110px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Article" HeaderText="Articolo">
                        <HeaderStyle HorizontalAlign="Center" Width="200px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="ChartColumn" HeaderText="Avanzamento" AllowFiltering="false" SortExpression="ProgressTime">
                        <ItemTemplate>
                            <div class="row" runat="server">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="progress progressWithoutMargin" style="margin-bottom:0px!important;">
                                        <div runat="server" id="barEstimatedTime" class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="100"
                                            aria-valuemin="0" aria-valuemax="100" style="width:100%">
                                            <div runat="server" id="barTheoreticalTime" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100"
                                                aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                            </div>
                                            <div runat="server" class="progress-bar progress-bar-warning" id="textEstimatedTime" style="width:0%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="progress progressWithoutMargin" style="text-align:right; margin-bottom:0px!important;">
                                        <div runat="server" id="barProducedTime" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0"
                                                aria-valuemin="0" aria-valuemax="100" style="text-align:center; float:left; width:0%;"></div>
                                        <div runat="server" id="barLeftTime" class="progress-bar" role="progressbar" aria-valuenow="100"
                                                aria-valuemin="0" aria-valuemax="100" style="background-color:#f5f5f5 !important; text-align:center; width:100%; border:solid; float:left;"></div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </ContentTemplate>
</asp:UpdatePanel>