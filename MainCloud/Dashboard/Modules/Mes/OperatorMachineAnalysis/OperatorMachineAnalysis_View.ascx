﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OperatorMachineAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Mes.OperatorMachineAnalysis.OperatorMachineAnalysis_View" %>

<script type="text/javascript">
    var limit = 5;
    function OnClientDropDownOpening(sender, args) {
        if (sender.get_entries().get_count() == limit) {
            args.set_cancel(true);
        }
    }

    function OnClientEntryAdding(sender, args) {
        var entries = sender.get_entries();
        for (var i = 0; i < entries.get_count() ; i++) {
            if (entries.getEntry(i).get_value() == args.get_entry().get_value()) {
                args.set_cancel(true);
            }
        }
    }

    <%--function OnAssetFilterLoad() {
        OnFilterLoad();
        OnConfirmBtnLoad();

        if ($("#mpstatsDiv .headerDiv div:first-child .mpeBkTitolo").text() == "---") {
            $("#mpstatsDiv .headerDiv div").eq(0).attr("style", "display: none");
            $("#mpstatsDiv .headerDiv div").eq(1).attr("style", "display: none");
            $("#mpstatsDiv .headerDiv div").eq(2).attr("style", "width: 10%");
            $("#mpstatsDiv .headerDiv div").eq(3).attr("style", "width: 90%");
        }
        else {
            $("#mpstatsDiv .headerDiv div").eq(0).attr("style", "width: 6%");
            $("#mpstatsDiv .headerDiv div").eq(1).attr("style", "width: 25%");
            $("#mpstatsDiv .headerDiv div").eq(2).attr("style", "width: 10%");
            $("#mpstatsDiv .headerDiv div").eq(3).attr("style", "width: 20%");
        }
    }

    function OnFilterLoad() {
        if ($("#<%= submitFlag.ClientID %>").val() == 1) {
            $("#<%= submitFlag.ClientID %>").val(0);

            $("#mpstatsDiv").css("display", "inherit");
            $("#mpdTableContainer").css("display", "inherit");
            $("#mpdeTableContainer").css("display", "none");
        }

        if ($("#<%= selectedPlanId.ClientID %>").val() != "") {
            selectActivity($("#<%= selectedPlanIdIndex.ClientID %>").val());
            $("#<%= operatorFilter.ClientID %> .racTextToken").text($("#<%= operatorFilterVal.ClientID %>").val());
            $("#<%= selectedPlanId.ClientID %>").val("");
            $("#mpstatsDiv").css("display", "inherit");
            $("#mpdTableContainer").css("display", "inherit");

            $(".resultLabel").each(function () {
                $(this).parent().css("background-color", $(this).attr("data-bg"));
            });

            $("#mpdeTableContainer").css("display", "inherit");
        }
    }

    function OnConfirmBtnLoad() {
        $("#btnConfirmSelection").off("click");
        $("#btnConfirmSelection").click(function () {
            printSelectedEntries();
            var ofv = $("#<%= moldFilter.ClientID %> .racTextToken").text(), opfv = $("#<%= operatorFilter.ClientID %> .racTextToken").text();

            if (ofv.length > 0) {
                var mv = $("#<%= moldFilter.ClientID %>_ClientState").attr("value");
                mv = mv.substring(mv.indexOf('"value":"') + 9, mv.indexOf('"}}]}'));

                var dstart = $("#<%= dtPickStart.ClientID%>_dateInput_ClientState").val();
                dstart = dstart.substring(dstart.indexOf('AsString"') + 11, dstart.indexOf('minDateStr') - 3).replace("-00-00-00", "");
                var dend = $("#<%= dtPickEnd.ClientID%>_dateInput_ClientState").val();
                dend = dend.substring(dend.indexOf('AsString"') + 11, dend.indexOf('minDateStr') - 3).replace("-00-00-00", "");

                $("#<%= moldIdFilterVal.ClientID %>").val(mv);
                $("#<%= moldFilterVal.ClientID %>").val(ofv);
                $("#<%= operatorFilterVal.ClientID %>").val(opfv);
                $("#<%= submitFlag.ClientID %>").val(1);
                $("#<%= selectedDateStart.ClientID %>").val(dstart);
                $("#<%= selectedDateEnd.ClientID %>").val(dend);

                $("#mpstatsDiv").css("display", "none");
                $("#mpdTableContainer").css("display", "none");
                $("#mpdeTableContainer").css("display", "none");
                __doPostBack('<%=moldFilter.ClientID %>', "submit");
            } else {
                alert("Compilare tutti i campi.");
            }
        });
    }--%>
</script>

<asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="panel panel-info">
            <div class="panel-body">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="assetFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="135px" Width="280px"
                                    TextSettings-SelectionMode="Single" MaxResultCount="10" MinFilterLength="0" DataSourceID="edsAsset" DataTextField="Code" DataValueField="Id" InputType="Text" Filter="StartsWith" Label="Asset: " OnClientDropDownOpening="OnClientDropDownOpening" OnClientEntryAdding="OnClientEntryAdding">
                                </telerik:RadAutoCompleteBox>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                                <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                                    <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                                </telerik:RadDatePicker>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="operatorFilter" EmptyMessage="Seleziona" EnableClientFiltering="false" LabelWidth="135px" Width="280px"
                                    TextSettings-SelectionMode="Single" InputType="Text" MaxResultCount="10" MinFilterLength="0" DataSourceID="edsOperator" DataTextField="Username" DataValueField="Id" Label="Operatore: " OnClientDropDownOpening="OnClientDropDownOpening" OnClientEntryAdding="OnClientEntryAdding">
                                    <DropDownItemTemplate>
                                        <table>
                                            <tr>
                                                <td style="padding-right: 10px"><%# DataBinder.Eval(Container.DataItem, "Badge")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "Username")%></td>
                                            </tr>
                                        </table>
                                    </DropDownItemTemplate>
                                </telerik:RadAutoCompleteBox>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                                <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                                    <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                                </telerik:RadDatePicker>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="text-align:right;">
                    <asp:Button runat="server" ID="btnConfirmSelection" CssClass="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
                </div>
            </div>
        </div>
        <telerik:RadGrid runat="server" ID="gridTransaction" RenderMode="Lightweight" AutoGenerateColumns="false" ShowFooter="True" AllowPaging="true"
            OnNeedDataSource="gridTransaction_NeedDataSource" GroupingEnabled="true">
            <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
            <MasterTableView ShowGroupFooter="true" FilterExpression="" Caption="Dettaglio transazioni" NoMasterRecordsText="Non ci sono transazioni con commessa e/o produzione">
                <Columns>
                    <telerik:GridDropDownColumn DataField="MachineId" HeaderText="Macchina"
                        ListValueField="Id" ListTextField="Description" DataSourceID="edsAsset">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDropDownColumn>
                    <%--<telerik:GridBoundColumn DataField="Status" HeaderText="Stato">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="Operatore">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridBoundColumn DataField="Cause.Description" HeaderText="Causale">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Job.Order.CustomerOrder.OrderCode" HeaderText="Commessa">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Job.Order.Article.Code" HeaderText="Articolo">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn DataField="Start" HeaderText="Start" PickerType="DateTimePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn DataField="End" HeaderText="End" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn DataField="Duration" HeaderText="Tempo produzione">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridBoundColumn DataField="PartialCounting" HeaderText="Colpi" DataFormatString="{0:f0}" Aggregate="Sum" FooterText="Tot:">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                        <FooterStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CavityMoldNum" HeaderText="Impronte" DataFormatString="{0:f0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="QtyProduced" HeaderText="Pezzi" DataFormatString="{0:f0}" Aggregate="Sum" FooterText="Tot:">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                        <FooterStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="QtyProductionWaste" HeaderText="Scarti" DataFormatString="{0:f0}" Aggregate="Sum" FooterText="Tot:">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                        <FooterStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </ContentTemplate>
</asp:UpdatePanel>

<ef:EntityDataSource ID="edsAsset" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    OrderBy="it.Code" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsOperator" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Operators"
    OrderBy="it.Badge" AutoGenerateWhereClause="true" />