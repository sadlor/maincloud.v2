﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.Mes.OperatorMachineAnalysis
{
    public partial class OperatorMachineAnalysis_View : DataWidget<OperatorMachineAnalysis_View>
    {
        public OperatorMachineAnalysis_View() : base(typeof(OperatorMachineAnalysis_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                gridTransaction.Visible = false;

                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Now;
                }
            }
            edsAsset.WhereParameters.Clear();
            edsAsset.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsOperator.WhereParameters.Clear();
            edsOperator.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        protected void gridTransaction_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (gridTransaction.Visible)
            {
                ProductionPaymentService ppService = new ProductionPaymentService();
                string assetId = assetFilter.Entries.Count > 0 ? assetFilter.Entries[0].Value : string.Empty;
                string operatorId = operatorFilter.Entries.Count > 0 ? operatorFilter.Entries[0].Value : string.Empty;
                DateTime? start = dtPickStart.SelectedDate.Value;
                DateTime? end = dtPickEnd.SelectedDate.Value;
                if (start.HasValue)
                {
                    if (end == null || end?.Date < start?.Date)
                    {
                        end = DateTime.Now;
                    }
                    if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(operatorId))
                    {
                        gridTransaction.DataSource = ppService.ProductionPaymentPerAssetOperator(assetId, operatorId, start.Value, end.Value);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId))
                        {
                            gridTransaction.DataSource = ppService.ProductionPaymentPerAsset(assetId, start.Value, end.Value);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(assetId))
                            {
                                gridTransaction.DataSource = ppService.ProductionPaymentPerOperator(operatorId, start.Value, end.Value);
                            }
                        }
                    }
                }
            }
        }

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            gridTransaction.Visible = true;
            gridTransaction.Rebind();
        }
    }
}