﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.MaintenancePlansList
{
    public partial class MaintenancePlansList_View : DataWidget<MaintenancePlansList_View>
    {
        public MaintenancePlansList_View() : base(typeof(MaintenancePlansList_View)) { }

        protected void mpTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            MaintenancePlanRepository mpRep = new MaintenancePlanRepository();
            List<MaintenancePlan> mpList = mpRep.ReadAll().ToList();
            mpTable.DataSource = mpList;
        }

        protected void mpTable_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
                mpTable.Rebind();
        }

        protected void mpdTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            string IdPlan = ((GridNestedViewItem)(sender as RadGrid).Parent.BindingContainer).ParentItem.KeyValues.Replace("{Id:\"", "").Replace("\"}", "");
            MaintenancePlanDetailRepository mpDRep = new MaintenancePlanDetailRepository();
            List<MaintenancePlanDetail> mpDList = mpDRep.GetAllByMaintenancePlanId(Convert.ToInt32(IdPlan));
            (sender as RadGrid).DataSource = mpDList;
        }
    }
}