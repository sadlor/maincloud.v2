﻿//------------------------------------------------------------------------------
// <generato automaticamente>
//     Codice generato da uno strumento.
//
//     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
//     il codice viene rigenerato. 
// </generato automaticamente>
//------------------------------------------------------------------------------

namespace MainCloud.Dashboard.Modules.MaintenancePlansList {
    
    
    public partial class MaintenancePlansList_Edit {
        
        /// <summary>
        /// Controllo dlgPlanErrorDialog.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MainCloudFramework.Web.PopUpDialog dlgPlanErrorDialog;
        
        /// <summary>
        /// Controllo errorLabel.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label errorLabel;
        
        /// <summary>
        /// Controllo edsMU.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::Microsoft.AspNet.EntityDataSource.EntityDataSource edsMU;
        
        /// <summary>
        /// Controllo mpTable.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::Telerik.Web.UI.RadGrid mpTable;
    }
}
