﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaintenancePlansList_Edit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MaintenancePlansList.MaintenancePlansList_Edit" %>

<link href="/Dashboard/Modules/MaintenancePlansList/MaintenancePlansList.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    function PopUpShowing(sender, eventArgs)
    {
        var popUp = eventArgs.get_popUp();
        popUp.style.position = "fixed";
        popUp.style.top = "10%";
    }

    function DPopUpShowing(sender, eventArgs)
    {
        var popUp = eventArgs.get_popUp();
        popUp.style.position = "fixed";
        popUp.style.top = "10%";
    }

    function radioTimeFrequency_CheckedChanged(sender, eventArgs)
    {
        if (eventArgs.get_checked())
        {
            document.getElementById("txtTimeFrequencyHours").disabled = false;
            document.getElementById("txtTimeFrequencyMinutes").disabled = false;
            document.getElementById("txtTimeFrequencySeconds").disabled = false;
        }
        else
        {
            document.getElementById("txtTimeFrequencyHours").disabled = true;
            document.getElementById("txtTimeFrequencyMinutes").disabled = true;
            document.getElementById("txtTimeFrequencySeconds").disabled = true;
        }
    }

    function radioQtyFrequency_CheckedChanged(sender, eventArgs)
    {
        if (eventArgs.get_checked())
        {
            document.getElementById("txtQtyFrequency").disabled = false;
        }
        else
        {
            document.getElementById("txtQtyFrequency").disabled = true;
        }
    }

    function radioOperator_CheckedChanged(sender, eventArgs)
    {
        if (eventArgs.get_checked())
        {
            document.getElementById("txtExecutionOperator").disabled  = false;
        }
        else
        {
            document.getElementById("txtExecutionOperator").disabled  = true;
        }
    }

    function radioSupplier_CheckedChanged(sender, eventArgs)
    {
        if (eventArgs.get_checked())
        {
            document.getElementById("txtExecutionSupplier").disabled  = false;
        }
        else
        {
            document.getElementById("txtExecutionSupplier").disabled  = true;
        }
    }
</script>

<style>
    .rgEditFormContainer tr
    {
        height: 50px!important;
    }

    .rgEditFormContainer tr td:first-child
    {
        width: 120px;
        font-weight: bold!important;
    }
</style>


<mcf:PopUpDialog ID="dlgPlanErrorDialog" runat="server" Title="Warning">
    <Body>
        <asp:label ID="errorLabel" Text="Il formato della data non è corretto.<br>Le modifiche non sono state salvate." runat="server"/><br /><br />
        <telerik:RadButton runat="server" Text="Ok" OnClick="dlgPlanError_Ok_Click"></telerik:RadButton>
    </Body>
</mcf:PopUpDialog>


<ef:EntityDataSource ID="edsMU" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="MeasureUnits"
    AutoGenerateWhereClause="true" />

<telerik:RadGrid runat="server" ID="mpTable" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="True" AllowSorting="true"
    OnNeedDataSource="mpTable_NeedDataSource" OnUpdateCommand="mpTable_UpdateCommand" OnInsertCommand="mpTable_InsertCommand" OnDeleteCommand="mpTable_DeleteCommand" OnItemCommand="mpTable_ItemCommand"
    AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false" OnPreRender="mpTable_PreRender" Height="700px" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <MasterTableView FilterExpression="" Caption="Maintenance Plans" CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" DataKeyNames="Id" NoMasterRecordsText="No records to display." EditMode="PopUp">
        <EditFormSettings InsertCaption="Create new Maintenance Plan">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridTemplateColumn DataField="Id" HeaderText="Cod." ItemStyle-Width="50px" ItemStyle-Wrap="true" ReadOnly="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Id") %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="Description" HeaderText="Descrizione" ItemStyle-Width="290px" ItemStyle-Wrap="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Description") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtDescription"
                            Text='<%# Eval("Description")%>' DbValue='<%# Eval("Description")%>' Width="100%" MaxLength="50" TextMode="MultiLine" Rows="2">
                        </telerik:RadTextBox>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="CreationDate" HeaderText="Creazione" ItemStyle-Width="110px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("CreationDateText") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadDatePicker RenderMode="Lightweight" runat="server" ID="txtCreationDate"
                            DbSelectedDate='<%# Eval("CreationDate")%>' DbValue='<%# Eval("CreationDate")%>' DataFormatString="{0:dd/MM/yyyy}" Width="100%">
                        </telerik:RadDatePicker>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn> 
            <telerik:GridTemplateColumn DataField="ValidationDate" HeaderText="Validità" ItemStyle-Width="110px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ValidationDateText") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadDatePicker RenderMode="Lightweight" runat="server" ID="txtValidationDate"
                            DbSelectedDate='<%# Eval("ValidationDate")%>' DbValue='<%# Eval("ValidationDate")%>' DataFormatString="{0:dd/MM/yyyy}" Width="100%">
                        </telerik:RadDatePicker>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn> 
            <telerik:GridTemplateColumn DataField="UpdateDate" HeaderText="Modifica" ItemStyle-Width="110px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("UpdateDateText") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadDatePicker RenderMode="Lightweight" runat="server" ID="txtUpdateDate"
                            DbSelectedDate='<%# Eval("UpdateDate")%>' DbValue='<%# Eval("UpdateDate")%>' DataFormatString="{0:dd/MM/yyyy}" Width="100%">
                        </telerik:RadDatePicker>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn> 
            <telerik:GridTemplateColumn DataField="ApprovalUser" HeaderText="Approvazione" ItemStyle-Width="110px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ApprovalUser") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtApprovalUser"
                            Text='<%# Eval("ApprovalUser")%>' DbValue='<%# Eval("ApprovalUser")%>' MaxLength="50" Width="100%">
                        </telerik:RadTextBox>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>                
            <telerik:GridTemplateColumn HeaderText="Esecuzione" ItemStyle-Width="110px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Executor") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12" style="padding-right: 0; height: 60px;">
                        <div>
                            <telerik:RadButton runat="server" ID="executionOperator" ButtonType="ToggleButton" ToggleType="Radio" ClientIDMode="Static" AutoPostBack="false" GroupName="executionButtons" OnClientCheckedChanged="radioOperator_CheckedChanged" Checked='<%# Eval("ExecutionOperatorFlag")%>' style="position: relative; top: 25px;"/>
                        </div>
                        <div style="margin-left: 10px; position: relative; top: -22px;">
                            <div class="col-md-12">
                                <asp:Label runat="server">Operatore</asp:Label><br />
                                <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtExecutionOperator" ClientIDMode="Static"
                                    Text='<%# Eval("ExecutionOperator")%>' DbValue='<%# Eval("ExecutionOperator")%>' MaxLength="50" Width="100%" Enabled='<%# Eval("ExecutionOperatorFlag")%>'>
                                </telerik:RadTextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding-right: 0; height: 60px;">
                        <div>
                             <telerik:RadButton runat="server" ID="executionSupplier" ButtonType="ToggleButton" ToggleType="Radio" ClientIDMode="Static" AutoPostBack="false" GroupName="executionButtons" OnClientCheckedChanged="radioSupplier_CheckedChanged" Checked='<%# Eval("ExecutionSupplierFlag")%>' style="position: relative; top: 25px;"/>
                        </div>
                        <div style="margin-left: 10px; position: relative; top: -22px;">
                            <div class="col-md-12">
                                <asp:Label runat="server">Fornitore</asp:Label><br />
                                <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtExecutionSupplier" ClientIDMode="Static"
                                    Text='<%# Eval("ExecutionSupplier")%>' DbValue='<%# Eval("ExecutionSupplier")%>' MaxLength="50" Width="100%" Enabled='<%# Eval("ExecutionSupplierFlag")%>'>
                                </telerik:RadTextBox>
                            </div>
                        </div>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Tipologia" ItemStyle-Width="50px" ItemStyle-Wrap="true" ReadOnly="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Type") %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="TimeFrequencyDate" HeaderText="Freq. Tempo" ItemStyle-Width="85px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("TimeFrequencyDate") %>' Visible='<%# Eval("TimeFrequencyFlag")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12" style="padding-right: 0; height: 50px;">
                        <div>
                            <telerik:RadButton runat="server" ID="radioTimeFrequency" ButtonType="ToggleButton" ToggleType="Radio" ClientIDMode="Static" AutoPostBack="false" GroupName="freqButtons" OnClientCheckedChanged="radioTimeFrequency_CheckedChanged" Checked='<%# Eval("TimeFrequencyFlag")%>' style="position: relative; top: 17px;"/>
                        </div>
                        <div style="margin-left: 10px; position: relative; top: -18px;">
                            <div class="col-md-4">
                                <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtTimeFrequencyHours" NumberFormat-DecimalDigits="0" Label="" ClientIDMode="Static"
                                    Text='<%# Eval("TimeFrequencyHours")%>' DbValue='<%# Eval("TimeFrequencyHours")%>' Width="100%" Enabled='<%# Eval("TimeFrequencyFlag")%>'>
                                </telerik:RadNumericTextBox>
                                <br />
                                <asp:Label runat="server">hours</asp:Label>
                            </div>
                            <div class="col-md-4">
                                <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtTimeFrequencyMinutes" NumberFormat-DecimalDigits="0" Label="" ClientIDMode="Static"
                                    Text='<%# Eval("TimeFrequencyMinutes")%>' DbValue='<%# Eval("TimeFrequencyMinutes")%>' Width="100%" Enabled='<%# Eval("TimeFrequencyFlag")%>'>
                                </telerik:RadNumericTextBox>
                                <br />
                                <asp:Label runat="server">min.</asp:Label>
                            </div>
                            <div class="col-md-4">
                                <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtTimeFrequencySeconds" NumberFormat-DecimalDigits="0" Label="" ClientIDMode="Static"
                                    Text='<%# Eval("TimeFrequencySeconds")%>' DbValue='<%# Eval("TimeFrequencySeconds")%>' Width="100%" Enabled='<%# Eval("TimeFrequencyFlag")%>'>
                                </telerik:RadNumericTextBox>
                                <br />
                                <asp:Label runat="server">sec.</asp:Label>
                            </div>
                        </div>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="QtyFrequency" HeaderText="Freq. Quantità" ItemStyle-Width="100px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("QtyFrequency") %>' Visible='<%# Eval("QtyFrequencyFlag")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12" style="padding-right: 0; height: 50px;">
                        <div>
                            <telerik:RadButton runat="server" ID="radioQtyFrequency" ButtonType="ToggleButton" ToggleType="Radio" ClientIDMode="Static" AutoPostBack="false" GroupName="freqButtons" OnClientCheckedChanged="radioQtyFrequency_CheckedChanged" Checked='<%# Eval("QtyFrequencyFlag")%>' style="position: relative; top: 17px;"/>
                        </div>
                        <div style="margin-left: 10px; position: relative; top: -11px;">
                            <div class="col-md-12">
                                <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtQtyFrequency" ClientIDMode="Static"
                                    Text='<%# Eval("QtyFrequency")%>' DbValue='<%# Eval("QtyFrequency")%>' Width="100%" Enabled='<%# Eval("QtyFrequencyFlag")%>'>
                                </telerik:RadNumericTextBox>
                            </div>
                        </div>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Tempo previsto" ItemStyle-Width="85px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("TimeExpectedString") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12" style="padding-left: 0; padding-right: 0; height: 50px;">
                        <div class="col-md-4">
                            <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtTimeExpectedHours" NumberFormat-DecimalDigits="0" Label="" ClientIDMode="Static"
                                Text='<%# Eval("TimeExpectedHours")%>' DbValue='<%# Eval("TimeExpectedHours")%>' Width="100%">
                            </telerik:RadNumericTextBox>
                            <br />
                            <asp:Label runat="server">hours</asp:Label>
                        </div>
                        <div class="col-md-4">
                            <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtTimeExpectedMinutes" NumberFormat-DecimalDigits="0" Label="" ClientIDMode="Static"
                                Text='<%# Eval("TimeExpectedMinutes")%>' DbValue='<%# Eval("TimeExpectedMinutes")%>' Width="100%">
                            </telerik:RadNumericTextBox>
                            <br />
                            <asp:Label runat="server">min.</asp:Label>
                        </div>
                        <div class="col-md-4">
                            <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtTimeExpectedSeconds" NumberFormat-DecimalDigits="0" Label="" ClientIDMode="Static"
                                Text='<%# Eval("TimeExpectedSeconds")%>' DbValue='<%# Eval("TimeExpectedSeconds")%>' Width="100%">
                            </telerik:RadNumericTextBox>
                            <br />
                            <asp:Label runat="server">sec.</asp:Label>
                        </div>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="PlanNotes" HeaderText="Note" ItemStyle-Wrap="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("PlanNotes") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtPlanNotes"
                            Text='<%# Eval("PlanNotes")%>' DbValue='<%# Eval("PlanNotes")%>' Width="100%" MaxLength="2000" TextMode="MultiLine" Rows="2">
                        </telerik:RadTextBox>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>       
            <telerik:GridButtonColumn ConfirmText="Delete this Control Plan and all its Activites?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
        <NestedViewTemplate runat="server">
            <br />
            <asp:Label runat="server">
                <b>Maintenance Plan Definition</b>
            </asp:Label>
            <br /><br />
            <telerik:RadGrid ID="mpdTable" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="true" OnNeedDataSource="mpdTable_NeedDataSource" OnItemCommand="mpdTable_ItemCommand" OnUpdateCommand="mpdTable_UpdateCommand" OnInsertCommand="mpdTable_InsertCommand" OnDeleteCommand="mpdTable_DeleteCommand" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
                <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" CommandItemDisplay="Top" NoMasterRecordsText="No records to display." EditMode="PopUp">
                    <EditFormSettings InsertCaption="Create new Activity">
                        <PopUpSettings Modal="true" />
                    </EditFormSettings>
                    <Columns>
                        <telerik:GridEditCommandColumn />
                        <telerik:GridTemplateColumn HeaderText="Descrizione" ItemStyle-Width="40%" ItemStyle-Wrap="true">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Description") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadTextBox RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="txtDetailDescription"
                                        Text='<%# Eval("Description")%>' DbValue='<%# Eval("Description")%>' Width="100%" MaxLength="40" TextMode="MultiLine" Rows="2">
                                    </telerik:RadTextBox>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="ItemCode" HeaderText="Cod. Particolare" ItemStyle-Width="120px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ItemCode") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadTextBox RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="txtDetailItemCode"
                                        Text='<%# Eval("ItemCode")%>' DbValue='<%# Eval("ItemCode")%>' MaxLength="12" Width="100%">
                                    </telerik:RadTextBox>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="ToolCode" HeaderText="Cod. Strumento" ItemStyle-Width="120px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ToolCode") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadTextBox RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="txtDetailToolCode"
                                        Text='<%# Eval("ToolCode")%>' DbValue='<%# Eval("ToolCode")%>' MaxLength="12" Width="100%">
                                    </telerik:RadTextBox>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="DetailNotes" HeaderText="Note" ItemStyle-Width="30%" ItemStyle-Wrap="true">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("DetailNotes") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadTextBox RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="txtDetailDetailNotes"
                                        Text='<%# Eval("DetailNotes")%>' DbValue='<%# Eval("DetailNotes")%>' Width="100%" MaxLength="2000" TextMode="MultiLine" Rows="2">
                                    </telerik:RadTextBox>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete this Activity?" ConfirmDialogType="RadWindow"
                            ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <ClientEvents OnPopUpShowing="DPopUpShowing" />
                </ClientSettings>
            </telerik:RadGrid>
            <br />
        </NestedViewTemplate>
    </MasterTableView>
    <ClientSettings>
        <Scrolling AllowScroll="true" EnableColumnClientFreeze="true" FrozenColumnsCount="2" UseStaticHeaders="false" />
        <ClientEvents OnPopUpShowing="PopUpShowing" />
    </ClientSettings>
</telerik:RadGrid>