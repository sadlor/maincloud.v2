﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaintenancePlansList_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.MaintenancePlansList.MaintenancePlansList_View" %>

<link href="/Dashboard/Modules/MaintenancePlansList/MaintenancePlansList.css" rel="stylesheet" type="text/css" />

<asp:UpdateProgress runat="server" ID="progressPnl">
    <ProgressTemplate>
        <div class="loading">Loading&#8230;</div>
    </ProgressTemplate>
</asp:UpdateProgress>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <telerik:RadGrid runat="server" ID="mpTable" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="True" AllowSorting="true"
            OnNeedDataSource="mpTable_NeedDataSource" OnPreRender="mpTable_PreRender" Width="100%" Height="600px" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="false" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
            <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" NoMasterRecordsText="No records to display.">
                <Columns>
                    <telerik:GridBoundColumn DataField="Id" HeaderText="Cod." ItemStyle-Width="50px" ItemStyle-Wrap="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Descrizione" ItemStyle-Width="340px" ItemStyle-Wrap="true" MaxLength="50">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="CreationDate" HeaderText="Creazione" ItemStyle-Width="110px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("CreationDateText") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn> 
                    <telerik:GridTemplateColumn DataField="ValidationDate" HeaderText="Validità" ItemStyle-Width="110px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ValidationDateText") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn> 
                    <telerik:GridTemplateColumn DataField="UpdateDate" HeaderText="Modifica" ItemStyle-Width="110px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("UpdateDateText") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn> 
                    <telerik:GridBoundColumn DataField="ApprovalUser" HeaderText="Approvazione" ItemStyle-Width="110px" MaxLength="50">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>                
                    <telerik:GridBoundColumn DataField="Executor" HeaderText="Esecuzione" ItemStyle-Width="110px" MaxLength="50">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Tipologia" ItemStyle-Width="50px" ItemStyle-Wrap="true" ReadOnly="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Type") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="TimeFrequencyDate" HeaderText="Freq. Tempo" ItemStyle-Width="85px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("TimeFrequencyDate") %>' Visible='<%# Eval("TimeFrequencyFlag")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="QtyFrequency" HeaderText="Freq. Quantità" ItemStyle-Width="100px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("QtyFrequency") %>' Visible='<%# Eval("QtyFrequencyFlag")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Tempo previsto" ItemStyle-Width="85px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("TimeExpectedString") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="PlanNotes" HeaderText="Note" MaxLength="2000" ItemStyle-Wrap="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>    
                </Columns>
                <NestedViewTemplate runat="server">
                    <br />
                    <asp:Label runat="server">
                        <b>Maintenance Plan Definition</b>
                    </asp:Label>
                    <br /><br />
                    <telerik:RadGrid ID="mpdTable" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="true" OnNeedDataSource="mpdTable_NeedDataSource" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
                        <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" NoMasterRecordsText="No records to display.">
                            <Columns>
                                <telerik:GridBoundColumn DataField="Description" HeaderText="Descrizione" ItemStyle-Width="40%" MaxLength="40" ItemStyle-Wrap="true">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ItemCode" HeaderText="Cod. Particolare" ItemStyle-Width="120px" MaxLength="40" ItemStyle-Wrap="true">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ToolCode" HeaderText="Cod. Strumento" ItemStyle-Width="120px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DetailNotes" HeaderText="Note" MaxLength="2000" ItemStyle-Width="30%" ItemStyle-Wrap="true">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <br />
                </NestedViewTemplate>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="true" EnableColumnClientFreeze="true" FrozenColumnsCount="1" UseStaticHeaders="false" />
            </ClientSettings>
        </telerik:RadGrid>
    </ContentTemplate>
</asp:UpdatePanel>