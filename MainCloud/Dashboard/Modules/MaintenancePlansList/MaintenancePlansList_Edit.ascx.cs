﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.UI.Modules;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.MaintenancePlansList
{
    public partial class MaintenancePlansList_Edit : WidgetControl<object>
    {
        public MaintenancePlansList_Edit() : base(typeof(MaintenancePlansList_Edit)) { }

        protected void Page_Load(object sender, EventArgs e) { }

        public string ApplicationId
        {
            get
            {
                return (Page as PageEdit).GetApplicationId();
            }
        }

        protected void mpTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            MaintenancePlanRepository mpRep = new MaintenancePlanRepository();
            List<MaintenancePlan> mpList = mpRep.ReadAll().ToList();
            mpTable.DataSource = mpList;
        }

        protected void mpTable_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
                mpTable.Rebind();
        }

        protected void mpTable_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                //Add new button clicked
                e.Canceled = true;
                //Prepare an IDictionary with the predefined values
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();
                newValues["Description"] = "";
                newValues["TimeFrequencyFlag"] = true;
                newValues["QtyFrequencyFlag"] = false;
                newValues["CreationDate"] = DateTime.Today;
                newValues["ValidationDate"] = DateTime.Today;
                newValues["UpdateDate"] = DateTime.Today;
                newValues["TimeFrequencyHours"] = "00";
                newValues["TimeFrequencyMinutes"] = "00";
                newValues["TimeFrequencySeconds"] = "00";
                newValues["QtyFrequency"] = "0,00";
                newValues["TimeExpectedHours"] = "00";
                newValues["TimeExpectedMinutes"] = "00";
                newValues["TimeExpectedSeconds"] = "00";
                newValues["ExecutionSupplier"] = "";
                newValues["ExecutionOperator"] = "";
                newValues["ExecutionSupplierFlag"] = false;
                newValues["ExecutionOperatorFlag"] = true;
                //Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }

        protected void mpTable_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);

            MaintenancePlanRepository mpRep = new MaintenancePlanRepository();
            MaintenancePlan mp = mpRep.FindByID(editableItem.GetDataKeyValue("Id"));

            if (mp != null)
            {
                bool inputCheck = true;

                mp.Description = (editableItem.FindControl("txtDescription") as RadTextBox).Text;

                if ((editableItem.FindControl("txtCreationDate") as RadDatePicker).SelectedDate != null)
                {
                    mp.CreationDate = (DateTime)(editableItem.FindControl("txtCreationDate") as RadDatePicker).SelectedDate;
                    if (!checkDate(mp.CreationDate))
                    {
                        inputCheck = false;
                    }
                }
                else
                {
                    inputCheck = false;
                }

                if ((editableItem.FindControl("txtValidationDate") as RadDatePicker).SelectedDate != null)
                {
                    mp.ValidationDate = (DateTime)(editableItem.FindControl("txtValidationDate") as RadDatePicker).SelectedDate;
                    if (!checkDate(mp.ValidationDate))
                    {
                        inputCheck = false;
                    }
                }
                else
                {
                    inputCheck = false;
                }

                if ((editableItem.FindControl("txtUpdateDate") as RadDatePicker).SelectedDate != null)
                {
                    mp.UpdateDate = (DateTime)(editableItem.FindControl("txtUpdateDate") as RadDatePicker).SelectedDate;
                    if (!checkDate(mp.UpdateDate))
                    {
                        inputCheck = false;
                    }
                }
                else
                {
                    inputCheck = false;
                }

                mp.ApprovalUser = (editableItem.FindControl("txtApprovalUser") as RadTextBox).Text;
                mp.ExecutionOperator = (editableItem.FindControl("txtExecutionOperator") as RadTextBox).Text;
                mp.ExecutionSupplier = (editableItem.FindControl("txtExecutionSupplier") as RadTextBox).Text;
                mp.ExecutionSupplierFlag = (editableItem.FindControl("executionSupplier") as RadButton).Checked;

                mp.QtyFrequencyFlag = (editableItem.FindControl("radioQtyFrequency") as RadButton).Checked;
                if (mp.QtyFrequencyFlag)
                {
                    mp.QtyFrequency = Convert.ToDecimal((editableItem.FindControl("txtQtyFrequency") as RadNumericTextBox).Value);
                }

                if (!mp.QtyFrequencyFlag)
                {
                    string tfHs = (editableItem.FindControl("txtTimeFrequencyHours") as RadNumericTextBox).Text,
                           tfMs = (editableItem.FindControl("txtTimeFrequencyMinutes") as RadNumericTextBox).Text,
                           tfSs = (editableItem.FindControl("txtTimeFrequencySeconds") as RadNumericTextBox).Text;
                    int tfH = Convert.ToInt32((tfHs != "") ? tfHs : "0"),
                        tfM = Convert.ToInt32((tfMs != "") ? tfMs : "0"),
                        tfS = Convert.ToInt32((tfSs != "") ? tfSs : "0");

                    TimeSpan tf = new TimeSpan(0);
                    tf = tf.Add(new TimeSpan(tfH, tfM, tfS));
                    mp.TimeFrequency = tf.Ticks;
                }

                string teHs = (editableItem.FindControl("txtTimeExpectedHours") as RadNumericTextBox).Text,
                       teMs = (editableItem.FindControl("txtTimeExpectedMinutes") as RadNumericTextBox).Text,
                       teSs = (editableItem.FindControl("txtTimeExpectedSeconds") as RadNumericTextBox).Text;
                int teH = Convert.ToInt32((teHs != "") ? teHs : "0"),
                    teM = Convert.ToInt32((teMs != "") ? teMs : "0"),
                    teS = Convert.ToInt32((teSs != "") ? teSs : "0");

                TimeSpan ts = new TimeSpan(0);
                ts = ts.Add(new TimeSpan(teH, teM, teS));
                mp.TimeExpected = ts.Ticks;

                mp.PlanNotes = (editableItem.FindControl("txtPlanNotes") as RadTextBox).Text;

                if (inputCheck)
                {
                    // Aggiorna valori, in tabella e database
                    editableItem.UpdateValues(mp);
                    mpRep.SaveChanges();
                }
                else
                {
                    // Valori inseriti errati, ignora le modifiche
                    errorLabel.Text = "Il formato della data non è corretto.<br>Le modifiche non sono state salvate.";
                    // Wrong date format.<br>The changes had not been saved.
                    dlgPlanErrorDialog.OpenDialog();
                }
            }
        }

        protected void mpTable_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);

            MaintenancePlanRepository mpRep = new MaintenancePlanRepository();
            MaintenancePlan mp = new MaintenancePlan();

            bool inputCheck = true;

            mp.Description = (editableItem.FindControl("txtDescription") as RadTextBox).Text;

            if ((editableItem.FindControl("txtCreationDate") as RadDatePicker).SelectedDate != null)
            {
                mp.CreationDate = (DateTime)(editableItem.FindControl("txtCreationDate") as RadDatePicker).SelectedDate;
                if (!checkDate(mp.CreationDate))
                {
                    inputCheck = false;
                }
            }
            else
            {
                inputCheck = false;
            }

            if ((editableItem.FindControl("txtValidationDate") as RadDatePicker).SelectedDate != null)
            {
                mp.ValidationDate = (DateTime)(editableItem.FindControl("txtValidationDate") as RadDatePicker).SelectedDate;
                if (!checkDate(mp.ValidationDate))
                {
                    inputCheck = false;
                }
            }
            else
            {
                inputCheck = false;
            }

            if ((editableItem.FindControl("txtUpdateDate") as RadDatePicker).SelectedDate != null)
            {
                mp.UpdateDate = (DateTime)(editableItem.FindControl("txtUpdateDate") as RadDatePicker).SelectedDate;
                if (!checkDate(mp.UpdateDate))
                {
                    inputCheck = false;
                }
            }
            else
            {
                inputCheck = false;
            }

            mp.ApprovalUser = (editableItem.FindControl("txtApprovalUser") as RadTextBox).Text;
            mp.ExecutionOperator = (editableItem.FindControl("txtExecutionOperator") as RadTextBox).Text;
            mp.ExecutionSupplier = (editableItem.FindControl("txtExecutionSupplier") as RadTextBox).Text;
            mp.ExecutionSupplierFlag = (editableItem.FindControl("executionSupplier") as RadButton).Checked;

            mp.QtyFrequencyFlag = (editableItem.FindControl("radioQtyFrequency") as RadButton).Checked;
            if (mp.QtyFrequencyFlag)
            {
                mp.QtyFrequency = Convert.ToDecimal((editableItem.FindControl("txtQtyFrequency") as RadNumericTextBox).Value);
            }
            else
            {
                mp.QtyFrequency = 0;
            }

            if (!mp.QtyFrequencyFlag)
            {
                string tfHs = (editableItem.FindControl("txtTimeFrequencyHours") as RadNumericTextBox).Text,
                       tfMs = (editableItem.FindControl("txtTimeFrequencyMinutes") as RadNumericTextBox).Text,
                       tfSs = (editableItem.FindControl("txtTimeFrequencySeconds") as RadNumericTextBox).Text;
                int tfH = Convert.ToInt32((tfHs != "") ? tfHs : "0"),
                    tfM = Convert.ToInt32((tfMs != "") ? tfMs : "0"),
                    tfS = Convert.ToInt32((tfSs != "") ? tfSs : "0");

                TimeSpan tf = new TimeSpan(0);
                tf = tf.Add(new TimeSpan(tfH, tfM, tfS));
                mp.TimeFrequency = tf.Ticks;
            }
            else
            {
                mp.TimeFrequency = 0;
            }

            string teHs = (editableItem.FindControl("txtTimeExpectedHours") as RadNumericTextBox).Text,
                   teMs = (editableItem.FindControl("txtTimeExpectedMinutes") as RadNumericTextBox).Text,
                   teSs = (editableItem.FindControl("txtTimeExpectedSeconds") as RadNumericTextBox).Text;
            int teH = Convert.ToInt32((teHs != "") ? teHs : "0"),
                teM = Convert.ToInt32((teMs != "") ? teMs : "0"),
                teS = Convert.ToInt32((teSs != "") ? teSs : "0");

            TimeSpan ts = new TimeSpan(0);
            ts = ts.Add(new TimeSpan(teH, teM, teS));
            mp.TimeExpected = ts.Ticks;

            mp.PlanNotes = (editableItem.FindControl("txtPlanNotes") as RadTextBox).Text;
            mp.ApplicationId = ApplicationId;

            if (inputCheck)
            {
                // Salvo il nuovo Piano di manutenzione
                mpRep.Insert(mp);
                mpRep.SaveChanges();

                // Aggiungo il Client Id
                MaintenancePlan mpN = mpRep.FindByID(mp.Id);
                mpN.ClientId = mp.Id;
                mpRep.Update(mpN);
                mpRep.SaveChanges();
            }
            else
            {
                // Valori inseriti errati
                errorLabel.Text = "Il formato della data non è corretto.<br>Le modifiche non sono state salvate.";
                dlgPlanErrorDialog.OpenDialog();
            }
        }

        protected void mpTable_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);

            MaintenanceRepository mRep = new MaintenanceRepository();
            List<Maintenance> mList = mRep.GetAllByMaintenancePlanId(Convert.ToInt32(editableItem.GetDataKeyValue("Id")));

            if (mList.Count == 0)
            {
                MaintenancePlanRepository mpRep = new MaintenancePlanRepository();
                MaintenancePlan mp = mpRep.FindByID(editableItem.GetDataKeyValue("Id"));

                MaintenancePlanDetailRepository mpdRep = new MaintenancePlanDetailRepository();
                List<MaintenancePlanDetail> mpdList = mpdRep.GetAllByMaintenancePlanId(Convert.ToInt32(editableItem.GetDataKeyValue("Id")));

                foreach (MaintenancePlanDetail mpd in mpdList)
                {
                    mpdRep.Delete(mpd);
                }
                mpdRep.SaveChanges();

                mpRep.Delete(mp);
                mpRep.SaveChanges();
            }
            else
            {
                // Errore: Il piano di manutenzione è collegato ad uno Stampo/un Macchinario, l'eliminazione è impossibile
                errorLabel.Text = "Questo piano di manutenzione non può essere eliminato perchè è collegato ad uno Stampo e/o un Macchinario.<br>Rimuovi il riferimento e riprova.";
                dlgPlanErrorDialog.OpenDialog();
            }
        }

        protected void mpdTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            string IdPlan = ((GridNestedViewItem)(sender as RadGrid).Parent.BindingContainer).ParentItem.KeyValues.Replace("{Id:\"", "").Replace("\"}", "");
            MaintenancePlanDetailRepository mpDRep = new MaintenancePlanDetailRepository();
            List<MaintenancePlanDetail> mpDList = mpDRep.GetAllByMaintenancePlanId(Convert.ToInt32(IdPlan));
            (sender as RadGrid).DataSource = mpDList;
        }

        protected void mpdTable_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                //Add new button clicked
                e.Canceled = true;
                //Prepare an IDictionary with the predefined values
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();
                newValues["Description"] = "";
                newValues["Tolerance1Value"] = "0,00";
                newValues["Tolerance2Value"] = "0,00";
                newValues["BaseValue"] = "0,00";
                newValues["ItemCode"] = "";
                //Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }

        protected void mpdTable_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);

            MaintenancePlanDetailRepository mpdRep = new MaintenancePlanDetailRepository();
            MaintenancePlanDetail mpd = mpdRep.FindByID(editableItem.GetDataKeyValue("Id"));

            if (mpd != null)
            {
                mpd.Description = (editableItem.FindControl("txtDetailDescription") as RadTextBox).Text;
                mpd.ItemCode = (editableItem.FindControl("txtDetailItemCode") as RadTextBox).Text;
                mpd.ToolCode = (editableItem.FindControl("txtDetailToolCode") as RadTextBox).Text;
                mpd.DetailNotes = (editableItem.FindControl("txtDetailDetailNotes") as RadTextBox).Text;

                editableItem.UpdateValues(mpd);
                mpdRep.SaveChanges();
            }
        }

        protected void mpdTable_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            string IdPlan = ((GridNestedViewItem)(sender as RadGrid).Parent.BindingContainer).ParentItem.KeyValues.Replace("{Id:\"", "").Replace("\"}", "");

            MaintenancePlanDetailRepository mpdRep = new MaintenancePlanDetailRepository();
            MaintenancePlanDetail mpd = new MaintenancePlanDetail();

            mpd.MaintenancePlanId = Convert.ToInt32(IdPlan);
            mpd.Description = (editableItem.FindControl("txtDetailDescription") as RadTextBox).Text;
            mpd.ItemCode = (editableItem.FindControl("txtDetailItemCode") as RadTextBox).Text;
            mpd.ToolCode = (editableItem.FindControl("txtDetailToolCode") as RadTextBox).Text;
            mpd.DetailNotes = (editableItem.FindControl("txtDetailDetailNotes") as RadTextBox).Text;
            mpd.ApplicationId = ApplicationId;

            mpdRep.Insert(mpd);
            mpdRep.SaveChanges();

            // Aggiungo il Client Id
            MaintenancePlanDetail mpdN = mpdRep.FindByID(mpd.Id);
            mpdN.ClientId = mpd.Id;
            mpdRep.Update(mpdN);
            mpdRep.SaveChanges();
        }

        protected void mpdTable_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);

            MaintenancePlanDetailRepository mpdRep = new MaintenancePlanDetailRepository();
            MaintenancePlanDetail mpd = mpdRep.FindByID(editableItem.GetDataKeyValue("Id"));

            mpdRep.Delete(mpd);
            mpdRep.SaveChanges();
        }

        public bool checkDate(DateTime date)
        {
            return !(date < DateTime.MinValue || date > DateTime.MaxValue);
        }

        protected void dlgPlanError_Ok_Click(object sender, EventArgs e)
        {
            dlgPlanErrorDialog.CloseDialog();
        }
    }
}
