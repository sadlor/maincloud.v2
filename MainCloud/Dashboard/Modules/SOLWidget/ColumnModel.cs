﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.SOLWidget
{
    [Serializable]
    public class ColumnModel
    {
        public ColumnModel()
        {
            UId = new Guid().ToString();
        }
        public String UId { get; }
        public Type ColumnType { get; set; }
        public string HeaderText { get; set; }
        public string DataField { get; set; }
    }
}