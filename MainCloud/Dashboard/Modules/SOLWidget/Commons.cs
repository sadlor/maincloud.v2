﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.SOLWidget
{
    public class Commons
    {
        public const string DYNAMIC_TABLE_ENTITY_TYPE = "dynamic_table_entity_type";
        public const string DYNAMIC_TABLE_FIELDS_NAME = "dynamic_table_fields_name";
    }
}