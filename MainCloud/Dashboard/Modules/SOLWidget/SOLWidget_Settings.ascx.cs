﻿using MainCloudFramework.UI.Modules;
using MainCloudFramework.UI.Utilities;
using SOLModule.Services;
using SOLModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace MainCloud.Dashboard.Modules.SOLWidget
{
    public partial class SOLWidget_Settings : WidgetSetting
    {
        const string FILTER_EXPRESSION = "FilterExpression";
        const string FILTER_EDITORS = "FilterEditors";

        const string SELECTED_GRID_COLUMNS = "SelectedGridColumns";

        public IList<ColumnModel> selectedGridColumns;

        private WeldDbService weldDbSerive;
        protected void Page_Load(object sender, EventArgs e)
        {
            #region SetUp Filtro
            //Filter.ConnectionStringName = "SOLWeldConnection";
            //Filter.EntityName = "Weld";
            Filter.EntityNameType = typeof(SOLModule.Models.Weld);

            selectedGridColumns = ViewState[SELECTED_GRID_COLUMNS] as IList<ColumnModel>;
            if (selectedGridColumns == null)
            {
                selectedGridColumns  = new List<ColumnModel>();
            }
            #endregion
        }
        public override bool ApplyChanges(DataCustomSettings widgetCustomSettings)
        {
            //widgetCustomSettings.AddOrUpdate(FILTER_EDITORS, (Filter.FindControl("Filter") as RadFilter).FieldEditors);
            widgetCustomSettings.AddOrUpdate(FILTER_EXPRESSION, (Filter.FindControl("Filter") as RadFilter).SaveSettings());

            widgetCustomSettings.AddOrUpdate(Commons.DYNAMIC_TABLE_ENTITY_TYPE, ddlEntity.SelectedValue);
            widgetCustomSettings.AddOrUpdate(Commons.DYNAMIC_TABLE_FIELDS_NAME, lstSelectedEntityFields.SelectedValue);

            return true;
        }
        public override void SyncChanges(DataCustomSettings widgetCustomSettings)
        {
            if (!string.IsNullOrEmpty(widgetCustomSettings.Find(FILTER_EXPRESSION) as string))
            {
                (Filter.FindControl("Filter") as RadFilter).LoadSettings(widgetCustomSettings.Find(FILTER_EXPRESSION).ToString());
            }

            #region Setup entity
            //if (!IsPostBack) {

            weldDbSerive = new WeldDbService();
            ddlEntity.DataSource = weldDbSerive.entitiesType;
            ddlEntity.DataBind();

            string dynEntityType = widgetCustomSettings.Find(Commons.DYNAMIC_TABLE_ENTITY_TYPE) as string;
            if (!string.IsNullOrEmpty(dynEntityType))
            {
                ddlEntity.SelectedValue = dynEntityType;
            }

            ViewState.Add(SELECTED_GRID_COLUMNS, selectedGridColumns);
            loadEntityProperty(ddlEntity.SelectedItem.Value);
            lstSelectedEntityFields.DataSource = selectedGridColumns;
            lstSelectedEntityFields.DataBind();

            JArray dynFieldsName = widgetCustomSettings.Find(Commons.DYNAMIC_TABLE_FIELDS_NAME) as JArray;
            /*if (dynFieldsName != null && dynFieldsName.Count > 0)
            {
                var options = dynFieldsName.Select(jv => (string)jv).ToArray();
                chklstEntityFields.AppendDataBoundItems = true;
                foreach(ButtonListItem item in chklstEntityFields.Items)
                {
                    item.Selected = options.Contains(item.Value);
                }
            }*/

            //}
            #endregion
        }
        private void loadEntityProperty(string value)
        {
            Type t = Type.GetType(value);
            // Get the public properties.
            PropertyInfo[] propInfos = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            ddlEntityFields.DataSource = propInfos;
            ddlEntityFields.DataBind();
        }
        protected void btnAddEntityField_Click(object sender, EventArgs e)
        {
            var col = new ColumnModel()
            {
                ColumnType = typeof(GridBoundColumn),
                HeaderText = ddlEntityFields.SelectedItem.Value,
                DataField = ddlEntityFields.SelectedItem.Value
            };
            selectedGridColumns.Add(col);
            lstSelectedEntityFields.DataSource = selectedGridColumns;
            lstSelectedEntityFields.DataBind();
//            ddlEntityFields.Items.Remove(ddlEntityFields.SelectedItem);
        }
        protected void lstSelectedEntityFields_SelectedIndexChanged(object sender, EventArgs e)
        {
            var idx = lstSelectedEntityFields.SelectedIndex;
            var item = selectedGridColumns[idx];
            var l = new List<ColumnModel>();
            l.Add(item);
            columnPropertyEditor.DataSource = l;
            columnPropertyEditor.DataBind();
        }

        /// <summary> 
        /// Edit button click 
        /// </summary> 
        /// <param name="sender"></param> 
        /// <param name="e"></param> 
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (lstSelectedEntityFields.SelectedIndex != -1)
            {
                columnPropertyEditor.ChangeMode(DetailsViewMode.Edit);

                //Bind the DetailsView with data 
                var idx = lstSelectedEntityFields.SelectedIndex;
                var item = selectedGridColumns[idx];
                var l = new List<ColumnModel>();
                l.Add(item);
                columnPropertyEditor.DataSource = l;
                columnPropertyEditor.DataBind();
                btnEdit.Visible = false;
                btnCancel.Visible = btnUpdate.Visible = true;
            }
        }

        /// <summary> 
        /// Cancel button click 
        /// </summary> 
        /// <param name="sender"></param> 
        /// <param name="e"></param> 
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            PropertyEditorExitEditMode();
        }
        /// <summary> 
        /// Update button click event 
        /// </summary> 
        /// <param name="sender"></param> 
        /// <param name="e"></param> 
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //http://msdn.microsoft.com/en-us/library/system.web.ui.webcontrols.detailsview.updateitem.aspx 
            columnPropertyEditor.UpdateItem(true);
            PropertyEditorExitEditMode();
        }
        private void PropertyEditorExitEditMode()
        {
            columnPropertyEditor.ChangeMode(DetailsViewMode.ReadOnly);
            //Bind the DetailsView with data 
            var idx = lstSelectedEntityFields.SelectedIndex;
            var item = selectedGridColumns[idx];
            var l = new List<ColumnModel>();
            l.Add(item);
            columnPropertyEditor.DataSource = l;
            columnPropertyEditor.DataBind();

            btnEdit.Visible = true;
            btnCancel.Visible = btnUpdate.Visible = false;
        }
        protected void columnPropertyEditor_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            var idx = lstSelectedEntityFields.SelectedIndex;
            ColumnModel item = selectedGridColumns[idx];
            var cmType = item.GetType();
            foreach (DictionaryEntry entry in e.NewValues)
            {
                cmType.GetProperty((string)entry.Key).SetValue(item, entry.Value);
            }
        }

        protected void ddlEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadEntityProperty(ddlEntity.SelectedValue);
        }
    }

}