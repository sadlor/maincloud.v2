﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SOLWidget_Settings.ascx.cs" Inherits="MainCloud.Dashboard.Modules.SOLWidget.SOLWidget_Settings" %>
<%@ Register Src="~/Dashboard/Controls/Filters.ascx" TagPrefix="mcf" TagName="Filter" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <asp:DropDownList ID="ddlEntity" runat="server" DataTextField="Name" DataValueField="AssemblyQualifiedName"
                            OnSelectedIndexChanged="ddlEntity_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>        
                        <asp:DropDownList ID="ddlEntityFields" runat="server" DataTextField="Name" DataValueField="Name"></asp:DropDownList>
                        <asp:LinkButton ID="btnAddEntityField" runat="server" OnClick="btnAddEntityField_Click">+ Add</asp:LinkButton>
                    </div>
                    <div class="col-lg-12">
                        <telerik:RadListBox RenderMode="Lightweight" runat="server" ID="lstSelectedEntityFields" Height="200px" Width="230px"
                            ButtonSettings-AreaWidth="35px" AllowDelete="true" AllowReorder="true" DataTextField="HeaderText" DataValueField="DataField" 
                            OnSelectedIndexChanged="lstSelectedEntityFields_SelectedIndexChanged" AutoPostBack="true">
                        </telerik:RadListBox>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <asp:Button ID="btnEdit" runat="server" Text="Edit" onclick="btnEdit_Click" />  
                <asp:Button ID="btnUpdate" runat="server" Text="Update" onclick="btnUpdate_Click" Visible="false" />  
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click" Visible="false" /> 
                <br /><br />

                <asp:DetailsView ID="columnPropertyEditor" runat="server"
                     AutoGenerateRows="false" DataKeyNames="UId" Width="100%"
                    onitemupdating="columnPropertyEditor_ItemUpdating"> 
                     <Fields> 
                        <asp:BoundField HeaderText="HeaderText" DataField="HeaderText" /> 
                        <asp:BoundField HeaderText="DataField" DataField="DataField" ReadOnly="true" /> 
                     </Fields> 
                </asp:DetailsView>

            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<br />
<br />
<br />
<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <mcf:Filter runat="server" ID="Filter" ConnectionStringName="SOLWeldConnection" EntityName="Weld"></mcf:Filter>
    </ContentTemplate>
</asp:UpdatePanel>