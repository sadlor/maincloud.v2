﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SOLWidget_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.SOLWidget.SOLWidget_View" %>
<%@ Register Src="~/Dashboard/Controls/Filters.ascx" TagPrefix="mcf" TagName="Filter" %>

 <link href="Style.css" rel="stylesheet" type="text/css" />

<%--<asp:Content ID="FilterContent" ContentPlaceHolderID="CphFilter" runat="server">
</asp:Content>--%>

<%--<asp:Content ID="SubMenuContent" ContentPlaceHolderID="MainContent" runat="Server">
</asp:Content>--%>

<mcf:Filter runat="server" ID="Filter" ConnectionStringName="SOLWeldConnection" EntityName="Job"></mcf:Filter>    


<telerik:RadGrid ID="dynTable" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="false">
    <MasterTableView FilterExpression=""></MasterTableView>
</telerik:RadGrid>

<%--<telerik:RadGrid ID="table" runat="server" RenderMode="Lightweight" AutoGenerateColumns="true">
    <MasterTableView FilterExpression=""></MasterTableView>
</telerik:RadGrid>--%>

<telerik:RadGrid ID="JobGrid" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="true"
    OnNeedDataSource="JobGrid_NeedDataSource" OnItemCommand="JobGrid_ItemCommand"
    AllowFilteringByColumn="false" AllowMultiRowSelection="false">
    <ClientSettings>
        <Selecting AllowRowSelect="true" UseClientSelectColumnOnly="true" />
    </ClientSettings>
    <MasterTableView Caption="Production info : Job" FilterExpression="" DataKeyNames="Id">
        <Columns>
            <telerik:GridTemplateColumn UniqueName="SelectCol">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBoxSelect" runat="server" OnCheckedChanged="ToggleRowSelection"
                        AutoPostBack="True" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridEditCommandColumn>
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
            </telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn HeaderText="Ref. number" DataField="JobName">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
            </telerik:GridBoundColumn>
            <telerik:GridDropDownColumn HeaderText="Operator" UniqueName="ddcOperator" DataField="OperatorId"
                DataSourceID="edsOperator" ListTextField="Name" ListValueField="Id" DropDownControlType="RadComboBox">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="Machine" UniqueName="ddcMachine" DataField="AssetId"
                DataSourceID="edsMachine" ListTextField="Description" ListValueField="Id" DropDownControlType="RadComboBox">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="Equipment" UniqueName="ddcEquipment" DataField="EquipmentId"
                DataSourceID="edsEquipment" ListTextField="EquipName" ListValueField="Id" DropDownControlType="RadComboBox">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn HeaderText="WPS" DataField="WPSId">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<br />

<telerik:RadGrid ID="WeldGrid" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
    OnItemDataBound="WeldGrid_ItemDataBound">
    <MasterTableView Caption="Data : List Weld" FilterExpression="">
        <SortExpressions>
            <telerik:GridSortExpression FieldName="Ref" SortOrder="Descending" />
        </SortExpressions>
        <Columns>
            <telerik:GridBoundColumn HeaderText="Ref N°" DataField="Ref">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridDateTimeColumn HeaderText="Date" DataField="Date">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn HeaderText="Current [A]" DataField="Current" UniqueName="Current">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Voltage [V]" DataField="Volt" UniqueName="Voltage">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Wire Speed [m/min]" DataField="WireSpeed" UniqueName="WireSpeed">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Gas Flow [l/min]" DataField="GasFlow" UniqueName="GasFlow">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Temp [°C]" DataField="Temp">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Weld Time [s]" DataField="WeldTime">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Power [kW]" DataField="Energy">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Heat Input [kJ/mm]" DataField="HeatInput">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Weld Length [mm]" DataField="WeldLength">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="WPS" DataField="WPSId">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<ef:EntityDataSource runat="server" ID="edsOperator" ContextTypeName="SOLModule.Models.WeldDbContext" EntitySetName="Operators">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsMachine" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsEquipment" ContextTypeName="SOLModule.Models.WeldDbContext" EntitySetName="Equipments">
</ef:EntityDataSource>

<br />

<telerik:RadFormDecorator RenderMode="Lightweight" ID="FormDecorator1" runat="server" DecoratedControls="all" DecorationZoneID="decorationZone">
</telerik:RadFormDecorator>
<div class="demo-containers">
    <div class="demo-container" id="decorationZone">
        <h4>Analysis</h4>
        <fieldset>
            <legend>Times</legend>
                <div class="formRow" style="padding-right: 10px; padding-left: 10px;">
                    <div class="row">
                        <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                            <asp:Label Text="Ʃ Job Time" runat="server" AssociatedControlID="txtJobTotTime" />
                            <asp:TextBox runat="server" ReadOnly="true" ID="txtJobTotTime" TextMode="Time" />
                        </div>
                        <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                            <asp:Label Text="Weld Time" runat="server" AssociatedControlID="txtJobWeldTime" />
                            <asp:TextBox runat="server" ReadOnly="true" ID="txtJobWeldTime" TextMode="Time" />
                        </div>
                        <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                            <asp:Label Text="% Weld Time" runat="server" AssociatedControlID="txtPercWeldTime" />
                            <asp:TextBox runat="server" ReadOnly="true" ID="txtPercWeldTime" Width="100%" />
                        </div>
                        <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                            <asp:Label Text="Stop Time" runat="server" AssociatedControlID="txtJobStopTime" />
                            <asp:TextBox runat="server" ReadOnly="true" ID="txtJobStopTime" TextMode="Time" />
                        </div>
                        <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                            <asp:Label Text="% Stop Time" runat="server" AssociatedControlID="txtPercStopTime" />
                            <asp:TextBox runat="server" ReadOnly="true" ID="txtPercStopTime" />
                        </div>
                    </div>
                    <div class="row">
                        <%--<div class="col-xs-6 col-sm-1 col-md-2 col-lg-2">
                            <asp:TextBox runat="server" ReadOnly="true" ID="txtJobTotTime" TextMode="Time" />
                        </div>--%>
                        <%--<div class="col-xs-6 col-sm-1 col-md-2 col-lg-2">
                            <asp:TextBox runat="server" ReadOnly="true" ID="txtJobWeldTime" TextMode="Time" />
                        </div>--%>
                        <%--<div class="col-xs-6 col-sm-1 col-md-2 col-lg-2">
                            <asp:TextBox runat="server" ReadOnly="true" ID="txtPercWeldTime" />
                        </div>--%>
                        <%--<div class="col-xs-6 col-sm-1 col-md-2 col-lg-2">
                            <asp:TextBox runat="server" ReadOnly="true" ID="txtJobStopTime" TextMode="Time" />
                        </div>--%>
                        <%--<div class="col-xs-6 col-sm-1 col-md-2 col-lg-2">
                            <asp:TextBox runat="server" ReadOnly="true" ID="txtPercStopTime" TextMode="Number" />
                        </div>--%>
                    </div>
                </div>
        </fieldset>
    </div>
</div>