﻿using MainCloud.Dashboard.Controls;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using Newtonsoft.Json.Linq;
using SOLModule.Models;
using SOLModule.Repositories;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.SOLWidget
{
    public partial class SOLWidget_View : DataWidget<SOLWidget_View>
    {
        public SOLWidget_View() : base(typeof(SOLWidget_View)) { }

        const string DYNAMIC_TABLE_ENTITY_TYPE = "dynamic_table_entity_type";
        const string DYNAMIC_TABLE_FIELDS_NAME = "dynamic_table_fields_name";
        protected void Page_Load(object sender, EventArgs e)
        {
            #region SetUp Filtro
            //accetta come parametri: 
            //ConnectionStringName: stringa di connessione al DB contenente la tabella
            //EntityName: tabella DB per selezione campi su cui eseguire il filtro
            //EntityNameType: typeof dell'entità
            //FilterContainerId: Id dell'oggetto su cui viene applicato il filtro

            //Filter.ConnectionStringName = "SOLWeldConnection";
            //Filter.EntityName = "Weld";
            Filter.EntityNameType = typeof(SOLModule.Models.Job);
            Filter.FilterContainerId = JobGrid.ID;
            #endregion

            #region per filtri in settings
            //if (!string.IsNullOrEmpty(CustomSettings.Find("FilterExpression") as string))
            //{
            //    Filters f = new Filters();
            //    f.ConnectionStringName = "SOLWeldConnection";
            //    f.EntityName = "Weld";
            //    f.EntityNameType = typeof(SOLModule.Models.Weld);
            //    f.FilterContainerId = table.ID;
            //    (f.FindControl("Filter") as RadFilter).LoadSettings((string)CustomSettings.Find("FilterExpression"));
            //    (f.FindControl("Filter") as RadFilter).FireApplyCommand();

            //RadFilter filter = new RadFilter();
            //foreach (var item in CustomSettings.Find("FilterEditors") as RadFilterDataFieldEditorCollection)
            //{
            //    filter.FieldEditors.Add(item);
            //}
            //filter.LoadSettings((string)CustomSettings.Find("FilterExpression"));
            //filter.FilterContainerID = table.ID;
            //filter.FireApplyCommand();
            //}
            #endregion

            //List<Weld> dataSource = new List<Weld>();
            //using (SOLModule.Models.WeldDbContext db = new WeldDbContext())
            //{
            //    dataSource.AddRange(db.Welds.ToList());
            //}
            //table.DataSource = dataSource;

            if (!IsPostBack)
            {
                WeldGrid.Visible = false;

                string table = CustomSettings.Find(DYNAMIC_TABLE_ENTITY_TYPE) as string;
                if (!String.IsNullOrEmpty(table))
                {
                    Type t = Type.GetType(table);
                    WeldDbContext db = new WeldDbContext();
                    var s = db.Set<Weld>();
                    dynTable.DataSource = s.ToList();

                    JArray dynFieldsName = CustomSettings.Find(DYNAMIC_TABLE_FIELDS_NAME) as JArray;
                    if (dynFieldsName != null && dynFieldsName.Count > 0)
                    {
                        var options = dynFieldsName.Select(jv => (string)jv).ToArray();
                        foreach (var o in options)
                        {
                            var c = new GridBoundColumn();
                            c.DataField = o;
                            c.HeaderText = o;
                            dynTable.Columns.Add(c);
                        }
                        var cc = new GridCalculatedColumn();
                        cc.UniqueName = "Tot";
                        cc.DataFields = new string[] { "WeldTime", "Energy" };
                        cc.Expression = "{0}*{1}";
                        dynTable.Columns.Add(cc);
                        dynTable.DataBind();
                    }
                }
            }


        }

        //protected void WeldGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        //{
        //    WeldRepository repos = new WeldRepository();
        //    List<Weld> dataSource = new List<Weld>();
        //    dataSource = repos.FindAll().ToList();
        //    WeldGrid.DataSource = dataSource;
        //}

        protected void WeldGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            // change color:
            // WARNING -> orange
            // ERROR -> red
            const string ERR = "ERR";
            const string WRNG = "WRNG";

            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem dataBoundItem = e.Item as GridDataItem;
                Weld row = dataBoundItem.DataItem as Weld;
                if (row.Current_WPS == ERR)
                {
                    dataBoundItem["Current"].BackColor = Color.Red; // change particular cell
                    dataBoundItem["Current"].ForeColor = Color.White;
                }
                if (row.Current_WPS == WRNG)
                {
                    dataBoundItem["Current"].BackColor = Color.Orange;
                    dataBoundItem["Current"].ForeColor = Color.Black;
                }
                if (row.GasFlow_WPS == ERR)
                {
                    dataBoundItem["GasFlow"].BackColor = Color.Red;
                    dataBoundItem["GasFlow"].ForeColor = Color.White;
                }
                if (row.GasFlow_WPS == WRNG)
                {
                    dataBoundItem["GasFlow"].BackColor = Color.Orange;
                    dataBoundItem["GasFlow"].ForeColor = Color.Black;
                }
                if (row.Volt_WPS == ERR)
                {
                    dataBoundItem["Voltage"].BackColor = Color.Red;
                    dataBoundItem["Voltage"].ForeColor = Color.White;
                }
                if (row.Volt_WPS == WRNG)
                {
                    dataBoundItem["Voltage"].BackColor = Color.Orange;
                    dataBoundItem["Voltage"].ForeColor = Color.Black;
                }
                if (row.WireSpeed_WPS == ERR)
                {
                    dataBoundItem["WireSpeed"].BackColor = Color.Red;
                    dataBoundItem["WireSpeed"].ForeColor = Color.White;
                }
                if (row.WireSpeed_WPS == WRNG)
                {
                    dataBoundItem["WireSpeed"].BackColor = Color.Orange;
                    dataBoundItem["WireSpeed"].ForeColor = Color.Black;
                }

                //e.Item.BackColor = Color.LightGoldenrodYellow; // for whole row
            }
        }

        protected void JobGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            JobRepository repos = new JobRepository();
            List<Job> dataSource = new List<Job>();
            dataSource = repos.FindAll().ToList();
            JobGrid.DataSource = dataSource;
        }

        protected void JobGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.SelectCommandName)
            {
                GridDataItem item = e.Item as GridDataItem;
                string jobId = item.KeyValues.Split(':')[1].Replace("\"", "").Replace("}", "");
                if (!string.IsNullOrEmpty(jobId))
                {
                    WeldRepository repos = new WeldRepository();
                    List<Weld> dataSource = new List<Weld>();
                    dataSource = repos.ReadAll(x => x.JobId == jobId).ToList();
                    WeldGrid.DataSource = dataSource;
                    WeldGrid.Visible = false;
                    WeldGrid.DataBind();

                    Analisi(dataSource);
                }
            }

            //if (e.CommandName == RadGrid.FilterCommandName)
            //{
            //    Pair command = (Pair)e.CommandArgument;
            //    if (command.Second.ToString() == "ddcOperator")
            //    {
            //        e.Canceled = true;
            //        GridFilteringItem filter = (GridFilteringItem)e.Item;
            //        ((filter["OperatorName"].Controls[0]) as TextBox).Text = ((filter["ddcOperator"].Controls[0]) as TextBox).Text;
            //        command.Second = "OperatorName";
            //        filter.FireCommandEvent("Filter", new Pair(command.First, "Operator.Name"));
            //    }
            //}
        }

        //protected void JobGrid_ItemDataBound(object sender, GridItemEventArgs e)
        //{
        //    if (e.Item is GridFilteringItem)
        //    {
        //        GridFilteringItem item = e.Item as GridFilteringItem;
        //        ((item["ddcOperator"].Controls[0]) as TextBox).Text = ((item["OperatorName"].Controls[0]) as TextBox).Text;
        //    }
        //}

        protected void ToggleRowSelection(object sender, EventArgs e)
        {
            GridDataItem parent = (sender as CheckBox).NamingContainer as GridDataItem;
            parent.Selected = (sender as CheckBox).Checked;
            //((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
            foreach (GridDataItem dataItem in JobGrid.MasterTableView.Items)
            {
                if ((dataItem.FindControl("CheckBoxSelect") as CheckBox).Checked && dataItem != parent)
                {
                    (dataItem.FindControl("CheckBoxSelect") as CheckBox).Checked = false;
                    dataItem.Selected = false;
                }
            }

            ((sender as CheckBox).NamingContainer as GridDataItem).FireCommandEvent("Select", new GridSelectCommandEventArgs((GridDataItem)(sender as CheckBox).NamingContainer, null, null));
        }

        public void Analisi(List<Weld> dataSource)
        {
            //DataSource -> JobList
            //tempo effettivo di saldatura -> somma weld time per job
            //tempo totale job -> Data fine(order by descending DateTime .First) - Data inizio(order by Date .First)
            //tempo di fermo -> Data inizio - data fine precedente

            txtJobTotTime.Text = "";
            txtJobWeldTime.Text = "";
            txtPercWeldTime.Text = "";
            txtJobStopTime.Text = "";
            txtPercStopTime.Text = "";

            if (dataSource.Count > 0)
            {
                txtJobTotTime.Text = string.Format(@"{0:hh\:mm\:ss}", (dataSource.OrderByDescending(x => x.DateEnd).First().DateEnd - dataSource.OrderBy(x => x.Date).First().Date));
                txtJobWeldTime.Text = string.Format(@"{0:hh\:mm\:ss}", TimeSpan.FromSeconds((double)dataSource.Sum(x => x.WeldTime).Value));
                txtPercWeldTime.Text = Math.Round(
                    TimeSpan.FromSeconds((double)dataSource.Sum(x => x.WeldTime).Value).TotalSeconds /
                    (dataSource.OrderByDescending(x => x.DateEnd).First().DateEnd - dataSource.OrderBy(x => x.Date).First().Date).TotalSeconds
                     * 100, 2).ToString() + " %";

                TimeSpan stopTime = new TimeSpan(0, 0, 0);
                DateTime finePrec = dataSource.Min(x => x.Date);
                foreach (var item in dataSource.OrderBy(x => x.Date))
                {
                    stopTime = stopTime + (item.Date - finePrec);
                    finePrec = item.DateEnd;
                }
                txtJobStopTime.Text = string.Format(@"{0:hh\:mm\:ss}", stopTime);
                txtPercStopTime.Text = Math.Round(
                    stopTime.TotalSeconds /
                    (dataSource.OrderByDescending(x => x.DateEnd).First().DateEnd - dataSource.OrderBy(x => x.Date).First().Date).TotalSeconds
                    * 100, 2).ToString() + " %";
            }
        }
    }
}