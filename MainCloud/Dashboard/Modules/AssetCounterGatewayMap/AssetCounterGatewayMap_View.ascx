﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssetCounterGatewayMap_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.AssetCounterGatewayMap.AssetCounterGatewayMap_View" %>
<%@ Register Src="MCFDepartmentViewMap.ascx" TagPrefix="mcf" TagName="MCFDepartmentViewMap" %>

<style>
    .RadMap {
        outline: 0;
    }
 
    .RadMap_Glow.k-widget.k-tooltip {
        border: 1px solid #2E3940;
    }
 
    div.RadMap_Glow {
        border: 1px solid #51636D;
        background: #A3CDD6;
    }
 
    div.RadMap_Glow .k-widget, .RadMap_Glow .k-header {
        background: #29343b;
    }
 
    .markers {
        padding: 9px;
        color: #dddddd;
        text-align: center;
    }
 
        .markers hr {
            border: 0;
            border-bottom: 1px solid #49545A;
            color: #1A1F22;
            background-color: #1A1F22;
            height: 1px;
            width: 200px;
        }
 
        .markers .title {
            padding-top: 3px;
            text-align: center;
            text-transform: uppercase;
        }
 
        .markers .details {
            font-size: 11px;
        }
 
        .markers img {
            border: 1px solid #51636D;
            border-bottom: none;
        }
 
    .radMapWrapper {
        padding: 0 35px 35px 35px;
        background-color: #29343B;
    }
 
    .demo-container .mapTitle {
        background: #29343B;
        color: #ffffff;
        font-family: 'Segoe UI',Segoe,'Roboto','Droid Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-size: 20px;
        font-weight: lighter;
        text-align: center;
        text-transform: uppercase;
        line-height: 59px;
    }
    .thumbPlant {
        width: 90%;
    }
</style>
<script type="text/javascript">
    function OnLoad(sender, args) {
        var $ = $telerik.$;
        var kendoMap = sender.get_kendoWidget();
        var Extent = kendo.dataviz.map.Extent;

        // Get the Markers collection
        var markers = kendoMap.markers.items;
        var markerLocations = [];

        // Extract the markers' locations.
        for (var i = 0; i < markers.length; i++) {
            markerLocations.push(markers[i].location());
        }

        // Create an extent based on the first marker
        var myExtent = Extent.create(markerLocations[0], markerLocations[0]);

        // Extend the extent with all markers
        myExtent.includeAll(markerLocations);

        // Center RadMap based on the created extent.
        kendoMap.extent(myExtent);

        // You may need to zoom out to show all markers properly. 
        // This can be furtehr adjusted based on your own preferences.
        kendoMap.zoom(kendoMap.zoom() - 2)
    }
</script>

<telerik:RadMap RenderMode="Lightweight" runat="server" ID="RadMapPlants" OnItemDataBound="RadMapPlants_ItemDataBound" Skin="Glow">
    <ClientEvents OnLoad="OnLoad" />
    <DataBindings>
        <MarkerBinding DataTitleField="City" DataLocationLatitudeField="Latitude" DataLocationLongitudeField="Longitude" />
    </DataBindings>
    <LayersCollection>
        <telerik:MapLayer Type="Tile" Subdomains="a,b,c" Shape="PinTarget"
            UrlTemplate="http://#= subdomain #.tile.openstreetmap.org/#= zoom #/#= x #/#= y #.png"
            Attribution="&copy; <a href='http://osm.org/copyright' title='OpenStreetMap contributors' target='_blank'>OpenStreetMap contributors</a>."
            >
        </telerik:MapLayer>
    </LayersCollection>
</telerik:RadMap>

<asp:Panel ID="pnlDepartments" runat="server">

    <div style="width:20%; float:left">
        <asp:LinkButton ID="btnGoToMapPlant" runat="server" CssClass="btn btn-default" OnClick="btnGoToMapPlant_Click" Width="100%" style="margin-bottom: 20px;"> << Torna alla mappa </asp:LinkButton>
        <div>
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4">
                    Mappa
                </div>
                <div class="col-md-8">
                    <div class="btn-group" id="toggle_event_map">
	                    <button type="button" class="btn btn-info btn-xs locked_active"><span class="fa fa-eye fa-2" aria-hidden="true"></span> ON</button>
                	    <button type="button" class="btn btn-default btn-xs unlocked_inactive"><span class="fa fa-eye-slash fa-2" aria-hidden="true"></span> OFF</button>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4">
                    Progetto
                </div>
                <div class="col-md-8">
                    <div class="btn-group" id="toggle_event_prj">
	                    <button type="button" class="btn btn-info btn-xs locked_active"><span class="fa fa-eye fa-2" aria-hidden="true"></span> ON</button>
                	    <button type="button" class="btn btn-default btn-xs unlocked_inactive"><span class="fa fa-eye-slash fa-2" aria-hidden="true"></span> OFF</button>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4">
                    Assets
                </div>
                <div class="col-md-8">
                    <div class="btn-group" id="toggle_event_asset">
	                    <button type="button" class="btn btn-info btn-xs locked_active"><span class="fa fa-eye fa-2" aria-hidden="true"></span> ON</button>
                	    <button type="button" class="btn btn-default btn-xs unlocked_inactive"><span class="fa fa-eye-slash fa-2" aria-hidden="true"></span> OFF</button>
                    </div>
                </div>                     
            </div>
        </div>
        <asp:Repeater ID="lstDepartment" runat="server" OnItemDataBound="lstDepartment_ItemDataBound" OnItemCommand="lstDepartment_ItemCommand" EnableViewState="false">
            <ItemTemplate>
                <asp:ImageButton ID="map" runat="server" Width="100%" CommandName="selectDepartment" CommandArgument='<%# Eval("Id") %>'/>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div style="width:74%; float:left; margin-left:3%; margin-right:3%">
        <mcf:MCFDepartmentViewMap runat="server" id="selectedMap" />
    </div>

</asp:Panel>