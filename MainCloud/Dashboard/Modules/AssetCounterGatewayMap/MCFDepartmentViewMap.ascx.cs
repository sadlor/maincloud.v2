﻿using AssetManagement.Core;
using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule.Core;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.UI.Utilities;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web.Multitenants;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.AssetCounterGatewayMap
{
    public partial class MCFDepartmentViewMap : System.Web.UI.UserControl
    {
        private Department departmentItem;

        public string Map { get; private set; }

        private DepartmentService service = new DepartmentService();

        [Description("DepartmentItem"), Category("Data")]
        public string DepartmentItemId
        {
            get
            {
                return ViewState["departmentItemId"] as string;
            }
            set
            {
                ViewState["departmentItemId"] = value;
                if (value != null)
                {
                    departmentItem = service.GetDepartmentById(value);
                }
                else
                {
                    departmentItem = null;
                    viewMap.ImageUrl = null;
                }
            }
        }

        public Plant Plant { get; set; }

        public DataCustomSettings WidgetCustomSettings { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (DepartmentItemId != null)
            {
                departmentItem = service.GetDepartmentById(DepartmentItemId);
            }
        }

        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            if (departmentItem != null)
            {
                if (departmentItem.PathImg != null)
                {
                    viewMap.ImageUrl = MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(departmentItem.PathImg);
                }
                else
                {
                    viewMap.ImageUrl = ResolveUrl(AppRelativeTemplateSourceDirectory + "image/missing-image-640x360.png");
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            var defaultImage = ResolveUrl(AppRelativeTemplateSourceDirectory + "image/missing-image-640x360.png");

            if (departmentItem != null)
            {
                var department = departmentItem;
                Map = MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(department.PathImg);
                viewMap.ImageUrl = MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(department.PathImg);  //Fix load dept image
                Page.Title = department.Code;

                // Load assets
                var assets = department.Asset;
                var objDataUsed = new List<ImagesData>();
                var objDataNotUsed = new List<ImagesData>();
                foreach (var item in assets)
                {
                    var imgData = new ImageDataAsset()
                    {
                        Id = item.Id,
                        Thumb = item.PathImg != null ? MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(item.PathImg) : defaultImage,
                        Title = item.Code,
                        Data = item,
                        InitStyle = calculateStile(item.PositionAndSize),
                        DataObjectType = JsonConvert.SerializeObject(new SelectedContext(item.Id, AssetManagementEnum.AnalyzerContext.Asset))
                    };

                    if (item.PositionAndSize != null)
                    {
                        objDataUsed.Add(imgData);
                    }
                }

                var analyzer = from a in assets
                               where a.Analyzer != null
                               select a.Analyzer;
                foreach (var item in analyzer)
                {
                    var imgData = new ImageDataAnalyzer()
                    {
                        Id = item.Id,
                        Thumb = item.PathImg != null ? MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(item.PathImg) : defaultImage,
                        Title = item.SerialCode,
                        Data = item,
                        InitStyle = calculateStile(item.PositionAndSize)
                    };

                    if (item.PositionAndSize != null)
                    {
                        objDataUsed.Add(imgData);
                    }
                }

                var gateway = from a in analyzer
                              where a.Gateway != null
                              select a.Gateway;
                foreach (var item in gateway)
                {
                    var imgData = new ImageDataGateway()
                    {
                        Id = item.Id,
                        Thumb = item.PathImg != null ? MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(item.PathImg) : defaultImage,
                        Title = item.SerialCode,
                        Data = item,
                        InitStyle = calculateStile(item.PositionAndSize)
                    };

                    if (item.PositionAndSize != null)
                    {
                        objDataUsed.Add(imgData);
                    }
                }
                lvDetail.DataSource = objDataUsed;
                lvDetail.DataBind();
            }
            else if (Plant != null)
            {
                if (Plant.PathImg != null)
                {
                    viewMap.ImageUrl = MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(Plant.PathImg);
                }
                else
                {
                    viewMap.ImageUrl = ResolveUrl(AppRelativeTemplateSourceDirectory + "image/missing-image-640x360.png");
                }
            }
        }

        public HttpMultitenantsSessionState SessionMultitenant
        {
            get { return Session.SessionMultitenant(); }
        }

        public HttpMultitenantsApplicationState ApplicationMultitenant
        {
            get { return Application.ApplicationMultitenant(); }
        }

        private string calculateStile(string positionAndSize)
        {
            string style = "";
            if (positionAndSize != null)
            {
                var posAndSize = JsonConvert.DeserializeObject<JObject>(positionAndSize);
                style = string.Format("left: {0}%; top: {1}%; width: {2}%; height:{3}%; position: absolute; z-index: 1; right: auto; bottom: auto; display: block;",
                    posAndSize["left"], posAndSize["top"], posAndSize["width"], posAndSize["height"]);
            }
            return style;
        }

        protected void lvDetail_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            //            SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT] = e.Node.Value;
            //           SessionMultitenant[EMConstants.START_ACTIVEENERGY_COUNTER] = null;
            //            SessionMultitenant[EMConstants.START_REACTIVEENERGY_COUNTER] = null;

            string returnUrl = Request["returnurl"];
            if (string.IsNullOrEmpty(returnUrl))
            {
                string destinationAreaId = WidgetCustomSettings.Find(Constants.SETTINGS_DESTINATION_AREA_ID) as string;
                if (!string.IsNullOrEmpty(destinationAreaId))
                {
                    WidgetAreaRepository areaService = new WidgetAreaRepository();
                    WidgetArea wa = areaService.FindByID(destinationAreaId);
                    SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT] = e.CommandArgument;
                    SessionMultitenant[EMConstants.START_ACTIVEENERGY_COUNTER] = null;
                    SessionMultitenant[EMConstants.START_REACTIVEENERGY_COUNTER] = null;

                    Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
                }
            }
            else
            {
                Response.Redirect("~" + returnUrl);
            }
        }
    }
}