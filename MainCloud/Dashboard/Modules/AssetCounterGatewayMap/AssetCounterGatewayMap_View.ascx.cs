﻿using AssetManagement.Models;
using AssetManagement.Services;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Map;

namespace MainCloud.Dashboard.Modules.AssetCounterGatewayMap
{
    public partial class AssetCounterGatewayMap_View : WidgetControl<object>, IPostBackEventHandler
    {
        public AssetCounterGatewayMap_View() : base(typeof(AssetCounterGatewayMap_View)) { }

        private string Mode;

        private static string TOOLTIP_TEMPLATE = @"
            <div class='markers'>
                <img src='{1}' class='thumbPlant'>
                <div class='title'> {2}, {3} - {4}</div>
                <div class='details'>
                    <table class='table'>
                        <tr><th>Code:</th><td>{5}</td></tr>
                        <tr><th>Description:</th><td>{6}</td></tr>
                        <tr><th>POD:</th><td>{7}</td></tr>
                        <tr><th>Voltage:</th><td>{8}</td></tr>
                        <tr><th>AnalyzerID:</th><td>{9}</td></tr>
                    </table>
                    <input class='btn btn-default' type='button' id='btnShowDepartments' onclick=""javascript:{0}"" value='Show departments'/>
                </div>               
            </div>
            ";

        private PlantService service = new PlantService();

        protected void Page_Load(object sender, EventArgs e)
        {
            selectedMap.WidgetCustomSettings = CustomSettings;

            if (SessionMultitenant.ContainsKey(UniqueID + "_mode"))
            {
                Mode = (string)SessionMultitenant[UniqueID + "_mode"];
            }
            else
            {
                Mode = Constants.MODE_MAP;
            }

            LoadDepartmentsList();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Mode == Constants.MODE_MAP)
            {
                RadMapPlants.Visible = true;
                pnlDepartments.Visible = false;
                if (!Page.IsPostBack || RadMapPlants.DataSource == null)
                {
                    var plants = service.PlantList().Where(x => x.Longitude != null && x.Latitude != null).ToList();
                    RadMapPlants.DataSource = plants;
                    RadMapPlants.DataBind();
                }
            }

            LoadDepartmentsList();

            SessionMultitenant[UniqueID + "_mode"] = Mode;
        }

        private void LoadDepartmentsList()
        {
            Plant plant = null;
            if (SessionMultitenant.ContainsKey(Constants.PlantIdSessionVarName) && SessionMultitenant[Constants.PlantIdSessionVarName] != null)
            {
                plant = service.GetPlantById(SessionMultitenant[Constants.PlantIdSessionVarName].ToString());
                selectedMap.Plant = plant;
            }

            if (Mode == Constants.MODE_DEPARTMENT)
            {
                RadMapPlants.Visible = false;
                pnlDepartments.Visible = true;

                if (selectedMap.DepartmentItemId == null && plant != null && plant.Department.Count > 0)
                {
                    string depId = SessionMultitenant.ContainsKey(WidgetId + Constants.DepartmentIdSessionVarName) ?
                        SessionMultitenant[WidgetId + Constants.DepartmentIdSessionVarName] as string :
                        null;
                    if (depId != null) {
                        selectedMap.DepartmentItemId = depId;
                    }

                    selectedMap.DataBind();
                }

                lstDepartment.DataSource = plant.Department;
                lstDepartment.DataBind();

                SessionMultitenant[WidgetId + Constants.DepartmentIdSessionVarName] = selectedMap.DepartmentItemId;
            }
        }

        protected void RadMapPlants_ItemDataBound(object sender, MapItemDataBoundEventArgs e)
        {
            MapMarker marker = e.Item as MapMarker;
            if (marker != null)
            {
                Plant item = e.DataItem as Plant;
                string imgUrl = item.PathImg;
                if (!string.IsNullOrEmpty(item.PathImg))
                {
                    imgUrl = ResolveClientUrl(MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(item.PathImg));
                }
                string postBackArgs = Page.ClientScript.GetPostBackEventReference(this, item.Id);

                marker.TooltipSettings.AutoHide = false;
                marker.TooltipSettings.AnimationSettings.OpenSettings.Duration = 300;
                marker.TooltipSettings.AnimationSettings.OpenSettings.Effects = "fade:in";
                marker.TooltipSettings.AnimationSettings.CloseSettings.Duration = 200;
                marker.TooltipSettings.AnimationSettings.CloseSettings.Effects = "fade:out";
                marker.TooltipSettings.Width = 300;
                marker.TooltipSettings.Content = string.Format(TOOLTIP_TEMPLATE, postBackArgs, imgUrl, item.Address, item.City, item.Country, item.Code, item.Description, item.POD, item.Voltage, item.AnalyzerId);
            }
        }

        protected void btnGoToMapPlant_Click(object sender, EventArgs e)
        {
            SessionMultitenant[Constants.PlantIdSessionVarName] = null;
            SessionMultitenant[WidgetId + Constants.DepartmentIdSessionVarName] = null;
            selectedMap.DepartmentItemId = null;
            Mode = Constants.MODE_MAP;
        }

        /// <summary>
        /// Solleva questo evento se viene selezionato un impianto (sites alias plant)
        /// </summary>
        /// <param name="eventArgument"></param>
        public void RaisePostBackEvent(string plantId)
        {
            if (plantId != null)
            {
                SessionMultitenant[Constants.PlantIdSessionVarName] = plantId; // parameter
                Mode = Constants.MODE_DEPARTMENT;
            }
        }

        protected void lstDepartment_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Department DepartmentItem = e.Item.DataItem as Department;
            ImageButton btn = e.Item.FindControl("map") as ImageButton;
            if (DepartmentItem != null)
            {
                if (DepartmentItem.PathImg != null)
                {
                    btn.ImageUrl = MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(DepartmentItem.PathImg);
                }
                else
                {
                    btn.ImageUrl = ResolveUrl(AppRelativeTemplateSourceDirectory + "image/missing-image-640x360.png");
                }
                
                if (selectedMap.DepartmentItemId == DepartmentItem.Id)
                {
                    btn.Style.Add("border", "solid blue");
                }
            }
        }

        protected void lstDepartment_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //DepartmentService service = new DepartmentService();
            selectedMap.DepartmentItemId = e.CommandArgument.ToString();
        }
    }
}