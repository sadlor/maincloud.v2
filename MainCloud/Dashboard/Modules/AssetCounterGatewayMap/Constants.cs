﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.AssetCounterGatewayMap
{
    public class Constants
    {
        public static string PlantIdSessionVarName = "PlantId";
        public static string DepartmentIdSessionVarName = "DepartmentId";

        public static string MODE_MAP = "map";
        public static string MODE_DEPARTMENT = "department";

        public static string SETTINGS_DESTINATION_AREA_ID = "settings-destination-area-id";
    }
}