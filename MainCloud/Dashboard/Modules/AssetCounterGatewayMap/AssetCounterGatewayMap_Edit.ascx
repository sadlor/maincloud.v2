﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssetCounterGatewayMap_Edit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.AssetCounterGatewayMap.AssetCounterGatewayMap_Edit" %>

<style>
    .asset, .analyzer, .gateway {
      /*background: #00e;*/
      color: #fff;
      display: block;
      float: left;
      line-height: 20px;
      text-align: center;
    }
    .asset {
        z-index: 100;
    } 
    .analyzer, .gateway {
        z-index: 200;
    }

    .asset .label {
        background-color: blue !important;
    }

    .analyzer .label, .gateway .label {
        background-color: red !important;
    }

    .droppable_plant {
      /*bottom: 68px;*/
      left: 0;
      position: absolute;
      right: 0;
      top: 0;
    }

    .asset-list {
      background: #ccc;
      bottom: 0;
      left: 0;
      padding: 4px;
      right: 0;
      display: inline-block;
      width: 100%;
      min-height: 400px;
    }

    .asset-list .asset, .asset-list .analyzer, .asset-list .gateway {
      margin: 0 4px 0 0;
    }

    .asset .img, .analyzer .img, .gateway .img {
        width: 100%;
    }
</style>

<script>
    $(function () {

        function saveAssetCoords(event, ui, x, y, reset) {
            var c = $("#droppable_plant");
            var $this = ui.helper ? ui.helper : ui.item; // o draggable??
            var id = $this.data('id');
            var data = reset ? null : JSON.stringify({
                left: (x / c.width() * 100).toFixed(2),
                top: (y / c.height() * 100).toFixed(2),
                width: ($this.width() / c.width() * 100).toFixed(2),
                height: ($this.height() / c.height() * 100).toFixed(2)
            });
            var type = null;
            if ($this.hasClass("asset")) {
                type = "asset";
            }
            if ($this.hasClass("analyzer")) {
                type = "analyzer";
            }
            if ($this.hasClass("gateway")) {
                type = "gateway";
            }

            if (id && type != null) {
                var request = $.ajax({
                    url: '<%= MainCloudFramework.Web.Helpers.MultiTenantsHelper.MountAndResolveMultiTenantUrl("/Dashboard/Modules/AssetCounterGatewayMap/MapAssetsService.svc/SaveMapPositionAndSize") %>',
                    method: 'GET',
                    data: { 'id': id, 'type': type, 'positionAndSize': data },
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                });
    /*
                request.done(function (response) {  //string
                    if (response.d != "") {
                        $find(CounterId).set_displayValue(response.d + " " + measureUnit);
                    }
                });

    */ 
                request.fail(function (jqXHR, textStatus) {
                    alert("Request failed: " + textStatus);
                });
            }
        }

        var gameContainer = $(".row");

        var myTray = $("#assetlist").sortable({
            containment: gameContainer,
            helper: "clone",
            revert: 100,
            tolerance: "intersect",
            update: function (ev, ui) {
                ui.item.addClass("onassetlist").css({
                    "left": "0px",
                    "position": "static",
                    "top": "0px"
                });
                ui.item.removeAttr('style');
                ui.item.resizable('destroy');
                saveAssetCoords(event, ui, null, null, true);
            }
        }).disableSelection();

        var resizeOpts = {
            handles: "se",
//            autoHide: true,
//            minWidth: 100,
            aspectRatio: true,
            containment: "#droppable_plant",
            stop: function (event, ui) {
                saveAssetCoords(event, ui, ui.helper.position().left, ui.helper.position().top, false);
            }
        };

        var zIndex = 0;

        var setTileDraggable = function (elements) {
            var $elem = elements.draggable({
                connectToSortable: myTray,
                containment: gameContainer,
                helper: "original",
                revert: "invalid"
//                start: function (e, ui) { ui.helper.css('z-index', ++zIndex); },
                //stop: function (e, ui) {
                    // saveAssetCoords(event, ui);
                //}
            }).disableSelection();

            $('#droppable_plant .asset').resizable(resizeOpts);
            $('#droppable_plant .analyzer').resizable(resizeOpts);
            $('#droppable_plant .gateway').resizable(resizeOpts);
        };

        var myBoard = $("#droppable_plant").droppable({
            accept: ".asset, .analyzer, .gateway",
            drop: function (ev, ui) {
                if (ui.draggable.hasClass("onassetlist")) {

                    // tile (not red) coming from tray, place it into .tiles child div
                    var cloneTile = ui.draggable.removeClass("onassetlist").clone().show();
                    var dropx = ui.offset.left - myBoard.offset().left;
                    var dropy = ui.offset.top - myBoard.offset().top;
                    cloneTile.css({
                        "left": dropx + "px",
                        "position": "absolute",
                        "top": dropy + "px"
                    });
                    ui.helper.position().left = dropx;
                    ui.helper.position().top = dropy;
                    saveAssetCoords(event, ui, dropx, dropy, false);
                    myBoard.append(cloneTile);
                    setTileDraggable(cloneTile);

                    ui.helper.remove();
                    ui.draggable.remove();                    
                }
                else {
                    saveAssetCoords(event, ui, ui.helper.position().left, ui.helper.position().top, false);
                }
            }
        }).disableSelection();

        // set up draggables
        setTileDraggable($(".asset"));
        setTileDraggable($(".analyzer"));
        setTileDraggable($(".gateway"));

        function toggleButton(selector, filterClass) {
            $(selector + ' button').click(function () {
                //if ($(this).hasClass('locked_active') || $(this).hasClass('unlocked_inactive')) {
                //    /* code to do when unlocking */
                //    $(filterClass).html('Switched on.');
                //} else {
                //    /* code to do when locking */
                //    $('#switch_status').html('Switched off.');
                //}
                $(filterClass).toggle();

                /* reverse locking status */
                $(selector + ' button').eq(0).toggleClass('locked_active btn-default btn-info');
                $(selector + ' button').eq(1).toggleClass('unlocked_active btn-info btn-default');
            });
        }

        toggleButton("#toggle_event_map", "#imgmap");
        toggleButton("#toggle_event_prj", ".gateway, .analyzer");
        toggleButton("#toggle_event_asset", ".asset");
    });
</script>


<div class="row">
       <div class="col-md-2" style="margin-bottom: 20px;">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4">
                    Mappa
                </div>
                <div class="col-md-8">
                    <div class="btn-group" id="toggle_event_map">
	                    <button type="button" class="btn btn-info btn-xs locked_active"><span class="fa fa-eye fa-2" aria-hidden="true"></span> ON</button>
                	    <button type="button" class="btn btn-default btn-xs unlocked_inactive"><span class="fa fa-eye-slash fa-2" aria-hidden="true"></span> OFF</button>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4">
                    Progetto
                </div>
                <div class="col-md-8">
                    <div class="btn-group" id="toggle_event_prj">
	                    <button type="button" class="btn btn-info btn-xs locked_active"><span class="fa fa-eye fa-2" aria-hidden="true"></span> ON</button>
                	    <button type="button" class="btn btn-default btn-xs unlocked_inactive"><span class="fa fa-eye-slash fa-2" aria-hidden="true"></span> OFF</button>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4">
                    Assets
                </div>
                <div class="col-md-8">
                    <div class="btn-group" id="toggle_event_asset">
	                    <button type="button" class="btn btn-info btn-xs locked_active"><span class="fa fa-eye fa-2" aria-hidden="true"></span> ON</button>
                	    <button type="button" class="btn btn-default btn-xs unlocked_inactive"><span class="fa fa-eye-slash fa-2" aria-hidden="true"></span> OFF</button>
                    </div>
                </div>                     
            </div>
            <div id="assetlist" class="asset-list">
                <asp:ListView ID="lvDetailNotUsed" runat="server">
                    <LayoutTemplate>
                        <div id="itemPlaceholder" runat="server"></div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div id="itemPlaceholder" runat="server" class='<%#Eval("CssClassType") + " onassetlist ui-widget-content"%>' data-id='<%#Eval("Id")%>'>
                            <asp:Label runat="server" Text='<%#Eval("Title")%>' CssClass="label"></asp:Label>
                            <asp:image runat="server" ImageUrl='<%#Eval("Thumb")%>' class="img"/>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
       </div>

       <div class="col-md-10">
            <div id="droppable_plant" style="position:relative;    border: solid 1px red;">
                <img id="imgmap" src="<%= Map %>" style="width: 100%; position:relative; z-index:-1"/>
                <asp:ListView ID="lvDetail" runat="server">
                    <LayoutTemplate>
                        <div id="itemPlaceholder" runat="server"></div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div id="itemPlaceholder" runat="server" class='<%#Eval("CssClassType") + " ui-widget-content"%>' data-id='<%#Eval("Id")%>' style='<%# Eval("InitStyle") %>'>
                            <asp:Label runat="server" Text='<%#Eval("Title")%>' CssClass="label"></asp:Label>
                            <asp:image runat="server" ImageUrl='<%#Eval("Thumb")%>' class="img"/>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
       </div>
  </div>