﻿using AssetManagement.Models;
using AssetManagement.Services;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.AssetCounterGatewayMap
{
    public abstract class ImagesData
    {
        public string Id { get; set; }
        public string ImageURL { get; set; }
        public string Thumb { get; set; }
        public string Title { get; set; }
        public string InitStyle { get; set; }
        public string DataObjectType { get; set; }
    }

    public class ImageDataAsset : ImagesData
    {
        public Asset Data { get; set; }
        public string CssClassType { get { return "asset"; } }
    }

    public class ImageDataGateway : ImagesData
    {
        public Gateway Data { get; set; }
        public string CssClassType { get { return "gateway"; } }
    }

    public class ImageDataAnalyzer : ImagesData
    {
        public Analyzer Data { get; set; }
        public string CssClassType { get { return "analyzer"; } }
    }

    public partial class AssetCounterGatewayMap_Edit : WidgetControl<object>
    {
        public AssetCounterGatewayMap_Edit() : base(typeof(AssetCounterGatewayMap_Edit)) { }

        public string Map { get; private set; }

        private DepartmentService service = new DepartmentService();

        protected void Page_Load(object sender, EventArgs e)
        {
            var defaultImage = ResolveUrl(AppRelativeTemplateSourceDirectory + "image/missing-image-640x360.png");

            var key = WidgetId + Constants.DepartmentIdSessionVarName;
            if (SessionMultitenant.ContainsKey(key) && !string.IsNullOrEmpty("" + SessionMultitenant[key]))
            {
                var departmentIdSession = SessionMultitenant[key].ToString();
                var department = service.GetDepartmentById(departmentIdSession);

                // Set department map
                Map = Page.ResolveUrl(department.PathImg != null ? MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(department.PathImg) : defaultImage);
                Page.Title = department.Code;

                // Load assets
                var assets = department.Asset;
                var objDataUsed = new List<ImagesData>();
                var objDataNotUsed = new List<ImagesData>();
                foreach (var item in assets)
                {
                    var imgData = new ImageDataAsset()
                    {
                        Id = item.Id,
                        Thumb = item.PathImg != null ? MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(item.PathImg) : defaultImage,
                        Title = item.Code,
                        Data = item,
                        InitStyle = calculateStile(item.PositionAndSize)
                    };

                    if (item.PositionAndSize != null)
                    {
                        objDataUsed.Add(imgData);
                    }
                    else
                    {
                        objDataNotUsed.Add(imgData);
                    }
                }

                var analyzer = from a in assets
                               where a.Analyzer != null
                               select a.Analyzer;
                foreach (var item in analyzer)
                {
                    var imgData = new ImageDataAnalyzer()
                    {
                        Id = item.Id,
                        Thumb = item.PathImg != null ? MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(item.PathImg) : defaultImage,
                        Title = item.SerialCode,
                        Data = item,
                        InitStyle = calculateStile(item.PositionAndSize)
                    };

                    if (item.PositionAndSize != null)
                    {
                        objDataUsed.Add(imgData);
                    }
                    else
                    {
                        objDataNotUsed.Add(imgData);
                    }
                }

                var gateway = from a in analyzer
                              where a.Gateway != null
                              select a.Gateway;
                foreach (var item in gateway)
                {
                    var imgData = new ImageDataGateway()
                    {
                        Id = item.Id,
                        Thumb = item.PathImg != null ? MultiTenantsHelper.UploadApplicationWidgetResourcePathShared(item.PathImg) : defaultImage,
                        Title = item.SerialCode,
                        Data = item,
                        InitStyle = calculateStile(item.PositionAndSize)
                    };

                    if (item.PositionAndSize != null)
                    {
                        objDataUsed.Add(imgData);
                    }
                    else
                    {
                        objDataNotUsed.Add(imgData);
                    }
                }

                // Used
                lvDetail.DataSource = objDataUsed;
                lvDetail.DataBind();
                // Unused
                lvDetailNotUsed.DataSource = objDataNotUsed;
                lvDetailNotUsed.DataBind();
            }
            else
            {
                Response.Redirect(Request.UrlReferrer.ToString()); // back prev page
            }
        }

        private string calculateStile(string positionAndSize)
        {
            string style = "";
            if (positionAndSize != null)
            {
                var posAndSize = JsonConvert.DeserializeObject<JObject>(positionAndSize);
                style = string.Format("left: {0}%; top: {1}%; width: {2}%; height:{3}%; position: absolute; z-index: 1; right: auto; bottom: auto; display: block;",
                    posAndSize["left"], posAndSize["top"], posAndSize["width"], posAndSize["height"]);
            }
            return style;
        }

    }
}