﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MCFDepartmentViewMap.ascx.cs" Inherits="MainCloud.Dashboard.Modules.AssetCounterGatewayMap.MCFDepartmentViewMap" %>
   <style>

    .img {
        height: 100%;
        height: 100%;
    }

    .labelAsset {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        background-color: #fff;
    }

    .asset, .analyzer, .gateway {
      color: #fff;
      display: block;
      float: left;
      line-height: 20px;
      text-align: center;
      font-weight: bold;
    }
    
    .asset {
        z-index: 100;
    } 
    .analyzer, .gateway {
        z-index: 200;
    }
    
    .asset .label {
        background-color: blue !important;
    }

    .analyzer .label, .gateway .label {
        background-color: red !important;
    }

    .droppable_plant {
      /*bottom: 68px;*/
      left: 0;
      position: absolute;
      right: 0;
      top: 0;
    }

    .asset .img, .analyzer .img, .gateway .img {
        width: 100%;
    }

    </style>

<script>
    $(function () {
        function toggleButton(selector, filterClass) {
            $(selector + ' button').click(function () {
                //if ($(this).hasClass('locked_active') || $(this).hasClass('unlocked_inactive')) {
                //    /* code to do when unlocking */
                //    $(filterClass).html('Switched on.');
                //} else {
                //    /* code to do when locking */
                //    $('#switch_status').html('Switched off.');
                //}
                $(filterClass).toggle();

                /* reverse locking status */
                $(selector + ' button').eq(0).toggleClass('locked_active btn-default btn-info');
                $(selector + ' button').eq(1).toggleClass('unlocked_active btn-info btn-default');
            });
        }

        toggleButton("#toggle_event_map", "#viewMap");
        toggleButton("#toggle_event_prj", ".gateway, .analyzer");
        toggleButton("#toggle_event_asset", ".asset");
    });
</script>

<div id="droppable_plant" style="position:relative; border: solid 1px gray;">
    <asp:ListView ID="lvDetail" runat="server" OnItemCommand="lvDetail_ItemCommand">
        <LayoutTemplate>
            <div id="itemPlaceholder" runat="server"></div>
        </LayoutTemplate>
        <ItemTemplate>
            <div id="itemPlaceholder" runat="server" class='<%#Eval("CssClassType")%>' data-id='<%#Eval("Id")%>' style='<%# Eval("InitStyle") %>'>
                <asp:LinkButton ID="assetSelect" runat="server" CommandName="selectItem" CommandArgument='<%#Eval("DataObjectType")%>' Enabled='<%#Eval("DataObjectType") != null%>'>
                    <asp:Label runat="server" Text='<%#Eval("Title")%>' CssClass="label"></asp:Label>
                    <asp:image runat="server" ImageUrl='<%#Eval("Thumb")%>' class="img"/>
                </asp:LinkButton>
            </div>
        </ItemTemplate>
    </asp:ListView>
    <asp:Image ID="viewMap" runat="server" Width="100%" ClientIDMode="Static"/>
</div>
