﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoadProfileChart_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.LoadProfile.LoadProfileChart.LoadProfileChart_View" %>

<telerik:RadHtmlChart ID="ProfileChart" runat="server" >
    <PlotArea>
        <XAxis>
            <LabelsAppearance RotationAngle="40"/>
            <Items>
                <telerik:AxisItem LabelText="Gennaio" />
                <telerik:AxisItem LabelText="Febbraio" />
                <telerik:AxisItem LabelText="Marzo" />
                <telerik:AxisItem LabelText="Aprile" />
                <telerik:AxisItem LabelText="Maggio" />
                <telerik:AxisItem LabelText="Giugno" />
                <telerik:AxisItem LabelText="Luglio" />
                <telerik:AxisItem LabelText="Agosto" />
                <telerik:AxisItem LabelText="Settembre" />
                <telerik:AxisItem LabelText="Ottobre" />
                <telerik:AxisItem LabelText="Novembre" />
                <telerik:AxisItem LabelText="Dicembre" />
            </Items>
        </XAxis>
        <Series>
            <telerik:LineSeries>
                <SeriesItems>
                    <telerik:CategorySeriesItem Y="228" />
                    <telerik:CategorySeriesItem Y="228" />
                    <telerik:CategorySeriesItem Y="228" />
                    <telerik:CategorySeriesItem Y="228" />
                    <telerik:CategorySeriesItem Y="228" />
                    <telerik:CategorySeriesItem Y="228" />
                    <telerik:CategorySeriesItem Y="228" />
                    <telerik:CategorySeriesItem Y="228" />
                    <telerik:CategorySeriesItem Y="228" />
                    <telerik:CategorySeriesItem Y="228" />
                    <telerik:CategorySeriesItem Y="228" />
                    <telerik:CategorySeriesItem Y="228" />
                </SeriesItems>
                <MarkersAppearance Size="0" />
                <LabelsAppearance Visible="false"></LabelsAppearance>
                <Appearance>
                    <FillStyle BackgroundColor="Red" />
                </Appearance>
            </telerik:LineSeries>
        </Series>
    </PlotArea>
</telerik:RadHtmlChart>