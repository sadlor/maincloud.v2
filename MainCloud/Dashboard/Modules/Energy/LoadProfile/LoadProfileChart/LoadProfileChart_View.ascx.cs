﻿using FTPModule.ReadCSV;
using MainCloudFramework.UI.Modules;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.Energy.LoadProfile.LoadProfileChart
{
    public partial class LoadProfileChart_View : WidgetControl<object>
    {
        public LoadProfileChart_View() : base(typeof(LoadProfileChart_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            //this.ProfileChart.PlotArea.Series.Clear();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //this.ProfileChart.PlotArea.XAxis.Items.Clear();
            Year_History();
        }

        public void Year_History()
        {
            CSVStructure structure = ReadData(this.MapPath("./test_annuale.CSV"));

            //(Orientation)Enum.Parse(typeof(Orientation), column["orientation"])

            Telerik.Web.UI.LineSeries year = new Telerik.Web.UI.LineSeries();
            year.LabelsAppearance.Visible = false;
            //year.Appearance.FillStyle.BackgroundColor = "#8dcb2a";

            var ci = CultureInfo.InvariantCulture.Clone() as CultureInfo;
            ci.NumberFormat.NumberDecimalSeparator = ",";

            List<decimal> items = new List<decimal>();
            foreach (List<CSVData> row in structure)
            {
                //this.ProfileChart.PlotArea.XAxis.Items.Add(row[0].Value.Substring(0, 2));
                year.SeriesItems.Add(new Telerik.Web.UI.CategorySeriesItem(decimal.Parse(row[1].Value, ci)));
            }
            this.ProfileChart.PlotArea.Series.Add(year);
        }

        public CSVStructure ReadData(string fileName)
        {
            CSVStructure structure = new CSVStructure(fileName);
            if (structure.Count == 0)
            {
                throw new Exception("File CSV vuoto");
            }
            return structure;
        }
    }
}