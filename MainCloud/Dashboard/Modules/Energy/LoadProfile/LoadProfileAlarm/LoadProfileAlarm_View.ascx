﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoadProfileAlarm_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.LoadProfile.LoadProfileAlarm.LoadProfileAlarm_View" %>

<telerik:RadGrid ID="GridAlarm" runat="server" >
    <MasterTableView FilterExpression="">
        <Columns>
            <telerik:GridTemplateColumn UniqueName="Picco" HeaderText="Picco" ItemStyle-ForeColor="Red">
                <ItemTemplate>
                    <%# DataBinder.Eval(Container.DataItem, "ValueUp") %>
                    <asp:Image ImageUrl="~/Dashboard/Modules/AssetDetails/AssetImages/alarm-small.jpg" runat="server" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>