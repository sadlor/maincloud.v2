﻿using MainCloudFramework.UI.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using MainCloudFramework.Web.Multitenants;
using EnergyModule.Models;
using EnergyModule.Services;
using Newtonsoft.Json;
using EnergyModule.Core;

namespace MainCloud.Dashboard.Modules.Energy.LoadProfile.LoadProfileAlarm
{
    public class AlarmType
    {
        public DateTime Time { get; set; }
        public string AssetName { get; set; }
        public decimal ValueLimit { get; set; }
        public decimal ValueUp { get; set; }

        public AlarmType() {}
    }

    public partial class LoadProfileAlarm_View : WidgetControl<object>
    {
        public LoadProfileAlarm_View() : base(typeof(LoadProfileAlarm_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            string descr = "";
            if (Application.ApplicationMultitenant().ContainsKey(EMConstants.SELECTED_ID_CONTEXT) && Application.ApplicationMultitenant()[EMConstants.SELECTED_ID_CONTEXT] != null)
            {
                SelectedContext selectedContext = JsonConvert.DeserializeObject<SelectedContext>((string)Application.ApplicationMultitenant()[EMConstants.SELECTED_ID_CONTEXT]);
                SelectedContextService scService = new SelectedContextService();
                descr = scService.GetSelectedDescription(selectedContext);
            }

            List<AlarmType> dataSource = new List<AlarmType>();

            AlarmType a = new AlarmType();
            a.Time = new DateTime(2014, 07, 11);
            a.AssetName = descr;
            a.ValueLimit = Convert.ToDecimal(228);
            a.ValueUp = Convert.ToDecimal(228.308);
            dataSource.Add(a);

            a = new AlarmType();
            a.Time = new DateTime(2014, 10, 19);
            a.AssetName = descr;
            a.ValueLimit = Convert.ToDecimal(228);
            a.ValueUp = Convert.ToDecimal(228.425);
            dataSource.Add(a);

            //RadGrid GridAlarm = new RadGrid();
            GridAlarm.AllowPaging = true;
            GridAlarm.CellSpacing = 0;
            GridAlarm.MasterTableView.AutoGenerateColumns = false;
            GridAlarm.MasterTableView.TableLayout = GridTableLayout.Fixed;
            
            GridBoundColumn col = new GridBoundColumn();
            col.DataField = "Time";
            col.HeaderText = "Datetime";
            col.UniqueName = "DateTime";
            GridAlarm.MasterTableView.Columns.AddAt(0, col);

            col = new GridBoundColumn();
            col.DataField = "AssetName";
            col.HeaderText = "Analyzer";
            col.UniqueName = "Analyzer";
            GridAlarm.MasterTableView.Columns.AddAt(1, col);

            col = new GridBoundColumn();
            col.DataField = "ValueLimit";
            col.HeaderText = "Soglia";
            col.UniqueName = "Soglia";
            GridAlarm.MasterTableView.Columns.AddAt(2, col);
            
            //col = new GridBoundColumn();
            //col.DataField = "ValueUp";
            //col.HeaderText = "Picco";
            //col.UniqueName = "Picco";
            //col.ItemStyle.ForeColor = System.Drawing.Color.Red;
            //GridAlarm.MasterTableView.Columns.Add(col);

            GridAlarm.DataSource = dataSource;
            GridAlarm.Rebind();
            //this.Controls.Add(GridAlarm);
        }
    }
}