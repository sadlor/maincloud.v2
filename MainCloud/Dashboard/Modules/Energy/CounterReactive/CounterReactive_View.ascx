﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CounterReactive_View.ascx.cs" Inherits="MainEnergy.Dashboard.Modules.Energy.CounterReactive.CounterReactive_View" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %> 

<telerik:RadTextBox runat="server" Text="" ID="txtContatore" Width="100%" style="background-color:rgb(145, 220, 243);"></telerik:RadTextBox>

<script src="<%=ResolveUrl("~/Dashboard/Modules/Energy/CounterActive/CounterScript.js")%>"></script>
<script type="text/javascript">

    $(function () {
        UpdateCounter('<%= MainCloudFramework.Web.Helpers.MultiTenantsHelper.MountAndResolveMultiTenantUrl("/Dashboard/Modules/Energy/CounterReactive/ReactiveEnergyCounter.svc/GetValue") %>', '<%=txtContatore.ClientID %>', "kvarh", "TotalReactiveEnergyConsumed", false);
        setInterval(function () {
            UpdateCounter('<%= MainCloudFramework.Web.Helpers.MultiTenantsHelper.MountAndResolveMultiTenantUrl("/Dashboard/Modules/Energy/CounterReactive/ReactiveEnergyCounter.svc/GetValue") %>', '<%=txtContatore.ClientID %>', "kvarh", "TotalReactiveEnergyConsumed", false);
        }, 60000); //5 min
    });

</script>

<%--<script type="text/javascript">

    $(function () {
        UpdateReactiveCounter();
        setInterval(function () {
            UpdateReactiveCounter();
        }, 60000); //5 min
    });

    function UpdateReactiveCounter()
    {
        var request = $.ajax({
            url: '<%= MainCloudFramework.Web.Helpers.MultiTenantsHelper.MountAndResolveMultiTenantUrl("/Dashboard/Modules/CounterReactive/ReactiveEnergyCounter.svc/GetValue") %>',
            method: 'GET',
            //data: JSON.stringify({ filter: _filter }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        });

        request.done(function (response) {  //string
            if (response.d != "") {
                $find("<%=txtContatore.ClientID %>").set_displayValue(response.d + " kvarh");
            }
        });

        request.fail(function (jqXHR, textStatus) {
            //alert("Request failed: " + textStatus);
        });
    }

</script>--%>