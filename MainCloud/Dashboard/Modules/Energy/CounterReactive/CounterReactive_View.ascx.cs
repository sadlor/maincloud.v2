﻿using MainCloudFramework.UI.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainEnergy.Dashboard.Modules.Energy.CounterReactive
{
    public partial class CounterReactive_View : WidgetControl<object>
    {
        public CounterReactive_View() : base(typeof(CounterReactive_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}