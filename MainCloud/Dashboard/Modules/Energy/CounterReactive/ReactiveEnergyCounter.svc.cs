﻿using EnergyModule.Core;
using EnergyModule.Core.Exceptions;
using EnergyModule.Services;
using MainCloudFramework.Web.Multitenants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace MainCloud.Dashboard.Modules.Energy.CounterReactive
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ReactiveEnergyCounter
    {
        // Per utilizzare HTTP GET, aggiungere l'attributo [WebGet]. (ResponseFormat predefinito è WebMessageFormat.Json)
        // Per creare un'operazione che restituisca XML,
        //     aggiungere [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     e includere la riga seguente nel corpo dell'operazione:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        [WebGet]
        public string GetValue()
        {
            try
            {
                var tenantName = HttpContext.Current.Request.QueryString["MultiTenant"];
                HttpMultitenantsSessionState SessionMultitenant = HttpContext.Current.Session.SessionMultitenant(tenantName);
                if (SessionMultitenant.ContainsKey(EMConstants.SELECTED_ID_CONTEXT) && SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT] != null)
                {
                    SelectedContext selectedContext = JsonConvert.DeserializeObject<SelectedContext>((string)SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT]);
                    EnergyDataService energyDataService = new EnergyDataService();
                    string analyzerId = energyDataService.GetAnalyzerId(selectedContext);
                    if (!string.IsNullOrEmpty(analyzerId))
                    {
                        try
                        {
                            var energyList = energyDataService.GetAnalyzerEnergyDataList(x => x.AnalyzerId == analyzerId && x.TimeStamp >= DateTime.Today);
                            EnergyRecord last = JsonConvert.DeserializeObject<EnergyRecord>(energyList.LastOrDefault(
                                x => JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Count > 0 &&
                                     JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Exist("TotalReactiveEnergyConsumed")).Data);
                            if (!SessionMultitenant.ContainsKey(EMConstants.START_REACTIVEENERGY_COUNTER))
                            {
                                try
                                {
                                    EnergyRecord first = JsonConvert.DeserializeObject<EnergyRecord>(energyList.FirstOrDefault(
                                         x => JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Count > 0 &&
                                              JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Exist("TotalReactiveEnergyConsumed") &&
                                              JsonConvert.DeserializeObject<EnergyRecord>(x.Data).GetData("TotalReactiveEnergyConsumed") > 0).Data);
                                    SessionMultitenant[EMConstants.START_REACTIVEENERGY_COUNTER] = first.GetData<decimal>("TotalReactiveEnergyConsumed");
                                }
                                catch (Exception ex)
                                {
                                    return Math.Round(last.GetData<decimal>("TotalReactiveEnergyConsumed"), 1).ToString();
                                }
                            }
                            return (Math.Round(last.GetData<decimal>("TotalReactiveEnergyConsumed") - Convert.ToDecimal(SessionMultitenant[EMConstants.START_REACTIVEENERGY_COUNTER]), 1)).ToString();
                        }
                        catch (EnergyDataException e)
                        {
                            return "No data entry";
                        }
                    }
                    else
                    {
                        //analyzerId null
                        return "No data entry";
                    }
                }
                else
                {
                    throw new Exception("Non è stato selezionato un contesto"); //visualizzare l'avviso
                }
            }
            catch (Exception ex)
            {
                return "No data entry";
            }
        }

        // Add more operations here and mark them with [OperationContract]
    }
}
