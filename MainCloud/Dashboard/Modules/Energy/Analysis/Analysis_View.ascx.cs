﻿using AssetManagement.Core;
using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule;
using EnergyModule.Core;
using EnergyModule.Models;
using EnergyModule.Services;
using MainCloud.Dashboard.Modules.Energy.History;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Energy.Analysis
{
    public partial class Analysis_View : WidgetControl<object>
    {
        public Analysis_View() : base(typeof(Analysis_View)) { }

        EnergyEnum.TimeScale TypeOfTime
        {
            get
            {
                return (EnergyEnum.TimeScale)Convert.ToInt16(this.CustomSettings.Find("TypeOfTime"));
            }
        }

        string Behaviour
        {
            get
            {
                return (string)CustomSettings.Find("Behaviour") ?? "Chart";
            }
        }

        string ParameterCode
        {
            get
            {
                return (string)this.CustomSettings.Find("ParameterCode") ?? "TotalActivePower";
            }
        }

        List<SelectedContext> SelectedContextList
        {
            get
            {
                try
                {
                    var list = JsonConvert.DeserializeObject<List<string>>((string)CustomSettings.Find("SelectedContextList"));
                    List<SelectedContext> selectedList = new List<SelectedContext>();
                    foreach (var item in list)
                    {
                        selectedList.Add(JsonConvert.DeserializeObject<SelectedContext>(item));
                    }
                    return selectedList;
                }
                catch (Exception ex)
                {
                    return null;
                } 
            }
        }

        DateTime? DateStart
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateStart");
            }
        }

        DateTime? DateEnd
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateEnd");
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadAssetTree();
            AnalysisChart.PlotArea.XAxis.Items.Clear();
            AnalysisChart.PlotArea.Series.Clear();

            AnalysisChart.Visible = false;
            AnalysisGrid.Visible = false;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (SelectedContextList != null)//(!string.IsNullOrEmpty((string)CustomSettings.Find("SelectedContextId")))
            {
                listSelectedAssets.Items.Clear();
                listSelectedContext.Items.Clear();
                SelectedContextService service = new SelectedContextService();
                foreach (var item in SelectedContextList)
                {
                    RadListBoxItem i = new RadListBoxItem(service.GetSelectedDescription(item), JsonConvert.SerializeObject(item));
                    listSelectedAssets.Items.Add(i);
                    RadListBoxItem i2 = new RadListBoxItem(service.GetSelectedDescription(item), JsonConvert.SerializeObject(item));
                    listSelectedContext.Items.Add(i2);
                }
                listSelectedAssets.Items.Sort(x => x.Text);
                listSelectedContext.Items.Sort(x => x.Text);
            }

            ddlParameter.Items.FindByValue(ParameterCode).Selected = true;
            if (!string.IsNullOrEmpty(Behaviour))
            {
                rbtnBehaviour.Items.FindByValue(Behaviour).Selected = true;
            }
            //setto le date con il valore in memoria
            dtPickStart.SelectedDate = DateStart;
            dtPickEnd.SelectedDate = DateEnd;


            //if (DateStart != null && DateEnd != null && listSelectedAssets.Items.Count > 0)
            //{
            //    if ((DateEnd.Value - DateStart.Value).Days > 62)
            //    {
            //        if ((DateEnd.Value - DateStart.Value).Days > 730)
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Year);
            //        }
            //        else
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Month);
            //        }
            //    }
            //    else
            //    {
            //        if (DateEnd.Value == DateStart.Value)
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Hour);
            //        }
            //        else
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Day);
            //        }
            //    }
                //switch (Behaviour)
                //{
                //    case "Table":
                //        AnalysisChart.Visible = false;
                //        AnalysisGrid.Visible = true;
                //        CreateTable(DateStart.Value, DateEnd.Value, ParameterCode, TypeOfTime, listSelectedAssets.Items);
                //        break;
                //    case "Chart":
                //        AnalysisGrid.Visible = false;
                //        AnalysisChart.Visible = true;
                //        CreateChart(DateStart.Value, DateEnd.Value, ParameterCode, TypeOfTime, listSelectedAssets.Items);
                //        switch (ParameterCode)
                //        {
                //            case "TotalActivePower":
                //                AnalysisChart.PlotArea.YAxis.TitleAppearance.Text += "KW";
                //                break;
                //            case "TotalActiveEnergyConsumed":
                //                AnalysisChart.PlotArea.YAxis.TitleAppearance.Text += "KWh";
                //                break;
                //            case "TotalReactiveEnergyConsumed":
                //                AnalysisChart.PlotArea.YAxis.TitleAppearance.Text += "kvarh";
                //                break;
                //        }
                //        break;
                //}
            //}
        }

        #region SelectAsset
        protected void btnSelect_Click(object sender, EventArgs e)
        {
            LoadAssetTree();
            wndSelectAsset.OpenDialog();
        }

        protected void LoadAssetTree()
        {
            AssetTreeView.Nodes.Clear();

            PlantService service = new PlantService();
            var plants = service.PlantList();
            foreach (Plant p in plants.OrderBy(x => x.Description))
            {
                //Node Plant
                RadTreeNode nodeP = new RadTreeNode(p.Description);
                if (string.IsNullOrEmpty(p.AnalyzerId))
                {
                    nodeP.ForeColor = Color.Red;
                    nodeP.Value = "";
                }
                else
                {
                    nodeP.Value = JsonConvert.SerializeObject(new SelectedContext(p.Id, AssetManagementEnum.AnalyzerContext.Plant));
                }
                nodeP.Expanded = true;
                foreach (Department d in p.Department.OrderBy(x => x.Description))
                {
                    //Node Department
                    RadTreeNode nodeD = new RadTreeNode(d.Description);
                    if (string.IsNullOrEmpty(d.AnalyzerId))
                    {
                        nodeD.ForeColor = Color.Red;
                        nodeD.Value = "";
                    }
                    else
                    {
                        nodeD.Value = JsonConvert.SerializeObject(new SelectedContext(d.Id, AssetManagementEnum.AnalyzerContext.Department));
                    }
                    nodeD.Expanded = true;
                    foreach (Asset a in d.Asset.OrderBy(x => x.Description))
                    {
                        //Node Asset
                        RadTreeNode nodeA = new RadTreeNode(a.Description);
                        if (string.IsNullOrEmpty(a.AnalyzerId))
                        {
                            nodeA.ForeColor = Color.Red;
                            nodeA.Value = "";
                        }
                        else
                        {
                            nodeA.Value = JsonConvert.SerializeObject(new SelectedContext(a.Id, AssetManagementEnum.AnalyzerContext.Asset));
                        }
                        nodeA.Expanded = true;

                        nodeD.Nodes.Add(nodeA);
                    }
                    nodeP.Nodes.Add(nodeD);
                }
                AssetTreeView.Nodes.Add(nodeP);
            }
        }

        protected void AssetTreeView_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            RadListBoxItem item = new RadListBoxItem(e.Node.Text, e.Node.Value);
            listSelectedAssets.Items.Add(item);
        }

        protected void btnConfirmSelect_Click(object sender, EventArgs e)
        {
            //ViewState["AssetSelected"] = listSelectedAssets.Items;
            List<string> list = new List<string>();
            foreach (RadListBoxItem item in listSelectedAssets.Items)
            {
                list.Add(item.Value);
            }
            CustomSettings.AddOrUpdate("SelectedContextList", JsonConvert.SerializeObject(list));
            wndSelectAsset.CloseDialog();
        }

        #endregion

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate("ParameterCode", ddlParameter.SelectedValue);
            CustomSettings.AddOrUpdate("Behaviour", rbtnBehaviour.SelectedValue);
            if (dtPickStart.SelectedDate == null || dtPickEnd.SelectedDate == null)
            {
                messaggio.Text = "Inserire data di inizio o fine";
                dlgWidgetConfig.OpenDialog();
            }
            else
            {
                CustomSettings.AddOrUpdate("DateStart", dtPickStart.SelectedDate.Value);
                CustomSettings.AddOrUpdate("DateEnd", dtPickEnd.SelectedDate.Value);

                if (listSelectedAssets.Items.Count == 0)
                {
                    messaggio.Text = "Selezionare prima un asset";
                    dlgWidgetConfig.OpenDialog();
                }
                else
                {
                    if ((DateEnd.Value - DateStart.Value).Days > 62)
                    {
                        if ((DateEnd.Value - DateStart.Value).Days > 730)
                        {
                            CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Year);
                        }
                        else
                        {
                            CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Month);
                        }
                    }
                    else
                    {
                        if (DateEnd.Value == DateStart.Value)
                        {
                            CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Hour);
                        }
                        else
                        {
                            CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Day);
                        }
                    }

                    switch (Behaviour)
                    {
                        case "Table":
                            AnalysisChart.Visible = false;
                            AnalysisGrid.Visible = true;
                            CreateTable(DateStart.Value, DateEnd.Value, ParameterCode, TypeOfTime, listSelectedAssets.Items);
                            break;
                        case "Chart":
                            AnalysisGrid.Visible = false;
                            AnalysisChart.Visible = true;
                            CreateChart(DateStart.Value, DateEnd.Value, ParameterCode, TypeOfTime, listSelectedAssets.Items);
                            switch (ParameterCode)
                            {
                                case "TotalActivePower":
                                    AnalysisChart.PlotArea.YAxis.TitleAppearance.Text += "KW";
                                    break;
                                case "TotalActiveEnergyConsumed":
                                    AnalysisChart.PlotArea.YAxis.TitleAppearance.Text += "KWh";
                                    break;
                                case "TotalReactiveEnergyConsumed":
                                    AnalysisChart.PlotArea.YAxis.TitleAppearance.Text += "kvarh";
                                    break;
                            }
                            break;
                    }
                    //updatePnlData.Update();
                }
            }
        }

        protected void CreateTable(DateTime start, DateTime end, string paramName, EnergyEnum.TimeScale typeOfTime, RadListBoxItemCollection selections)
        {
            if (paramName == EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString())
            {
                AnalysisGrid.RenderMode = RenderMode.Lightweight;
                AnalysisGrid.MasterTableView.Caption = "kWh per asset";
                AnalysisGrid.AutoGenerateColumns = false;
                AnalysisGrid.MasterTableView.Columns.Clear();

                GridDateTimeColumn dateCol = new GridDateTimeColumn();
                switch (typeOfTime)
                {
                    case EnergyEnum.TimeScale.Hour:
                        dateCol.DataFormatString = "{0:dd/MM/yyyy HH:mm}";
                        break;
                    case EnergyEnum.TimeScale.Day:
                        dateCol.DataFormatString = "{0:dd/MM/yyyy}";
                        break;
                    case EnergyEnum.TimeScale.Month:
                        dateCol.DataFormatString = "{0:MM/yyyy}";
                        break;
                    case EnergyEnum.TimeScale.Year:
                        dateCol.DataFormatString = "{0:yyyy}";
                        break;
                }
                dateCol.HeaderText = "Data";
                dateCol.DataField = "Key";
                dateCol.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                AnalysisGrid.MasterTableView.Columns.Add(dateCol);

                EnergyDataService EDS = new EnergyDataService();
                EnergyConsumptionService ECS = new EnergyConsumptionService();
                SelectedContextService contextService = new SelectedContextService();
                SortedDictionary<DateTime, Dictionary<string, decimal>> tableDataSource = new SortedDictionary<DateTime, Dictionary<string, decimal>>();

                foreach (RadListBoxItem selection in selections)
                {
                    List<EnergyConsumption> list = ECS.GetList(EDS.GetAnalyzerId(JsonConvert.DeserializeObject<SelectedContext>(selection.Value)), start, end);
                    if (list.Count > 0)
                    {
                        foreach (var item in list)
                        {
                            try
                            {
                                tableDataSource[item.Date][contextService.GetSelectedDescription(JsonConvert.DeserializeObject<SelectedContext>(selection.Value))] = item.kWh;
                            }
                            catch (KeyNotFoundException ex)
                            {
                                tableDataSource.Add(item.Date, new Dictionary<string, decimal>(){
                                {contextService.GetSelectedDescription(JsonConvert.DeserializeObject<SelectedContext>(selection.Value)), item.kWh
                                }});
                            }
                        }
                    }
                    GridBoundColumn col = new GridBoundColumn();
                    col.DataField = "Value[" + contextService.GetSelectedDescription(JsonConvert.DeserializeObject<SelectedContext>(selection.Value)) + "]";
                    col.HeaderText = contextService.GetSelectedDescription(JsonConvert.DeserializeObject<SelectedContext>(selection.Value));
                    col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    col.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    AnalysisGrid.MasterTableView.Columns.Add(col);
                }
                AnalysisGrid.DataSource = tableDataSource;
                AnalysisGrid.DataBind();
            }
            else
            {
                AnalysisGrid.RenderMode = RenderMode.Lightweight;
                AnalysisGrid.MasterTableView.Caption = "History - " + paramName + " - " + typeOfTime;
                AnalysisGrid.AutoGenerateColumns = false;
                AnalysisGrid.MasterTableView.Columns.Clear();

                GridDateTimeColumn dateCol = new GridDateTimeColumn();
                switch (typeOfTime)
                {
                    case EnergyEnum.TimeScale.Hour:
                        dateCol.DataFormatString = "{0:dd/MM/yyyy HH:mm}";
                        break;
                    case EnergyEnum.TimeScale.Day:
                        dateCol.DataFormatString = "{0:dd/MM/yyyy}";
                        break;
                    case EnergyEnum.TimeScale.Month:
                        dateCol.DataFormatString = "{0:MM/yyyy}";
                        break;
                    case EnergyEnum.TimeScale.Year:
                        dateCol.DataFormatString = "{0:yyyy}";
                        break;
                }
                dateCol.HeaderText = "Data";
                dateCol.DataField = "Key";
                dateCol.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                AnalysisGrid.MasterTableView.Columns.Add(dateCol);

                SortedDictionary<DateTime, Dictionary<string, decimal>> tableDataSource = new SortedDictionary<DateTime, Dictionary<string, decimal>>();
                EnergyDataService energyDataService = new EnergyDataService();
                SelectedContextService contextService = new SelectedContextService();

                foreach (RadListBoxItem selection in selections)
                {
                    var seriesItems = energyDataService.CreateHistorySerie(JsonConvert.DeserializeObject<SelectedContext>(selection.Value), start, end, paramName, typeOfTime);
                    if (seriesItems != null)
                    {
                        foreach (var item in seriesItems)
                        {
                            try
                            {
                                tableDataSource[item.Key][contextService.GetSelectedDescription(JsonConvert.DeserializeObject<SelectedContext>(selection.Value))] = (decimal)item.Value.Value;
                            }
                            catch (KeyNotFoundException ex)
                            {
                                tableDataSource.Add(item.Key, new Dictionary<string, decimal>(){
                                {contextService.GetSelectedDescription(JsonConvert.DeserializeObject<SelectedContext>(selection.Value)), (decimal)item.Value.Value
                                }});
                            }
                        }
                    }
                    GridBoundColumn col = new GridBoundColumn();
                    col.DataField = "Value[" + contextService.GetSelectedDescription(JsonConvert.DeserializeObject<SelectedContext>(selection.Value)) + "]";
                    col.HeaderText = contextService.GetSelectedDescription(JsonConvert.DeserializeObject<SelectedContext>(selection.Value));
                    col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    col.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    AnalysisGrid.MasterTableView.Columns.Add(col);
                }

                AnalysisGrid.DataSource = tableDataSource;
                AnalysisGrid.DataBind();
            }
        }

        protected void CreateChart(DateTime start, DateTime end, string paramName, EnergyEnum.TimeScale typeOfTime, RadListBoxItemCollection selections)
        {
            CreateXAxis(start, end, typeOfTime);

            if (paramName == EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString())
            {
                EnergyDataService EDS = new EnergyDataService();
                EnergyConsumptionService ECS = new EnergyConsumptionService();
                SelectedContextService scService = new SelectedContextService();

                foreach (RadListBoxItem selection in selections)
                {
                    List<CategorySeriesItem> valueSerie = new List<CategorySeriesItem>();

                    List<EnergyConsumption> list = ECS.GetList(EDS.GetAnalyzerId(JsonConvert.DeserializeObject<SelectedContext>(selection.Value)), start, end);
                    foreach (var item in list)
                    {
                        valueSerie.Add(new CategorySeriesItem(item.kWh));
                    }
                    
                    LineSeries lineSerie = new LineSeries();
                    lineSerie.Name = scService.GetSelectedDescription(JsonConvert.DeserializeObject<SelectedContext>(selection.Value));
                    lineSerie.LabelsAppearance.Visible = false;
                    lineSerie.SeriesItems.AddRange(valueSerie);
                    AnalysisChart.PlotArea.Series.Add(lineSerie);
                }
            }
            else
            {
                bool createX = true;
                foreach (RadListBoxItem selection in selections)
                {
                    List<EnergyData> seriesItems = new List<EnergyData>();
                    if (createX)
                    {
                        seriesItems = CreateSeriesItems(JsonConvert.DeserializeObject<SelectedContext>(selection.Value), start, end, paramName, typeOfTime);
                        createX = false;
                    }
                    else
                    {
                        seriesItems = CreateSeriesItems(JsonConvert.DeserializeObject<SelectedContext>(selection.Value), start, end, paramName, typeOfTime, false);
                    }

                    if (seriesItems != null)
                    {
                        List<CategorySeriesItem> valueSerie = new List<CategorySeriesItem>();
                        List<CategorySeriesItem> minSerie = new List<CategorySeriesItem>();
                        List<CategorySeriesItem> maxSerie = new List<CategorySeriesItem>();

                        foreach (var item in seriesItems)
                        {
                            valueSerie.Add(new CategorySeriesItem((decimal)item.Value));
                            minSerie.Add(new CategorySeriesItem((decimal)item.Min));
                            maxSerie.Add(new CategorySeriesItem((decimal)item.Max));
                        }

                        SelectedContextService scService = new SelectedContextService();

                        LineSeries lineSerie = new LineSeries();
                        lineSerie.Name = scService.GetSelectedDescription(JsonConvert.DeserializeObject<SelectedContext>(selection.Value));
                        lineSerie.LabelsAppearance.Visible = false;
                        lineSerie.SeriesItems.AddRange(valueSerie);
                        AnalysisChart.PlotArea.Series.Add(lineSerie);

                        //if (paramName == "TotalActivePower")
                        //{
                        //    LineSeries lineSerieMin = new LineSeries();
                        //    lineSerieMin.Name = "Minimi";
                        //    lineSerieMin.LabelsAppearance.Visible = false;
                        //    lineSerieMin.SeriesItems.AddRange(seriesItems["Min"]);
                        //    //lineSerieMin.SeriesItems.AddRange(CreateSeriesItemMin<CategorySeriesItem>(dataHistory, paramName, typeOfTime, false));
                        //    GenericChart.PlotArea.Series.Add(lineSerieMin);

                        //    LineSeries lineSerieMax = new LineSeries();
                        //    lineSerieMax.Name = "Picchi massimi";
                        //    lineSerieMax.LabelsAppearance.Visible = false;
                        //    lineSerieMax.SeriesItems.AddRange(seriesItems["Max"]);
                        //    //lineSerieMax.SeriesItems.AddRange(CreateSeriesItemMax<CategorySeriesItem>(dataHistory, paramName, typeOfTime, false));
                        //    GenericChart.PlotArea.Series.Add(lineSerieMax);
                        //}
                    }
                }
            }
        }

        private void CreateXAxis(DateTime start, DateTime end, EnergyEnum.TimeScale typeOfTime)
        {
            switch (typeOfTime)
            {
                case EnergyEnum.TimeScale.Hour:
                    while (start.Date <= end.Date)
                    {
                        AnalysisChart.PlotArea.XAxis.Items.Add((start.Hour + 1).ToString());
                        start = start.AddHours(1);
                    }
                    break;
                case EnergyEnum.TimeScale.Day:
                    while (start.Date <= end.Date)
                    {
                        AnalysisChart.PlotArea.XAxis.Items.Add(start.Date.ToString("dd"));
                        start = start.AddDays(1);
                    }
                    //GenericChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 35;
                    break;
                case EnergyEnum.TimeScale.Month:
                    while (start.Date <= end.Date)
                    {
                        AnalysisChart.PlotArea.XAxis.Items.Add(start.Date.ToString("yyyy/MM"));
                        start = start.AddMonths(1);
                    }
                    break;
                case EnergyEnum.TimeScale.Year:
                    while (start.Date != end.Date)
                    {
                        AnalysisChart.PlotArea.XAxis.Items.Add(start.Date.ToString("yyyy"));
                        start = start.AddYears(1);
                    }
                    break;
            }
        }

        private List<EnergyData> CreateSeriesItems(SelectedContext selection, DateTime start, DateTime end, string paramName, EnergyEnum.TimeScale typeOfTime, bool createX = true)
        {
            EnergyDataService energyDataService = new EnergyDataService();

            var list = energyDataService.CreateHistorySerie(selection, start, end, paramName, typeOfTime);

            if (list.Count > 0)
            {
                //if (createX)
                //{
                //    foreach (var item in CauseList)
                //    {
                //        switch (typeOfTime)
                //        {
                //            case EnergyEnum.TimeScale.Hour:
                //                AnalysisChart.PlotArea.XAxis.Items.Add(item.Key.Hour.ToString());
                //                break;
                //            case EnergyEnum.TimeScale.Day:
                //                //GenericChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 35;
                //                AnalysisChart.PlotArea.XAxis.Items.Add(item.Key.Date.ToString("dd"));
                //                break;
                //            case EnergyEnum.TimeScale.Month:
                //                AnalysisChart.PlotArea.XAxis.Items.Add(item.Key.Date.ToString("yyyy/MM"));
                //                break;
                //            case EnergyEnum.TimeScale.Year:
                //                break;
                //        }
                //    }
                //}
                return list.Values.ToList();
            }
            return null;
        }
    }
}