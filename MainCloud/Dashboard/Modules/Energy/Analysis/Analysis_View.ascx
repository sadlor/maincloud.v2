﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Analysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.Analysis.Analysis_View" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<link href="/Dashboard/WidgetStyle.css" rel="stylesheet">
<style type="text/css">
    .rlbItem {
        float: left !important;
    }
    .rlbGroup, .RadListBox {
        width: auto !important;
    }
    .RadListBox, .rlbGroup {
        border: none !important;
    }
</style>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <mcf:Alert runat="server" id="AlertMessagePopup" />
    </ContentTemplate>
</asp:UpdatePanel>

        <telerik:RadDockZone ID="dockZoneSettings" runat="server" Orientation="Vertical" BorderStyle="None">
            <telerik:RadDock RenderMode="Auto" ID="dockSettings" runat="server" Title="Settings"
                EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false" Resizable="true" Width="100%">
                <ContentTemplate>
                    <div class="row" style="width:100%;">
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                            <%--<asp:UpdatePanel runat="server">
                                <ContentTemplate>--%>
                                    <asp:Button runat="server" ID="btnSelect" Text="Select Assets" OnClick="btnSelect_Click" CssClass="btnConfig" />
                                <%--</ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                            <asp:Button runat="server" CssClass="btn btn-success" ID="lblSelectedContext" Width="100%" Text="Asset selezionati" />
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
                            <asp:DropDownList runat="server" ID="ddlParameter" AutoPostBack="false" CssClass="btn btn-default dropdown-toggle" Width="100%">
                                <asp:ListItem Text="Energia attiva" Value="TotalActiveEnergyConsumed" />
                                <asp:ListItem Text="Energia reattiva" Value="TotalReactiveEnergyConsumed" />
                                <asp:ListItem Text="Potenza" Value="TotalActivePower" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                            <asp:RadioButtonList runat="server" ID="rbtnBehaviour" AutoPostBack="false" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Table" Value="Table" />
                                <asp:ListItem Text="Chart" Value="Chart" />
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <br />

                    <div class="row" style="width:100%;">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <asp:RadioButtonList ID="rbtnListLastTime" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" style="margin-left:18px;">
                                <asp:ListItem Text="Last day" Value="LastDay" />
                                <asp:ListItem Text="Last month" Value="LastMonth" />
                                <asp:ListItem Text="Last year" Value="LastYear" />
                                <asp:ListItem Text="Current month" Value="CurrentMonth" />
                                <asp:ListItem Text="Current year" Value="CurrentYear" />
                            </asp:RadioButtonList>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                            <asp:Label Text="Data inizio:" runat="server" AssociatedControlID="dtPickStart" />
                            <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickStart" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                            <asp:Label Text="Data fine:" runat="server" AssociatedControlID="dtPickEnd" />
                            <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickEnd" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                        </div>
                        <%--<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>--%>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                            <br />
                            <%--<asp:UpdatePanel runat="server">
                                <ContentTemplate>--%>
                                    <asp:LinkButton ID="btnConfirm" Text="Confirm <i class='fa fa-check-circle'></i>" runat="server" OnClick="btnConfirm_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;" />
                                <%--</ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </div>
                    </div>

                </ContentTemplate>
            </telerik:RadDock>
            <telerik:RadDock RenderMode="Auto" runat="server" ID="dockAsset" Title="Selected assets" AutoPostBack="false"
                EnableAnimation="true" EnableDrag="false" EnableRoundedCorners="true" Collapsed="true" Width="100%">
                <Commands>
                    <telerik:DockExpandCollapseCommand AutoPostBack="false" />
                </Commands>
                <ContentTemplate>
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <telerik:RadListBox runat="server" ID="listSelectedContext" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </telerik:RadDock>
        </telerik:RadDockZone>

<%--<asp:UpdatePanel runat="server" ID="updatePnlData" UpdateMode="Conditional">
    <ContentTemplate>--%>
        <telerik:RadHtmlChart ID="AnalysisChart" runat="server" />
        <telerik:RadGrid ID="AnalysisGrid" runat="server" MasterTableView-FilterExpression="" />
        <%--</ContentTemplate>
</asp:UpdatePanel>--%>

        <mcf:PopUpDialog ID="wndSelectAsset" runat="server" Title="Seleziona assets">
            <Body>
                <div class="row">
                    <div class="col-xs-6 col-md-6">
                        <%--<div class="row">
                            <asp:Label runat="server" Text="Assets" AssociatedControlID="listSelectedAssets" />
                        </div>--%>
                        <%--<div class="row">--%>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <telerik:RadTreeView runat="Server" ID="AssetTreeView" EnableViewState="false" EnableDragAndDrop="false" EnableDragAndDropBetweenNodes="false" OnClientNodeClicked="AddAsset" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%--</div>--%>
                    </div>
                    <div class="col-xs-1 col-md-1"></div>
                    <div class="col-xs-5 col-md-5">
                        <div class="row">
                            <asp:Label runat="server" ID="lblSelectedAssets" Text="Assets selezionati" AssociatedControlID="listSelectedAssets" />
                        </div>
                        <div class="row">
                            <telerik:RadListBox runat="server" ID="listSelectedAssets" />
                            <asp:LinkButton Text="Delete <i class='fa fa-trash-o'></i>" runat="server" CssClass="btn btn-danger btn-xs btn-outline" OnClientClick="DeleteAsset();return false;" />
                        </div>
                        <br /><br /><br />
                        <div class="row">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:LinkButton ID="btnConfirmSelect" Text="Confirm <i class='fa fa-arrow-circle-right'></i>" runat="server" OnClick="btnConfirmSelect_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;margin-left:50%;" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div> 
            </Body>
        </mcf:PopUpDialog>

    

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
    <Body>
        <asp:label id="messaggio" runat="server"/>
    </Body>
</mcf:PopUpDialog>

<script src="<%=ResolveUrl("~/Dashboard/WidgetGenericScript/SettingsGraphScript.js")%>"></script>
<script type="text/javascript">
    <%--function OpenPopup() {
        $find('<%= wndSelectAsset.ClientID %>').show();
    }--%>

    <%--function ClosePopup() {
        $find('<%= wndSelectAsset.ClientID %>').close();
    }--%>

    $(function () {
        $(<%= rbtnListLastTime.ClientID %>).on('click', function () {
            SetDate("<%= dtPickStart.ClientID%>", "<%= dtPickEnd.ClientID%>", "<%= rbtnListLastTime.ClientID%>");
        });
        $(<%= dtPickStart.ClientID %>).on('select', function () {
            ClearRadioButtonListDate("<%= rbtnListLastTime.ClientID %>");
        });
        $(<%= lblSelectedContext.ClientID %>).on('click', function () {
            var dock = $find("<%= dockAsset.ClientID %>");
            if (dock.get_collapsed()) {
                dock.set_collapsed(false);
            }
            return false;
        });
    });

    function AddAsset(sender, args) {
        if (args.get_node().get_value() != null && args.get_node().get_value() != "") {
            var item = new Telerik.Web.UI.RadListBoxItem();
            item.set_text(args.get_node().get_text());
            item.set_value(args.get_node().get_value());
            var listbox = $find("<%= listSelectedAssets.ClientID %>");
            listbox.trackChanges();
            listbox.get_items().add(item);
            listbox.commitChanges();
        }
    }

    function DeleteAsset() {
        var listBox = $find("<%= listSelectedAssets.ClientID %>");
        if (listBox.get_items().get_count() < 1) {
            alert("The listBox is empty.");
            return false;
        }

        var selectedItem = listBox.get_selectedItem();
        if (!selectedItem) {
            alert("You need to select a item first.");
            return false;
        }

        if (listBox.get_items().get_count() == 1) {
            if (!confirm("This is the last item in the listBox. Are you sure you want to delete it?"))
                return false;
        }

        listBox.trackChanges();
        listBox.deleteItem(selectedItem);
        listBox.commitChanges();
        return false;
    }

</script>