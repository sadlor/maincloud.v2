﻿using AssetManagement.Core;
using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule.ConsumptionStorage;
using EnergyModule.Core;
using EnergyModule.Models;
using EnergyModule.Services;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Energy.RiepilogoEnergiaConsumataMensile
{
    public partial class RiepilogoEnergiaConsumataMensile_View : DataWidget<RiepilogoEnergiaConsumataMensile_View>
    {
        public RiepilogoEnergiaConsumataMensile_View() : base(typeof(RiepilogoEnergiaConsumataMensile_View)) { }

        const string SELECTED_CONTEXT_ID = "SelectedContextId";

        SelectedContext SelectedContextId
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<SelectedContext>((string)CustomSettings.Find(SELECTED_CONTEXT_ID));
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnergyChart.Visible = false;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (SelectedContextId != null)
            {
                SelectedContextService service = new SelectedContextService();
                lblSelectedContext.Text = service.GetSelectedDescription(SelectedContextId);
            }
        }

        #region AssetTree
        protected void btnSelectContext_Click(object sender, EventArgs e)
        {
            LoadAssetTree();
            wndSelectContext.OpenDialog();
        }

        protected void LoadAssetTree()
        {
            AssetTreeView.Nodes.Clear();

            PlantService service = new PlantService();
            var plants = service.PlantList();
            foreach (Plant p in plants.OrderBy(x => x.Description))
            {
                //Node Plant
                RadTreeNode nodeP = new RadTreeNode(p.Description);
                if (string.IsNullOrEmpty(p.AnalyzerId))
                {
                    nodeP.ForeColor = Color.Red;
                    nodeP.Value = "";
                }
                else
                {
                    nodeP.Value = JsonConvert.SerializeObject(new SelectedContext(p.Id, AssetManagementEnum.AnalyzerContext.Plant));
                }
                nodeP.Expanded = true;
                foreach (Department d in p.Department.OrderBy(x => x.Description))
                {
                    //Node Department
                    RadTreeNode nodeD = new RadTreeNode(d.Description);
                    if (string.IsNullOrEmpty(d.AnalyzerId))
                    {
                        nodeD.ForeColor = Color.Red;
                        nodeD.Value = "";
                    }
                    else
                    {
                        nodeD.Value = JsonConvert.SerializeObject(new SelectedContext(d.Id, AssetManagementEnum.AnalyzerContext.Department));
                    }
                    nodeD.Expanded = true;
                    foreach (Asset a in d.Asset.OrderBy(x => x.Description))
                    {
                        //Node Asset
                        RadTreeNode nodeA = new RadTreeNode(a.Description);
                        if (string.IsNullOrEmpty(a.AnalyzerId))
                        {
                            nodeA.ForeColor = Color.Red;
                            nodeA.Value = "";
                        }
                        else
                        {
                            nodeA.Value = JsonConvert.SerializeObject(new SelectedContext(a.Id, AssetManagementEnum.AnalyzerContext.Asset));
                        }
                        nodeA.Expanded = true;

                        nodeD.Nodes.Add(nodeA);
                    }
                    nodeP.Nodes.Add(nodeD);
                }
                AssetTreeView.Nodes.Add(nodeP);
            }
        }

        protected void btnConfirmSelectContext_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate(SELECTED_CONTEXT_ID, hiddenSelection.Value);
            wndSelectContext.CloseDialog();
        }
        #endregion

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (SelectedContextId != null)
            {
                DateTime start = new DateTime(DateTime.Today.AddMonths(-12).Year, DateTime.Today.AddMonths(-12).Month, 01);
                DateTime end = new DateTime(
                    DateTime.Today.Year,
                    DateTime.Today.AddMonths(-1).Month,
                    DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month));
                EnergyChart.Visible = true;
                CreateChartDataSource(start, end);
                //updatePnlData.Update();
            }
            else
            {
                messaggio.Text = "Inserire data di inizio o fine";
                dlgWidgetConfig.OpenDialog();
            }
        }

        protected void CreateChartDataSource(DateTime start, DateTime end)
        {
            EnergyChart.PlotArea.XAxis.Items.Clear();

            ConsumptionStorage storage = new ConsumptionStorage();
            EnergyDataService EDS = new EnergyDataService();
            List<MonthlyConsumption> list = storage.GetList(EDS.GetAnalyzerId(SelectedContextId), start, end);
            foreach (MonthlyConsumption item in list)
            {
                EnergyChart.PlotArea.XAxis.Items.Add(item.Date.ToString("MM/yyyy"));

                EnergyChart.PlotArea.Series[0].Items.Add(item.kWhF1);
                EnergyChart.PlotArea.Series[1].Items.Add(item.kWhF2);
                EnergyChart.PlotArea.Series[2].Items.Add(item.kWhF3);
            }
        }
    }
}