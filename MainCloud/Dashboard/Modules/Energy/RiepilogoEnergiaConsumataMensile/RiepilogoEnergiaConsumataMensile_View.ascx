﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RiepilogoEnergiaConsumataMensile_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.RiepilogoEnergiaConsumataMensile.RiepilogoEnergiaConsumataMensile_View" %>

<link href="/Dashboard/WidgetStyle.css" rel="stylesheet">

<telerik:RadDockZone ID="dockZoneSettings" runat="server" Orientation="Vertical" BorderStyle="None">
    <telerik:RadDock RenderMode="Auto" ID="dockSettings" runat="server" Title="Settings"
        EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false" Resizable="true" Width="100%">
        <ContentTemplate>
            <div class="row" style="width:100%;">
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                    <%--<asp:UpdatePanel runat="server">
                        <ContentTemplate>--%>
                            <asp:Button runat="server" ID="btnSelectContext" Text="Select Asset" OnClick="btnSelectContext_Click" CssClass="btnConfig" />
                        <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                    <asp:Label CssClass="btn btn-success" ID="lblSelectedContext" runat="server" />
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                    <br />
                    <%--<asp:UpdatePanel runat="server">
                        <ContentTemplate>--%>
                            <asp:LinkButton ID="btnConfirm" Text="Confirm <i class='fa fa-check-circle'></i>" runat="server" OnClick="btnConfirm_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;" />
                        <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
            </div>
        </ContentTemplate>
    </telerik:RadDock>
</telerik:RadDockZone>

<%--<asp:UpdatePanel runat="server" ID="updatePnlData">
    <ContentTemplate>--%>
        <telerik:RadHtmlChart ID="EnergyChart" runat="server">
            <PlotArea>
                <Series>
                    <telerik:ColumnSeries Name="F1" GroupName="Fasce">
                        <Appearance>
                            <FillStyle BackgroundColor="#ffc700" />
                        </Appearance>
                        <LabelsAppearance Visible="false" />
                    </telerik:ColumnSeries>
                    <telerik:ColumnSeries Name="F2" GroupName="Fasce">
                        <Appearance>
                            <FillStyle BackgroundColor="#2a94cb" />
                        </Appearance>
                        <LabelsAppearance Visible="false" />
                    </telerik:ColumnSeries>
                    <telerik:ColumnSeries Name="F3" GroupName="Fasce">
                        <Appearance>
                            <FillStyle BackgroundColor="#8dcb2a" />
                        </Appearance>
                        <LabelsAppearance Visible="false" />
                    </telerik:ColumnSeries>
                </Series>
                <YAxis>
                    <TitleAppearance Text="kWh" />
                </YAxis>
            </PlotArea>
        </telerik:RadHtmlChart>

        <asp:HiddenField ID="hiddenSelection" runat="server" />

        <asp:UpdateProgress runat="server">
            <ProgressTemplate>
                <div class="loading">Loading&#8230;</div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    <%--</ContentTemplate>
</asp:UpdatePanel>--%>

<mcf:PopUpDialog ID="wndSelectContext" runat="server" Title="Seleziona asset">
    <Body>
        <div class="row">
            <div class="col-xs-6 col-md-6">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <telerik:RadTreeView runat="Server" ID="AssetTreeView" EnableViewState="false" EnableDragAndDrop="false" EnableDragAndDropBetweenNodes="false" 
                            OnClientNodeClicked="function(sender, args){window['TreeNodeClick_' + sender._element.id](sender, args)}" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="col-xs-1 col-md-1"></div>
            <div class="col-xs-5 col-md-5">
                <br /><br /><br />
                <div class="row">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnConfirmSelectContext" Text="Confirm <i class='fa fa-arrow-circle-right'></i>" runat="server"
                                CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;margin-left:50%;"
                                OnClick="btnConfirmSelectContext_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </Body>
</mcf:PopUpDialog>

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
    <Body>
        <asp:label id="messaggio" runat="server"/>
    </Body>
</mcf:PopUpDialog>

<script src="<%=ResolveUrl("~/Dashboard/WidgetGenericScript/SettingsGraphScript.js")%>"></script>
<script type="text/javascript">
    function TreeNodeClick_<%= AssetTreeView.ClientID %>(sender, args) {
        if (args.get_node().get_value() != null && args.get_node().get_value() != "") {
            $(<%= btnConfirmSelectContext.ClientID %>).removeAttr('disabled');
            SaveSelection("<%= hiddenSelection.ClientID %>", '<%= lblSelectedContext.ClientID %>', args.get_node().get_value(), args.get_node().get_text());
        }
        else
        {
            $(<%= btnConfirmSelectContext.ClientID %>).attr('disabled', 'disabled');
        }
    }
</script>