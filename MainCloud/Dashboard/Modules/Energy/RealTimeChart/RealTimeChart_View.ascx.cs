﻿using MainCloudFramework.UI.Modules;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EnergyModule;
using Newtonsoft.Json;
using EnergyModule.Models;
using MainCloudFramework.Web.Multitenants;
using EnergyModule.Services;
using EnergyModule.Core.Exceptions;
using EnergyModule.Core;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using AssetManagement.Services;

namespace MainCloud.Dashboard.Modules.Energy.RealTimeChart
{
    public partial class RealTimeChart_View : WidgetControl<string>
    {
        public RealTimeChart_View() : base(typeof(RealTimeChart_View)) { }

        public const string START_ACTIVEENERGY_COUNTER = "StartActiveEnergyCounter";

        string ChartType
        {
            get
            {
                return "AreaSeries";//this.CustomSettings ?? "AreaSeries";
                //return (string)this.CustomSettings["ChartType"] ?? "LineSeries";
            }
            set
            {
                //this.CustomSettings = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            WidgetAreaRepository widgetAreaRepository = new WidgetAreaRepository();

            this.RealPowerChart.PlotArea.XAxis.Items.Clear();
            this.RealPowerChart.PlotArea.AdditionalYAxes.Clear();
            this.RealPowerChart.PlotArea.Series.Clear();

            //se l'area è RealTime, deve verificare che ci sia asset in memoria altrimenti redirect su area Assets
            string routePath = Page.RouteData.Values["name"] as string;
            WidgetArea page = widgetAreaRepository.FindWidgetAreaByRoutePath(routePath);
            if (page != null)
            {
                if (HttpContext.Current.Items[page.Id + "_PopupAlertOpened"] == null)
                {
                    HttpContext.Current.Items.Add(page.Id + "_PopupAlertOpened", true);
                    if (Session.SessionMultitenant().ContainsKey(EMConstants.SELECTED_ID_CONTEXT) && Session.SessionMultitenant()[EMConstants.SELECTED_ID_CONTEXT] != null)
                    {
                        SelectedContext selectedContext = JsonConvert.DeserializeObject<SelectedContext>((string)Session.SessionMultitenant()[EMConstants.SELECTED_ID_CONTEXT]);
                        SelectedContextService scService = new SelectedContextService();
                        Page.Title += " - " + scService.GetSelectedDescription(selectedContext);

                        CreateChartSeries(this.ChartType);
                    }
                    else
                    {
                        messaggio.Text = "Non è presente nessun asset selezionato, ritornare all'area Assets";
                        AlertPopup.OpenDialog();
                    }
                }
            }
        }

        public void CreateChartSeries(string chartType)
        {
            string nameSerie = "Active Power";
            //using (MainEnergyEntities db = new MainEnergyEntities())
            //{
            //    long id = (long)this.Session["AssetID"];
            //    nameSerie = db.Assets.Where(a => a.Id == id).SingleOrDefault().Description;
            //}
            //if (nameSerie.Contains(" "))
            //{
            //    //nameSerie = nameSerie.Replace(" ", "\n"); //non va, da sistemare
            //}

            if (SessionMultitenant.ContainsKey(EMConstants.SELECTED_ID_CONTEXT) && SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT] != null)
            {
                //create x axis   (max 96 valori -> un valore ogni 15 min, max 4 in un ora)
                for (int i = 1; i <= 96; i++)
                {
                    if (i % 4 == 0)
                    {
                        RealPowerChart.PlotArea.XAxis.Items.Add((i / 4).ToString()); //24 ore
                    }
                    else
                    {
                        RealPowerChart.PlotArea.XAxis.Items.Add("");
                    }
                }
                ////////

                switch (chartType)
                {
                    case "AreaSeries":
                        AreaSeries areaSerie = new AreaSeries();
                        areaSerie.Name = nameSerie;
                        areaSerie.SeriesItems.AddRange(CreateSeriesItem<CategorySeriesItem>("TotalActivePower"));
                        areaSerie.LabelsAppearance.Visible = false;
                        RealPowerChart.PlotArea.Series.Add(areaSerie);
                        break;
                    case "BarSeries":
                        BarSeries barSerie = new BarSeries();
                        barSerie.Name = nameSerie;
                        barSerie.SeriesItems.AddRange(CreateSeriesItem<CategorySeriesItem>("TotalActivePower"));
                        barSerie.LabelsAppearance.Visible = false;
                        RealPowerChart.PlotArea.Series.Add(barSerie);
                        break;
                    case "ColumnSeries":
                        ColumnSeries columnSerie = new ColumnSeries();
                        columnSerie.Name = nameSerie;
                        columnSerie.SeriesItems.AddRange(CreateSeriesItem<CategorySeriesItem>("TotalActivePower"));
                        columnSerie.LabelsAppearance.Visible = false;
                        RealPowerChart.PlotArea.Series.Add(columnSerie);
                        break;
                }

                RealPowerChart.PlotArea.XAxis.TitleAppearance.Text = "Time (h)";

                AxisY addY = new AxisY();
                addY.Name = "AdditionalAxis";
                addY.TitleAppearance.Text = "Active energy total (kWh)";
                RealPowerChart.PlotArea.AdditionalYAxes.Add(addY);

                LineSeries CounterSerie = new LineSeries();
                //CounterSerie.SeriesItems.Add(Convert.ToDecimal(0));
                CounterSerie.SeriesItems.AddRange(CreateSeriesItem<CategorySeriesItem>("TotalActiveEnergyConsumed"));
                CounterSerie.Name = "Active energy";
                CounterSerie.AxisName = "AdditionalAxis";
                CounterSerie.LabelsAppearance.Visible = false;
                RealPowerChart.PlotArea.Series.Add(CounterSerie);

                SelectedContext selectedContext = JsonConvert.DeserializeObject<SelectedContext>((string)SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT]);
                EnergyDataService energyDataService = new EnergyDataService();
                string analyzerId = energyDataService.GetAnalyzerId(selectedContext);
                AnalyzerService AS = new AnalyzerService();
                txtLastUpdate.Text = AS.GetLastUpdate(analyzerId).Value.ToString() ?? "";
            }
            else
            {
                AlertMessage.ShowError("Non è stato selezionato nessun contesto, tornare nell'area asset per poter effettuare la selezione");
            }
        }

        private List<T> CreateSeriesItem<T>(string paramName)
        {
            List<T> items = new List<T>();

            try
            {
                SelectedContext selectedContext = JsonConvert.DeserializeObject<SelectedContext>((string)SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT]);
                EnergyDataService energyDataService = new EnergyDataService();
                string analyzerId = energyDataService.GetAnalyzerId(selectedContext);
                if (!string.IsNullOrEmpty(analyzerId))
                {
                    List<AnalyzerEnergyData> list = energyDataService.GetAnalyzerEnergyDataList(x => x.AnalyzerId == analyzerId && x.TimeStamp >= DateTime.Today);

                    if (list.Count > 0)
                    {
                        if (paramName == "TotalActiveEnergyConsumed")
                        {
                            if (!SessionMultitenant.ContainsKey(EMConstants.START_ACTIVEENERGY_COUNTER) || SessionMultitenant[EMConstants.START_ACTIVEENERGY_COUNTER] == null)
                            {
                                SessionMultitenant[EMConstants.START_ACTIVEENERGY_COUNTER] = energyDataService.GetLastValidateActiveEnergyConsumed(DateTime.Today, analyzerId);

                                //try
                                //{
                                //    Application.ApplicationMultitenant()[EMConstants.START_ACTIVEENERGY_COUNTER] = JsonConvert.DeserializeObject<EnergyRecord>(
                                //        CauseList.First(
                                //        x => JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Count > 0 &&
                                //             JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Exist(paramName) &&
                                //             JsonConvert.DeserializeObject<EnergyRecord>(x.Data).GetData(paramName) > 0
                                //             ).Data).GetData<decimal>(paramName);
                                //}
                                //catch (InvalidOperationException ex)
                                //{
                                //    Application.ApplicationMultitenant()[EMConstants.START_ACTIVEENERGY_COUNTER] = JsonConvert.DeserializeObject<EnergyRecord>(
                                //        CauseList.FirstOrDefault(
                                //        x => JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Count > 0 &&
                                //             JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Exist(paramName)
                                //             ).Data).GetData<decimal>(paramName);
                                //}
                            }
                        }

                        foreach (AnalyzerEnergyData d in list)
                        {
                            EnergyRecord e = JsonConvert.DeserializeObject<EnergyRecord>(d.Data);
                            try
                            {
                                decimal data = e.GetData<decimal>(paramName);

                                if (paramName == "TotalActiveEnergyConsumed")
                                {
                                    if (data > 0)
                                    {
                                        data = Math.Round(data - Convert.ToDecimal(SessionMultitenant[EMConstants.START_ACTIVEENERGY_COUNTER]), 1);
                                        items.Add((T)Activator.CreateInstance(typeof(T), new object[] { data }));
                                    }
                                    else
                                    {
                                        items.Add(items.Last());
                                    }
                                }
                                if (paramName == "TotalActivePower")
                                {
                                    if (data >= 0)
                                    {
                                        data = Math.Round(data / 1000, 1);
                                        items.Add((T)Activator.CreateInstance(typeof(T), new object[] { data }));
                                    }
                                    else
                                    {
                                        items.Add((T)Activator.CreateInstance(typeof(T), new object[] { 0 }));
                                    }
                                }
                            }
                            catch (EnergyDataException ex)
                            {
                                //aggiungo un valore vuoto
                                items.Add((T)Activator.CreateInstance(typeof(T), new object[] { null }));
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Non esistono dati");
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return items;
        }

        protected void btnRedirectOkLink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/App/" + MultiTenantsHelper.ApplicationName + "/Area/Assets");
        }

        //protected void ddlChartType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    this.ChartType = ((DropDownList)sender).SelectedValue;
        //}
    }
}