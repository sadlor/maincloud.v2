﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RealTimeChart_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.RealTimeChart.RealTimeChart_View" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <telerik:RadHtmlChart ID="RealPowerChart" runat="server">
            <ClientEvents OnLoad="chartLoad" />
            <PlotArea>
                <XAxis MaxValue="96" BaseUnit="Hours">
                    <AxisCrossingPoints>
                        <telerik:AxisCrossingPoint Value="0" />
                        <telerik:AxisCrossingPoint Value="96" />
                    </AxisCrossingPoints>
                </XAxis>
                <YAxis>
                    <TitleAppearance Text="Active power total (kW)" />
                </YAxis>
            </PlotArea>
        </telerik:RadHtmlChart>

        <asp:Label Text="LastUpdate: " runat="server" AssociatedControlID="txtLastUpdate" />
        <asp:TextBox runat="server" ID="txtLastUpdate" />
    </ContentTemplate>
</asp:UpdatePanel>

<mcf:Alert runat="server" id="AlertMessage" />

<mcf:PopUpDialog runat="server" ID="AlertPopup" Title="Avviso">
        <Body>
            <asp:label id="messaggio" runat="server"/>
            <asp:Button Text="Ok" runat="server" ID="btnRedirectOkLink" OnClick="btnRedirectOkLink_Click" />
        </Body>
    </mcf:PopUpDialog>

<script type="text/javascript">

    $(function () {
        //UpdateChart();
        //setInterval(function () {
        //    UpdateChart();
        //}, 3180000); //5 min
    });

    function UpdateChart() {
        var request = $.ajax({
            url: '/Dashboard/Modules/RealTimeChart/ReadFtp.svc/UpdateValue',
            method: 'GET',
            //data: JSON.stringify({ filter: _filter }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        });

        request.done(function (response) {
            __doPostBack('<%= this.RealPowerChart.ClientID %>', '');
        });

        request.fail(function (jqXHR, textStatus) {
            //alert("Request failed: " + textStatus);
        });
    }

    (function (global) {
        var chart;

        function ChartLoad(sender, args) {
            chart = sender.get_kendoWidget(); //store a reference to the Kendo Chart widget, we will use its methods
        }

        global.chartLoad = ChartLoad;

        function resizeChart() {
            if (chart)
                chart.resize(); //redraw the chart so it takes the new size of its container when it changes (e.g., browser window size change, parent container size change)
        }


        //this logic ensures that the chart resizing will happen only once, at most - every 200ms
        //to prevent calling the handler too often if old browsers fire the window.onresize event multiple times
        var TO = false;
        window.onresize = function () {
            if (TO !== false)
                clearTimeout(TO);
            TO = setTimeout(resizeChart, 200);
        }

    })(window);
</script>
