﻿using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule.Models;
using EnergyModule.Services;
using MainCloudFramework.UI.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Energy.CosfiAnalysis
{
    public partial class CosfiAnalysis_View : WidgetControl<object>
    {
        public CosfiAnalysis_View() : base(typeof(CosfiAnalysis_View)) { }

        public override bool EnableExport
        {
            get
            {
                return true;
            }
        }

        public override void OnExport()
        {
            CreateTableDataSource(DateStart.Value, DateEnd.Value);

            gridCosfi.ExportSettings.Excel.Format = GridExcelExportFormat.Xlsx;
            gridCosfi.ExportSettings.IgnorePaging = true;
            gridCosfi.ExportSettings.ExportOnlyData = true;
            gridCosfi.ExportSettings.OpenInNewWindow = true;
            gridCosfi.MasterTableView.ExportToExcel();
        }

        DateTime? DateStart
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateStart");
            }
        }

        DateTime? DateEnd
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateEnd");
            }
        } 

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            dtPickStart.SelectedDate = DateStart;
            dtPickEnd.SelectedDate = DateEnd;
        }

        protected void CreateTableDataSource(DateTime start, DateTime end)
        {
            gridCosfi.MasterTableView.Columns.Clear();

            Dictionary<Asset, SortedDictionary<string, decimal>> dataSource = new Dictionary<Asset, SortedDictionary<string, decimal>>();

            AssetService AS = new AssetService();
            List<Asset> assetList = AS.GetAssetList();
            CosfiService CS = new CosfiService();
            foreach (Asset asset in assetList)
            {
                dataSource.Add(asset, CS.GetAverageDaily(asset.AnalyzerId, start, end));
            }

            GridBoundColumn assetColumn = new GridBoundColumn();
            assetColumn.HeaderText = "Asset";
            assetColumn.DataField = "Key.Description";
            assetColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            assetColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
            gridCosfi.MasterTableView.Columns.Add(assetColumn);

            for (DateTime day = start; day <= end; day = day.AddDays(1))
            {
                GridBoundColumn column = new GridBoundColumn();
                column.HeaderText = day.ToString("dd");
                column.ColumnGroupName = "Date";
                column.DataField = "Value[" + day.ToString("dd/MM/yyyy") + "]";
                column.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                column.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                gridCosfi.MasterTableView.Columns.Add(column);
            }

            gridCosfi.DataSource = dataSource;
            gridCosfi.DataBind();
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (dtPickStart.SelectedDate != null && dtPickEnd.SelectedDate != null)
            {
                CustomSettings.AddOrUpdate("DateStart", dtPickStart.SelectedDate.Value);
                CustomSettings.AddOrUpdate("DateEnd", dtPickEnd.SelectedDate.Value);
                
                CreateTableDataSource(DateStart.Value, DateEnd.Value);
                updatePnlData.Update();
            }
            else
            {
                messaggio.Text = "Inserire prima data inizio e fine";
                dlgWidgetConfig.OpenDialog();
            }
        }
    }
}