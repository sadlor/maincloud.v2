﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CosfiAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.CosfiAnalysis.CosfiAnalysis_View" %>

<telerik:RadDockZone ID="dockZoneSettings" runat="server" Orientation="Vertical" BorderStyle="None">
    <telerik:RadDock RenderMode="Lightweight" ID="dockSettings" runat="server" Title="Settings"
        EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false" Resizable="true" Width="100%">
        <ContentTemplate>
            <div class="row" style="width:100%;">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                    <asp:Label Text="Data inizio:" runat="server" AssociatedControlID="dtPickStart" />
                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickStart" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                    <asp:Label Text="Data fine:" runat="server" AssociatedControlID="dtPickEnd" />
                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickEnd" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                    <br />
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:LinkButton runat="server" ID="btnConfirm" Text="Confirm <i class='fa fa-check-circle'></i>"  
                                CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;"
                                OnClick="btnConfirm_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </ContentTemplate>
    </telerik:RadDock>
</telerik:RadDockZone>

<asp:UpdatePanel runat="server" ID="updatePnlData" UpdateMode="Conditional">
    <ContentTemplate>
        <telerik:RadGrid ID="gridCosfi" runat="server" Width="100%">
            <ClientSettings>
                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true" FrozenColumnsCount="1"></Scrolling>
            </ClientSettings>
            <MasterTableView FilterExpression="" Caption="">
                <ColumnGroups>
                    <telerik:GridColumnGroup Name="Date" HeaderText="Periodo">
                        <HeaderStyle HorizontalAlign="Center" />
                    </telerik:GridColumnGroup>
                </ColumnGroups>
            </MasterTableView>
        </telerik:RadGrid>
    </ContentTemplate>
</asp:UpdatePanel>

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
    <Body>
        <asp:label id="messaggio" runat="server"/>
    </Body>
</mcf:PopUpDialog>