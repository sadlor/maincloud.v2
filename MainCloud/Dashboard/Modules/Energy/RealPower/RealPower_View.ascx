﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RealPower_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.RealPower.RealPower_View" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %> 

<asp:UpdatePanel runat="server">
    <ContentTemplate>
<div class="row">
    <telerik:RadRadialGauge runat="server" ID="MeterPotenza" Width="100%">
        <Pointer Value="0">
            <Cap Size="0.1" />
        </Pointer>
        <Scale Min="0" Max="30" MajorUnit="6">
            <Labels Format="{0}" />
            <Ranges>
                <telerik:GaugeRange Color="#8dcb2a" From="6" To="12" />
                <telerik:GaugeRange Color="#ffc700" From="12" To="18" />
                <telerik:GaugeRange Color="#ff7a00" From="18" To="24" />
                <telerik:GaugeRange Color="#c20000" From="24" To="30" />
            </Ranges>
        </Scale>
    </telerik:RadRadialGauge>
<%--<asp:Label Text="KW" runat="server" ID="lblMeter" style="position:relative;left:45%;font-weight:bold;" />--%>
</div>
<div class="row">
    <telerik:RadTextBox runat="server" ID="txtMeter" EnabledStyle-HorizontalAlign="Center"
        style="position:relative;left:25%;font-weight:bold;" ReadOnly="true" />
</div>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">
    
   <%-- var TO = false;
    $(window).resize(function () {
        if (TO !== false)
            clearTimeout(TO);
        TO = setTimeout(Resize, 200);
    })

    function Resize() {
        __doPostBack('<%=MeterPotenza.ClientID %>');
    }--%>

    $(function () {
        UpdateMeter();
        setInterval(function () {
            UpdateMeter();
        }, 60000);
    });

    function UpdateMeter()
    {
        var request = $.ajax({
            url: '<%= MainCloudFramework.Web.Helpers.MultiTenantsHelper.MountAndResolveMultiTenantUrl("/Dashboard/Modules/Energy/RealPower/EnergyMeter.svc/GetValue") %>',
            method: 'GET',
            //data: JSON.stringify({ filter: _filter }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        });

        request.done(function (response) {  //decimal
            if (response.d != -1) {
                if (response.d < 0) {
                    $find("<%=txtMeter.ClientID%>").set_displayValue("0 kW");
                }
                else {
                    $find("<%=txtMeter.ClientID%>").set_displayValue(response.d.toString().replace(".",",") + " kW");
                }
                $find("<%=MeterPotenza.ClientID %>").set_value(response.d);
            }
        });

        request.fail(function (jqXHR, textStatus) {
            //alert("Request failed: " + textStatus);
        });
    }
</script>