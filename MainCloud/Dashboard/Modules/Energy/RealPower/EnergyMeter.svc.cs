﻿using EnergyModule.Core;
using EnergyModule.Services;
using MainCloudFramework.Web.Multitenants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace MainCloud.Dashboard.Modules.Energy.RealPower
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class EnergyMeter
    {
        // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
        // To create an operation that returns XML,
        //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     and include the following line in the operation body:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        [WebGet]
        public decimal GetValue()
        {
            try
            {
                var tenantName = HttpContext.Current.Request.QueryString["MultiTenant"];
                HttpMultitenantsSessionState SessionMultitenant = HttpContext.Current.Session.SessionMultitenant(tenantName);

                if (SessionMultitenant.ContainsKey(EMConstants.SELECTED_ID_CONTEXT) && SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT] != null)
                {
                    SelectedContext selectedContext = JsonConvert.DeserializeObject<SelectedContext>((string)SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT]);

                    EnergyDataService energyDataService = new EnergyDataService();
                    string analyzerId = energyDataService.GetAnalyzerId(selectedContext);
                    if (!string.IsNullOrEmpty(analyzerId))
                    {
                        var energyList = energyDataService.GetAnalyzerEnergyDataList(x => x.AnalyzerId == analyzerId && x.TimeStamp >= DateTime.Today);
                        EnergyRecord last = JsonConvert.DeserializeObject<EnergyRecord>(energyList.LastOrDefault().Data);
                        return Math.Round(last.GetData<decimal>("TotalActivePower") / 1000, 3);
                    }
                    else
                    {
                        //analyzerId null
                        return -1;
                    }
                }
                else
                {
                    throw new Exception("Non è stato selezionato un contesto"); //visualizzare l'avviso
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        // Add more operations here and mark them with [OperationContract]
    }
}
