﻿using AssetManagement.Services;
using EnergyModule.Core;
using EnergyModule.Models;
using EnergyModule.Services;
using MainCloudFramework.UI.Modules;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Energy.RealPower
{
    public partial class RealPower_View : WidgetControl<object>
    {
        public RealPower_View() : base(typeof(RealPower_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionMultitenant.ContainsKey(EMConstants.SELECTED_ID_CONTEXT) && SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT] != null)
            {
                SelectedContext selectedContext = JsonConvert.DeserializeObject<SelectedContext>((string)SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT]);
                EnergyDataService EDS = new EnergyDataService();

                if (EDS.GetPlantEnablePower(selectedContext) > 0)
                {
                    MeterPotenza.Scale.Max = EDS.GetPlantEnablePower(selectedContext);
                    decimal inc = MeterPotenza.Scale.Max / 5;
                    MeterPotenza.Scale.Ranges.Clear();
                    MeterPotenza.Scale.MajorUnit = inc;

                    ColorConverter conv = new ColorConverter();
                    Color[] colors = new Color[] { 
                    (Color)conv.ConvertFromString(""),
                    (Color)conv.ConvertFromString("#8dcb2a"),
                    (Color)conv.ConvertFromString("#ffc700"),
                    (Color)conv.ConvertFromString("#ff7a00"),
                    (Color)conv.ConvertFromString("#c20000")
                    };

                    int c = 0;
                    for (decimal i = 0; i < MeterPotenza.Scale.Max; i = i + inc, c++)
                    {
                        MeterPotenza.Scale.Ranges.Add(new GaugeRange() { From = i, To = i + inc, Color = colors[c] });
                    }
                }
            }
        }
    }
}