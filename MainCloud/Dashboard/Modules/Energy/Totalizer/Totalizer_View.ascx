﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Totalizer_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.Totalizer.Totalizer_View" %>
<%@ Register Src="TotalActiveEnergyConsumedTable.ascx" TagPrefix="uc1" TagName="TotalActiveEnergyConsumed" %>
<%@ Register Src="TotalActivePowerTable.ascx" TagPrefix="uc1" TagName="TotalActivePower" %>
<%@ Register Src="TotalEnergyCostTable.ascx" TagPrefix="uc1" TagName="TotalCost" %>

<link href="/Dashboard/WidgetStyle.css" rel="stylesheet">
<style>
    .vcenter {
    display: inline-block;
    vertical-align: middle;
    float: none;
}
</style>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <asp:RadioButtonList runat="server" ID="rbtnBehaviour" AutoPostBack="false" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Potenza" Value="TotalActivePower" />
                    <asp:ListItem Text="Energia" Value="TotalActiveEnergyConsumed" Selected="True" />
                    <asp:ListItem Text="Costi" Value="Cost" />
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <asp:RadioButtonList ID="rbtnListLastTime" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" style="margin-left:18px;">
                    <asp:ListItem Text="Last day" Value="LastDay" />
                    <asp:ListItem Text="Last month" Value="LastMonth" />
                    <asp:ListItem Text="Last year" Value="LastYear" />
                    <asp:ListItem Text="Current month" Value="CurrentMonth" />
                    <asp:ListItem Text="Current year" Value="CurrentYear" />
                </asp:RadioButtonList>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <asp:Label Text="Data inizio periodo" runat="server" AssociatedControlID="dtPickStart" />
                <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickStart" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" 
                    Calendar-ClientEvents-OnDateSelected="SetNullableLastValue" style="min-width:116px;" Width="100%" />
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <asp:Label Text="Data fine periodo" runat="server" AssociatedControlID="dtPickEnd" />
                <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickEnd" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy"
                    style="min-width:116px;" Width="100%" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                <br />
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="btnConfirm" Text="Confirm <i class='fa fa-check-circle'></i>" runat="server" OnClick="btnConfirm_Click"
                            CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;" />
                        </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
            <Body>
                <asp:label id="messaggio" runat="server"/>
            </Body>
        </mcf:PopUpDialog>
        
        <br />
        <br />
 
        <uc1:TotalActiveEnergyConsumed runat="server" id="tblTotalActiveEnergyConsumed" />
        <uc1:TotalActivePower runat="server" id="tblTotalActivePower" />
        <uc1:TotalCost runat="server" ID="tblTotalCost" />
        
        <asp:UpdateProgress runat="server">
            <ProgressTemplate>
                <div class="loading">Loading&#8230;</div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>

<script src="<%=ResolveUrl("~/Dashboard/WidgetGenericScript/SettingsGraphScript.js")%>"></script>
<script type="text/javascript">
    $(function () {
        $(<%= rbtnListLastTime.ClientID %>).on('click', function () {
            SetDate("<%= dtPickStart.ClientID%>", "<%= dtPickEnd.ClientID%>", "<%= rbtnListLastTime.ClientID%>");
        });
    });

    function SetNullableDate() {
        var startPicker = $find("<%= dtPickStart.ClientID%>");
        startPicker.set_selectedDate(null);
        var endPicker = $find("<%= dtPickEnd.ClientID%>");
        endPicker.set_selectedDate(null);
    }

    function SetNullableLastValue(sender, args) {
        <%--var radioBox = document.getElementById("<%= rbtnListLastTime.ClientID%>"); //non è un componente telerik
        var items = radioBox.getElementsByTagName('input');

        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            item.checked = false;
        }
        return false;--%>
    }
</script>