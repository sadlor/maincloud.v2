﻿using AssetManagement.Models;
using EnergyModule.Core;
using EnergyModule.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Energy.Totalizer
{
    public class TotalizerItemPlant : TotalizerItem
    {
        public Plant Data { get; set; }
        public EnergyData Record { get; set; }
        public List<TotalizerItemDepartment> Departments { get; set; }
        public EnergyConsumption kWhCost { get; set; }

        public override decimal InternalProduction { get; set; }

        /// <summary>
        /// Sommo le produzioni interne dei reparti del sito
        /// Sulla tabella va poi sommato TotalInternalProduction e InternalProduction per trovare il totale
        /// </summary>
        public decimal TotalInternalProduction
        {
            get
            {
                decimal sum = 0;
                foreach (var dep in Departments)
                {
                    sum += dep.InternalProduction + dep.TotalInternalProduction;
                }
                return sum;
            }
        }

        /// <summary>
        /// Se i consumi sono nulli sommo tutti i consumi dei department associati
        /// </summary>
        /// <returns></returns>
        public EnergyConsumption TotalkWhCost
        {
            get
            {
                if (kWhCost == null)
                {
                    EnergyConsumption tot = new EnergyConsumption();
                    tot.kWh = Departments.Sum(x => x.TotalkWhCost.kWh);
                    tot.kWhF1 = Departments.Sum(x => x.TotalkWhCost.kWhF1);
                    tot.kWhF2 = Departments.Sum(x => x.TotalkWhCost.kWhF2);
                    tot.kWhF3 = Departments.Sum(x => x.TotalkWhCost.kWhF3);
                    tot.CostF1 = Departments.Sum(x => x.TotalkWhCost.CostF1);
                    tot.CostF2 = Departments.Sum(x => x.TotalkWhCost.CostF2);
                    tot.CostF3 = Departments.Sum(x => x.TotalkWhCost.CostF3);
                    return tot;
                }
                else
                {
                    return kWhCost;
                }
            }
        }

        public decimal TotalCost
        {
            get
            {
                return TotalkWhCost.CostF1 + TotalkWhCost.CostF2 + TotalkWhCost.CostF3;
            }
        }

        /// <summary>
        /// Potenza Impegnata -> da leggere dall'anagrafica del sito
        /// </summary>
        public decimal PowerEngaged
        {
            get
            {
                if (!string.IsNullOrEmpty(Data.EnablePower))
                {
                    return Convert.ToDecimal(Data.EnablePower, CultureInfo.InvariantCulture);
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Potenza utilizzata -> rilevata da un analizzatore in ingresso (a 0 se non c'è analizzatore associato)
        /// </summary>
        public decimal PowerUsed
        {
            get
            {
                if (kWhCost != null)
                {
                    return kWhCost.kWh;
                }
                else
                {
                    if (Record == null)
                    {
                        decimal sum = 0;
                        foreach (var dep in Departments)
                        {
                            sum += dep.PowerUsed;
                        }
                        return sum;
                    }
                    else
                    {
                        return Convert.ToDecimal(Record.Value, CultureInfo.InvariantCulture);
                    }
                }
                //return Record == null ? 0 : Convert.ToDecimal(Record.Value, CultureInfo.InvariantCulture);
            }
        }

        public decimal TotalPowerUsed
        {
            get
            {
                return PowerUsed + InternalProduction; // - cessione alla rete
            }
        }

        public decimal PowerPercentageOnEngaged
        {
            get
            {
                try
                {
                    return Math.Round((PowerUsed / PowerEngaged) * 100, 2);
                }
                catch { }
                return 0;
            }
        }

        public decimal PowerNotDetected
        {
            get
            {
                return PowerUsed - DetectedValue;
            }
        }

        public decimal PowerPercentageNotDetected
        {
            get
            {
                if (PowerUsed == 0)
                {
                    return 0;
                }
                return Math.Round((PowerNotDetected / PowerUsed) * 100, 2);
            }
        }

        override public decimal DetectedValue
        {
            get
            {
                var tot = Departments.Sum(x => x.DetectedValue);
                return tot;
            }
        }

        override public decimal DetectedMin
        {
            get
            {
                var tot = Departments.Sum(x => x.DetectedMin);
                return tot;
            }
        }

        override public decimal DetectedMax
        {
            get
            {
                var tot = Departments.Sum(x => x.DetectedMax);
                return tot;
            }
        }

        public List<ChartItem> ChartData
        {
            get
            {
                var data = new List<ChartItem>();
                data.Add(new ChartItem("Utilizzata", (double)PowerPercentageOnEngaged, false));
                data.Add(new ChartItem("Non utilizzata", 100 - (double)PowerPercentageOnEngaged, false));

                return data;
            }
        }

    }

    public class ChartItem
    {
        public ChartItem(string name, double marketShare, bool isExploded)
        {
            _name = name;
            _marketShare = marketShare;
            _isExploded = isExploded;
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private double _marketShare;
        public double MarketShare
        {
            get { return _marketShare; }
            set { _marketShare = value; }
        }
        private bool _isExploded;
        public bool IsExploded
        {
            get { return _isExploded; }
            set { _isExploded = value; }
        }
    }
}