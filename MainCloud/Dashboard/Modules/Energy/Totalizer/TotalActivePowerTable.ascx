﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TotalActivePowerTable.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.Totalizer.TotalActivePower" %>
<asp:Repeater ID="rptPlants" runat="server" OnItemDataBound="rptPlants_ItemDataBound">
    <ItemTemplate>
        <table class="table">
            <thead>
                <tr class="info">
                    <th>Sito</th>
                    <th>Dettagli</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                         <asp:LinkButton runat="server" CommandName="RedirectPlant" OnCommand="RedirectLink_Command" CommandArgument='<%# Eval("Data.Id") %>'>
                             <%# Eval("Data.Description") %>
                         </asp:LinkButton>
                        <telerik:RadHtmlChart runat="server" ID="PieChartPlant" Width="200px" Height="200px" Transitions="true" Skin="Silk" OnClientSeriesClicked="OnClientSeriesClicked">
                            <ChartTitle Text=''>
                                <Appearance Align="Center" Position="Top">
                                </Appearance>
                            </ChartTitle>
                            <Legend>
                                <Appearance Position="Top" Visible="true">
                                </Appearance>
                            </Legend>
                            <PlotArea>
                                <Series>
                                    <telerik:PieSeries DataFieldY="MarketShare" NameField="Name" ExplodeField="IsExploded" StartAngle="90">
                                        <LabelsAppearance Position="Center" DataFormatString="{0} %">
                                        </LabelsAppearance>
                                        <TooltipsAppearance Color="White" DataFormatString="{0} %"></TooltipsAppearance>
                                    </telerik:PieSeries>
                                </Series>
                                <YAxis>
                                </YAxis>
                            </PlotArea>
                        </telerik:RadHtmlChart>
                    </td>
                    <td>
                        <table class="table" style="margin-bottom: 0">
                            <tbody>
                                <tr>
                                    <th>Impegnata kw</th>
                                    <td align="right" colspan="5"><%# Eval("PowerEngaged") %></td>
                                </tr>
                                <tr>
                                    <th>Utilizzata kw</th>
                                    <td align="right" colspan="2"><%# Eval("PowerUsed") %></td>
                                    <th>Percentuale di utilizzo sull'impegnato</th>
                                    <td align="right" colspan="2"><%# Eval("PowerPercentageOnEngaged") %> %</td>
                                </tr>
                                <tr>
                                    <th>Totale rilevato kw</th>
                                    <td align="right"><%# Eval("DetectedValue") %></td>
                                    <th colspan="3">Min kw</th>
                                    <td><%# Eval("DetectedMin") %></td>
                                    <th colspan="3">Max kw</th>
                                    <td align="right"><%# Eval("DetectedMax") %></td>
                                </tr>
                                <tr>
                                    <th>Potenza non rilevata kw</th>
                                    <td align="right" colspan="2"><%# Eval("PowerNotDetected") %></td>
                                    <th>Percentuale potenza non rilevata su utilizzata</th>
                                    <td align="right" colspan="2"><%# Eval("PowerPercentageNotDetected") %> %</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 5%">
                        <asp:Repeater ID="rptDepartments" DataSource='<%#Bind("Departments")%>' runat="server">
                            <ItemTemplate>
                                <table class="table" style="margin-bottom: 0">
                                    <thead>
                                        <tr class="warning">
                                            <th style="width:40%;">Reparto</th>
                                            <th>Dettagli</th>
                                            <%--<th style="width:30%;">kw</t>
                                            <th style="width:30%;">%</th>--%>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:LinkButton runat="server" CommandName="RedirectDepartment" OnCommand="RedirectLink_Command" CommandArgument='<%# Eval("Data.Id") %>'>
                                                    <%# Eval("Data.Description") %>
                                                </asp:LinkButton>
                                            </td>
                                            <td>
                                                <table class="table" style="margin-bottom: 0">
                                                    <tbody>
                                                        <tr>
                                                            <th>Utilizzata kw</th>
                                                            <td align="right" colspan="2"><%# Eval("PowerUsed") %></td>
                                                            <th>Percentuale su utilizzata sito</th>
                                                            <td align="right" colspan="2"><%# ((MainCloud.Dashboard.Modules.Energy.Totalizer.TotalizerItemDepartment)Container.DataItem)
                                                        .DetectValuePerc((MainCloud.Dashboard.Modules.Energy.Totalizer.TotalizerItemPlant)((RepeaterItem)Container.Parent.Parent).DataItem) %></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Totale rilevato kw</th>
                                                            <td align="right"><%# Eval("DetectedValue") %></td>
                                                            <%--<th colspan="3">Min kw</th>
                                                            <td><%# Eval("DetectedMin") %></td>
                                                            <th colspan="3">Max kw</th>
                                                            <td><%# Eval("DetectedMax") %></td>--%>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="padding-left: 5%">
                                                <table class="table" style="margin-bottom: 0">
                                                    <thead>
                                                        <tr class="active">
                                                            <th style="width:50%;">Asset</th>
                                                            <th style="width:25%;text-align:right;">kw</th>
                                                            <th style="width:25%;text-align:right;">%</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:Repeater ID="rptAssets" DataSource='<%#Bind("Assets")%>' runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton runat="server" CommandName="RedirectAsset" OnCommand="RedirectLink_Command" CommandArgument='<%# Eval("Data.Id") %>'>
                                                                            <%# Eval("Data.Description") %>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                    <td align="right">
                                                                        <%# Eval("DetectedValue") %>
                                                                    </td>
                                                                    <td align="right">
                                                                        <%# ((MainCloud.Dashboard.Modules.Energy.Totalizer.TotalizerItemAsset)Container.DataItem)
                                                                                    .DetectValuePerc((MainCloud.Dashboard.Modules.Energy.Totalizer.TotalizerItemPlant)((RepeaterItem)Container.Parent.Parent.Parent.Parent).DataItem) %>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </tbody>
        </table>
    </ItemTemplate>
</asp:Repeater>

<script type="text/javascript">
    function OnClientSeriesClicked(sender, args) {
        var theDataItem = args.get_dataItem();
        theDataItem.IsExploded = !theDataItem.IsExploded;
        sender.repaint();
    }
</script>