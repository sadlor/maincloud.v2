﻿using AssetManagement.Models;
using EnergyModule.Core;
using EnergyModule.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MainCloud.Dashboard.Modules.Energy.Totalizer
{
    public class TotalizerItemDepartment : TotalizerItem
    {
        public Department Data { get; set; }
        public EnergyData Record { get; set; }
        public List<TotalizerItemAsset> Assets { get; set; }
        public EnergyConsumption kWhCost { get; set; }

        public override decimal InternalProduction { get; set; }
        
        /// <summary>
        /// Sommo le produzioni interne degli assets del reparto
        /// Sulla tabella va poi sommato TotalInternalProduction e InternalProduction per trovare il totale
        /// </summary>
        public decimal TotalInternalProduction
        {
            get
            {
                decimal sum = 0;
                foreach (var a in Assets)
                {
                    sum += a.InternalProduction;
                }
                return sum;
            }
        }

        /// <summary>
        /// Se i consumi sono nulli sommo tutti i consumi degli asset associati
        /// </summary>
        /// <returns></returns>
        public EnergyConsumption TotalkWhCost
        {
            get
            {
                if (kWhCost == null)
                {
                    EnergyConsumption tot = new EnergyConsumption();
                    tot.kWh = Assets.Sum(x => x.kWhCost.kWh);
                    tot.kWhF1 = Assets.Sum(x => x.kWhCost.kWhF1);
                    tot.kWhF2 = Assets.Sum(x => x.kWhCost.kWhF2);
                    tot.kWhF3 = Assets.Sum(x => x.kWhCost.kWhF3);
                    tot.CostF1 = Assets.Sum(x => x.kWhCost.CostF1);
                    tot.CostF2 = Assets.Sum(x => x.kWhCost.CostF2);
                    tot.CostF3 = Assets.Sum(x => x.kWhCost.CostF3);
                    return tot;
                }
                else
                {
                    return kWhCost;
                }
            }
        }

        public decimal TotalCost
        {
            get
            {
                return TotalkWhCost.CostF1 + TotalkWhCost.CostF2 + TotalkWhCost.CostF3;
            }
        }

        /// <summary>
        /// Potenza utilizzata -> rilevata da un analizzatore in ingresso (a 0 se non c'è analizzatore associato)
        /// </summary>
        public decimal PowerUsed
        {
            get
            {
                if (kWhCost != null)
                {
                    return kWhCost.kWh;
                }
                else
                {
                    return Record == null ? 0 : Convert.ToDecimal(Record.Value, CultureInfo.InvariantCulture);
                }
            }
        }

        override public decimal DetectedValue
        {
            get
            {
                var tot = Assets.Sum(x => x.DetectedValue);
                return tot;
            }
        }

        override public decimal DetectedMin
        {
            get
            {
                var tot = Assets.Sum(x => x.DetectedMin);
                return tot;
            }
        }

        override public decimal DetectedMax
        {
            get
            {
                var tot = Assets.Sum(x => x.DetectedMax);
                return tot;
            }
        }

        public decimal DetectValuePerc(TotalizerItemPlant totPlant)
        {
            if (totPlant.PowerUsed == 0)
            {
                return 0;
            }
            return Math.Round((DetectedValue / totPlant.PowerUsed) * 100, 2);
        }

        public decimal PowerUsedPerc(TotalizerItemPlant totPlant)
        {
            if (totPlant.PowerUsed == 0)
            {
                return 0;
            }
            if (PowerUsed == 0)
            {
                return 0;
            }
            return Math.Round((PowerUsed / totPlant.PowerUsed) * 100, 2);
        }
    }
}