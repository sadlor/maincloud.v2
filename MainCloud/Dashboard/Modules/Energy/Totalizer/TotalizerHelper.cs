﻿using AssetManagement.Core;
using AssetManagement.Models;
using EnergyModule;
using EnergyModule.Core;
using EnergyModule.Models;
using EnergyModule.Services;
using MainCloud.Dashboard.Modules.Energy.History;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Energy.Totalizer
{
    public static class TotalizerHelper
    {
        public static EnergyData GetInternalProduction(Plant plant, DateTime start, DateTime end)
        {
            //per autoproduzione
            EnergyData internalProductionData = null;
            foreach (var department in plant.Department)
            {
                foreach (var asset in department.Asset)
                {
                    if (asset.AssetGroup != null && asset.AssetGroup.Description == "Auto Produzione")
                    {
                        EnergyData assetEnergyData = null;
                        if (!string.IsNullOrEmpty(asset.AnalyzerId))
                        {
                            EnergyDataService energyDataService = new EnergyDataService();
                            assetEnergyData = energyDataService.GetTotalizerEnergyData(asset.AnalyzerId, start, end, "TotalActiveEnergyProduced");
                            if (assetEnergyData != null)
                            {
                                internalProductionData.Value = Convert.ToDecimal(internalProductionData.Value, CultureInfo.InvariantCulture) + Convert.ToDecimal(assetEnergyData.Value, CultureInfo.InvariantCulture);
                            }
                        }
                    }
                }
            }
            if (internalProductionData != null)
            {
                internalProductionData.Code = "TotalActiveEnergyProduced";
                internalProductionData.Min = internalProductionData.Value;
                internalProductionData.Max = internalProductionData.Value;
            }
            return internalProductionData;
        }

        /// <summary>
        /// Calcola, se esiste, l'energia prodotta internamente
        /// </summary>
        /// <param name="context"> Asset/Department/Plant object </param>
        /// <param name="type"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static EnergyData GetInternalProduction(object context, AssetManagementEnum.AnalyzerContext type, DateTime start, DateTime end)
        {
            EnergyData internalProductionData = null;
            EnergyDataService energyDataService = new EnergyDataService();
            switch (type)
            {
                case AssetManagementEnum.AnalyzerContext.Asset:
                    Asset asset = context as Asset;
                    EnergyData assetEnergyData = null;
                    if (!string.IsNullOrEmpty(asset.AnalyzerId))
                    {
                        assetEnergyData = energyDataService.GetTotalizerEnergyData(asset.AnalyzerId, start, end, "TotalActiveEnergyProduced");
                        if (assetEnergyData != null)
                        {
                            internalProductionData.Value = Convert.ToDecimal(internalProductionData.Value, CultureInfo.InvariantCulture) + Convert.ToDecimal(assetEnergyData.Value, CultureInfo.InvariantCulture);
                        }
                    }
                    break;
                case AssetManagementEnum.AnalyzerContext.Department:
                    Department department = context as Department;
                    EnergyData depEnergyData = null;
                    if (!string.IsNullOrEmpty(department.AnalyzerId))
                    {
                        depEnergyData = energyDataService.GetTotalizerEnergyData(department.AnalyzerId, start, end, "TotalActiveEnergyProduced");
                        if (depEnergyData != null)
                        {
                            internalProductionData.Value = Convert.ToDecimal(internalProductionData.Value, CultureInfo.InvariantCulture) + Convert.ToDecimal(depEnergyData.Value, CultureInfo.InvariantCulture);
                        }
                    }
                    break;
                case AssetManagementEnum.AnalyzerContext.Plant:
                    Plant plant = context as Plant;
                    EnergyData plantEnergyData = null;
                    if (!string.IsNullOrEmpty(plant.AnalyzerId))
                    {
                        plantEnergyData = energyDataService.GetTotalizerEnergyData(plant.AnalyzerId, start, end, "TotalActiveEnergyProduced");
                        if (plantEnergyData != null)
                        {
                            internalProductionData.Value = Convert.ToDecimal(internalProductionData.Value, CultureInfo.InvariantCulture) + Convert.ToDecimal(plantEnergyData.Value, CultureInfo.InvariantCulture);
                        }
                    }
                    break;
            }
            
            if (internalProductionData != null)
            {
                internalProductionData.Code = "TotalActiveEnergyProduced";
                internalProductionData.Min = internalProductionData.Value;
                internalProductionData.Max = internalProductionData.Value;
            }
            return internalProductionData;
        }
    }
}