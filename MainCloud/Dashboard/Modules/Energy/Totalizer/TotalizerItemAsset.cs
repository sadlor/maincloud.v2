﻿using AssetManagement.Models;
using EnergyModule.Core;
using EnergyModule.Models;
using System;

namespace MainCloud.Dashboard.Modules.Energy.Totalizer
{
    public class TotalizerItemAsset : TotalizerItem
    {
        public Asset Data { get; set; }
        public EnergyData Record { get; set; }
        public EnergyConsumption kWhCost { get; set; }

        public decimal? TotalCost
        {
            get
            {
                if (kWhCost != null)
                {
                    return kWhCost.CostF1 + kWhCost.CostF2 + kWhCost.CostF3;
                }
                return null;
            }
        }

        public override decimal InternalProduction { get; set; }

        public override decimal DetectedValue
        {
            get
            {
                if (kWhCost != null)
                {
                    return kWhCost.kWh;
                }
                else
                {
                    return Record == null ? 0 : Convert.ToDecimal(Record.Value);
                }
            }
        }

        public override decimal DetectedMin
        {
            get
            {
                return Record == null ? 0 : Convert.ToDecimal(Record.Min);
            }
        }

        public override decimal DetectedMax
        {
            get
            {
                return Record == null ? 0 : Convert.ToDecimal(Record.Max);
            }
        }

        public decimal DetectValuePerc(TotalizerItemPlant totPlant)
        {
            if (totPlant.PowerUsed == 0)
            {
                return 0;
            }
            return Math.Round((DetectedValue / totPlant.PowerUsed) * 100, 2);
        }
    }
}