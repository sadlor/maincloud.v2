﻿using AssetManagement.Core;
using EnergyModule.Core;
using EnergyModule.Services;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web.Multitenants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Energy.Totalizer
{
    public partial class TotalEnergyCostTable : System.Web.UI.UserControl
    {
        public List<TotalizerItemPlant> TableDataSource
        {
            set
            {
                rptPlants.DataSource = value;
            }
            get
            {
                return rptPlants.DataSource as List<TotalizerItemPlant>;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void RedirectLink_Command(object sender, CommandEventArgs e)
        {
            SelectedContext context = null;
            switch (e.CommandName)
            {
                case "RedirectAsset":
                    context = new SelectedContext(e.CommandArgument.ToString(), AssetManagementEnum.AnalyzerContext.Asset);
                    break;
                case "RedirectDepartment":
                    context = new SelectedContext(e.CommandArgument.ToString(), AssetManagementEnum.AnalyzerContext.Department);
                    break;
                case "RedirectPlant":
                    context = new SelectedContext(e.CommandArgument.ToString(), AssetManagementEnum.AnalyzerContext.Plant);
                    break;
            }

            //Salvo in sessione e vado in real time
            Session.SessionMultitenant()[EMConstants.SELECTED_ID_CONTEXT] = JsonConvert.SerializeObject(context);
            Session.SessionMultitenant()[EMConstants.START_ACTIVEENERGY_COUNTER] = null;
            Session.SessionMultitenant()[EMConstants.START_REACTIVEENERGY_COUNTER] = null;
            Response.Redirect("~/App/" + MultiTenantsHelper.ApplicationName + "/Area/RealTime");
        }
    }
}