﻿using EnergyModule;
using MainCloudFramework.UI.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MainCloudFramework.Web.Multitenants;
using EnergyModule.Models;
using Telerik.Web.UI;
using Newtonsoft.Json;
using System.Globalization;
using MainCloudFramework.UI.Containers;
using System.ComponentModel.DataAnnotations;
using MainCloud.Dashboard.Modules.Energy.History;
using MainCloudFramework.Web.Helpers;
using System.Data.Entity;
using EnergyModule.Services;
using EnergyModule.Core;
using EnergyModule.ConsumptionStorage;
using AssetManagement.Models;
using AssetManagement.Services;

namespace MainCloud.Dashboard.Modules.Energy.Totalizer
{
    public partial class Totalizer_View : WidgetControl<object>
    {
        public Totalizer_View() : base(typeof(Totalizer_View)) { }

        protected List<Plant> plantList = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (dtPickStart.SelectedDate != null && dtPickEnd.SelectedDate != null)
            {
                string paramName = rbtnBehaviour.SelectedValue;
                if (rbtnBehaviour.SelectedItem.Value == "TotalActivePower")
                {
                    CreateTableDataSource(paramName, dtPickStart.SelectedDate.Value, dtPickEnd.SelectedDate.Value);
                }
                else
                {
                    CreateTableDataSourceCost(paramName, dtPickStart.SelectedDate.Value, dtPickEnd.SelectedDate.Value);
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate("Behaviour", rbtnBehaviour.SelectedValue);

            if ((dtPickStart.SelectedDate == null || dtPickEnd.SelectedDate == null))
            {
                messaggio.Text = "Inserire prima data inizio e fine";
                dlgWidgetConfig.OpenDialog();
            }
            //else
            //{
            //    if ((dtPickEnd.SelectedDate.Value - dtPickStart.SelectedDate.Value).Days > 62)
            //    {
            //        if ((dtPickEnd.SelectedDate.Value - dtPickStart.SelectedDate.Value).Days > 730)
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime", "Year");
            //        }
            //        else
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime", "Month");
            //        }
            //    }
            //    else
            //    {
            //        CustomSettings.AddOrUpdate("TypeOfTime", "Day");
            //    }
            //}

        }

        private void CreateTableDataSourceCost(string paramName, DateTime start, DateTime end)
        {
            //string paramName = EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString();
            EnergyConsumptionService ECS = new EnergyConsumptionService();

            if (plantList == null)
            {
                PlantService plantService = new PlantService();
                plantList = plantService.PlantList();

                var plants = new SortedDictionary<string, TotalizerItemPlant>();
                var departments = new SortedDictionary<string, TotalizerItemDepartment>();
                var assets = new SortedDictionary<string, TotalizerItemAsset>();

                var energyDataSource = new List<TotalizerItemPlant>();

                //EnergyDataService energyDataService = new EnergyDataService();
                //ConsumptionStorage storage = new ConsumptionStorage();

                foreach (Plant plant in plantList)
                {
                    EnergyConsumption plantCost = null;
                    if (!string.IsNullOrEmpty(plant.AnalyzerId))
                    {
                        plantCost = ECS.GetTotalizerCostConsumption(plant.AnalyzerId, start, end);
                        //plantCost = storage.GetTotalizerCostConsumption(plant.AnalyzerId, start, end);

                    }
                    if (!plants.ContainsKey(plant.Id))
                    {
                        plants[plant.Id] = new TotalizerItemPlant()
                        {
                            Data = plant,
                            Departments = new List<TotalizerItemDepartment>(),
                            kWhCost = plantCost
                        };
                    }
                    foreach (Department dep in plant.Department.OrderBy(x => x.Description))
                    {
                        EnergyConsumption depCost = null;
                        if (!string.IsNullOrEmpty(dep.AnalyzerId))
                        {
                            //depCost = storage.GetTotalizerCostConsumption(dep.AnalyzerId, start, end);
                            depCost = ECS.GetTotalizerCostConsumption(dep.AnalyzerId, start, end);
                        }
                        if (!departments.ContainsKey(dep.Id))
                        {
                            departments[dep.Id] = new TotalizerItemDepartment()
                            {
                                Data = dep,
                                Assets = new List<TotalizerItemAsset>(),
                                kWhCost = depCost
                            };
                        }
                        foreach (Asset asset in dep.Asset.OrderBy(x => x.Description))
                        {
                            EnergyConsumption assetCost = null;
                            if (!string.IsNullOrEmpty(asset.AnalyzerId))
                            {
                                //assetCost = storage.GetTotalizerCostConsumption(asset.AnalyzerId, start, end);
                                assetCost = ECS.GetTotalizerCostConsumption(asset.AnalyzerId, start, end);
                            }
                            if (!assets.ContainsKey(asset.Id))
                            {
                                assets[asset.Id] = new TotalizerItemAsset()
                                {
                                    Data = asset,
                                    kWhCost = assetCost
                                };
                                departments[dep.Id].Assets.Add(assets[asset.Id]);
                            }
                        }
                        plants[plant.Id].Departments.Add(departments[dep.Id]);
                    }
                    energyDataSource.Add(plants[plant.Id]);
                }
                
                tblTotalActivePower.Visible = false;
                tblTotalActiveEnergyConsumed.Visible = false;
                tblTotalCost.Visible = false;
                if (paramName == EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString())
                {
                    tblTotalActiveEnergyConsumed.TableDataSource = energyDataSource;
                    tblTotalActiveEnergyConsumed.DataBind();
                    tblTotalActiveEnergyConsumed.Visible = true;
                }
                else
                {
                    tblTotalCost.TableDataSource = energyDataSource;
                    tblTotalCost.DataBind();
                    tblTotalCost.Visible = true;
                }
            }
        }

        private void CreateTableDataSource(string paramName, DateTime start, DateTime end)
        {
            if (plantList == null)
            {
                PlantService plantService = new PlantService();
                plantList = plantService.PlantList();

                var plants = new SortedDictionary<string, TotalizerItemPlant>();
                var departments = new SortedDictionary<string, TotalizerItemDepartment>();
                var assets = new SortedDictionary<string, TotalizerItemAsset>();

                var energyDataSource = new List<TotalizerItemPlant>();

                EnergyDataService energyDataService = new EnergyDataService();

                foreach (Plant plant in plantList)
                {
                    EnergyData plantEnergyData = null;
                    EnergyData e = null;
                    if (!string.IsNullOrEmpty(plant.AnalyzerId))
                    {
                        plantEnergyData = energyDataService.GetTotalizerEnergyData(plant.AnalyzerId, start, end, paramName);
                        //e = TotalizerHelper.GetInternalProduction(plant, AssetManagementEnum.AnalyzerContext.Plant, start, end);
                    }
                    if (!plants.ContainsKey(plant.Id))
                    {
                        plants[plant.Id] = new TotalizerItemPlant()
                        {
                            Data = plant,
                            Record = plantEnergyData,
                            Departments = new List<TotalizerItemDepartment>(),
                            InternalProduction = e == null ? 0 : Convert.ToDecimal(e.Value, CultureInfo.InvariantCulture)
                        };
                    }
                    foreach (Department dep in plant.Department.OrderBy(x => x.Description))
                    {
                        EnergyData depEnergyData = null;
                        e = null;
                        if (!string.IsNullOrEmpty(dep.AnalyzerId))
                        {
                            depEnergyData = energyDataService.GetTotalizerEnergyData(dep.AnalyzerId, start, end, paramName);
                            //e = TotalizerHelper.GetInternalProduction(dep, AssetManagementEnum.AnalyzerContext.Department, start, end);
                        }
                        if (!departments.ContainsKey(dep.Id))
                        {
                            departments[dep.Id] = new TotalizerItemDepartment()
                            {
                                Data = dep,
                                Record = depEnergyData,
                                Assets = new List<TotalizerItemAsset>(),
                                InternalProduction = e == null ? 0 : Convert.ToDecimal(e.Value, CultureInfo.InvariantCulture)
                            };
                        }
                        foreach (Asset asset in dep.Asset.OrderBy(x => x.Description))
                        {
                            EnergyData assetEnergyData = null;
                            e = null;
                            if (!string.IsNullOrEmpty(asset.AnalyzerId))
                            {
                                assetEnergyData = energyDataService.GetTotalizerEnergyData(asset.AnalyzerId, start, end, paramName);
                                //e = TotalizerHelper.GetInternalProduction(asset, AssetManagementEnum.AnalyzerContext.Asset, start, end);
                            }
                            if (!assets.ContainsKey(asset.Id))
                            {
                                assets[asset.Id] = new TotalizerItemAsset()
                                {
                                    Data = asset,
                                    Record = assetEnergyData,
                                    InternalProduction = e == null ? 0 : Convert.ToDecimal(e.Value, CultureInfo.InvariantCulture)
                                };
                                departments[dep.Id].Assets.Add(assets[asset.Id]);
                            }
                        }
                        plants[plant.Id].Departments.Add(departments[dep.Id]);
                    }
                    energyDataSource.Add(plants[plant.Id]);
                }

                tblTotalActivePower.Visible = false;
                tblTotalActiveEnergyConsumed.Visible = false;
                tblTotalCost.Visible = false;

                if (paramName == EnergyEnum.ParamToRead.TotalActivePower.ToString())
                {
                    tblTotalActivePower.TableDataSource = energyDataSource;
                    tblTotalActivePower.DataBind();
                    tblTotalActivePower.Visible = true;
                }
                else if (paramName == EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString())
                {
                    tblTotalActiveEnergyConsumed.TableDataSource = energyDataSource;
                    tblTotalActiveEnergyConsumed.DataBind();
                    tblTotalActiveEnergyConsumed.Visible = true;
                }
            }
        }
    }
}