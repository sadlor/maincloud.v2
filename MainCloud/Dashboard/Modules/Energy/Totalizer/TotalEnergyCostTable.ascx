﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TotalEnergyCostTable.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.Totalizer.TotalEnergyCostTable" %>

<asp:Repeater ID="rptPlants" runat="server">
    <ItemTemplate>
        <table class="table" style="width:100%;">
            <thead>
                <tr class="info">
                    <th class="col-xs-4 col-sm-4 col-md-4">Sito</th>
                    <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">kWh F1</th>
                    <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">kWh F2</th>
                    <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">kWh F3</th>
                    <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;background-color:yellow;">TotkWh</th>
                    <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">F1(€)</th>
                    <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">F2(€)</th>
                    <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">F3(€)</th>
                    <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;background-color:limegreen;">Tot(€)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                         <asp:LinkButton runat="server" CommandName="RedirectPlant" OnCommand="RedirectLink_Command" CommandArgument='<%# Eval("Data.Id") %>'>
                             <%# Eval("Data.Description") %>
                         </asp:LinkButton>
                    </td>
                    <td align="right">
                        <%# Eval("TotalkWhCost.kWhF1", "{0:0.0}") %>
                    </td>
                    <td align="right">
                        <%# Eval("TotalkWhCost.kWhF2", "{0:0.0}") %>
                    </td>
                    <td align="right">
                        <%# Eval("TotalkWhCost.kWhF3", "{0:0.0}") %>
                    </td>
                    <td style="text-align:right;">
                        <%# Eval("TotalkWhCost.kWh", "{0:0.0}") %>
                    </td>
                    <td align="right">
                        <%# Eval("TotalkWhCost.CostF1", "{0:0.0}") %>
                    </td>
                    <td align="right">
                        <%# Eval("TotalkWhCost.CostF2", "{0:0.0}") %>
                    </td>
                    <td align="right">
                        <%# Eval("TotalkWhCost.CostF3", "{0:0.0}") %>
                    </td>
                    <td align="right">
                        <%# Eval("TotalCost", "{0:0.0}") %>
                    </td>
                </tr>
                <tr>
                    <td colspan="12">
                        <asp:Repeater ID="rptDepartments" DataSource='<%#Bind("Departments")%>' runat="server">
                            <ItemTemplate>
                                <table class="table" style="margin-bottom: 0">
                                    <thead>
                                        <tr class="warning">
                                            <%--<th class="col-xs-3 col-sm-3 col-md-3">Reparto</th>
                                            <th class="col-xs-5 col-sm-5 col-md-5">Consumi kWh</th>
                                            <th class="col-xs-4 col-sm-4 col-md-4">% sul sito</th  width:10%;>--%>
                                            <th class="col-xs-4 col-sm-4 col-md-4" style="padding-left:5%;">Reparto</th>
                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">kWh F1</th>
                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">kWh F2</th>
                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">kWh F3</th>
                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;background-color:yellow;">TotkWh</th>
                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">F1(€)</th>
                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">F2(€)</th>
                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">F3(€)</th>
                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;background-color:limegreen;">Tot(€)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="col-xs-4 col-sm-4 col-md-4" style="padding-left:5%;">
                                                 <asp:LinkButton runat="server" CommandName="RedirectDepartment" OnCommand="RedirectLink_Command" CommandArgument='<%# Eval("Data.Id") %>'>
                                                     <%# Eval("Data.Description") %>
                                                 </asp:LinkButton>
                                            </td>
                                            <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                <%# Eval("TotalkWhCost.kWhF1", "{0:0.0}") %>
                                            </td>
                                            <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                <%# Eval("TotalkWhCost.kWhF2", "{0:0.0}") %>
                                            </td>
                                            <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                <%# Eval("TotalkWhCost.kWhF3", "{0:0.0}") %>
                                            </td>
                                            <td class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">
                                                <%# Eval("TotalkWhCost.kWh", "{0:0.0}") %>
                                            </td>
                                            <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                <%# Eval("TotalkWhCost.CostF1", "{0:0.0}") %>
                                            </td>
                                            <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                <%# Eval("TotalkWhCost.CostF2", "{0:0.0}") %>
                                            </td>
                                            <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                <%# Eval("TotalkWhCost.CostF3", "{0:0.0}") %>
                                            </td>
                                            <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                <%# Eval("TotalCost", "{0:0.0}") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="9">
                                                <table class="table" style="margin-bottom: 0">
                                                    <thead>
                                                        <tr class="active">
                                                            <th class="col-xs-4 col-sm-4 col-md-4" style="padding-left:7%;">Asset</th>
                                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">kWh F1</th>
                                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">kWh F2</th>
                                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">kWh F3</th>
                                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;background-color:yellow;">TotkWh</th>
                                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">F1(€)</th>
                                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">F2(€)</th>
                                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">F3(€)</th>
                                                            <th class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;background-color:limegreen;">Tot(€)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:Repeater ID="rptAssets" DataSource='<%#Bind("Assets")%>' runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td class="col-xs-4 col-sm-4 col-md-4" style="padding-left:7%;">
                                                                        <asp:LinkButton runat="server" CommandName="RedirectAsset" OnCommand="RedirectLink_Command" CommandArgument='<%# Eval("Data.Id") %>'>
                                                                            <%# Eval("Data.Description") %>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                    <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                                        <%# Eval("kWhCost.kWhF1", "{0:0.0}") %>
                                                                    </td>
                                                                    <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                                        <%# Eval("kWhCost.kWhF2", "{0:0.0}") %>
                                                                    </td>
                                                                    <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                                        <%# Eval("kWhCost.kWhF3", "{0:0.0}") %>
                                                                    </td>
                                                                    <td class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;">
                                                                        <%# Eval("kWhCost.kWh", "{0:0.0}") %>
                                                                    </td>
                                                                    <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                                        <%# Eval("kWhCost.CostF1", "{0:0.0}") %>
                                                                    </td>
                                                                    <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                                        <%# Eval("kWhCost.CostF2", "{0:0.0}") %>
                                                                    </td>
                                                                    <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                                        <%# Eval("kWhCost.CostF3", "{0:0.0}") %>
                                                                    </td>
                                                                    <td class="col-xs-1 col-sm-1 col-md-1" align="right">
                                                                        <%# Eval("TotalCost", "{0:0.0}") %>
                                                                    </td>
                                                                </tr>    
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </tbody>
        </table>
    </ItemTemplate>
</asp:Repeater>