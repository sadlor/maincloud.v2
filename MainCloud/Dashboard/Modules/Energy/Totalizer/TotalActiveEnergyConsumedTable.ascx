﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TotalActiveEnergyConsumedTable.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.Totalizer.TotalActiveEnergyConsumed" %>
<asp:Repeater ID="rptPlants" runat="server">
    <ItemTemplate>
        <table class="table">
            <thead>
                <tr class="info">
                    <th>Sito</th>
                    <th>Dettagli</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                         <asp:LinkButton runat="server" CommandName="RedirectPlant" OnCommand="RedirectLink_Command" CommandArgument='<%# Eval("Data.Id") %>'>
                             <%# Eval("Data.Description") %>
                         </asp:LinkButton>
                    </td>
                    <td>
                        <table class="table" style="margin-bottom: 0">
                            <tbody>
                                <%--<tr>
                                    <th>Impegnata kwh</th>
                                    <td colspan="5"><%# Eval("PowerEngaged") %></td>
                                </tr>--%>
                                <tr>
                                    <th>Ingresso Enel kwh</th>
                                    <td align="right" colspan="2"><%# Eval("PowerUsed", "{0:0.0}") %></td>
                                    <%--<th>Percentuale di utilizzo sull'impegnato</th>
                                    <td colspan="2"><%# Eval("PowerPercentageOnEngaged") %> %</td>--%>
                                </tr>
                                <tr>
                                    <th>Autoproduzione</th>
                                    <td align="right" colspan="2"><%# Eval("InternalProduction", "{0:0.0}") %></td>
                                </tr>
                                <tr>
                                    <th>Cessione alla rete</th>
                                    <td align="right" colspan="2">0.0</td>
                                </tr>
                                <tr>
                                    <th>Totale utilizzata kwh</th>
                                    <%--<td align="right" colspan="2"><%# Eval("kWhCost.kWh", "{0:0.0}") %></td>--%>
                                    <td align="right" colspan="2"><%# Eval("TotalPowerUsed", "{0:0.0}") %></td>
                                </tr>
                                <tr>
                                    <th>Totale rilevata kwh</th>
                                    <%--<td align="right"><%# Eval("TotalkWhCost.kWh", "{0:0.0}") %></td>--%>
                                    <td align="right"><%# Eval("DetectedValue", "{0:0.0}") %></td>
                                    <%--<th>Min kwh</th>
                                    <td><%# Eval("DetectedMin") %></td>
                                    <th>Max kwh</th>
                                    <td><%# Eval("DetectedMax") %></td>--%>
                                </tr>
                                <tr>
                                    <th>Totale non rilevata kw</th>
                                    <td align="right" colspan="2"><%# Eval("PowerNotDetected", "{0:0.0}") %></td>
                                    <th>Percentuale non rilevata su utilizzata</th>
                                    <td align="right" colspan="2"><%# Eval("PowerPercentageNotDetected") %> %</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 5%">
                        <asp:Repeater ID="rptDepartments" DataSource='<%#Bind("Departments")%>' runat="server">
                            <ItemTemplate>
                                <table class="table" style="margin-bottom: 0">
                                    <thead>
                                        <tr class="warning">
                                            <th class="col-xs-3 col-sm-3 col-md-3">Reparto</th>
                                            <th class="col-xs-5 col-sm-5 col-md-5">Consumi kWh</th>
                                            <th class="col-xs-4 col-sm-4 col-md-4">% sul sito</th>
                                            <%--<th style="width:30%;">%</th>--%>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="col-xs-3 col-sm-3 col-md-3">
                                                 <asp:LinkButton runat="server" CommandName="RedirectDepartment" OnCommand="RedirectLink_Command" CommandArgument='<%# Eval("Data.Id") %>'>
                                                     <%# Eval("Data.Description") %>
                                                 </asp:LinkButton>
                                            </td>
                                            <td class="col-xs-5 col-sm-5 col-md-5">
                                                <table class="table" style="margin-bottom: 0">
                                                    <tbody>
                                                        <tr>
                                                            <th colspan="2">Rilevato kwh</th>
                                                            <td align="right" colspan="2"><%# Eval("PowerUsed", "{0:0.0}") %></td>
                                                            <%--<th>Percentuale su utilizzata sito</th>--%>
                                                            <%--<td align="right" colspan="2"><%# ((MainCloud.Dashboard.Modules.Totalizer.TotalizerItemDepartment)Container.DataItem)
                                                        .PowerUsedPerc((MainCloud.Dashboard.Modules.Totalizer.TotalizerItemPlant)((RepeaterItem)Container.Parent.Parent).DataItem) %>%</td>--%>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2">Totale asset rilevati kwh</th>
                                                            <td align="right"><%# Eval("DetectedValue", "{0:0.0}") %></td>
                                                            <%-- <td align="right" colspan="2"><%# ((MainCloud.Dashboard.Modules.Totalizer.TotalizerItemDepartment)Container.DataItem)
                                                        .DetectValuePerc((MainCloud.Dashboard.Modules.Totalizer.TotalizerItemPlant)((RepeaterItem)Container.Parent.Parent).DataItem) %>%</td>--%>
                                                            <%--<th colspan="3">Min kw</th>
                                                            <td><%# Eval("DetectedMin") %></td>
                                                            <th colspan="3">Max kw</th>
                                                            <td><%# Eval("DetectedMax") %></td> --%>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td class="col-xs-4 col-sm-4 col-md-4">
                                                <table class="table" style="margin-bottom: 0;">
                                                    <tbody>
                                                        <tr>
                                                            <td align="right" colspan="2"><%# ((MainCloud.Dashboard.Modules.Energy.Totalizer.TotalizerItemDepartment)Container.DataItem)
                                                        .PowerUsedPerc((MainCloud.Dashboard.Modules.Energy.Totalizer.TotalizerItemPlant)((RepeaterItem)Container.Parent.Parent).DataItem) %>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" colspan="2"><%# ((MainCloud.Dashboard.Modules.Energy.Totalizer.TotalizerItemDepartment)Container.DataItem)
                                                        .DetectValuePerc((MainCloud.Dashboard.Modules.Energy.Totalizer.TotalizerItemPlant)((RepeaterItem)Container.Parent.Parent).DataItem) %>%</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="padding-left: 5%">
                                                <table class="table" style="margin-bottom: 0">
                                                    <thead>
                                                        <tr class="active">
                                                            <th style="width:50%;">Asset</th>
                                                            <th style="width:25%;text-align:right;">kwh</th>
                                                            <th style="width:25%;text-align:right;">%</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:Repeater ID="rptAssets" DataSource='<%#Bind("Assets")%>' runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton runat="server" CommandName="RedirectAsset" OnCommand="RedirectLink_Command" CommandArgument='<%# Eval("Data.Id") %>'>
                                                                            <%# Eval("Data.Description") %>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                    <td align="right">
                                                                        <%# Eval("DetectedValue", "{0:0.0}") %>
                                                                    </td>
                                                                    <td align="right">
                                                                        <%# ((MainCloud.Dashboard.Modules.Energy.Totalizer.TotalizerItemAsset)Container.DataItem)
                                                                                    .DetectValuePerc((MainCloud.Dashboard.Modules.Energy.Totalizer.TotalizerItemPlant)((RepeaterItem)Container.Parent.Parent.Parent.Parent).DataItem) %>
                                                                    </td>
                                                                </tr>    
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </tbody>
        </table>
    </ItemTemplate>
</asp:Repeater>