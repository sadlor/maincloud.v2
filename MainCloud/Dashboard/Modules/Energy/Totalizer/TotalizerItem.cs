﻿using EnergyModule.Models;
using MainCloud.Dashboard.Modules.Energy.History;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Energy.Totalizer
{
    public abstract class TotalizerItem
    {
        public abstract decimal DetectedValue { get; }
        public abstract decimal DetectedMin { get; }
        public abstract decimal DetectedMax { get; }

        public abstract decimal InternalProduction { get; set; }
    }
}