﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Cosfi_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.Cosfi.Cosfi_View" %>

<telerik:RadRadialGauge runat="server" ID="MeterCosfi" Width="100%">
    <Pointer Value="0">
        <Cap Size="0.1" />
    </Pointer>
    <Scale Min="0" Max="1">
        <Labels Format="{0}" />
        <Ranges>
            <telerik:GaugeRange Color="#c20000" From="0" To="0.50" />
            <telerik:GaugeRange Color="#ff7a00" From="0.50" To="0.80" />
            <telerik:GaugeRange Color="#ffc700" From="0.80" To="0.95" />
            <telerik:GaugeRange Color="#8dcb2a" From="0.95" To="1" />
        </Ranges>
    </Scale>
</telerik:RadRadialGauge>

<script type="text/javascript">

    $(function () {
        UpdateCosfi();
        setInterval(function () {
            UpdateCosfi();
        }, 10000);
    });

    function UpdateCosfi() {
        var request = $.ajax({
            url: '<%= MainCloudFramework.Web.Helpers.MultiTenantsHelper.MountAndResolveMultiTenantUrl("/Dashboard/Modules/Energy/Cosfi/EnergyCosfi.svc/GetValue") %>',
            method: 'GET',
            //data: JSON.stringify({ filter: _filter }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        });

        request.done(function (response) {  //decimal
            if (response.d != -1) {
                $find("<%=MeterCosfi.ClientID %>").set_value(response.d); //da togliere ID fisso, passarlo per parametro
        }
        });

    request.fail(function (jqXHR, textStatus) {
        //alert("Request failed: " + textStatus);
    });
}
</script>