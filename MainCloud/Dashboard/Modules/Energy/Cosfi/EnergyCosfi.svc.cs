﻿using EnergyModule.Core;
using EnergyModule.Services;
using MainCloudFramework.Web.Multitenants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace MainCloud.Dashboard.Modules.Energy.Cosfi
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class EnergyCosfi
    {
        // Per utilizzare HTTP GET, aggiungere l'attributo [WebGet]. (ResponseFormat predefinito è WebMessageFormat.Json)
        // Per creare un'operazione che restituisca XML,
        //     aggiungere [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     e includere la riga seguente nel corpo dell'operazione:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        [WebGet]
        public decimal GetValue()
        {
            try
            {
                var tenantName = HttpContext.Current.Request.QueryString["MultiTenant"];
                HttpMultitenantsSessionState SessionMultitenant = HttpContext.Current.Session.SessionMultitenant(tenantName);

                if (SessionMultitenant.ContainsKey(EMConstants.SELECTED_ID_CONTEXT) && SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT] != null)
                {
                    SelectedContext selectedContext = JsonConvert.DeserializeObject<SelectedContext>((string)SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT]);

                    EnergyDataService energyDataService = new EnergyDataService();
                    string analyzerId = energyDataService.GetAnalyzerId(selectedContext);
                    if (!string.IsNullOrEmpty(analyzerId))
                    {
                        var energyList = energyDataService.GetAnalyzerEnergyDataList(x => x.AnalyzerId == analyzerId && x.TimeStamp >= DateTime.Today);
                        EnergyRecord last = JsonConvert.DeserializeObject<EnergyRecord>(energyList.LastOrDefault().Data);
                        return last.GetData<decimal>("TotalPowerFactor");
                    }
                    else
                    {
                        //analyzerId null
                        return -1;
                    }
                }
                else
                {
                    throw new Exception("Non è stato selezionato un contesto"); //visualizzare l'avviso
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        // Add more operations here and mark them with [OperationContract]
    }
}
