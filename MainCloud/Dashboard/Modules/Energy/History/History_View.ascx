﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="History_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.History.History_View" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<link href="/Dashboard/WidgetStyle.css" rel="stylesheet">

        <mcf:Alert runat="server" id="AlertMessage" />
        
        <telerik:RadDockZone ID="dockZoneSettings" runat="server" Orientation="Vertical" BorderStyle="None">
            <telerik:RadDock RenderMode="Lightweight" ID="dockSettings" runat="server" Title="Settings"
                EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false" Resizable="true" Width="100%">
                <ContentTemplate>
                    <div class="row container" style="width:100%;">
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Button runat="server" ID="btnSelect" Text="Select Assets" OnClick="btnSelect_Click" CssClass="btnConfig" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                            <asp:Label CssClass="btn btn-success" ID="lblSelectedContext" runat="server" />
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
                            <asp:DropDownList runat="server" ID="ddlParameter" AutoPostBack="false" CssClass="btn btn-default dropdown-toggle" Width="100%">
                                <asp:ListItem Text="Energia attiva" Value="TotalActiveEnergyConsumed" />
                                <asp:ListItem Text="Energia reattiva" Value="TotalReactiveEnergyConsumed" />
                                <asp:ListItem Text="Potenza" Value="TotalActivePower" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                            <asp:RadioButtonList runat="server" ID="rbtnBehaviour" AutoPostBack="false" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Table" Value="Table" />
                                <asp:ListItem Text="Chart" Value="Chart" />
                            </asp:RadioButtonList>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-2">
                            <asp:DropDownList runat="server" ID="ddlChartType" AutoPostBack="false" CssClass="btn btn-default dropdown-toggle">
                                <asp:ListItem Text="AreaChart" Value="AreaSeries" />
                                <asp:ListItem Text="BarChart" Value="BarSeries" />
                                <asp:ListItem Text="ColumnChart" Value="ColumnSeries" />
                                <asp:ListItem Text="LineChart" Value="LineSeries"/>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <br />

                    <div class="row" style="width:100%;">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <asp:RadioButtonList ID="rbtnListLastTime" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" style="margin-left:18px;">
                                <asp:ListItem Text="Last day" Value="LastDay" />
                                <asp:ListItem Text="Last month" Value="LastMonth" />
                                <asp:ListItem Text="Last year" Value="LastYear" />
                                <asp:ListItem Text="Current month" Value="CurrentMonth" />
                                <asp:ListItem Text="Current year" Value="CurrentYear" />
                            </asp:RadioButtonList>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                            <asp:Label Text="Data inizio:" runat="server" AssociatedControlID="dtPickStart" />
                            <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickStart" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                            <asp:Label Text="Data fine:" runat="server" AssociatedControlID="dtPickEnd" />
                            <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickEnd" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                            <br />
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:LinkButton ID="btnConfirm" Text="Confirm <i class='fa fa-check-circle'></i>" runat="server" OnClick="btnConfirm_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                </ContentTemplate>
            </telerik:RadDock>
        </telerik:RadDockZone>
        
<asp:UpdatePanel runat="server" ID="updatePnlData" UpdateMode="Conditional">
    <ContentTemplate>

        <telerik:RadHtmlChart ID="GenericChart" runat="server" />
        <telerik:RadGrid ID="GridAsset" runat="server">
            <MasterTableView FilterExpression="">
                <ColumnGroups>
                    <telerik:GridColumnGroup Name="kWh" HeaderText="kWh">
                        <HeaderStyle HorizontalAlign="Center" />
                    </telerik:GridColumnGroup>
                </ColumnGroups>
            </MasterTableView>
        
        </telerik:RadGrid>

        <asp:HiddenField ID="hiddenSelection" runat="server" />

        <asp:UpdateProgress runat="server">
            <ProgressTemplate>
                <div class="loading">Loading&#8230;</div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>

        <mcf:PopUpDialog ID="wndSelectAsset" runat="server" Title="Seleziona assets">
            <Body>
                <div class="row">
                    <div class="col-xs-6 col-md-6">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <telerik:RadTreeView runat="Server" ID="AssetTreeView" EnableViewState="false" EnableDragAndDrop="false" EnableDragAndDropBetweenNodes="false" 
                                    OnClientNodeClicked="function(sender, args){window['TreeNodeClick_' + sender._element.id](sender, args)}" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-xs-1 col-md-1"></div>
                    <div class="col-xs-5 col-md-5">
                        <br /><br /><br />
                        <div class="row">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:LinkButton ID="btnConfirmSelect" Text="Confirm <i class='fa fa-arrow-circle-right'></i>" runat="server"
                                        CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;margin-left:50%;"
                                        OnClick="btnConfirmSelect_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </Body>
        </mcf:PopUpDialog>

        <mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
            <Body>
                <asp:label id="messaggio" runat="server"/>
            </Body>
        </mcf:PopUpDialog>
    


<script src="<%=ResolveUrl("~/Dashboard/WidgetGenericScript/SettingsGraphScript.js")%>"></script>
<script type="text/javascript">
    $(function () {
        $(<%= rbtnListLastTime.ClientID %>).on('click', function () {
            SetDate("<%= dtPickStart.ClientID%>", "<%= dtPickEnd.ClientID%>", "<%= rbtnListLastTime.ClientID%>");
        });
        $(<%= rbtnBehaviour.ClientID %>).on('click', function () {
            var value = $('#<%= rbtnBehaviour.ClientID %> input:checked').val();
            if (value == "Chart") {
                $('#<%= ddlChartType.ClientID %>').show();
            }
            else {
                $('#<%= ddlChartType.ClientID %>').hide();
            }
        });
        $(<%= btnConfirmSelect.ClientID %>).on('click', function(){
            var attr = $(<%= btnConfirmSelect.ClientID %>).attr('disabled');
            if (typeof attr !== typeof undefined && attr !== false) {
                return false;
            }
        });
    });

    function TreeNodeClick_<%= AssetTreeView.ClientID %>(sender, args) {
        if (args.get_node().get_value() != null && args.get_node().get_value() != "") {
            $(<%= btnConfirmSelect.ClientID %>).removeAttr('disabled');
            SaveSelection("<%= hiddenSelection.ClientID %>", '<%= lblSelectedContext.ClientID %>', args.get_node().get_value(), args.get_node().get_text());
        }
        else
        {
            $(<%= btnConfirmSelect.ClientID %>).attr('disabled', 'disabled');
        }
    }

    <%--function SaveSelection(sender, args) {
        document.getElementById("<%= hiddenSelection.ClientID %>").value = args.get_node().get_value();
        $('#<%= lblSelectedContext.ClientID %>').text(args.get_node().get_text());
    }--%>

    <%--function ClosePopup() {
        $('#<%= wndSelectAsset.ClientID %>').modal('hide');
    }--%>
</script>