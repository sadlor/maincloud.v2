﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Energy.History
{
    public class HistoryRecord
    {
        public string Param;

        public decimal Min { get; set; }
        public decimal Max { get; set; }
        public decimal Value { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime TimeStamp { get; set; }

        public HistoryRecord(decimal value, decimal min, decimal max, DateTime time)
        {
            Min = min;
            Max = max;
            Value = value;
            TimeStamp = time;
            Param = "";
        }

        public HistoryRecord(decimal value, decimal min, decimal max, DateTime time, string paramName)
        {
            Min = min;
            Max = max;
            Value = value;
            TimeStamp = time;
            Param = paramName;
        }
    }
}