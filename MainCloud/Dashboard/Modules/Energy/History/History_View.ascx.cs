﻿using MainCloudFramework.UI.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MainCloudFramework.Web.Multitenants;
using EnergyModule.Models;
using Telerik.Web.UI;
using Newtonsoft.Json;
using System.Globalization;
using MainCloudFramework.UI.Containers;
using System.ComponentModel.DataAnnotations;
using EnergyModule.Services;
using EnergyModule.Core;
using System.Drawing;
using AssetManagement.Services;
using AssetManagement.Models;
using AssetManagement.Core;

namespace MainCloud.Dashboard.Modules.Energy.History
{
    public partial class History_View : WidgetControl<object>
    {
        public History_View() : base(typeof(History_View)) { }

        public override bool EnablePrint
        {
            get
            {
                return false;
            }
        }

        public override void OnPrint()
        {
            base.OnPrint();
            //string script = "function f(){$find(\"" + wndSelectAsset.ClientID + "\").show(); Sys.Application.remove_load(f);} Sys.Application.add_load(f);";
            //System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPopup", script, true);
        }

        public enum BehaviourKey
        {
            Table, //visualizza in forma tabellare
            Chart //visualizza in forma grafica 
        }

        string ChartType
        {
            get
            {
                return (string)this.CustomSettings.Find("ChartType") ?? "LineSeries";
            }
        }

        EnergyEnum.TimeScale TypeOfTime
        {
            get
            {
                return (EnergyEnum.TimeScale)Convert.ToInt16(this.CustomSettings.Find("TypeOfTime"));
            }
        }

        string ParameterCode
        {
            get
            {
                return (string)this.CustomSettings.Find("ParameterCode") ?? "TotalActivePower";
            }
        }

        string Behaviour
        {
            get
            {
                return (string)CustomSettings.Find("Behaviour") ?? "Chart";
            }
        }

        SelectedContext SelectedContextId
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<SelectedContext>((string)CustomSettings.Find("SelectedContextId"));
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        DateTime? DateStart
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateStart");
            }
        }

        DateTime? DateEnd
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateEnd");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GenericChart.PlotArea.XAxis.Items.Clear();
            GenericChart.PlotArea.Series.Clear();
            //LoadAssetTreeView();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (SelectedContextId != null)//(!string.IsNullOrEmpty((string)CustomSettings.Find("SelectedContextId")))
            {
                SelectedContextService service = new SelectedContextService();
                //hiddenSelection.Value = 
                lblSelectedContext.Text = service.GetSelectedDescription(SelectedContextId);
            }

            //setto le dropdown con il valore in memoria
            ddlChartType.Items.FindByValue(ChartType).Selected = true;
            ddlParameter.Items.FindByValue(ParameterCode).Selected = true;
            if (!string.IsNullOrEmpty(Behaviour))
            {
                rbtnBehaviour.Items.FindByValue(Behaviour).Selected = true;
            }

            //setto le date con il valore in memoria
            dtPickStart.SelectedDate = DateStart;
            dtPickEnd.SelectedDate = DateEnd;

            //if (Application.ApplicationMultitenant().ContainsKey(EMConstants.SELECTED_ID_CONTEXT) && Application.ApplicationMultitenant()[EMConstants.SELECTED_ID_CONTEXT] != null)
            if (SelectedContextId != null)
            {
                //EnergyDataService eS = new EnergyDataService();
                //segnalo il contesto già preselezionato
                //lblSelectedContext.Text = eS.GetSelectedDescription(JsonConvert.DeserializeObject<SelectedContext>((string)Application.ApplicationMultitenant()[EMConstants.SELECTED_ID_CONTEXT]));

                //switch (Behaviour)
                //{
                //    case "Table"://BehaviourKey.Table.ToString():
                //        GenericChart.Visible = false;
                //        GridAsset.Visible = true;
                //        //if ((TypeOfTime == "Day" || TypeOfTime == "Month" || TypeOfTime == "Year") && (dtPickStart.SelectedDate.Value == null || dtPickEnd.SelectedDate.Value == null))
                //        try
                //        {
                //            if (DateStart != null && DateEnd != null)
                //            {
                //                CreateTable(DateStart.Value, DateEnd.Value, ParameterCode, TypeOfTime);
                //            }
                //        }
                //        catch (Exception ex)
                //        {
                //            Console.Write(ex);
                //        }
                //        break;
                //    case "Chart":
                //        GenericChart.Visible = true;
                //        GridAsset.Visible = false;
                //        try
                //        {
                //            if (DateStart != null && DateEnd != null)
                //            {
                //                CreateChart(ChartType, DateStart.Value, DateEnd.Value, ParameterCode, TypeOfTime);
                //            }
                //        }
                //        catch (Exception ex)
                //        {
                //            Console.Write(ex);
                //        }
                //        break;
                //}
            }
            //else
            //{
            //    AlertMessage.ShowError("Non è stato selezionato nessun contesto, tornare nell'area asset per poter effettuare la selezione");
            //}
        }

        #region SelectAsset
        protected void btnSelect_Click(object sender, EventArgs e)
        {
            LoadAssetTreeView();
            DataBind();
            wndSelectAsset.OpenDialog();
        }

        protected void LoadAssetTreeView()
        {
            AssetTreeView.Nodes.Clear();

            PlantService service = new PlantService();
            var plants = service.PlantList();
            foreach (Plant p in plants.OrderBy(x => x.Description))
            {
                //Node Plant
                RadTreeNode nodeP = new RadTreeNode(p.Description);
                if (string.IsNullOrEmpty(p.AnalyzerId))
                {
                    nodeP.ForeColor = Color.Red;
                    nodeP.Value = "";
                }
                else
                {
                    nodeP.Value = JsonConvert.SerializeObject(new SelectedContext(p.Id, AssetManagementEnum.AnalyzerContext.Plant));
                }
                nodeP.Expanded = true;
                foreach (Department d in p.Department.OrderBy(x => x.Description))
                {
                    //Node Department
                    RadTreeNode nodeD = new RadTreeNode(d.Description);
                    if (string.IsNullOrEmpty(d.AnalyzerId))
                    {
                        nodeD.ForeColor = Color.Red;
                        nodeD.Value = "";
                    }
                    else
                    {
                        nodeD.Value = JsonConvert.SerializeObject(new SelectedContext(d.Id, AssetManagementEnum.AnalyzerContext.Department));
                    }
                    nodeD.Expanded = true;
                    foreach (Asset a in d.Asset.OrderBy(x => x.Description))
                    {
                        //Node Asset
                        RadTreeNode nodeA = new RadTreeNode(a.Description);
                        if (string.IsNullOrEmpty(a.AnalyzerId))
                        {
                            nodeA.ForeColor = Color.Red;
                            nodeA.Value = "";
                        }
                        else
                        {
                            nodeA.Value = JsonConvert.SerializeObject(new SelectedContext(a.Id, AssetManagementEnum.AnalyzerContext.Asset));
                        }
                        nodeA.Expanded = true;

                        nodeD.Nodes.Add(nodeA);
                    }
                    nodeP.Nodes.Add(nodeD);
                }
                AssetTreeView.Nodes.Add(nodeP);
            }
        }

        protected void btnConfirmSelect_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate("SelectedContextId", hiddenSelection.Value);
            wndSelectAsset.CloseDialog();
        }
        #endregion

        private class TableDataSource
        {
            public object Min { get; set; }
            public object Max { get; set; }
            public object Value { get; set; }
            public DateTime TimeStamp { get; set; }

            public TableDataSource(object value, object min, object max, DateTime time)
            {
                Min = min;
                Max = max;
                Value = value;
                TimeStamp = time;
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate("Behaviour", rbtnBehaviour.SelectedValue);
            CustomSettings.AddOrUpdate("ChartType", ddlChartType.SelectedValue);
            CustomSettings.AddOrUpdate("ParameterCode", ddlParameter.SelectedValue);
            if (dtPickStart.SelectedDate == null || dtPickEnd.SelectedDate == null)
            {
                messaggio.Text = "Inserire prima data inizio e fine";
                dlgWidgetConfig.OpenDialog();
            }
            else
            {
                CustomSettings.AddOrUpdate("DateStart", dtPickStart.SelectedDate.Value);
                CustomSettings.AddOrUpdate("DateEnd", dtPickEnd.SelectedDate.Value);
                if ((dtPickEnd.SelectedDate.Value - dtPickStart.SelectedDate.Value).Days > 62)
                {
                    if ((dtPickEnd.SelectedDate.Value - dtPickStart.SelectedDate.Value).Days > 730)
                    {
                        CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Year);
                    }
                    else
                    {
                        CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Month);
                    }
                }
                else
                {
                    if (dtPickEnd.SelectedDate.Value == dtPickStart.SelectedDate.Value)
                    {
                        CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Hour);
                    }
                    else
                    {
                        CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Day);
                    }
                }
                if (SelectedContextId != null)
                {
                    switch (Behaviour)
                    {
                        case "Table"://BehaviourKey.Table.ToString():
                            GenericChart.Visible = false;
                            GridAsset.Visible = true;
                            //if ((TypeOfTime == "Day" || TypeOfTime == "Month" || TypeOfTime == "Year") && (dtPickStart.SelectedDate.Value == null || dtPickEnd.SelectedDate.Value == null))
                            try
                            {
                                if (DateStart != null && DateEnd != null)
                                {
                                    CreateTable(DateStart.Value, DateEnd.Value, ParameterCode, TypeOfTime);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.Write(ex);
                            }
                            break;
                        case "Chart":
                            GenericChart.Visible = true;
                            GridAsset.Visible = false;
                            try
                            {
                                if (DateStart != null && DateEnd != null)
                                {
                                    CreateChart(ChartType, DateStart.Value, DateEnd.Value, ParameterCode, TypeOfTime);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.Write(ex);
                            }
                            break;
                    }
                    updatePnlData.Update();
                }
                else
                {
                    messaggio.Text = "Inserire prima un asset";
                    dlgWidgetConfig.OpenDialog();
                }
            }

            //dockSettings.Collapsed = true;
        }

        public void CreateTable(DateTime start, DateTime end, string paramName, EnergyEnum.TimeScale typeOfTime)
        {
            EnergyDataService energyDataService = new EnergyDataService();

            //Update new version
            if (paramName == EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString())
            {
                EnergyConsumptionService ECS = new EnergyConsumptionService();
                List<EnergyConsumption> dataSource = ECS.GetEnergyDataList(energyDataService.GetAnalyzerId(SelectedContextId), start, end, typeOfTime);

                GridAsset.RenderMode = RenderMode.Lightweight;
                GridAsset.MasterTableView.Caption = "History - " + paramName + " - " + typeOfTime;
                GridAsset.DataSource = dataSource;
                GridAsset.AutoGenerateColumns = false;
                GridAsset.MasterTableView.Columns.Clear();

                GridDateTimeColumn dateCol = new GridDateTimeColumn();
                switch (typeOfTime)
                {
                    case EnergyEnum.TimeScale.Hour:
                        dateCol.DataFormatString = "{0:dd/MM/yyyy HH:mm}";
                        break;
                    case EnergyEnum.TimeScale.Day:
                        dateCol.DataFormatString = "{0:dd/MM/yyyy}";
                        break;
                    case EnergyEnum.TimeScale.Month:
                        dateCol.DataFormatString = "{0:MM/yyyy}";
                        break;
                    case EnergyEnum.TimeScale.Year:
                        dateCol.DataFormatString = "{0:yyyy}";
                        break;
                }
                dateCol.HeaderText = "Date";
                dateCol.DataField = "Date";
                dateCol.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                GridAsset.MasterTableView.Columns.Add(dateCol);

                GridBoundColumn col = new GridBoundColumn();
                col.DataField = "kWhF1";
                col.HeaderText = "F1";
                col.ColumnGroupName = "kWh";
                col.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffc700");
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                GridAsset.MasterTableView.Columns.Add(col);

                col = new GridBoundColumn();
                col.DataField = "kWhF2";
                col.HeaderText = "F2";
                col.ColumnGroupName = "kWh";
                col.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#2a94cb");
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                GridAsset.MasterTableView.Columns.Add(col);

                col = new GridBoundColumn();
                col.DataField = "kWhF3";
                col.HeaderText = "F3";
                col.ColumnGroupName = "kWh";
                col.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#8dcb2a");
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                GridAsset.MasterTableView.Columns.Add(col);

                col = new GridBoundColumn();
                col.DataField = "kWh";
                col.HeaderText = "Tot";
                col.ColumnGroupName = "kWh";
                col.HeaderStyle.BackColor = Color.Yellow;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                GridAsset.MasterTableView.Columns.Add(col);
            }
            ////////////////////
            else
            {
                var list = energyDataService.CreateHistorySerie(SelectedContextId, start, end, paramName, typeOfTime);
                if (list.Count > 0)
                {
                    List<TableDataSource> tableDataSource = new List<TableDataSource>();
                    foreach (var item in list)
                    {
                        if (typeOfTime == EnergyEnum.TimeScale.Hour)
                        {
                            tableDataSource.Add(new TableDataSource(item.Value.Value, item.Value.Min, item.Value.Max, item.Key.AddHours(1)));
                        }
                        else
                        {
                            tableDataSource.Add(new TableDataSource(item.Value.Value, item.Value.Min, item.Value.Max, item.Key));
                        }
                    }

                    GridDateTimeColumn dateCol = new GridDateTimeColumn();
                    switch (typeOfTime)
                    {
                        case EnergyEnum.TimeScale.Hour:
                            dateCol.DataFormatString = "{0:dd/MM/yyyy HH:mm}";
                            break;
                        case EnergyEnum.TimeScale.Day:
                            dateCol.DataFormatString = "{0:dd/MM/yyyy}";
                            break;
                        case EnergyEnum.TimeScale.Month:
                            dateCol.DataFormatString = "{0:MM/yyyy}";
                            break;
                        case EnergyEnum.TimeScale.Year:
                            dateCol.DataFormatString = "{0:yyyy}";
                            break;
                    }
                    dateCol.HeaderText = "Data";
                    dateCol.DataField = "TimeStamp";
                    dateCol.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                    GridAsset.RenderMode = RenderMode.Lightweight;
                    GridAsset.MasterTableView.Caption = "History - " + paramName + " - " + typeOfTime;
                    GridAsset.DataSource = tableDataSource;
                    GridAsset.AutoGenerateColumns = false;
                    GridAsset.MasterTableView.Columns.Clear();

                    GridAsset.MasterTableView.Columns.Add(dateCol);

                    GridBoundColumn col = new GridBoundColumn();
                    col.DataField = "Value";
                    col.HeaderText = "Value";
                    col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    col.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    GridAsset.MasterTableView.Columns.Add(col);

                    col = new GridBoundColumn();
                    col.DataField = "Min";
                    col.HeaderText = "Min";
                    col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    col.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    GridAsset.MasterTableView.Columns.Add(col);

                    col = new GridBoundColumn();
                    col.DataField = "Max";
                    col.HeaderText = "Max";
                    col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    col.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    GridAsset.MasterTableView.Columns.Add(col);
                }
            }

            GridAsset.DataBind();
        }

        public void CreateChart(string chartType, DateTime start, DateTime end, string paramName, EnergyEnum.TimeScale typeOfTime)
        {
            //update new version
            if (paramName == EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString())
            {
                EnergyDataService energyDataService = new EnergyDataService();
                EnergyConsumptionService ECS = new EnergyConsumptionService();
                List<EnergyConsumption> dataSource = ECS.GetEnergyDataList(energyDataService.GetAnalyzerId(SelectedContextId), start, end, typeOfTime);

                //asse x
                foreach (var item in dataSource)
                {
                    switch (typeOfTime)
                    {
                        case EnergyEnum.TimeScale.Hour:
                            //GenericChart.PlotArea.XAxis.Items.Add((item.Date.Hour + 1).ToString());
                            //break;
                        case EnergyEnum.TimeScale.Day:
                            //GenericChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 35;
                            GenericChart.PlotArea.XAxis.Items.Add(item.Date.ToString("dd"));
                            break;
                        case EnergyEnum.TimeScale.Month:
                            GenericChart.PlotArea.XAxis.Items.Add(item.Date.ToString("MM/yyyy"));
                            break;
                        case EnergyEnum.TimeScale.Year:
                            break;
                    }
                }

                //asse y
                ColumnSeries F1Serie = new ColumnSeries();
                F1Serie.Name = "F1";
                F1Serie.GroupName = "Fasce";
                F1Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#ffc700");
                F1Serie.LabelsAppearance.Visible = false;
                IEnumerable<decimal> F1Items = dataSource.Select(x => x.kWhF1);
                foreach (var item in F1Items)
                {
                    F1Serie.SeriesItems.Add(new CategorySeriesItem(item));
                }

                ColumnSeries F2Serie = new ColumnSeries();
                F2Serie.Name = "F2";
                F2Serie.GroupName = "Fasce";
                F2Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#2a94cb");
                F2Serie.LabelsAppearance.Visible = false;
                IEnumerable<decimal> F2Items = dataSource.Select(x => x.kWhF2);
                foreach (var item in F2Items)
                {
                    F2Serie.SeriesItems.Add(new CategorySeriesItem(item));
                }

                ColumnSeries F3Serie = new ColumnSeries();
                F3Serie.Name = "F3";
                F3Serie.GroupName = "Fasce";
                F3Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#8dcb2a");
                F3Serie.LabelsAppearance.Visible = false;
                IEnumerable<decimal> F3Items = dataSource.Select(x => x.kWhF3);
                foreach (var item in F3Items)
                {
                    F3Serie.SeriesItems.Add(new CategorySeriesItem(item));
                }
                GenericChart.PlotArea.Series.Add(F1Serie);
                GenericChart.PlotArea.Series.Add(F2Serie);
                GenericChart.PlotArea.Series.Add(F3Serie);
                GenericChart.PlotArea.YAxis.TitleAppearance.Text += "KWh";
            }
            else
            {

                GenericChart.ChartTitle.Text = "History - " + paramName;

                string nameSerie = "AverageValue";

                List<EnergyData> seriesItems = CreateSeriesItems(start, end, paramName, typeOfTime);

                List<CategorySeriesItem> valueSerie = new List<CategorySeriesItem>();
                List<CategorySeriesItem> minSerie = new List<CategorySeriesItem>();
                List<CategorySeriesItem> maxSerie = new List<CategorySeriesItem>();

                foreach (var item in seriesItems)
                {
                    valueSerie.Add(new CategorySeriesItem((decimal)item.Value));
                    minSerie.Add(new CategorySeriesItem((decimal)item.Min));
                    maxSerie.Add(new CategorySeriesItem((decimal)item.Max));
                }

                switch (chartType)
                {
                    case "AreaSeries":
                        AreaSeries areaSerie = new AreaSeries();
                        areaSerie.Name = nameSerie;
                        areaSerie.LabelsAppearance.Visible = false;
                        areaSerie.SeriesItems.AddRange(valueSerie);
                        GenericChart.PlotArea.Series.Add(areaSerie);
                        break;
                    case "BarSeries":
                        BarSeries barSerie = new BarSeries();
                        barSerie.Name = nameSerie;
                        barSerie.LabelsAppearance.Visible = false;
                        barSerie.SeriesItems.AddRange(valueSerie);
                        GenericChart.PlotArea.Series.Add(barSerie);

                        if (paramName == "TotalActivePower")
                        {
                            BarSeries barSerieMin = new BarSeries();
                            barSerieMin.Name = "Minimi";
                            barSerieMin.LabelsAppearance.Visible = false;
                            barSerieMin.SeriesItems.AddRange(minSerie);
                            GenericChart.PlotArea.Series.Add(barSerieMin);

                            BarSeries barSerieMax = new BarSeries();
                            barSerieMax.Name = "Picchi massimi";
                            barSerieMax.LabelsAppearance.Visible = false;
                            barSerieMax.SeriesItems.AddRange(maxSerie);
                            GenericChart.PlotArea.Series.Add(barSerieMax);
                        }
                        break;
                    case "ColumnSeries":
                        ColumnSeries columnSerie = new ColumnSeries();
                        columnSerie.Name = nameSerie;
                        columnSerie.LabelsAppearance.Visible = false;
                        columnSerie.SeriesItems.AddRange(valueSerie);
                        GenericChart.PlotArea.Series.Add(columnSerie);

                        if (paramName == "TotalActivePower")
                        {
                            ColumnSeries columnSerieMin = new ColumnSeries();
                            columnSerieMin.Name = "Minimi";
                            columnSerieMin.LabelsAppearance.Visible = false;
                            columnSerieMin.SeriesItems.AddRange(minSerie);
                            GenericChart.PlotArea.Series.Add(columnSerieMin);

                            ColumnSeries columnSerieMax = new ColumnSeries();
                            columnSerieMax.Name = "Picchi massimi";
                            columnSerieMax.LabelsAppearance.Visible = false;
                            columnSerieMax.SeriesItems.AddRange(maxSerie);
                            GenericChart.PlotArea.Series.Add(columnSerieMax);
                        }
                        break;
                    case "LineSeries":
                        LineSeries lineSerie = new LineSeries();
                        lineSerie.Name = nameSerie;
                        lineSerie.LabelsAppearance.Visible = false;
                        lineSerie.SeriesItems.AddRange(valueSerie);
                        GenericChart.PlotArea.Series.Add(lineSerie);

                        if (paramName == "TotalActivePower")
                        {
                            LineSeries lineSerieMin = new LineSeries();
                            lineSerieMin.Name = "Minimi";
                            lineSerieMin.LabelsAppearance.Visible = false;
                            lineSerieMin.SeriesItems.AddRange(minSerie);
                            GenericChart.PlotArea.Series.Add(lineSerieMin);

                            LineSeries lineSerieMax = new LineSeries();
                            lineSerieMax.Name = "Picchi massimi";
                            lineSerieMax.LabelsAppearance.Visible = false;
                            lineSerieMax.SeriesItems.AddRange(maxSerie);
                            GenericChart.PlotArea.Series.Add(lineSerieMax);
                        }
                        break;
                }
                switch (paramName)
                {
                    case "TotalActivePower":
                        GenericChart.PlotArea.YAxis.TitleAppearance.Text += "KW";
                        break;
                    case "TotalActiveEnergyConsumed":
                        GenericChart.PlotArea.YAxis.TitleAppearance.Text += "KWh";
                        break;
                    case "TotalReactiveEnergyConsumed":
                        GenericChart.PlotArea.YAxis.TitleAppearance.Text += "kvarh";
                        break;
                }
            }
        }

        private List<EnergyData> CreateSeriesItems(DateTime start, DateTime end, string paramName, EnergyEnum.TimeScale typeOfTime, bool createX = true)
        {
            EnergyDataService energyDataService = new EnergyDataService();

            //var CauseList = energyDataService.CreateHistorySerie(JsonConvert.DeserializeObject<SelectedContext>((string)Application.ApplicationMultitenant()[EMConstants.SELECTED_ID_CONTEXT]), start, end, paramName, typeOfTime);
            var list = energyDataService.CreateHistorySerie(SelectedContextId, start, end, paramName, typeOfTime);

            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    switch (typeOfTime)
                    {
                        case EnergyEnum.TimeScale.Hour:
                            GenericChart.PlotArea.XAxis.Items.Add((item.Key.Hour + 1).ToString());
                            break;
                        case EnergyEnum.TimeScale.Day:
                            //GenericChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 35;
                            GenericChart.PlotArea.XAxis.Items.Add(item.Key.Date.ToString("dd"));
                            break;
                        case EnergyEnum.TimeScale.Month:
                            GenericChart.PlotArea.XAxis.Items.Add(item.Key.Date.ToString("yyyy/MM"));
                            break;
                        case EnergyEnum.TimeScale.Year:
                            break;
                    }
                }
                return list.Values.ToList();
            }
            return null;
        }
    }
}