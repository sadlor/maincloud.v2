﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HistoryComparison_View.ascx.cs" Inherits="MainEnergy.Dashboard.Modules.Energy.HistoryComparison.HistoryComparison_View" %>

<link href="/Dashboard/WidgetStyle.css" rel="stylesheet">

        <telerik:RadDockZone ID="dockZoneSettings" runat="server" Orientation="Vertical" BorderStyle="None">
            <telerik:RadDock RenderMode="Lightweight" ID="dockSettings" runat="server" Title="Settings"
                EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false" Resizable="true" Width="100%">
                <ContentTemplate>

                    <div class="row" style="width:100%;">
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                            <asp:Button runat="server" ID="btnSelect" Text="Select Assets" OnClick="btnSelect_Click" CssClass="btnConfig" />
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                            <asp:Label CssClass="btn btn-success" ID="lblSelectedContext" runat="server" />
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
                            <asp:DropDownList runat="server" ID="ddlParameter" AutoPostBack="false" CssClass="btn btn-default dropdown-toggle">
                                <asp:ListItem Text="Energia attiva" Value="TotalActiveEnergyConsumed" />
                                <asp:ListItem Text="Energia reattiva" Value="TotalReactiveEnergyConsumed" />
                                <asp:ListItem Text="Potenza" Value="TotalActivePower" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-sm-1 col-md-1 col-lg-5"></div>
                    </div>

                    <br />

                    <asp:UpdatePanel runat="server" ID="updatePnlDate" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row" style="width:100%">
                                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                    <asp:RadioButtonList ID="rbtnListTimeScale" runat="server" EnableViewState="true" AutoPostBack="true" OnSelectedIndexChanged="rbtnListTimeScale_SelectedIndexChanged" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Day" Value="Day" />
                                        <asp:ListItem Text="Month" Value="Month" />
                                        <asp:ListItem Text="Year" Value="Year" />
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="row" style="width:100%;">
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                    <asp:Label Text="Periodo 1" runat="server" />
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                                    <asp:Label Text="Data inizio:" runat="server" />
                                    <telerik:RadMonthYearPicker RenderMode="Lightweight" ID="monthPickStart1" runat="server" ShowPopupOnFocus="true">
                                        <DateInput runat="server" DateFormat="MM/yyyy"></DateInput>
                                    </telerik:RadMonthYearPicker>
                                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dayPickStart1" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                                    <asp:Label Text="Data fine:" runat="server" />
                                    <telerik:RadMonthYearPicker RenderMode="Lightweight" ID="monthPickEnd1" runat="server" ShowPopupOnFocus="true">
                                        <DateInput runat="server" DateFormat="MM/yyyy"></DateInput>
                                    </telerik:RadMonthYearPicker>
                                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dayPickEnd1" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                                </div>
                            </div>

                            <br />

                            <div class="row" style="width:100%;">
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                    <asp:Label Text="Periodo 2" runat="server" />
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                                    <asp:Label Text="Data inizio:" runat="server" />
                                    <telerik:RadMonthYearPicker RenderMode="Lightweight" ID="monthPickStart2" runat="server" ShowPopupOnFocus="true">
                                        <DateInput runat="server" DateFormat="MM/yyyy"></DateInput>
                                    </telerik:RadMonthYearPicker>
                                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dayPickStart2" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                                    <asp:Label Text="Data fine:" runat="server" />
                                    <telerik:RadMonthYearPicker RenderMode="Lightweight" ID="monthPickEnd2" runat="server" ShowPopupOnFocus="true">
                                        <DateInput runat="server" DateFormat="MM/yyyy"></DateInput>
                                    </telerik:RadMonthYearPicker>
                                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dayPickEnd2" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                                </div>
                            </div>
                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="pull-right">
                        <br />
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="btnConfirm" Text="Confirm <i class='fa fa-check-circle'></i>" runat="server" OnClick="btnConfirm_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </telerik:RadDock>
        </telerik:RadDockZone>

<asp:UpdatePanel runat="server" ID="updatePnlChart" UpdateMode="Conditional">
    <ContentTemplate>

        <asp:HiddenField ID="hiddenSelection" runat="server" />

        <telerik:RadHtmlChart runat="server" ID="GenericChart" Width="100%" Transitions="true"> <%--Skin="Silk"--%>
        </telerik:RadHtmlChart>

        <asp:UpdateProgress runat="server">
            <ProgressTemplate>
                <div class="loading">Loading&#8230;</div>
            </ProgressTemplate>
        </asp:UpdateProgress>

    </ContentTemplate>
</asp:UpdatePanel>

        <mcf:PopUpDialog ID="wndSelectAsset" runat="server" Title="Seleziona contesto">
            <Body>
                <div class="row">
                    <div class="col-xs-6 col-md-6">
                        <telerik:RadTreeView runat="Server" ID="AssetTreeView" EnableViewState="false" EnableDragAndDrop="false" EnableDragAndDropBetweenNodes="false" OnClientNodeClicked="TreeNodeClick" />
                    </div>
                    <div class="col-xs-1 col-md-1"></div>
                    <div class="col-xs-5 col-md-5">
                        <br /><br /><br />
                        <div class="row">
                            <asp:LinkButton ID="btnConfirmSelect" Text="Confirm <i class='fa fa-arrow-circle-right'></i>" runat="server" OnClick="btnConfirmSelect_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;margin-left:50%;" />
                        </div>
                    </div>
                </div> 
            </Body>
        </mcf:PopUpDialog>
    

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
    <Body>
        <asp:label id="messaggio" runat="server"/>
    </Body>
</mcf:PopUpDialog>

<script src="<%=ResolveUrl("~/Dashboard/WidgetGenericScript/SettingsGraphScript.js")%>"></script>
<script type="text/javascript">

    $(function () {
        <%--$(<%= rbtnListLastTime1.ClientID %>).on('click', function () {
            SetDate("<%= dtPickStart1.ClientID%>", "<%= dtPickEnd1.ClientID%>", "<%= rbtnListLastTime1.ClientID%>");
        });
        $(<%= rbtnListLastTime2.ClientID %>).on('click', function () {
            SetDate("<%= dtPickStart2.ClientID%>", "<%= dtPickEnd2.ClientID%>", "<%= rbtnListLastTime2.ClientID%>");
        });--%>
        $(<%= btnConfirmSelect.ClientID %>).on('click', function () {
            var attr = $(<%= btnConfirmSelect.ClientID %>).attr('disabled');
            if (typeof attr !== typeof undefined && attr !== false) {
                return false;
            }
        });
        <%--$(<%= rbtnListTimeScale.ClientID %>).on('click', function () {
            DateVisibility();
        });--%>
    });

    function TreeNodeClick(sender, args) {
        if (args.get_node().get_value() != null && args.get_node().get_value() != "") {
            $(<%= btnConfirmSelect.ClientID %>).removeAttr('disabled');
            SaveSelection('<%= hiddenSelection.ClientID %>', '<%= lblSelectedContext.ClientID %>', args.get_node().get_value(), args.get_node().get_text());
        }
        else
        {
            $(<%= btnConfirmSelect.ClientID %>).attr('disabled', 'disabled');
        }
    }

    <%--function DateSelected(sender, args) {
        var length = $find("<%= dtPickEnd1.ClientID%>").get_selectedDate() - $find("<%= dtPickStart1.ClientID%>").get_selectedDate();
        var length2 = $find("<%= dtPickEnd2.ClientID%>").get_selectedDate() - $find("<%= dtPickStart2.ClientID%>").get_selectedDate();
        //var length2 = args.get_newDate() - $find("<%= dtPickStart2.ClientID%>").get_selectedDate();
        if (length != length2) {
            //sender.set_selectedDate(args.get_oldDate());
            sender.clear();
            alert("I due periodi devono avere la stessa lunghezza");
        }
    }--%>

    <%--function ClosePopup()
    {
        $('#<%= wndSelectAsset.ClientID %>').modal('hide');
    }--%>
</script>