﻿using MainCloudFramework.UI.Modules;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MainCloudFramework.Web.Multitenants;
using Telerik.Web.UI.Skins;
using EnergyModule.Models;
using Telerik.Web.UI;
using EnergyModule;
using Newtonsoft.Json;
using MainCloud.Dashboard.Modules.Energy.History;
using EnergyModule.Services;
using EnergyModule.Repositories;
using MainCloudFramework.Web.Helpers;
using EnergyModule.Core;
using System.Drawing;
using AssetManagement.Services;
using AssetManagement.Models;
using AssetManagement.Core;

namespace MainEnergy.Dashboard.Modules.Energy.HistoryComparison
{
    public partial class HistoryComparison_View : WidgetControl<object>
    {
        public HistoryComparison_View() : base(typeof(HistoryComparison_View)) { }

        string ParameterCode
        {
            get
            {
                return (string)this.CustomSettings.Find("ParameterCode") ?? "TotalActiveEnergyConsumed";
            }
        }

        SelectedContext SelectedContextId
        {
            get 
            {
                try
                {
                    return JsonConvert.DeserializeObject<SelectedContext>((string)CustomSettings.Find("SelectedContextId"));
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        EnergyEnum.TimeScale TimeScale
        {
            get
            {
                return (EnergyEnum.TimeScale)Convert.ToInt16(CustomSettings.Find("TimeScale"));
            }
        }

        EnergyEnum.TimeScale TypeOfTime1
        {
            get
            {
                return (EnergyEnum.TimeScale)Convert.ToInt16(CustomSettings.Find("TypeOfTime1"));
            }
        }

        EnergyEnum.TimeScale TypeOfTime2
        {
            get
            {
                return (EnergyEnum.TimeScale)Convert.ToInt16(CustomSettings.Find("TypeOfTime2"));
            }
        }

        DateTime? DateStart1
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateStart1");
            }
        }

        DateTime? DateEnd1
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateEnd1");
            }
        }

        DateTime? DateStart2
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateStart2");
            }
        }

        DateTime? DateEnd2
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateEnd2");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GenericChart.PlotArea.XAxis.Items.Clear();
            GenericChart.PlotArea.Series.Clear();
            if (!IsPostBack)
            {
                rbtnListTimeScale.SelectedIndex = 0;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (SelectedContextId != null)//(!string.IsNullOrEmpty((string)CustomSettings.Find("SelectedContextId")))
            {
                SelectedContextService service = new SelectedContextService();
                //hiddenSelection.Value = 
                lblSelectedContext.Text = service.GetSelectedDescription(SelectedContextId);
                hiddenSelection.Value = JsonConvert.SerializeObject(SelectedContextId);
            }

            ddlParameter.Items.FindByValue(ParameterCode).Selected = true;

            #region visibilità date
            if (!IsPostBack)
            {
                monthPickStart1.Visible = false;
                monthPickEnd1.Visible = false;
                monthPickStart2.Visible = false;
                monthPickEnd2.Visible = false;
                //switch (TimeScale)
                //{
                //        case EnergyEnum.TimeScale.Hour:
                //        case EnergyEnum.TimeScale.Day:
                //            rbtnListTimeScale.Items.FindByValue("Day").Selected = true;
                //            //dayPickStart1.Visible = true;
                //            //dayPickEnd1.Visible = true;
                //            //dayPickStart2.Visible = true;
                //            //dayPickEnd2.Visible = true;
                //            break;
                //        case EnergyEnum.TimeScale.Month:
                //            rbtnListTimeScale.Items.FindByValue("Month").Selected = true;
                //            //monthPickStart1.Visible = true;
                //            //monthPickEnd1.Visible = true;
                //            //monthPickStart2.Visible = true;
                //            //monthPickEnd2.Visible = true;
                //            break;
                //        case EnergyEnum.TimeScale.Year:
                //            rbtnListTimeScale.Items.FindByValue("Year").Selected = true;
                //            //monthPickStart1.Visible = true;
                //            //monthPickEnd1.Visible = true;
                //            //monthPickStart2.Visible = true;
                //            //monthPickEnd2.Visible = true;
                //            break;
                //}

                //setto le date con il valore in memoria
                dayPickStart1.SelectedDate = DateStart1;
                dayPickEnd1.SelectedDate = DateEnd1;
                dayPickStart2.SelectedDate = DateStart2;
                dayPickEnd2.SelectedDate = DateEnd2;
            }
            #endregion

            //if (DateStart1 != null && DateEnd1 != null && DateStart2 != null && DateEnd2 != null && SelectedContextId != null)
            //{
            //    //Period1
            //    if ((DateEnd1.Value - DateStart1.Value).Days > 62)
            //    {
            //        if ((DateEnd1.Value - DateStart1.Value).Days > 730)
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime1", EnergyEnum.TimeScale.Year);
            //        }
            //        else
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime1", EnergyEnum.TimeScale.Month);
            //        }
            //    }
            //    else
            //    {
            //        if (DateEnd1.Value == DateStart1.Value)
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime1", EnergyEnum.TimeScale.Hour);
            //        }
            //        else
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime1", EnergyEnum.TimeScale.Day);
            //        }
            //    }
            //    //Period2
            //    if ((DateEnd2.Value - DateStart2.Value).Days > 62)
            //    {
            //        if ((DateEnd2.Value - DateStart2.Value).Days > 730)
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime2", EnergyEnum.TimeScale.Year);
            //        }
            //        else
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime2", EnergyEnum.TimeScale.Month);
            //        }
            //    }
            //    else
            //    {
            //        if (DateStart2.Value == DateEnd2.Value)
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime2", EnergyEnum.TimeScale.Hour);
            //        }
            //        else
            //        {
            //            CustomSettings.AddOrUpdate("TypeOfTime2", EnergyEnum.TimeScale.Day);
            //        }
            //    }
                //if ((DateEnd1.Value - DateStart1.Value).Days > (DateEnd2.Value - DateStart2.Value).Days)
                //{
                //    CreateChart(DateStart1.Value, DateEnd1.Value, ParameterCode, TypeOfTime1, "Periodo1");
                //    CreateChart(DateStart2.Value, DateEnd2.Value, ParameterCode, TypeOfTime2, "Periodo2", false);
                //}
                //else
                //{
                //    CreateChart(DateStart2.Value, DateEnd2.Value, ParameterCode, TypeOfTime2, "Periodo2");
                //    CreateChart(DateStart1.Value, DateEnd1.Value, ParameterCode, TypeOfTime1, "Periodo1", false);
                //}
            //}
            //switch (ParameterCode)
            //{
            //    case "TotalActivePower":
            //        GenericChart.PlotArea.YAxis.TitleAppearance.Text += "KW";
            //        break;
            //    case "TotalActiveEnergyConsumed":
            //        GenericChart.PlotArea.YAxis.TitleAppearance.Text += "KWh";
            //        break;
            //    case "TotalReactiveEnergyConsumed":
            //        GenericChart.PlotArea.YAxis.TitleAppearance.Text += "kvarh";
            //        break;
            //}
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            LoadAssetTreeView();
            //OpenPopup();
            wndSelectAsset.OpenDialog();
        }

        protected void LoadAssetTreeView()
        {
            AssetTreeView.Nodes.Clear();

            PlantService service = new PlantService();
            var plants = service.PlantList();
            foreach (Plant p in plants.OrderBy(x => x.Description))
            {
                //Node Plant
                RadTreeNode nodeP = new RadTreeNode(p.Description);
                if (string.IsNullOrEmpty(p.AnalyzerId))
                {
                    nodeP.ForeColor = Color.Red;
                    nodeP.Value = "";
                }
                else
                {
                    nodeP.Value = JsonConvert.SerializeObject(new SelectedContext(p.Id, AssetManagementEnum.AnalyzerContext.Plant));
                }
                nodeP.Expanded = true;
                foreach (Department d in p.Department.OrderBy(x => x.Description))
                {
                    //Node Department
                    RadTreeNode nodeD = new RadTreeNode(d.Description);
                    if (string.IsNullOrEmpty(d.AnalyzerId))
                    {
                        nodeD.ForeColor = Color.Red;
                        nodeD.Value = "";
                    }
                    else
                    {
                        nodeD.Value = JsonConvert.SerializeObject(new SelectedContext(d.Id, AssetManagementEnum.AnalyzerContext.Department));
                    }
                    nodeD.Expanded = true;
                    foreach (Asset a in d.Asset.OrderBy(x => x.Description))
                    {
                        //Node Asset
                        RadTreeNode nodeA = new RadTreeNode(a.Description);
                        if (string.IsNullOrEmpty(a.AnalyzerId))
                        {
                            nodeA.ForeColor = Color.Red;
                            nodeA.Value = "";
                        }
                        else
                        {
                            nodeA.Value = JsonConvert.SerializeObject(new SelectedContext(a.Id, AssetManagementEnum.AnalyzerContext.Asset));
                        }
                        nodeA.Expanded = true;

                        nodeD.Nodes.Add(nodeA);
                    }
                    nodeP.Nodes.Add(nodeD);
                }
                AssetTreeView.Nodes.Add(nodeP);
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate("ParameterCode", ddlParameter.SelectedValue);
            bool IsDataInserted = false;
            switch (rbtnListTimeScale.SelectedValue)
            {
                case "Day":
                    if (!(dayPickStart1.SelectedDate == null || dayPickEnd1.SelectedDate == null || dayPickStart2.SelectedDate == null || dayPickEnd2.SelectedDate == null))
                    {
                        IsDataInserted = true;
                    }
                    break;
                case "Month":
                case "Year":
                    if (!(monthPickStart1.SelectedDate == null || monthPickEnd1.SelectedDate == null || monthPickStart2.SelectedDate == null || monthPickEnd2.SelectedDate == null))
                    {
                        IsDataInserted = true;
                    }
                    break;
            }
            if (!IsDataInserted)
            {
                messaggio.Text = "Non tutte le date sono state inserite";
                dlgWidgetConfig.OpenDialog();
            }
            else
            {
                //if (dtPickEnd1.SelectedDate.Value - dtPickStart1.SelectedDate.Value == dtPickEnd2.SelectedDate.Value - dtPickStart2.SelectedDate.Value)
                //{}
                //else
                //{
                //CustomSettings.AddOrUpdate("DateStart1", dtPickStart1.SelectedDate.Value);
                //CustomSettings.AddOrUpdate("DateEnd1", dtPickEnd1.SelectedDate.Value);
                //CustomSettings.AddOrUpdate("DateStart2", dtPickStart2.SelectedDate.Value);
                //CustomSettings.AddOrUpdate("DateEnd2", dtPickEnd2.SelectedDate.Value);
                if (string.IsNullOrEmpty(hiddenSelection.Value))
                {
                    messaggio.Text = "Selezionare prima un asset";
                    dlgWidgetConfig.OpenDialog();
                }
                else
                {
                    CustomSettings.AddOrUpdate("SelectedContextId", hiddenSelection.Value);

                    switch (rbtnListTimeScale.SelectedValue)
                    {
                        case "Day"://EnergyEnum.TimeScale.Day.ToString():
                            if (dayPickStart1.SelectedDate.Value == dayPickEnd1.SelectedDate.Value)
                            {
                                CustomSettings.AddOrUpdate("TimeScale", EnergyEnum.TimeScale.Hour);
                            }
                            else
                            {
                                if ((dayPickEnd1.SelectedDate.Value - dayPickStart1.SelectedDate.Value).Days < 62)
                                {
                                    CustomSettings.AddOrUpdate("TimeScale", EnergyEnum.TimeScale.Day);
                                }
                                else
                                {
                                    CustomSettings.AddOrUpdate("TimeScale", EnergyEnum.TimeScale.Month);
                                }
                            }
                            CustomSettings.AddOrUpdate("DateStart1", dayPickStart1.SelectedDate.Value);
                            CustomSettings.AddOrUpdate("DateEnd1", dayPickEnd1.SelectedDate.Value);
                            CustomSettings.AddOrUpdate("DateStart2", dayPickStart2.SelectedDate.Value);
                            CustomSettings.AddOrUpdate("DateEnd2", dayPickEnd2.SelectedDate.Value);
                            break;
                        case "Month"://EnergyEnum.TimeScale.Month.ToString():
                        case "Year"://EnergyEnum.TimeScale.Year.ToString():
                            DateTime end1 = new DateTime(
                                monthPickEnd1.SelectedDate.Value.Year,
                                monthPickEnd1.SelectedDate.Value.Month,
                                DateTime.DaysInMonth(monthPickEnd1.SelectedDate.Value.Year, monthPickEnd1.SelectedDate.Value.Month)
                                );
                            DateTime end2 = new DateTime(
                                monthPickEnd2.SelectedDate.Value.Year,
                                monthPickEnd2.SelectedDate.Value.Month,
                                DateTime.DaysInMonth(monthPickEnd2.SelectedDate.Value.Year, monthPickEnd2.SelectedDate.Value.Month)
                                );
                            if (monthPickStart1.SelectedDate.Value.Month == monthPickEnd1.SelectedDate.Value.Month)
                            {
                                CustomSettings.AddOrUpdate("TimeScale", EnergyEnum.TimeScale.Day);
                            }
                            else
                            {
                                if ((end1 - monthPickStart1.SelectedDate.Value).Days > 62)
                                {
                                    if ((end1 - monthPickStart1.SelectedDate.Value).Days > 730)
                                    {
                                        CustomSettings.AddOrUpdate("TimeScale", EnergyEnum.TimeScale.Year);
                                    }
                                    else
                                    {
                                        CustomSettings.AddOrUpdate("TimeScale", EnergyEnum.TimeScale.Month);
                                    }
                                }
                                else
                                {
                                    CustomSettings.AddOrUpdate("TimeScale", EnergyEnum.TimeScale.Day);
                                }
                            }
                            CustomSettings.AddOrUpdate("DateStart1", monthPickStart1.SelectedDate.Value);
                            CustomSettings.AddOrUpdate("DateEnd1", end1);
                            CustomSettings.AddOrUpdate("DateStart2", monthPickStart2.SelectedDate.Value);
                            CustomSettings.AddOrUpdate("DateEnd2", end2);

                            break;
                    }

                    if ((DateEnd1.Value - DateStart1.Value).Days > (DateEnd2.Value - DateStart2.Value).Days)
                    {
                        //CreateXAxis(DateStart1.Value, DateEnd1.Value, TimeScale);
                        CreateChart(DateStart1.Value, DateEnd1.Value, ParameterCode, TimeScale, "Periodo1");
                        CreateChart(DateStart2.Value, DateEnd2.Value, ParameterCode, TimeScale, "Periodo2", false);
                    }
                    else
                    {
                        //CreateXAxis(DateStart2.Value, DateEnd2.Value, TimeScale);
                        CreateChart(DateStart2.Value, DateEnd2.Value, ParameterCode, TimeScale, "Periodo2");
                        CreateChart(DateStart1.Value, DateEnd1.Value, ParameterCode, TimeScale, "Periodo1", false);
                    }
                    switch (ParameterCode)
                    {
                        case "TotalActivePower":
                            GenericChart.PlotArea.YAxis.TitleAppearance.Text += "KW";
                            break;
                        case "TotalActiveEnergyConsumed":
                            GenericChart.PlotArea.YAxis.TitleAppearance.Text += "KWh";
                            break;
                        case "TotalReactiveEnergyConsumed":
                            GenericChart.PlotArea.YAxis.TitleAppearance.Text += "kvarh";
                            break;
                    }

                    updatePnlChart.Update();
                }
            //}
            }
        }

        protected void btnConfirmSelect_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate("SelectedContextId", hiddenSelection.Value);
            wndSelectAsset.CloseDialog();
        }

        protected void CreateXAxis(DateTime start, DateTime end, EnergyEnum.TimeScale typeOfTime)
        {
            switch (typeOfTime)
            {
                case EnergyEnum.TimeScale.Hour:
                    while (start.Date <= end.Date)
                    {
                        GenericChart.PlotArea.XAxis.Items.Add((start.Hour + 1).ToString());
                        start = start.AddHours(1);
                    }
                    break;
                case EnergyEnum.TimeScale.Day:
                    while (start.Date <= end.Date)
                    {
                        GenericChart.PlotArea.XAxis.Items.Add(start.Date.ToString("dd"));
                        start = start.AddDays(1);
                    }
                    //GenericChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 35;
                    break;
                case EnergyEnum.TimeScale.Month:
                    while (start.Date <= end.Date)
                    {
                        GenericChart.PlotArea.XAxis.Items.Add(start.Date.ToString("yyyy/MM"));
                        start = start.AddMonths(1);
                    }
                    break;
                case EnergyEnum.TimeScale.Year:
                    while (start.Date != end.Date)
                    {
                        GenericChart.PlotArea.XAxis.Items.Add(start.Date.ToString("yyyy"));
                        start = start.AddYears(1);
                    }
                    break;
            }
        }

        public void CreateChart(DateTime start, DateTime end, string paramName, EnergyEnum.TimeScale typeOfTime, string nameSerie, bool createX = true)
        {
            if (paramName == EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString())
            {

                EnergyDataService energyDataService = new EnergyDataService();
                EnergyConsumptionService ECS = new EnergyConsumptionService();
                List<EnergyConsumption> dataSource = ECS.GetEnergyDataList(energyDataService.GetAnalyzerId(SelectedContextId), start, end, typeOfTime);

                List<CategorySeriesItem> valueSerie = new List<CategorySeriesItem>();
                foreach (var item in dataSource)
                {
                    valueSerie.Add(new CategorySeriesItem(item.kWh));
                }

                LineSeries lineSerie = new LineSeries();
                lineSerie.Name = nameSerie;
                lineSerie.LabelsAppearance.Visible = false;
                lineSerie.SeriesItems.AddRange(valueSerie);
                GenericChart.PlotArea.Series.Add(lineSerie);
            }
            else
            {
                List<EnergyData> seriesItems = CreateSeriesItems(start, end, paramName, typeOfTime, createX);

                if (seriesItems != null)
                {
                    List<CategorySeriesItem> valueSerie = new List<CategorySeriesItem>();
                    List<CategorySeriesItem> minSerie = new List<CategorySeriesItem>();
                    List<CategorySeriesItem> maxSerie = new List<CategorySeriesItem>();

                    foreach (var item in seriesItems)
                    {
                        valueSerie.Add(new CategorySeriesItem((decimal)item.Value));
                        minSerie.Add(new CategorySeriesItem((decimal)item.Min));
                        maxSerie.Add(new CategorySeriesItem((decimal)item.Max));
                    }

                    //switch (chartType)
                    //{
                    //    case "ColumnSeries":
                    //        seriesItems = CreateSeriesItems<CategorySeriesItem>(dataHistory, paramName, start, typeOfTime);
                    //        ColumnSeries columnSerie = new ColumnSeries();
                    //        columnSerie.Name = nameSerie;
                    //        columnSerie.LabelsAppearance.Visible = false;
                    //        columnSerie.SeriesItems.AddRange(seriesItems["Value"]);
                    //        GenericChart.PlotArea.Series.Add(columnSerie);

                    //        if (paramName == "TotalActivePower")
                    //        {
                    //            ColumnSeries columnSerieMin = new ColumnSeries();
                    //            columnSerieMin.Name = "Minimi";
                    //            columnSerieMin.LabelsAppearance.Visible = false;
                    //            columnSerieMin.SeriesItems.AddRange(seriesItems["Min"]);
                    //            GenericChart.PlotArea.Series.Add(columnSerieMin);

                    //            ColumnSeries columnSerieMax = new ColumnSeries();
                    //            columnSerieMax.Name = "Picchi massimi";
                    //            columnSerieMax.LabelsAppearance.Visible = false;
                    //            columnSerieMax.SeriesItems.AddRange(seriesItems["Max"]);
                    //            GenericChart.PlotArea.Series.Add(columnSerieMax);
                    //        }
                    //        break;
                    //    case "LineSeries":
                    LineSeries lineSerie = new LineSeries();
                    lineSerie.Name = nameSerie;
                    lineSerie.LabelsAppearance.Visible = false;
                    lineSerie.SeriesItems.AddRange(valueSerie);
                    GenericChart.PlotArea.Series.Add(lineSerie);

                    //if (paramName == "TotalActivePower")
                    //{
                    //    LineSeries lineSerieMin = new LineSeries();
                    //    lineSerieMin.Name = "Minimi";
                    //    lineSerieMin.LabelsAppearance.Visible = false;
                    //    lineSerieMin.SeriesItems.AddRange(seriesItems["Min"]);
                    //    GenericChart.PlotArea.Series.Add(lineSerieMin);

                    //    LineSeries lineSerieMax = new LineSeries();
                    //    lineSerieMax.Name = "Picchi massimi";
                    //    lineSerieMax.LabelsAppearance.Visible = false;
                    //    lineSerieMax.SeriesItems.AddRange(seriesItems["Max"]);
                    //    GenericChart.PlotArea.Series.Add(lineSerieMax);
                    //}
                    //        break;
                    //}
                }
            }
        }

        private List<EnergyData> CreateSeriesItems(DateTime start, DateTime end, string paramName, EnergyEnum.TimeScale typeOfTime, bool createX = true)
        {
            EnergyDataService energyDataService = new EnergyDataService();

            var list = energyDataService.CreateHistorySerie(SelectedContextId, start, end, paramName, typeOfTime);

            if (list.Count > 0)
            {
                if (createX)
                {
                    foreach (var item in list)
                    {
                        switch (typeOfTime)
                        {
                            case EnergyEnum.TimeScale.Hour:
                                GenericChart.PlotArea.XAxis.Items.Add((item.Key.Hour + 1).ToString());
                                break;
                            case EnergyEnum.TimeScale.Day:
                                GenericChart.PlotArea.XAxis.Items.Add(item.Key.Date.ToString("dd"));
                                break;
                            case EnergyEnum.TimeScale.Month:
                                //GenericChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 35;
                                GenericChart.PlotArea.XAxis.Items.Add(item.Key.Date.ToString("yyyy/MM"));
                                break;
                        }
                    }
                }
                return list.Values.ToList();
            }
            return null;
        }

        protected void rbtnListTimeScale_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((sender as RadioButtonList).SelectedValue)
            {
                case "Day":
                    dayPickStart1.Visible = true;
                    dayPickEnd1.Visible = true;
                    monthPickStart1.Visible = false;
                    monthPickEnd1.Visible = false;
                    dayPickStart2.Visible = true;
                    dayPickEnd2.Visible = true;
                    monthPickStart2.Visible = false;
                    monthPickEnd2.Visible = false;
                    break;
                case "Month":
                case "Year":
                    dayPickStart1.Visible = false;
                    dayPickEnd1.Visible = false;
                    monthPickStart1.Visible = true;
                    monthPickEnd1.Visible = true;
                    dayPickStart2.Visible = false;
                    dayPickEnd2.Visible = false;
                    monthPickStart2.Visible = true;
                    monthPickEnd2.Visible = true;
                    break;
            }
            updatePnlDate.Update();
        }
    }
}