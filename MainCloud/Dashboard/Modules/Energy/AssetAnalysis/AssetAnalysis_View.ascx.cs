﻿using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule.ConsumptionStorage;
using EnergyModule.Models;
using EnergyModule.Services;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Energy.AssetAnalysis
{
    public partial class AssetAnalysis_View : DataWidget<AssetAnalysis_View>
    {
        public AssetAnalysis_View() : base(typeof(AssetAnalysis_View)) { }

        const string ASSET_GROUP_ID = "AssetGroupId";
        const string PLANT_ID = "PlantId";
        const string DEPARTMENT_ID = "DepartmentId";
        const string FILTER = "Filter";

        string AssetGroupId
        {
            get
            {
                return (string)CustomSettings.Find(ASSET_GROUP_ID);
            }
        }

        string PlantId
        {
            get
            {
                return (string)CustomSettings.Find(PLANT_ID);
            }
        }

        string Filter
        {
            get
            {
                return (string)CustomSettings.Find(FILTER);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AssetsChart.Visible = false;
                //AssetsChart.PlotArea.Series.Clear();
            }

            switch (rbtnFilter.SelectedValue)
            {
                case "Plant":
                    btnSelectGroup.Visible = false;
                    btnSelectDepartment.Visible = false;
                    btnSelectPlant.Visible = true;
                    break;
                case "Department":
                    btnSelectGroup.Visible = false;
                    btnSelectPlant.Visible = false;
                    btnSelectDepartment.Visible = true;
                    break;
                case "AssetGroup":
                    btnSelectPlant.Visible = false;
                    btnSelectDepartment.Visible = false;
                    btnSelectGroup.Visible = true;
                    break;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(Filter))
            //{
            //    rbtnFilter.Items.FindByValue(Filter).Selected = true;
            //}
            //switch (rbtnFilter.SelectedValue)
            //{
            //    case "Plant":
            //        if (!string.IsNullOrEmpty(PlantId))
            //        {
            //            PlantService PS = new PlantService();
            //            lblSelectedContext.Text = PS.GetDescriptionById(PlantId);
            //        }
            //        else
            //        {
            //            lblSelectedContext.Text = "";
            //        }
            //        break;
            //    case "AssetGroup":
            //        if (!string.IsNullOrEmpty(AssetGroupId))
            //        {
            //            AssetGroupService AGS = new AssetGroupService();
            //            lblSelectedContext.Text = AGS.GetDescriptionById(AssetGroupId);
            //        }
            //        else
            //        {
            //            lblSelectedContext.Text = "";
            //        }
            //        break;
            //}
        }

        #region AssetGroupTree
        protected void btnSelectGroup_Click(object sender, EventArgs e)
        {
            PlantTreeView.Visible = false;
            btnConfirmSelectPlant.Visible = false;
            DepartmentTreeView.Visible = false;
            btnConfirmSelectDepartment.Visible = false;
            AssetGroupTreeView.Visible = true;
            btnConfirmSelectGroup.Visible = true;
            LoadAssetGroupTree();
            wndSelectFilter.OpenDialog();
        }

        protected void LoadAssetGroupTree()
        {
            AssetGroupTreeView.Nodes.Clear();

            AssetGroupService service = new AssetGroupService();
            var groups = service.GroupList();
            foreach (AssetGroup g in groups.OrderBy(x => x.Description))
            {
                RadTreeNode node = new RadTreeNode(g.Description);
                node.Value = g.Id;
                node.Expanded = true;
                AssetGroupTreeView.Nodes.Add(node);
            }
        }

        protected void btnConfirmSelectGroup_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate(ASSET_GROUP_ID, hiddenSelection.Value);
            CustomSettings.AddOrUpdate(PLANT_ID, "");
            CustomSettings.AddOrUpdate(DEPARTMENT_ID, "");
            wndSelectFilter.CloseDialog();
        }
        #endregion

        #region PlantTree
        protected void btnSelectPlant_Click(object sender, EventArgs e)
        {
            AssetGroupTreeView.Visible = false;
            btnConfirmSelectGroup.Visible = false;
            DepartmentTreeView.Visible = false;
            btnConfirmSelectDepartment.Visible = false;
            PlantTreeView.Visible = true;
            btnConfirmSelectPlant.Visible = true;
            LoadPlantTree();
            wndSelectFilter.OpenDialog();
        }

        protected void LoadPlantTree()
        {
            PlantTreeView.Nodes.Clear();

            PlantService service = new PlantService();
            var plants = service.PlantList();
            foreach (Plant p in plants.OrderBy(x => x.Description))
            {
                RadTreeNode node = new RadTreeNode(p.Description);
                node.Value = p.Id;
                node.Expanded = true;
                PlantTreeView.Nodes.Add(node);
            }
        }

        protected void btnConfirmSelectPlant_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate(PLANT_ID, hiddenSelection.Value);
            CustomSettings.AddOrUpdate(DEPARTMENT_ID, "");
            CustomSettings.AddOrUpdate(ASSET_GROUP_ID, "");
            wndSelectFilter.CloseDialog();
        }
        #endregion

        #region DepartmentTree
        protected void btnSelectDepartment_Click(object sender, EventArgs e)
        {
            PlantTreeView.Visible = false;
            btnConfirmSelectPlant.Visible = false;
            AssetGroupTreeView.Visible = false;
            btnConfirmSelectGroup.Visible = false;
            DepartmentTreeView.Visible = true;
            btnConfirmSelectDepartment.Visible = true;
            LoadDepartmentTree();
            wndSelectFilter.OpenDialog();
        }

        protected void LoadDepartmentTree()
        {
            DepartmentTreeView.Nodes.Clear();

            DepartmentService service = new DepartmentService();
            var departments = service.DepartmentList();
            foreach (Department d in departments.OrderBy(x => x.Description))
            {
                RadTreeNode node = new RadTreeNode(d.Description);
                node.Value = d.Id;
                node.Expanded = true;
                DepartmentTreeView.Nodes.Add(node);
            }
        }

        protected void btnConfirmSelectDepartment_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate(DEPARTMENT_ID, hiddenSelection.Value);
            CustomSettings.AddOrUpdate(PLANT_ID, "");
            CustomSettings.AddOrUpdate(ASSET_GROUP_ID, "");
            wndSelectFilter.CloseDialog();
        }
        #endregion

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hiddenSelection.Value))
            {
                CustomSettings.AddOrUpdate(FILTER, rbtnFilter.SelectedValue);
                DateTime start = new DateTime();
                DateTime end = new DateTime();
                switch (rbtnListLastTime.SelectedValue)
                {
                    case "LastMonth":
                        start = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 01);
                        end = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month));
                        break;
                    case "CurrentYear":
                        start = new DateTime(DateTime.Today.Year, 01, 01);
                        end = DateTime.Today;
                        break;
                    case "LastYear":
                        start = new DateTime(DateTime.Today.Year - 1, DateTime.Today.AddMonths(-12).Month, 01);
                        end = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month));
                        break;
                }
                AssetsChart.Visible = true;
                switch (rbtnFilter.SelectedValue)
                {
                    case "Plant":
                        CreateChartByPlant(hiddenSelection.Value, start, end);
                        break;
                    case "Department":
                        CreateChartByDepartment(hiddenSelection.Value, start, end);
                        break;
                    case "AssetGroup":
                        CreateChartByAssetGroup(hiddenSelection.Value, start, end);
                        break;
                }
                //updatePnlData.Update();
            }
            else
            {
                messaggio.Text = "Selezionare prima un gruppo o un sito";
                dlgWidgetConfig.OpenDialog();
            }
        }

        protected void CreateChartByAssetGroup(string groupId, DateTime start, DateTime end)
        {
            AssetsChart.PlotArea.XAxis.Items.Clear();
            AssetsChart.PlotArea.Series[0].Items.Clear();
            AssetsChart.PlotArea.Series[1].Items.Clear();
            AssetsChart.PlotArea.Series[2].Items.Clear();

            AssetGroupService ASG = new AssetGroupService();
            List<Asset> assetList = ASG.GetAssetsByGroup(groupId);
            ConsumptionStorage storage = new ConsumptionStorage();
            foreach (Asset asset in assetList)
            {
                AssetsChart.PlotArea.XAxis.Items.Add(asset.Description);
                MonthlyConsumption tot = storage.GetTotalizerCostConsumption(asset.AnalyzerId, start, end);
                AssetsChart.PlotArea.Series[0].Items.Add(tot.kWhF1);
                AssetsChart.PlotArea.Series[1].Items.Add(tot.kWhF2);
                AssetsChart.PlotArea.Series[2].Items.Add(tot.kWhF3);
            }
        }

        protected void CreateChartByPlant(string plantId, DateTime start, DateTime end)
        {
            AssetsChart.PlotArea.XAxis.Items.Clear();
            AssetsChart.PlotArea.Series[0].Items.Clear();
            AssetsChart.PlotArea.Series[1].Items.Clear();
            AssetsChart.PlotArea.Series[2].Items.Clear();

            PlantService PS = new PlantService();
            List<Asset> assetList = PS.GetAssetListByPlant(plantId);
            ConsumptionStorage storage = new ConsumptionStorage();
            foreach (Asset asset in assetList)
            {
                AssetsChart.PlotArea.XAxis.Items.Add(asset.Description);
                MonthlyConsumption tot = storage.GetTotalizerCostConsumption(asset.AnalyzerId, start, end);
                AssetsChart.PlotArea.Series[0].Items.Add(tot.kWhF1);
                AssetsChart.PlotArea.Series[1].Items.Add(tot.kWhF2);
                AssetsChart.PlotArea.Series[2].Items.Add(tot.kWhF3);
            }
        }

        protected void CreateChartByDepartment(string departmentId, DateTime start, DateTime end)
        {
            AssetsChart.PlotArea.XAxis.Items.Clear();
            AssetsChart.PlotArea.Series[0].Items.Clear();
            AssetsChart.PlotArea.Series[1].Items.Clear();
            AssetsChart.PlotArea.Series[2].Items.Clear();

            AssetService AS = new AssetService();
            List<Asset> assetList = AS.GetAssetsByDepartment(departmentId);
            ConsumptionStorage storage = new ConsumptionStorage();
            foreach (Asset asset in assetList)
            {
                AssetsChart.PlotArea.XAxis.Items.Add(asset.Description);
                MonthlyConsumption tot = storage.GetTotalizerCostConsumption(asset.AnalyzerId, start, end);
                AssetsChart.PlotArea.Series[0].Items.Add(tot.kWhF1); //F1Serie.SeriesItems.Add(new CategorySeriesItem(tot.kWhF1));
                AssetsChart.PlotArea.Series[1].Items.Add(tot.kWhF2); //F2Serie.SeriesItems.Add(new CategorySeriesItem(tot.kWhF2));
                AssetsChart.PlotArea.Series[2].Items.Add(tot.kWhF3); //F3Serie.SeriesItems.Add(new CategorySeriesItem(tot.kWhF3));
            }
        }

        protected void rbtnFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (rbtnFilter.SelectedValue)
            {
                case "Plant":
                    btnSelectGroup.Visible = false;
                    btnSelectDepartment.Visible = false;
                    btnSelectPlant.Visible = true;
                    break;
                case "Department":
                    btnSelectGroup.Visible = false;
                    btnSelectPlant.Visible = false;
                    btnSelectDepartment.Visible = true;
                    break;
                case "AssetGroup":
                    btnSelectPlant.Visible = false;
                    btnSelectDepartment.Visible = false;
                    btnSelectGroup.Visible = true;
                    break;
            }
            lblSelectedContext.Text = "";
            updPnlButtons.Update();
        }
    }
}