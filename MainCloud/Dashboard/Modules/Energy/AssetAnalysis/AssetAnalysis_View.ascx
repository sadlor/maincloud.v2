﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssetAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.AssetAnalysis.AssetAnalysis_View" %>

<link href="/Dashboard/WidgetStyle.css" rel="stylesheet">

<telerik:RadDockZone ID="dockZoneSettings" runat="server" Orientation="Vertical" BorderStyle="None">
    <telerik:RadDock RenderMode="Auto" ID="dockSettings" runat="server" Title="Settings"
        EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false" Resizable="true" Width="100%">
        <ContentTemplate>
            <div class="row" style="width:100%;">
                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:RadioButtonList ID="rbtnFilter" runat="server" RepeatDirection="Horizontal" style="margin-left:18px;"
                                AutoPostBack="true" OnSelectedIndexChanged="rbtnFilter_SelectedIndexChanged">
                                <asp:ListItem Text="Plant" Value="Plant" Selected="True" />
                                <asp:ListItem Text="Department" Value="Department" />
                                <asp:ListItem Text="Group" Value="AssetGroup" />
                            </asp:RadioButtonList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                    <asp:UpdatePanel runat="server" ID="updPnlButtons" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button runat="server" ID="btnSelectGroup" Text="Select Group" OnClick="btnSelectGroup_Click" CssClass="btnConfig" />
                            <asp:Button runat="server" ID="btnSelectPlant" Text="Select Plant" OnClick="btnSelectPlant_Click" CssClass="btnConfig" />
                            <asp:Button runat="server" ID="btnSelectDepartment" Text="Select Department" OnClick="btnSelectDepartment_Click" CssClass="btnConfig" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <%--<div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                    <asp:Button runat="server" ID="btnSelect" Text="Select Group Asset" OnClick="btnSelect_Click" CssClass="btnConfig" />
                </div>--%>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                    <asp:Label CssClass="btn btn-success" ID="lblSelectedContext" runat="server" />
                </div>
            </div>
            <%--<div class="row" style="width:100%;">
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                    <asp:Button runat="server" ID="btnSelectPlant" Text="Select Plant" OnClick="btnSelectPlant_Click" CssClass="btnConfig" />
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                    <asp:Label CssClass="btn btn-success" ID="lblSelectedPlant" runat="server" />
                </div>
            </div>--%>

            <br />

            <div class="row" style="width:100%;">
                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                    <asp:RadioButtonList ID="rbtnListLastTime" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" style="margin-left:18px;">
                        <asp:ListItem Text="Last month" Value="LastMonth" Selected="True" />
                        <asp:ListItem Text="Current year" Value="CurrentYear" />
                        <asp:ListItem Text="Last 12 months" Value="LastYear" />
                    </asp:RadioButtonList>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                    <br />
                    <%--<asp:UpdatePanel runat="server">
                        <ContentTemplate>--%>
                            <asp:LinkButton ID="btnConfirm" Text="Confirm <i class='fa fa-check-circle'></i>" runat="server" OnClick="btnConfirm_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;" />
                        <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
            </div>
        </ContentTemplate>
    </telerik:RadDock>
</telerik:RadDockZone>

<%--<asp:UpdatePanel runat="server" ID="updatePnlData" UpdateMode="Conditional">
    <ContentTemplate>--%>
        <telerik:RadHtmlChart ID="AssetsChart" runat="server">
            <PlotArea>
                <Series>
                   <telerik:ColumnSeries Name="F1" GroupName="Fasce">
                        <Appearance>
                            <FillStyle BackgroundColor="#ffc700" />
                        </Appearance>
                        <LabelsAppearance Visible="false" />
                    </telerik:ColumnSeries>
                    <telerik:ColumnSeries Name="F2" GroupName="Fasce">
                        <Appearance>
                            <FillStyle BackgroundColor="#2a94cb" />
                        </Appearance>
                        <LabelsAppearance Visible="false" />
                    </telerik:ColumnSeries>
                    <telerik:ColumnSeries Name="F3" GroupName="Fasce">
                        <Appearance>
                            <FillStyle BackgroundColor="#8dcb2a" />
                        </Appearance>
                        <LabelsAppearance Visible="false" />
                    </telerik:ColumnSeries>                    
                </Series>
                <YAxis>
                    <TitleAppearance Text="kWh" />
                </YAxis>
            </PlotArea>
        </telerik:RadHtmlChart>

        <asp:HiddenField ID="hiddenSelection" runat="server" />

        <asp:UpdateProgress runat="server">
            <ProgressTemplate>
                <div class="loading">Loading&#8230;</div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    <%--</ContentTemplate>
</asp:UpdatePanel>--%>

<mcf:PopUpDialog ID="wndSelectFilter" runat="server" Title="Seleziona filtro">
    <Body>
        <div class="row">
            <div class="col-xs-6 col-md-6">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <telerik:RadTreeView runat="Server" ID="AssetGroupTreeView" EnableViewState="false" EnableDragAndDrop="false" EnableDragAndDropBetweenNodes="false" 
                            OnClientNodeClicked="function(sender, args){window['TreeNodeClick_' + sender._element.id](sender, args)}" />
                        <telerik:RadTreeView runat="Server" ID="PlantTreeView" EnableViewState="false" EnableDragAndDrop="false" EnableDragAndDropBetweenNodes="false"
                            OnClientNodeClicked="function(sender, args){window['TreeNodeClick_' + sender._element.id](sender, args)}" />
                        <telerik:RadTreeView runat="Server" ID="DepartmentTreeView" EnableViewState="false" EnableDragAndDrop="false" EnableDragAndDropBetweenNodes="false"
                            OnClientNodeClicked="function(sender, args){window['TreeNodeClick_' + sender._element.id](sender, args)}" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="col-xs-1 col-md-1"></div>
            <div class="col-xs-5 col-md-5">
                <br /><br /><br />
                <div class="row">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnConfirmSelectGroup" Text="Confirm <i class='fa fa-arrow-circle-right'></i>" runat="server"
                                CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;margin-left:50%;"
                                OnClick="btnConfirmSelectGroup_Click" />
                            <asp:LinkButton ID="btnConfirmSelectPlant" Text="Confirm <i class='fa fa-arrow-circle-right'></i>" runat="server"
                                CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;margin-left:50%;"
                                OnClick="btnConfirmSelectPlant_Click" />
                            <asp:LinkButton ID="btnConfirmSelectDepartment" Text="Confirm <i class='fa fa-arrow-circle-right'></i>" runat="server"
                                CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;margin-left:50%;"
                                OnClick="btnConfirmSelectDepartment_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </Body>
</mcf:PopUpDialog>

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
    <Body>
        <asp:label id="messaggio" runat="server"/>
    </Body>
</mcf:PopUpDialog>

<script src="<%=ResolveUrl("~/Dashboard/WidgetGenericScript/SettingsGraphScript.js")%>"></script>
<script type="text/javascript">
    window.onload = UpdateButton();
    <%--$(function () {
        //UpdateButton();

        $(<%= rbtnFilter.ClientID %>).on('click', function () {
            UpdateButton();
        });
    });--%>

    function UpdateButton()
    {
        switch ($('#<%= rbtnFilter.ClientID %> input:checked').val())
        {
            case "Plant":
                $('#<%= btnSelectGroup.ClientID %>').hide();
                $('#<%= btnSelectDepartment.ClientID %>').hide();
                $('#<%= btnSelectPlant.ClientID %>').show();
                break;
            case "Department":
                $('#<%= btnSelectPlant.ClientID %>').hide();
                $('#<%= btnSelectGroup.ClientID %>').hide();
                $('#<%= btnSelectDepartment.ClientID %>').show();
                break;
            case "AssetGroup":
                $('#<%= btnSelectPlant.ClientID %>').hide();
                $('#<%= btnSelectDepartment.ClientID %>').hide();
                $('#<%= btnSelectGroup.ClientID %>').show();
                break;
        }
    }

    function TreeNodeClick_<%= AssetGroupTreeView.ClientID %>(sender, args) {
        if (args.get_node().get_value() != null && args.get_node().get_value() != "") {
            $(<%= btnConfirmSelectGroup.ClientID %>).removeAttr('disabled');
            SaveSelection("<%= hiddenSelection.ClientID %>", '<%= lblSelectedContext.ClientID %>', args.get_node().get_value(), args.get_node().get_text());
        }
        else
        {
            $(<%= btnConfirmSelectGroup.ClientID %>).attr('disabled', 'disabled');
        }
    }

    function TreeNodeClick_<%= PlantTreeView.ClientID %>(sender, args) {
        if (args.get_node().get_value() != null && args.get_node().get_value() != "") {
            $(<%= btnConfirmSelectPlant.ClientID %>).removeAttr('disabled');
            SaveSelection("<%= hiddenSelection.ClientID %>", '<%= lblSelectedContext.ClientID %>', args.get_node().get_value(), args.get_node().get_text());
        }
        else {
            $(<%= btnConfirmSelectPlant.ClientID %>).attr('disabled', 'disabled');
        }
    }

    function TreeNodeClick_<%= DepartmentTreeView.ClientID %>(sender, args) {
        if (args.get_node().get_value() != null && args.get_node().get_value() != "") {
            $(<%= btnConfirmSelectDepartment.ClientID %>).removeAttr('disabled');
            SaveSelection("<%= hiddenSelection.ClientID %>", '<%= lblSelectedContext.ClientID %>', args.get_node().get_value(), args.get_node().get_text());
        }
        else {
            $(<%= btnConfirmSelectDepartment.ClientID %>).attr('disabled', 'disabled');
        }
    }
</script>