﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Energy.CounterActive
{
    public static class CounterEnum
    {
        public enum ParamToRead
        {
            TotalActiveEnergyConsumed,
            TotalReactiveEnergyConsumed
        }

        public enum SettingsKey
        {
            ParamToRead
        }
    }
}