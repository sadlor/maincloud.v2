﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CounterActive_View.ascx.cs" Inherits="MainEnergy.Dashboard.Modules.Energy.CounterActive.CounterActive_View" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %> 

<telerik:RadTextBox runat="server" Text="" ID="txtContatore" Width="100%" style="background-color:rgb(145, 220, 243);"></telerik:RadTextBox>

<script src="<%=ResolveUrl("~/Dashboard/Modules/Energy/CounterActive/CounterScript.js")%>"></script>
<script type="text/javascript">

    $(function () {
        UpdateCounter('<%= MainCloudFramework.Web.Helpers.MultiTenantsHelper.MountAndResolveMultiTenantUrl("/Dashboard/Modules/Energy/CounterActive/ActiveEnergyCounter.svc/GetValue") %>', '<%=txtContatore.ClientID %>', "kWh", '<%= this.CustomSettings.Find(MainCloud.Dashboard.Modules.Energy.CounterActive.CounterEnum.SettingsKey.ParamToRead.ToString()) %>', false);
        setInterval(function () {
            UpdateCounter('<%= MainCloudFramework.Web.Helpers.MultiTenantsHelper.MountAndResolveMultiTenantUrl("/Dashboard/Modules/Energy/CounterActive/ActiveEnergyCounter.svc/GetValue") %>', '<%=txtContatore.ClientID %>', "kWh", '<%= this.CustomSettings.Find(MainCloud.Dashboard.Modules.Energy.CounterActive.CounterEnum.SettingsKey.ParamToRead.ToString()) %>', false);
        }, 60000); //5 min
    });

</script>