﻿using EnergyModule.Core;
using EnergyModule.Services;
using MainCloudFramework.Web.Multitenants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace MainCloud.Dashboard.Modules.Energy.CounterActive
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ActiveEnergyCounter
    {
        // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
        // To create an operation that returns XML,
        //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     and include the following line in the operation body:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        [WebGet]
        public string GetValue(CounterEnum.ParamToRead paramName, bool progressive)
        {
            try
            {
                var tenantName = HttpContext.Current.Request.QueryString["MultiTenant"];
                HttpMultitenantsSessionState SessionMultitenant = HttpContext.Current.Session.SessionMultitenant(tenantName);

                if (SessionMultitenant.ContainsKey(EMConstants.SELECTED_ID_CONTEXT) && SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT] != null)
                {
                    SelectedContext selectedContext = JsonConvert.DeserializeObject<SelectedContext>((string)SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT]);

                    EnergyDataService energyDataService = new EnergyDataService();
                    string analyzerId = energyDataService.GetAnalyzerId(selectedContext);
                    if (!string.IsNullOrEmpty(analyzerId))
                    {
                        var energyList = energyDataService.GetAnalyzerEnergyDataList(x => x.AnalyzerId == analyzerId && x.TimeStamp >= DateTime.Today);

                        EnergyRecord last = JsonConvert.DeserializeObject<EnergyRecord>(energyList.LastOrDefault(
                            x => JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Count > 0 &&
                                 JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Exist(paramName.ToString())).Data);
                        switch (paramName)
                        {
                            case CounterEnum.ParamToRead.TotalActiveEnergyConsumed:
                                if (!SessionMultitenant.ContainsKey(EMConstants.START_ACTIVEENERGY_COUNTER) || SessionMultitenant[EMConstants.START_ACTIVEENERGY_COUNTER] == null)
                                {
                                    try
                                    {
                                        EnergyRecord first = JsonConvert.DeserializeObject<EnergyRecord>(energyList.FirstOrDefault(
                                            x => JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Count > 0 &&
                                                 JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Exist(paramName.ToString()) &&
                                                 JsonConvert.DeserializeObject<EnergyRecord>(x.Data).GetData(paramName.ToString()) > 0).Data);
                                        SessionMultitenant[EMConstants.START_ACTIVEENERGY_COUNTER] = first.GetData<decimal>(paramName.ToString());
                                    }
                                    catch (Exception ex)
                                    {
                                        return Math.Round(last.GetData<decimal>(paramName.ToString()), 1).ToString();
                                    }
                                }
                                break;
                            case CounterEnum.ParamToRead.TotalReactiveEnergyConsumed:
                                if (!SessionMultitenant.ContainsKey(EMConstants.START_REACTIVEENERGY_COUNTER) || SessionMultitenant[EMConstants.START_ACTIVEENERGY_COUNTER] == null)
                                {
                                    try
                                    {
                                        EnergyRecord first = JsonConvert.DeserializeObject<EnergyRecord>(energyList.FirstOrDefault(
                                            x => JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Count > 0 &&
                                                 JsonConvert.DeserializeObject<EnergyRecord>(x.Data).Exist(paramName.ToString()) &&
                                                 JsonConvert.DeserializeObject<EnergyRecord>(x.Data).GetData(paramName.ToString()) > 0).Data);
                                        SessionMultitenant[EMConstants.START_REACTIVEENERGY_COUNTER] = first.GetData<decimal>(paramName.ToString());
                                    }
                                    catch (Exception ex)
                                    {
                                        return Math.Round(last.GetData<decimal>(paramName.ToString()), 1).ToString();
                                    }
                                }
                                break;
                        }
                        if (progressive)
                        {
                            return (Math.Round(last.GetData<decimal>(paramName.ToString()), 1)).ToString();
                        }
                        else
                        {
                            return (Math.Round(last.GetData<decimal>(paramName.ToString()) - Convert.ToDecimal(SessionMultitenant[EMConstants.START_ACTIVEENERGY_COUNTER]), 1)).ToString();
                        }
                    }
                    else
                    {
                        //analyzerId null
                        return "No data entry";
                    }

                }
                else
                {
                    throw new Exception("Non è stato selezionato un contesto"); //visualizzare l'avviso
                }
            }
            catch (Exception ex)
            {
                return "No data entry";
            }
        }

        // Add more operations here and mark them with [OperationContract]
    }
}
