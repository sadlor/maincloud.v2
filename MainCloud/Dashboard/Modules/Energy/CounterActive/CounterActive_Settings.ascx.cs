﻿using MainCloudFramework.UI.Modules;
using MainCloudFramework.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.Energy.CounterActive
{
    public partial class CounterActive_Settings : WidgetSetting
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] items = System.Enum.GetNames(typeof(CounterEnum.ParamToRead));
            foreach (string item in items)
            {
                ListItem newItem = new ListItem(item, item);
                ddlParamToRead.Items.Add(newItem);
            }
        }

        public override bool ApplyChanges(DataCustomSettings widgetCustomSettings)
        {
            widgetCustomSettings.AddOrUpdate(CounterEnum.SettingsKey.ParamToRead.ToString(), ddlParamToRead.SelectedValue);
            return true;
        }

        public override void SyncChanges(DataCustomSettings widgetCustomSettings)
        {
            if (!string.IsNullOrEmpty((string)widgetCustomSettings.Find(CounterEnum.SettingsKey.ParamToRead.ToString())))
            {
                ddlParamToRead.SelectedValue = (string)widgetCustomSettings.Find(CounterEnum.SettingsKey.ParamToRead.ToString());
            }
        }
    }
}