﻿function UpdateCounter(path, CounterId, measureUnit, paramName, progressive) {
    var request = $.ajax({
        url: path,
        method: 'GET',
        data: { 'paramName': paramName, 'progressive': progressive },//JSON.stringify({ paramName: paramName, progressive: progressive }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json'
    });

    request.done(function (response) {  //string
        if (response.d != "") {
            $find(CounterId).set_displayValue(response.d + " " + measureUnit);
        }
    });

    request.fail(function (jqXHR, textStatus) {
        //alert("Request failed: " + textStatus);
    });
}