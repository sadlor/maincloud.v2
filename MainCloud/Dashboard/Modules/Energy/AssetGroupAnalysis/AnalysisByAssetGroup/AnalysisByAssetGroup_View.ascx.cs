﻿using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule.ConsumptionStorage;
using EnergyModule.Models;
using EnergyModule.Services;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Energy.AssetGroupAnalysis.AnalysisByAssetGroup
{
    public partial class AnalysisByAssetGroup_View : DataWidget<AnalysisByAssetGroup_View>
    {
        public AnalysisByAssetGroup_View() : base(typeof(AnalysisByAssetGroup_View)) { }

        const string ASSET_GROUP_ID = "AssetGroupId";
        const string BEHAVIOUR = "Behaviour";

        string AssetGroupId
        {
            get
            {
                return (string)CustomSettings.Find(ASSET_GROUP_ID);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridAssetGroup.Visible = false;
                ChartAssetGroup.Visible = false;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(AssetGroupId))
            {
                AssetGroupService AGS = new AssetGroupService();
                lblSelectedContext.Text = AGS.GetDescriptionById(AssetGroupId);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            LoadAssetGroupTree();
            wndSelectAssetGroup.OpenDialog();
        }

        protected void LoadAssetGroupTree()
        {
            AssetGroupTreeView.Nodes.Clear();

            AssetGroupService service = new AssetGroupService();
            var groups = service.GroupList();
            foreach (AssetGroup g in groups.OrderBy(x => x.Description))
            {
                RadTreeNode node = new RadTreeNode(g.Description);
                //if (string.IsNullOrEmpty(p.AnalyzerId))
                //{
                //    node.ForeColor = Color.Red;
                //    node.Value = "";
                //}
                //else
                //{
                    node.Value = g.Id;
                //}
                node.Expanded = true;
                AssetGroupTreeView.Nodes.Add(node);
            }
        }

        protected void btnConfirmSelect_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate(ASSET_GROUP_ID, hiddenSelection.Value);
            wndSelectAssetGroup.CloseDialog();
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            //CustomSettings.AddOrUpdate(BEHAVIOUR, rbtnBehaviour.SelectedValue);
            DateTime start = new DateTime();
            DateTime end = new DateTime();
            switch (rbtnListLastTime.SelectedValue)
            {
                case "CurrentYear":
                    start = new DateTime(DateTime.Today.Year, 01, 01);
                    end = DateTime.Today;
                    break;
                case "LastYear":
                    start = new DateTime(DateTime.Today.Year - 1, DateTime.Today.AddMonths(-12).Month, 01);
                    end = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month));
                    break;
            }
            if (rbtnBehaviour.SelectedValue == "Table")
            {
                ChartAssetGroup.Visible = false;
                GridAssetGroup.Visible = true;
                CreateTableDataSource(start, end);
            }
            else
            {
                GridAssetGroup.Visible = false;
                ChartAssetGroup.Visible = true;
                CreateChartDataSource(start, end);
            }
            updatePnlData.Update();
        }

        protected void CreateTableDataSource(DateTime start, DateTime end)
        {
            AssetGroupService AGS = new AssetGroupService();
            List<Asset> assetList = AGS.GetAssetsByGroup(AssetGroupId);
            List<MonthlyConsumption> dataSource = new List<MonthlyConsumption>();

            List<MonthlyConsumption> list = new List<MonthlyConsumption>();
            foreach (Asset assetItem in assetList)
            {
                ConsumptionStorage storage = new ConsumptionStorage();
                list.AddRange(storage.GetList(assetItem.AnalyzerId, start, end));  //valutare se non c'è analizzatore collegato
                //foreach (var item in list)
                //{
                //    dataSource.Add(new DataSource(assetItem, item));
                //}
            }

            foreach (DateTime date in list.Select(x => x.Date).Distinct())
            {
                MonthlyConsumption item = list.Where(x => x.Date == date).First();
                item.kWh = list.Where(x => x.Date == date).Sum(x => x.kWh);
                item.kWhF1 = list.Where(x => x.Date == date).Sum(x => x.kWhF1);
                item.kWhF2 = list.Where(x => x.Date == date).Sum(x => x.kWhF2);
                item.kWhF3 = list.Where(x => x.Date == date).Sum(x => x.kWhF3);
                dataSource.Add(item);
            }
            GridAssetGroup.DataSource = dataSource;
            GridAssetGroup.DataBind();
        }

        protected void CreateChartDataSource(DateTime start, DateTime end)
        {
            ChartAssetGroup.PlotArea.Series.Clear();
            ChartAssetGroup.PlotArea.XAxis.Items.Clear();

            AssetGroupService AGS = new AssetGroupService();
            List<Asset> assetList = AGS.GetAssetsByGroup(AssetGroupId);

            List<DateTime> dateList = new List<DateTime>();

            List<MonthlyConsumption> list = new List<MonthlyConsumption>();
            foreach (Asset assetItem in assetList)
            {
                ConsumptionStorage storage = new ConsumptionStorage();
                list.AddRange(storage.GetList(assetItem.AnalyzerId, start, end));  //valutare se non c'è analizzatore collegato
                //List<MonthlyConsumption> list = storage.GetList(assetItem.AnalyzerId, start, end);  //valutare se non c'è analizzatore collegato

                dateList.AddRange(list.Select(x => x.Date).Distinct().OrderBy(x => x.Date)); // per l'asse x

                //ColumnSeries F1Serie = new ColumnSeries();
                //F1Serie.Name = assetItem.Description + " - F1";
                //F1Serie.GroupName = assetItem.Description;
                //F1Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#ffc700");
                //F1Serie.LabelsAppearance.Visible = false;
                //F1Serie.SeriesItems.AddRange(list.Select(x => new CategorySeriesItem(x.kWhF1)));
                //ChartAssetGroup.PlotArea.Series.Add(F1Serie);

                //ColumnSeries F2Serie = new ColumnSeries();
                //F2Serie.Name = assetItem.Description + " - F2";
                //F2Serie.GroupName = assetItem.Description;
                //F2Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#2a94cb");
                //F2Serie.LabelsAppearance.Visible = false;
                //F2Serie.SeriesItems.AddRange(list.Select(x => new CategorySeriesItem(x.kWhF2)));
                //ChartAssetGroup.PlotArea.Series.Add(F2Serie);

                //ColumnSeries F3Serie = new ColumnSeries();
                //F3Serie.Name = assetItem.Description + " - F3";
                //F3Serie.GroupName = assetItem.Description;
                //F3Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#8dcb2a");
                //F3Serie.LabelsAppearance.Visible = false;
                //F3Serie.SeriesItems.AddRange(list.Select(x => new CategorySeriesItem(x.kWhF3)));
                //ChartAssetGroup.PlotArea.Series.Add(F3Serie);
            }

            List<MonthlyConsumption> dataSource = new List<MonthlyConsumption>();
            foreach (DateTime date in list.Select(x => x.Date).Distinct())
            {
                MonthlyConsumption item = list.Where(x => x.Date == date).First();
                item.kWh = list.Where(x => x.Date == date).Sum(x => x.kWh);
                item.kWhF1 = list.Where(x => x.Date == date).Sum(x => x.kWhF1);
                item.kWhF2 = list.Where(x => x.Date == date).Sum(x => x.kWhF2);
                item.kWhF3 = list.Where(x => x.Date == date).Sum(x => x.kWhF3);
                dataSource.Add(item);
            }

            ColumnSeries F1Serie = new ColumnSeries();
            F1Serie.Name = "F1";
            F1Serie.GroupName = "F";
            F1Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#ffc700");
            F1Serie.LabelsAppearance.Visible = false;
            //F1Serie.SeriesItems.Add(list.Sum(x => x.kWhF1));
            F1Serie.SeriesItems.AddRange(dataSource.Select(x => new CategorySeriesItem(x.kWhF1)));
            ChartAssetGroup.PlotArea.Series.Add(F1Serie);

            ColumnSeries F2Serie = new ColumnSeries();
            F2Serie.Name = "F2";
            F2Serie.GroupName = "F";
            F2Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#2a94cb");
            F2Serie.LabelsAppearance.Visible = false;
            //F2Serie.SeriesItems.Add(list.Sum(x => x.kWhF2));
            F2Serie.SeriesItems.AddRange(dataSource.Select(x => new CategorySeriesItem(x.kWhF2)));
            ChartAssetGroup.PlotArea.Series.Add(F2Serie);

            ColumnSeries F3Serie = new ColumnSeries();
            F3Serie.Name = "F3";
            F3Serie.GroupName = "F";
            F3Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#8dcb2a");
            F3Serie.LabelsAppearance.Visible = false;
            //F3Serie.SeriesItems.Add(list.Sum(x => x.kWhF3));
            F3Serie.SeriesItems.AddRange(dataSource.Select(x => new CategorySeriesItem(x.kWhF3)));
            ChartAssetGroup.PlotArea.Series.Add(F3Serie);

            foreach (var data in dateList.Distinct().OrderBy(x => x.Date))
            {
                ChartAssetGroup.PlotArea.XAxis.Items.Add(data.ToString("MM/yyyy"));
            }
        }

        private class DataSource
        {
            public Asset Asset { get; set; }
            public MonthlyConsumption Data { get; set; }

            public DataSource(Asset a, MonthlyConsumption d)
            {
                Asset = a;
                Data = d;
            }
        }
    }
}