﻿using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule.Models;
using EnergyModule.Services;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.Energy.AssetGroupAnalysis.AllAssetGroupsAnalysis
{
    public partial class AllAssetGroupsAnalysis_View : DataWidget<AllAssetGroupsAnalysis_View>
    {
        public AllAssetGroupsAnalysis_View() : base(typeof(AllAssetGroupsAnalysis_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridAssetGroup.Visible = false;
            ChartAssetGroup.Visible = false;
            ChartAssetGroup.PlotArea.Series.Clear();
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            DateTime start = new DateTime();
            DateTime end = new DateTime();
            switch (rbtnListLastTime.SelectedValue)
            {
                case "CurrentYear":
                    start = new DateTime(DateTime.Today.Year, 01, 01);
                    end = DateTime.Today;
                    break;
                case "LastYear":
                    start = new DateTime(DateTime.Today.Year - 1, DateTime.Today.AddMonths(-12).Month, 01);
                    end = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month));
                    break;
            }

            if (rbtnBehaviour.SelectedValue == "Table")
            {
                ChartAssetGroup.Visible = false;
                GridAssetGroup.Visible = true;
                CreateTableDataSource(start, end);
            }
            else
            {
                GridAssetGroup.Visible = false;
                ChartAssetGroup.Visible = true;
                CreateChartDataSource(start, end);
            }
            updatePnlData.Update();
        }

        protected void CreateTableDataSource(DateTime start, DateTime end)
        {
            AssetGroupService AGS = new AssetGroupService();
            EnergyConsumptionService ECS = new EnergyConsumptionService();
            List<AssetGroup> groupList = AGS.GroupList();
            List<DataSource> dataSource = new List<DataSource>();
            foreach (AssetGroup group in groupList)
            {
                List<MonthlyConsumption> list = ECS.TotalAssetGroupConsumption(group.Id, start, end);
                foreach (var item in list)
                {
                    dataSource.Add(new DataSource(group, item));
                }
            }
            GridAssetGroup.DataSource = dataSource;
            GridAssetGroup.DataBind();
        }

        protected void CreateChartDataSource(DateTime start, DateTime end)
        {
            AssetGroupService AGS = new AssetGroupService();
            EnergyConsumptionService ECS = new EnergyConsumptionService();
            List<AssetGroup> list = AGS.GroupList();
            foreach (AssetGroup item in list)
            {
                ECS.TotalAssetGroupConsumption(item.Id, start, end);
            }
        }

        private class DataSource
        {
            public AssetGroup Group { get; set; }
            public MonthlyConsumption Data { get; set; }

            public DataSource(AssetGroup group, MonthlyConsumption d)
            {
                Group = group;
                Data = d;
            }
        }
    }
}