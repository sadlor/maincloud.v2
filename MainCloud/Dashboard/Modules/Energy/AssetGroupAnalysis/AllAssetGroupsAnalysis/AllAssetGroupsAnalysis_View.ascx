﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AllAssetGroupsAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.AssetGroupAnalysis.AllAssetGroupsAnalysis.AllAssetGroupsAnalysis_View" %>

<link href="/Dashboard/WidgetStyle.css" rel="stylesheet">

<telerik:RadDockZone ID="dockZoneSettings" runat="server" Orientation="Vertical" BorderStyle="None">
    <telerik:RadDock RenderMode="Auto" ID="dockSettings" runat="server" Title="Settings"
        EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false" Resizable="true" Width="100%">
        <ContentTemplate>
            <div class="row" style="width:100%;">
                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                    <asp:RadioButtonList ID="rbtnListLastTime" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" style="margin-left:18px;">
                        <asp:ListItem Text="Last 12 months" Value="LastYear" />
                        <asp:ListItem Text="Current year" Value="CurrentYear" Selected="True" />
                    </asp:RadioButtonList>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                    <asp:RadioButtonList runat="server" ID="rbtnBehaviour" AutoPostBack="false" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Table" Value="Table" Selected="True" />
                        <asp:ListItem Text="Chart" Value="Chart" />
                    </asp:RadioButtonList>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                    <br />
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnConfirm" Text="Confirm <i class='fa fa-check-circle'></i>" runat="server" OnClick="btnConfirm_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </ContentTemplate>
    </telerik:RadDock>
</telerik:RadDockZone>

<asp:UpdatePanel runat="server" ID="updatePnlData" UpdateMode="Conditional">
    <ContentTemplate>

        <telerik:RadHtmlChart ID="ChartAssetGroup" runat="server" />
        <telerik:RadGrid ID="GridAssetGroup" runat="server" RenderMode="Lightweight" AllowSorting="true">
            <MasterTableView FilterExpression="" Caption="Analisi gruppi asset" AllowMultiColumnSorting="true">
                <SortExpressions>
                    <telerik:GridSortExpression FieldName="Group.Description" />
                    <telerik:GridSortExpression FieldName="Data.Date" />
                </SortExpressions>
                <ColumnGroups>
                    <telerik:GridColumnGroup Name="kWh" HeaderText="kWh" HeaderStyle-HorizontalAlign="Center" />
                </ColumnGroups>
                <Columns>
                    <telerik:GridBoundColumn HeaderText="AssetGroup" DataField="Group.Description" AllowSorting="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn DataField="Data.Date" HeaderText="Data" DataFormatString="{0:MM/yyyy}" AllowSorting="true">
                        <HeaderStyle HorizontalAlign="Center" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn DataField="Data.kWhF1" HeaderText="F1" ColumnGroupName="kWh">
                        <HeaderStyle BackColor="#ffc700" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Data.kWhF2" HeaderText="F2" ColumnGroupName="kWh">
                        <HeaderStyle BackColor="#2a94cb" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Data.kWhF3" HeaderText="F3" ColumnGroupName="kWh">
                        <HeaderStyle BackColor="#8dcb2a" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Data.kWh" HeaderText="Tot" ColumnGroupName="kWh">
                        <HeaderStyle BackColor="Yellow" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>

        <asp:UpdateProgress runat="server">
            <ProgressTemplate>
                <div class="loading">Loading&#8230;</div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>