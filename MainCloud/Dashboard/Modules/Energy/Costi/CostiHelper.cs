﻿using EnergyModule.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Energy.Costi
{
    public enum CostiSettingsKey
    {
        IsMonorario,
        Tariffe
    }

    public static class CostiHelper
    {
        public static object GetConfiguration(string applicationId, CostiSettingsKey key)
        {
            return GetConfiguration(applicationId, key.ToString());
        }

        public static object GetConfiguration(string applicationId, string key)
        {
            using (EnergyDbContext db = new EnergyDbContext())
            {
                try
                {
                    string settings = db.Costs.Where(x => x.ApplicationId == applicationId).Select(x => x.Config).FirstOrDefault();
                    if (!string.IsNullOrEmpty(settings))
                    {
                        Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(settings);
                        return config[key];
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static void UpdateSettings(string applicationId, CostiSettingsKey key, object obj)
        {
            UpdateSettings(applicationId, key.ToString(), obj);
        }

        public static void UpdateSettings(string applicationId, string key, object obj)
        {
            using (EnergyDbContext db = new EnergyDbContext())
            {
                var settings = db.Costs.Where(x => x.ApplicationId == applicationId).FirstOrDefault();
                if (string.IsNullOrEmpty(settings.Config))
                {
                    Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(settings.Config);
                    if (config.ContainsKey(key))
                    {
                        config[key] = obj;
                        settings.Config = JsonConvert.SerializeObject(config);
                    }
                    else
                    {
                        config.Add(key, obj);
                        settings.Config = JsonConvert.SerializeObject(config);
                    }
                }
                else
                {
                    Dictionary<string, object> config = new Dictionary<string, object>();
                    config.Add(key, obj);
                    settings.Config = JsonConvert.SerializeObject(config);
                }
                db.SaveChanges();
            }
        }
    }
}