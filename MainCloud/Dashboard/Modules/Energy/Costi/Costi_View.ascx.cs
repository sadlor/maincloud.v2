﻿using AssetManagement.Core;
using AssetManagement.Models;
using AssetManagement.Repositories;
using AssetManagement.Services;
using EnergyModule.ConsumptionStorage;
using EnergyModule.Core;
using EnergyModule.Models;
using EnergyModule.Services;
using MainCloudFramework.UI.Containers;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web.Multitenants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Energy.Costi
{
    public partial class Costi_View : WidgetControl<object>
    {
        public Costi_View() : base(typeof(Costi_View)) { }

        const string SESSION_TABLECOSTDATASOURCE = "SessionDataSourceWidget_";

        public override bool EnableExport
        {
            get
            {
                return true;
            }
        }

        public override void OnExport()
        {
            //base.OnExport();

            //TableCosti.MasterTableView.GetColumn("kWhF1").Display = true;
            //TableCosti.MasterTableView.GetColumn("kWhF2").Display = true;
            //TableCosti.MasterTableView.GetColumn("kWhF3").Display = true;
            
            if (SessionMultitenant.ContainsKey(SESSION_TABLECOSTDATASOURCE + this.ID))
            {
                TableCosti.DataSource = SessionMultitenant[SESSION_TABLECOSTDATASOURCE + this.ID];
                TableCosti.DataBind();
            }
                
            TableCosti.ExportSettings.Excel.Format = GridExcelExportFormat.Xlsx;
            TableCosti.ExportSettings.IgnorePaging = true;
            TableCosti.ExportSettings.ExportOnlyData = true;
            TableCosti.ExportSettings.OpenInNewWindow = true;
            TableCosti.MasterTableView.ExportToExcel();
        }

        SelectedContext SelectedContextId
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<SelectedContext>((string)CustomSettings.Find("SelectedContextId"));
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        string Behaviour
        {
            get
            {
                return (string)CustomSettings.Find("Behaviour") ?? "Chart";
            }
        }

        EnergyEnum.TimeScale TypeOfTime
        {
            get
            {
                try
                {
                    return (EnergyEnum.TimeScale)Convert.ToInt16(this.CustomSettings.Find("TypeOfTime"));
                }
                catch (Exception ex)
                {
                    return EnergyEnum.TimeScale.Day;
                }
            }
        }

        DateTime? DateStart
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateStart");
            }
        }

        DateTime? DateEnd
        {
            get
            {
                return (DateTime?)CustomSettings.Find("DateEnd");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ChartCosti.PlotArea.XAxis.Items.Clear();
            ChartCosti.PlotArea.Series.Clear();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (SelectedContextId != null)//(!string.IsNullOrEmpty((string)CustomSettings.Find("SelectedContextId")))
            {
                SelectedContextService service = new SelectedContextService();
                lblSelectedContext.Text = service.GetSelectedDescription(SelectedContextId);
                hiddenSelection.Value = JsonConvert.SerializeObject(SelectedContextId);

                try
                {
                    rbtnBehaviour.Items.FindByValue(Behaviour).Selected = true;
                }
                catch (Exception ex)
                {}

                //setto le date con il valore in memoria
                dtPickStart.SelectedDate = DateStart;
                dtPickEnd.SelectedDate = DateEnd;

                //if (DateStart != null && DateEnd != null)
                //{
                //    if (Behaviour == "Chart")
                //    {
                //        ChartCosti.Visible = true;
                //        TableCosti.Visible = false;
                //        ChartHistoryCosts(DateStart.Value, DateEnd.Value);
                //        TableHistoryCosts(DateStart.Value, DateEnd.Value);
                //    }
                //    else
                //    {
                //        ChartCosti.Visible = false;
                //        TableCosti.Visible = true;
                //        TableHistoryCosts(DateStart.Value, DateEnd.Value);
                //    }
                //}
            }
        }

        public void CreateChartDataSource(DateTime start, DateTime end)
        {
            EnergyDataService energyDataService = new EnergyDataService();
            string analyzerId = energyDataService.GetAnalyzerId(SelectedContextId);
            end = end.AddDays(1);
            EnergyConsumptionService ECS = new EnergyConsumptionService();
            List<EnergyConsumption> dataSource = ECS.GetEnergyDataList(energyDataService.GetAnalyzerId(SelectedContextId), start, end, TypeOfTime);

            List<Cost> tariffe = new List<Cost>();
            CostService CS = new CostService();
            tariffe = CS.GetRatesList(MultiTenantsHelper.ApplicationId, start, end);
            GridCosti.DataSource = tariffe;
            GridCosti.DataBind();

            //asse x
            foreach (var item in dataSource)
            {
                switch (TypeOfTime)
                {
                    case EnergyEnum.TimeScale.Hour:
                    //GenericChart.PlotArea.XAxis.Items.Add((item.Date.Hour + 1).ToString());
                    //break;
                    case EnergyEnum.TimeScale.Day:
                        //GenericChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 35;
                        ChartCosti.PlotArea.XAxis.Items.Add(item.Date.ToString("dd"));
                        break;
                    case EnergyEnum.TimeScale.Month:
                        ChartCosti.PlotArea.XAxis.Items.Add(item.Date.ToString("MM/yyyy"));
                        break;
                    case EnergyEnum.TimeScale.Year:
                        break;
                }
            }

            //asse y
            ColumnSeries F1Serie = new ColumnSeries();
            F1Serie.Name = "F1";
            F1Serie.GroupName = "Fasce";
            F1Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#ffc700");
            F1Serie.LabelsAppearance.Visible = false;
            IEnumerable<decimal> F1Items = dataSource.Select(x => x.CostF1);
            foreach (var item in F1Items)
            {
                F1Serie.SeriesItems.Add(new CategorySeriesItem(item));
            }

            ColumnSeries F2Serie = new ColumnSeries();
            F2Serie.Name = "F2";
            F2Serie.GroupName = "Fasce";
            F2Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#2a94cb");
            F2Serie.LabelsAppearance.Visible = false;
            IEnumerable<decimal> F2Items = dataSource.Select(x => x.CostF2);
            foreach (var item in F2Items)
            {
                F2Serie.SeriesItems.Add(new CategorySeriesItem(item));
            }

            ColumnSeries F3Serie = new ColumnSeries();
            F3Serie.Name = "F3";
            F3Serie.GroupName = "Fasce";
            F3Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#8dcb2a");
            F3Serie.LabelsAppearance.Visible = false;
            IEnumerable<decimal> F3Items = dataSource.Select(x => x.CostF3);
            foreach (var item in F3Items)
            {
                F3Serie.SeriesItems.Add(new CategorySeriesItem(item));
            }

            ChartCosti.PlotArea.Series.Add(F1Serie);
            ChartCosti.PlotArea.Series.Add(F2Serie);
            ChartCosti.PlotArea.Series.Add(F3Serie);
            ChartCosti.PlotArea.YAxis.TitleAppearance.Text += "€";
        }

        public void ChartHistoryCosts(DateTime start, DateTime end)
        {
            //if (Application.ApplicationMultitenant().ContainsKey(EMConstants.SELECTED_ID_CONTEXT) && Application.ApplicationMultitenant()[EMConstants.SELECTED_ID_CONTEXT] != null)
            //{
                //SelectedContext selectedContext = JsonConvert.DeserializeObject<SelectedContext>((string)Application.ApplicationMultitenant()[EMConstants.SELECTED_ID_CONTEXT]);
                EnergyDataService energyDataService = new EnergyDataService();
                string analyzerId = energyDataService.GetAnalyzerId(SelectedContextId);
                end = end.AddDays(1);
                List<AnalyzerEnergyData> list = energyDataService.GetAnalyzerEnergyDataList(x => x.AnalyzerId == analyzerId && x.TimeStamp >= start && x.TimeStamp < end);

                string paramName = "TotalActiveEnergyConsumed";

                ColumnSeries F1Serie = new ColumnSeries();
                F1Serie.Name = "F1";
                F1Serie.GroupName = "Fasce";
                F1Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#ffc700");
                F1Serie.LabelsAppearance.Visible = false;

                ColumnSeries F2Serie = new ColumnSeries();
                F2Serie.Name = "F2";
                F2Serie.GroupName = "Fasce";
                F2Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#2a94cb");
                F2Serie.LabelsAppearance.Visible = false;

                ColumnSeries F3Serie = new ColumnSeries();
                F3Serie.Name = "F3";
                F3Serie.GroupName = "Fasce";
                F3Serie.Appearance.FillStyle.BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#8dcb2a");
                F3Serie.LabelsAppearance.Visible = false;

                List<Cost> tariffe = new List<Cost>();
                using (EnergyDbContext db = new EnergyDbContext())
                {
                    if (db.Costs.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).Count() > 0)
                    {
                        DateTime maxDate;
                        try
                        {
                            maxDate = db.Costs.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && x.Date <= start).Max(x => x.Date);
                        }
                        catch (InvalidOperationException ex)
                        {
                            maxDate = db.Costs.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && x.Date >= start).Min(x => x.Date);
                        }
                        tariffe = db.Costs.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && x.Date >= maxDate && x.Date < end).OrderBy(x => x.Date).ToList();

                        GridCosti.DataSource = tariffe;
                        GridCosti.DataBind();
                    }
                    else
                    {
                        //non ci sono tariffe
                        AlertMessage.ShowError("Non ci sono tariffe applicabili al periodo selezionato, inserire prima delle tariffe");
                    }
                }

                SortedDictionary<DateTime, decimal> F1SerieDaily = new SortedDictionary<DateTime, decimal>();
                SortedDictionary<DateTime, decimal> F2SerieDaily = new SortedDictionary<DateTime, decimal>();
                SortedDictionary<DateTime, decimal> F3SerieDaily = new SortedDictionary<DateTime, decimal>();

                if (tariffe.Count > 0)
                {
                    for (DateTime day = start; day < end; day = day.AddDays(1))
                    {
                        List<decimal> F1Items = new List<decimal>();
                        List<decimal> F2Items = new List<decimal>();
                        List<decimal> F3Items = new List<decimal>();

                        List<AnalyzerEnergyData> dailyList = list.Where(x => x.TimeStamp.Date == day.Date).ToList();
                        if (dailyList.Count > 0)
                        {
                            SortedDictionary<DateTime, EnergyRecord> erList = new SortedDictionary<DateTime, EnergyRecord>();
                            foreach (var item in dailyList)
                            {
                                try
                                {
                                    EnergyRecord e = JsonConvert.DeserializeObject<EnergyRecord>(item.Data);
                                    erList[item.TimeStamp] = e;
                                }
                                catch (Exception ex)
                                { }
                            }

                            EnergyRecord first = erList.FirstOrDefault(x => x.Value.Count > 0 && x.Value.Exist(paramName) && x.Value.GetData(paramName) > 0).Value;

                            if (first != null)
                            {
                                int i = 0;
                                while (i < erList.Count)
                                {
                                    Fascia f = GetFascia(erList.ElementAt(i).Key);
                                    List<EnergyRecord> subList = erList.Values.ToList().GetRange(i, erList.Count - i);
                                    EnergyRecord firstkWh = subList.FirstOrDefault(x => x.Count > 0 && x.Exist(paramName) && x.GetData(paramName) > 0);
                                    KeyValuePair<DateTime, EnergyRecord> item = new KeyValuePair<DateTime,EnergyRecord>();
                                    while (i < erList.Count && f == GetFascia(erList.ElementAt(i).Key))
                                    {
                                        i++;
                                        if (erList.ElementAt(i - 1).Value.Count > 0 && erList.ElementAt(i - 1).Value.Exist(paramName) && erList.ElementAt(i - 1).Value.GetData(paramName) > 0)
                                        {
                                            item = erList.ElementAt(i - 1);
                                        }
                                    }
                                    //KeyValuePair<DateTime, EnergyRecord> item = erList.ElementAt(i - 1);
                                    switch (f)
                                    {
                                        case Fascia.F1:
                                            try
                                            {
                                                F1Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                            }
                                            catch (Exception ex)
                                            { }
                                            break;
                                        case Fascia.F2:
                                            try
                                            {
                                                F2Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                            }
                                            catch (Exception ex)
                                            { }
                                            break;
                                        case Fascia.F3:
                                            try
                                            {
                                                F3Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                            }
                                            catch (Exception ex)
                                            { }
                                            break;
                                    }
                                }
                            }

                            //EnergyRecord first = erList.First(x => x.Value.Count > 0 && x.Value.Exist(paramName) && x.Value.GetData(paramName) > 0).Value;
                            //foreach (var item in erList)
                            //{
                            //    switch (GetFascia(item.Key))
                            //    {
                            //        case Fascia.F1:
                            //            try
                            //            {
                            //                F1Items.Add(item.Value.GetData<decimal>(paramName) - first.GetData<decimal>(paramName));
                            //            }
                            //            catch (Exception ex)
                            //            { }
                            //            break;
                            //        case Fascia.F2:
                            //            try
                            //            {
                            //                F2Items.Add(item.Value.GetData<decimal>(paramName) - first.GetData<decimal>(paramName));
                            //            }
                            //            catch (Exception ex)
                            //            { }
                            //            break;
                            //        case Fascia.F3:
                            //            try
                            //            {
                            //                F3Items.Add(item.Value.GetData<decimal>(paramName) - first.GetData<decimal>(paramName));
                            //            }
                            //            catch (Exception ex)
                            //            { }
                            //            break;
                            //    }
                            //}

                            if (F1Items.Count > 0)
                            {
                                F1SerieDaily[day] = Math.Round(F1Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F1, 0);
                                //F1Serie.SeriesItems.Add(new CategorySeriesItem(Math.Round(F1Items.Sum() * tariffe.First(x => x.Date.Date <= day.Date).F1, 0)));
                            }
                            else
                            {
                                F1SerieDaily[day] = 0;
                                //F1Serie.SeriesItems.Add(new CategorySeriesItem(0));
                            }
                            if (F2Items.Count > 0)
                            {
                                F2SerieDaily[day] = Math.Round(F2Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F2, 0);
                                //F2Serie.SeriesItems.Add(new CategorySeriesItem(Math.Round(F2Items.Sum() * tariffe.First(x => x.Date.Date <= day.Date).F2, 0)));
                            }
                            else
                            {
                                F2SerieDaily[day] = 0;
                                //F2Serie.SeriesItems.Add(new CategorySeriesItem(0));
                            }
                            if (F3Items.Count > 0)
                            {
                                F3SerieDaily[day] = Math.Round(F3Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F3, 0);
                                //F3Serie.SeriesItems.Add(new CategorySeriesItem(Math.Round(F3Items.Sum() * tariffe.First(x => x.Date.Date <= day.Date).F3, 0)));
                            }
                            else
                            {
                                F3SerieDaily[day] = 0;
                                //F3Serie.SeriesItems.Add(new CategorySeriesItem(0));
                            }

                            if (TypeOfTime == EnergyEnum.TimeScale.Day)
                            {
                                ChartCosti.PlotArea.XAxis.Items.Add(day.ToString("dd"));
                            }
                        }
                    }
                }

                foreach (var item in F1SerieDaily)
                { 
                    F1Serie.SeriesItems.Add(new CategorySeriesItem(item.Value));
                }
                foreach (var item in F2SerieDaily)
                {
                    F2Serie.SeriesItems.Add(new CategorySeriesItem(item.Value));
                }
                foreach (var item in F3SerieDaily)
                {
                    F3Serie.SeriesItems.Add(new CategorySeriesItem(item.Value));
                }

                if (TypeOfTime == EnergyEnum.TimeScale.Month || TypeOfTime == EnergyEnum.TimeScale.Year) //TODO: togliere year e fare caso a parte
                {
                    F1Serie.SeriesItems.Clear();
                    F2Serie.SeriesItems.Clear();
                    F3Serie.SeriesItems.Clear();
                    DateTime date = start;
                    while (date < end)
                    {
                        F1Serie.SeriesItems.Add(new CategorySeriesItem(F1SerieDaily.Where(x => x.Key.Month == date.Month && x.Key.Year == date.Year).Sum(x => x.Value)));
                        F2Serie.SeriesItems.Add(new CategorySeriesItem(F2SerieDaily.Where(x => x.Key.Month == date.Month && x.Key.Year == date.Year).Sum(x => x.Value)));
                        F3Serie.SeriesItems.Add(new CategorySeriesItem(F3SerieDaily.Where(x => x.Key.Month == date.Month && x.Key.Year == date.Year).Sum(x => x.Value)));
                        ChartCosti.PlotArea.XAxis.Items.Add(date.ToString("MM/yyyy"));
                        date = date.AddDays(DateTime.DaysInMonth(date.Year, date.Month) - date.Day + 1);
                    }
                }

                ChartCosti.PlotArea.Series.Add(F1Serie);
                ChartCosti.PlotArea.Series.Add(F2Serie);
                ChartCosti.PlotArea.Series.Add(F3Serie);
                //if (Behaviour == "KWh")
                //{
                //    ChartCosti.ChartTitle.Text = "kWh";
                //    ChartCosti.PlotArea.YAxis.TitleAppearance.Text = "KWh";
                //}
                //else
                //{
                    ChartCosti.ChartTitle.Text = "Costi";
                    ChartCosti.PlotArea.YAxis.TitleAppearance.Text = "€";
                //}
            //}
        }

        public void CreateTableDataSource(DateTime start, DateTime end)
        {
            EnergyDataService energyDataService = new EnergyDataService();
            string analyzerId = energyDataService.GetAnalyzerId(SelectedContextId);
            end = end.AddDays(1);
            EnergyConsumptionService ECS = new EnergyConsumptionService();
            List<EnergyConsumption> dataSource = ECS.GetEnergyDataList(energyDataService.GetAnalyzerId(SelectedContextId), start, end, TypeOfTime);

            List<Cost> tariffe = new List<Cost>();
            CostService CS = new CostService();
            tariffe = CS.GetRatesList(MultiTenantsHelper.ApplicationId, start, end);
            GridCosti.DataSource = tariffe;
            GridCosti.DataBind();

            TableCosti.HeaderContextMenu.FindItemByValue("ColumnsContainer").Text = "Mostra/nascondi colonne";
            TableCosti.MasterTableView.IsFilterItemExpanded = false;
            TableCosti.MasterTableView.AllowFilteringByColumn = true;
            TableCosti.DataSource = dataSource;
            SessionMultitenant[SESSION_TABLECOSTDATASOURCE + this.ID] = dataSource;
            if (TypeOfTime == EnergyEnum.TimeScale.Month)
            {
                (TableCosti.Columns.FindByDataField("Date") as GridDateTimeColumn).DataFormatString = "{0:MM/yyyy}";
            }
            else
            {
                (TableCosti.Columns.FindByDataField("Date") as GridDateTimeColumn).DataFormatString = "{0:dd/MM/yyyy}";
            }
            TableCosti.DataBind();
        }

        public void TableHistoryCosts(DateTime start, DateTime end)
        {
            //if (Application.ApplicationMultitenant().ContainsKey(EMConstants.SELECTED_ID_CONTEXT) && Application.ApplicationMultitenant()[EMConstants.SELECTED_ID_CONTEXT] != null)
            //{
                //SelectedContext selectedContext = JsonConvert.DeserializeObject<SelectedContext>((string)Application.ApplicationMultitenant()[EMConstants.SELECTED_ID_CONTEXT]);
                EnergyDataService energyDataService = new EnergyDataService();
                string analyzerId = energyDataService.GetAnalyzerId(SelectedContextId);
                end = end.AddDays(1);
                List<AnalyzerEnergyData> list = energyDataService.GetAnalyzerEnergyDataList(x => x.AnalyzerId == analyzerId && x.TimeStamp >= start && x.TimeStamp < end);

                string paramName = "TotalActiveEnergyConsumed";

                List<Cost> tariffe = new List<Cost>();
                using (EnergyDbContext db = new EnergyDbContext())
                {
                    if (db.Costs.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).Count() > 0)
                    {
                        DateTime maxDate;
                        try
                        {
                            maxDate = db.Costs.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && x.Date <= start).Max(x => x.Date);
                        }
                        catch (InvalidOperationException ex)
                        {
                            maxDate = db.Costs.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && x.Date >= start).Min(x => x.Date);
                        }
                        tariffe = db.Costs.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && x.Date >= maxDate && x.Date < end).OrderBy(x => x.Date).ToList();

                        GridCosti.DataSource = tariffe;
                        GridCosti.DataBind();
                    }
                    else
                    {
                        //non ci sono tariffe
                        AlertMessage.ShowError("Non ci sono tariffe applicabili al periodo selezionato, inserire prima delle tariffe");
                    }
                }

                if (tariffe.Count > 0)
                {
                    List<TableDataSource> dataSource = new List<TableDataSource>();

                    for (DateTime day = start; day < end; day = day.AddDays(1))
                    {
                        List<decimal> F1Items = new List<decimal>();
                        List<decimal> F2Items = new List<decimal>();
                        List<decimal> F3Items = new List<decimal>();

                        List<AnalyzerEnergyData> dailyList = list.Where(x => x.TimeStamp.Date == day.Date).ToList();
                        if (dailyList.Count > 0)
                        {
                            SortedDictionary<DateTime, EnergyRecord> erList = new SortedDictionary<DateTime, EnergyRecord>();
                            foreach (var item in dailyList)
                            {
                                try
                                {
                                    EnergyRecord e = JsonConvert.DeserializeObject<EnergyRecord>(item.Data);
                                    erList[item.TimeStamp] = e;
                                }
                                catch (Exception ex)
                                { }
                            }

                            TableDataSource dataSourceItem = new TableDataSource();

                            EnergyRecord first = erList.FirstOrDefault(x => x.Value.Count > 0 && x.Value.Exist(paramName) && x.Value.GetData(paramName) > 0).Value;

                            if (first != null)
                            {
                                int i = 0;
                                while (i < erList.Count)
                                {
                                    Fascia f = GetFascia(erList.ElementAt(i).Key);
                                    List<EnergyRecord> subList = erList.Values.ToList().GetRange(i, erList.Count - i);
                                    EnergyRecord firstkWh = subList.FirstOrDefault(x => x.Count > 0 && x.Exist(paramName) && x.GetData(paramName) > 0);
                                    KeyValuePair<DateTime, EnergyRecord> item = new KeyValuePair<DateTime, EnergyRecord>();
                                    while (i < erList.Count && f == GetFascia(erList.ElementAt(i).Key))
                                    {
                                        i++;
                                        if (erList.ElementAt(i - 1).Value.Count > 0 && erList.ElementAt(i - 1).Value.Exist(paramName) && erList.ElementAt(i - 1).Value.GetData(paramName) > 0)
                                        {
                                            item = erList.ElementAt(i - 1);
                                        }
                                    }
                                    //KeyValuePair<DateTime, EnergyRecord> item = erList.ElementAt(i - 1);
                                    switch (f)
                                    {
                                        case Fascia.F1:
                                            try
                                            {
                                                F1Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                            }
                                            catch (Exception ex)
                                            { }
                                            break;
                                        case Fascia.F2:
                                            try
                                            {
                                                F2Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                            }
                                            catch (Exception ex)
                                            { }
                                            break;
                                        case Fascia.F3:
                                            try
                                            {
                                                F3Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                            }
                                            catch (Exception ex)
                                            { }
                                            break;
                                    }
                                }
                                dataSourceItem.kWh = Math.Round(erList.Where(x => x.Value.Count > 0 && x.Value.Exist(paramName)).Max(x => x.Value.GetData(paramName)) - first.GetData(paramName), 2);
                            }
                            else
                            {
                                dataSourceItem.kWh = 0;
                            }

                            if (F1Items.Count > 0)
                            {
                                dataSourceItem.kWhF1 = Math.Round(F1Items.Sum(), 2);
                                dataSourceItem.F1 = Math.Round(F1Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F1, 2);
                            }
                            else
                            {
                                dataSourceItem.kWhF1 = 0;
                                dataSourceItem.F1 = 0;
                            }
                            if (F2Items.Count > 0)
                            {
                                dataSourceItem.kWhF2 = Math.Round(F2Items.Sum(), 2);
                                dataSourceItem.F2 = Math.Round(F2Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F2, 2);
                            }
                            else
                            {
                                dataSourceItem.kWhF2 = 0;
                                dataSourceItem.F2 = 0;
                            }
                            if (F3Items.Count > 0)
                            {
                                dataSourceItem.kWhF3 = Math.Round(F3Items.Sum(), 2);
                                dataSourceItem.F3 = Math.Round(F3Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F3, 2);
                            }
                            else
                            {
                                dataSourceItem.kWhF3 = 0;
                                dataSourceItem.F3 = 0;
                            }

                            dataSourceItem.Date = day;
                            dataSource.Add(dataSourceItem);
                        }
                    }

                    if (TypeOfTime == EnergyEnum.TimeScale.Month || TypeOfTime == EnergyEnum.TimeScale.Year) //TODO: togliere year e fare un caso a parte
                    {
                        List<TableDataSource> dataSourceMonthly = new List<TableDataSource>();
                        DateTime date = start;
                        while (date < end)
                        {
                            TableDataSource dataSourceItem = new TableDataSource();
                            dataSourceItem.Date = date;
                            dataSourceItem.F1 = dataSource.Where(x => x.Date.Month == date.Month && x.Date.Year == date.Year).Sum(x => x.F1);
                            dataSourceItem.F2 = dataSource.Where(x => x.Date.Month == date.Month && x.Date.Year == date.Year).Sum(x => x.F2);
                            dataSourceItem.F3 = dataSource.Where(x => x.Date.Month == date.Month && x.Date.Year == date.Year).Sum(x => x.F3);
                            dataSourceItem.kWh = dataSource.Where(x => x.Date.Month == date.Month && x.Date.Year == date.Year).Sum(x => x.kWh);
                            dataSourceItem.kWhF1 = dataSource.Where(x => x.Date.Month == date.Month && x.Date.Year == date.Year).Sum(x => x.kWhF1);
                            dataSourceItem.kWhF2 = dataSource.Where(x => x.Date.Month == date.Month && x.Date.Year == date.Year).Sum(x => x.kWhF2);
                            dataSourceItem.kWhF3 = dataSource.Where(x => x.Date.Month == date.Month && x.Date.Year == date.Year).Sum(x => x.kWhF3);
                            date = date.AddDays(DateTime.DaysInMonth(date.Year, date.Month) - date.Day + 1);
                            dataSourceMonthly.Add(dataSourceItem);
                        }
                        dataSource = dataSourceMonthly;
                    }

                    //TableCosti.MasterTableView.Columns.Clear();
                    //TableCosti.EnableHeaderContextMenu = true;
                    TableCosti.HeaderContextMenu.FindItemByValue("ColumnsContainer").Text = "Mostra/nascondi colonne";
                    TableCosti.MasterTableView.IsFilterItemExpanded = false;
                    TableCosti.MasterTableView.AllowFilteringByColumn = true;
                    TableCosti.DataSource = dataSource;
                    SessionMultitenant[SESSION_TABLECOSTDATASOURCE + this.ID] = dataSource;

                    //GridDateTimeColumn dateColumn = new GridDateTimeColumn();
                    //dateColumn.DataField = "Date";
                    //dateColumn.HeaderText = "Date";
                    //dateColumn.AllowSorting = true;
                    //dateColumn.Groupable = false;
                    //dateColumn.Exportable = true;
                    if (TypeOfTime == EnergyEnum.TimeScale.Month)
                    {
                        (TableCosti.Columns.FindByDataField("Date") as GridDateTimeColumn).DataFormatString = "{0:MM/yyyy}";
                    }
                    else
                    {
                        (TableCosti.Columns.FindByDataField("Date") as GridDateTimeColumn).DataFormatString = "{0:dd/MM/yyyy}";
                    }
                    //dateColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //TableCosti.MasterTableView.Columns.Add(dateColumn);

                    //GridBoundColumn column = new GridBoundColumn();
                    //column.DataField = "kWh";
                    //column.HeaderText = "kWh totali";
                    //column.HeaderStyle.BackColor = Color.Yellow;
                    //column.AllowSorting = true;
                    //column.Groupable = false;
                    //column.SortExpression = "kWh";
                    //column.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //column.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    ////column.Exportable = true;
                    //TableCosti.MasterTableView.Columns.Add(column);

                    //column = new GridBoundColumn();
                    //column.DataField = "kWhF1";
                    //column.HeaderText = "kWh F1";
                    //column.UniqueName = "kWhF1";
                    //column.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffc700");
                    //column.AllowSorting = true;
                    //column.Groupable = false;
                    //column.SortExpression = "kWh";
                    ////column.Display = false;
                    ////column.Display = true;
                    //column.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //column.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    ////column.Exportable = true;
                    //TableCosti.MasterTableView.Columns.Add(column);

                    //column = new GridBoundColumn();
                    //column.DataField = "kWhF2";
                    //column.HeaderText = "kWh F2";
                    //column.UniqueName = "kWhF2";
                    //column.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#2a94cb");
                    //column.AllowSorting = true;
                    //column.Groupable = false;
                    //column.SortExpression = "kWh";
                    ////column.Display = false;
                    ////column.Display = true;
                    //column.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //column.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    ////column.Exportable = true;
                    //TableCosti.MasterTableView.Columns.Add(column);

                    //column = new GridBoundColumn();
                    //column.DataField = "kWhF3";
                    //column.HeaderText = "kWh F3";
                    //column.UniqueName = "kWhF3";
                    //column.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#8dcb2a");
                    //column.AllowSorting = true;
                    //column.Groupable = false;
                    //column.SortExpression = "kWh";
                    ////column.Display = false;
                    ////column.Display = true;
                    //column.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //column.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    ////column.Exportable = true;
                    //TableCosti.MasterTableView.Columns.Add(column);

                    //GridCalculatedColumn sumColumn = new GridCalculatedColumn();
                    //sumColumn.DataFields = new string[] { "F1", "F2", "F3" };
                    //sumColumn.Expression = "{0} + {1} + {2}";
                    //sumColumn.HeaderText = "€ totali";
                    //sumColumn.HeaderStyle.BackColor = Color.LimeGreen;
                    //sumColumn.AllowSorting = true;
                    //sumColumn.Groupable = false;
                    //sumColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //sumColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    ////column.Exportable = true;
                    //TableCosti.MasterTableView.Columns.Add(sumColumn);

                    //column = new GridBoundColumn();
                    //column.DataField = "F1";
                    //column.HeaderText = "F1 (€)";
                    //column.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffc700");
                    //column.AllowSorting = true;
                    //column.Groupable = false;
                    //column.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //column.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    ////column.Exportable = true;
                    //TableCosti.MasterTableView.Columns.Add(column);

                    //column = new GridBoundColumn();
                    //column.DataField = "F2";
                    //column.HeaderText = "F2 (€)";
                    //column.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#2a94cb");
                    //column.AllowSorting = true;
                    //column.Groupable = false;
                    //column.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //column.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    ////column.Exportable = true;
                    //TableCosti.MasterTableView.Columns.Add(column);

                    //column = new GridBoundColumn();
                    //column.DataField = "F3";
                    //column.HeaderText = "F3 (€)";
                    //column.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#8dcb2a");
                    //column.AllowSorting = true;
                    //column.Groupable = false;
                    //column.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //column.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    ////column.Exportable = true;
                    //TableCosti.MasterTableView.Columns.Add(column);

                    TableCosti.DataBind();
                }
            //}            
        }

        struct TableDataSource
        {
            public DateTime Date { get; set; }
            public decimal kWh { get; set; }
            public decimal kWhF1 { get; set; }
            public decimal kWhF2 { get; set; }
            public decimal kWhF3 { get; set; }
            public decimal F1 { get; set; }
            public decimal F2 { get; set; }
            public decimal F3 { get; set; }
        }

        private enum Fascia
        {
            F1,
            F2,
            F3
        }

        private Fascia GetFascia(DateTime time)
        {
            if (time.DayOfWeek == DayOfWeek.Sunday)
            {
                return Fascia.F3;
            }
            else
            {
                if (time.DayOfWeek == DayOfWeek.Saturday)
                {
                    if ((time.Hour >= 7 && time.Hour < 23) || (time.Hour == 23 && time.Minute == 00))
                    {
                        return Fascia.F2;
                    }
                    else
                    {
                        return Fascia.F3;
                    }
                }
                else
                {
                    if ((time.Hour >= 8 && time.Hour < 19) || (time.Hour == 19 && time.Minute == 00))
                    {
                        return Fascia.F1;
                    }
                    else
                    {
                        if ((time.Hour >= 7 && time.Hour < 8) || (time.Hour == 8 && time.Minute == 00) || (time.Hour >= 19 && time.Hour < 23) || (time.Hour == 23 && time.Minute == 00))
                        {
                            return Fascia.F2;
                        }
                        else
                        {
                            return Fascia.F3;
                        }
                    }
                }
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            LoadAssetTreeView();
            wndSelectAsset.OpenDialog();
        }

        protected void LoadAssetTreeView()
        {
            AssetTreeView.Nodes.Clear();

            PlantService service = new PlantService();
            var plants = service.PlantList();
            foreach (Plant p in plants.OrderBy(x => x.Description))
            {
                //Node Plant
                RadTreeNode nodeP = new RadTreeNode(p.Description);
                if (string.IsNullOrEmpty(p.AnalyzerId))
                {
                    nodeP.ForeColor = Color.Red;
                    nodeP.Value = "";
                }
                else
                {
                    nodeP.Value = JsonConvert.SerializeObject(new SelectedContext(p.Id, AssetManagementEnum.AnalyzerContext.Plant));
                }
                nodeP.Expanded = true;
                foreach (Department d in p.Department.OrderBy(x => x.Description))
                {
                    //Node Department
                    RadTreeNode nodeD = new RadTreeNode(d.Description);
                    if (string.IsNullOrEmpty(d.AnalyzerId))
                    {
                        nodeD.ForeColor = Color.Red;
                        nodeD.Value = "";
                    }
                    else
                    {
                        nodeD.Value = JsonConvert.SerializeObject(new SelectedContext(d.Id, AssetManagementEnum.AnalyzerContext.Department));
                    }
                    nodeD.Expanded = true;
                    foreach (Asset a in d.Asset.OrderBy(x => x.Description))
                    {
                        //Node Asset
                        RadTreeNode nodeA = new RadTreeNode(a.Description);
                        if (string.IsNullOrEmpty(a.AnalyzerId))
                        {
                            nodeA.ForeColor = Color.Red;
                            nodeA.Value = "";
                        }
                        else
                        {
                            nodeA.Value = JsonConvert.SerializeObject(new SelectedContext(a.Id, AssetManagementEnum.AnalyzerContext.Asset));
                        }
                        nodeA.Expanded = true;

                        nodeD.Nodes.Add(nodeA);
                    }
                    nodeP.Nodes.Add(nodeD);
                }
                AssetTreeView.Nodes.Add(nodeP);
            }
        }

        protected void btnConfirmSelect_Click(object sender, EventArgs e)
        {
            CustomSettings.AddOrUpdate("SelectedContextId", hiddenSelection.Value);
            wndSelectAsset.CloseDialog();
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (dtPickStart.SelectedDate != null && dtPickEnd.SelectedDate != null)
            {
                CustomSettings.AddOrUpdate("DateStart", dtPickStart.SelectedDate.Value);
                CustomSettings.AddOrUpdate("DateEnd", dtPickEnd.SelectedDate.Value);
                if ((dtPickEnd.SelectedDate.Value - dtPickStart.SelectedDate.Value).Days > 62)
                {
                    if ((dtPickEnd.SelectedDate.Value - dtPickStart.SelectedDate.Value).Days > 730)
                    {
                        CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Year);
                    }
                    else
                    {
                        CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Month);
                    }
                }
                else
                {
                    if (dtPickEnd.SelectedDate.Value == dtPickStart.SelectedDate.Value)
                    {
                        CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Hour);
                    }
                    else
                    {
                        CustomSettings.AddOrUpdate("TypeOfTime", EnergyEnum.TimeScale.Day);
                    }
                }
                CustomSettings.AddOrUpdate("Behaviour", rbtnBehaviour.SelectedValue);
                if (string.IsNullOrEmpty(hiddenSelection.Value))
                {
                    messaggio.Text = "Selezionare prima un asset";
                    dlgWidgetConfig.OpenDialog();
                }
                else
                {
                    if (Behaviour == "Chart")
                    {
                        ChartCosti.Visible = true;
                        TableCosti.Visible = false;
                        CreateChartDataSource(DateStart.Value, DateEnd.Value);//ChartHistoryCosts(DateStart.Value, DateEnd.Value);
                        CreateTableDataSource(DateStart.Value, DateEnd.Value);//TableHistoryCosts(DateStart.Value, DateEnd.Value);
                    }
                    else
                    {
                        ChartCosti.Visible = false;
                        TableCosti.Visible = true;
                        CreateTableDataSource(DateStart.Value, DateEnd.Value);//TableHistoryCosts(DateStart.Value, DateEnd.Value);
                    }
                    updatePnlData.Update();
                }
            }
            else
            {
                messaggio.Text = "Inserire prima data inizio e fine";
                dlgWidgetConfig.OpenDialog();
            }
        }

        protected void GridCosti_ExcelExportCellFormatting(object sender, ExcelExportCellFormattingEventArgs e)
        {
            if ((e.FormattedColumn.UniqueName) == "DateColumn")
            {
                e.Cell.Style["mso-number-format"] = @"dd\/MM\/yy";
            }
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            AnalyzerRepository repos = new AnalyzerRepository();
            List<Analyzer> list = repos.FindAll().ToList();

            ConsumptionStorage storage = new ConsumptionStorage();

            //storage.DailyStorage("e9142a6a-c978-412d-86e2-39b546346490", new DateTime(2016, 09, 15), new DateTime(2016, 09, 16));

            //using (EnergyDbContext db = new EnergyDbContext())
            //{
            //    CauseList = db.Analyzers.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
            //}
            //foreach (Analyzer a in CauseList)
            //{
            //    storage.DailyStorage(a.Id, new DateTime(2016, 11, 08), new DateTime(2016, 11, 08));
            //}

            //foreach (Analyzer a in list)
            //{
            //    for (int i = 9; i < 11; i++)
            //    {
            //        storage.MonthlyStorage(a.Id, new DateTime(2016, i, 01), new DateTime(2016, i, DateTime.DaysInMonth(2016, i)));
            //    }
            //}

            //foreach (Analyzer a in list)
            //{
            //    DateTime start = new DateTime(2016, 11, 01);
            //    DateTime end = new DateTime(2016, 11, 30);
            //    storage.MonthlyStorage(a.Id, start, end);
            //}
        }
    }
}