﻿using EnergyModule.Models;
using MainCloudFramework.UI.Modules;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Energy.Costi
{
    public partial class Costi_Edit : WidgetControl<object>
    {
        public Costi_Edit() : base(typeof(Costi_Edit)) { }

        public string ApplicationId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationId = (Page as PageEdit).GetApplicationId();

            edsCosti.WhereParameters.Clear();
            edsCosti.WhereParameters.Add("ApplicationId", ApplicationId);
        }

        public void Get()
        {
            string applicationId = (Page as PageEdit).GetApplicationId();
            

            Dictionary<string, decimal> config = (Dictionary<string, decimal>)CostiHelper.GetConfiguration(applicationId, CostiSettingsKey.Tariffe);
            EnergyCost dataSource = new EnergyCost(config["F1"], config["F2"], config["F3"]);
            GridCosti.DataSource = dataSource;
        }

        protected void GridCosti_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            using (EnergyDbContext db = new EnergyDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                Hashtable values = new Hashtable();
                editableItem.ExtractValues(values);

                var cost = new Cost();
                cost.ApplicationId = ApplicationId;
                cost.F1 = Convert.ToDecimal(values["F1"], CultureInfo.CurrentCulture);
                cost.F2 = Convert.ToDecimal(values["F2"], CultureInfo.CurrentCulture);
                cost.F3 = Convert.ToDecimal(values["F3"], CultureInfo.CurrentCulture);
                cost.Date = Convert.ToDateTime(values["Date"]);

                db.Costs.Add(cost);
                db.SaveChanges();
            }
        }

        protected void GridCosti_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            using (EnergyDbContext db = new EnergyDbContext())
            {
                var editableItem = ((GridEditableItem)e.Item);
                var costId = (int)editableItem.GetDataKeyValue("Id");

                var cost = db.Costs.Where(x => x.Id == costId).FirstOrDefault();
                if (cost != null)
                {
                    editableItem.UpdateValues(cost);
                    db.SaveChanges();
                }
            }
        }

        private class EnergyCost
        {
            public decimal F1 { get; set; }
            public decimal F2 { get; set; }
            public decimal F3 { get; set; }

            public EnergyCost(decimal f1, decimal f2, decimal f3)
            {
                F1 = f1;
                F2 = f2;
                F3 = f3;
            }
        }

        protected void GridCosti_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.IsInEditMode && e.Item is GridEditableItem)
            {
                GridEditableItem item = e.Item as GridEditableItem;
                RadNumericTextBox textF1 = item["F1"].Controls[0] as RadNumericTextBox;
                textF1.NumberFormat.DecimalSeparator = ",";
                RadNumericTextBox textF2 = item["F2"].Controls[0] as RadNumericTextBox;
                textF2.NumberFormat.DecimalSeparator = ",";
                RadNumericTextBox textF3 = item["F3"].Controls[0] as RadNumericTextBox;
                textF3.NumberFormat.DecimalSeparator = ",";
            }
        }

        protected void GridCosti_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            using (EnergyDbContext db = new EnergyDbContext())
            {
                var costId = (int)((GridDataItem)e.Item).GetDataKeyValue("Id");

                var cost = db.Costs.Where(c => c.Id == costId).FirstOrDefault();
                if (cost != null)
                {
                    db.Costs.Remove(cost);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "showError", "alert('Impossibile rimuovere il record');", true);
                        //messaggio.Text = "Impossibile rimuovere il record";
                        //dlgWidgetConfig.OpenDialog();
                    }
                }
            }
        }
    }
}