﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Costi_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.Costi.Costi_View" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<link href="/Dashboard/WidgetStyle.css" rel="stylesheet">

<mcf:Alert runat="server" id="AlertMessage" />

<%--<asp:Button Text="Archivia" runat="server" OnClick="Unnamed_Click" />--%>

<telerik:RadDockZone ID="dockZoneSettings" runat="server" Orientation="Vertical" BorderStyle="None">
    <telerik:RadDock RenderMode="Lightweight" ID="dockSettings" runat="server" Title="Settings"
        EnableAnimation="true" EnableRoundedCorners="true" Collapsed="false" EnableDrag="false" Resizable="true" Width="100%">
        <ContentTemplate>

            <div class="row" style="width:100%;">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Button runat="server" ID="btnSelect" Text="Select Assets" OnClick="btnSelect_Click" CssClass="btnConfig" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                    <asp:Label CssClass="btn btn-success" ID="lblSelectedContext" runat="server" />
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
                    <asp:RadioButtonList runat="server" ID="rbtnBehaviour" AutoPostBack="false" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Table" Value="Table" />
                        <asp:ListItem Text="Chart" Value="Chart" />
                    </asp:RadioButtonList>
                </div>
                <div class="col-sm-1 col-md-1 col-lg-5"></div>
            </div>

            <br />

            <div class="row" style="width:100%;">
                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                    <asp:RadioButtonList ID="rbtnListLastTime" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" style="margin-left:18px;">
                        <asp:ListItem Text="Last day" Value="LastDay" />
                        <asp:ListItem Text="Last month" Value="LastMonth" />
                        <asp:ListItem Text="Last year" Value="LastYear" />
                        <asp:ListItem Text="Current month" Value="CurrentMonth" />
                        <asp:ListItem Text="Current year" Value="CurrentYear" />
                    </asp:RadioButtonList>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                    <asp:Label Text="Data inizio:" runat="server" AssociatedControlID="dtPickStart" />
                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickStart" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                    <asp:Label Text="Data fine:" runat="server" AssociatedControlID="dtPickEnd" />
                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dtPickEnd" runat="server" ShowPopupOnFocus="true" DatePopupButton-Visible="true" DateInput-DisplayDateFormat="dd/MM/yyyy" Width="100%" style="min-width:116px;" />
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                    <br />
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnConfirm" Text="Confirm <i class='fa fa-check-circle'></i>" runat="server" OnClick="btnConfirm_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </ContentTemplate>
    </telerik:RadDock>
</telerik:RadDockZone>

<asp:UpdatePanel runat="server">
    <ContentTemplate>

        <asp:HiddenField ID="hiddenSelection" runat="server" />

    </ContentTemplate>
</asp:UpdatePanel>

        <mcf:PopUpDialog ID="wndSelectAsset" runat="server" Title="Seleziona contesto">
            <Body>
                <div class="row">
                    <div class="col-xs-6 col-md-6">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <telerik:RadTreeView runat="Server" ID="AssetTreeView" EnableViewState="false" EnableDragAndDrop="false" EnableDragAndDropBetweenNodes="false" 
                                    OnClientNodeClicked="function(sender, args){window['TreeNodeClick_' + sender._element.id](sender, args)}" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-xs-1 col-md-1"></div>
                    <div class="col-xs-5 col-md-5">
                        <br /><br /><br />
                        <div class="row">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:LinkButton ID="btnConfirmSelect" Text="Confirm <i class='fa fa-arrow-circle-right'></i>" runat="server" OnClick="btnConfirmSelect_Click" CssClass="btn btn-primary btn-outline" style="background-color:#0487c4;color:#ffffff;margin-left:50%;" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div> 
            </Body>
        </mcf:PopUpDialog>

<%--<telerik:RadAjaxPanel runat="server" ID="updatePnlData" PostBackControls="btnConfirm">--%>
<asp:UpdatePanel runat="server" ID="updatePnlData" UpdateMode="Conditional">
    <ContentTemplate>

<telerik:RadGrid RenderMode="Lightweight" runat="server" ID="GridCosti" AutoGenerateColumns="false" AllowPaging="false" OnExcelExportCellFormatting="GridCosti_ExcelExportCellFormatting">
    <MasterTableView DataKeyNames="Id" InsertItemPageIndexAction="ShowItemOnCurrentPage" Caption="Tariffe utilizzate"
        ShowHeadersWhenNoRecords="false" FilterExpression="">
        <Columns>
            <telerik:GridDateTimeColumn DataField="Date" HeaderText="From date" AllowSorting="true" DataFormatString="{0:dd/MM/yyyy}" UniqueName="DateColumn">
                <HeaderStyle HorizontalAlign="Center" />                
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn DataField="F1" HeaderText="F1 (€)" AllowSorting="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="F2" HeaderText="F2 (€)" AllowSorting="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="F3" HeaderText="F3 (€)" AllowSorting="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <PagerStyle Mode="NextPrevAndNumeric" />
</telerik:RadGrid>

<telerik:RadHtmlChart runat="server" ID="ChartCosti" Skin="Silk">
    <Legend>
        <Appearance Position="Bottom">
            <TextStyle Margin="20 0 0 0" />
        </Appearance>
    </Legend>
</telerik:RadHtmlChart>

<telerik:RadGrid ID="TableCosti" runat="server" AllowPaging="false" AutoGenerateColumns="false" CellSpacing="0" EnableViewState="true">
    <MasterTableView FilterExpression="" Caption="Costi" EnableHeaderContextMenu="true">
        <ColumnGroups>
            <telerik:GridColumnGroup Name="kWh" HeaderText="kWh">
                <HeaderStyle HorizontalAlign="Center" />
            </telerik:GridColumnGroup>
            <telerik:GridColumnGroup Name="Costi" HeaderText="€">
                <HeaderStyle HorizontalAlign="Center" />
            </telerik:GridColumnGroup>
        </ColumnGroups>
        <Columns>
            <telerik:GridDateTimeColumn DataField="Date" HeaderText="Date" Exportable="true" Groupable="false">
                <HeaderStyle HorizontalAlign="Center" />
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn DataField="kWhF1" HeaderText="F1" UniqueName="kWhF1" Exportable="true" Groupable="false" ColumnGroupName="kWh">
                <HeaderStyle BackColor="#ffc700" HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="kWhF2" HeaderText="F2" UniqueName="kWhF2" Exportable="true" Groupable="false" ColumnGroupName="kWh">
                <HeaderStyle BackColor="#2a94cb" HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="kWhF3" HeaderText="F3" UniqueName="kWhF3" Exportable="true" Groupable="false" ColumnGroupName="kWh">
                <HeaderStyle BackColor="#8dcb2a" HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="kWh" HeaderText="Tot" Exportable="true" Groupable="false" ColumnGroupName="kWh">
                <HeaderStyle BackColor="Yellow" HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CostF1" HeaderText="F1" UniqueName="F1" Exportable="true" Groupable="false" ColumnGroupName="Costi">
                <HeaderStyle BackColor="#ffc700" HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CostF2" HeaderText="F2" UniqueName="F2" Exportable="true" Groupable="false" ColumnGroupName="Costi">
                <HeaderStyle BackColor="#2a94cb" HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CostF3" HeaderText="F3" UniqueName="F3" Exportable="true" Groupable="false" ColumnGroupName="Costi">
                <HeaderStyle BackColor="#8dcb2a" HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridCalculatedColumn DataFields="CostF1,CostF2,CostF3" Expression="{0} + {1} + {2}" HeaderText="Tot" Exportable="true" Groupable="false" ColumnGroupName="Costi">
                <HeaderStyle BackColor="LimeGreen" HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridCalculatedColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

        <asp:UpdateProgress runat="server">
            <ProgressTemplate>
                <div class="loading">Loading&#8230;</div>
            </ProgressTemplate>
        </asp:UpdateProgress>

    </ContentTemplate>
</asp:UpdatePanel>
<%--</telerik:RadAjaxPanel>--%>

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
    <Body>
        <asp:label id="messaggio" runat="server"/>
    </Body>
</mcf:PopUpDialog>

<script src="<%=ResolveUrl("~/Dashboard/WidgetGenericScript/SettingsGraphScript.js")%>"></script>
<script type="text/javascript">
    $(function () {
        $(<%= rbtnListLastTime.ClientID %>).on('click', function () {
            SetDate("<%= dtPickStart.ClientID%>", "<%= dtPickEnd.ClientID%>", "<%= rbtnListLastTime.ClientID%>");
        });
        $(<%= btnConfirmSelect.ClientID %>).on('click', function () {
            var attr = $(<%= btnConfirmSelect.ClientID %>).attr('disabled');
            if (typeof attr !== typeof undefined && attr !== false) {
                return false;
            }
        });
    });

    function TreeNodeClick_<%= AssetTreeView.ClientID %>(sender, args) {
        if (args.get_node().get_value() != null && args.get_node().get_value() != "") {
            $(<%= btnConfirmSelect.ClientID %>).removeAttr('disabled');
            SaveSelection("<%= hiddenSelection.ClientID %>", '<%= lblSelectedContext.ClientID %>', args.get_node().get_value(), args.get_node().get_text());
        }
        else
        {
            $(<%= btnConfirmSelect.ClientID %>).attr('disabled', 'disabled');
        }
    }

</script>