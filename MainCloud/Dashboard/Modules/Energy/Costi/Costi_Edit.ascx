﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Costi_Edit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Energy.Costi.Costi_Edit" %>

<asp:ScriptManagerProxy runat="server" />

<telerik:RadGrid RenderMode="Lightweight" runat="server" ID="GridCosti" AutoGenerateColumns="false" AllowPaging="true" DataSourceID="edsCosti"
    AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="true" OnInsertCommand="GridCosti_InsertCommand" 
    OnUpdateCommand="GridCosti_UpdateCommand" EnableLinqExpressions="false" OnItemDataBound="GridCosti_ItemDataBound" OnDeleteCommand="GridCosti_DeleteCommand">
    <MasterTableView DataKeyNames="Id" CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" Caption="Costs" EditMode="PopUp">
        <EditFormSettings InsertCaption="Inserisci tariffe"><PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridDateTimeColumn DataField="Date" HeaderText="From date" AllowSorting="true" DataFormatString="{0:dd/MM/yyyy}"/>
            <telerik:GridNumericColumn DataField="F1" HeaderText="F1" UniqueName="F1" DataType="System.Decimal" DecimalDigits="6" />
            <telerik:GridNumericColumn DataField="F2" HeaderText="F2" UniqueName="F2" DataType="System.Decimal" DecimalDigits="6" />
            <telerik:GridNumericColumn DataField="F3" HeaderText="F3" UniqueName="F3" DataType="System.Decimal" DecimalDigits="6" />
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
    <PagerStyle Mode="NextPrevAndNumeric" />
</telerik:RadGrid>

<ef:EntityDataSource runat="server" ID="edsCosti" ContextTypeName="EnergyModule.Models.EnergyDbContext" EntitySetName="Costs"
    OrderBy="it.Date" AutoGenerateWhereClause="true" />