﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoldImage_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Fiera.MoldImage.MoldImage_View" %>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <center>
            <asp:Image runat="server" ID="imageMold" Width="100%" />
        </center>
    </div>
</div>
<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <asp:Label Text="Stampo:" runat="server" />
    </div>
    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <telerik:RadTextBox runat="server" ID="txtMoldName" ReadOnly="true" Font-Bold="true" Font-Size="Large" BackColor="White" ForeColor="Black" Width="100%" />
    </div>
</div>
<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <asp:Label Text="TagID:" runat="server" />
    </div>
    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <telerik:RadTextBox runat="server" ID="txtTouchTagID" ReadOnly="true" Font-Bold="true" Font-Size="Large" BackColor="White" ForeColor="Black" Width="100%" />
    </div>
</div>