﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TouchTagModule.Services;

namespace MainCloud.Dashboard.Modules.Fiera.MoldImage
{
    public partial class MoldImage_View : DataWidget<MoldImage_View>
    {
        public MoldImage_View() : base(typeof(MoldImage_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UpdateTestata();
            }
        }

        protected void UpdateTestata()
        {
            string moldId = SessionMultitenant[Const.MOLD_ID_KEY].ToString();
            MoldRepository moldRepos = new MoldRepository();
            Mold mold = moldRepos.ReadAll(x => x.Id == moldId).First();
            txtMoldName.Text = mold.Name;//moldRepos.GetName(moldId);
            imageMold.ImageUrl = mold.PathImg;// moldRepos.GetImagePath(moldId);
            //TouchTagMessageService tts = new TouchTagMessageService();
            txtTouchTagID.Text = moldRepos.GetDeviceId(mold.Id);
        }
    }
}