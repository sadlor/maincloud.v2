﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MachineView_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Fiera.MachineView.MachineView_View" %>

<link href="/Dashboard/Modules/Fiera/MachineView/Style.css" rel="stylesheet">

<div style="float:left;"><b>Legenda macchina:</b>&nbsp; &nbsp; &nbsp;</div>
<div style="float:left;"><div style="background-color:yellow; width:20px; height:20px; float:left;"></div>&nbsp;<b>Attrezzaggio</b>&nbsp; &nbsp; &nbsp;</div>
<div style="float:left;"><div style="background-color:lightgreen; width:20px; height:20px; float:left;"></div>&nbsp;<b>Produzione</b>&nbsp; &nbsp; &nbsp;</div>
<div style="float:left;"><div style="background-color:lightblue; width:20px; height:20px; float:left;"></div>&nbsp;<b>Manutenzione</b>&nbsp; &nbsp; &nbsp;</div>
<div style="float:left;"><div style="background-color:#f75555; width:20px; height:20px; float:left;"></div>&nbsp;<b>Ferma</b>&nbsp; &nbsp; &nbsp;</div>
<div style="float:left;"><div style="border:1px solid; width:20px; height:20px; float:left;"></div>&nbsp;<b>Spenta</b></div>
<br /><br />

<asp:UpdatePanel runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="RefreshTimer" />
    </Triggers>
    <ContentTemplate>
        <telerik:RadGrid runat="server" ID="transactionTable" AutoGenerateColumns="false" AllowPaging="true" AllowSorting="true"
            OnNeedDataSource="transactionTable_NeedDataSource" OnItemDataBound="transactionTable_ItemDataBound" OnItemCommand="transactionTable_ItemCommand"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US" GroupingEnabled="false" EnableLinqExpressions="false">
            <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
            <MasterTableView FilterExpression="" Caption="" CommandItemDisplay="Top" DataKeyNames="Id, MachineId, MoldId"
                EnableHeaderContextMenu="true" EnableHeaderContextAggregatesMenu="false" EnableHeaderContextFilterMenu="false">
                <CommandItemSettings ShowRefreshButton="true" ShowAddNewRecordButton="false" />
                <Columns>
                    <telerik:GridBoundColumn UniqueName="MoldColumn" DataField="MoldId" HeaderText="Stampo">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="AssetColumn" DataField="MachineId" HeaderText="Macchina">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Status" HeaderText="Stato">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="Operatore">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>--%>
                    <%--<telerik:GridBoundColumn DataField="Cause.Description" HeaderText="Causale">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Job.Order.CustomerOrder.OrderCode" HeaderText="Commessa">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Job.Order.Article.Code" HeaderText="Articolo">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridDateTimeColumn DataField="Start" HeaderText="Start" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDateTimeColumn>--%>
                    <telerik:GridBoundColumn DataField="Duration" HeaderText="Prossima manutenzione">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridDateTimeColumn DataField="End" HeaderText="DateEnd" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDateTimeColumn>--%>
                    <telerik:GridBoundColumn DataField="PartialCounting" HeaderText="Contatore" DataFormatString="{0:f0}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridTemplateColumn HeaderText="Q" UniqueName="QualityLevel">
                        <ItemTemplate>
                            <div runat="server" class="tableLight"></div>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </telerik:GridTemplateColumn>--%>
                    <%--<telerik:GridBoundColumn DataField="Open" HeaderText="Open">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>--%>
                    <telerik:GridButtonColumn CommandName="ViewDetail" Text="OEE">
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>

        <asp:Timer ID="RefreshTimer" runat="server" Interval="1000" OnTick="RefreshTimer_Tick" />
    </ContentTemplate>
</asp:UpdatePanel>

<mcf:PopUpDialog ID="dlgOEE" Title="OEE" runat="server" Width="90%" MarginLeft="5%">
    <Body>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="jobGrid" AutoGenerateColumns="false" GridLines="none" Skin="Silk" 
                    OnNeedDataSource="jobGrid_NeedDataSource" OnItemDataBound="jobGrid_ItemDataBound"
                    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
                    <MasterTableView FilterExpression="" Caption="">
                        <ColumnGroups>
                            <telerik:GridColumnGroup Name="Tempo" HeaderText="Tempo">
                                <HeaderStyle HorizontalAlign="Center" />
                            </telerik:GridColumnGroup>
                        </ColumnGroups>
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldName="GroupType"></telerik:GridGroupByField>
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldName="GroupType" SortOrder="None"></telerik:GridGroupByField>
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Label" HeaderText="">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Pezzi" HeaderText="Pezzi" DataFormatString="{0:f0}">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Colpi" HeaderText="Cicli" DataFormatString="{0:f0}">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Teorico" HeaderText="Cicli / h">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Eseguito" HeaderText="Tempo<br/>Effettivo">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Tempo" HeaderText="Tempo<br/>Totale">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Efficienza" HeaderText="Indice %">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                </telerik:RadGrid>

                <br />

                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="Disponibilità" runat="server" />
                            <telerik:RadRadialGauge runat="server" ID="meterAvailability">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtAvailability" style="text-align:center" />
                        </center>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="Efficienza" runat="server" />
                            <telerik:RadRadialGauge runat="server" ID="meterEfficiency">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtEfficiency" style="text-align:center" />
                        </center>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="Qualità" runat="server" />
                            <telerik:RadRadialGauge runat="server" ID="meterQuality">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtQuality" style="text-align:center" />
                        </center>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="OEE" runat="server" />
                            <telerik:RadRadialGauge runat="server" ID="meterOEE">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtOEE" style="text-align:center" />
                        </center>
                    </div>
                </div>
                <br />
                <div class="row alDx">
                    <telerik:RadButton ID="btnCloseOEE" runat="server" CssClass="btnActivity" OnClick="btnCloseOEE_Click">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> Chiudi </center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Body>
</mcf:PopUpDialog>