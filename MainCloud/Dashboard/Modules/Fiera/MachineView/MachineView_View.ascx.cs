﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TouchTagModule.Services;

namespace MainCloud.Dashboard.Modules.Fiera.MachineView
{
    public partial class MachineView_View : DataWidget<MachineView_View>
    {
        public MachineView_View() : base(typeof(MachineView_View)) { }

        private TransactionService transService = new TransactionService();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void transactionTable_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                string moldId = item.GetDataKeyValue("MoldId").ToString();
                string assetId = item.GetDataKeyValue("MachineId") as string;
                int? transId = Convert.ToInt32(item.GetDataKeyValue("Id"));
                TransactionService transService = new TransactionService();

                MoldRepository moldRepos = new MoldRepository();
                AssetRepository assetRepos = new AssetRepository();
                Mold mold = moldRepos.ReadAll(x => x.Id == moldId).First();
                if (!string.IsNullOrEmpty(item["AssetColumn"].Text))
                {
                    item["MoldColumn"].Text = string.Format("{0}", mold.Name);

                    item["AssetColumn"].Text = string.Format("{0}", assetRepos.GetCode(assetId));
                    item["AssetColumn"].BackColor = transService.GetCauseColorPerTransaction(transId.Value);
                }

                item["PartialCounting"].Style.Add(HtmlTextWriterStyle.FontWeight, "bold");
                item["PartialCounting"].Style.Add(HtmlTextWriterStyle.FontSize, "large");

                //TouchTagMessageService tts = new TouchTagMessageService();
                //int lastCount = tts.GetCounterUp(tts.GetTouchTagCode(mold.Id));
                //int lastCount = tts.GetLastTriggerCounter(tts.GetTouchTagCode(mold.Id), 2);
                //int produced = lastCount - mold.LastMaintenanceProgressiveCount;

                TransactionMoldService tService = new TransactionMoldService();
                TransactionMold lastT = tService.GetLastTransactionOpen(mold.Id);
                int lastShot = moldRepos.GetLastMaintenanceShot(mold.Id);
                int produced = (int)(lastT.CounterEnd.HasValue ? lastT.CounterEnd.Value : lastT.CounterStart) - lastShot;

                //cadency
                decimal cadency = 0;
                if (!string.IsNullOrEmpty(mold.AssetId))
                {
                    IndexService indexService = new IndexService();
                    TransactionRepository TRep = new TransactionRepository();
                    JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

                    Job job;
                    if (transId.HasValue)
                    {
                        job = TRep.FindByID(transId.Value).Job;
                    }
                    else
                    {
                        job = transService.GetLastTransactionOpen(mold.AssetId).Job;
                    }

                    if (job != null)
                    {
                        List<Transaction> tList = new List<Transaction>();
                        tList = TRep.ReadAll(x => x.MachineId == mold.AssetId && x.JobId == job.Id).ToList();
                        //tList = TRep.ReadAll(x => x.MachineId == AssetId && x.Start >= DateStart && x.Start <= DateEnd).ToList();

                        TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));

                        decimal qtyShotProd = 0;

                        if (productionTime.TotalSeconds > 0)
                        {
                            qtyShotProd = tList.Sum(x => x.PartialCounting);
                            cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                        }
                    }
                }
                if (cadency > 0)
                {
                    TimeSpan time = TimeSpan.FromHours((double)((mold.Maximum - produced) / cadency));
                    item["Duration"].Text = string.Format("{0:00}:{1:00}", time.Ticks < 0 ? 0 : (int)time.TotalHours, time.Ticks < 0 ? 0 : time.Minutes);
                    //item["Duration"].BackColor = GetColorMold(mold);
                    item["Duration"].BorderWidth = Unit.Pixel(2);
                    item["Duration"].BorderColor = GetColorMold(mold);
                }
                else
                {
                    item["Duration"].Text = "";
                }

                item["PartialCounting"].Text = produced.ToString();


                SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
                if (!boxService.IsMachineConnected(assetId))
                {
                    item["Status"].Text = "M";
                }
                else
                {
                    item["Status"].Text = item["Status"].Text == "False" ? "Off" : "On";
                }
            }
        }

        private Color GetColorMold(Mold mold)
        {
            //TouchTagMessageService tts = new TouchTagMessageService();
            //int lastCount = tts.GetLastTriggerCounter(tts.GetTouchTagCode(mold.Id), 2);
            //int produced = lastCount - mold.LastMaintenanceProgressiveCount;
            MoldRepository moldRepos = new MoldRepository();
            TransactionMoldService tService = new TransactionMoldService();
            TransactionMold lastT = tService.GetLastTransactionOpen(mold.Id);
            int lastShot = moldRepos.GetLastMaintenanceShot(mold.Id);
            int produced = (int)(lastT.CounterEnd.HasValue ? lastT.CounterEnd.Value : lastT.CounterStart) - lastShot;

            if (produced >= mold.Maximum)
            {
                return Color.FromName("#c20c0c");
            }
            else
            {
                if (produced > mold.Warning1)
                {
                    return Color.FromName("#ffc742");
                }
                else
                {
                    return Color.FromName("#28a745");
                }
            }
        }

        protected void transactionTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            TransactionRepository TR = new TransactionRepository();
            TransactionMoldRepository moldRepos = new TransactionMoldRepository();
            //AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            //var assetIdList = AS.GetAssetList().Select(x => x.Id);
            MoldRepository repos = new MoldRepository();
            //var assetIdList = repos.GetMoldIdList();
            var assetIdList = repos.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).Select(x => x.AssetId).Distinct().ToList();
            var moldIdList = repos.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).Select(x => x.Id).Distinct().ToList();
            DateTime now = DateTime.Now;
            var dataSource = from element in moldRepos.ReadAll(x => moldIdList.Contains(x.MoldId) && x.Start <= now)
                             group element by element.MoldId
                             into groups
                             select groups.OrderByDescending(p => p.Start).FirstOrDefault();
            //var dataSource = from element in TR.ReadAll(x => assetIdList.Contains(x.MachineId) && x.Start <= now)
            //                 group element by element.MachineId
            //                 into groups
            //                 select groups.OrderByDescending(p => p.Start).FirstOrDefault();
            transactionTable.DataSource = dataSource.ToList();
        }

        protected void RefreshTimer_Tick(object sender, EventArgs e)
        {
            transactionTable.Rebind();
        }

        #region OEE
        protected void btnCloseOEE_Click(object sender, EventArgs e)
        {
            transactionTable.Rebind();
            dlgOEE.CloseDialog();
        }

        protected void transactionTable_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ViewDetail")
            {
                //const string MACHINE_ID_KEY = "machineIdKey";
                //const string TRANSACTION_ID_KEY = "transactionIdKey";
                string assetId = (e.Item as GridDataItem).GetDataKeyValue("MachineId") as string;
                string moldId = (e.Item as GridDataItem).GetDataKeyValue("MoldId").ToString();
                int transId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));
                SessionMultitenant[Const.MACHINE_ID_KEY] = assetId;
                SessionMultitenant[Const.MOLD_ID_KEY] = moldId;
                SessionMultitenant[Const.TRANSACTION_ID_KEY] = transId;

                //string returnUrl = Request["returnurl"];
                //if (string.IsNullOrEmpty(returnUrl))
                //{
                    string destinationAreaId = CustomSettings.Find("settings-destination-area-id") as string;
                    if (!string.IsNullOrEmpty(destinationAreaId))
                    {
                        WidgetAreaRepository areaService = new WidgetAreaRepository();
                        WidgetArea wa = areaService.FindByID(destinationAreaId);

                        Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
                    }
                    //Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl("RealTime"));
                    // eliminare se funziona la riga sopra -- Response.Redirect("~/App/" + MultiTenantsHelper.ApplicationName + "/Area/RealTime");
                //}
                //else
                //{
                //    Response.Redirect("~" + returnUrl);
                //}

                //Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl("Analisi_stampo"));
                
                //Transaction t = transService.GetLastTransactionOpen(assetId);
                //if (!string.IsNullOrEmpty(t.JobId))
                //{
                //    AssetId = assetId;
                //    AssetRepository r = new AssetRepository();
                //    dlgOEE.Title = string.Format("OEE - Macchina: {0}", r.FindByID(assetId).Description);
                //    dlgOEE.OpenDialog();
                //    jobGrid.Rebind();
                //}
            }
        }

        protected string AssetId
        {
            get
            {
                if (string.IsNullOrEmpty((string)ViewState["MachineViewAssetId"]))
                {
                    return null;
                }
                return ViewState["MachineViewAssetId"].ToString();
            }
            set
            {
                ViewState["MachineViewAssetId"] = value;
            }
        }

        private class JobRecord
        {
            public string Label { get; set; }
            public decimal? Pezzi { get; set; }
            public decimal? Colpi { get; set; }
            public decimal? Teorico { get; set; }
            public string Eseguito { get; set; }
            public string Tempo { get; set; }
            public string Efficienza { get; set; }
            public string GroupType { get; set; }

            public JobRecord(string label, decimal? pezzi, decimal? colpi, decimal? teorico, TimeSpan? eseguito, TimeSpan? tempo, string efficienza, string groupType)
            {
                Label = label;
                Pezzi = pezzi;
                Colpi = colpi;
                Teorico = teorico;
                if (eseguito != null)
                {
                    Eseguito = string.Format("{0}:{1:d2}:{2:d2}", (int)eseguito?.TotalHours, Math.Abs(eseguito.Value.Minutes), Math.Abs(eseguito.Value.Seconds));
                }
                if (tempo != null)
                {
                    Tempo = string.Format("{0}:{1:d2}:{2:d2}", (int)tempo?.TotalHours, tempo?.Minutes, tempo?.Seconds);
                }
                Efficienza = efficienza;
                GroupType = groupType;
            }
        }

        protected void jobGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (dlgOEE.IsOpen())
            {
                if (!string.IsNullOrEmpty(AssetId))
                {
                    IndexService indexService = new IndexService();
                    TransactionRepository TRep = new TransactionRepository();
                    JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

                    Job job = transService.GetLastTransactionOpen(AssetId).Job;

                    List<Transaction> tList = new List<Transaction>();
                    tList = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id).ToList();
                    //tList = TRep.ReadAll(x => x.MachineId == AssetId && x.Start >= DateStart && x.Start <= DateEnd).ToList();

                    TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                    TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                    TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                    decimal oreProdTeoricoTotale = 0;
                    decimal oreProdTeorico = 0;
                    decimal qtyShotProd = 0;
                    decimal waste = 0;
                    decimal qtyProd = 0;
                    decimal cadency = 0;
                    decimal availability = 0;
                    decimal efficiency = 0;
                    decimal quality = 0;
                    decimal OEE = 0;

                    decimal impronte = transService.GetLastTransactionOpen(AssetId).CavityMoldNum;

                    if (productionTime.TotalSeconds > 0)
                    {
                        qtyShotProd = tList.Sum(x => x.PartialCounting);
                        cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                        if (job.StandardRate > 0)
                        {
                            oreProdTeorico += Math.Round(tList.Sum(x => x.PartialCounting) / job.StandardRate, 2);
                            oreProdTeoricoTotale += Math.Round((decimal)(job.QtyOrdered / (job.CavityMoldNum > 0 ? job.CavityMoldNum : 1)) / job.StandardRate, 2);
                        }
                        qtyProd = tList.Sum(x => x.PartialCounting * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                        try
                        {
                            waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.JobId == job.Id).Sum(x => x.QtyProductionWaste), 0);
                            //waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.Date >= DateStart && x.Date <= DateEnd).Sum(x => x.QtyProductionWaste), 0);
                        }
                        catch (InvalidOperationException) { }
                    }

                    availability = indexService.Availability(productionTime, machineOn);
                    efficiency = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
                    quality = indexService.Quality(qtyProd, waste);
                    OEE = indexService.OEE(availability / 100, efficiency / 100, quality / 100);

                    List<JobRecord> dataSource = new List<JobRecord>();

                    //generalità job
                    dataSource.Add(new JobRecord("Ordinato da produrre", job.QtyOrdered, Math.Round(job.QtyOrdered / (impronte > 0 ? impronte : 1), 2), job.StandardRate, null, TimeSpan.FromHours((double)oreProdTeoricoTotale), "", "Efficienza"));

                    //availability
                    dataSource.Add(new JobRecord("Tempo totale produzione / disponibile", null, null, null, productionTime, machineOn, availability + "%", "Disponibilità"));

                    //turni precedenti
                    decimal lastWorkshiftOreProdTeorico = 0;
                    decimal lastWorkshiftQtyShotProd = 0;
                    decimal lastWorkshiftQtyProd = 0;
                    decimal lastWorkshiftCadency = 0;
                    decimal lastWorkshiftEfficiency = 0;
                    Transaction lastTrans = transService.GetLastTransactionOpen(AssetId);
                    Transaction endLastWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId != lastTrans.OperatorId && !x.Open).OrderByDescending(x => x.Start).FirstOrDefault();
                    List<Transaction> lastWorkshift = new List<Transaction>();
                    if (endLastWorkshift != null)
                    {
                        lastWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && !x.Open && x.Start <= endLastWorkshift.Start).ToList();
                        lastWorkshift.Add(endLastWorkshift);
                    }
                    if (lastWorkshift.Count > 0)  //se c'è un turno precedente per quella commessa
                    {
                        TimeSpan lastWorkshiftProductionTime = TimeSpan.FromSeconds((double)lastWorkshift.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                        if (lastWorkshiftProductionTime.TotalSeconds > 0)
                        {
                            lastWorkshiftQtyShotProd = lastWorkshift.Sum(x => x.PartialCounting);
                            lastWorkshiftCadency = Math.Round(lastWorkshiftQtyShotProd / (decimal)lastWorkshiftProductionTime.TotalHours, 2);
                            lastWorkshiftQtyProd = lastWorkshift.Sum(x => x.PartialCounting * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                            if (job.StandardRate > 0)
                            {
                                lastWorkshiftOreProdTeorico += Math.Round(lastWorkshiftQtyShotProd / job.StandardRate, 2);
                            }
                        }
                        lastWorkshiftEfficiency = indexService.Efficiency((decimal)lastWorkshiftProductionTime.TotalHours, lastWorkshiftOreProdTeorico);
                        dataSource.Add(new JobRecord("Eseguito turni precedenti", lastWorkshiftQtyProd, lastWorkshiftQtyShotProd, lastWorkshiftCadency, lastWorkshiftProductionTime, null, lastWorkshiftEfficiency + "%", "Efficienza"));
                    }

                    //turno corrente
                    decimal currentWorkshiftOreProdTeorico = 0;
                    decimal currentWorkshiftQtyShotProd = 0;
                    decimal currentWorkshiftQtyProd = 0;
                    decimal currentWorkshiftCadency = 0;
                    decimal currentWorkshiftEfficiency = 0;
                    List<Transaction> currentWorkshift = new List<Transaction>();
                    if (transService.LastTransactionOpenIsInProduction(AssetId))
                    {
                        if (endLastWorkshift == null)
                        {
                            //non c'è turno precedente
                            currentWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId == lastTrans.OperatorId).ToList();
                        }
                        else
                        {
                            currentWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId == lastTrans.OperatorId && x.Start > endLastWorkshift.Start).ToList();
                        }
                    }
                    TimeSpan currentWorkshiftProductionTime = TimeSpan.FromSeconds((double)currentWorkshift.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                    if (currentWorkshiftProductionTime.TotalSeconds > 0)
                    {
                        currentWorkshiftQtyShotProd = currentWorkshift.Sum(x => x.PartialCounting);
                        currentWorkshiftCadency = Math.Round(currentWorkshiftQtyShotProd / (decimal)currentWorkshiftProductionTime.TotalHours, 2);
                        currentWorkshiftQtyProd = currentWorkshift.Sum(x => x.PartialCounting * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                        if (job.StandardRate > 0)
                        {
                            currentWorkshiftOreProdTeorico += Math.Round(currentWorkshiftQtyShotProd / job.StandardRate, 2);
                        }
                    }
                    currentWorkshiftEfficiency = indexService.Efficiency((decimal)currentWorkshiftProductionTime.TotalHours, currentWorkshiftOreProdTeorico);
                    dataSource.Add(new JobRecord("Eseguito turno corrente", currentWorkshiftQtyProd, currentWorkshiftQtyShotProd, currentWorkshiftCadency, currentWorkshiftProductionTime, null, currentWorkshiftEfficiency + "%", "Efficienza"));

                    ////efficiency
                    //dataSource.Add(new JobRecord("Eseguito effettivo", qtyProd, qtyShotProd, cadency, null, productionTime, efficiency + "%", "Efficienza"));
                    decimal pezziResidui = job.QtyOrdered - qtyProd + waste;
                    TimeSpan tempoResiduo = TimeSpan.FromHours((double)(cadency > 0 ? ((pezziResidui / (impronte > 0 ? impronte : 1)) / cadency) : 0) + productionTime.TotalHours);
                    decimal wasteShotResiduo = pezziResidui % (impronte > 0 ? impronte : 1) > 0 ? (int)(pezziResidui / (impronte > 0 ? impronte : 1)) + 1 : pezziResidui / (impronte > 0 ? impronte : 1);
                    dataSource.Add(
                        new JobRecord(
                            "Residuo da produrre (eccedenza) compreso scarti",
                            pezziResidui,
                            wasteShotResiduo,//Math.Round(pezziResidui / (impronte > 0 ? impronte : 1), 2),
                            null,
                            cadency > 0 ? TimeSpan.FromHours((double)((pezziResidui / (impronte > 0 ? impronte : 1)) / cadency)) : TimeSpan.FromHours((double)(0)),
                            null,
                            "",
                            "Efficienza"));

                    decimal tempoAssegnato = Math.Round(job.QtyOrdered / (job.StandardRate * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1)), 2);
                    decimal tempoPrevisto = 0;
                    if (efficiency > 0)
                    {
                        tempoPrevisto = Math.Round(tempoAssegnato / efficiency * 100, 2);
                    }
                    else
                    {
                        tempoPrevisto = tempoAssegnato;
                    }
                    dataSource.Add(new JobRecord("Tempo eccedenza (recupero) e totale lotto", null, null, null, TimeSpan.FromHours((double)(tempoPrevisto - tempoAssegnato)), tempoResiduo, 100 - Math.Round(tempoAssegnato / tempoPrevisto * 100, 2) + "%", "Efficienza"));

                    //quality
                    dataSource.Add(new JobRecord("Totale pezzi prodotti", qtyProd, null, null, null, null, quality + "%", "Qualità"));
                    decimal wasteShot = waste % (impronte > 0 ? impronte : 1) > 0 ? (int)(waste / (impronte > 0 ? impronte : 1)) + 1 : waste / (impronte > 0 ? impronte : 1);
                    if (job.StandardRate > 0)
                    {
                        dataSource.Add(new JobRecord("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours((double)(wasteShot / job.StandardRate)), null, (100 - quality) + "%", "Qualità"));
                    }
                    else
                    {
                        dataSource.Add(new JobRecord("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours(0), null, (100 - quality) + "%", "Qualità"));
                    }

                    jobGrid.DataSource = dataSource;

                    meterAvailability.Pointer.Value = availability;
                    txtAvailability.Text = availability.ToString();
                    meterEfficiency.Pointer.Value = efficiency;
                    txtEfficiency.Text = efficiency.ToString();
                    meterQuality.Pointer.Value = quality;
                    txtQuality.Text = quality.ToString();
                    meterOEE.Pointer.Value = OEE;
                    txtOEE.Text = OEE.ToString();
                }
            }

            //if (dlgOEE.IsOpen())
            //{
            //    if (!string.IsNullOrEmpty(AssetId))
            //    {
            //        List<JobRecord> dataSource = new List<JobRecord>();
            //        //    //6 righe fisse per il datasource
            //        //    //1 -> new JobTable("Da produrre colpi", 0, 0, 0, "100%", "Last");

            //        TransactionService TService = new TransactionService();
            //        Transaction lastTrans = TService.GetLastTransactionOpen(AssetId); //passare assetId dall'area rilievi
            //        if (!string.IsNullOrEmpty(lastTrans.JobId))
            //        {
            //            //JobRepository jobRep = new JobRepository();
            //            Job job = lastTrans.Job; //jobRep.FindByID(lastTrans.JobId);
            //            int nImpronte = 1;
            //            if (!string.IsNullOrEmpty(lastTrans.MoldId))
            //            {
            //                MoldRepository mRep = new MoldRepository();
            //                nImpronte = mRep.FindByID(lastTrans.MoldId).CavityMoldNum;
            //            }
            //            if (nImpronte == 0)
            //            {
            //                nImpronte = 1;
            //            }

            //            CauseService causeService = new CauseService();
            //            string causeId = causeService.GetCauseByCode("200").Id;

            //            TransactionRepository TRep = new TransactionRepository();
            //            Transaction endLastWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId != lastTrans.OperatorId && !x.Open).OrderByDescending(x => x.Start).FirstOrDefault();

            //            if (job.NStampateOra == 0)
            //            {
            //                job.NStampateOra = 1;
            //            }

            //            //generalità job
            //            dataSource.Add(new JobRecord("Quantità da produrre", job.QtyOrdered, job.QtyOrdered / nImpronte, job.NStampateOra, null, null, "", "Efficienza"));

            //            //turni precedenti
            //            List<Transaction> lastWorkshift;
            //            if (endLastWorkshift == null)
            //            {
            //                lastWorkshift = new List<Transaction>();
            //            }
            //            else
            //            {
            //                lastWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && !x.Open && x.Start <= endLastWorkshift.Start).ToList();
            //                //lastWorkshift.Add(endLastWorkshift); TODO: implementare i turni
            //                if (!lastWorkshift.Any(x => x == endLastWorkshift))
            //                {
            //                    lastWorkshift.Add(endLastWorkshift);
            //                }
            //            }
            //            decimal lastWorkshiftEffettivo = 0;
            //            if (lastWorkshift.Count > 0 && lastWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration) > 0)
            //            {
            //                lastWorkshiftEffettivo = Math.Round(lastWorkshift.Sum(x => x.PartialCounting) / Convert.ToDecimal(TimeSpan.FromSeconds((double)lastWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)).TotalHours), 2);
            //            }
            //            if (job.NStampateOra > 0)
            //            {
            //                dataSource.Add(
            //                    new JobRecord(
            //                        "Eseguito turni precedenti",
            //                        lastWorkshift.Sum(x => x.PartialCounting) * nImpronte,
            //                        lastWorkshift.Sum(x => x.PartialCounting),
            //                        null,
            //                        lastWorkshiftEffettivo,
            //                        TimeSpan.FromSeconds((double)lastWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)),
            //                        Math.Round(lastWorkshiftEffettivo / job.NStampateOra * 100, 2) + "%",
            //                        "Efficienza"));
            //            }
            //            else
            //            {
            //                dataSource.Add(
            //                    new JobRecord(
            //                        "Eseguito turni precedenti",
            //                        lastWorkshift.Sum(x => x.PartialCounting) * nImpronte,
            //                        lastWorkshift.Sum(x => x.PartialCounting),
            //                        null,
            //                        lastWorkshiftEffettivo,
            //                        TimeSpan.FromSeconds((double)lastWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)),
            //                        "0%",
            //                        "Efficienza"));
            //            }

            //            //turno corrente
            //            List<Transaction> currentWorkshift = new List<Transaction>();
            //            if (TService.LastTransactionOpenIsInProduction(AssetId))
            //            {
            //                if (endLastWorkshift == null)
            //                {
            //                    //non c'è turno precedente
            //                    currentWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId == lastTrans.OperatorId).ToList();
            //                }
            //                else
            //                {
            //                    currentWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId == lastTrans.OperatorId && x.Start > endLastWorkshift.Start).ToList();
            //                }
            //            }
            //            decimal currentEffettivo = 0;
            //            if (currentWorkshift.Sum(x => x.Duration) > 0)
            //            {
            //                currentEffettivo = Math.Round(currentWorkshift.Sum(x => x.PartialCounting) / Convert.ToDecimal(TimeSpan.FromSeconds((double)currentWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)).TotalHours), 2);
            //            }
            //            if (job.NStampateOra > 0)
            //            {
            //                dataSource.Add(
            //                        new JobRecord(
            //                            "Eseguito turno corrente",
            //                            currentWorkshift.Sum(x => x.PartialCounting) * nImpronte,
            //                            currentWorkshift.Sum(x => x.PartialCounting),
            //                            null,
            //                            currentEffettivo,
            //                            TimeSpan.FromSeconds((double)currentWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)),
            //                            Math.Round(currentEffettivo / job.NStampateOra * 100, 2) + "%",
            //                            "Efficienza"));
            //            }
            //            else
            //            {
            //                dataSource.Add(
            //                        new JobRecord(
            //                            "Eseguito turno corrente",
            //                            currentWorkshift.Sum(x => x.PartialCounting) * nImpronte,
            //                            currentWorkshift.Sum(x => x.PartialCounting),
            //                            null,
            //                            currentEffettivo,
            //                            TimeSpan.FromSeconds((double)currentWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)),
            //                            "0%",
            //                            "Efficienza"));
            //            }

            //            decimal totProduzione = 0;
            //            if (lastWorkshift.Count > 0)
            //            {
            //                totProduzione = lastWorkshift.Sum(x => x.PartialCounting);
            //            }
            //            if (currentWorkshift.Count > 0)
            //            {
            //                totProduzione += currentWorkshift.Sum(x => x.PartialCounting);
            //            }

            //            //scarti
            //            JobProductionWasteService jpws = new JobProductionWasteService();
            //            List<JobProductionWaste> scarti = jpws.GetWasteListByJobId(job.Id);
            //            dataSource.Add(new JobRecord("Scarti", scarti.Sum(x => x.QtyProductionWaste), scarti.Sum(x => x.QtyProductionWaste) / nImpronte, null, null, null, "", "Efficienza"));

            //            //residuo
            //            decimal pezziResidui = job.QtyOrdered - (totProduzione * nImpronte) + scarti.Sum(x => x.QtyProductionWaste);
            //            if (job.NStampateOra > 0)
            //            {
            //                dataSource.Add(
            //                    new JobRecord(
            //                        "Residuo da produrre (eccedenza)",
            //                        pezziResidui,
            //                        pezziResidui / nImpronte,
            //                        null,
            //                        null,
            //                        TimeSpan.FromHours((double)((pezziResidui / nImpronte) / job.NStampateOra)),
            //                        "",
            //                        "Efficienza"));
            //            }
            //            else
            //            {
            //                dataSource.Add(
            //                   new JobRecord(
            //                       "Residuo da produrre (eccedenza)",
            //                       pezziResidui,
            //                       pezziResidui / nImpronte,
            //                       null,
            //                       null,
            //                       TimeSpan.FromHours((double)(0)),
            //                       "",
            //                       "Efficienza"));
            //            }

            //            //Resa
            //            List<Transaction> currentJobTrans = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id).ToList();
            //            dataSource.Add(
            //                new JobRecord(
            //                    "Tempo totale disponibile", null, null, null, null, TimeSpan.FromSeconds((double)currentJobTrans.Sum(x => x.Duration)), "", "Disponibilità"));
            //            dataSource.Add(
            //                new JobRecord(
            //                    "Tempo totale produzione",
            //                    null,
            //                    null,
            //                    null,
            //                    null,
            //                    TimeSpan.FromSeconds((double)currentJobTrans.Where(x => x.CauseId == causeId).Sum(x => x.Duration)),
            //                    Math.Round(TimeSpan.FromSeconds((double)currentJobTrans.Where(x => x.CauseId == causeId).Sum(x => x.Duration)).TotalSeconds /
            //                    TimeSpan.FromSeconds((double)currentJobTrans.Sum(x => x.Duration)).TotalSeconds * 100, 2) + "%",
            //                    "Disponibilità"));

            //            //Qualità
            //            if (totProduzione > 0)
            //            {
            //                dataSource.Add(
            //                    new JobRecord(
            //                        "Totale pezzi prodotti",
            //                        totProduzione * nImpronte,
            //                        null,
            //                        null,
            //                        null,
            //                        null,
            //                        Math.Round((((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) * 100, 2) + "%",
            //                        "Qualità"));
            //            }
            //            else
            //            {
            //                dataSource.Add(new JobRecord("Totale pezzi prodotti", totProduzione, null, null, null, null, "", "Qualità"));
            //            }
            //            dataSource.Add(new JobRecord("Totale non conformità", scarti.Sum(x => x.QtyProductionWaste), null, null, null, null, "", "Qualità"));
            //        }
            //        else
            //        {
            //            //nessun job assegnato
            //            dataSource.Add(new JobRecord("Quantità da produrre", null, null, null, null, null, "", "Efficienza"));
            //            dataSource.Add(new JobRecord("Eseguito turni precedenti", null, null, null, null, null, "", "Efficienza"));
            //            dataSource.Add(new JobRecord("Eseguito turno corrente", null, null, null, null, null, "", "Efficienza"));
            //            dataSource.Add(new JobRecord("Scarti", null, null, null, null, null, "", "Efficienza"));
            //            dataSource.Add(new JobRecord("Residuo da produrre", null, null, null, null, null, "", "Efficienza"));
            //            dataSource.Add(new JobRecord("Tempo totale disponibile", null, null, null, null, null, "", "Resa"));
            //            dataSource.Add(new JobRecord("Tempo totale produzione", null, null, null, null, null, "", "Resa"));
            //            dataSource.Add(new JobRecord("Totale pezzi prodotti", null, null, null, null, null, "", "Qualità"));
            //            dataSource.Add(new JobRecord("Totale non conformità", null, null, null, null, null, "", "Qualità"));
            //        }

                    //jobGrid.DataSource = dataSource;
                //}
            //}
        }

        protected void jobGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridGroupHeaderItem)
            {
                GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;
                DataRowView groupDataRow = (DataRowView)e.Item.DataItem;
                item.DataCell.Text = groupDataRow["GroupType"].ToString();
            }
        }
        #endregion
    }
}