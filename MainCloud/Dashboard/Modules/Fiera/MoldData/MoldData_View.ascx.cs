﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TouchTagModule.Services;

namespace MainCloud.Dashboard.Modules.Fiera.MoldData
{
    public partial class MoldData_View : DataWidget<MoldData_View>
    {
        public MoldData_View() : base(typeof(MoldData_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TransactionMoldRepository TR = new TransactionMoldRepository();
                int? transactionId = null;
                if (SessionMultitenant.ContainsKey(Const.TRANSACTION_ID_KEY) && !string.IsNullOrEmpty(SessionMultitenant[Const.TRANSACTION_ID_KEY].ToString()))
                {
                    transactionId = Convert.ToInt32(SessionMultitenant[Const.TRANSACTION_ID_KEY]);
                }
                //string assetId = string.Empty;
                //if (SessionMultitenant.ContainsKey(Const.MACHINE_ID_KEY) && !string.IsNullOrEmpty(SessionMultitenant[Const.MACHINE_ID_KEY].ToString()))
                //{
                //    assetId = SessionMultitenant[Const.MACHINE_ID_KEY].ToString();
                //}
                //DateTime now = DateTime.Now;
                //Job j = TR.ReadAll(x => x.MachineId == assetId && x.Start <= now).OrderByDescending(x => x.Start).First().Job;
                if (transactionId != null)
                {
                    Job j = TR.FindByID(transactionId).Job;
                    AssetRepository repos = new AssetRepository();
                    txtMachineCode.Text = repos.GetCode(j.AssetId);
                    txtArticleCode.Text = j?.Order?.Article?.Code;
                    txtArticleDescription.Text = j?.Order?.Article?.Description;
                    txtCustomerOrderCode.Text = j?.Order?.CustomerOrder?.OrderCode;
                }
            }
            UpdateProgressiveCounter();
        }

        protected void UpdateProgressiveCounter()
        {
            if (SessionMultitenant.ContainsKey(Const.MOLD_ID_KEY) && !string.IsNullOrEmpty(SessionMultitenant[Const.MOLD_ID_KEY] as string))
            {
                string assetId = SessionMultitenant[Const.MACHINE_ID_KEY] as string;
                string moldId = SessionMultitenant[Const.MOLD_ID_KEY].ToString();
                int? transId = SessionMultitenant.ContainsKey(Const.TRANSACTION_ID_KEY) ? Convert.ToInt32(SessionMultitenant[Const.TRANSACTION_ID_KEY]) : (int?)null;

                MoldRepository repos = new MoldRepository();
                Mold mold = repos.ReadAll(x => x.Id == moldId).First();//repos.FindByID(moldId);

                //TouchTagMessageService tts = new TouchTagMessageService();
                //int lastCount = tts.GetLastTriggerCounter(tts.GetTouchTagCode(mold.Id), 2);
                //int lastCount = tts.GetCounterUp(tts.GetTouchTagCode(mold.Id));

                TransactionMoldService tService = new TransactionMoldService();
                TransactionMold lastT = tService.GetLastTransactionOpen(mold.Id);
                int lastCount = (int)(lastT.CounterEnd.HasValue ? lastT.CounterEnd.Value : lastT.CounterStart);

                ProgressiveCounter.Text = lastCount.ToString();

                //if (!string.IsNullOrEmpty(mold.AssetId))
                //{
                    //TransactionService transService = new TransactionService();
                    //IndexService indexService = new IndexService();
                    //TransactionRepository TRep = new TransactionRepository();
                    //JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

                    //Job job;
                    //if (transId.HasValue)
                    //{
                    //    job = TRep.FindByID(transId.Value).Job;
                    //}
                    //else
                    //{
                    //    job = transService.GetLastTransactionOpen(mold.AssetId).Job;
                    //}

                    //List<Transaction> tList = new List<Transaction>();
                    //tList = TRep.ReadAll(x => x.MachineId == mold.AssetId && x.JobId == job.Id).ToList();
                    ////tList = TRep.ReadAll(x => x.MachineId == AssetId && x.Start >= DateStart && x.Start <= DateEnd).ToList();

                    //TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));

                    //decimal qtyShotProd = 0;
                    //decimal cadency = 0;

                    //if (productionTime.TotalSeconds > 0)
                    //{
                    //    qtyShotProd = tList.Sum(x => x.PartialCounting);
                    //    cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                    //}

                    //txtCadency.Text = cadency.ToString();


                    TransactionMoldRepository TRep = new TransactionMoldRepository();

                    decimal produced = tService.LastCounterMold(mold.Id) - repos.GetLastMaintenanceShot(mold.Id);
                    decimal left = mold.Maximum - produced < 0 ? 0 : mold.Maximum - produced;

                    string jobId = tService.GetLastJob(mold.Id);

                    decimal cadency = 0;
                    if (!string.IsNullOrEmpty(jobId))
                    {
                        List<TransactionMold> tList = new List<TransactionMold>();
                        tList = TRep.ReadAll(x => x.MoldId == mold.Id && x.JobId == jobId).ToList();
                        //tList = TRep.ReadAll(x => x.MachineId == AssetId && x.Start >= DateStart && x.Start <= DateEnd).ToList();

                        TimeSpan productionTime;
                        if (mold.Id == "649ea07c-8fac-4e4b-817a-ee35e7deb744" || mold.Id == "131ff7ef-2d61-4914-af46-b7b8d447b7e7")
                        {
                            productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString() || x.PartialCounting > 0).Sum(x => x.Duration));
                        }
                        else
                        {
                            productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                        }

                        decimal qtyShotProd = 0;

                        if (productionTime.TotalSeconds > 0)
                        {
                            qtyShotProd = tList.Sum(x => x.PartialCounting);
                            cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                        }
                    }

                    txtCadency.Text = cadency.ToString();
                //}
            }
        }

        protected void Timer_Tick(object sender, EventArgs e)
        {
        }
    }
}