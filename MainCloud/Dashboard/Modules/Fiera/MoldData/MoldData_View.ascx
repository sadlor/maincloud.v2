﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoldData_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Fiera.MoldData.MoldData_View" %>

<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0;">
        <asp:Label Text="Pressa:" runat="server" />
    </div>
    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <telerik:RadTextBox runat="server" ID="txtMachineCode" ReadOnly="true" Font-Bold="true" Font-Size="Large" BackColor="White" ForeColor="Black" Width="100%" />
    </div>
</div>
<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0;">
        <asp:Label Text="Commessa:" runat="server" />
    </div>
    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <telerik:RadTextBox runat="server" ID="txtCustomerOrderCode" ReadOnly="true" Font-Bold="true" Font-Size="Large" BackColor="White" ForeColor="Black" Width="100%" />
    </div>
</div>
<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0;">
        <asp:Label Text="Articolo:" runat="server" />
    </div>
    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <telerik:RadTextBox runat="server" ID="txtArticleCode" ReadOnly="true" Font-Bold="true" Font-Size="Large" BackColor="White" ForeColor="Black" Width="100%" />
    </div>
</div>
<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0;">
    </div>
    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <telerik:RadTextBox runat="server" ID="txtArticleDescription" ReadOnly="true" Font-Bold="true" Font-Size="Large" BackColor="White" ForeColor="Black" Width="100%" />
    </div>
</div>

<asp:UpdatePanel runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="Timer" />
    </Triggers>
    <ContentTemplate>
        <asp:Timer runat="server" ID="Timer" OnTick="Timer_Tick" Interval="1000"></asp:Timer>
        <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0;">
                <asp:Label Text="Contatore:" runat="server" />
            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                <telerik:RadTextBox runat="server" ID="ProgressiveCounter" ReadOnly="true" Font-Bold="true" Font-Size="Large" ForeColor="Black" style="background-color:rgb(145, 220, 243);text-align:right;" Width="100%" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0;">
                <asp:Label Text="Cadenza colpi/h:" runat="server" />
            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                <telerik:RadTextBox runat="server" ID="txtCadency" ReadOnly="true" Font-Bold="true" Font-Size="Large" ForeColor="Black" style="text-align:right;" BackColor="White" Width="100%" />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>