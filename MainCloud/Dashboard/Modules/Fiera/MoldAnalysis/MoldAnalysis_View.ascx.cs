﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TouchTagModule.Services;

namespace MainCloud.Dashboard.Modules.Fiera.MoldAnalysis
{
    public partial class MoldAnalysis_View : DataWidget<MoldAnalysis_View>
    {
        public MoldAnalysis_View() : base(typeof(MoldAnalysis_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            UpdateMaintenance();
        }

        protected void UpdateMaintenance()
        {
            if (SessionMultitenant.ContainsKey(Const.MACHINE_ID_KEY) && !string.IsNullOrEmpty(SessionMultitenant[Const.MACHINE_ID_KEY].ToString()))
            {
                string assetId = SessionMultitenant[Const.MACHINE_ID_KEY].ToString();
                string moldId = SessionMultitenant[Const.MOLD_ID_KEY].ToString();
                int? transId = Convert.ToInt32(SessionMultitenant[Const.TRANSACTION_ID_KEY]);

                MoldRepository moldRepos = new MoldRepository();
                Mold mold = moldRepos.ReadAll(x => x.Id == moldId).First(); //moldRepos.FindByID(moldId);

                //TouchTagMessageService tts = new TouchTagMessageService();
                //int lastCount = tts.GetLastTriggerCounter(tts.GetTouchTagCode(mold.Id), 2);
                //int lastCount = tts.GetCounterUp(tts.GetTouchTagCode(mold.Id));

                TransactionMoldService tService = new TransactionMoldService();
                TransactionMold lastT = tService.GetLastTransactionOpen(mold.Id);
                int lastShot = moldRepos.GetLastMaintenanceShot(mold.Id);
                int produced = (int)(lastT.CounterEnd.HasValue ? lastT.CounterEnd.Value : lastT.CounterStart) - lastShot;

                //int produced = lastCount - mold.LastMaintenanceProgressiveCount;
                if (produced >= mold.Maximum)
                {
                    DoMaintenance(mold, lastT);
                    lastShot = moldRepos.GetLastMaintenanceShot(mold.Id);
                    produced = (int)(lastT.CounterEnd.HasValue ? lastT.CounterEnd.Value : lastT.CounterStart) - lastShot;
                }
                int left = mold.Maximum - produced;

                decimal warning1Perc = Math.Round((decimal)mold.Warning1 / mold.Maximum * 100, 0);
                decimal warning2Perc = Math.Round((decimal)mold.Warning2 / mold.Maximum * 100, 0);

                //progress bar
                dotContainerWarning1.Attributes["class"] = "milestones milestone__" + warning1Perc;
                dotWarning1.Style[HtmlTextWriterStyle.BackgroundColor] = "#ffbc42!important";
                labelWarning1.Attributes["class"] = "milestones milestone__" + warning1Perc;
                labelTimeWarning1.Attributes["class"] = "milestones milestone__" + warning1Perc;
                //labelWarning1.InnerText = string.Format("{0}%", warning1Perc);

                dotContainerWarning2.Attributes["class"] = "milestones milestone__" + warning2Perc;
                dotWarning2.Style[HtmlTextWriterStyle.BackgroundColor] = "#ff9342!important";
                labelWarning2.Attributes["class"] = "milestones milestone__" + warning2Perc;
                labelTimeWarning2.Attributes["class"] = "milestones milestone__" + warning2Perc;
                //labelWarning2.InnerText = string.Format("{0}%", warning2Perc);
                progressiveBar.Style[HtmlTextWriterStyle.Width] = string.Format("{0}%", Math.Round(((decimal)produced / mold.Maximum) * 100, 0));
                progressiveBar.InnerText = string.Format("{0}", produced);
                if (produced >= mold.Maximum)
                {
                    progressiveBar.Style[HtmlTextWriterStyle.BackgroundColor] = "#c20c0c!important";
                }
                else
                {
                    if (produced > mold.Warning1)
                    {
                        progressiveBar.Style[HtmlTextWriterStyle.BackgroundColor] = "#ffc742!important";
                    }
                    else
                    {
                        progressiveBar.Style[HtmlTextWriterStyle.BackgroundColor] = "#28a745!important";
                    }
                }

                //cadency
                decimal cadency = 0;
                if (!string.IsNullOrEmpty(mold.AssetId))
                {
                    TransactionService transService = new TransactionService();
                    IndexService indexService = new IndexService();
                    TransactionRepository TRep = new TransactionRepository();
                    JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

                    Job job;
                    if (transId.HasValue)
                    {
                        job = TRep.FindByID(transId.Value).Job;
                    }
                    else
                    {
                        job = transService.GetLastTransactionOpen(mold.AssetId).Job;
                    }

                    List<Transaction> tList = new List<Transaction>();
                    tList = TRep.ReadAll(x => x.MachineId == mold.AssetId && x.JobId == job.Id).ToList();
                    //tList = TRep.ReadAll(x => x.MachineId == AssetId && x.Start >= DateStart && x.Start <= DateEnd).ToList();

                    TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));

                    decimal qtyShotProd = 0;

                    if (productionTime.TotalSeconds > 0)
                    {
                        qtyShotProd = tList.Sum(x => x.PartialCounting);
                        cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                    }
                }

                //table
                LastMaintenanceCounter.InnerText = mold.LastMaintenanceProgressiveCount.ToString();
                TotalShotsLastDateField.InnerText = mold.LastMaintenanceDate?.ToString("dd/MM/yyyy HH:mm");

                tdProduced.InnerText = produced.ToString();
                tdProducedCounter.InnerText = (mold.LastMaintenanceProgressiveCount + produced).ToString();

                tdWarning1.InnerText = mold.Warning1.ToString();
                tdWarning1Counter.InnerText = (mold.LastMaintenanceProgressiveCount + mold.Warning1).ToString();
                tdWarning1Left.InnerText = (mold.Warning1 - produced < 0 ? 0 : mold.Warning1 - produced).ToString();
                labelWarning1.InnerText = (mold.Warning1 - produced < 0 ? 0 : mold.Warning1 - produced).ToString();

                tdWarning2.InnerText = mold.Warning2.ToString();
                tdWarning2Counter.InnerText = (mold.LastMaintenanceProgressiveCount + mold.Warning2).ToString();
                tdWarning2Left.InnerText = (mold.Warning2 - produced < 0 ? 0 : mold.Warning2 - produced).ToString();
                labelWarning2.InnerText = (mold.Warning2 - produced < 0 ? 0 : mold.Warning2 - produced).ToString();

                tdAlarm.InnerText = mold.Maximum.ToString();
                NextMaintenanceRemainingShotsField.InnerText = left.ToString();
                labelAlarm.Attributes["class"] = "milestones milestone__100";
                labelAlarm.InnerText = left.ToString();
                NextMaintenanceCounter.InnerText = (mold.LastMaintenanceProgressiveCount + mold.Maximum).ToString();

                if (cadency > 0)
                {
                    TimeSpan time;
                    time = TimeSpan.FromHours((double)((mold.Warning1 - produced) / cadency));
                    labelTimeWarning1.InnerText = string.Format("{0:00}:{1:00}", time.Ticks < 0 ? 0 : (int)time.TotalHours, time.Ticks < 0 ? 0 : time.Minutes);
                    tdWarning1Time.InnerText = string.Format("{0:00}:{1:00}", time.Ticks < 0 ? 0 : (int)time.TotalHours, time.Ticks < 0 ? 0 : time.Minutes);//TimeSpan.FromHours((double)((mold.Warning1 - produced) / cadency)).ToString(@"hh\:mm");
                    time = TimeSpan.FromHours((double)((mold.Warning2 - produced) / cadency));
                    labelTimeWarning2.InnerText = string.Format("{0:00}:{1:00}", time.Ticks < 0 ? 0 : (int)time.TotalHours, time.Ticks < 0 ? 0 : time.Minutes);
                    tdWarning2Time.InnerText = string.Format("{0:00}:{1:00}", time.Ticks < 0 ? 0 : (int)time.TotalHours, time.Ticks < 0 ? 0 : time.Minutes);//TimeSpan.FromHours((double)((mold.Warning2 - produced) / cadency)).ToString(@"hh\:mm");
                    time = TimeSpan.FromHours((double)(left / cadency));
                    labelTimeAlarm.InnerText = string.Format("{0:00}:{1:00}", time.Ticks < 0 ? 0 : (int)time.TotalHours, time.Ticks < 0 ? 0 : time.Minutes);
                    NextMaintenanceTime.InnerText = string.Format("{0:00}:{1:00}", (int)time.TotalHours, time.Minutes); //TimeSpan.FromHours((double)(left / cadency)).ToString(@"hh\:mm");
                }
            }
        }

        protected void DoMaintenance(Mold mold, TransactionMold lastT)
        {
            MoldRepository repos = new MoldRepository();
            mold.LastMaintenanceDate = DateTime.Now;
            mold.LastMaintenanceProgressiveCount = mold.LastMaintenanceProgressiveCount + mold.Maximum;

            repos.Update(mold);
            repos.SaveChanges();

            MoldMaintenanceRepository repo = new MoldMaintenanceRepository();
            MoldMaintenance m = new MoldMaintenance() { MoldId = lastT.MoldId, CauseId = lastT.CauseId, Date = DateTime.Now, OperatorId = lastT.OperatorId, ProgressiveCounter = mold.LastMaintenanceProgressiveCount, ApplicationId = MultiTenantsHelper.ApplicationId, Duration = 60 };
            repo.Insert(m);
            repo.SaveChanges();
        }

        protected void Timer_Tick(object sender, EventArgs e)
        {
        }
    }
}