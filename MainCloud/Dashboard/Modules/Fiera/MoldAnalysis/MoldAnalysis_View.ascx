﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoldAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Fiera.MoldAnalysis.MoldAnalysis_View" %>

<link href="/Dashboard/Modules/Fiera/MoldAnalysis/Style.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_beginRequest(beginRequest);

    function beginRequest() {
        prm._scrollPosition = null;
    }
</script>

<%--<div class="row">--%>
        <%--<telerik:RadDropDownList RenderMode="Lightweight" runat="server" ID="ddlSelectMold" DataSourceID="edsMold" Width="310px"
            DataTextField="Name" DataValueField="Id" DropDownWidth="310px" DropDownHeight="300px">
            <ItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 25%">Name:
                                    </td>
                                    <td style="width: 70%">
                                        <%# DataBinder.Eval(Container.DataItem, "Name")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Impronte:
                                    </td>
                                    <td>
                                        <%# DataBinder.Eval(Container.DataItem, "CavityMoldNum")%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right" style="width: 25%; padding-left: 10px;">
                            <asp:Image ImageUrl='<%# Eval("PathImg")%>' runat="server" Width="90px" Height="110px" />
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </telerik:RadDropDownList>--%>
    <%--<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <asp:UpdatePanel runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer" />
            </Triggers>
            <ContentTemplate>
                <telerik:RadNumericTextBox runat="server" ID="ProgressiveCounter" Width="100%" Label="Progressivo:" LabelWidth="20%"
                    style="background-color:rgb(145, 220, 243);text-align:right;" ReadOnly="true" />
                <asp:Timer runat="server" ID="Timer" OnTick="Timer_Tick" Interval="1"></asp:Timer>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>--%>
    <%--<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <asp:Label Text="Efficienza" runat="server" />
        <telerik:RadRadialGauge runat="server" ID="MeterEfficiency" Width="100%">
            <Pointer Value="91">
                <Cap Size="0.1" />
            </Pointer>
            <Scale Min="0" Max="100" MajorUnit="20">
                <Labels Format="{0}" />
                <Ranges>
                    <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                    <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                    <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                    <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                </Ranges>
            </Scale>
        </telerik:RadRadialGauge>
    </div>--%>
<%--</div>--%>

<br />

<asp:UpdatePanel runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Timer" EventName="Tick" />
        </Triggers>
        <ContentTemplate>
            <asp:Timer runat="server" ID="Timer" OnTick="Timer_Tick" Interval="1000"></asp:Timer>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="vefs-milestone-wrapper">
                        <div class="milestone-container">
                             <div class="label-container" style="margin-bottom:7px;">
                                <%--<div class="milestones milestone__10">
                                    <div class="label colored">10%</div>
                                </div>
                                <div class="milestones milestone__27">
                                    <div class="label colored">25%</div>
                                </div>--%>
                                <div runat="server" id="labelTimeWarning1">
                                    <div class="label colored"></div>
                                </div>
                                <div runat="server" id="labelTimeWarning2">
                                    <div class="label"></div>
                                </div>
                                <div runat="server" id="labelTimeAlarm" class="milestones milestone__100">
                                </div>
                            </div>
                            <div class="chart-container">
                                <div class="line-container">
                                    <div class="line"></div>
                                    <div runat="server" id="progressiveBar" class="progress-bar progress-bar-striped active line left fontBar" style="width: 67%;"></div>
                                </div>
                                <div class="dot-container">
                                    <%--<div class="milestones milestone__10">
                                        <div class="dot completed colored"></div>
                                    </div>
                                    <div class="milestones milestone__27">
                                        <div class="dot completed colored"></div>
                                    </div>--%>
                                    <div runat="server" id="dotContainerWarning1" class="milestones milestone__50">
                                        <div runat="server" id="dotWarning1" class="dot completed colored"></div>
                                    </div>
                                    <div runat="server" id="dotContainerWarning2" class="milestones milestone__80">
                                        <div runat="server" id="dotWarning2" class="dot"></div>
                                    </div>
                                    <div class="milestones milestone__100">
                                        <div class="dot colored"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="label-container" style="margin-bottom:7px;">
                                <%--<div class="milestones milestone__10">
                                    <div class="label colored">10%</div>
                                </div>
                                <div class="milestones milestone__27">
                                    <div class="label colored">25%</div>
                                </div>--%>
                                <div runat="server" id="labelWarning1" class="milestones milestone__50">
                                    <div class="label colored"></div>
                                </div>
                                <div runat="server" id="labelWarning2" class="milestones milestone__80">
                                    <div class="label"></div>
                                </div>
                                <div runat="server" id="labelAlarm" class="milestones milestone__100">
                                    <%--<div class="label">100%</div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--%>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default">
                        <table class="table table-bordered table-condensed info" style="width:100%;">
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="background-color:#428bca; color:white; text-align:center"><b>Progressivo</b></td>
                                <td style="background-color:#428bca; color:white; text-align:center"><b>Pz Mancanti</b></td>
                                <td style="background-color:#428bca; color:white; text-align:center"><b>Tempo</b></td>
                                <td style="background-color:#428bca; color:white; text-align:center"><b>Data</b></td>
                            </tr>
                            <tr>
                                <td><b>Ultima Manutenzione</b></td>
                                <td></td>
                                <td runat="server" id="LastMaintenanceCounter" style="text-align: right"></td>
                                <td></td>
                                <td></td>
                                <td runat="server" id="TotalShotsLastDateField"></td>
                            </tr>
                            <tr>
                                <td><b>Prodotti</b></td>
                                <td runat="server" id="tdProduced" style="text-align: right"></td>
                                <td runat="server" id="tdProducedCounter" style="text-align: right"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Warning1</b></td>
                                <td runat="server" id="tdWarning1" style="text-align: right"></td>
                                <td runat="server" id="tdWarning1Counter" style="text-align: right"></td>
                                <td runat="server" id="tdWarning1Left" style="text-align: right"></td>
                                <td runat="server" id="tdWarning1Time" style="text-align: right"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Warning2</b></td>
                                <td runat="server" id="tdWarning2" style="text-align: right"></td>
                                <td runat="server" id="tdWarning2Counter" style="text-align: right"></td>
                                <td runat="server" id="tdWarning2Left" style="text-align: right"></td>
                                <td runat="server" id="tdWarning2Time" style="text-align: right"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Prossima manutenzione</b></td>
                                <td runat="server" id="tdAlarm" style="text-align: right"></td>
                                <td runat="server" id="NextMaintenanceCounter" style="text-align: right"></td>
                                <td runat="server" id="NextMaintenanceRemainingShotsField" style="text-align: right"></td>
                                <td runat="server" id="NextMaintenanceTime" style="text-align: right"></td>
                                <td runat="server" id="NextMaintenanceTimeDate" style="text-align: right"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    OrderBy="it.Name" />