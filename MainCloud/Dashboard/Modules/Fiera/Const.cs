﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.Fiera
{
    public class Const
    {
        public const string MACHINE_ID_KEY = "machineIdKey";
        public const string MOLD_ID_KEY = "moldIdKey";
        public const string TRANSACTION_ID_KEY = "transactionIdKey";

        public const string MACHINE_ID = "machineId";
        public const string MOLD_ID = "moldId";
        public const string TRANSACTION_ID = "transactionId";
    }
}