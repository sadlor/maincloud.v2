﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using MoldID.Core;
using MoldID.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.Fiera.Efficiency
{
    public partial class Efficiency_View : DataWidget<Efficiency_View>
    {
        public Efficiency_View() : base(typeof(Efficiency_View)) {}

        protected string AssetId
        {
            get
            {
                if (string.IsNullOrEmpty((string)ViewState[this.ID + Const.MACHINE_ID]))
                {
                    return null;
                }
                return ViewState[this.ID + Const.MACHINE_ID].ToString();
            }
            set
            {
                ViewState[this.ID + Const.MACHINE_ID] = value;
            }
        }

        protected int? TransactionId
        {
            get
            {
                if (ViewState[this.ID + Const.TRANSACTION_ID] == null)
                {
                    return null;
                }
                return Convert.ToInt32(ViewState[this.ID + Const.TRANSACTION_ID]);
            }
            set
            {
                ViewState[this.ID + Const.TRANSACTION_ID] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionMultitenant.ContainsKey(Const.MACHINE_ID_KEY) && !string.IsNullOrEmpty(SessionMultitenant[Const.MACHINE_ID_KEY] as string))
            {
                AssetId = SessionMultitenant[Const.MACHINE_ID_KEY].ToString();
            }
            if (SessionMultitenant.ContainsKey(Const.TRANSACTION_ID_KEY) && !string.IsNullOrEmpty(SessionMultitenant[Const.TRANSACTION_ID_KEY].ToString()))
            {
                TransactionId = Convert.ToInt32(SessionMultitenant[Const.TRANSACTION_ID_KEY]);
            }
            UpdateEfficiency();
        }

        protected void UpdateEfficiency()
        {
            if (!string.IsNullOrEmpty(AssetId))
            {
                TransactionService transService = new TransactionService();
                IndexService indexService = new IndexService();
                TransactionRepository TRep = new TransactionRepository();
                JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

                Job job;
                //if (TransactionId.HasValue)
                //{
                //    job = TRep.FindByID(TransactionId.Value)?.Job;
                //}
                //else
                //{
                    job = transService.GetLastTransactionOpen(AssetId).Job;
                //}

                List<Transaction> tList = new List<Transaction>();

                decimal oreProdTeorico = 0;
                decimal efficiency = 0;

                if (job != null)
                {
                    tList = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id).ToList();
                    TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));

                    if (productionTime.TotalSeconds > 0)
                    {
                        if (job.StandardRate > 0)
                        {
                            oreProdTeorico += Math.Round(tList.Sum(x => x.PartialCounting) / job.StandardRate, 2);
                        }
                    }

                    efficiency = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
                }

                MeterEfficiency.Pointer.Value = efficiency;
            }
        }
    }
}