﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Efficiency_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Fiera.Efficiency.Efficiency_View" %>

<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <%--<Triggers>
        <asp:AsyncPostBackTrigger ControlID="Timer" />
    </Triggers>
        <asp:Timer runat="server" ID="Timer" OnTick="Timer_Tick" Interval="20"></asp:Timer>--%>
    <ContentTemplate>
        <%--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">--%>
            <center>
            <telerik:RadRadialGauge runat="server" ID="MeterEfficiency" Width="100%">
                <Pointer Value="0">
                    <Cap Size="0.1" />
                </Pointer>
                <Scale Min="0" Max="100" MajorUnit="20">
                    <Labels Format="{0}" />
                    <Ranges>
                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                    </Ranges>
                </Scale>
            </telerik:RadRadialGauge>
            </center>
        <%--</div>--%>
    </ContentTemplate>
</asp:UpdatePanel>