﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BoxList_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.BoxList.BoxList_View" %>

<asp:UpdatePanel ID="pnlTree" runat="server">
    <ContentTemplate>
        <%--<div class="row">
            <div class="col-xs-6">--%>
                <telerik:RadTreeView runat="Server" ID="BoxTreeView" EnableViewState="true" EnableDragAndDrop="true" EnableDragAndDropBetweenNodes="false"
                    OnNodeClick="BoxTreeView_NodeClick" OnClientMouseOver="TreeMouseOver">
                    </telerik:RadTreeView>
            <%--</div>
            <div class="col-xs-6">
                <telerik:RadTreeView runat="Server" ID="AssetImageTreeView" EnableViewState="true" EnableDragAndDrop="true" EnableDragAndDropBetweenNodes="false"
                    OnNodeClick="AssetTreeView_NodeClick" ShowLineImages="false" OnClientMouseOver="TreeMouseOver" />
            </div>
        </div>--%>

        <asp:Timer ID="timer" runat="server" Interval="25000" OnTick="timer_Tick"></asp:Timer>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="BoxTreeView" />
    </Triggers>
</asp:UpdatePanel>

<script type="text/javascript">

    var lastNodeSelected_<%=BoxTreeView.ClientID %> = null;

    function TreeMouseOver(sender, eventArgs) {
        var node = eventArgs.get_node();
        if (lastNodeSelected_<%=BoxTreeView.ClientID %> != node.get_value()) {
            lastNodeSelected_<%=BoxTreeView.ClientID %> = node.get_value();
            __doPostBack('<%=BoxTreeView.ClientID %>', node.get_value()); //"wp429903510"
        }
    }

    function ExpandCollapse(button)
    {
        if (button.Text == "Expand all")
        {
            treeExpandAllNodes();
            button.Text = "Collapse all";
            button.switchClass("fa-chevron-down", "fa-chevron-up", 1000);
        }
        else
        {
            treeCollapseAllNodes();
        }
    }

    function treeExpandAllNodes() {
        var treeView = $find("<%= BoxTreeView.ClientID %>");
        var nodes = treeView.get_allNodes();
        for (var i = 0; i < nodes.length; i++) {

            if (nodes[i].get_nodes() != null) {
                nodes[i].expand();
            }
        }
    }

    function treeCollapseAllNodes() {
        var treeView = $find("<%= BoxTreeView.ClientID %>");
        var nodes = treeView.get_allNodes();
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].get_nodes() != null) {
                nodes[i].collapse();
            }
        }
    }

</script>