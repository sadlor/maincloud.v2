﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using Newtonsoft.Json;
using SenecaModule.Models;
using SenecaModule.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.BoxList
{
    public partial class BoxList_View : DataWidget<BoxList_View>
    {
        public BoxList_View() : base(typeof(BoxList_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadBoxTreeView();
            }
        }

        protected void LoadBoxTreeView()
        {
            this.BoxTreeView.Nodes.Clear();

            BoxService service = new BoxService();
            var boxes = service.GetBoxListByApplicationId(MultiTenantsHelper.ApplicationId);
            foreach (Box b in boxes.OrderBy(x => x.SerialNumber))
            {
                //Node Box
                RadTreeNode nodeB = new RadTreeNode(b.SerialNumber);
                //if (string.IsNullOrEmpty(b.AnalyzerId))
                //{
                    nodeB.PostBack = false;
                //    nodeB.ForeColor = Color.Red;
                //}
                //else
                //{
                //    nodeB.PostBack = true;
                //}
                //nodeB.Value = JsonConvert.SerializeObject(new SelectedContext(p.Id, AssetManagementEnum.AnalyzerContext.Plant));
                nodeB.Expanded = true;
                nodeB.Width = Unit.Percentage(66);
                nodeB.Height = Unit.Percentage(30);
                foreach (Gateway g in b.Gateway.OrderBy(x => x.SerialNumber))
                {
                    //Node Gateway
                    RadTreeNode nodeG = new RadTreeNode(g.Model + " - " + g.NetworkAddress);
                    //if (VerifyConnectivity(g.NetworkAddress).Result)
                    if (VerifyConnectivity(g.NetworkAddress))
                    {
                        nodeG.Text += " <i class='fa fa-circle' style='color: green'></i>";
                    }
                    else
                    {
                        nodeG.Text += " <i class='fa fa-circle' style='color: red'></i>";
                    }
                    //if (string.IsNullOrEmpty(d.AnalyzerId))
                    //{
                        nodeG.PostBack = false;
                    //    nodeG.ForeColor = Color.Red;
                    //}
                    //else
                    //{
                    //    nodeG.PostBack = true;
                    //}
                    //nodeG.Value = JsonConvert.SerializeObject(new SelectedContext(d.Id, AssetManagementEnum.AnalyzerContext.Department));
                    nodeG.Expanded = false;
                    nodeG.Width = Unit.Percentage(66);
                    nodeG.Height = Unit.Percentage(30);
                    foreach (Device d in g.DeviceList.OrderBy(x => x.ConnectedNumber))
                    {
                        //Node Device
                        RadTreeNode nodeD = new RadTreeNode(d.Model + " - " + d.SerialNumber);
                        //if (string.IsNullOrEmpty(d.AnalyzerId))
                        //{
                            nodeD.PostBack = false;
                        //    nodeD.ForeColor = Color.Red;
                        //}
                        //else
                        //{
                        //    nodeD.PostBack = true;
                        //}
                        //nodeD.Value = JsonConvert.SerializeObject(new SelectedContext(a.Id, AssetManagementEnum.AnalyzerContext.Asset));
                        nodeD.Expanded = true;
                        nodeD.Width = Unit.Percentage(66);
                        nodeD.Height = Unit.Percentage(30);

                        AssetManagement.Repositories.AssetRepository assetRepos = new AssetManagement.Repositories.AssetRepository();
                        foreach (ConnectedMachine cm in d.ConnectedMachineList.OrderBy(x => x.ValuePin))
                        {
                            //Node ConnectedMachine
                            RadTreeNode nodeCM = new RadTreeNode(assetRepos.FindByID(cm.AssetId).Description);
                            nodeCM.PostBack = false;
                            nodeCM.Expanded = true;
                            nodeCM.Width = Unit.Percentage(66);
                            nodeCM.Height = Unit.Percentage(30);

                            nodeD.Nodes.Add(nodeCM);
                        }

                        nodeG.Nodes.Add(nodeD);
                    }
                    nodeB.Nodes.Add(nodeG);
                }
                BoxTreeView.Nodes.Add(nodeB);
            }
        }

        protected void BoxTreeView_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            if (("" + CustomSettings.Find("Behaviour")).Trim().ToLower() == "redirect")
            {
                //Pagina di selezione asset, salvo in sessione e vado in real time
                //SessionMultitenant[EMConstants.SELECTED_ID_CONTEXT] = e.Node.Value;
                //SessionMultitenant[EMConstants.START_ACTIVEENERGY_COUNTER] = null;
                //SessionMultitenant[EMConstants.START_REACTIVEENERGY_COUNTER] = null;

                //string returnUrl = Request["returnurl"];
                //if (string.IsNullOrEmpty(returnUrl))
                //{
                //    string destinationAreaId = CustomSettings.Find("settings-destination-area-id") as string;
                //    if (!string.IsNullOrEmpty(destinationAreaId))
                //    {
                //        WidgetAreaRepository areaService = new WidgetAreaRepository();
                //        WidgetArea wa = areaService.FindByID(destinationAreaId);

                //        Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
                //    }
                    //Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl("RealTime"));
                    // eliminare se funziona la riga sopra -- Response.Redirect("~/App/" + MultiTenantsHelper.ApplicationName + "/Area/RealTime");
                //}
                //else
                //{
                //    Response.Redirect("~" + returnUrl);
                //}
            }
        }

        //static async Task<bool> VerifyConnectivity(string ip)
        protected bool VerifyConnectivity(string ip)
        {
            return false;
            //WebRequest request = WebRequest.Create(string.Format("http://{0}/maintenance/index.html", ip));
            //request.Timeout = 1000;
            //try
            //{
            //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //    if (response == null || response.StatusCode != HttpStatusCode.OK)
            //    {
            //        return false;
            //    }
            //    else
            //    {
            //        return true;
            //    }
            //}
            //catch (Exception)
            //{
            //    return false;
            //}
            //    using (var client = new HttpClient())
            //{
            //    client.Timeout = TimeSpan.FromSeconds(1);
            //    try
            //    {
            //        HttpResponseMessage response = await client.GetAsync(string.Format("http://{0}/maintenance/index.html", ip));
            //        return response.IsSuccessStatusCode;
            //    }
            //    catch (Exception)
            //    {
            //        return false;
            //    }
            //}
        }

        protected void timer_Tick(object sender, EventArgs e)
        {
            LoadBoxTreeView();
        }
    }
}