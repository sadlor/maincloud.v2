﻿using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using SenecaModule.Models;
using SenecaModule.Repositories;
using SenecaModule.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.BoxList
{
    public partial class BoxList_Edit : WidgetControl<object>
    {
        public BoxList_Edit() : base(typeof(BoxList_Edit)) { }

        //private DataTable deviceAvailableList;
        //private DataTable groupTypeDevice;
        //private DataTable devices;

        //protected override void OnInit(EventArgs e)
        //{
        //    //added to prevent incorrect reorder (e.g if it is navigated to another page and returns with back button - IE issue)
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);

        //    base.OnInit(e);
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            edsBox.WhereParameters.Clear();
            edsBox.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsGateway.WhereParameters.Clear();
            edsGateway.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsDevice.WhereParameters.Clear();
            edsDevice.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            //edsConnectedMachine.WhereParameters.Clear();
            //edsConnectedMachine.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsAsset.WhereParameters.Clear();
            edsAsset.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        #region grafica drag and drop
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);
        //    manager.AjaxRequest += new RadAjaxControl.AjaxRequestDelegate(manager_AjaxRequest);

        //    if (!IsPostBack)
        //    {
        //        //avoiding sharing Session variables between tabs in one browser
        //        SessionID.Value = Guid.NewGuid().ToString();
        //    }

        //    if (Session[SessionID.Value + "GroupTypeDeviceEnabledBindingCS"] == null || Session[SessionID.Value + "DevicesGroupEnabledBindingCS"] == null ||
        //        Session[SessionID.Value + "DeviceAvailableList"] == null || !IsPostBack)
        //    {
        //        CreateGroupType();
        //        CreateDevices();
        //        CreateDeviceAvailableInListBox();
        //    }
        //    else
        //    {
        //        groupTypeDevice = Session[SessionID.Value + "GroupTypeDeviceEnabledBindingCS"] as DataTable;
        //        devices = Session[SessionID.Value + "DevicesGroupEnabledBindingCS"] as DataTable;
        //        deviceAvailableList = Session[SessionID.Value + "DeviceAvailableList"] as DataTable;
        //    }

        //    CreateDiagram();

        //    RadOrgChart1.NodeDrop += new Telerik.Web.UI.OrgChartNodeDropEventHandler(RadOrgChart1_NodeDrop);
        //    RadOrgChart1.GroupItemDrop += new Telerik.Web.UI.OrgChartGroupItemDropEventHandler(RadOrgChart1_GroupItemDrop);

        //    RadOrgChart1.GroupEnabledBinding.NodeBindingSettings.DataFieldID = "GroupTypeId";
        //    RadOrgChart1.GroupEnabledBinding.NodeBindingSettings.DataFieldParentID = "ParentId";
        //    RadOrgChart1.RenderedFields.NodeFields.Add(new Telerik.Web.UI.OrgChartRenderedField() { DataField = "Name" });
        //    RadOrgChart1.GroupEnabledBinding.NodeBindingSettings.DataSource = groupTypeDevice;

        //    RadOrgChart1.GroupEnabledBinding.GroupItemBindingSettings.DataFieldID = "DeviceId";
        //    RadOrgChart1.GroupEnabledBinding.GroupItemBindingSettings.DataFieldNodeID = "GroupTypeId";
        //    RadOrgChart1.GroupEnabledBinding.GroupItemBindingSettings.DataTextField = "Name";
        //    RadOrgChart1.GroupEnabledBinding.GroupItemBindingSettings.DataImageUrlField = "ImageUrl";
        //    RadOrgChart1.GroupEnabledBinding.GroupItemBindingSettings.DataSource = devices;

        //    RadOrgChart1.DataBind();

        //    RadListBox1.DataSource = deviceAvailableList;
        //    //manually databind so we can sort the items
        //    RadListBox1.DataBind();
        //    //sort the items
        //    RadListBox1.SortItems();
        //}

        //private void CreateDeviceAvailableInListBox()
        //{
        //    // create device available in listbox for drag & drop

        //    deviceAvailableList = new DataTable();
        //    deviceAvailableList.Columns.Add("Name");      //modello
        //    deviceAvailableList.Columns.Add("ImageUrl");  //immagine

        //    deviceAvailableList.Rows.Add(new string[] { "Z-10-DIN", "/Dashboard/Modules/Seneca/BoxView/z-10-d-in.jpg" });
        //    deviceAvailableList.Rows.Add(new string[] { "Z-KEY", "/Dashboard/Modules/Seneca/BoxView/z-key.jpg" });

        //    Session[SessionID.Value + "DeviceAvailableList"] = deviceAvailableList;
        //}

        //private void CreateGroupType()
        //{
        //    groupTypeDevice = new DataTable();
        //    groupTypeDevice.Columns.Add("GroupTypeId");
        //    groupTypeDevice.Columns.Add("ParentId");
        //    groupTypeDevice.Columns.Add("Name");

        //    groupTypeDevice.Rows.Add(new string[] { "Box", null, "Box" });
        //    groupTypeDevice.Rows.Add(new string[] { "Gateway", "Box", "Gateway" });
        //    groupTypeDevice.Rows.Add(new string[] { "Device", "Gateway", "Devices" });

        //    Session[SessionID.Value + "GroupTypeDeviceEnabledBindingCS"] = groupTypeDevice;
        //}

        //private void CreateDevices()
        //{
        //    devices = new DataTable();
        //    devices.Columns.Add("DeviceId");
        //    devices.Columns.Add("GroupTypeId");
        //    devices.Columns.Add("ParentId");
        //    devices.Columns.Add("Name");
        //    devices.Columns.Add("SerialNumber");
        //    devices.Columns.Add("ImageUrl");

        //    //devices.Rows.Add(new string[] { "1", "1", "Kevin Buchanan", "images/Northwind/Customers/ERNSH.jpg" });

        //    Session[SessionID.Value + "DevicesGroupEnabledBindingCS"] = devices;

        //    //edsDevice.WhereParameters.Clear();
        //    //edsDevice.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        //    //edsDevice.WhereParameters.Add("BoxId", "");

        //}

        //private void CreateDiagram()
        //{
        //    BoxService boxService = new BoxService();
        //    var boxDataSource = boxService.GetBoxListByApplicationId(MultiTenantsHelper.ApplicationId);

        //    foreach (SenecaModule.Models.Box box in boxDataSource)
        //    {
        //        SenecaModule.Models.Gateway gtw = box.Gateway;
        //        devices.Rows.Add(new string[] { gtw.Id, "Gateway", "Box", "Z-KEY", gtw.SerialNumber, "" });
        //        foreach (SenecaModule.Models.Device dev in gtw.DeviceList)
        //        {
        //            devices.Rows.Add(new string[] { dev.Id, "Device", "Gateway", dev.Model, dev.SerialNumber, "" });
        //        }
        //    }
        //}

        //void RadOrgChart1_GroupItemDrop(object sender, Telerik.Web.UI.OrgChartGroupItemDropEventArguments e)
        //{
        //    var rows = from myRow in devices.AsEnumerable() where myRow.Field<string>("DeviceId") == e.SourceGroupItem.ID select new { values = myRow };
        //    foreach (var row in rows)
        //    {
        //        row.values.SetField<string>("GroupTypeId", e.DestinationNode.ID);
        //    }
        //    Session[SessionID.Value + "DevicesGroupEnabledBindingCS"] = devices;
        //    RadOrgChart1.DataBind();
        //}

        //void RadOrgChart1_NodeDrop(object sender, Telerik.Web.UI.OrgChartNodeDropEventArguments e)
        //{
        //    var rows = from myRow in groupTypeDevice.AsEnumerable() where myRow.Field<string>("GroupTypeId") == e.SourceNode.ID select new { values = myRow };
        //    foreach (var row in rows)
        //    {
        //        row.values.SetField<string>("ParentId", e.DestinationNode.ID);
        //    }
        //    Session[SessionID.Value + "GroupTypeDeviceEnabledBindingCS"] = groupTypeDevice;
        //    RadOrgChart1.DataBind();
        //}

        //protected void manager_AjaxRequest(object sender, Telerik.Web.UI.AjaxRequestEventArgs e)
        //{
        //    char[] seps = { '_' };
        //    string[] textAndIds = e.Argument.Split(seps, StringSplitOptions.None);

        //    char[] sep = { ':' };
        //    string[] stringIds = textAndIds[2].Split(sep, StringSplitOptions.RemoveEmptyEntries);
        //    int[] ids = new int[stringIds.Length];
        //    for (int i = 0; i < stringIds.Length; i++)
        //    {
        //        ids[i] = Int32.Parse(stringIds[i]);
        //    }

        //    if (textAndIds[0] == "drop")
        //    {
        //        string newText = textAndIds[1];

        //        OrgChartNode node = RadOrgChart1.GetNodeByHierarchicalIndex(ids);

        //        var rows = from myRow in deviceAvailableList.AsEnumerable() where myRow.Field<string>("Name") == newText select new { values = myRow };
        //        string imageUrl = "";
        //        string deviceId = "";
        //        foreach (var row in rows)
        //        {
        //            imageUrl = row.values.Field<string>("ImageUrl");
        //            deviceId = row.values.Field<string>("Name");
        //            row.values.Delete();
        //            break;
        //        }
        //        devices.Rows.Add(new string[] { deviceId, node.ID, newText, imageUrl });
        //    }
        //    else if (textAndIds[0] == "remove")
        //    {
        //        int groupIndex = Int32.Parse(textAndIds[1]);

        //        OrgChartNode node = (OrgChartNode)RadOrgChart1.GetNodeByHierarchicalIndex(ids);
        //        OrgChartGroupItem groupItem = node.GroupItems[groupIndex];
        //        var rows = from myRow in devices.AsEnumerable() where myRow.Field<string>("Name") == groupItem.Text select new { values = myRow };
        //        string imageUrl = "";

        //        foreach (var row in rows)
        //        {
        //            imageUrl = row.values.Field<string>("ImageUrl");
        //            row.values.Delete();
        //            break;
        //        }

        //        deviceAvailableList.Rows.Add(new string[] { groupItem.ID, groupItem.Text, imageUrl });
        //    }

        //    Session[SessionID.Value + "DevicesGroupEnabledBindingCS"] = devices;
        //    Session[SessionID.Value + "DeviceAvailableList"] = deviceAvailableList;
        //    RadOrgChart1.DataBind();
        //    RadListBox1.DataBind();
        //    RadListBox1.SortItems();
        //}

        #endregion


        #region InsertCommand
        protected void gridBox_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var box = new Box();
            box.ApplicationId = MultiTenantsHelper.ApplicationId;
            box.SerialNumber = (string)values["SerialNumber"];
            box.Supplier = (string)values["Supplier"];
            if (!string.IsNullOrEmpty((string)values["InstallationDate"]))
            {
                box.InstallationDate = Convert.ToDateTime(values["InstallationDate"]);
            }

            //var img = editableItem["Image"].FindControl("ImageManager");
            //string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
            //if (!string.IsNullOrEmpty(pathImg))
            //{
            //    box.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
            //}

            BoxRepository rep = new BoxRepository();
            rep.Insert(box);
            rep.SaveChanges();
        }

        protected void gridGateway_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var gateway = new Gateway();
            gateway.ApplicationId = MultiTenantsHelper.ApplicationId;
            gateway.SerialNumber = (string)values["SerialNumber"];
            gateway.Model = (string)values["Model"];
            gateway.Supplier = (string)values["Supplier"];
            gateway.NetworkAddress = (string)values["NetworkAddress"];
            gateway.BoxId = (string)values["BoxId"];
            gateway.Active = Convert.ToBoolean(values["Active"]);
            if (!string.IsNullOrEmpty((string)values["InstallationDate"]))
            {
                gateway.InstallationDate = Convert.ToDateTime(values["InstallationDate"]);
            }

            GatewayRepository rep = new GatewayRepository();
            rep.Insert(gateway);
            rep.SaveChanges();
        }

        protected void gridDevice_InsertCommand(object sender, GridCommandEventArgs e)
        {
            DeviceRepository rep = new DeviceRepository();

            switch (e.Item.OwnerTableView.Name)
            {
                case "Device":
                    {
                        //GridDataItem parentItem = (GridDataItem)e.Item.OwnerTableView.ParentItem;
                        var editableItem = ((GridEditableItem)e.Item);
                        Hashtable values = new Hashtable();
                        editableItem.ExtractValues(values);

                        var device = new Device();
                        device.ApplicationId = MultiTenantsHelper.ApplicationId;
                        device.SerialNumber = (string)values["SerialNumber"];
                        device.Model = (string)values["Model"];
                        device.Supplier = (string)values["Supplier"];
                        device.NetworkAddress = (string)values["NetworkAddress"];
                        device.GatewayId = (string)values["GatewayId"];
                        device.ConnectedNumber = Convert.ToInt32(values["ConnectedNumber"]);
                        //device.BoxId = (string)values["BoxId"];
                        if (!string.IsNullOrEmpty((string)values["InstallationDate"]))
                        {
                            device.InstallationDate = Convert.ToDateTime(values["InstallationDate"]);
                        }

                        rep.Insert(device);
                        rep.SaveChanges();
                    }
                    break;
                case "ConnectedMachine":
                    {
                        GridDataItem parentItem = (GridDataItem)e.Item.OwnerTableView.ParentItem;
                        string deviceId = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["Id"].ToString();

                        var editableItem = ((GridEditableItem)e.Item);
                        Hashtable values = new Hashtable();
                        editableItem.ExtractValues(values);

                        var device = rep.FindByID(deviceId);
                        ConnectedMachine cm = new ConnectedMachine();
                        cm.DeviceId = deviceId;
                        cm.AssetId = (string)values["AssetId"];
                        cm.ValuePin = Convert.ToInt32(values["ValuePin"]);
                        cm.StatusPin = Convert.ToInt32(values["StatusPin"]);
                        device.ConnectedMachineList.Add(cm);
                        rep.Update(device);
                        rep.SaveChanges();
                    }
                    break;
            }
        }
        #endregion

        #region UpdateCommand
        protected void gridBox_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            BoxRepository rep = new BoxRepository();

            var editableItem = ((GridEditableItem)e.Item);
            var boxId = (string)editableItem.GetDataKeyValue("Id");

            var box = rep.FindByID(boxId);
            if (box != null)
            {
                editableItem.UpdateValues(box);

                //var img = editableItem["Image"].FindControl("ImageManager");
                //string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                //if (!string.IsNullOrEmpty(pathImg))
                //{
                //    box.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                //}

                rep.Update(box);
                rep.SaveChanges();
            }
        }

        protected void gridGateway_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            GatewayRepository rep = new GatewayRepository();

            var editableItem = ((GridEditableItem)e.Item);
            var gatewayId = (string)editableItem.GetDataKeyValue("Id");

            var gateway = rep.FindByID(gatewayId);
            if (gateway != null)
            {
                editableItem.UpdateValues(gateway);

                //if (gateway.BoxId == string.Empty)
                //{
                //    gateway.BoxId = null;
                //}

                rep.Update(gateway);
                rep.SaveChanges();
            }
        }

        protected void gridDevice_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.Item.OwnerTableView.Name)
            {
                case "Device":
                    {
                        DeviceRepository rep = new DeviceRepository();
                        var editableItem = ((GridEditableItem)e.Item);
                        var deviceId = (string)editableItem.GetDataKeyValue("Id");

                        var device = rep.FindByID(deviceId);
                        if (device != null)
                        {
                            editableItem.UpdateValues(device);

                            rep.Update(device);
                            rep.SaveChanges();
                        }
                    }
                    break;
                case "ConnectedMachine":
                    {
                        ConnectedMachineRepository rep = new ConnectedMachineRepository();
                        var editableItem = ((GridEditableItem)e.Item);
                        var cmId = (int)editableItem.GetDataKeyValue("Id");

                        var cm = rep.FindByID(cmId);
                        if (cm != null)
                        {
                            editableItem.UpdateValues(cm);

                            rep.Update(cm);
                            rep.SaveChanges();
                        }
                    }
                    break;
            }
        }
        #endregion

        #region DeleteCommand
        protected void gridBox_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            BoxRepository rep = new BoxRepository();

            var boxId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");
            var box = rep.FindByID(boxId);

            if (box != null)
            {
                rep.Delete(box);
                try
                {
                    rep.SaveChanges();
                }
                catch (Exception)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }

        protected void gridGateway_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GatewayRepository rep = new GatewayRepository();

            var gtwId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");
            var gtw = rep.FindByID(gtwId);

            if (gtw != null)
            {
                rep.Delete(gtw);
                try
                {
                    rep.SaveChanges();
                }
                catch (Exception)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }

        protected void gridDevice_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.Item.OwnerTableView.Name)
            {
                case "Device":
                    {
                        DeviceRepository rep = new DeviceRepository();

                        var deviceId = (string)((GridDataItem)e.Item).GetDataKeyValue("Id");
                        var device = rep.FindByID(deviceId);

                        if (device != null)
                        {
                            rep.Delete(device);
                            try
                            {
                                rep.SaveChanges();
                            }
                            catch (Exception)
                            {
                                messaggio.Text = "Impossibile rimuovere il record";
                                dlgWidgetConfig.OpenDialog();
                            }
                        }
                    }
                    break;
                case "ConnectedMachine":
                    {
                        ConnectedMachineRepository rep = new ConnectedMachineRepository();

                        var cmId = (int)((GridDataItem)e.Item).GetDataKeyValue("Id");
                        var cm = rep.FindByID(cmId);

                        if (cm != null)
                        {
                            rep.Delete(cm);
                            try
                            {
                                rep.SaveChanges();
                            }
                            catch (Exception)
                            {
                                messaggio.Text = "Impossibile rimuovere il record";
                                dlgWidgetConfig.OpenDialog();
                            }
                        }
                    }
                    break;
            }
        }
        #endregion

        protected void gridDevice_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            string deviceId = dataItem.GetDataKeyValue("Id").ToString();
            DeviceRepository rep = new DeviceRepository();
            var device = rep.FindByID(deviceId);
            e.DetailTableView.DataSource = device.ConnectedMachineList;
        }
    }
}