﻿(function () {
    var $;
    var demo = window.demo = window.demo || {};
    var orgChart;
    var ajaxManager;

    demo.initialize = function () {
        $ = $telerik.$;
        ajaxManager = window.ajaxManager;
        orgChart = window.orgChart;

        $("body").append("<div class='dropClue'></div><div class='noDropClue'></div>");

        dropClue = $(".dropClue")[0];
        noDropClue = $(".noDropClue")[0];

        $(".rocRemoveItemButton").click(function myfunction(e) {
            removePerson(e);
        })

        $(document).mouseup(function () {
            dropClue.style.display = "none";
            noDropClue.style.display = "none";
        });
    };

    var dropClue;
    var noDropClue;


    function pageLoad() {

    }

    function removePerson(e) {
        var index = orgChart.extractGroupItemFromDomElement(e.target).get_index();
        var hierarchicalIndex = orgChart.extractNodeFromDomElement(e.target).get_hierarchicalIndex();
        hierarchicalIndex = orgChart.getRealHierarchicalIndex(hierarchicalIndex); //gets the right index if drill down is on
        ajaxManager.ajaxRequest("remove_" + index + "_" + hierarchicalIndex);

    }

    window.OnClientDropping = function (sender, args) {
        var itemText = args.get_sourceItem().get_text().trim();
        if (args.get_htmlElement().className.indexOf("rocGroup") != -1 || $(args.get_htmlElement()).parents(".rocGroup").length > 0) {
            var hierarchicalIndex = orgChart.extractNodeFromDomElement(args.get_htmlElement()).get_hierarchicalIndex();
            hierarchicalIndex = orgChart.getRealHierarchicalIndex(hierarchicalIndex); //gets right index if drill down is on
            ajaxManager.ajaxRequest("drop_" + itemText + "_" + hierarchicalIndex);
        }

        dropClue.style.display = "none";
        noDropClue.style.display = "none";
    }

    window.OnClientDragging = function (sender, args) {
        var event = args.get_domEvent();
        var scrollOffset = $telerik.getScrollOffset(document.body, true);
        var top = (event.clientY + (window.pageYOffset || scrollOffset.y));
        var left = (event.clientX + (window.pageXOffset || scrollOffset.x));


        if (args.get_htmlElement().className.indexOf("rocGroup") != -1 || $(args.get_htmlElement()).parents(".rocGroup").length > 0) {
            noDropClue.style.display = "none";
            dropClue.style.top = top + 10 + "px";
            dropClue.style.left = left + 20 + "px";
            dropClue.style.display = "block";
        }
        else {
            dropClue.style.display = "none";
            noDropClue.style.top = top + 10 + "px";
            noDropClue.style.left = left + 20 + "px";
            noDropClue.style.display = "block";
        }
    }
})();