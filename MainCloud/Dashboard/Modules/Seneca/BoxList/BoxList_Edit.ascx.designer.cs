﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace MainCloud.Dashboard.Modules.Seneca.BoxList {
    
    
    public partial class BoxList_Edit {
        
        /// <summary>
        /// gridBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadGrid gridBox;
        
        /// <summary>
        /// gridGateway control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadGrid gridGateway;
        
        /// <summary>
        /// gridDevice control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadGrid gridDevice;
        
        /// <summary>
        /// dlgWidgetConfig control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MainCloudFramework.Web.PopUpDialog dlgWidgetConfig;
        
        /// <summary>
        /// messaggio control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label messaggio;
        
        /// <summary>
        /// edsBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Microsoft.AspNet.EntityDataSource.EntityDataSource edsBox;
        
        /// <summary>
        /// edsGateway control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Microsoft.AspNet.EntityDataSource.EntityDataSource edsGateway;
        
        /// <summary>
        /// edsDevice control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Microsoft.AspNet.EntityDataSource.EntityDataSource edsDevice;
        
        /// <summary>
        /// edsConnectedMachine control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Microsoft.AspNet.EntityDataSource.EntityDataSource edsConnectedMachine;
        
        /// <summary>
        /// edsAsset control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Microsoft.AspNet.EntityDataSource.EntityDataSource edsAsset;
    }
}
