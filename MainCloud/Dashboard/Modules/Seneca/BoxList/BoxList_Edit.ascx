﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BoxList_Edit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.BoxList.BoxList_Edit" %>

<%--<link rel="stylesheet" type="text/css" href="/Dashboard/Modules/Seneca/BoxView/Styles.css" />--%>
<%--<script src="<%=ResolveUrl("~/Dashboard/Modules/Seneca/BoxView/Scripts.js")%>"></script>--%>

<%--<asp:HiddenField runat="server" ID="SessionID" />--%>

<telerik:RadGrid ID="gridBox" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" DataSourceID="edsBox"
    OnInsertCommand="gridBox_InsertCommand" OnUpdateCommand="gridBox_UpdateCommand" OnDeleteCommand="gridBox_DeleteCommand"
    AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false" AllowPaging="true" AllowSorting="true"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView FilterExpression="" Caption="Boxes" DataKeyNames="Id"
        CommandItemDisplay="Top">
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="SerialNumber" HeaderText="S/N">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Supplier" HeaderText="Fornitore">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridDateTimeColumn DataField="InstallationDate" HeaderText="Installation Date" UniqueName="DtInstallation" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy}">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true">
                </ColumnValidationSettings>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<br />

<telerik:RadGrid ID="gridGateway" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" DataSourceID="edsGateway"
    OnInsertCommand="gridGateway_InsertCommand" OnUpdateCommand="gridGateway_UpdateCommand" OnDeleteCommand="gridGateway_DeleteCommand"
    AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false" AllowPaging="true" AllowSorting="true"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView FilterExpression="" Caption="Gateways" DataKeyNames="Id"
        CommandItemDisplay="Top">
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="SerialNumber" HeaderText="S/N">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Model" HeaderText="Model">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Supplier" HeaderText="Fornitore">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="NetworkAddress" HeaderText="NetworkAddress">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridDateTimeColumn DataField="InstallationDate" HeaderText="Installation Date" UniqueName="DtInstallation" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy}">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true">
                </ColumnValidationSettings>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridDropDownColumn UniqueName="ddlBox" ListTextField="SerialNumber"
                ListValueField="Id" DataSourceID="edsBox" HeaderText="Box" DataField="BoxId"
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>
            <telerik:GridCheckBoxColumn DataField="Active" HeaderText="Attivo">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridCheckBoxColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<br />

<telerik:RadGrid ID="gridDevice" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" DataSourceID="edsDevice"
    OnInsertCommand="gridDevice_InsertCommand" OnUpdateCommand="gridDevice_UpdateCommand" OnDeleteCommand="gridDevice_DeleteCommand" OnDetailTableDataBind="gridDevice_DetailTableDataBind"
    AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false" ShowStatusBar="true" AllowPaging="true" AllowSorting="true"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView FilterExpression="" Caption="Slaves" DataKeyNames="Id"
        CommandItemDisplay="Top" Name="Device">
        <DetailTables>
            <telerik:GridTableView runat="server" DataKeyNames="Id" Width="100%" EnableHierarchyExpandAll="true"
                CommandItemDisplay="Top" Name="ConnectedMachine" Caption="Connected Machines">
                <Columns>
                    <telerik:GridEditCommandColumn />
                    <telerik:GridDropDownColumn UniqueName="ddlAsset" ListTextField="Description"
                        ListValueField="Id" DataSourceID="edsAsset" HeaderText="Asset" DataField="AssetId" 
                        DropDownControlType="RadComboBox" AllowSorting="true" ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                        <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required" ControlToValidate="ddlAsset"></RequiredFieldValidator>
                        </ColumnValidationSettings>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDropDownColumn>
                    <telerik:GridNumericColumn DataField="ValuePin" HeaderText="Value Pin">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridNumericColumn>
                    <telerik:GridNumericColumn DataField="StatusPin" HeaderText="Status Pin">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridNumericColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
                </Columns>
            </telerik:GridTableView>
        </DetailTables>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="SerialNumber" HeaderText="S/N">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Model" HeaderText="Model">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Supplier" HeaderText="Fornitore">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="NetworkAddress" HeaderText="NetworkAddress">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridDateTimeColumn DataField="InstallationDate" HeaderText="Installation Date" UniqueName="DtInstallation" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy}">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true">
                </ColumnValidationSettings>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridDropDownColumn UniqueName="ddlGateway" ListTextField="SerialNumber"
                ListValueField="Id" DataSourceID="edsGateway" HeaderText="Gateway" DataField="GatewayId" 
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>
            <telerik:GridNumericColumn DataField="ConnectedNumber" HeaderText="Connected Number">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridNumericColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
    <Body>
        <asp:label id="messaggio" runat="server"/>
    </Body>
</mcf:PopUpDialog>

<ef:EntityDataSource runat="server" ID="edsBox" ContextTypeName="SenecaModule.Models.SenecaDbContext" EntitySetName="Boxes"
    AutoGenerateWhereClause="true" OrderBy="it.SerialNumber" />
<ef:EntityDataSource runat="server" ID="edsGateway" ContextTypeName="SenecaModule.Models.SenecaDbContext" EntitySetName="Gateways"
    AutoGenerateWhereClause="true" OrderBy="it.SerialNumber" />
<ef:EntityDataSource runat="server" ID="edsDevice" ContextTypeName="SenecaModule.Models.SenecaDbContext" EntitySetName="Devices"
    AutoGenerateWhereClause="true" OrderBy="it.SerialNumber" />
<ef:EntityDataSource runat="server" ID="edsConnectedMachine" ContextTypeName="SenecaModule.Models.SenecaDbContext" EntitySetName="ConnectedMachines" AutoGenerateWhereClause="true">
</ef:EntityDataSource>
<ef:EntityDataSource ID="edsAsset" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />

<%--<telerik:RadAjaxManagerProxy runat="server" ID="RadAjaxManager1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadListBox1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadOrgChart1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadOrgChart1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadOrgChart1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>


<telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" Skin="Default" />
<div class="demo-container no-bg">
    <div class="qsf-demo-canvas">
 
        <div class="new-employees">
            <telerik:RadListBox RenderMode="Lightweight" ID="RadListBox1" runat="server" Width="250px" Height="225px"
                OnClientDragging="OnClientDragging" EnableDragAndDrop="true" OnClientDropping="OnClientDropping"
                DataTextField="Name" DataValueField="ImageUrl">
                <ItemTemplate>
                    <img class="thumb" width="30" height="37" src='<%# DataBinder.Eval(Container, "Value")%>'
                        alt="employee thumb" />
                    <span class="employee-title"><%# DataBinder.Eval(Container, "Text")%></span>
                </ItemTemplate>
                <HeaderTemplate>
                    <span>Device List</span>
                </HeaderTemplate>
            </telerik:RadListBox>
        </div>
 
        <div class="organizedEmployees">
            <telerik:RadOrgChart RenderMode="Lightweight" ID="RadOrgChart1" runat="server" Orientation="Horizontal" GroupColumnCount="2"
                EnableCollapsing="true" EnableGroupCollapsing="true" EnableDragAndDrop="true">
                <ItemTemplate>
                    <span class="pseudoImage" style="background-image: url('<%# DataBinder.Eval(Container, "ImageUrl") %>')"></span>
                    <strong><%# DataBinder.Eval(Container, "Name") %></strong>
                    <button type="button" class="rocRemoveItemButton" title="Remove person">Remove</button>
                </ItemTemplate>
            </telerik:RadOrgChart>
        </div>

    </div>
</div>--%>


<%--<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            //<![CDATA[
            Sys.Application.add_load(function () {
                window.orgChart = $find("<%= RadOrgChart1.ClientID %>");
                window.ajaxManager = $find("<%= RadAjaxManager.GetCurrent(Page).ClientID %>");
                demo.initialize();
            });
            //]]>
        </script>
    </telerik:RadCodeBlock>--%>