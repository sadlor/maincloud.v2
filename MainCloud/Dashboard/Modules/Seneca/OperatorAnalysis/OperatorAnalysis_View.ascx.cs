﻿using AssetManagement.Repositories;
using MainCloud.HMI.Models;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Configuration;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.OperatorAnalysis
{
    public partial class OperatorAnalysis_View : DataWidget<OperatorAnalysis_View>
    {
        public OperatorAnalysis_View() : base(typeof(OperatorAnalysis_View)) { }

        IndexService indexService = new IndexService();
        TransactionService transService = new TransactionService();

        protected void Page_Load(object sender, EventArgs e)
        {
            edsMachine.WhereParameters.Clear();
            edsMachine.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsOperator.WhereParameters.Clear();
            edsOperator.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsCustomerOrder.WhereParameters.Clear();
            edsCustomerOrder.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);

            if (!IsPostBack)
            {
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }

            if (!IsPostBack)
            {
                customerOrderFilter.DataSource = edsCustomerOrder;
                customerOrderFilter.DataBind();
            }
            SetCustomerOrderFilter();
        }

        protected void jobGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridGroupHeaderItem)
            {
                GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;
                DataRowView groupDataRow = (DataRowView)e.Item.DataItem;
                item.DataCell.Text = groupDataRow["GroupType"].ToString();
            }
        }

        private void CreateTable(string assetId, List<Transaction> tList)
        {
            TransactionRepository TRep = new TransactionRepository();
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();
            JobRepository jobRep = new JobRepository();

            if (tList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.JobId).Distinct().Count() > 1)
            {
                //analisi su commesse in un certo periodo -> media degli indici
                TimeSpan totMachineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                TimeSpan totProductionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                decimal totQtyShotProd = 0;
                decimal totQtyProd = 0;
                decimal totCadency = 0;
                List<decimal> efficiencyList = new List<decimal>();
                List<decimal> qualityList = new List<decimal>();
                List<decimal> OEEList = new List<decimal>();

                foreach (var jobId in tList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.JobId).Distinct())
                {
                    //singola commessa
                    Job job = jobRep.FindByID(tList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.JobId).FirstOrDefault());

                    List<Transaction> tListJob = tList.Where(x => x.JobId == jobId).ToList();
                    TimeSpan machineOn = TimeSpan.FromSeconds((double)tListJob.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                    TimeSpan productionTime = TimeSpan.FromSeconds((double)tListJob.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                    TimeSpan stopTime = TimeSpan.FromSeconds((double)tListJob.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                    decimal oreProdTeoricoTotale = 0;
                    decimal oreProdTeorico = 0;
                    decimal qtyShotProd = 0;
                    decimal waste = 0;
                    decimal qtyProd = 0;
                    decimal cadency = 0;
                    decimal availability = 0;
                    decimal efficiency = 0;
                    decimal quality = 0;
                    decimal OEE = 0;

                    if (job != null)
                    {
                        decimal impronte = 1;
                        Transaction lastT = transService.GetLastTransactionOpen(assetId);
                        if (lastT != null)
                        {
                            impronte = lastT.CavityMoldNum;
                        }

                        if (productionTime.TotalSeconds > 0)
                        {
                            qtyShotProd = tListJob.Sum(x => x.PartialCounting - x.BlankShot);
                            totQtyShotProd += qtyShotProd;
                            qtyProd = tListJob.Sum(x => (x.PartialCounting - x.BlankShot) * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                            totQtyProd += qtyProd;
                            cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);

                            ConfigurationService config = new ConfigurationService();
                            if (Convert.ToBoolean(config.GetByParameter(ParamContext.StandardRate).FirstOrDefault().Value))
                            {
                                AssetRepository aRep = new AssetRepository();
                                job.StandardRate = aRep.GetStandardRate(assetId);
                            }
                            if (job.StandardRate > 0)
                            {
                                //oreProdTeorico += Math.Round(tList.Sum(x => x.PartialCounting - x.BlankShot) / job.StandardRate, 2);
                                oreProdTeorico += Math.Round(qtyShotProd / job.StandardRate, 2);
                                oreProdTeoricoTotale += Math.Round((decimal)(job.QtyOrdered / (job.CavityMoldNum > 0 ? job.CavityMoldNum : 1)) / job.StandardRate, 2);
                            }

                            try
                            {
                                waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == assetId && x.JobId == job.Id).Sum(x => x.QtyProductionWaste), 0);
                                //waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.Date >= DateStart && x.Date <= DateEnd).Sum(x => x.QtyProductionWaste), 0);
                            }
                            catch (InvalidOperationException) { }
                        }

                        availability = indexService.Availability(productionTime, machineOn);
                        efficiency = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
                        quality = indexService.Quality(qtyProd, waste);
                        OEE = indexService.OEE(availability / 100, efficiency / 100, quality / 100);
                        //indexList.Add(MesEnum.IndexParam.Availability, availability);
                        efficiencyList.Add(efficiency);
                        qualityList.Add(quality);
                        OEEList.Add(OEE);
                    }
                }
                totCadency = Math.Round(totQtyShotProd / (decimal)totProductionTime.TotalHours, 2);
                decimal totAvailability = indexService.Availability(totProductionTime, totMachineOn);
                decimal totEfficiency = efficiencyList.Average();

                List<JobRecordViewModel> dataSource = new List<JobRecordViewModel>();
                //availability
                dataSource.Add(new JobRecordViewModel("Tempo totale produzione / disponibile", null, null, null, totProductionTime, totMachineOn, totAvailability + "%", "Disponibilità"));
                //performance
                dataSource.Add(new JobRecordViewModel("Eseguito totale", totQtyProd, totQtyShotProd, totCadency, totProductionTime, null, totEfficiency + "%", "Efficienza"));
                ////quality
                //dataSource.Add(new JobRecordViewModel("Totale quantità prodotta", qtyProd, null, null, null, null, quality + "%", "Qualità"));
                //decimal wasteShot = waste % (impronte > 0 ? impronte : 1) > 0 ? (int)(waste / (impronte > 0 ? impronte : 1)) + 1 : waste / (impronte > 0 ? impronte : 1);
                //if (job.StandardRate > 0)
                //{
                //    dataSource.Add(new JobRecordViewModel("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours((double)(wasteShot / job.StandardRate)), null, (100 - quality) + "%", "Qualità"));
                //}
                //else
                //{
                //    dataSource.Add(new JobRecordViewModel("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours(0), null, (100 - quality) + "%", "Qualità"));
                //}

                jobGrid.DataSource = dataSource;
                jobGrid.DataBind();

                CauseService causeService = new CauseService();
                List<string> fermiList = causeService.GetListCauseIdByGroupName("Fermi");
                List<string> setupList = causeService.GetListCauseIdByGroupName("Attrezzaggio");
                decimal totTime = tList.Sum(x => x.Duration);
                decimal setupTime = tList.Where(x => setupList.Contains(x.CauseId)).Sum(x => x.Duration);
                decimal stopTimeWithNoSetup = tList.Where(x => fermiList.Contains(x.CauseId) || string.IsNullOrEmpty(x.CauseId)).Sum(x => x.Duration);
                TimeSpan totStopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                timeChart.PlotArea.Series[0].Items.Clear();
                if (totTime > 0)
                {
                    (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((setupTime / totTime) * 100, 2), System.Drawing.Color.Yellow, "Attrezzaggio"));
                    (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((stopTimeWithNoSetup / totTime) * 100, 2), System.Drawing.Color.Red, "Fermi"));
                    //(timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round(((decimal)totStopTime.TotalSeconds / totTime) * 100, 2), System.Drawing.Color.Red, "Fermi"));
                    (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round(((decimal)totProductionTime.TotalSeconds / totTime) * 100, 2), System.Drawing.Color.Green, "Produzione"));
                }

                meterAvailability.Pointer.Value = totAvailability;
                txtAvailability.Text = totAvailability.ToString();
                meterEfficiency.Pointer.Value = totEfficiency;
                txtEfficiency.Text = totEfficiency.ToString();
                //meterQuality.Pointer.Value = quality;
                //txtQuality.Text = quality.ToString();
                //meterOEE.Pointer.Value = OEE;
                //txtOEE.Text = OEE.ToString();
            }
            else
            {
                //analisi per singola commessa
                Job job = jobRep.FindByID(tList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.JobId).FirstOrDefault());

                TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                decimal oreProdTeoricoTotale = 0;
                decimal oreProdTeorico = 0;
                decimal qtyShotProd = 0;
                decimal qtyOrdered = 0;
                decimal waste = 0;
                decimal qtyProd = 0;
                decimal cadency = 0;
                decimal availability = 0;
                decimal efficiency = 0;
                decimal quality = 0;
                decimal OEE = 0;

                if (job != null)
                {
                    //JobService jobService = new JobService();
                    //qtyOrdered = jobService.GetBOM(job.Id).FirstOrDefault().Value.Sum(x => x.Quantity);
                    qtyOrdered = job.Order.QtyOrdered;

                    decimal impronte = 1;
                    Transaction lastT = transService.GetLastTransactionOpen(assetId);
                    if (lastT != null)
                    {
                        impronte = lastT.CavityMoldNum;
                    }

                    if (productionTime.TotalSeconds > 0)
                    {
                        qtyShotProd = tList.Sum(x => x.PartialCounting - x.BlankShot);
                        qtyProd = tList.Sum(x => (x.PartialCounting - x.BlankShot) * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                        cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);

                        ConfigurationService config = new ConfigurationService();
                        if (Convert.ToBoolean(config.GetByParameter(ParamContext.StandardRate).FirstOrDefault().Value))
                        {
                            AssetRepository aRep = new AssetRepository();
                            job.StandardRate = aRep.GetStandardRate(assetId);
                        }
                        if (job.StandardRate > 0)
                        {
                            //oreProdTeorico += Math.Round(tList.Sum(x => x.PartialCounting - x.BlankShot) / job.StandardRate, 2);
                            //oreProdTeoricoTotale += Math.Round((decimal)(job.QtyOrdered / (job.CavityMoldNum > 0 ? job.CavityMoldNum : 1)) / job.StandardRate, 2);
                            oreProdTeorico += Math.Round(qtyShotProd / job.StandardRate, 2);
                            oreProdTeoricoTotale += Math.Round((decimal)(qtyOrdered / (job.CavityMoldNum > 0 ? job.CavityMoldNum : 1)) / job.StandardRate, 2);
                        }

                        try
                        {
                            waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == assetId && x.JobId == job.Id).Sum(x => x.QtyProductionWaste), 0);
                            //waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.Date >= DateStart && x.Date <= DateEnd).Sum(x => x.QtyProductionWaste), 0);
                        }
                        catch (InvalidOperationException) { }
                    }

                    availability = indexService.Availability(productionTime, machineOn);
                    efficiency = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
                    quality = indexService.Quality(qtyProd, waste);
                    OEE = indexService.OEE(availability / 100, efficiency / 100, quality / 100);

                    List<JobRecordViewModel> dataSource = new List<JobRecordViewModel>();

                    //generalità job
                    //dataSource.Add(new JobRecordViewModel("Ordinato da produrre", job.QtyOrdered, Math.Round(job.QtyOrdered / (impronte > 0 ? impronte : 1), 2), job.StandardRate, null, TimeSpan.FromHours((double)oreProdTeoricoTotale), "", "Efficienza"));
                    dataSource.Add(new JobRecordViewModel("Ordinato da produrre", qtyOrdered, null, job.StandardRate, null, TimeSpan.FromHours((double)oreProdTeoricoTotale), "", "Efficienza"));

                    //availability
                    dataSource.Add(new JobRecordViewModel("Tempo totale produzione / disponibile", null, null, null, productionTime, machineOn, availability + "%", "Disponibilità"));

                    //performance
                    dataSource.Add(new JobRecordViewModel("Eseguito totale", qtyProd, qtyShotProd, cadency, productionTime, null, efficiency + "%", "Efficienza"));

                    decimal pezziResidui = qtyOrdered - qtyProd + waste;
                    decimal wasteShotResiduo = pezziResidui % (impronte > 0 ? impronte : 1) > 0 ? (int)(pezziResidui / (impronte > 0 ? impronte : 1)) + 1 : pezziResidui / (impronte > 0 ? impronte : 1);
                    //TimeSpan tempoResiduo = TimeSpan.FromHours((double)(cadency > 0 ? ((pezziResidui / (impronte > 0 ? impronte : 1)) / cadency) : 0) + productionTime.TotalHours);
                    TimeSpan tempoResiduo = TimeSpan.FromHours((double)(cadency > 0 ? (wasteShotResiduo / cadency) : 0) + productionTime.TotalHours);
                    dataSource.Add(
                        new JobRecordViewModel(
                            "Residuo da produrre (eccedenza) compreso scarti",
                            pezziResidui,
                            wasteShotResiduo,//Math.Round(pezziResidui / (impronte > 0 ? impronte : 1), 2),
                            null,
                            //cadency > 0 ? TimeSpan.FromHours((double)((pezziResidui / (impronte > 0 ? impronte : 1)) / cadency)) : TimeSpan.FromHours((double)(0)),
                            cadency > 0 ? TimeSpan.FromHours((double)(wasteShotResiduo / cadency)) : TimeSpan.FromHours(0),
                            null,
                            "",
                            "Efficienza"));

                    if (job.StandardRate > 0)
                    {
                        //decimal tempoAssegnato = Math.Round(job.QtyOrdered / (job.StandardRate * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1)), 2);
                        decimal tempoAssegnato = Math.Round(qtyOrdered / (job.StandardRate * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1)), 2);
                        decimal tempoPrevisto = 0;
                        if (efficiency > 0)
                        {
                            tempoPrevisto = Math.Round(tempoAssegnato / efficiency * 100, 2);
                        }
                        else
                        {
                            tempoPrevisto = tempoAssegnato;
                        }
                        if (tempoPrevisto > 0)
                        {
                            dataSource.Add(new JobRecordViewModel("Tempo eccedenza (recupero) e totale lotto", null, null, null, TimeSpan.FromHours((double)(tempoPrevisto - tempoAssegnato)), tempoResiduo, 100 - Math.Round(tempoAssegnato / tempoPrevisto * 100, 2) + "%", "Efficienza"));
                        }
                    }

                    //quality
                    dataSource.Add(new JobRecordViewModel("Totale quantità prodotta", qtyProd, null, null, null, null, quality + "%", "Qualità"));
                    decimal wasteShot = waste % (impronte > 0 ? impronte : 1) > 0 ? (int)(waste / (impronte > 0 ? impronte : 1)) + 1 : waste / (impronte > 0 ? impronte : 1);
                    if (job.StandardRate > 0)
                    {
                        dataSource.Add(new JobRecordViewModel("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours((double)(wasteShot / job.StandardRate)), null, (100 - quality) + "%", "Qualità"));
                    }
                    else
                    {
                        dataSource.Add(new JobRecordViewModel("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours(0), null, (100 - quality) + "%", "Qualità"));
                    }

                    jobGrid.DataSource = dataSource;
                    jobGrid.DataBind();
                }

                CauseService causeService = new CauseService();
                List<string> fermiList = causeService.GetListCauseIdByGroupName("Fermi");
                List<string> setupList = causeService.GetListCauseIdByGroupName("Attrezzaggio");
                decimal totTime = tList.Sum(x => x.Duration);
                decimal setupTime = tList.Where(x => setupList.Contains(x.CauseId)).Sum(x => x.Duration);
                decimal stopTimeWithNoSetup = tList.Where(x => fermiList.Contains(x.CauseId) || string.IsNullOrEmpty(x.CauseId)).Sum(x => x.Duration);
                timeChart.PlotArea.Series[0].Items.Clear();
                if (totTime > 0)
                {
                    (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((setupTime / totTime) * 100, 2), System.Drawing.Color.Yellow, "Attrezzaggio"));
                    (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((stopTimeWithNoSetup / totTime) * 100, 2), System.Drawing.Color.Red, "Fermi"));
                    //(timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round(((decimal)stopTime.TotalSeconds / totTime) * 100, 2), System.Drawing.Color.Red, "Fermi"));
                    (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round(((decimal)productionTime.TotalSeconds / totTime) * 100, 2), System.Drawing.Color.Green, "Produzione"));
                }

                meterAvailability.Pointer.Value = availability;
                txtAvailability.Text = availability.ToString();
                meterEfficiency.Pointer.Value = efficiency;
                txtEfficiency.Text = efficiency.ToString();
                meterQuality.Pointer.Value = quality;
                txtQuality.Text = quality.ToString();
                meterOEE.Pointer.Value = OEE;
                txtOEE.Text = OEE.ToString();
            }
        }

        //private void CreateTable_old(List<Transaction> tList)
        //{
        //    TransactionRepository TRep = new TransactionRepository();
        //    JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

        //    JobRepository jobRep = new JobRepository();
        //    Job job = jobRep.FindByID(tList.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.JobId).FirstOrDefault());
        //    //Job job = transService.GetLastTransactionOpen(AssetId).Job;

        //    TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
        //    TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
        //    TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

        //    decimal oreProdTeoricoTotale = 0;
        //    decimal oreProdTeorico = 0;
        //    decimal qtyShotProd = 0;
        //    decimal waste = 0;
        //    decimal qtyProd = 0;
        //    decimal cadency = 0;
        //    decimal availability = 0;
        //    decimal efficiency = 0;
        //    decimal quality = 0;
        //    decimal OEE = 0;

        //    if (job != null)
        //    {
        //        //int impronte = transService.GetLastTransactionOpen(AssetId).CavityMoldNum;
        //        var impronte = job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1;

        //        if (productionTime.TotalSeconds > 0)
        //        {
        //            qtyShotProd = tList.Sum(x => x.PartialCounting);
        //            cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
        //            if (job.StandardRate > 0)
        //            {
        //                oreProdTeorico += Math.Round(tList.Sum(x => x.PartialCounting) / job.StandardRate, 2);
        //                oreProdTeoricoTotale += Math.Round((decimal)(job.QtyOrdered / (job.CavityMoldNum > 0 ? job.CavityMoldNum : 1)) / job.StandardRate, 2);
        //            }
        //            qtyProd = tList.Sum(x => x.PartialCounting * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
        //            try
        //            {
        //                //JobProductionWasteService jpws = new JobProductionWasteService();
        //                //waste = jpws.GetWasteListByJobId(job.Id).Sum(x => x.QtyProductionWaste);
        //                waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.JobId == job.Id).Sum(x => x.QtyProductionWaste), 0);
        //                //waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.Date >= DateStart && x.Date <= DateEnd).Sum(x => x.QtyProductionWaste), 0);
        //            }
        //            catch (InvalidOperationException) { }
        //        }

        //        availability = indexService.Availability(productionTime, machineOn);
        //        efficiency = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
        //        quality = indexService.Quality(qtyProd, waste);
        //        OEE = indexService.OEE(availability / 100, efficiency / 100, quality / 100);

        //        List<JobRecord> dataSource = new List<JobRecord>();

        //        //generalità job
        //        dataSource.Add(new JobRecord("Ordinato da produrre", job.QtyOrdered, Math.Round(job.QtyOrdered / impronte, 2), job.StandardRate, null, TimeSpan.FromHours((double)oreProdTeoricoTotale), "", "Efficienza"));

        //        //availability
        //        dataSource.Add(new JobRecord("Tempo totale produzione / disponibile", null, null, null, productionTime, machineOn, availability + "%", "Disponibilità"));

        //        ////efficiency
        //        dataSource.Add(new JobRecord("Eseguito effettivo", qtyProd, qtyShotProd, cadency, null, productionTime, efficiency + "%", "Efficienza"));
        //        decimal pezziResidui = job.QtyOrdered - qtyProd + waste;
        //        TimeSpan tempoResiduo = TimeSpan.FromHours((double)(cadency > 0 ? ((pezziResidui / (impronte > 0 ? impronte : 1)) / cadency) : 0) + productionTime.TotalHours);
        //        decimal wasteShotResiduo = pezziResidui % (impronte > 0 ? impronte : 1) > 0 ? (int)(pezziResidui / (impronte > 0 ? impronte : 1)) + 1 : pezziResidui / (impronte > 0 ? impronte : 1);
        //        dataSource.Add(
        //            new JobRecord(
        //                "Residuo da produrre (eccedenza) compreso scarti",
        //                pezziResidui,
        //                wasteShotResiduo,//Math.Round(pezziResidui / (impronte > 0 ? impronte : 1), 2),
        //                null,
        //                cadency > 0 ? TimeSpan.FromHours((double)((pezziResidui / (impronte > 0 ? impronte : 1)) / cadency)) : TimeSpan.FromHours((double)(0)),
        //                null,
        //                "",
        //                "Efficienza"));

        //        decimal tempoAssegnato = Math.Round(job.QtyOrdered / (job.StandardRate * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1)), 2);
        //        decimal tempoPrevisto = 0;
        //        if (efficiency > 0)
        //        {
        //            tempoPrevisto = Math.Round(tempoAssegnato / efficiency * 100, 2);
        //        }
        //        else
        //        {
        //            tempoPrevisto = tempoAssegnato;
        //        }
        //        dataSource.Add(new JobRecord("Tempo eccedenza (recupero) e totale lotto", null, null, null, TimeSpan.FromHours((double)(tempoPrevisto - tempoAssegnato)), tempoResiduo, 100 - Math.Round(tempoAssegnato / tempoPrevisto * 100, 2) + "%", "Efficienza"));

        //        //quality
        //        dataSource.Add(new JobRecord("Totale pezzi prodotti", qtyProd, null, null, null, null, quality + "%", "Qualità"));
        //        decimal wasteShot = waste % (impronte > 0 ? impronte : 1) > 0 ? (int)(waste / (impronte > 0 ? impronte : 1)) + 1 : waste / (impronte > 0 ? impronte : 1);
        //        if (job.StandardRate > 0)
        //        {
        //            dataSource.Add(new JobRecord("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours((double)(wasteShot / job.StandardRate)), null, (100 - quality) + "%", "Qualità"));
        //        }
        //        else
        //        {
        //            dataSource.Add(new JobRecord("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours(0), null, (100 - quality) + "%", "Qualità"));
        //        }

        //        jobGrid.DataSource = dataSource;
        //        jobGrid.Rebind();
        //    }

        //    CauseService causeService = new CauseService();
        //    List<string> fermiList = causeService.GetListCauseIdByGroupName("Fermi");
        //    List<string> setupList = causeService.GetListCauseIdByGroupName("Attrezzaggio");
        //    decimal totTime = tList.Sum(x => x.Duration);
        //    decimal setupTime = tList.Where(x => setupList.Contains(x.CauseId)).Sum(x => x.Duration);
        //    decimal stopTimeWithNoSetup = tList.Where(x => fermiList.Contains(x.CauseId) || string.IsNullOrEmpty(x.CauseId)).Sum(x => x.Duration);
        //    timeChart.PlotArea.Series[0].Items.Clear();
        //    if (totTime > 0)
        //    {
        //        (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((setupTime / totTime) * 100, 2), System.Drawing.Color.Yellow, "Attrezzaggio"));
        //        (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((stopTimeWithNoSetup / totTime) * 100, 2), System.Drawing.Color.Red, "Fermi"));
        //        (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round(((decimal)productionTime.TotalSeconds / totTime) * 100, 2), System.Drawing.Color.Green, "Produzione"));
        //    }

        //    meterAvailability.Pointer.Value = availability;
        //    txtAvailability.Text = availability.ToString();
        //    meterEfficiency.Pointer.Value = efficiency;
        //    txtEfficiency.Text = efficiency.ToString();
        //    meterQuality.Pointer.Value = quality;
        //    txtQuality.Text = quality.ToString();
        //    meterOEE.Pointer.Value = OEE;
        //    txtOEE.Text = OEE.ToString();
        //}

        private void CreateTable_old_old(List<Transaction> workshift)
        {
            //List<JobRecord> dataSource = new List<JobRecord>();
            //if (workshift.Count > 0)
            //{
            //    TransactionService TService = new TransactionService();

            //    JobRepository jobRep = new JobRepository();
            //    Job job = jobRep.FindByID(workshift.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.JobId).First());
            //    int nImpronte = 1;
            //    if (!string.IsNullOrEmpty(job.MoldId))
            //    {
            //        MoldRepository mRep = new MoldRepository();
            //        nImpronte = mRep.FindByID(job.MoldId).CavityMoldNum;
            //    }
            //    else
            //    {
            //        nImpronte = job.CavityMoldNum.HasValue ? job.CavityMoldNum.Value : nImpronte;
            //    }
            //    if (nImpronte == 0)
            //    {
            //        nImpronte = 1;
            //    }

            //    CauseService causeService = new CauseService();
            //    string productionCauseId = causeService.GetCauseByCode("200").Id;
            //    List<string> fermiList = causeService.GetListCauseIdByGroupName("Fermi");
            //    List<string> setupList = causeService.GetListCauseIdByGroupName("Attrezzaggio");

            //    if (job.NStampateOra == 0)
            //    {
            //        job.NStampateOra = 1;
            //    }

            //    //generalità job
            //    dataSource.Add(new JobRecord("Quantità da produrre", job.QtyOrdered, job.QtyOrdered / nImpronte, job.NStampateOra, null, null, "", "Efficienza"));

            //    decimal workshiftEffettivo = 0;
            //    if (workshift.Count > 0 && workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration) > 0)
            //    {
            //        workshiftEffettivo = Math.Round(workshift.Sum(x => x.PartialCounting) / Convert.ToDecimal(TimeSpan.FromSeconds((double)workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalHours), 2);
            //    }

            //    decimal totProduzione = workshift.Sum(x => x.PartialCounting);

            //    dataSource.Add(
            //        new JobRecord(
            //            "Eseguito totale",
            //            totProduzione * nImpronte,
            //            totProduzione,
            //            null,
            //            workshiftEffettivo,
            //            TimeSpan.FromSeconds((double)workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)),
            //            job.NStampateOra == 0 ? null : indexService.Efficiency(job.NStampateOra, totProduzione, TimeSpan.FromSeconds((double)workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration))) + "%",//Math.Round(workshiftEffettivo / job.NStampateOra * 100, 2) + "%",
            //                "Efficienza"));

            //    //scarti
            //    JobProductionWasteService jpws = new JobProductionWasteService();
            //    List<JobProductionWaste> scarti = jpws.GetWasteListByJobId(job.Id);
            //    dataSource.Add(new JobRecord("Scarti", scarti.Sum(x => x.QtyProductionWaste), scarti.Sum(x => x.QtyProductionWaste) / nImpronte, null, null, null, "", "Efficienza"));

            //    //residuo
            //    decimal pezziResidui = job.QtyOrdered - (totProduzione * nImpronte) + scarti.Sum(x => x.QtyProductionWaste);
            //    dataSource.Add(
            //        new JobRecord(
            //            "Residuo da produrre (eccedenza)",
            //            pezziResidui,
            //            pezziResidui / nImpronte,
            //            null,
            //            null,
            //            TimeSpan.FromHours((double)((pezziResidui / nImpronte) / job.NStampateOra)),
            //            "",
            //            "Efficienza"));

            //    //Resa
            //    dataSource.Add(
            //        new JobRecord(
            //            "Tempo totale disponibile", null, null, null, null, TimeSpan.FromSeconds((double)workshift.Sum(x => x.Duration)), "", "Disponibilità"));
            //    dataSource.Add(
            //        new JobRecord(
            //            "Tempo totale produzione",
            //            null,
            //            null,
            //            null,
            //            null,
            //            TimeSpan.FromSeconds((double)workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)),
            //            Math.Round(TimeSpan.FromSeconds((double)workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalSeconds /
            //            TimeSpan.FromSeconds((double)workshift.Sum(x => x.Duration)).TotalSeconds * 100, 2) + "%",
            //            "Disponibilità"));

            //    //Qualità
            //    if (totProduzione > 0)
            //    {
            //        dataSource.Add(
            //            new JobRecord(
            //                "Totale pezzi prodotti",
            //                totProduzione * nImpronte,
            //                null,
            //                null,
            //                null,
            //                null,
            //                Math.Round((((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) * 100, 2) + "%",
            //                "Qualità"));
            //    }
            //    else
            //    {
            //        dataSource.Add(new JobRecord("Totale pezzi prodotti", totProduzione, null, null, null, null, "", "Qualità"));
            //    }
            //    dataSource.Add(new JobRecord("Totale non conformità", scarti.Sum(x => x.QtyProductionWaste), null, null, null, null, "", "Qualità"));


            //    var setupTime = workshift.Where(x => setupList.Contains(x.CauseId)).Sum(x => x.Duration);
            //    var stopTime = workshift.Where(x => fermiList.Contains(x.CauseId) || string.IsNullOrEmpty(x.CauseId)).Sum(x => x.Duration);
            //    var prodTime = workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration);
            //    var totTime = workshift.Sum(x => x.Duration);

            //    timeChart.PlotArea.Series[0].Items.Clear();
            //    if (totTime > 0)
            //    {
            //        (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((setupTime / totTime) * 100, 2), System.Drawing.Color.Yellow, "Attrezzaggio"));
            //        (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((stopTime / totTime) * 100, 2), System.Drawing.Color.Red, "Fermi"));
            //        (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((prodTime / totTime) * 100, 2), System.Drawing.Color.Green, "Produzione"));
            //    }

            //    meterEfficienza.Pointer.Value = job.NStampateOra == 0 ? 0 : indexService.Efficiency(job.NStampateOra, totProduzione, TimeSpan.FromSeconds((double)prodTime));//Math.Round(workshiftEffettivo / job.NStampateOra * 100, 2);
            //    txtEfficienza.Text = job.NStampateOra == 0 ? null : Math.Round(workshiftEffettivo / job.NStampateOra * 100, 2).ToString();
            //    if (totProduzione > 0)
            //    {
            //        meterQualità.Pointer.Value = Math.Round((((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) * 100, 2);
            //        txtQualità.Text = Math.Round((((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) * 100, 2).ToString();
            //    }
            //    else
            //    {
            //        meterQualità.Pointer.Value = 0;
            //        txtQualità.Text = 0.ToString();
            //    }
            //    if (workshift.Sum(x => x.Duration) > 0)
            //    {
            //        meterResa.Pointer.Value = Convert.ToDecimal(Math.Round(TimeSpan.FromSeconds((double)workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalSeconds /
            //                TimeSpan.FromSeconds((double)workshift.Sum(x => x.Duration)).TotalSeconds * 100, 2));
            //        txtResa.Text = Math.Round(TimeSpan.FromSeconds((double)workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalSeconds /
            //                TimeSpan.FromSeconds((double)workshift.Sum(x => x.Duration)).TotalSeconds * 100, 2).ToString();
            //    }
            //    else
            //    {
            //        meterResa.Pointer.Value = 0;
            //        txtResa.Text = 0.ToString();
            //    }

            //    if (meterEfficienza.Pointer.Value == 0 || meterResa.Pointer.Value == 0)
            //    {
            //        meterOEE.Pointer.Value = 0;
            //        txtOEE.Text = 0.ToString();
            //    }
            //    else
            //    {
            //        meterOEE.Pointer.Value = Math.Round(workshiftEffettivo / job.NStampateOra *
            //            (((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) *
            //            (decimal)(TimeSpan.FromSeconds((double)workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalSeconds /
            //                TimeSpan.FromSeconds((double)workshift.Sum(x => x.Duration)).TotalSeconds)
            //            * 100, 2);
            //        txtOEE.Text = Math.Round(workshiftEffettivo / job.NStampateOra *
            //            (((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) *
            //            (decimal)(TimeSpan.FromSeconds((double)workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalSeconds /
            //                TimeSpan.FromSeconds((double)workshift.Sum(x => x.Duration)).TotalSeconds)
            //            * 100, 2).ToString();
            //    }


            //}
            //else
            //{
            //    //vuoto
            //    dataSource.Add(new JobRecord("Quantità da produrre", null, null, null, null, null, "", "Efficienza"));
            //    dataSource.Add(new JobRecord("Eseguito turni precedenti", null, null, null, null, null, "", "Efficienza"));
            //    dataSource.Add(new JobRecord("Eseguito turno corrente", null, null, null, null, null, "", "Efficienza"));
            //    dataSource.Add(new JobRecord("Scarti", null, null, null, null, null, "", "Efficienza"));
            //    dataSource.Add(new JobRecord("Residuo da produrre", null, null, null, null, null, "", "Efficienza"));
            //    dataSource.Add(new JobRecord("Tempo totale disponibile", null, null, null, null, null, "", "Resa"));
            //    dataSource.Add(new JobRecord("Tempo totale produzione", null, null, null, null, null, "", "Resa"));
            //    dataSource.Add(new JobRecord("Totale pezzi prodotti", null, null, null, null, null, "", "Qualità"));
            //    dataSource.Add(new JobRecord("Totale non conformità", null, null, null, null, null, "", "Qualità"));
            //}
            //jobGrid.DataSource = dataSource;
            //jobGrid.Rebind();
        }

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            CreateFilter();
        }

        private void CreateFilter()
        {
            string assetId = machineFilter.Entries.Count > 0 ? machineFilter.Entries[0].Value : string.Empty;
            int customerOrderId = customerOrderFilter.Entries.Count > 0 ? Convert.ToInt32(customerOrderFilter.Entries[0].Value) : -1;
            string operatorId = operatorFilter.Entries.Count > 0 ? operatorFilter.Entries[0].Value : string.Empty;
            DateTime? start = dtPickStart.SelectedDate;
            DateTime? end = dtPickEnd.SelectedDate;
            if (end.HasValue && end == DateTime.Today)
            {
                end = DateTime.Now;
            }

            TransactionRepository TRep = new TransactionRepository();
            List<Transaction> dataSource = new List<Transaction>();
            IQueryable<Transaction> returnList = null;
            if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(operatorId) && customerOrderId != -1)
            {
                returnList = TRep.ReadAll(x => x.MachineId == assetId && x.OperatorId == operatorId && x.Job.Order.CustomerOrder.Id == customerOrderId);
            }
            else
            {
                if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(operatorId))
                {
                    returnList = TRep.ReadAll(x => x.MachineId == assetId && x.OperatorId == operatorId);
                    goto EndFilter;
                }
                if (!string.IsNullOrEmpty(assetId) && customerOrderId != -1)
                {
                    returnList = TRep.ReadAll(x => x.MachineId == assetId && x.Job.Order.CustomerOrder.Id == customerOrderId);
                    goto EndFilter;
                }
                if (!string.IsNullOrEmpty(operatorId) && customerOrderId != -1)
                {
                    returnList = TRep.ReadAll(x => x.OperatorId == operatorId && x.Job.Order.CustomerOrder.Id == customerOrderId);
                    goto EndFilter;
                }
                if (!string.IsNullOrEmpty(assetId))
                {
                    returnList = TRep.ReadAll(x => x.MachineId == assetId);
                    goto EndFilter;
                }
                if (!string.IsNullOrEmpty(operatorId))
                {
                    returnList = TRep.ReadAll(x => x.OperatorId == operatorId);
                    goto EndFilter;
                }
                if (customerOrderId != -1)
                {
                    returnList = TRep.ReadAll(x => x.Job.Order.CustomerOrder.Id == customerOrderId);
                    goto EndFilter;
                }
            }

            EndFilter:
            if (returnList != null && returnList.Count() > 0)
            {
                if (start.HasValue)
                {
                    if (end.HasValue)
                    {
                        dataSource = returnList.Where(x => x.Start >= start && x.Start <= end).ToList();
                    }
                    else
                    {
                        dataSource = returnList.Where(x => x.Start >= start).ToList();
                    }
                }
                else
                {
                    dataSource = returnList.ToList();
                }
            }
            else
            {
                if (start.HasValue)
                {
                    if (end.HasValue)
                    {
                        dataSource = TRep.ReadAll(x => x.Start >= start && x.Start <= end).ToList();
                    }
                    else
                    {
                        dataSource = TRep.ReadAll(x => x.Start >= start).ToList();
                    }
                }
            }

            CreateTable(assetId, dataSource);


            //var param = Expression.Parameter(typeof(Transaction), "t");
            //BinaryExpression body;

            //if (!string.IsNullOrEmpty(assetId))
            //{
            //    body = Expression..And(body, Expression.Equal(
            //            Expression.PropertyOrField(param, "MachineId"),
            //            Expression.Constant(assetId)));
            //}
            //if (!string.IsNullOrEmpty(operatorId))
            //{
            //    body = Expression.And(body, Expression.Equal(
            //            Expression.PropertyOrField(param, "OperatorId"),
            //            Expression.Constant(operatorId)));
            //}
            //var lambda = Expression.Lambda<Func<Transaction, bool>>(body, param);
            //returnList = TRep.ReadAll(lambda).ToList();

            //Expression<Func<Purchase, bool>> criteria1 = p => p.Price > 1000;
            //Expression<Func<Purchase, bool>> criteria2 = p => criteria1.Invoke(p)
            //                                                  || p.Description.Contains("a");

            //Console.WriteLine(criteria2.Expand().ToString());
        }

        protected void machineFilter_TextChanged(object sender, AutoCompleteTextEventArgs e)
        {
            SetCustomerOrderFilter();
        }

        private void SetCustomerOrderFilter()
        {
            if (machineFilter.Entries.Count > 0)
            {
                string assetId = machineFilter.Entries[0].Value;

                TransactionService TService = new TransactionService();
                customerOrderFilter.DataSource = TService.SearchCustomerOrderPerMachine(assetId);
                customerOrderFilter.DataBind();
            }
            else
            {
                customerOrderFilter.DataSource = edsCustomerOrder;
                customerOrderFilter.DataBind();
            }
        }

        protected void operatorFilter_TextChanged(object sender, AutoCompleteTextEventArgs e)
        {
            if (operatorFilter.Entries.Count > 0)
            {
                OperatorRepository OR = new OperatorRepository();
                Operator op = OR.FindByID(operatorFilter.Entries[0].Value);
                txtOperatorName.Text = op.UserName;
            }
            else
            {
                txtOperatorName.Text = string.Empty;
            }
        }
    }
}