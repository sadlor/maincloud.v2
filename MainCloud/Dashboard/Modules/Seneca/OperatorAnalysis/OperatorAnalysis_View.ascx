﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OperatorAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.OperatorAnalysis.OperatorAnalysis_View" %>

<script type="text/javascript">
    function OpenEndCalendar()
    {
        <%--var calendarStart = $find("<%=dtPickStart.ClientID%>");
        var calendarEnd = $find("<%=dtPickEnd.ClientID%>");
        calendarEnd.navigateToDate(calendarStart.get_selectedDates());--%>
    }
</script>

<div class="panel panel-info">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-5">
                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="machineFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px" Width="200px"
                    TextSettings-SelectionMode="Single" DataSourceID="edsMachine" DataTextField="Code" DataValueField="Id" InputType="Text" Filter="Contains" Label="Macchina: "
                    OnTextChanged="machineFilter_TextChanged">
                </telerik:RadAutoCompleteBox>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                    <ClientEvents OnDateSelected="OpenEndCalendar" />
                    <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-5">
                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="customerOrderFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px"
                     TextSettings-SelectionMode="Single" DataTextField="OrderCode" DataValueField="Id" InputType="Text" Filter="Contains" Label="Commessa: " Width="200px">
                </telerik:RadAutoCompleteBox>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-4">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="operatorFilter" EmptyMessage="Seleziona" TextSettings-SelectionMode="Single" LabelWidth="87px" Width="200px"
                            DataSourceID="edsOperator" DataTextField="Badge" DataValueField="Id" InputType="Text" Filter="Contains" Label="Operatore: " OnTextChanged="operatorFilter_TextChanged">
                        </telerik:RadAutoCompleteBox>
                        <telerik:RadTextBox runat="server" ID="txtOperatorName" Enabled="false" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div style="text-align:right">
            <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
        </div>
    </div>
</div>

<br />

<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnConfirmSelection" EventName="Click" />
    </Triggers>
    <ContentTemplate>
        <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="jobGrid" AutoGenerateColumns="false" GridLines="none" Skin="Silk" 
            OnItemDataBound="jobGrid_ItemDataBound"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <MasterTableView FilterExpression="" Caption="">
                <ColumnGroups>
                    <telerik:GridColumnGroup Name="Tempo" HeaderText="Tempo">
                        <HeaderStyle HorizontalAlign="Center" />
                    </telerik:GridColumnGroup>
                </ColumnGroups>
                <GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldName="GroupType"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldName="GroupType" SortOrder="None"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>
                <Columns>
                    <telerik:GridBoundColumn DataField="Label" HeaderText="">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Pezzi" HeaderText="Pezzi" DataFormatString="{0:f0}">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Colpi" HeaderText="Colpi" DataFormatString="{0:f0}">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Teorico" HeaderText="Colpi / h">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Eseguito" HeaderText="Tempo<br/>Effettivo">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Tempo" HeaderText="Tempo<br/>Totale">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Efficienza" HeaderText="Indice %">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <Selecting AllowRowSelect="True"></Selecting>
            </ClientSettings>
        </telerik:RadGrid>
        <telerik:RadHtmlChart runat="server" ID="timeChart" Transitions="true" Skin="Silk">
            <ChartTitle Text="Tempo utilizzato">
                <Appearance Align="Center" Position="Top">
                </Appearance>
            </ChartTitle>
            <Legend>
                <Appearance Position="Right" Visible="true">
                </Appearance>
            </Legend>
            <PlotArea>
                <Series>
                    <telerik:PieSeries StartAngle="90">
                        <LabelsAppearance Position="OutsideEnd" DataFormatString="{0} %">
                        </LabelsAppearance>
                        <TooltipsAppearance Color="White" DataFormatString="{0} %"></TooltipsAppearance>
                    </telerik:PieSeries>
                </Series>
            </PlotArea>
        </telerik:RadHtmlChart>
    </ContentTemplate>
</asp:UpdatePanel>

<br />

<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnConfirmSelection" EventName="Click" />
    </Triggers>
    <ContentTemplate>
        <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <center>
                    <asp:Label Font-Bold="true" Text="Disponibilità" runat="server" />
                    <telerik:RadRadialGauge runat="server" ID="meterAvailability">
                        <Pointer Value="0">
                            <Cap Size="0.1" />
                        </Pointer>
                        <Scale Min="0" Max="100" MajorUnit="20">
                            <Labels Format="{0}" />
                            <Ranges>
                                <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                            </Ranges>
                        </Scale>
                    </telerik:RadRadialGauge>
                    <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtAvailability" style="text-align:center" />
                </center>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <center>
                    <asp:Label Font-Bold="true" Text="Efficienza" runat="server" />
                    <telerik:RadRadialGauge runat="server" ID="meterEfficiency">
                        <Pointer Value="0">
                            <Cap Size="0.1" />
                        </Pointer>
                        <Scale Min="0" Max="100" MajorUnit="20">
                            <Labels Format="{0}" />
                            <Ranges>
                                <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                            </Ranges>
                        </Scale>
                    </telerik:RadRadialGauge>
                    <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtEfficiency" style="text-align:center" />
                </center>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <center>
                    <asp:Label Font-Bold="true" Text="Qualità" runat="server" />
                    <telerik:RadRadialGauge runat="server" ID="meterQuality">
                        <Pointer Value="0">
                            <Cap Size="0.1" />
                        </Pointer>
                        <Scale Min="0" Max="100" MajorUnit="20">
                            <Labels Format="{0}" />
                            <Ranges>
                                <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                            </Ranges>
                        </Scale>
                    </telerik:RadRadialGauge>
                    <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtQuality" style="text-align:center" />
                </center>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <center>
                    <asp:Label Font-Bold="true" Text="OEE" runat="server" />
                    <telerik:RadRadialGauge runat="server" ID="meterOEE">
                        <Pointer Value="0">
                            <Cap Size="0.1" />
                        </Pointer>
                        <Scale Min="0" Max="100" MajorUnit="20">
                            <Labels Format="{0}" />
                            <Ranges>
                                <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                            </Ranges>
                        </Scale>
                    </telerik:RadRadialGauge>
                    <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtOEE" style="text-align:center" />
                </center>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>


<ef:EntityDataSource ID="edsMachine" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    AutoGenerateWhereClause="true" OrderBy="it.Code">
</ef:EntityDataSource>
<ef:EntityDataSource ID="edsOperator" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Operators"
    OrderBy="it.UserName" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsCustomerOrder" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="CustomerOrders"
    OrderBy="it.OrderCode" AutoGenerateWhereClause="true" />