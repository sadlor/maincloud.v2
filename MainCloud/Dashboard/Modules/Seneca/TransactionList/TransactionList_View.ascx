﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TransactionList_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.TransactionList.TransactionList_View" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        <%--function FilterCreated(sender, eventArgs) {
            var filterMenu = sender.get_contextMenu();
            filterMenu.add_showing(FilterMenuShowing);
        }
        function FilterMenuShowing(sender, eventArgs) {
            var filter = $find("<%= TransFilter.ClientID %>");
            var currentExpandedItem = sender.get_attributes()._data.ItemHierarchyIndex;

            sender.findItemByValue("And").set_visible(false);
            sender.findItemByValue("Or").set_visible(false);
            sender.findItemByValue("NotOr").set_visible(false);
            sender.findItemByValue("NotAnd").set_visible(false);

            sender.findItemByValue("IsNull").set_visible(false);
            sender.findItemByValue("NotIsNull").set_visible(false);
            sender.findItemByValue("IsEmpty").set_visible(false);
            sender.findItemByValue("NotIsEmpty").set_visible(false);
            sender.findItemByValue("Contains").set_visible(false);
            sender.findItemByValue("DoesNotContain").set_visible(false);
            sender.findItemByValue("GreaterThan").set_visible(false);
            sender.findItemByValue("GreaterThanOrEqualTo").set_visible(false);
            sender.findItemByValue("LessThan").set_visible(false);
            sender.findItemByValue("LessThanOrEqualTo").set_visible(false);
            sender.findItemByValue("NotEqualTo").set_visible(false);
            sender.findItemByValue("EqualTo").set_visible(false);
            sender.findItemByValue("StartsWith").set_visible(false);
            sender.findItemByValue("EndsWith").set_visible(false);
            sender.findItemByValue("Between").set_visible(false);
            sender.findItemByValue("NotBetween").set_visible(false);
            //if (filter._expressionItems[currentExpandedItem] == "Machine")
            //{
            //    sender.findItemByValue("Between").set_visible(true);
            //    sender.findItemByValue("EqualTo").set_visible(true);
            //}
            //sender.findItemByValue("Machine").set_visible(false);
        }--%>

        var popUp;
        function PopUpShowing(sender, eventArgs) {
            popUp = eventArgs.get_popUp();
            var gridWidth = sender.get_element().offsetWidth;
            var gridHeight = sender.get_element().offsetHeight;
            //var popUpWidth = popUp.style.width.substr(0, popUp.style.width.indexOf("px"));
            var popUpHeight = popUp.style.height.substr(0, popUp.style.height.indexOf("px"));
            popUp.style.left = "0px";//((gridWidth - popUpWidth) / 2 + sender.get_element().offsetLeft).toString() + "px";
            popUp.style.top = ((gridHeight - popUpHeight) / 2 + sender.get_element().offsetTop).toString() + "px";
        }

        function OnClientDropDownClosedHandler(sender, eventArgs) {
            document.activeElement.blur();
        }

        function OnEntryAdded(sender, eventArgs) {
            document.activeElement.blur();
        }

        function BatchEditCellValueChanged(sender, args) {
            var batchEditingManager = sender.get_batchEditingManager();
            batchEditingManager.saveAllChanges();
        }
    </script>
</telerik:RadCodeBlock>

<style type="text/css">
    .RadPicker.RadDateTimePicker.RadPicker_Bootstrap.rfControl{
        width: 211px !important;
    }

    [id$=dlgAddTransaction_wizard] div.rwzContent {
        height: 336px !important;
    }
</style>

<%--<telerik:RadFilter RenderMode="Lightweight" runat="server" ID="TransFilter" ExpressionPreviewPosition="None" OperationMode="ServerAndClient"
    OnApplyExpressions="TransFilter_ApplyExpressions" OnExpressionItemCreated="TransFilter_ExpressionItemCreated"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <FieldEditors>
        <telerik:RadFilterDropDownEditor FieldName="Machine" DisplayName="Machine" DataSourceID="edsMachine"
            DataTextField="Description" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />
        <telerik:RadFilterDateFieldEditor FieldName="Date" DisplayName="Date" PickerType="DateTimePicker" DateFormat="dd/MM/yyyy HH:mm:ss"
            DefaultFilterFunction="Between" />
    </FieldEditors>
    <ClientSettings>
        <ClientEvents OnFilterCreated="FilterCreated" />
    </ClientSettings>
</telerik:RadFilter>--%>

<asp:UpdatePanel runat="server">
    <ContentTemplate>

<div class="panel panel-info">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="machineFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px"
                    TextSettings-SelectionMode="Single" DataSourceID="edsMachine" DataTextField="Description" DataValueField="Id" InputType="Text" Filter="Contains" Label="Macchina: ">
                </telerik:RadAutoCompleteBox>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDateTimePicker runat="server" ID="dtPickStart" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDateTimePicker>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDateTimePicker runat="server" ID="dtPickEnd" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDateTimePicker>
            </div>
        </div>
        <%--<div class="row">
            <asp:CheckBoxList runat="server" AutoPostBack="false">
                <asp:ListItem Text="Tutti" Selected="true" />
                <asp:ListItem Text="Produzione" />
                <asp:ListItem Text="Fermi" />
            </asp:CheckBoxList>
        </div>--%>
        <%--<br />--%>
        <div style="text-align:right;">
            <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
        </div>
    </div>
</div>

        <mcf:Alert runat="server" id="AlertMessage" />
        <br />
<telerik:RadGrid runat="server" ID="transactionTable" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="true" AllowAutomaticUpdates="false"
    OnNeedDataSource="transactionTable_NeedDataSource" OnItemDataBound="transactionTable_ItemDataBound" OnUpdateCommand="transactionTable_UpdateCommand" OnItemCommand="transactionTable_ItemCommand"
    OnCustomAggregate="transactionTable_CustomAggregate" LocalizationPath="~/App_GlobalResources/" Culture="en-US" PageSize="25" AllowMultiRowEdit="true" AllowMultiRowSelection="true" ShowFooter="true">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView FilterExpression="" Caption="" CommandItemDisplay="Top" EditMode="EditForms" DataKeyNames="Id">
        <EditFormSettings UserControlName="~/Dashboard/Modules/Seneca/TransactionList/TransactionEdit.ascx" EditFormType="WebUserControl">
            <EditColumn UniqueName="EditCommandColumn">
            </EditColumn>
        </EditFormSettings>
        <CommandItemTemplate>
            <telerik:RadButton ID="btnModifySelectedRows" Text="Modifica selezioni" runat="server" CssClass="btn" CommandName="ModifySelectedRows" BackColor="Transparent" BorderStyle="None">
                <Icon PrimaryIconCssClass="rbEdit" />
            </telerik:RadButton>
            <telerik:RadButton ID="btnVerifiedSelectedRows" Text="Verifica selezioni" runat="server" CssClass="btn" CommandName="VerifiedSelectedRows" BackColor="Transparent" BorderStyle="None">
                <Icon PrimaryIconCssClass="rbOk" />
            </telerik:RadButton>
            <telerik:RadButton ID="btnCancelVerified" Text="Annulla verifica" runat="server" CssClass="btn" CommandName="CancelVerified" BackColor="Transparent" BorderStyle="None">
                <Icon PrimaryIconCssClass="rbCancel" />
            </telerik:RadButton>
            <div style="float:right;">
                <telerik:RadButton runat="server" Text="Refresh" CssClass="btn" CommandName="Refresh" BackColor="Transparent" BorderStyle="None">
                    <Icon PrimaryIconCssClass="rbRefresh" />
                </telerik:RadButton>
            </div>
        </CommandItemTemplate>
        <CommandItemSettings ShowRefreshButton="true" ShowAddNewRecordButton="false" />
        <Columns>
            <telerik:GridClientSelectColumn UniqueName="SelectColumn">
            </telerik:GridClientSelectColumn>
            <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" ItemStyle-Width="40px" />
            <telerik:GridTemplateColumn UniqueName="AddCommandColumn">
                <ItemTemplate>
                    <telerik:RadButton ID="btnAddRow" runat="server" CssClass="btn" CommandName="AddTransaction" BackColor="Transparent" BorderStyle="None">
                        <Icon PrimaryIconCssClass="rbAdd" />
                    </telerik:RadButton>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <%--<telerik:GridBoundColumn DataField="MachineId" HeaderText="Macchina">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>--%>
            <telerik:GridDropDownColumn DataField="MachineId" HeaderText="Macchina"
                ListValueField="Id" ListTextField="Description" DataSourceID="edsMachine">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="200px" />
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="Status" HeaderText="Stato">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="Operatore">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Cause.Description" HeaderText="Causale">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridDropDownColumn DataField="OperatorId" HeaderText="Operatore"
                ListValueField="Id" ListTextField="UserName" DataSourceID="edsOperator"
                EnableEmptyListItem="true" ConvertEmptyStringToNull="true">
                <HeaderStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn DataField="CauseId" HeaderText="Causale"
                ListValueField="Id" ListTextField="Description" DataSourceID="edsCause">
                <HeaderStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>--%>
            <telerik:GridBoundColumn DataField="Job.Order.CustomerOrder.OrderCode" HeaderText="Commessa">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridDropDownColumn DataField="JobId" HeaderText="Job"
                ListValueField="Id" ListTextField="Code" DataSourceID="edsJob"
                EnableEmptyListItem="true" ConvertEmptyStringToNull="true">
                <HeaderStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>--%>
            <%--<telerik:GridDropDownColumn DataField="IdArticle" HeaderText="Articolo"
                ListValueField="Id" ListTextField="Code" DataSourceID="edsArticle"
                EnableEmptyListItem="true" ConvertEmptyStringToNull="true">
                <HeaderStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>--%>
            <telerik:GridBoundColumn DataField="Job.Order.Article.Code" HeaderText="Articolo">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridDateTimeColumn DataField="Start" HeaderText="Start" PickerType="DateTimePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="End" HeaderText="End" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <%--<telerik:GridBoundColumn DataField="StandardRate" HeaderText="Stampate<br />ora" DataFormatString="{0:n2}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>--%>
            <telerik:GridBoundColumn DataField="PartialCounting" HeaderText="Colpi" DataFormatString="{0:n0}" Aggregate="Custom" FooterText="Tot:" UniqueName="PartialCounting">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="BlankShot" HeaderText="Colpi a vuoto" DataFormatString="{0:n0}" Aggregate="Sum" FooterText="Tot:">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CavityMoldNum" HeaderText="Impronte" DataFormatString="{0:n0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="QtyProduced" HeaderText="Pezzi<br />prodotti" DataFormatString="{0:n0}" Aggregate="Sum" FooterText="Tot:">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="QtyProductionWaste" HeaderText="Scarti" DataFormatString="{0:n0}" Aggregate="Sum" FooterText="Tot:">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridBoundColumn DataField="Open" HeaderText="Open">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>--%>
            <telerik:GridCheckBoxColumn DataField="Verified" HeaderText="Verificata">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridCheckBoxColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <Selecting AllowRowSelect="True"></Selecting>
        <ClientEvents OnPopUpShowing="PopUpShowing" />
    </ClientSettings>
</telerik:RadGrid>

    </ContentTemplate>
</asp:UpdatePanel>

<mcf:PopUpDialog ID="ModifyTransactionsWindow" runat="server" Width="100%" MarginLeft="0%" Title="Modifica transazioni">
    <Body>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlModifyOperator" GroupingText="Cambio operatore">
                    <asp:Label Text="Operatore:" runat="server" Width="75px" Font-Bold="true" />
                    <telerik:RadComboBox RenderMode="Lightweight" ID="ddlChangeOperator" AllowCustomText="true" runat="server" Filter="Contains" AutoPostBack="true"
                        DataSourceID="edsOperator" DataValueField="Id" DataTextField="UserName" EmptyMessage="Seleziona operatore">
                    </telerik:RadComboBox>
                </asp:Panel>
                <br />
                <asp:Panel runat="server" ID="pnlModifyOrder" GroupingText="Cambio commessa">
                   <div class="row">
                       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <telerik:RadButton ID="btnSearchOrder" runat="server" CssClass="btnActivity" OnClick="btnSearchOrder_Click" style="float:left;">
                               <ContentTemplate>
                                   <center><i class="fa fa-search" aria-hidden="true"></i> Altre commesse</center>
                               </ContentTemplate>
                           </telerik:RadButton>
                           <telerik:RadTextBox ID="txtSearchOrder" runat="server" EmptyMessage="Inserisci codice" Visible="false"
                               AutoPostBack="true" OnTextChanged="txtSearchOrder_TextChanged" />
                           <asp:Panel ID="pnlTastieraCommessa" Width="300px" runat="server" Visible="false" style="float:left;">
                               <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="7" OnClientClick="ModifyOrderCode(this);return false;" />
                               <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="8" OnClientClick="ModifyOrderCode(this);return false;" />
                               <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="9" OnClientClick="ModifyOrderCode(this);return false;" />

                               <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="4" OnClientClick="ModifyOrderCode(this);return false;" />
                               <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="5" OnClientClick="ModifyOrderCode(this);return false;" />
                               <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="6" OnClientClick="ModifyOrderCode(this);return false;" />

                               <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="1" OnClientClick="ModifyOrderCode(this);return false;" />
                               <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="2" OnClientClick="ModifyOrderCode(this);return false;" />
                               <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="3" OnClientClick="ModifyOrderCode(this);return false;" />
                               <asp:Button AutoPostBack="false" Width="174px" Height="50px" runat="server" Text="0" OnClientClick="ModifyOrderCode(this);return false;" />
                               <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyOrderCode(this);return false;" />
                           </asp:Panel>
                           <script type="text/javascript">
                               function ModifyOrderCode(sender) {
                                   var textBox = $find('<%= txtSearchOrder.ClientID %>');
                                   var txtValue = textBox.get_value();
                                   var char = sender.value;//sender.get_text();
                                   if (char == "Canc") {
                                       if (txtValue.length > 0) {
                                           textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                                       }
                                   }
                                   else {
                                       if (char == ',' || char == '.') {
                                           if (txtValue.includes(',') || txtValue.includes('.')) { }
                                           else {
                                               if (txtValue.length == 0) {
                                                   textBox.set_value('0' + char);
                                               }
                                               else {
                                                   textBox.set_value(txtValue + char);
                                               }
                                           }
                                       }
                                       else {
                                           textBox.set_value(txtValue + char);
                                       }
                                   }
                               }
                            </script>
                        </div>
                    </div>
                    <br />
                    <telerik:RadGrid runat="server" ID="gridSelectOrder" RenderMode="Lightweight" AutoGenerateColumns="false"
                        OnNeedDataSource="gridSelectOrder_NeedDataSource">
                        <MasterTableView DataKeyNames="Id" Caption="Seleziona nuova commessa" FilterExpression="" NoMasterRecordsText="Non ci sono commesse per questa macchina">
                            <Columns>
                                <telerik:GridBoundColumn DataField="Code" HeaderText="Fase">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Order.CustomerOrder.OrderCode" HeaderText="<%$ Resources:Mes,Order %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Order.Article.Code" HeaderText="<%$ Resources:Mes,Article %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <%--<telerik:GridBoundColumn DataField="Order.OrderCode" HeaderText="OP">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>--%>
                                <telerik:GridDateTimeColumn DataField="StartDate" HeaderText="Inizio previsto" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="Fine prevista" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridDateTimeColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>



                    <%--<asp:Label Text="Commessa:" runat="server" Width="75px" />
                    <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="ddlChangeOrder" EmptyMessage="Cerca commessa" EnableClientFiltering="true"
                        InputType="Text" Filter="Contains" DataSourceID="edsOrder" DataTextField="OrderCode" DataValueField="Id"
                        OnTextChanged="ddlChangeOrder_TextChanged" OnClientDropDownClosed="OnClientDropDownClosedHandler" OnClientEntryAdded="OnEntryAdded">
                        <TextSettings SelectionMode="Single" />
                    </telerik:RadAutoCompleteBox>
                    <telerik:RadComboBox RenderMode="Lightweight" ID="ddlChangeJob" AllowCustomText="true" runat="server" Filter="Contains"
                        DataValueField="Id" DataTextField="Description" EmptyMessage="Seleziona fase">
                    </telerik:RadComboBox>--%>
                </asp:Panel>
                <br />
                <div style="text-align:right;">
                    <telerik:RadButton runat="server" ID="btnConfirmModify" CssClass="btn" style="background-color:#5cb85c!important;border-color:#4cae4c!important;color:#fff!important;" OnClick="btnConfirmModify_Click">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton runat="server" ID="btnCancelModify" CssClass="btn" style="background-color:#d9534f!important;border-color:#d9534f!important;color:#fff!important;" OnClick="btnCancelModify_Click">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Body>
</mcf:PopUpDialog>

<mcf:PopUpDialog runat="server" ID="dlgAddTransaction" Title="Nuova transazione">
    <Body>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:HiddenField runat="server" ID="hiddenTId" />
                <telerik:RadWizard RenderMode="Lightweight" ID="wizard" runat="server" DisplayCancelButton="true" Height="500px"
                    OnClientLoad="OnClientLoad" OnClientButtonClicking="OnClientButtonClicking" OnCancelButtonClick="wizard_CancelButtonClick" OnNextButtonClick="wizard_NextButtonClick">
                    <Localization Next="Avanti" Previous="Indietro" Cancel="Annulla" Finish="Conferma" />
                    <WizardSteps>
                        <telerik:RadWizardStep Title="Commessa" StepType="Start">
                            <div class="inputWapper first">
                                <asp:Label Text="Commessa:" runat="server" AssociatedControlID="wizardTxtOrder" />
                                <telerik:RadTextBox ID="wizardTxtOrder" runat="server" EmptyMessage="Inserisci codice" AutoPostBack="true" OnTextChanged="wizardTxtOrder_TextChanged" />
                                <telerik:RadGrid runat="server" ID="wizardGridOrder" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="true"
                                    OnNeedDataSource="wizardGridOrder_NeedDataSource">
                                    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
                                    <MasterTableView DataKeyNames="Id, Order.CustomerOrder.OrderCode" Caption="Seleziona nuova commessa" FilterExpression=""
                                        NoMasterRecordsText="Nessuna commessa con quel codice" Width="100%" PageSize="10">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="Code" HeaderText="Fase">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Order.CustomerOrder.OrderCode" HeaderText="<%$ Resources:Mes,Order %>">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Order.Article.Code" HeaderText="<%$ Resources:Mes,Article %>">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridBoundColumn>
                                            <%--<telerik:GridBoundColumn DataField="Order.OrderCode" HeaderText="OP">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridBoundColumn>--%>
                                            <telerik:GridDateTimeColumn DataField="StartDate" HeaderText="Inizio previsto" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy}">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridDateTimeColumn>
                                            <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="Fine prevista" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy}">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridDateTimeColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Scrolling AllowScroll="true" ScrollHeight="300px" />
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </div>
                        </telerik:RadWizardStep>
                        <telerik:RadWizardStep StepType="Step" Title="Operatore">
                            <asp:Label Text="Operatore:" runat="server" Width="75px" Font-Bold="true" />
                            <telerik:RadComboBox RenderMode="Lightweight" ID="wizardOperator" AllowCustomText="true" runat="server" Filter="Contains" AutoPostBack="false"
                                DataSourceID="edsOperator" DataValueField="Id" DataTextField="UserName" EmptyMessage="Seleziona operatore">
                            </telerik:RadComboBox>
                        </telerik:RadWizardStep>
                        <telerik:RadWizardStep StepType="Step" Title="Causale" ValidationGroup="Cause">
                            <asp:Label Text="Causale:" runat="server" AssociatedControlID="wizardDdlCause" />
                            <telerik:RadComboBox RenderMode="Lightweight" ID="wizardDdlCause" AllowCustomText="true" runat="server" Filter="Contains" AutoPostBack="false"
                                DataSourceID="edsCause" DataValueField="Id" DataTextField="Description" EmptyMessage="Seleziona causale">
                            </telerik:RadComboBox>
                            <asp:RequiredFieldValidator runat="server" ID="wizardCauseValidator" ControlToValidate="wizardDdlCause"
                                Display="Dynamic" ErrorMessage="Selezionare una causale" ValidationGroup="Cause" />
                        </telerik:RadWizardStep>
                        <telerik:RadWizardStep StepType="Step" Title="Colpi">
                            <asp:Label Text="Colpi:" runat="server" AssociatedControlID="wizardTxtColpi" />
                            <telerik:RadNumericTextBox runat="server" ID="wizardTxtColpi" MinValue="0" EmptyMessage="Inserisci colpi" style="text-align:right;">
                                <NumberFormat DecimalDigits="0" />
                            </telerik:RadNumericTextBox>
                        </telerik:RadWizardStep>
                        <telerik:RadWizardStep StepType="Step" Title="Tempo" ValidationGroup="validationTime">
                            <div class="row" style="width:100%;">
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <asp:Label Text="Inizio:" runat="server" AssociatedControlID="wizardDtStart" />
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <telerik:RadDateTimePicker runat="server" ID="wizardDtStart" Width="100%" Visible="true">
                                        <DateInput runat="server" DateFormat="dd/MM/yyyy HH:mm:ss">
                                        </DateInput>
                                    </telerik:RadDateTimePicker>
                                    <%--<telerik:RadTimePicker runat="server" ID="wizardTimeStart" Visible="false">
                                    </telerik:RadTimePicker>--%>
                                    <asp:RangeValidator ID="wizardDateValidator" runat="server" ControlToValidate="wizardDtStart"
                                        Display="Dynamic" ValidationGroup="validationTime">
                                    </asp:RangeValidator>
                                </div>
                            </div>
                        </telerik:RadWizardStep>
                        <telerik:RadWizardStep runat="server" StepType="Finish" Title="Riepilogo" DisplayCancelButton="true">
                            <div class="row" style="width:100%;">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <asp:Label Text="Riepilogo:" runat="server" Font-Bold="true" />
                                </div>
                            </div>
                            <div class="row style="width:100%;"">
                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                    <asp:Label ID="wizardLblOrder" runat="server" />
                                </div>
                            </div>
                            <div class="row" style="width:100%;">
                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                    <asp:Label ID="wizardLblOperator" Text="" runat="server" />
                                </div>
                            </div>
                            <div class="row" style="width:100%;">
                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                    <asp:Label ID="wizardLblCause" Text="" runat="server" />
                                </div>
                            </div>
                            <div class="row" style="width:100%;">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <asp:Label ID="wizardLblShot" Text="" runat="server" />
                                </div>
                            </div>
                            <div class="row" style="width:100%;">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <asp:Label ID="wizardLblStart" Text="" runat="server" />
                                </div>
                            </div>
                            <div class="row" style="width:100%;">
                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                    <asp:Label ID="wizardLblEnd" Text="" runat="server" />
                                </div>
                            </div>
                            <br />
                            <telerik:RadButton RenderMode="Lightweight" ID="btnConfirmNewTransaction" runat="server" Text="Conferma nuova transazione"
                                OnClick="btnConfirmNewTransaction_Click">
                            </telerik:RadButton>
                        </telerik:RadWizardStep>
                    </WizardSteps>
                </telerik:RadWizard>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Body>
</mcf:PopUpDialog>


<ef:EntityDataSource ID="edsMachine" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    AutoGenerateWhereClause="true" OrderBy="it.Description">
</ef:EntityDataSource>
<ef:EntityDataSource ID="edsOperator" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Operators"
    OrderBy="it.UserName" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsCause" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Causes"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsOrder" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="CustomerOrders"
    OrderBy="it.OrderCode" AutoGenerateWhereClause="true" />
<%--<ef:EntityDataSource ID="edsJob" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Jobs"
    OrderBy="it.Code" AutoGenerateWhereClause="true" />--%>

<script type="text/javascript">
    (function () {

        window.OnClientLoad = function (sender, args) {
            for (var i = 1; i < sender.get_wizardSteps().get_count() ; i++) {
                sender.get_wizardSteps().getWizardStep(i).set_enabled(false);
            }
            var $ = $telerik.$;
            $(".rwzFinish").hide();
        }

        window.OnClientButtonClicking = function (sender, args) {
            if (!args.get_nextActiveStep().get_enabled()) {
                args.get_nextActiveStep().set_enabled(true);
            }
        }
    })();
</script>