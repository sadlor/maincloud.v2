﻿using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.TransactionList
{
    public partial class TransactionList_View : DataWidget<TransactionList_View>
    {
        public TransactionList_View() : base(typeof(TransactionList_View)) {}

        private const string REMOVE_OPERATOR = "RemoveOperator";
        TransactionRepository repos = new TransactionRepository();
        //TransactionService transService = new TransactionService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                VerifyRedirect();
                if (dtPickStart.SelectedDate == null)
                {
                    dtPickStart.SelectedDate = DateTime.Today.AddDays(-1);
                }
                if (dtPickEnd.SelectedDate == null)
                {
                    dtPickEnd.SelectedDate = DateTime.Today;
                }
            }
            edsMachine.WhereParameters.Clear();
            edsMachine.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsOperator.WhereParameters.Clear();
            edsOperator.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsCause.WhereParameters.Clear();
            edsCause.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsOrder.WhereParameters.Clear();
            edsOrder.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            //edsJob.WhereParameters.Clear();
            //edsJob.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        private void VerifyRedirect()
        {
            if (SessionMultitenant.ContainsKey(MesConstants.TRANSACTION_ID_TO_MODIFY) && SessionMultitenant[MesConstants.TRANSACTION_ID_TO_MODIFY] != null)
            {
                int transId = Convert.ToInt32(SessionMultitenant[MesConstants.TRANSACTION_ID_TO_MODIFY]);
                Transaction t = repos.FindByID(transId);
                dtPickStart.SelectedDate = t.Start.Date;
                dtPickEnd.SelectedDate = t.Start.Date;
                AssetRepository ar = new AssetRepository();
                machineFilter.Entries.Add(new AutoCompleteBoxEntry(ar.FindByID(t.MachineId).Description, t.MachineId));
                CreateFilter();
            }
            if (SessionMultitenant.ContainsKey(MesConstants.ASSET_TO_MODIFY_TRANSACTIONS) && SessionMultitenant[MesConstants.ASSET_TO_MODIFY_TRANSACTIONS] != null)
            {
                string machineId = SessionMultitenant[MesConstants.ASSET_TO_MODIFY_TRANSACTIONS].ToString();
                var query = repos.ReadAll(x => x.MachineId == machineId && (x.PartialCounting > 0 || x.QtyProductionWaste > 0) && !x.Verified && string.IsNullOrEmpty(x.JobId));
                dtPickStart.SelectedDate = query.Min(x => x.Start);
                dtPickEnd.SelectedDate = query.Max(x => x.Start);
                AssetRepository ar = new AssetRepository();
                machineFilter.Entries.Add(new AutoCompleteBoxEntry(ar.GetDescription(machineId), machineId));
                CreateFilter();
            }
            if (SessionMultitenant.ContainsKey(MesConstants.ASSET_TO_VIEW_TRANSACTIONS) && SessionMultitenant[MesConstants.ASSET_TO_VIEW_TRANSACTIONS] != null)
            {
                string machineId = SessionMultitenant[MesConstants.ASSET_TO_VIEW_TRANSACTIONS].ToString();
                dtPickStart.SelectedDate = DateTime.Today;
                dtPickEnd.SelectedDate = DateTime.Now;
                AssetRepository ar = new AssetRepository();
                machineFilter.Entries.Add(new AutoCompleteBoxEntry(ar.GetDescription(machineId), machineId));
                CreateFilter();
            }
        }

        protected string AssetId
        {
            get
            {
                if (string.IsNullOrEmpty((string)ViewState["TransactionListAssetId"]))
                {
                    return null;
                }
                return ViewState["TransactionListAssetId"].ToString();
            }
            set
            {
                ViewState["TransactionListAssetId"] = value;
            }
        }

        protected DateTime? DateStart
        {
            get
            {
                if (ViewState["TransactionListDateStart"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["TransactionListDateStart"]);
            }
            set
            {
                ViewState["TransactionListDateStart"] = value;
            }
        }

        protected DateTime? DateEnd
        {
            get
            {
                if (ViewState["TransactionListDateEnd"] == null)
                {
                    return null;
                }
                return Convert.ToDateTime(ViewState["TransactionListDateEnd"]);
            }
            set
            {
                ViewState["TransactionListDateEnd"] = value;
            }
        }

        #region Filter
        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            if (SessionMultitenant.ContainsKey(MesConstants.TRANSACTION_ID_TO_MODIFY))
            {
                object obj;
                SessionMultitenant.TryRemove(MesConstants.TRANSACTION_ID_TO_MODIFY, out obj);
            }
            if (SessionMultitenant.ContainsKey(MesConstants.ASSET_TO_MODIFY_TRANSACTIONS))
            {
                object obj;
                SessionMultitenant.TryRemove(MesConstants.ASSET_TO_MODIFY_TRANSACTIONS, out obj);
            }
            CreateFilter();
        }

        private void CreateFilter()
        {
            string assetId = machineFilter.Entries.Count > 0 ? machineFilter.Entries[0].Value : string.Empty;
            DateTime? start = dtPickStart.SelectedDate;
            DateTime? end = dtPickEnd.SelectedDate;

            AssetId = assetId;
            DateStart = start.Value;
            DateEnd = end.Value;

            transactionTable.Rebind();
        }
        #endregion

        protected void transactionTable_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(AssetId))
            {
                List<Transaction> dataSource = new List<Transaction>();
                IQueryable<Transaction> returnList = null;

                returnList = repos.ReadAll(x => x.MachineId == AssetId);

                DateTime? end = DateEnd.Value;//.AddDays(1);

                if (returnList != null && returnList.Count() > 0)
                {
                    if (DateStart.HasValue)
                    {
                        if (end.HasValue)
                        {
                            //dataSource = returnList.Where(x => x.Start >= DateStart && x.Start <= end && !x.Open).OrderByDescending(x => x.Start).ToList();
                            dataSource = returnList.Where(x => x.Start >= DateStart && x.Start <= end).OrderByDescending(x => x.Start).ToList();
                        }
                        else
                        {
                            //dataSource = returnList.Where(x => x.Start >= DateStart && !x.Open).OrderByDescending(x => x.Start).ToList();
                            dataSource = returnList.Where(x => x.Start >= DateStart).OrderByDescending(x => x.Start).ToList();
                        }
                    }
                    else
                    {
                        dataSource = returnList.ToList();
                    }
                }
                else
                {
                    if (DateStart.HasValue)
                    {
                        if (end.HasValue)
                        {
                            //dataSource = repos.ReadAll(x => x.Start >= DateStart && x.Start <= end && !x.Open).OrderByDescending(x => x.Start).ToList();
                            dataSource = repos.ReadAll(x => x.Start >= DateStart && x.Start <= end).OrderByDescending(x => x.Start).ToList();
                        }
                        else
                        {
                            //dataSource = repos.ReadAll(x => x.Start >= DateStart && !x.Open).OrderByDescending(x => x.Start).ToList();
                            dataSource = repos.ReadAll(x => x.Start >= DateStart).OrderByDescending(x => x.Start).ToList();
                        }
                    }
                }

                transactionTable.DataSource = dataSource;
            }
        }

        protected void transactionTable_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;

                if ((item.DataItem as Transaction).Open)
                {
                    item["AddCommandColumn"].Controls[1].Visible = false;
                }

                item["Status"].Text = item["Status"].Text == "False" ? "Off" : "On";

                if ((item.DataItem as Transaction).Exported)
                {
                    item.SelectableMode = GridItemSelectableMode.None;
                    item["SelectColumn"].Controls[0].Visible = false;
                    item["EditCommandColumn"].Controls[0].Visible = false;
                }
                else
                {
                    item.SelectableMode = GridItemSelectableMode.ServerAndClientSide;
                }

                if (!IsPostBack)
                {
                    if (SessionMultitenant.ContainsKey(MesConstants.TRANSACTION_ID_TO_MODIFY) && ((Transaction)e.Item.DataItem).Id == Convert.ToInt32(SessionMultitenant[MesConstants.TRANSACTION_ID_TO_MODIFY]))
                    {
                        item.Edit = true;
                        //item.FireCommandEvent(RadGrid.EditCommandName, new GridCommandEventArgs(item, null, new CommandEventArgs(RadGrid.EditCommandName, item)));
                    }
                }
            }
        }

        protected void transactionTable_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            //Hashtable values = new Hashtable();
            //editableItem.ExtractValues(values);

            var transId = editableItem.GetDataKeyValue("Id");

            Transaction data = repos.FindByID(transId);
            if (data != null)
            {
                UserControl userControl = (UserControl)e.Item.FindControl(GridEditFormItem.EditFormUserControlID);
                //Update new values
                Hashtable newValues = new Hashtable();
                if (!string.IsNullOrEmpty((userControl.FindControl("ddlOperator") as RadComboBox).SelectedValue))
                {
                    if ((userControl.FindControl("ddlOperator") as RadComboBox).SelectedValue == REMOVE_OPERATOR)
                    {
                        data.OperatorId = null;
                    }
                    else
                    {
                        data.OperatorId = (userControl.FindControl("ddlOperator") as RadComboBox).SelectedValue;
                    }
                }
                if (!string.IsNullOrEmpty((userControl.FindControl("ddlCause") as RadComboBox).SelectedValue))
                {
                    data.CauseId = (userControl.FindControl("ddlCause") as RadComboBox).SelectedValue;
                }
                if (!string.IsNullOrEmpty((userControl.FindControl("ddlJob") as RadComboBox).SelectedValue))
                {
                    data.JobId = (userControl.FindControl("ddlJob") as RadComboBox).SelectedValue;
                    JobRepository jr = new JobRepository();
                    Job j = jr.FindByID(data.JobId);
                    data.StandardRate = j.StandardRate;
                    data.CavityMoldNum = j.CavityMoldNum ?? 1;
                    if (data.PartialCounting > 0)
                    {
                        data.QtyProduced = (data.PartialCounting - data.BlankShot) * (data.CavityMoldNum > 0 ? data.CavityMoldNum : 1);
                    }
                }
                if (!string.IsNullOrEmpty((userControl.FindControl("txtNewCavityMoldNum") as RadNumericTextBox).Text))
                {
                    int num = Convert.ToInt32((userControl.FindControl("txtNewCavityMoldNum") as RadNumericTextBox).Text);
                    data.CavityMoldNum = num > 0 ? num : 1;
                    if (data.PartialCounting > 0)
                    {
                        data.QtyProduced = (data.PartialCounting - data.BlankShot) * (data.CavityMoldNum > 0 ? data.CavityMoldNum : 1);
                    }
                }

                if ((userControl.FindControl("hiddenIsBlankShotModify") as HiddenField).Value == bool.TrueString)
                {
                    RadGrid grid = userControl.FindControl("gridBlankShot") as RadGrid;
                    int blankShot = Convert.ToInt32(grid.Items[0].GetDataKeyValue("NewValue"));
                    if ((data.PartialCounting - data.BlankShot) >= blankShot)
                    {
                        data.BlankShot += blankShot;
                        data.QtyProduced = (data.PartialCounting - data.BlankShot) * (data.CavityMoldNum > 0 ? data.CavityMoldNum : 1);
                        data.QtyOK = data.QtyProduced - data.QtyProductionWaste;
                    }
                }
                if ((userControl.FindControl("hiddenIsModify") as HiddenField).Value == bool.TrueString)
                {
                    JobProductionWasteService jpws = new JobProductionWasteService();
                    JobProductionWasteRepository jpwr = new JobProductionWasteRepository();
                    RadGrid grid = userControl.FindControl("gridScarti") as RadGrid;
                    List<JobProductionWaste> wasteList = jpws.GetWasteListPerTransaction(data.Id, data.JobId);
                    foreach (GridDataItem item in grid.Items)
                    {
                        string causeId = item.GetDataKeyValue("CauseId").ToString();
                        int waste = Convert.ToInt32(item.GetDataKeyValue("NewValue"));
                        //se waste > 0 controllo che non esista già -> se esiste lo modifico, altrimenti lo creo
                        //se waste = 0 controllo prima che esista la causale -> se si, la elimino, altrimenti non faccio niente
                        if (waste != 0)
                        {
                            if (wasteList.Where(x => x.CauseId == causeId && x.Quality).Any())
                            {
                                wasteList.Where(x => x.CauseId == causeId && x.Quality).First().QtyProductionWaste += waste;
                            }
                            else
                            {
                                //non esiste, quindi lo creo
                                JobProductionWaste jpw = new JobProductionWaste();
                                jpw.AssetId = data.MachineId;
                                jpw.CauseId = causeId;
                                jpw.Date = data.Start;
                                jpw.JobId = data.JobId;
                                jpw.OperatorId = data.OperatorId;
                                jpw.QtyShotProduced = data.PartialCounting - data.BlankShot;
                                jpw.QtyProduced = (data.PartialCounting - data.BlankShot) * data.CavityMoldNum;
                                jpw.QtyProductionWaste = waste;
                                jpw.ArticleId = data?.Job?.Order?.IdArticle;
                                jpw.TransactionId = data.Id;
                                jpw.Quality = true;
                                jpwr.Insert(jpw);
                            }
                        }
                    }
                    jpwr.UpdateAll(wasteList);
                    jpwr.SaveChanges();
                    data.QtyProductionWaste = jpws.GetWasteListPerTransaction(data.Id, data.JobId).Sum(x => x.QtyProductionWaste);
                    data.QtyOK = data.QtyProduced - data.QtyProductionWaste;
                }

                repos.Update(data);
                try
                {
                    repos.SaveChanges();
                }
                catch (Exception ex)
                {
                    AlertMessage.ShowError("Si è verificato un problema con l'update, operazione annullata. " + ex.Message);
                    e.Canceled = true;
                }
            }

            //if (data != null)
            //{
            //    editableItem.UpdateValues(data);

            //    if (string.IsNullOrEmpty(data.CauseId))
            //    {
            //        data.CauseId = null;
            //    }
            //    if (string.IsNullOrEmpty(data.OperatorId))
            //    {
            //        data.OperatorId = null;
            //    }
            //    if (string.IsNullOrEmpty(data.IdArticle))
            //    {
            //        data.IdArticle = null;
            //    }
            //    if (string.IsNullOrEmpty(data.MoldId))
            //    {
            //        data.MoldId = null;
            //    }
            //    if (string.IsNullOrEmpty(data.MachineId))
            //    {
            //        data.MachineId = null;
            //    }
            //    if (string.IsNullOrEmpty(data.JobId))
            //    {
            //        data.JobId = null;
            //    }
            //    if (string.IsNullOrEmpty(data.EquipmentId))
            //    {
            //        data.EquipmentId = null;
            //    }
                
                //data.ProductionComply = data.PartialCounting - data.ProductionWaste;

            //    repos.Update(data);
            //try
            //{
            //    repos.SaveChanges();
            //}
            //catch (Exception ex)
            //{
            //    AlertMessage.ShowError("Si è verificato un problema con l'update, operazione annullata. " + ex.Message);
            //    e.Canceled = true;
            //}
        }

        protected void transactionTable_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ModifySelectedRows")
            {
                //ddlChangeOrder.Entries.Clear();
                //ddlChangeJob.Items.Clear();
                //ddlChangeJob.Text = string.Empty;
                ddlChangeOperator.Text = string.Empty;
                pnlModifyOperator.DataBind();
                ModifyTransactionsWindow.OpenDialog();
                txtSearchOrder.Visible = false;
                pnlTastieraCommessa.Visible = false;
                gridSelectOrder.Rebind();
            }
            if (e.CommandName == "VerifiedSelectedRows")
            {
                if (transactionTable.SelectedItems.Count > 0)
                {
                    foreach (GridEditableItem item in transactionTable.SelectedItems)
                    {
                        int tId = Convert.ToInt32(item.GetDataKeyValue("Id"));
                        Transaction t = repos.FindByID(tId);
                        t.Verified = true;
                        repos.Update(t);
                    }
                    repos.SaveChanges();
                    transactionTable.Rebind();
                }
            }
            if (e.CommandName == "CancelVerified")
            {
                if (transactionTable.SelectedItems.Count > 0)
                {
                    foreach (GridEditableItem item in transactionTable.SelectedItems)
                    {
                        int tId = Convert.ToInt32(item.GetDataKeyValue("Id"));
                        Transaction t = repos.FindByID(tId);
                        t.Verified = false;
                        repos.Update(t);
                    }
                    repos.SaveChanges();
                    transactionTable.Rebind();
                }
            }
            if (e.CommandName == "AddTransaction")
            {
                int index = (e.Item as GridDataItem).ItemIndex;
                string tId = (e.Item as GridDataItem).GetDataKeyValue("Id").ToString();
                hiddenTId.Value = tId;

                dlgAddTransaction.OpenDialog();

                Transaction t = repos.FindByID(Convert.ToInt32(tId));
                wizardTxtOrder.Text = t.Job?.Order?.CustomerOrder?.OrderCode;
                wizardGridOrder.Rebind();
                //if (t.End.Value.Date != t.Start.Date)
                //{
                //    //dichiarare data ora inizio
                //    wizardDtStart.Visible = true;
                //    wizardTimeStart.Visible = false;
                //    //wizardDtStart.MinDate = t.Start;
                //    //wizardDtStart.MaxDate = t.End.Value;
                //    wizardDateValidator.MinimumValue = t.Start.AddMinutes(1).ToString();
                //    wizardDateValidator.MaximumValue = t.End.Value.AddMinutes(-1).ToString();
                //    //wizardDtStart.TimeView.StartTime = t.Start.AddMinutes(1).TimeOfDay;
                //    //wizardDtStart.TimeView.EndTime = t.End.Value.AddMinutes(-1).TimeOfDay;
                //}
                //else
                //{
                //    //dichiarare solo ora inizio
                //    wizardDtStart.Visible = false;
                //    wizardTimeStart.Visible = true;
                //    wizardTimeStart.TimeView.StartTime = t.Start.AddMinutes(1).TimeOfDay;
                //    wizardTimeStart.TimeView.EndTime = t.End.Value.AddMinutes(-1).TimeOfDay;
                //}

                wizardDateValidator.MinimumValue = t.Start.AddMinutes(1).ToString("yyyy-MM-dd-HH-mm-ss");
                wizardDateValidator.MaximumValue = t.End.Value.AddMinutes(-1).ToString("yyyy-MM-dd-HH-mm-ss");
                wizardDateValidator.ErrorMessage = string.Format("Data-ora deve essere compreso tra {0} e {1}", t.Start.AddMinutes(1).ToString(), t.End.Value.AddMinutes(-1).ToString());

                wizardTxtColpi.MaxValue = (double)t.PartialCounting;
                wizardTxtColpi.Text = "0";
            }
        }

        protected void btnConfirmModify_Click(object sender, EventArgs e)
        {
            if (transactionTable.SelectedItems.Count > 0)
            {
                string jobId = string.Empty;
                if (gridSelectOrder.SelectedItems.Count > 0)
                {
                    jobId = (gridSelectOrder.SelectedItems[0] as GridEditableItem).GetDataKeyValue("Id").ToString(); //ddlChangeJob.SelectedValue;
                }
                string operatorId = ddlChangeOperator.SelectedValue;
                JobRepository jobRepos = new JobRepository();
                List<Transaction> list = new List<Transaction>();
                List<string> jobList = new List<string>();
                List<string> opList = new List<string>();
                foreach (GridEditableItem item in transactionTable.SelectedItems)
                {
                    int tId = Convert.ToInt32(item.GetDataKeyValue("Id"));
                    Transaction t = repos.FindByID(tId);
                    if (!string.IsNullOrEmpty(jobId) || !string.IsNullOrEmpty(operatorId))
                    {
                        if (!string.IsNullOrEmpty(jobId))
                        {
                            jobList.Add(t.JobId);
                            opList.Add(t.OperatorId);
                            t.JobId = jobId;
                            t.StandardRate = jobRepos.GetStandardRate(jobId);
                            t.CavityMoldNum = jobRepos.GetCavityMoldNum(jobId);
                            t.QtyProduced = (t.PartialCounting - t.BlankShot) * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
                        }
                        if (!string.IsNullOrEmpty(operatorId))
                        {
                            t.OperatorId = operatorId;
                        }
                        list.Add(t);
                    }
                }

                TransactionOperatorRepository opRep = new TransactionOperatorRepository();
                DateTime startComparison = list.Min(x => x.Start).AddMinutes(-5);
                DateTime endComparison = list.Max(x => x.Start).AddMinutes(5);
                List<TransactionOperator> listToModify = opRep.ReadAll(x => opList.Contains(x.OperatorId) && jobList.Contains(x.JobId) && (x.DateStart >= startComparison && x.DateStart < endComparison)).ToList();
                foreach (TransactionOperator tOp in listToModify)
                {
                    tOp.JobId = jobId;
                }
                opRep.UpdateAll(listToModify);
                opRep.SaveChanges();

                repos.UpdateAll(list);
                repos.SaveChanges();
            }
            transactionTable.Rebind();
            ModifyTransactionsWindow.CloseDialog();
        }

        protected void btnCancelModify_Click(object sender, EventArgs e)
        {
            ModifyTransactionsWindow.CloseDialog();
        }

        //protected void ddlChangeOrder_TextChanged(object sender, AutoCompleteTextEventArgs e)
        //{
        //   if (ddlChangeOrder.Entries.Count > 0)
        //    {
        //        int orderId = Convert.ToInt32(ddlChangeOrder.Entries[0].Value);

        //        JobService JService = new JobService();
        //        ddlChangeJob.DataSource = JService.GetJobListByCustomerOrder(orderId);
        //        ddlChangeJob.DataBind();
        //    }
        //}

        protected void gridSelectOrder_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (ModifyTransactionsWindow.IsOpen())
            {
                JobService jobService = new JobService();
                if (txtSearchOrder.Visible && txtSearchOrder.Text.Length > 0)
                {
                    gridSelectOrder.DataSource = jobService.GetJobListOrderCodeFiltered(txtSearchOrder.Text);
                }
                else
                {
                    gridSelectOrder.DataSource = jobService.GetProductionJobListByAsset(AssetId);
                }
            }
        }

        protected void btnSearchOrder_Click(object sender, EventArgs e)
        {
            txtSearchOrder.Visible = true;
            pnlTastieraCommessa.Visible = true;
        }

        protected void txtSearchOrder_TextChanged(object sender, EventArgs e)
        {
            gridSelectOrder.Rebind();
        }

        protected void transactionTable_CustomAggregate(object sender, GridCustomAggregateEventArgs e)
        {
            if (e.Column.UniqueName == "PartialCounting")
            {
                e.Result = (transactionTable.DataSource as List<Transaction>).Sum(x => x.PartialCounting - x.BlankShot);
            }
        }

        #region Wizard AddTransaction
        protected void wizard_CancelButtonClick(object sender, WizardEventArgs e)
        {
            wizardTxtOrder.Text = string.Empty;
            dlgAddTransaction.CloseDialog();
        }

        protected void wizardTxtOrder_TextChanged(object sender, EventArgs e)
        {
            wizardGridOrder.Rebind();
        }

        protected void wizardGridOrder_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(wizardTxtOrder.Text)) //dlgAddTransaction.IsOpen() && 
            {
                JobService jobService = new JobService();
                wizardGridOrder.DataSource = jobService.GetJobListOrderCodeFiltered(wizardTxtOrder.Text);
            }
        }

        protected void wizard_NextButtonClick(object sender, WizardEventArgs e)
        {
            if (e.NextStep.StepType == RadWizardStepType.Finish)
            {
                btnConfirmNewTransaction.Enabled = true;
                if (wizardGridOrder.SelectedItems.Count > 0)
                {
                    wizardLblOrder.Text = string.Format("Commessa: {0}", (wizardGridOrder.SelectedItems[0] as GridEditableItem).GetDataKeyValue("Order.CustomerOrder.OrderCode").ToString());
                }
                else
                {
                    wizardLblOrder.Text = string.Format("Commessa:");
                    btnConfirmNewTransaction.Enabled = false;
                }
                wizardLblOperator.Text = string.Format("Operatore: {0}", wizardOperator.Text);
                wizardLblCause.Text = string.Format("Causale: {0}", wizardDdlCause.Text);
                wizardLblShot.Text = string.Format("Colpi: {0}", wizardTxtColpi.Text);
                if (wizardDtStart.SelectedDate.HasValue)
                {
                    wizardLblStart.Text = string.Format("Inizio: {0}", wizardDtStart.SelectedDate.Value.ToString());
                }
                else
                {
                    wizardLblStart.Text = string.Format("Inizio:");
                    btnConfirmNewTransaction.Enabled = false;
                }
            }
        }

        protected void btnConfirmNewTransaction_Click(object sender, EventArgs e)
        {
            Transaction t = repos.FindByID(Convert.ToInt32(hiddenTId.Value));

            Transaction newT = new Transaction();
            newT.MachineId = t.MachineId;
            newT.Status = t.Status;

            newT.JobId = (wizardGridOrder.SelectedItems[0] as GridEditableItem).GetDataKeyValue("Id").ToString();
            JobRepository jr = new JobRepository();
            Job j = jr.FindByID(newT.JobId);
            newT.StandardRate = j.StandardRate;
            newT.CavityMoldNum = j.CavityMoldNum ?? 1;
            
            newT.OperatorId = !string.IsNullOrEmpty(wizardOperator.SelectedValue) ? wizardOperator.SelectedValue : null;
            newT.CauseId = !string.IsNullOrEmpty(wizardDdlCause.SelectedValue) ? wizardDdlCause.SelectedValue : null;
            newT.PartialCounting = Convert.ToDecimal(wizardTxtColpi.Text);
            newT.QtyProduced = newT.PartialCounting * newT.CavityMoldNum;
            newT.Start = wizardDtStart.SelectedDate.Value;
            newT.End = t.End;
            newT.Duration = (decimal)(newT.End - newT.Start).Value.TotalSeconds;
            newT.LastUpdate = DateTime.Now;
            newT.Open = false;
            newT.Verified = false;
            newT.Exported = false;
            newT.IsOrderClosed = null;

            //Modify previous transaction
            if (t.PartialCounting > 0 && newT.PartialCounting > 0)
            {
                t.PartialCounting -= newT.PartialCounting;
                t.QtyProduced = (t.PartialCounting - t.BlankShot) * t.CavityMoldNum;
                t.CounterEnd = t.CounterStart + t.PartialCounting;
            }
            newT.CounterStart = t.CounterEnd.Value;
            newT.CounterEnd = newT.CounterStart + newT.PartialCounting;
            t.End = newT.Start;
            t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
            t.LastUpdate = DateTime.Now;

            repos.Insert(newT);
            repos.Update(t);
            repos.SaveChanges();

            dlgAddTransaction.CloseDialog();
            transactionTable.Rebind();
        }
        #endregion
    }
}