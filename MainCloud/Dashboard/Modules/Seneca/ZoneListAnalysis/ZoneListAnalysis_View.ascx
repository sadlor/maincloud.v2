﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZoneListAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.ZoneListAnalysis.ZoneListAnalysis_View" %>

<telerik:RadGrid runat="server" RenderMode="Lightweight" ID="zoneTable" AutoGenerateColumns="false"
    OnNeedDataSource="zoneTable_NeedDataSource" OnDetailTableDataBind="zoneTable_DetailTableDataBind"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <MasterTableView FilterExpression="" Caption="Zone" DataKeyNames="Id"
        HierarchyDefaultExpanded="false" RetainExpandStateOnRebind="true" Name="Zone">
         <DetailTables>
            <telerik:GridTableView Name="MachineList">
                <Columns>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:Mes,Status %>">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Operator" HeaderText="<%$ Resources:Mes,Operator %>">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Article" HeaderText="<%$ Resources:Mes,Article %>">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                </Columns>
            </telerik:GridTableView>
        </DetailTables>
        <Columns>
            <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>