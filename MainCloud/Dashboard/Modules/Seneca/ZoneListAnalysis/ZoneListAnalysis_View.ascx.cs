﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.ZoneListAnalysis
{
    public partial class ZoneListAnalysis_View : DataWidget<ZoneListAnalysis_View>
    {
        public ZoneListAnalysis_View() : base(typeof(ZoneListAnalysis_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void zoneTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                ZoneRepository zoneRep = new ZoneRepository();
                zoneTable.DataSource = zoneRep.ReadAll().ToList();
            }
        }

        protected class MachineRecord
        {
            public string Description { get; set; }
            public string Status { get; set; }
            public string Operator { get; set; }
            public int? Article { get; set; }
        }

        protected void zoneTable_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            string zoneId = dataItem.GetDataKeyValue("Id").ToString();
            AssetRepository assetRep = new AssetRepository();
            List<Asset> assetList = assetRep.ReadAll(x => x.ZoneId == zoneId && x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
            TransactionService TService = new TransactionService();
            List<MachineRecord> dataSource = new List<MachineRecord>();
            foreach (Asset a in assetList)
            {
                Transaction t = TService.GetLastTransactionOpen(a.Id);
                MachineRecord mr = new MachineRecord();
                mr.Description = a.Description;
                mr.Status = t.Status ? "On" : "Off";
                mr.Operator = t.OperatorId;
                mr.Article = t.IdArticle;

                dataSource.Add(mr);
            }
            e.DetailTableView.DataSource = dataSource;
        }
    }
}