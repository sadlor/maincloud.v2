﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ControlPlansStats_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.ControlPlansStats.ControlPlansStats_View" %>

<link href="/Dashboard/Modules/Seneca/ControlPlansStats/ControlPlan.css" rel="stylesheet" type="text/css" />

<script type="text/javscript">
    $(document).ready(function() {
        bindEvents();   
    });

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function(evt, args) {
        bindEvents();
    });
</script>

<script type="text/javascript">
    var limit = 5;
    function OnClientDropDownOpening(sender, args) {
        if (sender.get_entries().get_count() == limit) {
            args.set_cancel(true);
        }
    }

    function OnClientEntryAdding(sender, args) {
        var entries = sender.get_entries();
        for (var i = 0; i < entries.get_count() ; i++) {
            if (entries.getEntry(i).get_value() == args.get_entry().get_value()) {
                args.set_cancel(true);
            }
        }
    }

    function OnOrderFilterChanged() {
        $("#jobFilter").click(function () {
            $("#orderFilterVal").val($("#orderFilter .racTextToken").text());
            $("#jobList").val("");
            $("#updateFlag").val(1);
            __doPostBack('<%=orderFilter.ClientID %>', "updateFilter");
        });
    }

    function selectJob(index, value) {
	    $("#jobFilter_DropDown .rddlList > li").each(function(i) {
		    var txt = $(this).text(), flg = ((index > -1 && i == index) || (index == -1 && $(this).text() == value));
		
		    if (flg) {
			    if (!$(this).hasClass("rddlItemSelected"))
                    $(this).addClass("rddlItemSelected");
                $("#jobFilter .rddlFakeInput").html($(this).text());
                $("#jobFilter_ClientState").attr("value", '{"enabled":true,"logEntries":[],"selectedIndex":' + i + ',"selectedText":"' + $(this).text().replace(" ", "%20") + '","selectedValue":""}');
                index = i;
		    } else {
			    $(this).removeClass("rddlItemSelected");
		    }
        });

        if (index > -1) {
            try {
                var dropdownlist = $find("<%= jobFilter.ClientID %>");
                var itemByIndex = dropdownlist.getItem(index);
                itemByIndex.select();
            } catch (err) {}
        }
    }

    function selectActivity(index) {
        if (index > -1) {
            $("#<%=cpdTable.ClientID%> .rgMasterTable tbody tr").each(function(i) {
                if (i == index) {
                    if (!$(this).hasClass("rgSelectedRow"))
                        $(this).addClass("rgSelectedRow");
                } else {
                    $(this).removeClass("rgSelectedRow");
                }
            });
        }
    }

    function OnJobFilterLoad() {
        // Indispensabile per permettere il caricamento degli items
        selectJob(1, "");
        selectJob(0, "");
        /////////////////

        if ($("#submitFlag").val() == 1) {
            $("#submitFlag").val(0);
            selectJob(-1, $("#jobFilterCompleteVal").val());
            $("#statsDiv").css("display", "inherit");
            $("#cpdTableContainer").css("display", "inherit");
            $("#cpdeTableContainer").css("display", "none");
        } else if ($("#updateFlag").val() == 0) {
            loadJobs();

            selectJob(1, "");
            selectJob(0, "");
        } else {
            $("#updateFlag").val(0);

            saveJobs();            
        }

        if ($("#selectedActivity").val() != "") {
            selectActivity($("#selectedActivityIndex").val());
            selectJob(-1, $("#jobFilterCompleteVal").val());
            $("#operatorFilter .racTextToken").text($("#operatorFilterVal").val());
            $("#selectedActivity").val("");
            $("#statsDiv").css("display", "inherit");
            $("#cpdTableContainer").css("display", "inherit");

            $(".resultLabel").each(function () {
                $(this).parent().css("background-color", ($(this).html() == "Ok" ? "PaleGreen" : "LightCoral"));
            });

            $("#cpdeTableContainer").css("display", "inherit");
        }
    }

    function OnOrderFilterLoad() {
        $("#orderFilter").change(function () {
            $("#orderFilter .racTextToken").text("");
            OnOrderFilterChanged();
        });

        OnConfirmBtnLoad();
    }

    function OnConfirmBtnLoad() {
        $("#btnConfirmSelection").off("click");
        $("#btnConfirmSelection").click(function () {
            var ofv = $("#orderFilter .racTextToken").text(), jfv = $("#jobFilter .rddlFakeInput").text(), opfv = $("#operatorFilter .racTextToken").text();
            if (ofv.length > 0 && jfv.length > 0 && jfv != "Seleziona") {
                $("#orderFilterVal").val(ofv);
                $("#jobFilterCompleteVal").val(jfv);
                $("#jobFilterVal").val(jfv.substr(0, jfv.indexOf(" ")));
                $("#operatorFilterVal").val(opfv);
                $("#submitFlag").val(1);

                $("#statsDiv").css("display", "none");
                $("#cpdTableContainer").css("display", "none");
                $("#cpdeTableContainer").css("display", "none");
                __doPostBack('<%=orderFilter.ClientID %>', "submit");
            } else {
                alert("Compilare tutti i campi.");
            }
        });
    }

    function saveJobs() {
        var vals = "", sep = "|&|";

        $("#jobFilter_DropDown .rddlList > li").each(function (i) {
            vals += $(this).text() + sep;
        });

        if (vals.length > 0) {
            $("#jobList").val(vals.substring(0, vals.length - sep.length));
        }
    }

    function loadJobs() {
        var sep = "|&|";
        var valsArr = String($("#jobList").val()).split(sep);

        clearJobs();
        var i = 0;
        for (let j of valsArr) {
            addItemToJobs(valsArr[i]);
            i++;
        }
    }

    function clearJobs() {
        var dropdownlist = $find("<%= jobFilter.ClientID %>");
        dropdownlist.get_items().clear();
        dropdownlist.get_textElement().innerHTML = "";
    }

    function addItemToJobs(value) {
        var dropdownlist = $find("<%= jobFilter.ClientID %>");
        var dropdownItem = new Telerik.Web.UI.DropDownListItem();
        dropdownItem.set_text(value);
        dropdownlist.get_items().add(dropdownItem);
    }

    function RowSelected(sender, eventArgs) {
        var currentRow = $(sender)[0]._selection._masterTable.children[$(sender)[0]._selectedIndexes[0]];
        var cpdId = $(currentRow).find(".descr").attr("data-index");

        selectActivity($(sender)[0]._selectedIndexes[0]);
        $("#selectedActivityIndex").val($(sender)[0]._selectedIndexes[0]);
        $("#selectedActivity").val(cpdId);
        $("#cpdeTableContainer").css("display", "none");
        __doPostBack('<%=orderFilter.ClientID %>', "selectActivity");
    }
</script>

<asp:UpdateProgress runat="server" ID="progressPnl">
    <ProgressTemplate>
        <div class="loading">Loading&#8230;</div>
    </ProgressTemplate>
</asp:UpdateProgress>

<div class="panel panel-info">
    <div class="panel-body">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="orderFilter" ClientIDMode="Static" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="130px" Width="420px"
                            TextSettings-SelectionMode="Single" MaxResultCount="10" MinFilterLength="4" DataSourceID="edsOrder" DataTextField="OrderCode" DataValueField="Id" InputType="Text" Filter="StartsWith" Label="Commessa: " OnClientLoad="OnOrderFilterLoad" OnClientDropDownOpening="OnClientDropDownOpening" OnClientEntryAdding="OnClientEntryAdding">
                        </telerik:RadAutoCompleteBox>
                    </div>
                    <%--<div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>--%>
                </div>
                <div class="row" style="margin-top: 10px">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:Label runat="server" Width="130px"><b>Fase: </b></asp:Label>
                        <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ID="jobFilter" ClientIDMode="Static" Placeholder="Seleziona" Width="289px" style="position: relative; left: -4px;"
                            OnClientLoad="OnJobFilterLoad">
                            <Items>
                                <telerik:DropDownListItem Text="Seleziona" />
                            </Items>
                        </telerik:RadDropDownList>
                    </div>
                    <%--<div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                        <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                            <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                        </telerik:RadDatePicker>
                    </div>--%>
                </div>
                <div class="row" style="margin-top: 10px">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="operatorFilter" ClientIDMode="Static" EmptyMessage="Seleziona" EnableClientFiltering="false" LabelWidth="130px" Width="420px"
                            TextSettings-SelectionMode="Single" InputType="Text" MaxResultCount="10" MinFilterLength="0" DataSourceID="edsOperator" DataTextField="Badge" DataValueField="Badge" Label="Badge Operatore: " OnClientDropDownOpening="OnClientDropDownOpening" OnClientEntryAdding="OnClientEntryAdding">
                            <DropDownItemTemplate>
                                <table>
                                    <tr>
                                        <td style="padding-right: 10px"><%# DataBinder.Eval(Container.DataItem, "Badge")%></td>
                                        <td><%# DataBinder.Eval(Container.DataItem, "Username")%></td>
                                    </tr>
                                </table>
                            </DropDownItemTemplate>
                        </telerik:RadAutoCompleteBox>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="text-align:right;">
            <button ID="btnConfirmSelection" class="btn btn-primary" onclick="return false;">Applica</button>
        </div>
    </div>
</div>

<asp:HiddenField runat="server" ClientIDMode="Static" ID="orderFilterVal"/>
<asp:HiddenField runat="server" ClientIDMode="Static" ID="jobFilterVal"/>
<asp:HiddenField runat="server" ClientIDMode="Static" ID="jobFilterCompleteVal"/>
<asp:HiddenField runat="server" ClientIDMode="Static" ID="operatorFilterVal"/>
<asp:HiddenField runat="server" ClientIDMode="Static" ID="submitFlag"/>
<asp:HiddenField runat="server" ClientIDMode="Static" ID="updateFlag"/>
<asp:HiddenField runat="server" ClientIDMode="Static" ID="jobList"/>
<asp:HiddenField runat="server" ClientIDMode="Static" ID="selectedActivity" Value=""/>
<asp:HiddenField runat="server" ClientIDMode="Static" ID="selectedActivityIndex" Value="-1"/>

<ef:EntityDataSource ID="edsOrder" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Orders"
    OrderBy="it.Description" AutoGenerateWhereClause="false" />
<ef:EntityDataSource ID="edsJob" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Jobs"
    OrderBy="it.Description" AutoGenerateWhereClause="false"/>
<ef:EntityDataSource ID="edsOperator" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Operators"
    AutoGenerateWhereClause="false" />

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div id="statsDiv" class="panel" style="display: none">
            <div class="panel-body">
                <div class="headerDiv">
                    <div style="width: 6%;">
                        <asp:Label runat="server" Text="Cod." />
                        <asp:Label class="cpeBkTitolo " ID="lblOpCode" Width="100%" runat="server" Text="---" />
                    </div>
                    <div style="width: 25%;">
                        <asp:Label runat="server" Text="Operatore" />
                        <asp:Label class="cpeBkTitolo " ID="lblOpUserName" Width="100%" runat="server" Text="---" />
                    </div>
                    <div style="width: 20%;">
                        <asp:Label runat="server" Text="Cod. Articolo" />
                        <asp:Label class="cpeBkTitolo " ID="lblArticleCode" Width="100%" runat="server" Text="---" />
                    </div>
                    <div style="width: 49%;">
                        <asp:Label runat="server" Text="Descr. Articolo" />
                        <asp:Label class="cpeBkTitolo " ID="lblArticleDescription" Width="100%" runat="server" Text="---" />
                    </div>
                </div>
                <div class="headerDiv">
                    <div style="width: 10%;">
                        <asp:Label runat="server" Text="Commessa" />
                        <asp:Label class="cpeBkTitolo " ID="lblOrder" Width="100%" runat="server" Text="---" />
                    </div>
                    <div style="width: 20%;">
                        <asp:Label runat="server" Text="Fase" />
                        <asp:Label class="cpeBkTitolo " ID="lblJob" Width="100%" runat="server" Text="---" />
                    </div>
                    <div style="width: 20%;">
                        <asp:Label runat="server" Text="Descr. Piano di Controllo" />
                        <asp:Label class="cpeBkTitolo " ID="lblCpDescription" Width="100%" runat="server" Text="---" />
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<div style="max-height: 302px">
    <div id="cpdTableContainer" style="display: none;" class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
        <asp:Label runat="server">Selezionare l'attività per cui mostrare le rilevazioni:</asp:Label><br /><br />
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <telerik:RadGrid runat="server" ID="cpdTable" AutoGenerateColumns="false"
                    OnNeedDataSource="cpdTable_NeedDataSource" Width="100%" Height="300px"  >
                    <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" NoMasterRecordsText="Nessun attività disponibile.">
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Descrizione" ItemStyle-Width="410px" ItemStyle-Wrap="true">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label runat="server" CssClass="control-label descr" Text='<%# Eval("Description") %>' data-index='<%# Eval("Id") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Tipologia" ItemStyle-Width="140px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" CssClass="control-label ctype" Text='<%# Eval("ControlType") %>' data-index='<%# Eval("Id") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <ClientEvents OnRowSelected="RowSelected" />
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="true" EnableColumnClientFreeze="true" FrozenColumnsCount="1" UseStaticHeaders="false" />
                    </ClientSettings>
                </telerik:RadGrid>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="cpdeTableContainer" style="display: none;" class="col-xs-8 col-sm-8 col-md-8 col-lg-9">
        <asp:Label runat="server">Lista rilevazioni:</asp:Label><br /><br />
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <telerik:RadGrid runat="server" ID="cpdeTable" AutoGenerateColumns="false"
                    OnNeedDataSource="cpdeTable_NeedDataSource" Width="100%" Height="300px" OnPreRender="cpdeTable_PreRender">
                    <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" NoMasterRecordsText="Nessun rilevazione disponibile.">
                        <Columns>
                            <telerik:GridTemplateColumn DataField="ExecutionDate" HeaderText="Data" ItemStyle-Width="140px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ExecutionDate") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="OperatorUserName" HeaderText="Operatore" ItemStyle-Width="190px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("OperatorUserName") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Qty. prod." ItemStyle-Width="120px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("QtyProduced") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Tempo prod." ItemStyle-Width="120px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ProductionTimeString") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="Operator" UniqueName="ValueOperator" HeaderText="Op." ItemStyle-Width="30px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ValueOperatorSymbol") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="Udm" UniqueName="UDM" HeaderText="UDM" ItemStyle-Width="65px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Udm") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="Tolerance1Sign" UniqueName="Tol1" HeaderText="Seg.1" ItemStyle-Width="35px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="Tol1Sign" CssClass="control-label" Text='<%# Eval("Tolerance1Sign") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="Tolerance1Value" UniqueName="Tol1Value" HeaderText="Toll.1" ItemStyle-Width="80px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="Tol1" CssClass="control-label" Text='<%# Eval("Tolerance1Value") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="BaseValue" UniqueName="BaseValue" HeaderText="Valore" ItemStyle-Width="90px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="BaseValue" CssClass="control-label" Text='<%# Eval("BaseValue") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="Tolerance2Sign" UniqueName="Tol2" HeaderText="Seg.2" ItemStyle-Width="35px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="Tol2Sign" CssClass="control-label" Text='<%# Eval("Tolerance2Sign") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="Tolerance2Value" UniqueName="Tol2Value" HeaderText="Toll.2" ItemStyle-Width="80px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="Tol2" CssClass="control-label" Text='<%# Eval("Tolerance2Value") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="ToolCode" HeaderText="Cod. Strumento" ItemStyle-Width="80px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Valore Misurato" ItemStyle-Wrap="true" ItemStyle-Width="90px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="measuredValueLbl" ClientIDMode="AutoID" data-numpadindex="0" Font-Bold="true" CssClass="control-label inputBox" Text='<%# Eval("StringValue") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Esito" ItemStyle-Wrap="true" ItemStyle-Width="80px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="resultLbl" ClientIDMode="AutoID" CssClass="control-label resultLabel" Text='<%# Eval("ResultValueString") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="true" EnableColumnClientFreeze="true" FrozenColumnsCount="1" UseStaticHeaders="false" />
                    </ClientSettings>
                </telerik:RadGrid>

                <div runat="server" class="row" style="margin-top: 10px; padding-left: 5px;">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label runat="server">Numero rilevazioni:</asp:Label><br />
                        <asp:Label ID="lblMeasuresCount" Font-Bold="true" runat="server"></asp:Label>
                    </div>
                    <div runat="server" ID="mediaStatsDiv" class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label runat="server">Media valori:</asp:Label><br />
                        <asp:Label ID="lblMedia" Font-Bold="true" runat="server"></asp:Label>
                    </div>
                    <div runat="server" class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label runat="server">Esiti Positivi:</asp:Label><br />
                        <asp:Label ID="lblPositiviCount" Font-Bold="true" runat="server"></asp:Label>
                    </div>
                    <div runat="server" class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label runat="server">Esiti Negativi:</asp:Label><br />
                        <asp:Label ID="lblNegativiCount" Font-Bold="true" runat="server"></asp:Label>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
