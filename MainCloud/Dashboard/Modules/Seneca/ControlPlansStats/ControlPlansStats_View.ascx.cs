﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.ControlPlansStats
{
    public partial class ControlPlansStats_View : DataWidget<ControlPlansStats_View>
    {
        public ControlPlansStats_View() : base(typeof(ControlPlansStats_View)) { }

        private object jobId, jobCode, jobDescription, orderId, orderCode, articleId, articleCode, articleDescription, operatorId, operatorCode, operatorBadge, operatorUserName, cpId, cpdId, cpDescription;

        private List<ControlPlanDetail> cpd;

        protected void Page_Load(object sender, EventArgs e)
        {
            var r = Request["__EVENTARGUMENT"];
            if (IsPostBack)
            {
                if (r == "updateFilter")
                {
                    getJobs(Request.Form[orderFilterVal.UniqueID].Trim());
                }
                else if (r == "submit")
                {
                    getJobs(Request.Form[orderFilterVal.UniqueID].Trim());
                    getActivityList();
                }
                else if (r == "selectActivity")
                {
                    getJobs(Request.Form[orderFilterVal.UniqueID].Trim());
                    getActivityList();
                    cpdId = Request.Form[selectedActivity.UniqueID].Trim();
                    cpdeTable.Rebind();
                }
            }
        }

        protected void getJobs(string orderCode)
        {
            edsJob.WhereParameters.Clear();
            edsJob.AutoGenerateWhereClause = true;

            MesDbContext DBContext = new MesDbContext();
            Order ord = null;
            try
            {
                ord = DBContext.Orders.Where(x => x.OrderCode == orderCode).First();
            }
            catch (Exception ex) { }
            if (ord != null)
            {
                edsJob.AutoGenerateWhereClause = false;
                edsJob.WhereParameters.Clear();
                edsJob.Where = "it.[OrderID] = " + ord.Id.ToString();

                JobRepository jRep = new JobRepository();
                List<Job> jList = DBContext.Jobs.Where(x => x.OrderId == ord.Id).ToList();

                jobFilter.Items.Clear();

                DropDownListItem ddli = new DropDownListItem();
                ddli.Controls.Add(new Label { Text = "Seleziona" });
                jobFilter.Items.Add(ddli);

                foreach (Job j in jList)
                {
                    ddli = new DropDownListItem();
                    ddli.Controls.Add(new Label { Text = j.Code + " " + j.Description });
                    jobFilter.Items.Add(ddli);
                }

                jobFilter.Enabled = true;
            }
        }

        protected void getActivityList()
        {
            orderCode = Request.Form[orderFilterVal.UniqueID].Trim();
            jobCode = Request.Form[jobFilterVal.UniqueID].Trim();
            operatorBadge = Request.Form[operatorFilterVal.UniqueID].Trim();

            loadData();

            lblOpCode.Text = (string)operatorCode;
            lblOpUserName.Text = (string)operatorUserName;
            lblArticleCode.Text = (string)articleCode;
            lblArticleDescription.Text = (string)articleDescription;
            lblOrder.Text = (string)orderCode;
            lblJob.Text = (string)jobDescription;
            lblCpDescription.Text = (string)cpDescription;

            if (lblOpCode.Text == "") lblOpCode.Text = "---";
            if (lblOpUserName.Text == "") lblOpUserName.Text = "---";
            if (lblArticleCode.Text == "") lblArticleCode.Text = "---";
            if (lblArticleDescription.Text == "") lblArticleDescription.Text = "---";
            if (lblOrder.Text == "") lblOrder.Text = "---";
            if (lblJob.Text == "") lblJob.Text = "---";
            if (lblCpDescription.Text == "") lblCpDescription.Text = "---";

            cpdTable.Rebind();
        }

        protected void loadData()
        {
            // Get Operator
            MesDbContext DBContext = new MesDbContext();
            if (operatorBadge.ToString().Trim().Length > 0)
            {
                Operator op = DBContext.Operators.Where(x => x.Badge == (string)operatorBadge).FirstOrDefault();
                if (op != null)
                {
                    operatorId = op.Id;
                    operatorCode = op.Code;
                    operatorUserName = op.UserName;
                }
            }

            // Get Order
            Order ord = DBContext.Orders.Where(x => x.OrderCode == (string)orderCode).FirstOrDefault();
            if (ord != null)
            {
                orderId = ord.Id;

                // Get Job
                Job j = DBContext.Jobs.Where(x => x.Code == (string)jobCode && x.OrderId == (int)orderId).FirstOrDefault();

                if (j != null)
                {
                    jobId = j.Id;

                    // Get Job Description
                    jobDescription = j.Description;

                    if (ord.Article != null)
                    {
                        // Get Article Id
                        articleId = ord.Article?.Id;

                        // Get Article Code
                        articleCode = ord.Article?.Code;

                        // Get Article Description
                        articleDescription = ord.Article?.Description;

                        if (j.ControlPlan != null)
                        {
                            // Get Control Plan Id
                            cpId = j.ControlPlan?.Id;

                            // Get Control Plan Description
                            cpDescription = j.ControlPlan?.Description;

                            // Get Control Plan Definitions
                            ControlPlanDetailRepository cpdRep = new ControlPlanDetailRepository();
                            cpd = cpdRep.GetAllByControlPlanId(Convert.ToInt32(cpId));
                        }
                    }
                }
            }
        }

        protected void cpdTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ControlPlanDetailExecutedRepository cpDERep = new ControlPlanDetailExecutedRepository();
            List<ControlPlanDetail> cpDList = cpDERep.GetControlPlanDetailsByJobAndOperator((string)jobId, (string)operatorId);
            cpdTable.DataSource = cpDList;
        }

        protected void cpdeTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ControlPlanDetailExecutedRepository cpDERep = new ControlPlanDetailExecutedRepository();
            List<ControlPlanDetailExecuted> cpDEList = cpDERep.GetByCpdJobAndOperator(Convert.ToInt32(cpdId), (string)jobId, (string)operatorId);
            cpdeTable.DataSource = cpDEList;
        }

        protected void cpdeTable_PreRender(object sender, EventArgs e)
        {
            List<ControlPlanDetailExecuted> cpdeData = (List<ControlPlanDetailExecuted>)cpdeTable.DataSource;

            if (cpdeData != null && cpdeData.Count > 0)
            {
                bool toleranceFlag = cpdeData[0].isToleranceVisible,
                     controlDimensionalFlag = cpdeData[0].isControlDimensional;

                cpdeTable.MasterTableView.GetColumn("OperatorUserName").Visible = (operatorId == null);

                cpdeTable.MasterTableView.GetColumn("Tol1").Visible = toleranceFlag;
                cpdeTable.MasterTableView.GetColumn("Tol1Value").Visible = toleranceFlag;
                cpdeTable.MasterTableView.GetColumn("Tol2").Visible = toleranceFlag;
                cpdeTable.MasterTableView.GetColumn("Tol2Value").Visible = toleranceFlag;

                cpdeTable.MasterTableView.GetColumn("ValueOperator").Visible = controlDimensionalFlag;
                cpdeTable.MasterTableView.GetColumn("UDM").Visible = controlDimensionalFlag;
                cpdeTable.MasterTableView.GetColumn("BaseValue").Visible = controlDimensionalFlag;

                mediaStatsDiv.Visible = controlDimensionalFlag;

                decimal sumVal = 0, positiviCount = 0;
                foreach (ControlPlanDetailExecuted cpde in cpdeData)
                {
                    sumVal += cpde.MeasuredValue;

                    if (cpde.ResultValue)
                        positiviCount++;
                }

                lblPositiviCount.Text = positiviCount.ToString();
                lblNegativiCount.Text = (cpdeData.Count - positiviCount).ToString();

                if (controlDimensionalFlag)
                    lblMedia.Text = Math.Round(sumVal / cpdeData.Count, 3).ToString();
            }

            lblMeasuresCount.Text = (cpdeData != null) ? cpdeData.Count.ToString() : "0";
        }
    }
}