﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.TransactionLog
{
    public partial class TransactionLog_View : DataWidget<TransactionLog_View>
    {
        public TransactionLog_View() : base(typeof(TransactionLog_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void gridLog_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                if (item["Status"].Text == "0")
                {
                    item["Status"].Text = "Off";
                }
                else
                {
                    item["Status"].Text = "On";
                }
                //item["Machine"].Text = Asset item["Machine"].Text;
            }
        }

        //protected void edsLog_QueryCreated(object sender, QueryCreatedEventArgs e)
        //{
        //    var transactions = e.Query.Cast<SenecaModule.Models.TransactionLog>();
        //    var assets = e.Query.Cast<AssetManagement.Models.Asset>();
        //    e.Query = from t in transactions
        //              join a in assets on t.Machine equals a.Id
        //              select new { t.Date, a.Description, t.Status, t.Counter};
        //}
    }
}