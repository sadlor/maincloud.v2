﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TransactionLog_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.TransactionLog.TransactionLog_View" %>

<telerik:RadGrid runat="server" ID="gridLog" RenderMode="Lightweight" DataSourceID="edsLog" AutoGenerateColumns="false"
    AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false"
    OnItemDataBound="gridLog_ItemDataBound"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView Caption="Log" FilterExpression="" DataKeyNames="Id">
        <Columns>
            <telerik:GridDateTimeColumn DataField="Date" HeaderText="DateTime" PickerType="DatePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn DataField="Machine" HeaderText="Machine">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Status" HeaderText="Status">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Counter" HeaderText="Counter">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <PagerStyle Mode="NextPrevAndNumeric" />
</telerik:RadGrid>

<ef:EntityDataSource runat="server" ID="edsLog" ContextTypeName="SenecaModule.Models.SenecaDbContext" EntitySetName="TransactionLogs" AutoGenerateWhereClause="true">
</ef:EntityDataSource>