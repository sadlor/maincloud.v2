﻿using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.SummaryMachine
{
    public partial class SummaryMachine_View : DataWidget<SummaryMachine_View>
    {
        public SummaryMachine_View() : base(typeof(SummaryMachine_View)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        //protected string AssetId
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty((string)ViewState["SummaryMachineAssetId"]))
        //        {
        //            return null;
        //        }
        //        return ViewState["SummaryMachineAssetId"].ToString();
        //    }
        //    set
        //    {
        //        ViewState["SummaryMachineAssetId"] = value;
        //    }
        //}

        private class JobRecord
        {
            public string Label { get; set; }
            public decimal? Pezzi { get; set; }
            public decimal? Colpi { get; set; }
            public decimal? Teorico { get; set; }
            public decimal? Eseguito { get; set; }
            public string Tempo { get; set; }
            public string Efficienza { get; set; }
            public string GroupType { get; set; }

            public JobRecord(string label, decimal? pezzi, decimal? colpi, decimal? teorico, decimal? eseguito, TimeSpan? tempo, string efficienza, string groupType)
            {
                Label = label;
                Pezzi = pezzi;
                Colpi = colpi;
                Teorico = teorico;
                Eseguito = eseguito;
                if (tempo != null)
                {
                    Tempo = string.Format("{0}:{1}:{2}", (int)tempo?.TotalHours, tempo?.Minutes, tempo?.Seconds);
                }
                Efficienza = efficienza;
                GroupType = groupType;
            }
        }

        protected void JobFilter_ExpressionItemCreated(object sender, RadFilterExpressionItemCreatedEventArgs e)
        {
            if (e.Item is RadFilterSingleExpressionItem)
            {
                RadFilterSingleExpressionItem item = e.Item as RadFilterSingleExpressionItem;
                if (item != null && item.FieldName == "Machine")
                {
                    item.RemoveButton.Visible = false;
                }
            }
            if (e.Item is RadFilterGroupExpressionItem)
            {
                RadFilterGroupExpressionItem groupItem = e.Item as RadFilterGroupExpressionItem;
                if (groupItem != null)
                {
                    if (groupItem.IsRootGroup)
                    {
                        groupItem.AddGroupExpressionButton.Visible = false;
                        groupItem.RemoveButton.Visible = false;
                    }
                }
            }
        }

        protected void JobFilter_ApplyExpressions(object sender, RadFilterApplyExpressionsEventArgs e)
        {
            DateTime? start = null;
            DateTime? end = null;
            string assetId = string.Empty;
            string jobId = string.Empty;

            RadFilterExpressionsCollection exprList = e.ExpressionRoot.Expressions;
            if (exprList.Count == 0)
            {
                return;
            }
            foreach (var expr in exprList)
            {
                if (expr is RadFilterEqualToFilterExpression<string>)
                {
                    var item = expr as RadFilterEqualToFilterExpression<string>;
                    switch (item.FieldName)
                    {
                        case "Machine":
                            assetId = item.Value;
                            break;
                        case "Job":
                            jobId = item.Value;
                            break;
                    }
                }
                if (expr is RadFilterEqualToFilterExpression<DateTime>)
                {
                    var item = expr as RadFilterEqualToFilterExpression<DateTime>;
                    start = item.Value;
                    end = item.Value.AddDays(1);
                }
                if (expr is RadFilterBetweenFilterExpression<DateTime>)
                {
                    var item = expr as RadFilterBetweenFilterExpression<DateTime>;
                    start = item.LeftValue;
                    if (item.RightValue == DateTime.MinValue)
                    {
                        end = item.LeftValue.AddDays(1);
                    }
                    else
                    {
                        end = item.RightValue.AddDays(1);
                    }
                }
            }

            if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(jobId))
            {
                CreateTable(assetId, jobId, start, end);
            }
        }

        protected void JobFilter_ItemCommand(object sender, RadFilterCommandEventArgs e)
        {
            if (e.CommandName == RadFilter.AddExpressionCommandName || e.CommandName == RadFilter.ApplyCommandName)
            {
                RadFilterExpression expr = JobFilter.RootGroupItem.Expression.Expressions.Where(x => (x as RadFilterEqualToFilterExpression<string>)?.FieldName == "Machine").FirstOrDefault();
                if (expr != null)
                {
                    if ((expr as RadFilterEqualToFilterExpression<string>).Value != null)
                    {
                        string machineId = (expr as RadFilterEqualToFilterExpression<string>).Value;

                        //edsJob.WhereParameters.Clear();
                        //edsJob.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
                        //edsJob.WhereParameters.Add("AssetId", machineId);
                    }
                }
            }
        }

        //protected void JobFilter_FieldEditorCreating(object sender, RadFilterFieldEditorCreatingEventArgs e)
        //{
        //    if (e.EditorType == "RadCustomFilterDropDownEditor")
        //    {
        //        e.Editor = new RadCustomFilterDropDownEditor();
        //    }
        //}

        protected void jobGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridGroupHeaderItem)
            {
                GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;
                DataRowView groupDataRow = (DataRowView)e.Item.DataItem;
                item.DataCell.Text = groupDataRow["GroupType"].ToString();
            }
        }

        protected void CreateTable(string AssetId, string jobId, DateTime? start, DateTime? end)
        {
            if (!string.IsNullOrEmpty(AssetId) && !string.IsNullOrEmpty(jobId))
            {
                List<JobRecord> dataSource = new List<JobRecord>();
                //    //6 righe fisse per il datasource
                //    //1 -> new JobTable("Da produrre colpi", 0, 0, 0, "100%", "Last");

                TransactionService TService = new TransactionService();
                Transaction lastTrans = TService.GetLastTransactionOpen(AssetId); //passare assetId dall'area rilievi
                if (lastTrans != null)
                {
                    if (!string.IsNullOrEmpty(lastTrans.JobId))
                    {
                        JobRepository jobRep = new JobRepository();
                        Job job = jobRep.FindByID(jobId);
                        int nImpronte = 1;
                        if (!string.IsNullOrEmpty(job.MoldId))
                        {
                            MoldRepository mRep = new MoldRepository();
                            nImpronte = mRep.FindByID(job.MoldId).CavityMoldNum;
                        }
                        if (nImpronte == 0)
                        {
                            nImpronte = 1;
                        }

                        CauseService causeService = new CauseService();
                        string productionCauseId = causeService.GetCauseByCode("200").Id;
                        List<string> fermiList = causeService.GetListCauseIdByGroupName("Fermi");
                        List<string> setupList = causeService.GetListCauseIdByGroupName("Attrezzaggio");

                        TransactionRepository TRep = new TransactionRepository();

                        //generalità job
                        dataSource.Add(new JobRecord("Quantità da produrre", job.QtyOrdered, job.QtyOrdered / nImpronte, job.StandardRate, null, null, "", "Efficienza"));


                        List<Transaction> workshift;
                        if (!string.IsNullOrEmpty(jobId))
                        {
                            workshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id).ToList();
                        }
                        else
                        {
                            workshift = TRep.ReadAll(x => x.MachineId == AssetId).ToList();
                        }

                        decimal workshiftEffettivo = 0;
                        if (workshift.Count > 0 && workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration) > 0)
                        {
                            workshiftEffettivo = Math.Round(workshift.Sum(x => x.PartialCounting) / Convert.ToDecimal(TimeSpan.FromSeconds((double)workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalHours), 2);
                        }
                        dataSource.Add(
                            new JobRecord(
                                "Eseguito totale",
                                workshift.Sum(x => x.PartialCounting) * nImpronte,
                                workshift.Sum(x => x.PartialCounting),
                                null,
                                workshiftEffettivo,
                                TimeSpan.FromSeconds((double)workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)),
                                Math.Round(workshiftEffettivo / job.StandardRate * 100, 2) + "%",
                                "Efficienza"));

                        decimal totProduzione = workshift.Sum(x => x.PartialCounting);

                        //scarti
                        JobProductionWasteService jpws = new JobProductionWasteService();
                        List<JobProductionWaste> scarti = jpws.GetWasteListByJobId(job.Id);
                        dataSource.Add(new JobRecord("Scarti", scarti.Sum(x => x.QtyProductionWaste), scarti.Sum(x => x.QtyProductionWaste) / nImpronte, null, null, null, "", "Efficienza"));

                        //residuo
                        decimal pezziResidui = job.QtyOrdered - (totProduzione * nImpronte) + scarti.Sum(x => x.QtyProductionWaste);
                        dataSource.Add(
                            new JobRecord(
                                "Residuo da produrre (eccedenza)",
                                pezziResidui,
                                pezziResidui / nImpronte,
                                null,
                                null,
                                TimeSpan.FromHours((double)((pezziResidui / nImpronte) / job.StandardRate)),
                                "",
                                "Efficienza"));

                        //Resa
                        List<Transaction> currentJobTrans = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id).ToList();
                        dataSource.Add(
                            new JobRecord(
                                "Tempo totale disponibile", null, null, null, null, TimeSpan.FromSeconds((double)currentJobTrans.Sum(x => x.Duration)), "", "Resa"));
                        dataSource.Add(
                            new JobRecord(
                                "Tempo totale produzione",
                                null,
                                null,
                                null,
                                null,
                                TimeSpan.FromSeconds((double)currentJobTrans.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)),
                                Math.Round(TimeSpan.FromSeconds((double)currentJobTrans.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalSeconds /
                                TimeSpan.FromSeconds((double)currentJobTrans.Sum(x => x.Duration)).TotalSeconds * 100, 2) + "%",
                                "Resa"));

                        //Qualità
                        if (totProduzione > 0)
                        {
                            dataSource.Add(
                                new JobRecord(
                                    "Totale pezzi prodotti",
                                    totProduzione * nImpronte,
                                    null,
                                    null,
                                    null,
                                    null,
                                    Math.Round((((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) * 100, 2) + "%",
                                    "Qualità"));
                        }
                        else
                        {
                            dataSource.Add(new JobRecord("Totale pezzi prodotti", totProduzione, null, null, null, null, "", "Qualità"));
                        }
                        dataSource.Add(new JobRecord("Totale non conformità", scarti.Sum(x => x.QtyProductionWaste), null, null, null, null, "", "Qualità"));


                        var setupTime = workshift.Where(x => setupList.Contains(x.CauseId)).Sum(x => x.Duration);
                        var stopTime = workshift.Where(x => fermiList.Contains(x.CauseId) || string.IsNullOrEmpty(x.CauseId)).Sum(x => x.Duration);
                        var prodTime = workshift.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration);
                        var totTime = workshift.Sum(x => x.Duration);

                        timeChart.PlotArea.Series[0].Items.Clear();
                        if (totTime > 0)
                        {
                            (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((setupTime / totTime) * 100, 2), System.Drawing.Color.Yellow, "Attrezzaggio"));
                            (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((stopTime / totTime) * 100, 2), System.Drawing.Color.Red, "Fermi"));
                            (timeChart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(new PieSeriesItem(Math.Round((prodTime / totTime) * 100, 2), System.Drawing.Color.Green, "Produzione"));
                        }

                        meterEfficienza.Pointer.Value = Math.Round(workshiftEffettivo / job.StandardRate * 100, 2);
                        txtEfficienza.Text = Math.Round(workshiftEffettivo / job.StandardRate * 100, 2).ToString();
                        if (totProduzione > 0)
                        {
                            meterQualità.Pointer.Value = Math.Round((((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) * 100, 2);
                            txtQualità.Text = Math.Round((((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) * 100, 2).ToString();
                        }
                        else
                        {
                            meterQualità.Pointer.Value = 0;
                            txtQualità.Text = 0.ToString();
                        }
                        if (currentJobTrans.Sum(x => x.Duration) > 0)
                        {
                            meterResa.Pointer.Value = Convert.ToDecimal(Math.Round(TimeSpan.FromSeconds((double)currentJobTrans.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalSeconds /
                                    TimeSpan.FromSeconds((double)currentJobTrans.Sum(x => x.Duration)).TotalSeconds * 100, 2));
                            txtResa.Text = Math.Round(TimeSpan.FromSeconds((double)currentJobTrans.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalSeconds /
                                    TimeSpan.FromSeconds((double)currentJobTrans.Sum(x => x.Duration)).TotalSeconds * 100, 2).ToString();
                        }
                        else
                        {
                            meterResa.Pointer.Value = 0;
                            txtResa.Text = 0.ToString();
                        }

                        if (meterEfficienza.Pointer.Value == 0 || meterResa.Pointer.Value == 0)
                        {
                            meterOEE.Pointer.Value = 0;
                            txtOEE.Text = 0.ToString();
                        }
                        else
                        {
                            meterOEE.Pointer.Value = Math.Round(workshiftEffettivo / job.StandardRate *
                                (((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) *
                                (decimal)(TimeSpan.FromSeconds((double)currentJobTrans.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalSeconds /
                                    TimeSpan.FromSeconds((double)currentJobTrans.Sum(x => x.Duration)).TotalSeconds)
                                * 100, 2);
                            txtOEE.Text = Math.Round(workshiftEffettivo / job.StandardRate *
                                (((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) *
                                (decimal)(TimeSpan.FromSeconds((double)currentJobTrans.Where(x => x.CauseId == productionCauseId).Sum(x => x.Duration)).TotalSeconds /
                                    TimeSpan.FromSeconds((double)currentJobTrans.Sum(x => x.Duration)).TotalSeconds)
                                * 100, 2).ToString();
                        }
                    }
                    else
                    {
                        //nessun job assegnato
                        dataSource.Add(new JobRecord("Quantità da produrre", null, null, null, null, null, "", "Efficienza"));
                        dataSource.Add(new JobRecord("Eseguito turni precedenti", null, null, null, null, null, "", "Efficienza"));
                        dataSource.Add(new JobRecord("Eseguito turno corrente", null, null, null, null, null, "", "Efficienza"));
                        dataSource.Add(new JobRecord("Scarti", null, null, null, null, null, "", "Efficienza"));
                        dataSource.Add(new JobRecord("Residuo da produrre", null, null, null, null, null, "", "Efficienza"));
                        dataSource.Add(new JobRecord("Tempo totale disponibile", null, null, null, null, null, "", "Resa"));
                        dataSource.Add(new JobRecord("Tempo totale produzione", null, null, null, null, null, "", "Resa"));
                        dataSource.Add(new JobRecord("Totale pezzi prodotti", null, null, null, null, null, "", "Qualità"));
                        dataSource.Add(new JobRecord("Totale non conformità", null, null, null, null, null, "", "Qualità"));
                    }
                }
                else
                {
                    //nessuna transazione
                    dataSource.Add(new JobRecord("Quantità da produrre", null, null, null, null, null, "", "Efficienza"));
                    dataSource.Add(new JobRecord("Eseguito turni precedenti", null, null, null, null, null, "", "Efficienza"));
                    dataSource.Add(new JobRecord("Eseguito turno corrente", null, null, null, null, null, "", "Efficienza"));
                    dataSource.Add(new JobRecord("Scarti", null, null, null, null, null, "", "Efficienza"));
                    dataSource.Add(new JobRecord("Residuo da produrre", null, null, null, null, null, "", "Efficienza"));
                    dataSource.Add(new JobRecord("Tempo totale disponibile", null, null, null, null, null, "", "Resa"));
                    dataSource.Add(new JobRecord("Tempo totale produzione", null, null, null, null, null, "", "Resa"));
                    dataSource.Add(new JobRecord("Totale pezzi prodotti", null, null, null, null, null, "", "Qualità"));
                    dataSource.Add(new JobRecord("Totale non conformità", null, null, null, null, null, "", "Qualità"));
                }

                jobGrid.DataSource = dataSource;
                jobGrid.Rebind();
            }
        }
    }
}