﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SummaryMachine_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.SummaryMachine.SummaryMachine_View" %>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function FilterCreated(sender, eventArgs) {
            var filterMenu = sender.get_contextMenu();
            filterMenu.add_showing(FilterMenuShowing);
        }
        function FilterMenuShowing(sender, eventArgs) {
            var filter = $find("<%= JobFilter.ClientID %>");
            var currentExpandedItem = sender.get_attributes()._data.ItemHierarchyIndex;

            sender.findItemByValue("And").set_visible(false);
            sender.findItemByValue("Or").set_visible(false);
            sender.findItemByValue("NotOr").set_visible(false);
            sender.findItemByValue("NotAnd").set_visible(false);

            sender.findItemByValue("IsNull").set_visible(false);
            sender.findItemByValue("NotIsNull").set_visible(false);
            sender.findItemByValue("IsEmpty").set_visible(false);
            sender.findItemByValue("NotIsEmpty").set_visible(false);
            sender.findItemByValue("Contains").set_visible(false);
            sender.findItemByValue("DoesNotContain").set_visible(false);
            sender.findItemByValue("GreaterThan").set_visible(false);
            sender.findItemByValue("GreaterThanOrEqualTo").set_visible(false);
            sender.findItemByValue("LessThan").set_visible(false);
            sender.findItemByValue("LessThanOrEqualTo").set_visible(false);
            sender.findItemByValue("NotEqualTo").set_visible(false);
            sender.findItemByValue("EqualTo").set_visible(false);
            sender.findItemByValue("StartsWith").set_visible(false);
            sender.findItemByValue("EndsWith").set_visible(false);
            sender.findItemByValue("Between").set_visible(false);
            sender.findItemByValue("NotBetween").set_visible(false);
            //if (filter._expressionItems[currentExpandedItem] == "Machine")
            //{
            //    sender.findItemByValue("Between").set_visible(true);
            //    sender.findItemByValue("EqualTo").set_visible(true);
            //}
            //sender.findItemByValue("Machine").set_visible(false);
        }

    </script>
</telerik:RadCodeBlock>

<style type="text/css">
    .RadPicker.RadDateTimePicker.RadPicker_Bootstrap.rfControl{
        width: 211px !important;
    }
</style>

<telerik:RadFilter RenderMode="Lightweight" runat="server" ID="JobFilter" ExpressionPreviewPosition="None" OperationMode="ServerAndClient"
    OnApplyExpressions="JobFilter_ApplyExpressions" OnExpressionItemCreated="JobFilter_ExpressionItemCreated" OnItemCommand="JobFilter_ItemCommand"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <FieldEditors>
        <%--<customFilter:RadCustomFilterDropDownEditor FieldName="Machine" DataTextField="Description" DataValueField="Id" />--%>
        <telerik:RadFilterDropDownEditor FieldName="Machine" DisplayName="Machine" DataSourceID="edsMachine"
            DataTextField="Description" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />
        <telerik:RadFilterDropDownEditor FieldName="Job" DisplayName="Job" DataSourceID="edsJob"
            DataTextField="Code" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />
        <telerik:RadFilterDateFieldEditor FieldName="Date" DisplayName="Date" PickerType="DateTimePicker" DateFormat="dd/MM/yyyy HH:mm:ss"
            DefaultFilterFunction="Between" />
    </FieldEditors>
    <ClientSettings>
        <ClientEvents OnFilterCreated="FilterCreated" />
    </ClientSettings>
</telerik:RadFilter>

<br />

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="jobGrid" AutoGenerateColumns="false" GridLines="none" Skin="Silk" 
            OnItemDataBound="jobGrid_ItemDataBound"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <MasterTableView FilterExpression="" Caption="">
                <ColumnGroups>
                    <telerik:GridColumnGroup Name="Tempo" HeaderText="Tempo">
                        <HeaderStyle HorizontalAlign="Center" />
                    </telerik:GridColumnGroup>
                </ColumnGroups>
                <GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldName="GroupType"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldName="GroupType" SortOrder="None"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>
                <Columns>
                    <telerik:GridBoundColumn DataField="Label" HeaderText="">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Pezzi" HeaderText="Pezzi">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Colpi" HeaderText="Colpi">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Teorico" HeaderText="Teorico">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Eseguito" HeaderText="Eseguito">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Tempo" HeaderText="Tempo">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Efficienza" HeaderText="Efficienza %">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <Selecting AllowRowSelect="True"></Selecting>
            </ClientSettings>
        </telerik:RadGrid>
    </ContentTemplate>
</asp:UpdatePanel>

<%--<br />--%>

<telerik:RadHtmlChart runat="server" ID="timeChart" Transitions="true" Skin="Silk">
    <ChartTitle Text="Time Usage">
        <Appearance Align="Center" Position="Top">
        </Appearance>
    </ChartTitle>
    <Legend>
        <Appearance Position="Right" Visible="true">
        </Appearance>
    </Legend>
    <PlotArea>
        <Series>
            <telerik:PieSeries StartAngle="90">
                <LabelsAppearance Position="OutsideEnd" DataFormatString="{0} %">
                </LabelsAppearance>
                <TooltipsAppearance Color="White" DataFormatString="{0} %"></TooltipsAppearance>
            </telerik:PieSeries>
        </Series>
    </PlotArea>
</telerik:RadHtmlChart>

<br />

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="row" style="text-align:center;">
                    <asp:Label Font-Bold="true" Text="Efficienza" runat="server" />
                </div>
                <div class="row">
                    <center>
                        <telerik:RadRadialGauge runat="server" ID="meterEfficienza">
                            <Pointer Value="0">
                                <Cap Size="0.1" />
                            </Pointer>
                            <Scale Min="0" Max="100" MajorUnit="20">
                                <Labels Format="{0}" />
                                <Ranges>
                                    <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                    <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                    <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                    <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                </Ranges>
                            </Scale>
                        </telerik:RadRadialGauge>
                        <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtEfficienza" style="text-align:center" />
                    </center>
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="row" style="text-align:center;">
                    <center><asp:Label Font-Bold="true" Text="Qualità" runat="server" /></center>
                </div>
                <div class="row">
                    <center>
                        <telerik:RadRadialGauge runat="server" ID="meterQualità">
                            <Pointer Value="0">
                                <Cap Size="0.1" />
                            </Pointer>
                            <Scale Min="0" Max="100" MajorUnit="20">
                                <Labels Format="{0}" />
                                <Ranges>
                                    <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                    <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                    <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                    <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                </Ranges>
                            </Scale>
                        </telerik:RadRadialGauge>
                        <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtQualità" style="text-align:center" />
                    </center>
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="row" style="text-align:center;">
                    <center><asp:Label Font-Bold="true" Text="Resa" runat="server" /></center>
                </div>
                <div class="row">
                    <center>
                        <telerik:RadRadialGauge runat="server" ID="meterResa">
                            <Pointer Value="0">
                                <Cap Size="0.1" />
                            </Pointer>
                            <Scale Min="0" Max="100" MajorUnit="20">
                                <Labels Format="{0}" />
                                <Ranges>
                                    <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                    <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                    <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                    <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                </Ranges>
                            </Scale>
                        </telerik:RadRadialGauge>
                        <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtResa" style="text-align:center" />
                    </center>
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="row">
                    <center><asp:Label Font-Bold="true" Text="OEE" runat="server" /></center>
                </div>
                <div class="row">
                    <center>
                        <telerik:RadRadialGauge runat="server" ID="meterOEE">
                            <Pointer Value="0">
                                <Cap Size="0.1" />
                            </Pointer>
                            <Scale Min="0" Max="100" MajorUnit="20">
                                <Labels Format="{0}" />
                                <Ranges>
                                    <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                    <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                    <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                    <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                </Ranges>
                            </Scale>
                        </telerik:RadRadialGauge>
                        <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtOEE" style="text-align:center" />
                    </center>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>


<ef:EntityDataSource ID="edsMachine" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    AutoGenerateWhereClause="true" OrderBy="it.Description">
</ef:EntityDataSource>
<ef:EntityDataSource ID="edsJob" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Jobs"
        OrderBy="it.Description" AutoGenerateWhereClause="true" />