﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobRealTime_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.JobRealTime.JobRealTime_View" %>

<style type="text/css">
    div.RadGridCustomClass .rgHeader,
    div.RadGridCustomClass th.rgResizeCol,
    div.RadGridCustomClass .rgRow td,
    div.RadGridCustomClass .rgAltRow td,
    div.RadGridCustomClass .rgFooter td {
        border-left: 1px solid #828282 !important;
        border-right: 1px solid #828282 !important;
        /*border-width: 1px !important;*/
    }

    .customLabel
    {
        white-space: pre-wrap !important;
    }

    .btnActivity{
        min-width: 125px;
        min-height: 40px;
        border: 1px solid;
        background-color:#dddddd !important;
        border-color:black !important;
    }

    .alDx {
        width: 100%;
        display: inline-block;
        text-align: right !important;
    }

    .textNoPadding{
        padding-left:0px;
        padding-right:0px;
    }
</style>

<asp:UpdatePanel runat="server" ID="updatePanelIntestazione" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnConfirmUpdateTransaction" EventName="Click" />
    </Triggers>
    <ContentTemplate>
        <%--<div class="row">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <asp:Label Text="<%$ Resources:Mes,Code %>" runat="server" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <asp:Label Text="<%$ Resources:Mes,Description %>" runat="server" />
            </div>
        </div>--%>
        <br />
        <div class="row">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                <b><%: GetGlobalResourceObject("Mes","Job") %>:</b>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtCodeJob" EmptyMessage="<%$ Resources:Mes,Code %>" ReadOnly="true" Width="100%" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtDescriptionJob" EmptyMessage="<%$ Resources:Mes,Description %>" ReadOnly="true" Width="100%" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtCodeOrder" ReadOnly="true" Width="100%" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                <b><%: GetGlobalResourceObject("Mes","Article") %>:</b>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtCodeArticle" EmptyMessage="<%$ Resources:Mes,Code %>" ReadOnly="true" Width="100%" />
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtDescriptionArticle" EmptyMessage="<%$ Resources:Mes,Description %>" ReadOnly="true" Width="100%" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtQtyArticle" ReadOnly="true" Width="100%" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                <b><%: GetGlobalResourceObject("Mes","Mold") %>:</b>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtCodeMold" EmptyMessage="<%$ Resources:Mes,Code %>" ReadOnly="true" Width="100%" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtDescriptionMold" EmptyMessage="<%$ Resources:Mes,Description %>" ReadOnly="true" Width="100%" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtNImpronte" ReadOnly="true" Width="100%" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtStatusMold" ReadOnly="true" Width="100%" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtLocationMold" ReadOnly="true" Width="100%" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><%--style="max-width:60px;"--%>
                <b>MP:</b>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtCodeMP" ReadOnly="true" Width="100%" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtDescriptionMP" ReadOnly="true" Width="100%" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtLotto" ReadOnly="true" Width="100%" />
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                <telerik:RadTextBox runat="server" ID="txtQtyMP" ReadOnly="true" Width="100%" />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<br /><br />

<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <telerik:RadButton ID="btnAttrezzaggio" runat="server" Text="Attrezzaggio" OnClick="btnAttrezzaggio_Click" CssClass="btnActivity" style="text-align:-webkit-center;" />
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <telerik:RadButton ID="btnProduzione" runat="server" Text="Produzione" OnClick="btnProduzione_Click" CssClass="btnActivity" style="text-align:-webkit-center;" />
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <telerik:RadButton ID="btnManutenzione" runat="server" Text="Manutenzione" OnClick="btnManutenzione_Click" CssClass="btnActivity" style="text-align:-webkit-center;">
            <%--<ContentTemplate>
                <center>Manutenzione<i class="fa fa-briefcase" aria-hidden="true"></i></center>
            </ContentTemplate>--%>
        </telerik:RadButton>
    </div>
</div>
<br />
<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <telerik:RadButton ID="btnFermi" runat="server" Text="Fermi" OnClick="btnFermi_Click" CssClass="btnActivity" BackColor="#dddddd" style="text-align:-webkit-center;" />
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <telerik:RadButton ID="btnScarti" runat="server" Text="Scarti" OnClick="btnScarti_Click" CssClass="btnActivity" style="text-align:-webkit-center;" />
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <telerik:RadButton ID="btnQualità" runat="server" Text="Qualità" CssClass="btnActivity" style="text-align:-webkit-center;" />
    </div>
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <telerik:RadButton ID="btnExit" runat="server" BackColor="#d9534f" CssClass="btnActivity" OnClick="btnExit_Click">
            <ContentTemplate>
                <center><i class="fa fa-window-close" aria-hidden="true" style="color:black"></i> Esci</center>
            </ContentTemplate>
        </telerik:RadButton>
    </div>
</div>
<br /><br />

<telerik:RadGrid runat="server" RenderMode="Lightweight" ID="jobGrid" AutoGenerateColumns="false" GridLines="none" Skin="Silk" 
    OnNeedDataSource="jobGrid_NeedDataSource"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <MasterTableView FilterExpression="" Caption="">
        <ColumnGroups>
            <telerik:GridColumnGroup Name="Tempo" HeaderText="Tempo">
                <HeaderStyle HorizontalAlign="Center" />
            </telerik:GridColumnGroup>
        </ColumnGroups>
        <GroupByExpressions>
            <telerik:GridGroupByExpression>
                <SelectFields>
                    <telerik:GridGroupByField FieldAlias="GroupType" FieldName="GroupType"></telerik:GridGroupByField>
                </SelectFields>
                <GroupByFields>
                    <telerik:GridGroupByField FieldName="GroupType" SortOrder="Descending"></telerik:GridGroupByField>
                </GroupByFields>
            </telerik:GridGroupByExpression>
        </GroupByExpressions>
        <Columns>
            <telerik:GridBoundColumn DataField="Label" HeaderText="">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Colpi" HeaderText="Colpi">
                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Teorico" HeaderText="T Teorico">
                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Eseguito" HeaderText="T Eseguito">
                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Efficienza" HeaderText="Efficienza">
                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <Selecting AllowRowSelect="True"></Selecting>
    </ClientSettings>
</telerik:RadGrid>
<br />
<div class="row">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <telerik:RadButton ID="RadButton1" runat="server" Text="Disegno articolo" CssClass="btnActivity" />
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <telerik:RadButton ID="RadButton2" runat="server" Text="Piani di setup" CssClass="btnActivity" />
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <telerik:RadButton ID="RadButton3" runat="server" Text="Note sicurezza" CssClass="btnActivity" />
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <telerik:RadButton ID="RadButton5" runat="server" Text="Da decidere" CssClass="btnActivity" />
    </div>
</div>

<mcf:PopUpDialog ID="dlgUpdateTransaction" runat="server">
    <Body>
        <asp:UpdatePanel runat="server" UpdateMode="Always">
            <Triggers>
                <%--<asp:AsyncPostBackTrigger ControlID="tableSelection" EventName="SelectedIndexChanged" />--%>
            </Triggers>
            <ContentTemplate>
                <telerik:RadButton Text="Job" ID="btnShowJob" runat="server" CssClass="btnActivity" OnClick="btnShowJob_Click" />
                <telerik:RadTextBox runat="server" ID="txtSelectedJob" ReadOnly="true" />
                <asp:HiddenField runat="server" ID="hiddenSelectedJobId" />
                <br /><br />
                <telerik:RadButton Text="<%$ Resources:Mes,Mold %>" ID="btnShowMold" runat="server" CssClass="btnActivity" OnClick="btnShowMold_Click" />
                <telerik:RadTextBox runat="server" ID="txtSelectedMold" ReadOnly="true" />
                <asp:HiddenField runat="server" ID="hiddenSelectedMoldId" />
                <br /><br />
                <telerik:RadButton Text="MP" ID="btnShowMP" runat="server" CssClass="btnActivity" OnClick="btnShowMP_Click" />
                <telerik:RadTextBox runat="server" ID="txtSelectedMP" ReadOnly="true" />
                <asp:HiddenField runat="server" ID="hiddenSelectedMPId" />
                <br /><br />
                <div class="row alDx">
                    <telerik:RadButton ID="btnConfirmUpdateTransaction" runat="server" OnClick="btnConfirmUpdateTransaction_Click" CssClass="btnActivity">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnCancelUpdateTransaction" runat="server" OnClick="btnCancelUpdateTransaction_Click" CssClass="btnActivity">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Body>
</mcf:PopUpDialog>

<%--<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowJob" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowMold" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowMP" EventName="Click" />
    </Triggers>
    <ContentTemplate>--%>
<mcf:PopUpDialog ID="PopUpDialogSelection" Title="Seleziona" runat="server" Width="80%" MarginLeft="10%">
    <Body>
        <br />
        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowJob" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowMold" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowMP" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <telerik:RadGrid runat="server" ID="tableSelection" RenderMode="Lightweight" AutoGenerateColumns="false"
                    LocalizationPath="~/App_GlobalResources/">
                    <MasterTableView FilterExpression="" Caption="">
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:Panel ID="panelMP" runat="server" Visible="false">
                    <asp:TextBox runat="server" ID="txtNewCodeRawMaterial" />
                    <telerik:RadGrid runat="server" ID="tableMP" RenderMode="Lightweight" AutoGenerateColumns="false"
                        LocalizationPath="~/App_GlobalResources/" DataSourceID="edsMP">
                        <MasterTableView FilterExpression="" Caption="MP" DataKeyNames="Id">
                            <Columns>
                                <telerik:GridBoundColumn DataField="Code" HeaderText="MP">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Description" HeaderText="Lotto">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </asp:Panel>
                <br />
                <div class="row alDx">
                    <telerik:RadButton ID="btnConfirmSelection" runat="server" OnClick="btnConfirmSelection_Click" CssClass="btnActivity">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnCancelSelection" runat="server" OnClick="btnCancelSelection_Click" CssClass="btnActivity">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Body>
</mcf:PopUpDialog>
  <%--  </ContentTemplate>
</asp:UpdatePanel>--%>

<mcf:PopUpDialog ID="dlgFermi" runat="server" Title="Fermi" Width="100%" MarginLeft="0%">
    <Body>
        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="causeTreeView" EventName="NodeClick" />
                    </Triggers>
                    <ContentTemplate>
                        <telerik:RadGrid runat="server" ID="gridFermi" RenderMode="Lightweight" AutoGenerateColumns="false" AllowMultiRowSelection="false"
                            OnItemDataBound="gridFermi_ItemDataBound">
                            <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
                            <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id">
                                <Columns>
                                    <telerik:GridDateTimeColumn DataField="Start" HeaderText="<%$ Resources:Mes,Start %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridDateTimeColumn DataField="End" HeaderText="<%$ Resources:Mes,End %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:Mes,Status %>">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridDropDownColumn DataField="OperatorId" HeaderText="<%$ Resources:Mes,Operator %>" UniqueName="ddlOperator"
                                        ListTextField="Name" ListValueField="Id" DataSourceID="edsOperator" DropDownControlType="RadComboBox"
                                        ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                                    </telerik:GridDropDownColumn>
                                    <telerik:GridDropDownColumn DataField="CauseId" HeaderText="<%$ Resources:Mes,Cause %>" UniqueName="ddlCause"
                                        ListTextField="Description" ListValueField="Id" DataSourceID="edsCause" DropDownControlType="RadComboBox"
                                        ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                                    </telerik:GridDropDownColumn>
                                    <telerik:GridDropDownColumn DataField="IdArticle" HeaderText="<%$ Resources:Mes,Article %>" UniqueName="ddlArticle"
                                        ListTextField="Code" ListValueField="Id" DataSourceID="edsCause" DropDownControlType="RadComboBox"
                                        ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                                    </telerik:GridDropDownColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="true" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True" ScrollHeight="650px" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <b><%: GetGlobalResourceObject("Mes", "Causes") %>:</b>
                <br /><br />
                <telerik:RadTreeView RenderMode="Lightweight" runat="Server" ID="causeTreeView" OnNodeClick="causeTreeView_NodeClick">
                </telerik:RadTreeView>
                <br /><br />
                <telerik:RadButton ID="btnConfirmCausaleFermo" runat="server" CssClass="btnActivity" OnClick="btnConfirmCausaleFermo_Click">
                    <ContentTemplate>
                        <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                    </ContentTemplate>
                </telerik:RadButton>
                <telerik:RadButton ID="btnCancelCausaleFermo" runat="server" CssClass="btnActivity" OnClick="btnCancelCausaleFermo_Click">
                    <ContentTemplate>
                        <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                    </ContentTemplate>
                </telerik:RadButton>
            </div>
        </div>
    </Body>
</mcf:PopUpDialog>

<mcf:PopUpDialog ID="dlgScarti" runat="server" Title="Scarti" Width="90%" MarginLeft="5%">
    <Body>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="row">
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                        <telerik:RadTextBox runat="server" ID="txtQtyProducedTransaction" InputType="Number" ReadOnly="true" Label="Qta prodotta:" LabelWidth="95px" />
                        <br />
                        <telerik:RadTextBox runat="server" InputType="Number" ID="txtQtyWasteTransaction" ReadOnly="true" Label="Qta scarti:" LabelWidth="95px" />
                        <br /><br />
                        <telerik:RadGrid runat="server" ID="gridScarti" RenderMode="Lightweight" AutoGenerateColumns="false">
                            <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridNumericColumn DataField="Num" HeaderText="Num">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">

                        <asp:Panel class="col-xs-6 col-sm-6 col-md-6 col-lg-6" Width="330px" ID="pnlTastieraNum" runat="server" Visible="true">
                            <telerik:RadTextBox runat="server" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  ID="txtInsValue" RenderMode="Lightweight" Width="293px" Visible ="true"/>
                            <telerik:RadButton RenderMode="Lightweight" Width="95px" Height="60px" ID="btn7" runat="server" Primary="false" Text="7" OnClick="num_Click" />
                            <telerik:RadButton RenderMode="Lightweight" Width="95px" Height="60px" ID="btn8" runat="server" Primary="false" Text="8" OnClick="num_Click" />
                            <telerik:RadButton RenderMode="Lightweight" Width="95px" Height="60px" ID="btn9" runat="server" Primary="false" Text="9" OnClick="num_Click" />

                            <telerik:RadButton RenderMode="Lightweight" Width="95px" Height="60px" ID="btn4" runat="server" Primary="false" Text="4" OnClick="num_Click" />
                            <telerik:RadButton RenderMode="Lightweight" Width="95px" Height="60px" ID="btn5" runat="server" Primary="false" Text="5" OnClick="num_Click" />
                            <telerik:RadButton RenderMode="Lightweight" Width="95px" Height="60px" ID="btn6" runat="server" Primary="false" Text="6" OnClick="num_Click" />

                            <telerik:RadButton RenderMode="Lightweight" Width="95px" Height="60px" ID="btn1" runat="server" Primary="false" Text="1" OnClick="num_Click" />
                            <telerik:RadButton RenderMode="Lightweight" Width="95px" Height="60px" ID="btn2" runat="server" Primary="false" Text="2" OnClick="num_Click" />
                            <telerik:RadButton RenderMode="Lightweight" Width="95px" Height="60px" ID="btn3" runat="server" Primary="false" Text="3" OnClick="num_Click" />
                            <telerik:RadButton RenderMode="Lightweight" Width="194px" Height="60px" ID="btn0" runat="server" Primary="false" Text="0" OnClick="num_Click" />
                            <telerik:RadButton RenderMode="Lightweight" Width="95px" Height="60px" ID="btnVir" runat="server" Primary="false" Text="," OnClick="vir_Click" />
                            <telerik:RadButton RenderMode="Lightweight" Width="194px" Height="60px" ID="btnOk" runat="server" Primary="false" Text="Ok" OnClick="btnOk_Click" />
                            <telerik:RadButton RenderMode="Lightweight" Width="95px" Height="60px" ID="btnCanc" runat="server" Primary="false" Text="Canc" OnClick="canc_Click" />           
                         </asp:Panel>

                    </div>
                </div>
                <div class="row alDx">
                    <br />
                    <telerik:RadButton ID="btnConfirmScarti" runat="server" CssClass="btnActivity" OnClick="btnConfirmScarti_Click">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnCancelScarti" runat="server" CssClass="btnActivity" OnClick="btnCancelScarti_Click">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Body>
</mcf:PopUpDialog>


<ef:EntityDataSource ID="edsJob" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Jobs"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsCause" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Causes"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsArticle" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Articles"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsOrder" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Orders"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsOperator" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Operators"
    OrderBy="it.Name" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsMP" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="RawMaterials"
    OrderBy="it.Name" AutoGenerateWhereClause="true" />


<%--<script type="text/javascript">
    function ChangeSize(sender) {
        sender._originalTextBoxCssText = 'width:' + ((sender._value.length) * 10) + 'px;';
        sender.updateCssClass();
    }
</script>--%>