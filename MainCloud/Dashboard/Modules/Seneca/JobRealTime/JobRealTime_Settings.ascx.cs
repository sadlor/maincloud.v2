﻿using MainCloudFramework.UI.Modules;
using MainCloudFramework.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.Seneca.JobRealTime
{
    public partial class JobRealTime_Settings : WidgetSetting
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlArea.DataBind();
            }
        }

        public override bool ApplyChanges(DataCustomSettings widgetCustomSettings)
        {
            widgetCustomSettings.AddOrUpdate("settings-destination-area-id", ddlArea.SelectedValue);
            return true;
        }

        public override void SyncChanges(DataCustomSettings widgetCustomSettings)
        {
            ddlArea.LoadDefualtAreaData();
            ddlArea.DataBind();
            ddlArea.SelectedValue = widgetCustomSettings.Find("settings-destination-area-id") as string;
        }
    }
}