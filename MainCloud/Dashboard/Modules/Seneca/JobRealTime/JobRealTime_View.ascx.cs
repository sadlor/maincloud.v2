﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.JobRealTime
{
    public partial class JobRealTime_View : DataWidget<JobRealTime_View>
    {
        public JobRealTime_View() : base(typeof(JobRealTime_View)) {}

        protected string AssetId
        {
            get
            {
                return Request.QueryString["asset"];
            }
        }

        const string JOB_PRODUCTIONWASTE_TO_MODIFY = "JobProductionWasteListToModify";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (CustomSettings.Params.ContainsKey(TRANSACTION_LIST_TO_MODIFY))
                {
                    CustomSettings.Params.Remove(TRANSACTION_LIST_TO_MODIFY);
                }
            }

            WidgetWebPartContainer.Title = " RealTime";
            if (!string.IsNullOrEmpty(AssetId))
            {
                AssetManagement.Services.AssetService assetService = new AssetManagement.Services.AssetService();
                WidgetWebPartContainer.Title = assetService.GetAssedById(AssetId).Description + WidgetWebPartContainer.Title;
                TransactionService TService = new TransactionService();
                Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);

                JobRepository jobRep = new JobRepository();
                Job jobCurrent = jobRep.FindByID(lastTrans.JobId);
                txtDescriptionJob.Text = jobCurrent?.Description;
                txtCodeOrder.Text = jobCurrent?.Order?.OrderCode;
                txtCodeArticle.Text = jobCurrent?.Order?.Article?.Code;
                txtDescriptionArticle.Text = jobCurrent?.Order?.Article?.Description;

                MoldRepository moldRep = new MoldRepository();
                Mold mold = moldRep.FindByID(lastTrans.MoldId);
                txtCodeMold.Text = mold?.Code;
                txtDescriptionMold.Text = mold?.Description;
                txtNImpronte.Text = "N° impronte = " + mold?.CavityMoldNum.ToString();
            }

            edsJob.WhereParameters.Clear();
            edsJob.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsArticle.WhereParameters.Clear();
            edsArticle.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsOrder.WhereParameters.Clear();
            edsOrder.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsCause.WhereParameters.Clear();
            edsCause.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsOperator.WhereParameters.Clear();
            edsOperator.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsMP.WhereParameters.Clear();
            edsMP.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        private class JobRecord
        {
            public string Label { get; set; }
            public decimal? Colpi { get; set; }
            public decimal? Teorico { get; set; }
            public decimal? Eseguito { get; set; }
            public string Efficienza { get; set; }
            public string GroupType { get; set; }

            public JobRecord(string label, decimal? colpi, decimal? teorico, decimal? eseguito, string efficienza, string groupType)
            {
                Label = label;
                Colpi = colpi;
                Teorico = teorico;
                Eseguito = eseguito;
                Efficienza = efficienza;
                GroupType = groupType;
            }
        }

        protected void jobGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(AssetId))
            {
                List<JobRecord> dataSource = new List<JobRecord>();
                //    //6 righe fisse per il datasource
                //    //1 -> new JobTable("Da produrre colpi", 0, 0, 0, "100%", "Last");

                TransactionService TService = new TransactionService();
                Transaction lastTrans = TService.GetLastTransactionOpen(AssetId); //passare assetId dall'area rilievi
                if (!string.IsNullOrEmpty(lastTrans.JobId))
                {
                    //JobRepository jobRep = new JobRepository();
                    Job job = lastTrans.Job; //jobRep.FindByID(lastTrans.JobId);
                    int nImpronte = 1;
                    if (!string.IsNullOrEmpty(lastTrans.MoldId))
                    {
                        MoldRepository mRep = new MoldRepository();
                        nImpronte = mRep.FindByID(lastTrans.MoldId).CavityMoldNum;
                    }

                    //generalità job
                    dataSource.Add(new JobRecord("Colpi da produrre", job.QtyOrdered / nImpronte, job.CycleTime * job.QtyOrdered, null, "", "Last"));

                    TransactionRepository TRep = new TransactionRepository();
                    List<Transaction> lastWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && !x.Open).ToList();

                    //ulitmo turno
                    var lastTeorico = lastWorkshift.Sum(x => x.PartialCounting) * job.CycleTime;
                    if (lastTeorico == 0)
                    {
                        dataSource.Add(new JobRecord("Eseguito turni precedenti", lastWorkshift.Sum(x => x.PartialCounting), 0, 0, "0%", "Last"));
                    }
                    else
                    {
                        dataSource.Add(new JobRecord("Eseguito turni precedenti", lastWorkshift.Sum(x => x.PartialCounting), lastTeorico, lastWorkshift.Sum(x => x.Duration), (lastWorkshift.Sum(x => x.Duration) / lastTeorico) * 100 + "%", "Last"));
                    }

                    //turno corrente
                    var currentTeorico = lastTrans.PartialCounting * job.CycleTime;
                    if (currentTeorico == 0)
                    {
                        dataSource.Add(new JobRecord("Colpi attuali", lastTrans.PartialCounting, currentTeorico, lastTrans.Duration, "0%", "Current"));
                    }
                    else
                    {
                        dataSource.Add(new JobRecord("Colpi attuali", lastTrans.PartialCounting, currentTeorico, lastTrans.Duration, lastTrans.Duration / currentTeorico * 100 + "%", "Current"));
                    }
                    dataSource.Add(new JobRecord("Cadenza oraria", null, 0, 0, "100%", "Current"));
                    dataSource.Add(new JobRecord("Mancanti", job.QtyOrdered / nImpronte - lastTrans.PartialCounting, 0, 0, "100%", "Current"));
                    dataSource.Add(new JobRecord("Recupero scarti", 0, 0, 0, "100%", "Current"));
                    dataSource.Add(new JobRecord("Totale colpi", 0, 0, 0, "100%", "Current"));
                }
                else
                {
                    //nessun job assegnato
                    //TODO: chiedere cosa fare in questo caso
                }

                jobGrid.DataSource = dataSource;
            }
        }

        protected void btnAttrezzaggio_Click(object sender, EventArgs e)
        {
            //chiudo e apro nuova transazione
            btnConfirmUpdateTransaction.Enabled = false;
            if (!string.IsNullOrEmpty(AssetId))
            {
                TransactionService TService = new TransactionService();
                Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);
                JobRepository jobRep = new JobRepository();
                txtSelectedJob.Text = jobRep.FindByID(lastTrans.JobId)?.Description;
                if (!string.IsNullOrEmpty(txtSelectedJob.Text))
                {
                    btnConfirmUpdateTransaction.Enabled = true;
                }
                MoldRepository moldRep = new MoldRepository();
                txtSelectedMold.Text = moldRep.FindByID(lastTrans.MoldId)?.Name;
            }
            hiddenSelectedJobId.Value = null;
            hiddenSelectedMoldId.Value = null;
            dlgUpdateTransaction.Title = "Attrezzaggio";
            dlgUpdateTransaction.OpenDialog();
        }

        protected void btnProduzione_Click(object sender, EventArgs e)
        {
            //chiudo e apro nuova transazione
            btnConfirmUpdateTransaction.Enabled = false;
            if (!string.IsNullOrEmpty(AssetId))
            {
                TransactionService TService = new TransactionService();
                Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);
                JobRepository jobRep = new JobRepository();
                txtSelectedJob.Text = jobRep.FindByID(lastTrans.JobId)?.Description;
                if (!string.IsNullOrEmpty(txtSelectedJob.Text))
                {
                    btnConfirmUpdateTransaction.Enabled = true;
                }
                MoldRepository moldRep = new MoldRepository();
                txtSelectedMold.Text = moldRep.FindByID(lastTrans.MoldId)?.Name;
            }
            hiddenSelectedJobId.Value = null;
            hiddenSelectedMoldId.Value = null;
            dlgUpdateTransaction.Title = "Produzione";
            dlgUpdateTransaction.OpenDialog();
        }

        protected void btnManutenzione_Click(object sender, EventArgs e)
        {
            //chiudo e apro nuova transazione
        }

        protected void btnShowJob_Click(object sender, EventArgs e)
        {
            PopUpDialogSelection.Title = "Job";
            PopUpDialogSelection.OpenDialog();
            //tableMP.Visible = false;
            panelMP.Visible = false;
            tableSelection.Visible = true;

            tableSelection.MasterTableView.Name = "Job";
            tableSelection.MasterTableView.Caption = "Job";
            tableSelection.MasterTableView.DataKeyNames = new string[] { "Id" };
            tableSelection.MasterTableView.Columns.Clear();

            GridBoundColumn bCol = new GridBoundColumn();
            bCol.DataField = "Code";
            bCol.HeaderText = GetGlobalResourceObject("Mes", "Code").ToString();
            tableSelection.MasterTableView.Columns.Add(bCol);

            bCol = new GridBoundColumn();
            bCol.DataField = "Description";
            bCol.HeaderText = GetGlobalResourceObject("Mes", "Description").ToString();
            tableSelection.MasterTableView.Columns.Add(bCol);

            GridDropDownColumn col = new GridDropDownColumn();
            col.DataField = "OrderId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Order").ToString();
            col.ListTextField = "Description";
            col.ListValueField = "Id";
            col.DataSourceID = "edsOrder";
            tableSelection.MasterTableView.Columns.Add(col);

            col = new GridDropDownColumn();
            col.DataField = "ArticleId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Code").ToString() + GetGlobalResourceObject("Mes", "Article").ToString();
            col.ListTextField = "Code";
            col.ListValueField = "Id";
            col.DataSourceID = "edsArticle";
            tableSelection.MasterTableView.Columns.Add(col);

            col = new GridDropDownColumn();
            col.DataField = "ArticleId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Description").ToString() + GetGlobalResourceObject("Mes", "Article").ToString();
            col.ListTextField = "Description";
            col.ListValueField = "Id";
            col.DataSourceID = "edsArticle";
            tableSelection.MasterTableView.Columns.Add(col);

            tableSelection.DataSource = edsJob;
            tableSelection.DataBind();
        }

        protected void btnCancelSelection_Click(object sender, EventArgs e)
        {
            PopUpDialogSelection.CloseDialog();
        }

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            if (tableSelection.Visible)
            {
                if (tableSelection.SelectedItems.Count > 0)
                {
                    GridDataItem item = (GridDataItem)tableSelection.SelectedItems[0];//get selected row

                    ArticleMoldService AMS = new ArticleMoldService();
                    switch (tableSelection.MasterTableView.Name)
                    {
                        case "Job":
                            string id = item.GetDataKeyValue("Id").ToString();
                            hiddenSelectedJobId.Value = id;
                            JobRepository jobRep = new JobRepository();
                            txtSelectedJob.Text = jobRep.FindByID(id).Description;
                            if (AMS.GetMoldListByJobId(id).Count == 1)
                            {
                                hiddenSelectedMoldId.Value = AMS.GetMoldListByJobId(id)[0].MoldId;
                                txtSelectedMold.Text = AMS.GetMoldById(hiddenSelectedMoldId.Value).Name;
                            }
                            btnConfirmUpdateTransaction.Enabled = true;
                            break;
                        case "Mold":
                            string moldId = item.GetDataKeyValue("MoldId").ToString();
                            hiddenSelectedMoldId.Value = moldId;
                            txtSelectedMold.Text = AMS.GetMoldById(moldId).Name;
                            break;
                    }
                }
            }
            else
            {
                //MP
                GridDataItem item = (GridDataItem)tableMP.SelectedItems[0];//get selected row
                string id = item.GetDataKeyValue("Id").ToString();
                hiddenSelectedMPId.Value = id;
                //RawMaterialRepository rmRep = new RawMaterialRepository();
                //txtSelectedMP.Text = rmRep.FindByID(id).Code;
            }
            PopUpDialogSelection.CloseDialog();
        }

        protected void btnCancelUpdateTransaction_Click(object sender, EventArgs e)
        {
            hiddenSelectedJobId.Value = null;
            hiddenSelectedMoldId.Value = null;
            hiddenSelectedMPId.Value = null;
            dlgUpdateTransaction.CloseDialog();
        }

        protected void btnConfirmUpdateTransaction_Click(object sender, EventArgs e)
        {
            //chiudo e apro nuova transazione

            TransactionService TService = new TransactionService();

            KeyValuePair<MesEnum.TransactionParam, object> cause = new KeyValuePair<MesEnum.TransactionParam, object>();
            switch (dlgUpdateTransaction.Title)
            {
                case "Attrezzaggio":
                    cause = new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, MesEnum.Cause.SetUp);

                    if (!string.IsNullOrEmpty(TService.GetLastTransactionOpen(AssetId).JobId) && string.IsNullOrEmpty(hiddenSelectedJobId.Value) && string.IsNullOrEmpty(TService.GetLastTransactionOpen(AssetId).CauseId))
                    {
                        //se il job era già presente, e non è stato cambiato, ed è senza causale, aggiorno la transazione in corso senza chiuderla
                        if (!string.IsNullOrEmpty(hiddenSelectedMoldId.Value))
                        {
                            // se c'è un cambio di stampo, aggiorno anche quello nella transaction corrente
                            TService.UpdateLastTransactionOpen(AssetId,
                                new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                            cause });
                        }
                        else
                        {
                            //c'è solo la causale da aggiornare
                            TService.UpdateLastTransactionOpen(AssetId, new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] { cause });
                        }

                        goto EndChangeTransaction;
                    }

                    break;
                case "Produzione":
                    cause = new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, MesEnum.Cause.Production);

                    if (!string.IsNullOrEmpty(TService.GetLastTransactionOpen(AssetId).JobId) && 
                        string.IsNullOrEmpty(hiddenSelectedJobId.Value) && 
                        (string.IsNullOrEmpty(TService.GetLastTransactionOpen(AssetId).CauseId) || TService.LastTransactionOpenIsInProduction(AssetId)))
                    {
                        //se il job era già presente, e non è stato cambiato, ed è senza causale o è in produzione, aggiorno la transazione in corso senza chiuderla
                        if (!string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && !TService.LastTransactionOpenIsInProduction(AssetId))
                        {
                            // se c'è un cambio di stampo, aggiorno anche quello nella transaction corrente
                            TService.UpdateLastTransactionOpen(AssetId,
                                new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                    new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                                    cause });
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(hiddenSelectedMoldId.Value))
                            {
                                TService.UpdateLastTransactionOpen(AssetId,
                                new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                    new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value) });
                            }
                        }
                    }

                    break;
            }
            if (!string.IsNullOrEmpty(hiddenSelectedJobId.Value) || !string.IsNullOrEmpty(hiddenSelectedMoldId.Value) || !string.IsNullOrEmpty(hiddenSelectedMPId.Value))
            {
                if (!string.IsNullOrEmpty(hiddenSelectedJobId.Value) && !string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && !string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                {
                    //Job, mold, MP
                    TService.CloseAndOpenTransaction(
                    AssetId,
                    new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, hiddenSelectedJobId.Value),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.MP, hiddenSelectedMPId.Value),
                            cause});
                }
                else
                {
                    if (!string.IsNullOrEmpty(hiddenSelectedJobId.Value) && !string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //Job e mold
                        TService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, hiddenSelectedJobId.Value),
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                                cause});
                        goto EndChangeTransaction;
                    }
                    if (!string.IsNullOrEmpty(hiddenSelectedJobId.Value) && string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && !string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //Job e MP
                        TService.CloseAndOpenTransaction(
                        AssetId,
                        new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, hiddenSelectedJobId.Value),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.MP, hiddenSelectedMPId.Value),
                            cause});
                        goto EndChangeTransaction;
                    }
                    if (string.IsNullOrEmpty(hiddenSelectedJobId.Value) && !string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && !string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //Mold e MP
                        TService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.MP, hiddenSelectedMPId.Value),
                                cause});
                        goto EndChangeTransaction;
                    }
                    if (!string.IsNullOrEmpty(hiddenSelectedJobId.Value) && string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //Job
                        TService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, hiddenSelectedJobId.Value),
                                cause});
                        goto EndChangeTransaction;
                    }
                    if (string.IsNullOrEmpty(hiddenSelectedJobId.Value) && !string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //Mold
                        TService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                                cause});
                        goto EndChangeTransaction;
                    }
                    if (string.IsNullOrEmpty(hiddenSelectedJobId.Value) && string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && !string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //MP
                        TService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.MP, hiddenSelectedMPId.Value),
                                cause});
                        goto EndChangeTransaction;
                    }
                }
            }
            else
            {
                //c'è solo la causale da cambiare
                TService.CloseAndOpenTransaction(
                    AssetId,
                    new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] { cause });
                goto EndChangeTransaction;
            }

            EndChangeTransaction:
            dlgUpdateTransaction.CloseDialog();
        }

        protected void btnShowMold_Click(object sender, EventArgs e)
        {
            PopUpDialogSelection.Title = GetGlobalResourceObject("Mes", "Mold").ToString();
            PopUpDialogSelection.OpenDialog();
            panelMP.Visible = false;
            tableSelection.Visible = true;

            tableSelection.MasterTableView.Name = "Mold";
            tableSelection.MasterTableView.Caption = GetGlobalResourceObject("Mes", "Mold").ToString();
            tableSelection.MasterTableView.DataKeyNames = new string[] { "MoldId" };
            tableSelection.MasterTableView.Columns.Clear();

            GridDropDownColumn col = new GridDropDownColumn();
            col.DataField = "MoldId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Code").ToString();
            col.ListTextField = "Code";
            col.ListValueField = "Id";
            col.DataSourceID = "edsMold";
            tableSelection.MasterTableView.Columns.Add(col);

            col = new GridDropDownColumn();
            col.DataField = "MoldId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Name").ToString();
            col.ListTextField = "Name";
            col.ListValueField = "Id";
            col.DataSourceID = "edsMold";
            tableSelection.MasterTableView.Columns.Add(col);

            col = new GridDropDownColumn();
            col.DataField = "ArticleId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Code").ToString() + GetGlobalResourceObject("Mes", "Article").ToString();
            col.ListTextField = "Code";
            col.ListValueField = "Id";
            col.DataSourceID = "edsArticle";
            tableSelection.MasterTableView.Columns.Add(col);

            col = new GridDropDownColumn();
            col.DataField = "ArticleId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Article").ToString();
            col.ListTextField = "Description";
            col.ListValueField = "Id";
            col.DataSourceID = "edsArticle";
            tableSelection.MasterTableView.Columns.Add(col);

            ArticleMoldService AMS = new ArticleMoldService();
            TransactionService TService = new TransactionService();
            string jobId = TService.GetLastTransactionOpen(AssetId)?.JobId;
            if (!string.IsNullOrEmpty(jobId))
            {
                jobId = hiddenSelectedJobId.Value;
            }
            tableSelection.DataSource = AMS.GetMoldListByJobId(jobId); //se un job non è già stato scelto, il datasource sarà vuoto
            tableSelection.DataBind();
        }

        protected void btnShowMP_Click(object sender, EventArgs e)
        {
            PopUpDialogSelection.Title = "MP";
            PopUpDialogSelection.OpenDialog();
            panelMP.Visible = true;
            tableSelection.Visible = false;
            tableMP.DataBind();
        }

        //protected void btnAddRawMaterial_Click(object sender, EventArgs e)
        //{
        //    panelMP.Visible = true;
        //    txtNewCodeRawMaterial.Focus();
        //}

        //protected void btnCancelAddNewRawMaterial_Click(object sender, EventArgs e)
        //{
        //    txtNewCodeRawMaterial.Text = "";
        //    panelMP.Visible = false;
        //}

        protected void btnConfirmAddNewRawMaterial_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNewCodeRawMaterial.Text))
            {
                //RawMaterial rm = new RawMaterial();
                //rm.Code = txtNewCodeRawMaterial.Text;
                //rm.ApplicationId = MultiTenantsHelper.ApplicationId;
                //RawMaterialRepository rmRep = new RawMaterialRepository();
                //if (!rmRep.ReadAll(x => x.Code == rm.Code && x.ApplicationId == rm.ApplicationId).Any())
                //{
                //    //Non esiste, insert
                //    rmRep.Insert(rm);
                //    rmRep.SaveChanges();
                //}
                //else
                //{
                //    //esiste già
                //}

                //tableMP.DataBind();
                //tableMP.MasterTableView.FindItemByKeyValue("Id", rmRep.ReadAll(x => x.Code == rm.Code && x.ApplicationId == rm.ApplicationId).First().Id).Selected = true;
            }

            txtNewCodeRawMaterial.Text = "";
            panelMP.Visible = false;
        }

        protected void btnFermi_Click(object sender, EventArgs e)
        {
            dlgFermi.OpenDialog();

            TransactionService TS = new TransactionService();
            List<Transaction> datasource = TS.TransactionToJustifyList(AssetId);
            gridFermi.DataSource = datasource;
            gridFermi.DataBind();

            LoadCauseTreeView();
        }

        protected void gridFermi_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                item["Status"].Text = item["Status"].Text == "False" ? "Off" : "On";
            }
        }

        protected void LoadCauseTreeView()
        {
            causeTreeView.Nodes.Clear();
            CauseTypeRepository CTR = new CauseTypeRepository();
            List<CauseType> causeList = CTR.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && x.Description == "Fermi").ToList();
            foreach (CauseType ct in causeList)
            {
                //Node CauseType
                RadTreeNode nodeCT = new RadTreeNode(ct.Description, ct.Id);
                nodeCT.PostBack = false;
                nodeCT.Expanded = true;
                foreach (Cause c in ct.Cause.OrderBy(x => x.Code))
                {
                    RadTreeNode nodeC = new RadTreeNode(c.Code + " - " + c.Description, c.Id);
                    nodeC.PostBack = true;
                    nodeC.BorderStyle = BorderStyle.Solid;
                    nodeC.BorderColor = System.Drawing.Color.Black;
                    nodeC.Height = Unit.Percentage(30);
                    //nodeC.Width = Unit.Percentage(66);
                    nodeCT.Nodes.Add(nodeC);
                }
                causeTreeView.Nodes.Add(nodeCT);
            }
        }

        const string TRANSACTION_LIST_TO_MODIFY = "TransactionListToModify";

        protected void causeTreeView_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            TransactionRepository tRep = new TransactionRepository();
            List<Transaction> transactionListToModify = new List<Transaction>();
            if (CustomSettings.Params.ContainsKey(TRANSACTION_LIST_TO_MODIFY))
            {
                transactionListToModify = JsonConvert.DeserializeObject<List<Transaction>>(CustomSettings.Params[TRANSACTION_LIST_TO_MODIFY].ToString());
            }
            GridItemCollection listItem = gridFermi.SelectedItems; //get selected rows
            foreach (GridDataItem item in listItem)
            {
                int id = (int)item.GetDataKeyValue("Id");
                Transaction t = tRep.FindByID(id);
                t.CauseId = e.Node.Value;
                transactionListToModify.Add(t);
            }
            CustomSettings.AddOrUpdate(TRANSACTION_LIST_TO_MODIFY, transactionListToModify);

            //update table fermi
            TransactionService TS = new TransactionService();
            List<Transaction> datasource = TS.TransactionToJustifyList(AssetId);
            foreach (Transaction t in transactionListToModify)
            {
                int index = datasource.IndexOf(datasource.Where(x => x.Id == t.Id).First());
                datasource.ElementAt(index).CauseId = t.CauseId;
            }
            gridFermi.DataSource = datasource;
            gridFermi.DataBind();
        }

        protected void btnConfirmCausaleFermo_Click(object sender, EventArgs e)
        {
            TransactionRepository tRep = new TransactionRepository();
            if (CustomSettings.Params.ContainsKey(TRANSACTION_LIST_TO_MODIFY))
            {
                List<Transaction> transactionListToModify = JsonConvert.DeserializeObject<List<Transaction>>(CustomSettings.Params[TRANSACTION_LIST_TO_MODIFY].ToString());
                tRep.UpdateAll(transactionListToModify);
                tRep.SaveChanges();
                CustomSettings.Params.Remove(TRANSACTION_LIST_TO_MODIFY);
            }
            dlgFermi.CloseDialog();
        }

        protected void btnCancelCausaleFermo_Click(object sender, EventArgs e)
        {
            if (CustomSettings.Params.ContainsKey(TRANSACTION_LIST_TO_MODIFY))
            {
                CustomSettings.Params.Remove(TRANSACTION_LIST_TO_MODIFY);
            }
            dlgFermi.CloseDialog();
        }

        protected void btnScarti_Click(object sender, EventArgs e)
        {
            dlgScarti.OpenDialog();

            TransactionService TService = new TransactionService();
            Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);
            //string jobId = lastTrans.JobId;

            txtQtyProducedTransaction.Text = lastTrans.PartialCounting.ToString();

            CauseTypeRepository CTR = new CauseTypeRepository();
            List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
            List<ScartiRecord> dataSource = new List<ScartiRecord>();
            foreach (CauseType ct in causeList)
            {
                foreach (Cause c in ct.Cause)
                {
                    dataSource.Add(new ScartiRecord(c.Id, c.Code + " - " + c.Description));
                }
            }

            gridScarti.DataSource = dataSource;
            gridScarti.DataBind();
        }

        private class ScartiRecord
        {
            public string CauseId { get; set; }
            public string Description { get; set; } // code - description
            public decimal Num { get; set; } //num scarti

            public ScartiRecord(string id, string description)
            {
                CauseId = id;
                Description = description;
            }

            public ScartiRecord(string id, string description, decimal num)
            {
                CauseId = id;
                Description = description;
                Num = num;
            }
        }

        protected void btnConfirmScarti_Click(object sender, EventArgs e)
        {
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();
            if (CustomSettings.Params.ContainsKey(JOB_PRODUCTIONWASTE_TO_MODIFY))
            {
                List<ScartiRecord> jobWasteListToModify = JsonConvert.DeserializeObject<List<ScartiRecord>>(CustomSettings.Params[JOB_PRODUCTIONWASTE_TO_MODIFY].ToString());
                TransactionService TService = new TransactionService();
                Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);
                foreach (ScartiRecord sr in jobWasteListToModify)
                {
                    JobProductionWaste jpw = new JobProductionWaste();
                    jpw.AssetId = AssetId;
                    jpw.CauseId = sr.CauseId;
                    jpw.Date = DateTime.Now;
                    jpw.JobId = lastTrans.JobId;
                    jpw.OperatorId = lastTrans.OperatorId;
                    jpw.QtyProductionWaste = sr.Num;
                    jpw.ArticleId = lastTrans?.Job?.Order?.IdArticle;
                    jpwRep.Insert(jpw);
                }
                jpwRep.SaveChanges();
                CustomSettings.Params.Remove(JOB_PRODUCTIONWASTE_TO_MODIFY);
            }

            dlgScarti.CloseDialog();
        }

        protected void btnCancelScarti_Click(object sender, EventArgs e)
        {
            if (CustomSettings.Params.ContainsKey(JOB_PRODUCTIONWASTE_TO_MODIFY))
            {
                CustomSettings.Params.Remove(JOB_PRODUCTIONWASTE_TO_MODIFY);
            }
            dlgScarti.CloseDialog();
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            string destinationAreaId = CustomSettings.Find("settings-destination-area-id") as string;
            if (!string.IsNullOrEmpty(destinationAreaId))
            {
                WidgetAreaRepository areaService = new WidgetAreaRepository();
                WidgetArea wa = areaService.FindByID(destinationAreaId);

                Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
            }
        }

        #region Tastierino numrerico
        protected void num_Click(object sender, EventArgs e)
        {
            float num;
            RadButton btn = (RadButton)sender;
            txtInsValue.Text = txtInsValue.Text + btn.Text;
            if (float.TryParse(txtInsValue.Text, out num))
            {
                txtInsValue.Text = num.ToString();
            }
        }

        protected void vir_Click(object sender, EventArgs e)
        {
            RadButton btn = (RadButton)sender;
            if (txtInsValue.Text.IndexOf(",") < 0)
            {
                txtInsValue.Text = txtInsValue.Text + btn.Text;
            }

        }

        protected void canc_Click(object sender, EventArgs e)
        {
            float num;
            if (txtInsValue.Text.Length > 0)
            {
                txtInsValue.Text = txtInsValue.Text.Substring(0, txtInsValue.Text.Length - 1);
            }
            if (float.TryParse(txtInsValue.Text, out num))
            {
                txtInsValue.Text = num.ToString();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            float num;
            if (float.TryParse(txtInsValue.Text, out num))
            {
                txtInsValue.Text = num.ToString();
            }

            List<ScartiRecord> jobWasteListToModify = new List<ScartiRecord>();
            if (CustomSettings.Params.ContainsKey(JOB_PRODUCTIONWASTE_TO_MODIFY))
            {
                jobWasteListToModify = JsonConvert.DeserializeObject<List<ScartiRecord>>(CustomSettings.Params[JOB_PRODUCTIONWASTE_TO_MODIFY].ToString());
            }

            GridDataItem selectedItem = (GridDataItem)gridScarti.SelectedItems[0]; //get selected row
            jobWasteListToModify.Add(new ScartiRecord(selectedItem.GetDataKeyValue("CauseId").ToString(), "", Convert.ToDecimal(txtInsValue.Text)));
            CustomSettings.AddOrUpdate(JOB_PRODUCTIONWASTE_TO_MODIFY, jobWasteListToModify);

            //update datasource
            CauseTypeRepository CTR = new CauseTypeRepository();
            List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
            List<ScartiRecord> dataSource = new List<ScartiRecord>();
            foreach (CauseType ct in causeList)
            {
                foreach (Cause c in ct.Cause)
                {
                    dataSource.Add(new ScartiRecord(c.Id, c.Code + " - " + c.Description));
                }
            }

            foreach (ScartiRecord sr in jobWasteListToModify)
            {
                int index = dataSource.IndexOf(dataSource.Where(x => x.CauseId == sr.CauseId).First());
                dataSource.ElementAt(index).Num = sr.Num;
            }
            gridScarti.DataSource = dataSource;
            gridScarti.DataBind();

            txtQtyWasteTransaction.Text = jobWasteListToModify.Sum(x => x.Num).ToString();

            txtInsValue.Text = "";
        }
        #endregion
    }
}