﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OperatorMachineMonitored_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.OperatorMachineMonitored.OperatorMachineMonitored_View" %>

<style type="text/css">
    .RadDockZone_Bootstrap{
        background-color: #fcfcfc;
    }
</style>

<%--<script src="<%=ResolveUrl("~/Dashboard/Modules/Seneca/OperatorMachineMonitored/scripts.js")%>"> type="text/javascript"></script>
<script type="text/javascript">
    //<![CDATA[
    serverIDs({
        ajaxID: "<%= RadAjaxPanel1.ClientID%>",
        ajaxServerID: "<%= RadAjaxPanel1.UniqueID %>"
    });
    //]]>
</script>--%>

<%--<telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" MinDisplayTime="500">
</telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
    OnAjaxRequest="RadAjaxPanel1_AjaxRequest" Width="100%">--%>

<telerik:RadDockLayout runat="server" ID="DockLayout" OnSaveDockLayout="DockLayout_SaveDockLayout">
    <div class="row" style="width:100%;">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <telerik:RadDockZone runat="server" ID="DockZone1" Orientation="Vertical" MinHeight="150px" Width="100%" FitDocks="true">  
            </telerik:RadDockZone>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <telerik:RadDockZone runat="server" ID="DockZone2" Orientation="Vertical" MinHeight="150px" Width="100%" FitDocks="true">
            </telerik:RadDockZone>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <telerik:RadDockZone runat="server" ID="DockZone3" Orientation="Vertical" MinHeight="150px" Width="100%" FitDocks="true">
            </telerik:RadDockZone>
        </div>
    </div>
    <div class="row" style="width:100%;">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <telerik:RadDockZone runat="server" ID="DockZone4" Orientation="Vertical" MinHeight="150px" Width="100%" FitDocks="true">
            </telerik:RadDockZone>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <telerik:RadDockZone runat="server" ID="DockZone5" Orientation="Vertical" MinHeight="150px" Width="100%" FitDocks="true">
            </telerik:RadDockZone>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <telerik:RadDockZone runat="server" ID="DockZone6" Orientation="Vertical" MinHeight="150px" Width="100%" FitDocks="true">
            </telerik:RadDockZone>
        </div>
    </div>
</telerik:RadDockLayout>

<telerik:RadButton ID="btnAddNewMachine" Text="Add new machine" runat="server" OnClick="btnAddNewMachine_Click" />

<%--</telerik:RadAjaxPanel>--%>