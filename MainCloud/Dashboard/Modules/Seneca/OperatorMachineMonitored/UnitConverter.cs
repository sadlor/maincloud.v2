﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.Seneca.OperatorMachineMonitored
{
    public class UnitConverter : JavaScriptConverter
    {
        public override object Deserialize(System.Collections.Generic.IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException("dictionary");
            }

            if (object.ReferenceEquals(type, typeof(Unit)) && dictionary.Count > 0)
            {
                if ((bool)dictionary["IsEmpty"])
                    return Unit.Empty;

                double value = Convert.ToDouble(dictionary["Value"]);
                UnitType unitType = (UnitType)Enum.Parse(typeof(UnitType), dictionary["Type"].ToString());
                Unit unit = new Unit(value, unitType);

                return unit;
            }

            return null;
        }


        public override System.Collections.Generic.IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
        {
            Unit unit = (Unit)obj;

            if (unit != null)
            {
                Dictionary<string, object> dict = new Dictionary<string, object>();
                dict.Add("Type", unit.Type);
                dict.Add("Value", unit.Value);
                dict.Add("IsEmpty", unit.IsEmpty);
                return dict;
            }

            return new Dictionary<string, object>();
        }

        public override System.Collections.Generic.IEnumerable<Type> SupportedTypes
        {
            get { return new ReadOnlyCollection<Type>(new List<Type>(new Type[] { typeof(Unit) })); }
        }
    }
}