﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MachineBox.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.OperatorMachineMonitored.MachineBox" %>

<div class="row" style="width:100%;">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <asp:Label Text="Macchina" runat="server" />
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <asp:TextBox runat="server" Text="CodiceSeriale" />
    </div>

<asp:Image ImageUrl="imageurl" runat="server" />

</div>
<div class="row" style="width:100%;">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <asp:TextBox runat="server" Text="Stato" />
    </div>
</div>
<div class="row" style="width:100%;">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <asp:TextBox runat="server" Text="Operatore" />
    </div>
</div>
<div class="row" style="width:100%;">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <asp:TextBox runat="server" Text="Commessa" />
    </div>
</div>
<asp:Button ID="btnLogout" Text="Logout" runat="server" OnClick="btnLogout_Click" />