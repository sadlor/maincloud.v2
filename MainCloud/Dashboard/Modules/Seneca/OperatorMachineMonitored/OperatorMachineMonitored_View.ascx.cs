﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.OperatorMachineMonitored
{
    public partial class OperatorMachineMonitored_View : DataWidget<OperatorMachineMonitored_View>
    {
        public OperatorMachineMonitored_View() : base(typeof(OperatorMachineMonitored_View)) {}

        public string MyDockStates
        {
            get
            {
                string _dockStates = String.Empty;
                HttpCookie positionsCookie = Request.Cookies["PositionsCookie"];
                if (Object.Equals(positionsCookie, null))
                {
                    _dockStates = String.Empty;
                }
                else
                {
                    _dockStates = DecodeString(positionsCookie.Value);
                }
                return _dockStates;
            }
            set
            {
                // Store the positions in a cookie. Note, that if there are lots of dock objects on the page
                // the cookie length might become insufficient. In this case it would be better to use the 
                // cookie to store a key from a database, where the positions will be actually stored.
                //
                // You can store the positions directly in a database and use the ID of the currently logged 
                // user as a key to his personalized positions.
                HttpCookie positionsCookie = default(HttpCookie);
                positionsCookie = new HttpCookie("PositionsCookie");
                positionsCookie.Value = EncodeString(value);
                positionsCookie.Expires = DateTime.Now.AddYears(1);
                Response.Cookies.Add(positionsCookie);
            }
        }

        //Encode and Decode the dock state to make sure it can be stored in cookies in every browser
        private static string EncodeString(string toEncode)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(toEncode));
        }
        private static string DecodeString(string toDecode)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(toDecode));
        }

        Dictionary<string, string> dockParents = new Dictionary<string, string>();
        Dictionary<string, int> dockIndices = new Dictionary<string, int>();

        protected void Page_Init(object sender, EventArgs e)
        {
            LoadState(MyDockStates);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadState(MyDockStates);
        }

        private RadDock CreateRadDock()
        {
            RadDock dock = new RadDock();
            dock.UniqueName = Guid.NewGuid().ToString().Replace("-", "a");
            dock.ID = string.Format("RadDock{0}", dock.UniqueName);
            //dock.AllowedZones = new string[] { "DockZoneMachine" };
            dock.AutoPostBack = false; //da cambiare in true
            dock.Collapsed = false;
            dock.Title = "Nome Macchina";
            dock.ContentContainer.Controls.Add(LoadControl("~/Dashboard/Modules/Seneca/OperatorMachineMonitored/MachineBox.ascx"));
            dock.DefaultCommands = Telerik.Web.UI.Dock.DefaultCommands.None;

            //DockLayout.Controls.Add(dock);
            //dock.Dock(FindEmptyDockZone());

            return dock;
        }

        private RadDock ReCreateRadDock(DockState state)
        {
            RadDock dock = new RadDock();
            dock.ApplyState(state);
            dock.UniqueName = state.UniqueName;
            dock.ID = string.Format("RadDock{0}", dock.UniqueName);
            //dock.AllowedZones = new string[] { "DockZoneMachine" };
            dock.AutoPostBack = false; //da cambiare in true
            dock.Collapsed = false;
            dock.Title = "Nome Macchina";
            dock.ContentContainer.Controls.Add(LoadControl("~/Dashboard/Modules/Seneca/OperatorMachineMonitored/MachineBox.ascx"));
            dock.DefaultCommands = Telerik.Web.UI.Dock.DefaultCommands.None;

            dock.Dock(DockLayout.Controls.OfType<RadDockZone>().Where(x => x.ClientID == state.DockZoneID).First());

            return dock;
        }

        protected void btnAddNewMachine_Click(object sender, EventArgs e)
        {
            RadDock dock = CreateRadDock();
            DockLayout.Controls.Add(dock);
            dock.Dock(FindEmptyDockZone());

            //RadDock dock = new RadDock();
            //dock.AllowedZones = new string[] { "DockZoneMachine" };
            //dock.AutoPostBack = false; //da cambiare in true
            //dock.Collapsed = false;
            //dock.Title = "Nome Macchina";
            //dock.ContentContainer.Controls.Add(LoadControl("~/Dashboard/Modules/Seneca/OperatorMachineMonitored/MachineBox.ascx"));
            //dock.DefaultCommands = Telerik.Web.UI.Dock.DefaultCommands.None;

            //DockLayout.Controls.Add(dock);
            //dock.Dock(FindEmptyDockZone());
            //MyDockStates = SaveState();
        }

        private RadDockZone FindEmptyDockZone()
        {
            return DockLayout.Controls.OfType<RadDockZone>().Where(x => x.Docks.Count == 0).First();//.RegisteredZones.Where(x => x.Docks.Count == 0).OrderBy(x => x.ID).First();
        }

        protected void RadAjaxPanel1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "AddDock")
            {
                RadDock dock = CreateRadDock();
                DockLayout.Controls.Add(dock);
                dock.Dock(FindEmptyDockZone());
                //CreateSaveStateTrigger(dock);

                MyDockStates = SaveState();
            }
        }

        //protected void DockLayout_LoadDockLayout(object sender, DockLayoutEventArgs e)
        //{
        //    //Save the initial state of the docks so it can be loaded when the "Reset" button is clicked
        //    if (!Page.IsPostBack)
        //    {
        //        Session["InitialState"] = SaveState();
        //        foreach (RadDock dock in DockLayout.RegisteredDocks)
        //        {
        //            dock.UniqueName = dock.ID;
        //            //Save the initial DockZones and Indices so that the DockLayout can be reset
        //            dockParents.Add(dock.UniqueName, dock.DockZoneID);
        //            dockIndices.Add(dock.UniqueName, dock.Index);
        //        }
        //    }
        //    LoadState(MyDockStates, e.Positions, e.Indices);
        //}

        private void LoadState(string myDockState)//, Dictionary<string, string> dockParents, Dictionary<string, int> dockIndices)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<JavaScriptConverter> converters = new List<JavaScriptConverter>();
            converters.Add(new UnitConverter());
            serializer.RegisterConverters(converters);
            foreach (string str in myDockState.Split('|'))
            {
                if (str != String.Empty)
                {
                    DockState state = serializer.Deserialize<DockState>(str);
                    try
                    {
                        RadDock dock = ReCreateRadDock(state);
                        DockLayout.Controls.Add(dock); //dock = DockLayout.FindControl(state.UniqueName) as RadDock;
                        //dock.ApplyState(state);
                        //dock.Dock(DockLayout.Controls.OfType<RadDockZone>().Where(x => x.ClientID == state.DockZoneID).First());
                        //dock.ID = string.Format("RadDock{0}", dock.UniqueName);
                    }
                    catch (NullReferenceException)
                    {
                        //non ci sono dock
                    }
                    //dockParents[state.UniqueName] = state.DockZoneID;
                    //dockIndices[state.UniqueName] = state.Index;
                }
            }
        }

        private string SaveState()
        {
            List<DockState> dockStates = DockLayout.GetRegisteredDocksState();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<JavaScriptConverter> converters = new List<JavaScriptConverter>();
            converters.Add(new UnitConverter());
            serializer.RegisterConverters(converters);

            string stateString = String.Empty;
            foreach (DockState state in dockStates)
            {
                string ser = serializer.Serialize(state);
                stateString = stateString + "|" + ser;
            }
            return stateString;
        }

        protected void DockLayout_SaveDockLayout(object sender, DockLayoutEventArgs e)
        {
            MyDockStates = SaveState();
        }
    }
}