﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.OperatorMachineMonitored
{
    public partial class MachineBox : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            //stacco il dock dalla dockzone e lo chiudo
            RadDock dock = this.Parent.NamingContainer as RadDock;
            dock.Undock();
            dock.Closed = true;
            dock.Dispose();
        }
    }
}