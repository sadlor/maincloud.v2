﻿(function (global, undefined) {
    var demo = {};

    function addDock() {
        $find(demo.ajaxID).ajaxRequestWithTarget(demo.ajaxServerID, "AddDock");
    }

    function serverID(name, id) {
        demo[name] = id;
    }

    function serverIDs(obj) {
        for (var name in obj) {
            serverID(name, obj[name]);
        }
    }

    global.serverIDs = serverIDs;
    global.addDock = addDock;
})(window);