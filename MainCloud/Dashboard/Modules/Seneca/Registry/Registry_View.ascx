﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Registry_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.Registry.Registry_View" %>
<%@ Register src="~/Controls/ImageManager/ImageManager.ascx" tagname="ImageManager" tagprefix="mcf" %>

<%--<telerik:RadButton runat="server" ID="btnArticle" Text="Articoli" OnClick="btnArticle_Click" />
<br />
<telerik:RadButton runat="server" ID="btnOrder" Text="Commesse" OnClick="btnOrder_Click" />
<br />
<telerik:RadButton runat="server" ID="btnJob" Text="Jobs" OnClick="btnJob_Click" />
<br />
<telerik:RadButton runat="server" ID="btnCause" Text="Causali" OnClick="btnCause_Click" />--%>

<%--<br />--%>

<%--<asp:Panel runat="server" ID="pnlArticle" Visible="false">
    <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="articleTable" AutoGenerateColumns="false" DataSourceID="edsArticle">
        <MasterTableView FilterExpression="" Caption="Articoli">
            <Columns>
                <telerik:GridBoundColumn DataField="Code" HeaderText="<%$ Resources:Mes,Code %>">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="TechnicalNotes" HeaderText="Note tecniche">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="QualityNotes" HeaderText="Note qualità">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="PathImg" HeaderText="Image" UniqueName="Image">
                    <ItemTemplate>
                        <asp:Image ImageUrl='<%# string.Format("~/{0}", Eval("PathImg"))%>' runat="server" Width="50%" />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
</asp:Panel>--%>

<%--<br />--%>

<%--<asp:Panel runat="server" ID="pnlOrder" Visible="false">
    <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="orderTable" AutoGenerateColumns="false" DataSourceID="edsOrder">
        <MasterTableView FilterExpression="" Caption="Commesse">
            <Columns>
                <telerik:GridBoundColumn DataField="OrderCode" HeaderText="<%$ Resources:Mes,Code %>">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridDropDownColumn DataField="IdArticle" HeaderText="Articolo" UniqueName="ddlArticle" 
                    ListTextField="Description" ListValueField="Id" DataSourceID="edsArticle" DropDownControlType="RadComboBox"
                    ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                </telerik:GridDropDownColumn>
                <telerik:GridDateTimeColumn DataField="RequestDate" HeaderText="Data richiesta" DataFormatString="{0:dd/MM/yyyy}">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn DataField="PlannedStartDate" HeaderText="Data inizio prevista" DataFormatString="{0:dd/MM/yyyy}">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn DataField="PlannedEndDate" HeaderText="Data fine prevista" DataFormatString="{0:dd/MM/yyyy}">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridDateTimeColumn>
                <telerik:GridNumericColumn DataField="QtyOrdered" DataType="System.Decimal" HeaderText="Qta ordinata">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridNumericColumn>
                <telerik:GridBoundColumn DataField="StatusCode" HeaderText="Codice stato">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="StatusDescription" HeaderText="Descrizione stato">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
</asp:Panel>--%>

<%--<br />--%>

<%--<asp:Panel runat="server" ID="pnlJob" Visible="false">
    <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="jobTable" AutoGenerateColumns="false" AllowPaging="true" DataSourceID="edsJob"
         LocalizationPath="~/App_GlobalResources/" Culture="en-US">
        <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
        <MasterTableView FilterExpression="" Caption="Jobs">
            <Columns>
                <telerik:GridBoundColumn DataField="Code" HeaderText="Code">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Description" HeaderText="Description">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridDropDownColumn DataField="ArticleId" HeaderText="Articolo" UniqueName="ddlArticle" 
                    ListTextField="Code" ListValueField="Id" DataSourceID="edsArticle" DropDownControlType="RadComboBox"
                    ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                </telerik:GridDropDownColumn>
                <telerik:GridDropDownColumn DataField="OrderId" HeaderText="Ordine" UniqueName="ddlOrder" 
                    ListTextField="Description" ListValueField="Id" DataSourceID="edsOrder" DropDownControlType="RadComboBox"
                    ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                </telerik:GridDropDownColumn>
                <telerik:GridDropDownColumn DataField="AssetId" HeaderText="Macchina" UniqueName="ddlAsset" 
                    ListTextField="Description" ListValueField="Id" DataSourceID="edsAsset" DropDownControlType="RadComboBox"
                    ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                </telerik:GridDropDownColumn>
                <telerik:GridDropDownColumn DataField="EquipmentId" HeaderText="Equipment" UniqueName="ddlEquipment" 
                    ListTextField="Description" ListValueField="Id" DataSourceID="edsEquipment" DropDownControlType="RadComboBox"
                    ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                </telerik:GridDropDownColumn>
                <telerik:GridDropDownColumn DataField="MoldId" HeaderText="Stampo" UniqueName="ddlMold" 
                    ListTextField="Description" ListValueField="Id" DataSourceID="edsMold" DropDownControlType="RadComboBox"
                    ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                </telerik:GridDropDownColumn>
                <telerik:GridDateTimeColumn DataField="StartDate" HeaderText="Start" DataFormatString="{0:dd/MM/yyyy}">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="End" DataFormatString="{0:dd/MM/yyyy}">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridDateTimeColumn>
                <telerik:GridNumericColumn DataField="StandardRate" DataType="System.Decimal" HeaderText="Cadenza oraria [pz/h]">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridNumericColumn>
                <telerik:GridNumericColumn DataField="CycleTime" DataType="System.Decimal" HeaderText="Tempo Assegnato Unitario [s]">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridNumericColumn>
                <telerik:GridNumericColumn DataField="QtyOrdered" DataType="System.Decimal" HeaderText="Qta Ordinata">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridNumericColumn>
                <telerik:GridBoundColumn DataField="DisegnoFase" HeaderText="Disegno fase">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SetupPlan" HeaderText="SetupPlan">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SafetyNotes" HeaderText="Note sicurezza">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EnvironmentNotes" HeaderText="Note ambiente">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
</asp:Panel>--%>

<%--<br />--%>

<asp:UpdatePanel runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>
        <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="gridJob" AutoGenerateColumns="false" AllowSorting="true" AllowFilteringByColumn="true" AllowPaging="true"
            OnItemCommand="gridJob_ItemCommand" OnItemDataBound="gridJob_ItemDataBound" OnNeedDataSource="gridJob_NeedDataSource"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US" EnableLinqExpressions="false">
            <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView Caption="Job" DataKeyNames="Id">
                <Columns>
                    <telerik:GridBoundColumn DataField="Code" HeaderText="Codice" AllowFiltering="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Descrizione" DataType="System.String" AllowFiltering="true"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Order.CustomerOrder.OrderCode" HeaderText="Commessa" AllowFiltering="true"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Order.Article.Code" HeaderText="Articolo" DataType="System.String" AllowFiltering="true"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Order.OrderCode" HeaderText="OP" AllowFiltering="true"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Order.StatusDescription" HeaderText="Stato Ordine" AllowFiltering="true"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridDropDownColumn DataField="AssetId" HeaderText="Macchina" UniqueName="ddlAsset"
                        ListTextField="Description" ListValueField="Id" DataSourceID="edsAsset" DropDownControlType="RadComboBox"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500" DataType="System.String">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDropDownColumn>
                    <telerik:GridBoundColumn DataField="AssetId" HeaderText="Description" SortExpression="Description"
                        UniqueName="AssetName" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridDropDownColumn DataField="MoldId" HeaderText="Stampo" UniqueName="ddlMold"
                        ListTextField="Description" ListValueField="Id" DataSourceID="edsMold" DropDownControlType="RadComboBox"
                        ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="" Display="false">
                    </telerik:GridDropDownColumn>
                    <telerik:GridDateTimeColumn DataField="StartDate" HeaderText="Inizio" DataFormatString="{0:dd/MM/yyyy}"
                        AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="GreaterThanOrEqualTo" ShowFilterIcon="false" FilterDelay="500">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="Fine" DataFormatString="{0:dd/MM/yyyy}"
                        AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="LessThanOrEqualTo" ShowFilterIcon="false" FilterDelay="500">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridNumericColumn DataField="StandardRate" DataType="System.Decimal" HeaderText="Cadenza oraria [pz/h]" AllowFiltering="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridNumericColumn>
                    <telerik:GridNumericColumn DataField="CycleTime" DataType="System.Decimal" HeaderText="Tempo Assegnato Unitario [s]" AllowFiltering="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridNumericColumn>
                    <telerik:GridNumericColumn DataField="QtyOrdered" DataType="System.Decimal" HeaderText="Qta Ordinata" AllowFiltering="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridNumericColumn>
                    <telerik:GridNumericColumn DataField="CavityMoldNum" HeaderText="N Impronte" AllowFiltering="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridNumericColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </ContentTemplate>
</asp:UpdatePanel>
<br />
<asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnlCause" Visible="true">
            <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="causeTable" AutoGenerateColumns="false"
                OnNeedDataSource="causeTable_NeedDataSource" OnDetailTableDataBind="causeTable_DetailTableDataBind"
                LocalizationPath="~/App_GlobalResources/" Culture="en-US">
                <MasterTableView FilterExpression="" Caption="Causali" DataKeyNames="Id"
                    HierarchyDefaultExpanded="false" RetainExpandStateOnRebind="true" Name="CauseType">
                     <DetailTables>
                        <telerik:GridTableView DataKeyNames="Id" Name="Cause">
                            <Columns>
                                <telerik:GridBoundColumn DataField="Code" HeaderText="<%$ Resources:Mes,Code %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ExternalCode" HeaderText="Codice aziendale">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </telerik:GridTableView>
                    </DetailTables>
                    <Columns>
                        <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </asp:Panel>
    </ContentTemplate>
</asp:updatepanel>

<ef:EntityDataSource ID="edsAsset" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<%--<ef:EntityDataSource ID="edsJob" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Jobs"
    OrderBy="it.Description" Include="Order, Order.Article" />--%>
<%--<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsArticle" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Articles"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsOrder" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Orders"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsOperator" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Operators"
    OrderBy="it.UserName" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsEquipment" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Equipments"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />--%>