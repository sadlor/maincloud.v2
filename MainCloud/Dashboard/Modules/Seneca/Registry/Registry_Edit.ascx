﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Registry_Edit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.Registry.Registry_Edit" %>
<%@ Register src="~/Controls/ImageManager/ImageManager.ascx" tagname="ImageManager" tagprefix="mcf" %>

<telerik:RadGrid runat="server" RenderMode="Lightweight" ID="articleTable" AutoGenerateColumns="false" AllowPaging="true" AllowFilteringByColumn="true"
    OnInsertCommand="articleTable_InsertCommand" OnUpdateCommand="articleTable_UpdateCommand" OnDeleteCommand="articleTable_DeleteCommand" OnNeedDataSource="articleTable_NeedDataSource"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <GroupingSettings CaseSensitive="false" />
    <MasterTableView FilterExpression="" Caption="Articoli" DataKeyNames="Id" CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
        <EditFormSettings InsertCaption="Inserisci articolo">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="Code" HeaderText="<%$ Resources:Mes,Code %>"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="TechnicalNotes" HeaderText="Note tecniche" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="QualityNotes" HeaderText="Note qualità" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridTemplateColumn DataField="Id" HeaderText="Stampi" UniqueName="Molds">
                <EditItemTemplate>
                    <telerik:RadListBox RenderMode="Lightweight" runat="server" ID="listBox" CheckBoxes="true"
                        DataTextField="Name" DataValueField="Id">
                    </telerik:RadListBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <telerik:RadListBox RenderMode="Lightweight" runat="server" ID="listBox" CheckBoxes="true"
                        DataTextField="Name" DataValueField="Id">
                    </telerik:RadListBox>
                </ItemTemplate>
            </telerik:GridTemplateColumn>--%>
            <telerik:GridTemplateColumn DataField="PathImg" HeaderText="Image" UniqueName="Image" AllowFiltering="false">
                <ItemTemplate>
                    <asp:Image ImageUrl='<%# string.Format("~/{0}", Eval("PathImg"))%>' runat="server" Width="50%" />
                </ItemTemplate>
                <EditItemTemplate>
                    <mcf:ImageManager runat="server" ID="ImageManager" Path='<%# string.Format("/Uploads/{0}/Widgets/Shared", MainCloudFramework.Web.Helpers.MultiTenantsHelper.ApplicationId)%>' />
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<br />

<%--<telerik:RadGrid runat="server" ID="gridMoldConnect" DataSourceID="edsMold">
    <MasterTableView DataKeyNames="Id" Caption="Stampi">
        <Columns>
            <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
            </telerik:GridClientSelectColumn>
            <telerik:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:Mes,Mold %>">
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <Selecting AllowRowSelect="True"></Selecting>
    </ClientSettings>
</telerik:RadGrid>
<telerik:RadGrid runat="server" ID="gridArticleConnect" DataSourceID="edsArticle">
    <MasterTableView DataKeyNames="Id" Caption="Articoli">
        <Columns>
            <telerik:GridBoundColumn DataField="Code" HeaderText="<%$ Resources:Mes,Article %>">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Article %>">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>--%>
<br />

<telerik:RadGrid runat="server" RenderMode="Lightweight" ID="gridCustomerOrder" AutoGenerateColumns="false" AllowPaging="true" AllowFilteringByColumn="true"
    OnInsertCommand="gridCustomerOrder_InsertCommand" OnUpdateCommand="gridCustomerOrder_UpdateCommand" OnDeleteCommand="gridCustomerOrder_DeleteCommand" OnNeedDataSource="gridCustomerOrder_NeedDataSource"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <GroupingSettings CaseSensitive="false" />
    <MasterTableView FilterExpression="" Caption="Commesse" DataKeyNames="Id" CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
        <EditFormSettings InsertCaption="Inserisci commessa">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="OrderCode" HeaderText="<%$ Resources:Mes,Code %>"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="Articolo" UniqueName="Article" DataField="Article.Description"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <EditItemTemplate>
                    <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="articleFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" Width="100%"
                        TextSettings-SelectionMode="Single" DataSourceID="edsArticle" DataTextField="Code" DataValueField="Id" InputType="Text" Filter="Contains">
                    </telerik:RadAutoCompleteBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ErrorMessage="* Field required" ControlToValidate="articleFilter" runat="server" />
                </EditItemTemplate>
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Article.Description")%>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridDateTimeColumn DataField="RequestDate" HeaderText="Data richiesta" DataFormatString="{0:dd/MM/yyyy}" AllowFiltering="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridNumericColumn DataField="QtyOrdered" DataType="System.Decimal" HeaderText="Qta ordinata" MinValue="1" DefaultInsertValue="1" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridNumericColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
</telerik:RadGrid>
<br />

<telerik:RadGrid runat="server" RenderMode="Lightweight" ID="orderTable" AutoGenerateColumns="false" AllowPaging="true" AllowFilteringByColumn="true"
    OnInsertCommand="orderTable_InsertCommand" OnUpdateCommand="orderTable_UpdateCommand" OnDeleteCommand="orderTable_DeleteCommand" OnItemDataBound="orderTable_ItemDataBound" OnNeedDataSource="orderTable_NeedDataSource"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <GroupingSettings CaseSensitive="false" />
    <MasterTableView FilterExpression="" Caption="Ordini di produzione" DataKeyNames="Id" CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
        <EditFormSettings InsertCaption="Inserisci ordine di produzione">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="OrderCode" HeaderText="<%$ Resources:Mes,Code %>"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="Commessa" UniqueName="CustomerOrder" DataField="CustomerOrder.OrderCode"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <EditItemTemplate>
                    <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="customerOrderFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" Width="100%"
                        TextSettings-SelectionMode="Single" DataSourceID="edsCustomerOrder" DataTextField="OrderCode" DataValueField="Id" InputType="Text" Filter="Contains">
                    </telerik:RadAutoCompleteBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ErrorMessage="* Field required" ControlToValidate="customerOrderFilter" runat="server" />
                </EditItemTemplate>
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "CustomerOrder.OrderCode")%>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Articolo" UniqueName="Article" DataField="Article.Code"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <EditItemTemplate>
                    <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="articleFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" Width="100%"
                        TextSettings-SelectionMode="Single" DataSourceID="edsArticle" DataTextField="Code" DataValueField="Id" InputType="Text" Filter="Contains">
                    </telerik:RadAutoCompleteBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ErrorMessage="* Field required" ControlToValidate="articleFilter" runat="server" />
                </EditItemTemplate>
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Article.Code")%>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <%--<telerik:GridDropDownColumn DataField="IdArticle" HeaderText="Articolo" UniqueName="ddlArticle" 
                ListTextField="Description" ListValueField="Id" DataSourceID="edsArticle" DropDownControlType="RadComboBox"
                ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
            </telerik:GridDropDownColumn>--%>
            <telerik:GridDateTimeColumn DataField="RequestDate" HeaderText="Data richiesta" DataFormatString="{0:dd/MM/yyyy}" AllowFiltering="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="PlannedStartDate" HeaderText="Data inizio prevista" DataFormatString="{0:dd/MM/yyyy}" AllowFiltering="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="PlannedEndDate" HeaderText="Data fine prevista" DataFormatString="{0:dd/MM/yyyy}" AllowFiltering="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridNumericColumn DataField="QtyOrdered" DataType="System.Decimal" MinValue="1" DefaultInsertValue="1" HeaderText="Qta ordinata" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
            </telerik:GridNumericColumn>
            <telerik:GridBoundColumn DataField="StatusCode" HeaderText="Codice stato" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="StatusDescription" HeaderText="Descrizione stato" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<br />

<telerik:RadGrid runat="server" RenderMode="Lightweight" ID="jobTable" AutoGenerateColumns="false" AllowPaging="true" AllowSorting="true" AllowFilteringByColumn="true"
    OnNeedDataSource="jobTable_NeedDataSource" OnInsertCommand="jobTable_InsertCommand" OnUpdateCommand="jobTable_UpdateCommand" OnDeleteCommand="jobTable_DeleteCommand" OnItemDataBound="jobTable_ItemDataBound"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <GroupingSettings CaseSensitive="false" />
    <MasterTableView Caption="Fasi" DataKeyNames="Id"
        CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" EditMode="PopUp">
        <EditFormSettings InsertCaption="Inserisci fase">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridBoundColumn DataField="Code" HeaderText="Code" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Description" HeaderText="Description" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridBoundColumn DataField="Order.CustomerOrder.OrderCode" HeaderText="Commessa"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>--%>
            <telerik:GridBoundColumn DataField="Order.Article.Code" HeaderText="Articolo" ReadOnly="true"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridDropDownColumn DataField="ArticleId" HeaderText="Articolo" UniqueName="ddlArticle" 
                ListTextField="Description" ListValueField="Id" DataSourceID="edsArticle" DropDownControlType="RadComboBox"
                ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn DataField="OrderId" HeaderText="Commessa" UniqueName="ddlOrder"
                ListTextField="OrderCode" ListValueField="Id" DataSourceID="edsOrder" DropDownControlType="RadComboBox"
                ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
            </telerik:GridDropDownColumn>--%>
            <telerik:GridTemplateColumn HeaderText="Ordine" UniqueName="Order" DataField="Order.OrderCode"
                AllowFiltering="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
                <HeaderStyle HorizontalAlign="Center" />
                <EditItemTemplate>
                    <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="orderFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" Width="100%"
                        TextSettings-SelectionMode="Single" DataSourceID="edsOrder" DataTextField="OrderCode" DataValueField="Id" InputType="Text" Filter="Contains">
                    </telerik:RadAutoCompleteBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ErrorMessage="* Field required" ControlToValidate="orderFilter" runat="server" />
                </EditItemTemplate>
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Order.OrderCode")%>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridNumericColumn DataField="QtyOrdered" DataType="System.Decimal" HeaderText="Qta Ordinata" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridNumericColumn>
            <telerik:GridDropDownColumn DataField="AssetId" HeaderText="Macchina" UniqueName="ddlAsset"
                ListTextField="Description" ListValueField="Id" DataSourceID="edsAsset" DropDownControlType="RadComboBox"
                ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue=""
                AllowFiltering="false" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterDelay="500">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn DataField="EquipmentId" HeaderText="Equipment" UniqueName="ddlEquipment" AllowFiltering="false" 
                ListTextField="Description" ListValueField="Id" DataSourceID="edsEquipment" DropDownControlType="RadComboBox"
                ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn DataField="MoldId" HeaderText="Stampo" UniqueName="ddlMold" AllowFiltering="false"
                ListTextField="Name" ListValueField="Id" DataSourceID="edsMold" DropDownControlType="RadComboBox"
                ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
            </telerik:GridDropDownColumn>
            <telerik:GridDateTimeColumn DataField="StartDate" HeaderText="Start" DataFormatString="{0:dd/MM/yyyy}" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="End" DataFormatString="{0:dd/MM/yyyy}" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridNumericColumn DataField="StandardRate" DataType="System.Decimal" HeaderText="Cadenza oraria [pz/h]" AllowFiltering="false">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridNumericColumn>
            <telerik:GridNumericColumn DataField="CycleTime" DataType="System.Decimal" HeaderText="Tempo Assegnato Unitario [s]" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridNumericColumn>
            <telerik:GridNumericColumn DataField="CavityMoldNum" DataType="System.Int32" HeaderText="Impronte" MinValue="1" DecimalDigits="0" DefaultInsertValue="1" DataFormatString="{0:f0}" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridNumericColumn>
            <telerik:GridDropDownColumn DataField="ControlPlanId" HeaderText="Piano di controllo" UniqueName="ddlControlPlan" AllowFiltering="false"
                ListTextField="Description" ListValueField="Id" DataSourceID="edsCpd" DropDownControlType="RadComboBox"
                ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="DisegnoFase" HeaderText="Disegno fase" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SetupPlan" HeaderText="SetupPlan" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SafetyNotes" HeaderText="Note sicurezza" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="EnvironmentNotes" HeaderText="Note ambiente" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<br />

<telerik:RadGrid runat="server" RenderMode="Lightweight" ID="causeTable" AutoGenerateColumns="false"
    OnNeedDataSource="causeTable_NeedDataSource" OnDetailTableDataBind="causeTable_DetailTableDataBind"
    OnInsertCommand="causeTable_InsertCommand" OnUpdateCommand="causeTable_UpdateCommand" OnDeleteCommand="causeTable_DeleteCommand"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <MasterTableView FilterExpression="" Caption="Causali" DataKeyNames="Id" CommandItemDisplay="Top"
        HierarchyDefaultExpanded="false" RetainExpandStateOnRebind="true" Name="CauseType" EditMode="PopUp">
        <EditFormSettings>
            <PopUpSettings Modal="true" />
        </EditFormSettings>
         <DetailTables>
            <telerik:GridTableView DataKeyNames="Id" Name="Cause" CommandItemDisplay="Top" EditMode="PopUp">
                <EditFormSettings>
                    <PopUpSettings Modal="true" />
                </EditFormSettings>
                <Columns>
                    <telerik:GridEditCommandColumn />
                    <telerik:GridBoundColumn DataField="Code" HeaderText="<%$ Resources:Mes,Code %>" ReadOnly="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ExternalCode" HeaderText="Codice aziendale">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
                </Columns>
            </telerik:GridTableView>
        </DetailTables>
        <Columns>
            <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                <ColumnValidationSettings EnableModelErrorMessageValidation="true" EnableRequiredFieldValidation="true">
                    <RequiredFieldValidator ForeColor="Red" ErrorMessage=" * Field required"></RequiredFieldValidator>
                </ColumnValidationSettings>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<mcf:PopUpDialog ID="dlgWidgetConfig" runat="server" Title="Avviso">
    <Body>
        <asp:label id="messaggio" runat="server"/>
        <%--<telerik:RadButton runat="server" Text="Ok"></telerik:RadButton>--%>
    </Body>
</mcf:PopUpDialog>

<ef:EntityDataSource ID="edsAsset" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    OrderBy="it.Description" AutoGenerateWhereClause="true"></ef:EntityDataSource>
<ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
    OrderBy="it.Name" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsJob" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Jobs"
    OrderBy="it.Description" AutoGenerateWhereClause="true" Include="Order" />
<ef:EntityDataSource ID="edsArticle" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Articles"
    OrderBy="it.Code" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsOrder" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Orders"
    OrderBy="it.OrderCode" AutoGenerateWhereClause="true" Include="CustomerOrder" />
<ef:EntityDataSource ID="edsCustomerOrder" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="CustomerOrders"
    OrderBy="it.OrderCode" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsOperator" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Operators"
    OrderBy="it.UserName" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsEquipment" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Equipments"
    OrderBy="it.Description" AutoGenerateWhereClause="true" />
<ef:EntityDataSource ID="edsCpd" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="ControlPlans" />