﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.Registry
{
    public partial class Registry_Edit : WidgetControl<object>
    {
        public Registry_Edit() : base(typeof(Registry_Edit)) { }

        protected void Page_Load(object sender, EventArgs e)
        {
            edsArticle.WhereParameters.Clear();
            edsArticle.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsAsset.WhereParameters.Clear();
            edsAsset.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsEquipment.WhereParameters.Clear();
            edsEquipment.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsJob.WhereParameters.Clear();
            edsJob.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsOperator.WhereParameters.Clear();
            edsOperator.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsOrder.WhereParameters.Clear();
            edsOrder.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        #region InsertCommand
        protected void articleTable_InsertCommand(object sender, GridCommandEventArgs e)
        {
            ArticleRepository artRep = new ArticleRepository();

            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var article = new Article();
            article.ApplicationId = MultiTenantsHelper.ApplicationId;
            article.Code = (string)values["Code"];
            article.Description = (string)values["Description"];
            article.QualityNotes = (string)values["QualityNotes"];
            article.TechnicalNotes = (string)values["TechnicalNotes"];

            var img = editableItem["Image"].FindControl("ImageManager");
            string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
            if (!string.IsNullOrEmpty(pathImg))
            {
                article.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
            }

            artRep.Insert(article);
            artRep.SaveChanges();
        }

        protected void gridCustomerOrder_InsertCommand(object sender, GridCommandEventArgs e)
        {
            CustomerOrderRepository ordRep = new CustomerOrderRepository();

            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var order = new CustomerOrder();
            order.ApplicationId = MultiTenantsHelper.ApplicationId;
            order.OrderCode = (string)values["OrderCode"];
            order.Description = (string)values["Description"];

            var af = editableItem["Article"].FindControl("articleFilter") as RadAutoCompleteBox;
            if (af.Entries.Count > 0 && !string.IsNullOrEmpty(af.Entries[0].Value))
            {
                order.IdArticle = Convert.ToInt32(af.Entries[0].Value);
            }

            order.RequestDate = Convert.ToDateTime(values["RequestDate"]);
            order.QtyOrdered = Convert.ToDecimal(values["QtyOrdered"]);

            ordRep.Insert(order);
            ordRep.SaveChanges();
        }

        protected void orderTable_InsertCommand(object sender, GridCommandEventArgs e)
        {
            OrderRepository ordRep = new OrderRepository();

            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var order = new Order();
            order.ApplicationId = MultiTenantsHelper.ApplicationId;
            order.OrderCode = (string)values["OrderCode"];
            order.Description = (string)values["Description"];

            var cof = editableItem["CustomerOrder"].FindControl("customerOrderFilter") as RadAutoCompleteBox;            
            if (cof.Entries.Count > 0 && !string.IsNullOrEmpty(cof.Entries[0].Value))
            {
                order.CustomerOrderId = Convert.ToInt32(cof.Entries[0].Value);
            }
            var af = editableItem["Article"].FindControl("articleFilter") as RadAutoCompleteBox;
            if (af.Entries.Count > 0 && !string.IsNullOrEmpty(af.Entries[0].Value))
            {
                order.IdArticle = Convert.ToInt32(af.Entries[0].Value);
            }

            //order.IdArticle = Convert.ToInt32(values["IdArticle"]);
            order.PlannedStartDate = Convert.ToDateTime(values["PlannedStartDate"]);
            order.PlannedEndDate = Convert.ToDateTime(values["PlannedEndDate"]);
            order.RequestDate = Convert.ToDateTime(values["RequestDate"]);
            if (string.IsNullOrEmpty((string)values["StatusCode"]))
            {
                order.StatusCode = ((int)MesEnum.OrderStatusCode.Esecutiva).ToString();
            }
            else
            {
                order.StatusCode = (string)values["StatusCode"];
            }
            if (string.IsNullOrEmpty((string)values["StatusDescription"]))
            {
                order.StatusDescription = MesEnum.OrderStatusCode.Esecutiva.ToString();
            }
            else
            {
                order.StatusDescription = (string)values["StatusDescription"];
            }
            order.QtyOrdered = Convert.ToDecimal(values["QtyOrdered"]);

            ordRep.Insert(order);
            ordRep.SaveChanges();
        }

        protected void jobTable_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            JobRepository jobRep = new JobRepository();

            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var job = new Job();
            job.ApplicationId = MultiTenantsHelper.ApplicationId;
            job.Code = (string)values["Code"];
            job.Description = (string)values["Description"];
            job.EnvironmentNotes = (string)values["EnvironmentNotes"];
            job.SafetyNotes = (string)values["SafetyNotes"];
            ////job.ArticleId = Convert.ToInt32(values["ArticleId"]);
            //if (values["OrderId"] != null)
            //{
            //    job.OrderId = Convert.ToInt32(values["OrderId"]);
            //}
            //else
            //{
            //    job.OrderId = null;
            //}
            var cof = editableItem["Order"].FindControl("orderFilter") as RadAutoCompleteBox;
            if (cof.Entries.Count > 0 && !string.IsNullOrEmpty(cof.Entries[0].Value))
            {
                job.OrderId = Convert.ToInt32(cof.Entries[0].Value);
            }
            job.AssetId = (string)values["AssetId"];
            job.EquipmentId = (string)values["EquipmentId"];
            job.MoldId = (string)values["MoldId"];
            job.QtyOrdered = Convert.ToInt32(values["QtyOrdered"]);
            if (!string.IsNullOrEmpty((string)values["StartDate"]))
            {
                job.StartDate = Convert.ToDateTime(values["StartDate"]);
            }
            else
            {
                job.StartDate = DateTime.Now;
            }
            if (!string.IsNullOrEmpty((string)values["EndDate"]))
            {
                job.EndDate = Convert.ToDateTime(values["EndDate"]);
            }
            job.StandardRate = Convert.ToDecimal(values["StandardRate"]);
            if (!string.IsNullOrEmpty((string)values["CycleTime"]))
            {
                job.CycleTime = Convert.ToDecimal(values["CycleTime"]);
            }
            else
            {
                job.CycleTime = 3600 / job.StandardRate;
            }
            job.CavityMoldNum = Convert.ToInt32(values["CavityMoldNum"]);

            //job.SetupPlan = (string)values["SetupPlan"];
            //job.DisegnoFase = (string)values["DisegnoFase"];

            //var img = editableItem["Image"].FindControl("ImageManager");
            //string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
            //if (!string.IsNullOrEmpty(pathImg))
            //{
            //    plant.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
            //}

            jobRep.Insert(job);
            jobRep.SaveChanges();
        }

        protected void moldTable_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            MoldRepository moldRep = new MoldRepository();

            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            var mold = new Mold();
            mold.ApplicationId = MultiTenantsHelper.ApplicationId;
            mold.Name = (string)values["Name"];
            mold.Code = (string)values["Code"];
            mold.Description = (string)values["Description"];
            mold.DtInstallation = Convert.ToDateTime(values["DtInstallation"]);
            mold.Brand = (string)values["Brand"];
            mold.IdNumber = (string)values["IdNumber"];
            mold.Model = (string)values["Model"];
            mold.CavityMoldNum = (int)values["CavityMoldNum"];
            if (!string.IsNullOrEmpty((string)values["DtEndGuarantee"]))
            {
                mold.DtEndGuarantee = Convert.ToDateTime(values["DtEndGuarantee"]);
            }
            //if (values["Frequency"] != null)
            //{
            //    mold.Frequency = (int)values["Frequency"];
            //}

            //var img = editableItem["Image"].FindControl("ImageManager");
            //string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
            //if (!string.IsNullOrEmpty(pathImg))
            //{
            //    mold.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
            //}

            moldRep.Insert(mold);
            moldRep.SaveChanges();
        }

        protected void causeTable_InsertCommand(object sender, GridCommandEventArgs e)
        {
            CauseTypeRepository causeTRep = new CauseTypeRepository();
            switch (e.Item.OwnerTableView.Name)
            {
                case "CauseType":
                    var editableItem = ((GridEditableItem)e.Item);
                    Hashtable values = new Hashtable();
                    editableItem.ExtractValues(values);

                    var causeType = new CauseType();
                    causeType.ApplicationId = MultiTenantsHelper.ApplicationId;
                    causeType.Description = (string)values["Description"];
                    causeTRep.Insert(causeType);
                    causeTRep.SaveChanges();
                    break;
                case "Cause":
                    GridDataItem parentItem = (GridDataItem)e.Item.OwnerTableView.ParentItem;
                    string parentId = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["Id"].ToString();

                    var editableItem2 = ((GridEditableItem)e.Item);
                    Hashtable values2 = new Hashtable();
                    editableItem2.ExtractValues(values2);

                    CauseRepository causeRep = new CauseRepository();
                    Cause cause;
                    string lastCode = causeRep.ReadAll(x => x.CauseTypeId == parentId).Max(x => x.Code);
                    switch (causeTRep.FindByID(parentId).Description)
                    {
                        case "Attrezzaggio":
                            cause = new Cause();
                            cause.ApplicationId = MultiTenantsHelper.ApplicationId;
                            cause.CauseTypeId = parentId;
                            if (string.IsNullOrEmpty(lastCode))
                            {
                                cause.Code = 100.ToString();
                            }
                            else
                            {
                                cause.Code = (Convert.ToInt32(lastCode) + 1).ToString();
                            }
                            cause.ExternalCode = (string)values2["ExternalCode"];
                            cause.Description = (string)values2["Description"];
                            causeRep.Insert(cause);
                            causeRep.SaveChanges();
                            break;
                        case "Produzione":
                            cause = new Cause();
                            cause.ApplicationId = MultiTenantsHelper.ApplicationId;
                            cause.CauseTypeId = parentId;
                            if (string.IsNullOrEmpty(lastCode))
                            {
                                cause.Code = 200.ToString();
                            }
                            else
                            {
                                cause.Code = (Convert.ToInt32(lastCode) + 1).ToString();
                            }
                            cause.ExternalCode = (string)values2["ExternalCode"];
                            cause.Description = (string)values2["Description"];
                            causeRep.Insert(cause);
                            causeRep.SaveChanges();
                            break;
                        case "Manutenzione":
                            cause = new Cause();
                            cause.ApplicationId = MultiTenantsHelper.ApplicationId;
                            cause.CauseTypeId = parentId;
                            if (string.IsNullOrEmpty(lastCode))
                            {
                                cause.Code = 300.ToString();
                            }
                            else
                            {
                                cause.Code = (Convert.ToInt32(lastCode) + 1).ToString();
                            }
                            cause.ExternalCode = (string)values2["ExternalCode"];
                            cause.Description = (string)values2["Description"];
                            causeRep.Insert(cause);
                            causeRep.SaveChanges();
                            break;
                        case "Fermi":
                            cause = new Cause();
                            cause.ApplicationId = MultiTenantsHelper.ApplicationId;
                            cause.CauseTypeId = parentId;
                            if (string.IsNullOrEmpty(lastCode))
                            {
                                cause.Code = 400.ToString();
                            }
                            else
                            {
                                cause.Code = (Convert.ToInt32(lastCode) + 1).ToString();
                            }
                            cause.ExternalCode = (string)values2["ExternalCode"];
                            cause.Description = (string)values2["Description"];
                            causeRep.Insert(cause);
                            causeRep.SaveChanges();
                            break;
                        case "Scarti":
                            cause = new Cause();
                            cause.ApplicationId = MultiTenantsHelper.ApplicationId;
                            cause.CauseTypeId = parentId;
                            if (string.IsNullOrEmpty(lastCode))
                            {
                                cause.Code = 500.ToString();
                            }
                            else
                            {
                                cause.Code = (Convert.ToInt32(lastCode) + 1).ToString();
                            }
                            cause.ExternalCode = (string)values2["ExternalCode"];
                            cause.Description = (string)values2["Description"];
                            causeRep.Insert(cause);
                            causeRep.SaveChanges();
                            break;
                        default:
                            cause = new Cause();
                            cause.ApplicationId = MultiTenantsHelper.ApplicationId;
                            cause.CauseTypeId = parentId;
                            lastCode = causeRep.ReadAll(x => string.Compare(x.Code, "600") >= 0).Max(x => x.Code);
                            if (string.IsNullOrEmpty(lastCode))
                            {
                                cause.Code = 600.ToString();
                            }
                            else
                            {
                                cause.Code = (Convert.ToInt32(lastCode) + 1).ToString();
                            }
                            cause.ExternalCode = (string)values2["ExternalCode"];
                            cause.Description = (string)values2["Description"];
                            causeRep.Insert(cause);
                            causeRep.SaveChanges();
                            break;
                    }
                    break;
            }
        }
        #endregion

        #region UpdateCommand
        protected void articleTable_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            ArticleRepository artRep = new ArticleRepository();
            var editableItem = ((GridEditableItem)e.Item);
            var artId = Convert.ToInt32(editableItem.GetDataKeyValue("Id"));
            var article = artRep.FindByID(artId);
            if (article != null)
            {
                editableItem.UpdateValues(article);

                var img = editableItem["Image"].FindControl("ImageManager");
                string pathImg = (img.FindControl("ImageSelectedPath") as HiddenField).Value;
                if (!string.IsNullOrEmpty(pathImg))
                {
                    article.PathImg = pathImg.Remove(0, pathImg.IndexOf("/Uploads/"));
                }

                artRep.Update(article);
                artRep.SaveChanges();
            }
        }

        protected void gridCustomerOrder_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            CustomerOrderRepository ordRep = new CustomerOrderRepository();
            var editableItem = ((GridEditableItem)e.Item);
            var ordId = (int)editableItem.GetDataKeyValue("Id");
            var order = ordRep.FindByID(ordId);

            if (order != null)
            {
                editableItem.UpdateValues(order);

                //if (string.IsNullOrEmpty(order.IdArticle))
                //{
                //    order.IdArticle = null;
                //}

                ordRep.Update(order);
                ordRep.SaveChanges();
            }
        }

        protected void orderTable_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            OrderRepository ordRep = new OrderRepository();
            var editableItem = ((GridEditableItem)e.Item);
            var ordId = (int)editableItem.GetDataKeyValue("Id");
            var order = ordRep.FindByID(ordId);

            if (order != null)
            {
                editableItem.UpdateValues(order);

                //var cof = editableItem["CustomerOrder"].FindControl("customerOrderFilter") as RadAutoCompleteBox;
                //if (cof.Entries.Count > 0 && !string.IsNullOrEmpty(cof.Entries[0].Value))
                //{
                //    order.CustomerOrderId = Convert.ToInt32(cof.Entries[0].Value);
                //}
                //var af = editableItem["Article"].FindControl("articleFilter") as RadAutoCompleteBox;
                //if (af.Entries.Count > 0 && !string.IsNullOrEmpty(af.Entries[0].Value))
                //{
                //    order.IdArticle = Convert.ToInt32(af.Entries[0].Value);
                //}
                //if (string.IsNullOrEmpty(order.IdArticle))
                //{
                //    order.IdArticle = null;
                //}

                ordRep.Update(order);
                ordRep.SaveChanges();
            }
        }

        protected void jobTable_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            JobRepository jobRep = new JobRepository();

            var editableItem = ((GridEditableItem)e.Item);
            var jobId = (string)editableItem.GetDataKeyValue("Id");
            var job = jobRep.FindByID(jobId);
            if (job != null)
            {
                editableItem.UpdateValues(job);
                if (job.StandardRate != Convert.ToDecimal(editableItem.SavedOldValues["StandardRate"]))
                {
                    //cambiato il numero stampate ora, cambio quindi il tempo unitario
                    job.CycleTime = 3600 / job.StandardRate;
                }
                //if (job.TempoAssegnatoUnitario != Convert.ToDecimal(editableItem.SavedOldValues["TempoAssegnatoUnitario"]))
                //{
                //    //cambiato il numero stampate ora, cambio quindi il tempo unitario
                //    job.NStampateOra = 3600 / job.TempoAssegnatoUnitario;
                //}
                //if (string.IsNullOrEmpty(job.ArticleId))
                //{
                //    job.ArticleId = null;
                //}
                if (string.IsNullOrEmpty(job.AssetId))
                {
                    job.AssetId = null;
                }
                if (string.IsNullOrEmpty(job.MoldId))
                {
                    job.MoldId = null;
                }
                if (string.IsNullOrEmpty(job.EquipmentId))
                {
                    job.EquipmentId = null;
                }

                jobRep.Update(job);
                jobRep.SaveChanges();
            }
        }

        protected void causeTable_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.Item.OwnerTableView.Name)
            {
                case "CauseType":
                    CauseTypeRepository causeTRep = new CauseTypeRepository();
                    var editableItem = ((GridEditableItem)e.Item);
                    var causeTId = editableItem.GetDataKeyValue("Id").ToString();
                    var causeT = causeTRep.FindByID(causeTId);
                    if (causeT != null)
                    {
                        editableItem.UpdateValues(causeT);

                        causeTRep.Update(causeT);
                        causeTRep.SaveChanges();
                    }
                    break;
                case "Cause":
                    CauseRepository causeRep = new CauseRepository();
                    var editableItem2 = ((GridEditableItem)e.Item);
                    var causeId = editableItem2.GetDataKeyValue("Id").ToString();
                    var cause = causeRep.FindByID(causeId);
                    if (cause != null)
                    {
                        editableItem2.UpdateValues(cause);

                        causeRep.Update(cause);
                        causeRep.SaveChanges();
                    }
                    break;
            }
        }
        #endregion

        #region DeleteCommand
        protected void articleTable_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            ArticleRepository artRep = new ArticleRepository();
            var articleId = Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("Id"));
            var article = artRep.FindByID(articleId);
            if (article != null)
            {
                artRep.Delete(article);
                try
                {
                    artRep.SaveChanges();
                }
                catch (Exception ex)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }

        protected void gridCustomerOrder_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            CustomerOrderRepository orderRep = new CustomerOrderRepository();
            var orderId = Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("Id"));
            var order = orderRep.FindByID(orderId);
            if (order != null)
            {
                orderRep.Delete(order);  //TODO: cancellare tutti i job associati?
                try
                {
                    orderRep.SaveChanges();
                }
                catch (Exception ex)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }

        protected void orderTable_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            OrderRepository orderRep = new OrderRepository();
            var orderId = Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("Id"));
            var order = orderRep.FindByID(orderId);
            if (order != null)
            {
                orderRep.Delete(order);  //TODO: cancellare tutti i job associati?
                try
                {
                    orderRep.SaveChanges();
                }
                catch (Exception ex)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }

        protected void jobTable_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            JobRepository jobRep = new JobRepository();
            var jobId = ((GridDataItem)e.Item).GetDataKeyValue("Id").ToString();
            var job = jobRep.FindByID(jobId);
            if (job != null)
            {
                jobRep.Delete(job);
                try
                {
                    jobRep.SaveChanges();
                }
                catch (Exception ex)
                {
                    messaggio.Text = "Impossibile rimuovere il record";
                    dlgWidgetConfig.OpenDialog();
                }
            }
        }

        protected void causeTable_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.Item.OwnerTableView.Name)
            {
                case "CauseType":
                    CauseTypeRepository causeTRep = new CauseTypeRepository();
                    var causeTId = ((GridDataItem)e.Item).GetDataKeyValue("Id").ToString();
                    var causeType = causeTRep.FindByID(causeTId);
                    if (causeType != null)
                    {
                        causeTRep.Delete(causeType);
                        try
                        {
                            causeTRep.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            messaggio.Text = "Impossibile rimuovere il record";
                            dlgWidgetConfig.OpenDialog();
                        }
                    }
                    break;
                case "Cause":
                    CauseRepository causeRep = new CauseRepository();
                    var causeId = ((GridDataItem)e.Item).GetDataKeyValue("Id").ToString();
                    var cause = causeRep.FindByID(causeId);
                    if (cause != null)
                    {
                        causeRep.Delete(cause);
                        try
                        {
                            causeRep.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            messaggio.Text = "Impossibile rimuovere il record";
                            dlgWidgetConfig.OpenDialog();
                        }
                    }
                    break;
            }
        }
        #endregion

        #region NeedDataSource
        protected void causeTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                CauseTypeRepository causeRep = new CauseTypeRepository();
                causeTable.DataSource = causeRep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Description).ToList();
            }
        }

        protected void articleTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ArticleRepository repos = new ArticleRepository();
            articleTable.DataSource = repos.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
        }

        protected void gridCustomerOrder_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            CustomerOrderRepository repos = new CustomerOrderRepository();
            gridCustomerOrder.DataSource = repos.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
        }

        protected void orderTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            OrderRepository repos = new OrderRepository();
            orderTable.DataSource = repos.GetAllOrders();
            //orderTable.DataSource = repos.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
        }

        protected void jobTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            JobRepository jRepos = new JobRepository();
            jobTable.DataSource = jRepos.GetAllJobs();
        }
        #endregion

        protected void causeTable_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            string causeTypeId = dataItem.GetDataKeyValue("Id").ToString();
            CauseRepository causeRep = new CauseRepository();
            e.DetailTableView.DataSource = causeRep.ReadAll(x => x.CauseTypeId == causeTypeId && x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.ExternalCode.Length).ThenBy(x => x.ExternalCode).ToList();
        }

        #region ItemDataBound

        protected void articleTable_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item2 = (GridDataItem)e.Item;
                RadListBox listBox2 = item2["Molds"].Controls[1] as RadListBox;

                int articleId = Convert.ToInt32(item2.GetDataKeyValue("Id"));
                ArticleMoldService artMS = new ArticleMoldService();
                listBox2.DataSource = artMS.GetMoldListByArticleId(articleId);

                if (e.Item is GridEditFormInsertItem || e.Item is GridDataInsertItem)
                {
                    // insert item
                    GridDataItem item = (GridDataItem)e.Item;
                    RadListBox listBox = item["Molds"].Controls[1] as RadListBox;

                    MoldRepository moldRep = new MoldRepository();
                    listBox.DataSource = moldRep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                }
                else
                {
                    // edit item
                    GridDataItem item = (GridDataItem)e.Item;
                    RadListBox listBox = item["Molds"].Controls[1] as RadListBox;

                    MoldRepository moldRep = new MoldRepository();
                    listBox.DataSource = moldRep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();

                    //string articleId = item.GetDataKeyValue("Id").ToString();
                    //ArticleMoldService artMS = new ArticleMoldService();
                    //listBox.FindItemByValue().Selected = true;
                    //.DataSource = artMS.GetMoldListByArticleId(articleId);
                }
            }
        }

        protected void orderTable_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.IsInEditMode)
            {
                if (!(e.Item is IGridInsertItem))
                {
                    RadAutoCompleteBox auto = e.Item.FindControl("customerOrderFilter") as RadAutoCompleteBox;
                    auto.Entries.Add(new AutoCompleteBoxEntry((e.Item.DataItem as Order).CustomerOrder.OrderCode));

                    auto = e.Item.FindControl("articleFilter") as RadAutoCompleteBox;
                    auto.Entries.Add(new AutoCompleteBoxEntry((e.Item.DataItem as Order).Article.Code));
                }
            }
        }

        protected void jobTable_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.IsInEditMode)
            {
                if (!(e.Item is IGridInsertItem))
                {
                    RadAutoCompleteBox auto = e.Item.FindControl("orderFilter") as RadAutoCompleteBox;
                    auto.Entries.Add(new AutoCompleteBoxEntry((e.Item.DataItem as Job).Order.OrderCode));
                }
            }
        }
        #endregion
    }
}