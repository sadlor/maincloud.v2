﻿using AssetManagement.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.Registry
{
    public partial class Registry_View : DataWidget<Registry_View>
    {
        public Registry_View() : base(typeof(Registry_View)) {}

        //protected override void OnInit(EventArgs e)
        //{
        //    base.OnInit(e);

        //    edsJob.Selecting += new EventHandler<Microsoft.AspNet.EntityDataSource.EntityDataSourceSelectingEventArgs>(edsJob_Selecting);
        //}

        //protected void edsJob_Selecting(object sender, Microsoft.AspNet.EntityDataSource.EntityDataSourceSelectingEventArgs e)
        //{
        //    if (e.DataSource.Where != string.Empty)
        //    {
        //        e.DataSource.Where += " AND ";
        //    }

        //    e.DataSource.Where += string.Format("it.ApplicationId == '{0}'", MultiTenantsHelper.ApplicationId);
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            edsAsset.WhereParameters.Clear();
            edsAsset.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            //edsArticle.WhereParameters.Clear();
            //edsArticle.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            //edsEquipment.WhereParameters.Clear();
            //edsEquipment.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            //edsJob.WhereParameters.Clear();
            //edsJob.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            //edsMold.WhereParameters.Clear();
            //edsMold.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            //edsOperator.WhereParameters.Clear();
            //edsOperator.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            //edsOrder.WhereParameters.Clear();
            //edsOrder.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        //protected void btnArticle_Click(object sender, EventArgs e)
        //{
        //    pnlArticle.Visible = true;
        //    pnlOrder.Visible = false;
        //    pnlCause.Visible = false;
        //    pnlJob.Visible = false;
        //}

        //protected void btnOrder_Click(object sender, EventArgs e)
        //{
        //    pnlOrder.Visible = true;
        //    pnlArticle.Visible = false;
        //    pnlJob.Visible = false;
        //    pnlCause.Visible = false;
        //}

        //protected void btnJob_Click(object sender, EventArgs e)
        //{
        //    pnlJob.Visible = true;
        //    pnlArticle.Visible = false;
        //    pnlOrder.Visible = false;
        //    pnlCause.Visible = false;
        //}

        //protected void btnOperator_Click(object sender, EventArgs e)
        //{
        //    pnlArticle.Visible = false;
        //    pnlOrder.Visible = false;
        //    pnlCause.Visible = false;
        //    pnlJob.Visible = false;
        //}

        //protected void btnCause_Click(object sender, EventArgs e)
        //{
        //    pnlCause.Visible = true;
        //    pnlArticle.Visible = false;
        //    pnlOrder.Visible = false;
        //    pnlJob.Visible = false;
        //    causeTable.Rebind();
        //}

        protected void causeTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                CauseTypeRepository causeRep = new CauseTypeRepository();
                causeTable.DataSource = causeRep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Description).ToList();
            }
        }

        protected void causeTable_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            string causeTypeId = dataItem.GetDataKeyValue("Id").ToString();
            CauseRepository causeRep = new CauseRepository();
            e.DetailTableView.DataSource = causeRep.ReadAll(x => x.CauseTypeId == causeTypeId && x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.ExternalCode.Length).ThenBy(x => x.ExternalCode).ToList();
        }

        protected void gridJob_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            JobRepository jobRep = new JobRepository();
            gridJob.DataSource = jobRep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
        }

        protected void gridJob_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                //GetFilteredDataSource();
                Pair command = (Pair)e.CommandArgument;
                if (command.Second.ToString() == "ddlAsset")
                {
                    e.Canceled = true;
                    GridFilteringItem filter = (GridFilteringItem)e.Item;
                    string s = ((filter["ddlAsset"].Controls[0]) as TextBox).Text;

                    AssetRepository assetRep = new AssetRepository();
                    string newFilter = string.Empty;
                    foreach (var id in assetRep.ReadAll(x => x.Description.Contains(s)).Select(x => x.Id).ToList())
                    {
                        if (!string.IsNullOrEmpty(newFilter))
                        {
                            newFilter += " OR ([AssetId] = '" + id + "')";
                        }
                        else
                        {
                            newFilter = "([AssetId] = '" + id + "')";
                        }
                    }

                    if (gridJob.MasterTableView.FilterExpression == "")
                    {
                        gridJob.MasterTableView.FilterExpression = newFilter;
                    }
                    else
                    {
                        gridJob.MasterTableView.FilterExpression = "((" + gridJob.MasterTableView.FilterExpression + ") AND (" + newFilter + "))";
                    }
                    gridJob.Rebind();

                    //command.Second = "AssetName";
                    //filter.FireCommandEvent("Filter", new Pair(command.First, "AssetName"));
                }
            }
        }

        protected void gridJob_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridFilteringItem)
            {
                GridFilteringItem item = e.Item as GridFilteringItem;
                ((item["ddlAsset"].Controls[0]) as TextBox).Text = ((item["AssetName"].Controls[0]) as TextBox).Text;
            }
        }

        private List<Job> GetFilteredDataSource()
        {
            List<Job> DT = new List<Job>();
            List<Job> FilteredDT = new List<Job>();
            string filterexpression = string.Empty;
            filterexpression = gridJob.MasterTableView.FilterExpression;
            DT = (List<Job>)gridJob.DataSource;
            //FilteredDT = DT.AsEnumerable()
            //.AsQueryable()
            //.Where(filterexpression)
            //.CopyToDataTable();
            return FilteredDT;
        }
    }
}