﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ControlPlansList_Edit.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.ControlPlansList.ControlPlansList_Edit" %>

<link href="/Dashboard/Modules/Seneca/ControlPlansList/ControlPlan.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    function PopUpShowing(sender, eventArgs)
    {
        var popUp = eventArgs.get_popUp();
        popUp.style.position = "fixed";
        popUp.style.top = "10%";
    }

    function DPopUpShowing(sender, eventArgs)
    {
        var popUp = eventArgs.get_popUp();
        popUp.style.position = "fixed";
        popUp.style.top = "10%";
    }

    function radioTimeFrequency_CheckedChanged(sender, eventArgs)
    {
        if (eventArgs.get_checked())
        {
            document.getElementById("txtTimeFrequencyHours").disabled = false;
            document.getElementById("txtTimeFrequencyMinutes").disabled = false;
            document.getElementById("txtTimeFrequencySeconds").disabled = false;
        }
        else
        {
            document.getElementById("txtTimeFrequencyHours").disabled = true;
            document.getElementById("txtTimeFrequencyMinutes").disabled = true;
            document.getElementById("txtTimeFrequencySeconds").disabled = true;
        }
    }

    function radioQtyFrequency_CheckedChanged(sender, eventArgs)
    {
        if (eventArgs.get_checked())
        {
            document.getElementById("txtQtyFrequency").disabled = false;
        }
        else
        {
            document.getElementById("txtQtyFrequency").disabled = true;
        }
    }
</script>

<style>
    .rgEditFormContainer tr
    {
        height: 50px!important;
    }

    .rgEditFormContainer tr td:first-child
    {
        width: 120px;
        font-weight: bold!important;
    }
</style>


<mcf:PopUpDialog ID="dlgPlanErrorDialog" runat="server" Title="Warning">
    <Body>
        <asp:label ID="errorLabel" Text="Il formato della data non è corretto.<br>Le modifiche non sono state salvate." runat="server"/><br /><br />
        <telerik:RadButton runat="server" Text="Ok" OnClick="dlgPlanError_Ok_Click"></telerik:RadButton>
    </Body>
</mcf:PopUpDialog>


<ef:EntityDataSource ID="edsMU" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="MeasureUnits"
    AutoGenerateWhereClause="true" />

<telerik:RadGrid runat="server" ID="cpTable" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="True" AllowSorting="true"
    OnNeedDataSource="cpTable_NeedDataSource" OnUpdateCommand="cpTable_UpdateCommand" OnInsertCommand="cpTable_InsertCommand" OnDeleteCommand="cpTable_DeleteCommand" OnItemCommand="cpTable_ItemCommand"
    AllowAutomaticInserts="false" AllowAutomaticUpdates="false" AllowAutomaticDeletes="false" OnPreRender="cpTable_PreRender" Height="700px" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <MasterTableView FilterExpression="" Caption="Control Plans" CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage" DataKeyNames="Id" NoMasterRecordsText="No records to display." EditMode="PopUp">
        <EditFormSettings InsertCaption="Create new Control Plan">
            <PopUpSettings Modal="true" />
        </EditFormSettings>
        <Columns>
            <telerik:GridEditCommandColumn />
            <telerik:GridTemplateColumn DataField="Id" HeaderText="Cod." ItemStyle-Width="50px" ItemStyle-Wrap="true" ReadOnly="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Id") %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="Description" HeaderText="Descrizione" ItemStyle-Width="290px" ItemStyle-Wrap="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Description") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtDescription"
                            Text='<%# Eval("Description")%>' DbValue='<%# Eval("Description")%>' Width="100%" MaxLength="50" TextMode="MultiLine" Rows="2">
                        </telerik:RadTextBox>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="CreationDate" HeaderText="Creazione" ItemStyle-Width="110px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("CreationDateText") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadDatePicker RenderMode="Lightweight" runat="server" ID="txtCreationDate"
                            DbSelectedDate='<%# Eval("CreationDate")%>' DbValue='<%# Eval("CreationDate")%>' DataFormatString="{0:dd/MM/yyyy}" Width="100%">
                        </telerik:RadDatePicker>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn> 
            <telerik:GridTemplateColumn DataField="ValidationDate" HeaderText="Validità" ItemStyle-Width="110px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ValidationDateText") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadDatePicker RenderMode="Lightweight" runat="server" ID="txtValidationDate"
                            DbSelectedDate='<%# Eval("ValidationDate")%>' DbValue='<%# Eval("ValidationDate")%>' DataFormatString="{0:dd/MM/yyyy}" Width="100%">
                        </telerik:RadDatePicker>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn> 
            <telerik:GridTemplateColumn DataField="UpdateDate" HeaderText="Modifica" ItemStyle-Width="110px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("UpdateDateText") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadDatePicker RenderMode="Lightweight" runat="server" ID="txtUpdateDate"
                            DbSelectedDate='<%# Eval("UpdateDate")%>' DbValue='<%# Eval("UpdateDate")%>' DataFormatString="{0:dd/MM/yyyy}" Width="100%">
                        </telerik:RadDatePicker>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn> 
            <telerik:GridTemplateColumn DataField="ApprovalUser" HeaderText="Approvazione" ItemStyle-Width="110px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ApprovalUser") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtApprovalUser"
                            Text='<%# Eval("ApprovalUser")%>' DbValue='<%# Eval("ApprovalUser")%>' MaxLength="50" Width="100%">
                        </telerik:RadTextBox>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>                
            <telerik:GridTemplateColumn DataField="ExecutionUser" HeaderText="Esecuzione" ItemStyle-Width="110px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ExecutionUser") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtExecutionUser"
                            Text='<%# Eval("ExecutionUser")%>' DbValue='<%# Eval("ExecutionUser")%>' MaxLength="50" Width="100%">
                        </telerik:RadTextBox>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="TimeFrequencyDate" HeaderText="Freq. Tempo" ItemStyle-Width="85px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("TimeFrequencyDate") %>' Visible='<%# Eval("TimeFrequencyFlag")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12" style="padding-right: 0; height: 50px;">
                        <div>
                            <telerik:RadButton runat="server" ID="radioTimeFrequency" ButtonType="ToggleButton" ToggleType="Radio" ClientIDMode="Static" AutoPostBack="false" GroupName="freqButtons" OnClientCheckedChanged="radioTimeFrequency_CheckedChanged" Checked='<%# Eval("TimeFrequencyFlag")%>' style="position: relative; top: 17px;"/>
                        </div>
                        <div style="margin-left: 10px; position: relative; top: -18px;">
                            <div class="col-md-4">
                                <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtTimeFrequencyHours" NumberFormat-DecimalDigits="0" Label="" ClientIDMode="Static"
                                    Text='<%# Eval("TimeFrequencyHours")%>' DbValue='<%# Eval("TimeFrequencyHours")%>' Width="100%" Enabled='<%# Eval("TimeFrequencyFlag")%>'>
                                </telerik:RadNumericTextBox>
                                <br />
                                <asp:Label runat="server">hours</asp:Label>
                            </div>
                            <div class="col-md-4">
                                <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtTimeFrequencyMinutes" NumberFormat-DecimalDigits="0" Label="" ClientIDMode="Static"
                                    Text='<%# Eval("TimeFrequencyMinutes")%>' DbValue='<%# Eval("TimeFrequencyMinutes")%>' Width="100%" Enabled='<%# Eval("TimeFrequencyFlag")%>'>
                                </telerik:RadNumericTextBox>
                                <br />
                                <asp:Label runat="server">min.</asp:Label>
                            </div>
                            <div class="col-md-4">
                                <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtTimeFrequencySeconds" NumberFormat-DecimalDigits="0" Label="" ClientIDMode="Static"
                                    Text='<%# Eval("TimeFrequencySeconds")%>' DbValue='<%# Eval("TimeFrequencySeconds")%>' Width="100%" Enabled='<%# Eval("TimeFrequencyFlag")%>'>
                                </telerik:RadNumericTextBox>
                                <br />
                                <asp:Label runat="server">sec.</asp:Label>
                            </div>
                        </div>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="QtyFrequency" HeaderText="Freq. Quantità" ItemStyle-Width="100px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("QtyFrequency") %>' Visible='<%# Eval("QtyFrequencyFlag")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12" style="padding-right: 0; height: 50px;">
                        <div>
                            <telerik:RadButton runat="server" ID="radioQtyFrequency" ButtonType="ToggleButton" ToggleType="Radio" ClientIDMode="Static" AutoPostBack="false" GroupName="freqButtons" OnClientCheckedChanged="radioQtyFrequency_CheckedChanged" Checked='<%# Eval("QtyFrequencyFlag")%>' style="position: relative; top: 17px;"/>
                        </div>
                        <div style="margin-left: 10px; position: relative; top: -11px;">
                            <div class="col-md-12">
                                <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtQtyFrequency" ClientIDMode="Static"
                                    Text='<%# Eval("QtyFrequency")%>' DbValue='<%# Eval("QtyFrequency")%>' Width="100%" Enabled='<%# Eval("QtyFrequencyFlag")%>'>
                                </telerik:RadNumericTextBox>
                            </div>
                        </div>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Num. controlli" ItemStyle-Width="60px">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ControlsCountNum") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ID="txtControlsCount" ClientIDMode="Static"
                            Text='<%# Eval("ControlsCountNum")%>' DbValue='<%# Eval("ControlsCountNum")%>' Width="100%">
                        </telerik:RadNumericTextBox>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="PlanNotes" HeaderText="Note" ItemStyle-Wrap="true">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("PlanNotes") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <div class="col-md-12">
                        <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtPlanNotes"
                            Text='<%# Eval("PlanNotes")%>' DbValue='<%# Eval("PlanNotes")%>' Width="100%" MaxLength="2000" TextMode="MultiLine" Rows="2">
                        </telerik:RadTextBox>
                    </div>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>       
            <telerik:GridButtonColumn ConfirmText="Delete this Control Plan and all its Activites?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
        </Columns>
        <NestedViewTemplate runat="server">
            <br />
            <asp:Label runat="server">
                <b>Control Plan Definition</b>
            </asp:Label>
            <br /><br />
            <telerik:RadGrid ID="cpdTable" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="true" OnNeedDataSource="cpdTable_NeedDataSource" OnItemCommand="cpdTable_ItemCommand" OnUpdateCommand="cpdTable_UpdateCommand" OnInsertCommand="cpdTable_InsertCommand" OnDeleteCommand="cpdTable_DeleteCommand" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
                <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" CommandItemDisplay="Top" NoMasterRecordsText="No records to display." EditMode="PopUp">
                    <EditFormSettings InsertCaption="Create new Activity">
                        <PopUpSettings Modal="true" />
                    </EditFormSettings>
                    <Columns>
                        <telerik:GridEditCommandColumn />
                        <telerik:GridTemplateColumn HeaderText="Descrizione" ItemStyle-Width="240px" ItemStyle-Wrap="true">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Description") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadTextBox RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="txtDetailDescription"
                                        Text='<%# Eval("Description")%>' DbValue='<%# Eval("Description")%>' Width="100%" MaxLength="40" TextMode="MultiLine" Rows="2">
                                    </telerik:RadTextBox>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="ControlType" HeaderText="Tipolog." ItemStyle-Width="100px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ControlType") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="comboDetailControlType"
                                        DbValue='<%# Eval("ControlType")%>' SelectedText='<%# Eval("ControlType")%>'  Width="100%"
                                        DataTextField="ControlType"
                                        DataValueField="ControlType">
                                        <Items> 
                                            <telerik:DropDownListItem runat="server" Text="Dimensionale" />
                                            <telerik:DropDownListItem runat="server" Text="Visivo" />
                                        </Items>
                                    </telerik:RadDropDownList>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="Operator" HeaderText="Operatore" ItemStyle-Width="100px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Operator") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="comboDetailOperator"
                                        DbValue='<%# Eval("Operator")%>' SelectedText='<%# Eval("Operator")%>' DataTextField="Operator"
                                        DataValueField="Operator" Width="100%">
                                        <Items> 
                                            <telerik:DropDownListItem runat="server" Text="Uguale a" />
                                            <telerik:DropDownListItem runat="server" Text="Maggiore di" />
                                            <telerik:DropDownListItem runat="server" Text="Minore di" />
                                            <telerik:DropDownListItem runat="server" Text="Maggiore Uguale di" />
                                            <telerik:DropDownListItem runat="server" Text="Minore Uguale di" />
                                        </Items>
                                    </telerik:RadDropDownList>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>    
                        <telerik:GridTemplateColumn DataField="Udm" HeaderText="UDM" ItemStyle-Width="65px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Udm") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadComboBox RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="comboDetailUdm"
                                        DbValue='<%# Eval("Udm")%>' DataSourceID="edsMU" DataTextField="Name" DataValueField="Id" Text='<%# Eval("Udm")%>' AllowCustomText="true" MaxLength="7" Width="100%">
                                    </telerik:RadComboBox>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn> 
                        <telerik:GridTemplateColumn DataField="Tolerance1Sign" HeaderText="Seg.1" ItemStyle-Width="35px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Tolerance1Sign") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="comboDetailTolerance1Sign"
                                        DbValue='<%# Eval("Tolerance1Sign")%>' SelectedText='<%# Eval("Tolerance1Sign")%>'  DataTextField="Tolerance1Sign" DataValueField="Tolerance1Sign" Width="100%">
                                        <Items> 
                                            <telerik:DropDownListItem runat="server" Text="-" />
                                            <telerik:DropDownListItem runat="server" Text="+" />
                                        </Items>
                                    </telerik:RadDropDownList>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn> 
                        <telerik:GridTemplateColumn DataField="Tolerance1Value" HeaderText="Toll.1" ItemStyle-Width="80px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Tolerance1Value") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="txtDetailTolerance1" MinValue="0"
                                        Text='<%# Eval("Tolerance1Value")%>' DbValue='<%# Eval("Tolerance1Value")%>' Width="100%">
                                    </telerik:RadNumericTextBox>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="BaseValue" HeaderText="Valore" ItemStyle-Width="90px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("BaseValue") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="txtDetailBaseValue"
                                        Text='<%# Eval("BaseValue")%>' DbValue='<%# Eval("BaseValue")%>' Width="100%">
                                    </telerik:RadNumericTextBox>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="Tolerance2Sign" HeaderText="Seg.2" ItemStyle-Width="35px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Tolerance2Sign") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="comboDetailTolerance2Sign"
                                        DbValue='<%# Eval("Tolerance2Sign")%>' SelectedText='<%# Eval("Tolerance2Sign")%>'  DataTextField="Tolerance2Sign" DataValueField="Tolerance2Sign" Width="100%">
                                        <Items>
                                            <telerik:DropDownListItem runat="server" Text="+" />
                                            <telerik:DropDownListItem runat="server" Text="-" />
                                        </Items>
                                    </telerik:RadDropDownList>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="Tolerance2Value" HeaderText="Toll.2" ItemStyle-Width="80px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Tolerance2Value") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadNumericTextBox RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="txtDetailTolerance2" MinValue="0"
                                        Text='<%# Eval("Tolerance2Value")%>' DbValue='<%# Eval("Tolerance2Value")%>' Width="100%">
                                    </telerik:RadNumericTextBox>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="ToolCode" HeaderText="Cod. Strumento" ItemStyle-Width="80px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ToolCode") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadTextBox RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="txtDetailToolCode"
                                        Text='<%# Eval("ToolCode")%>' DbValue='<%# Eval("ToolCode")%>' MaxLength="12" Width="100%">
                                    </telerik:RadTextBox>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="DetailNotes" HeaderText="Note" ItemStyle-Wrap="true">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("DetailNotes") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="col-md-12">
                                    <telerik:RadTextBox RenderMode="Lightweight" runat="server" ClientIDMode="Static" ID="txtDetailDetailNotes"
                                        Text='<%# Eval("DetailNotes")%>' DbValue='<%# Eval("DetailNotes")%>' Width="100%" MaxLength="2000" TextMode="MultiLine" Rows="2">
                                    </telerik:RadTextBox>
                                </div>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete this Activity?" ConfirmDialogType="RadWindow"
                            ConfirmTitle="Delete" ButtonType="FontIconButton" CommandName="Delete" />
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <ClientEvents OnPopUpShowing="DPopUpShowing" />
                </ClientSettings>
            </telerik:RadGrid>
            <br />
        </NestedViewTemplate>
    </MasterTableView>
    <ClientSettings>
        <Scrolling AllowScroll="true" EnableColumnClientFreeze="true" FrozenColumnsCount="2" UseStaticHeaders="false" />
        <ClientEvents OnPopUpShowing="PopUpShowing" />
    </ClientSettings>
</telerik:RadGrid>