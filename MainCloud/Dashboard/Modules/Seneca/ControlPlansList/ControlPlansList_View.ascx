﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ControlPlansList_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.ControlPlansList.ControlPlansList_View" %>

<link href="/Dashboard/Modules/Seneca/ControlPlansList/ControlPlan.css" rel="stylesheet" type="text/css" />

<asp:UpdateProgress runat="server" ID="progressPnl">
    <ProgressTemplate>
        <div class="loading">Loading&#8230;</div>
    </ProgressTemplate>
</asp:UpdateProgress>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <telerik:RadGrid runat="server" ID="cpTable" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="True" AllowSorting="true"
            OnNeedDataSource="cpTable_NeedDataSource" OnPreRender="cpTable_PreRender" Width="100%" Height="600px" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="false" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
            <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" NoMasterRecordsText="No records to display.">
                <Columns>
                    <telerik:GridBoundColumn DataField="Id" HeaderText="Cod." ItemStyle-Width="50px" ItemStyle-Wrap="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Descrizione" ItemStyle-Width="340px" ItemStyle-Wrap="true" MaxLength="50">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="CreationDate" HeaderText="Creazione" ItemStyle-Width="110px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("CreationDateText") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn> 
                    <telerik:GridTemplateColumn DataField="ValidationDate" HeaderText="Validità" ItemStyle-Width="110px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ValidationDateText") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn> 
                    <telerik:GridTemplateColumn DataField="UpdateDate" HeaderText="Modifica" ItemStyle-Width="110px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("UpdateDateText") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn> 
                    <telerik:GridBoundColumn DataField="ApprovalUser" HeaderText="Approvazione" ItemStyle-Width="110px" MaxLength="50">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>                
                    <telerik:GridBoundColumn DataField="ExecutionUser" HeaderText="Esecuzione" ItemStyle-Width="110px" MaxLength="50">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="TimeFrequencyDate" HeaderText="Freq. Tempo" ItemStyle-Width="85px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("TimeFrequencyDate") %>' Visible='<%# Eval("TimeFrequencyFlag")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="QtyFrequency" HeaderText="Freq. Quantità" ItemStyle-Width="100px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("QtyFrequency") %>' Visible='<%# Eval("QtyFrequencyFlag")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Num. controlli" ItemStyle-Width="60px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ControlsCountNum") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="PlanNotes" HeaderText="Note" MaxLength="2000" ItemStyle-Wrap="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>    
                </Columns>
                <NestedViewTemplate runat="server">
                    <br />
                    <asp:Label runat="server">
                        <b>Control Plan Definition</b>
                    </asp:Label>
                    <br /><br />
                    <telerik:RadGrid ID="cpdTable" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="true" OnNeedDataSource="cpdTable_NeedDataSource" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
                        <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" NoMasterRecordsText="No records to display.">
                            <Columns>
                                <telerik:GridBoundColumn DataField="Description" HeaderText="Descrizione" ItemStyle-Width="410px" MaxLength="40" ItemStyle-Wrap="true">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ControlType" HeaderText="Tipolog." ItemStyle-Width="100px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="Operator" HeaderText="Operatore" ItemStyle-Width="100px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Operator") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>    
                                <telerik:GridTemplateColumn DataField="Udm" HeaderText="UDM" ItemStyle-Width="65px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Udm") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn> 
                                <telerik:GridTemplateColumn DataField="Tolerance1Sign" HeaderText="Seg.1" ItemStyle-Width="35px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Tolerance1Sign") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn> 
                                <telerik:GridTemplateColumn DataField="Tolerance1Value" HeaderText="Toll.1" ItemStyle-Width="80px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Tolerance1Value") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="BaseValue" HeaderText="Valore" ItemStyle-Width="90px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("BaseValue") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Tolerance2Sign" HeaderText="Seg.2" ItemStyle-Width="35px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Tolerance2Sign") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Tolerance2Value" HeaderText="Toll.2" ItemStyle-Width="80px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Tolerance2Value") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="ToolCode" HeaderText="Cod. Strumento" ItemStyle-Width="80px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DetailNotes" HeaderText="Note" MaxLength="2000" ItemStyle-Wrap="true">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <br />
                </NestedViewTemplate>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="true" EnableColumnClientFreeze="true" FrozenColumnsCount="1" UseStaticHeaders="false" />
            </ClientSettings>
        </telerik:RadGrid>                      
    </ContentTemplate>
</asp:UpdatePanel>