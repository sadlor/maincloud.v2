﻿using MainCloudFramework.UI.Modules;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.ControlPlansList
{
    public partial class ControlPlansList_Edit : WidgetControl<object>
    {
        public ControlPlansList_Edit() : base(typeof(ControlPlansList_Edit)) { }

        protected void Page_Load(object sender, EventArgs e) { }

        public string ApplicationId
        {
            get
            {
                return (Page as PageEdit).GetApplicationId();
            }
        }

        protected void cpTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ControlPlanRepository cpRep = new ControlPlanRepository();
            List<ControlPlan> cpList = cpRep.ReadAll().ToList();
            cpTable.DataSource = cpList;
        }

        protected void cpTable_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
                cpTable.Rebind();
        }

        protected void cpTable_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                //Add new button clicked
                e.Canceled = true;
                //Prepare an IDictionary with the predefined values
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();
                newValues["Description"] = "";
                newValues["TimeFrequencyFlag"] = true;
                newValues["QtyFrequencyFlag"] = false;
                newValues["CreationDate"] = DateTime.Today;
                newValues["ValidationDate"] = DateTime.Today;
                newValues["UpdateDate"] = DateTime.Today;
                newValues["TimeFrequencyHours"] = "00";
                newValues["TimeFrequencyMinutes"] = "00";
                newValues["TimeFrequencySeconds"] = "00";
                newValues["QtyFrequency"] = "0,00";
                newValues["ControlsCountNum"] = 1;
                //Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }

        protected void cpTable_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);

            ControlPlanRepository cpRep = new ControlPlanRepository();
            ControlPlan cp = cpRep.FindByID(editableItem.GetDataKeyValue("Id"));

            if (cp != null)
            {
                bool inputCheck = true;

                cp.Description = (editableItem.FindControl("txtDescription") as RadTextBox).Text;

                if ((editableItem.FindControl("txtCreationDate") as RadDatePicker).SelectedDate != null)
                {
                    cp.CreationDate = (DateTime)(editableItem.FindControl("txtCreationDate") as RadDatePicker).SelectedDate;
                    if (!checkDate(cp.CreationDate))
                    {
                        inputCheck = false;
                    }
                }
                else
                {
                    inputCheck = false;
                }

                if ((editableItem.FindControl("txtValidationDate") as RadDatePicker).SelectedDate != null)
                {
                    cp.ValidationDate = (DateTime)(editableItem.FindControl("txtValidationDate") as RadDatePicker).SelectedDate;
                    if (!checkDate(cp.ValidationDate))
                    {
                        inputCheck = false;
                    }
                }
                else
                {
                    inputCheck = false;
                }

                if ((editableItem.FindControl("txtUpdateDate") as RadDatePicker).SelectedDate != null)
                {
                    cp.UpdateDate = (DateTime)(editableItem.FindControl("txtUpdateDate") as RadDatePicker).SelectedDate;
                    if (!checkDate(cp.UpdateDate))
                    {
                        inputCheck = false;
                    }
                }
                else
                {
                    inputCheck = false;
                }

                cp.ApprovalUser = (editableItem.FindControl("txtApprovalUser") as RadTextBox).Text;
                cp.ExecutionUser = (editableItem.FindControl("txtExecutionUser") as RadTextBox).Text;

                cp.QtyFrequencyFlag = (editableItem.FindControl("radioQtyFrequency") as RadButton).Checked;
                if (cp.QtyFrequencyFlag)
                {
                    cp.QtyFrequency = Convert.ToDecimal((editableItem.FindControl("txtQtyFrequency") as RadNumericTextBox).Value);
                }

                if (!cp.QtyFrequencyFlag)
                {
                    string tfHs = (editableItem.FindControl("txtTimeFrequencyHours") as RadNumericTextBox).Text,
                           tfMs = (editableItem.FindControl("txtTimeFrequencyMinutes") as RadNumericTextBox).Text,
                           tfSs = (editableItem.FindControl("txtTimeFrequencySeconds") as RadNumericTextBox).Text;
                    int tfH = Convert.ToInt32((tfHs != "") ? tfHs : "0"),
                        tfM = Convert.ToInt32((tfMs != "") ? tfMs : "0"),
                        tfS = Convert.ToInt32((tfSs != "") ? tfSs : "0");

                    TimeSpan tf = new TimeSpan(0);
                    tf = tf.Add(new TimeSpan(tfH, tfM, tfS));
                    cp.TimeFrequency = tf.Ticks;
                }

                if ((editableItem.FindControl("txtControlsCount") as RadNumericTextBox).Text.Trim().Length > 0)
                {
                    try
                    {
                        cp.ControlsCount = Convert.ToInt32((editableItem.FindControl("txtControlsCount") as RadNumericTextBox).Text);
                    }
                    catch (Exception)
                    {
                        cp.ControlsCount = 1;
                    }
                }
                else
                {
                    cp.ControlsCount = 1;
                }

                cp.PlanNotes = (editableItem.FindControl("txtPlanNotes") as RadTextBox).Text;

                if (inputCheck)
                {
                    // Aggiorna valori, in tabella e database
                    editableItem.UpdateValues(cp);
                    cpRep.SaveChanges();

                    // Aggiungo il Client Id
                    ControlPlan cpN = cpRep.FindByID(cp.Id);
                    cpN.ClientId = cp.Id;
                    cpRep.Update(cpN);
                    cpRep.SaveChanges();
                }
                else
                {
                    // Valori inseriti errati, ignora le modifiche
                    errorLabel.Text = "Il formato della data non è corretto.<br>Le modifiche non sono state salvate.";
                    // Wrong date format.<br>The changes had not been saved.
                    dlgPlanErrorDialog.OpenDialog();
                }
            }
        }

        protected void cpTable_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);

            ControlPlanRepository cpRep = new ControlPlanRepository();
            ControlPlan cp = new ControlPlan();

            bool inputCheck = true;

            cp.Description = (editableItem.FindControl("txtDescription") as RadTextBox).Text;

            if ((editableItem.FindControl("txtCreationDate") as RadDatePicker).SelectedDate != null)
            {
                cp.CreationDate = (DateTime)(editableItem.FindControl("txtCreationDate") as RadDatePicker).SelectedDate;
                if (!checkDate(cp.CreationDate))
                {
                    inputCheck = false;
                }
            }
            else
            {
                inputCheck = false;
            }

            if ((editableItem.FindControl("txtValidationDate") as RadDatePicker).SelectedDate != null)
            {
                cp.ValidationDate = (DateTime)(editableItem.FindControl("txtValidationDate") as RadDatePicker).SelectedDate;
                if (!checkDate(cp.ValidationDate))
                {
                    inputCheck = false;
                }
            }
            else
            {
                inputCheck = false;
            }

            if ((editableItem.FindControl("txtUpdateDate") as RadDatePicker).SelectedDate != null)
            {
                cp.UpdateDate = (DateTime)(editableItem.FindControl("txtUpdateDate") as RadDatePicker).SelectedDate;
                if (!checkDate(cp.UpdateDate))
                {
                    inputCheck = false;
                }
            }
            else
            {
                inputCheck = false;
            }

            cp.ApprovalUser = (editableItem.FindControl("txtApprovalUser") as RadTextBox).Text;
            cp.ExecutionUser = (editableItem.FindControl("txtExecutionUser") as RadTextBox).Text;

            cp.QtyFrequencyFlag = (editableItem.FindControl("radioQtyFrequency") as RadButton).Checked;
            if (cp.QtyFrequencyFlag)
            {
                cp.QtyFrequency = Convert.ToDecimal((editableItem.FindControl("txtQtyFrequency") as RadNumericTextBox).Value);
            }
            else
            {
                cp.QtyFrequency = 0;
            }

            if (!cp.QtyFrequencyFlag)
            {
                string tfHs = (editableItem.FindControl("txtTimeFrequencyHours") as RadNumericTextBox).Text,
                       tfMs = (editableItem.FindControl("txtTimeFrequencyMinutes") as RadNumericTextBox).Text,
                       tfSs = (editableItem.FindControl("txtTimeFrequencySeconds") as RadNumericTextBox).Text;
                int tfH = Convert.ToInt32((tfHs != "") ? tfHs : "0"),
                    tfM = Convert.ToInt32((tfMs != "") ? tfMs : "0"),
                    tfS = Convert.ToInt32((tfSs != "") ? tfSs : "0");

                TimeSpan tf = new TimeSpan(0);
                tf = tf.Add(new TimeSpan(tfH, tfM, tfS));
                cp.TimeFrequency = tf.Ticks;
            }
            else
            {
                cp.TimeFrequency = 0;
            }

            if ((editableItem.FindControl("txtControlsCount") as RadNumericTextBox).Text.Trim().Length > 0)
            {
                try
                {
                    cp.ControlsCount = Convert.ToInt32((editableItem.FindControl("txtControlsCount") as RadNumericTextBox).Text);
                }
                catch (Exception)
                {
                    cp.ControlsCount = 1;
                }
            }
            else
            {
                cp.ControlsCount = 1;
            }

            cp.PlanNotes = (editableItem.FindControl("txtPlanNotes") as RadTextBox).Text;
            cp.ApplicationId = ApplicationId;

            if (inputCheck)
            {
                // Salvo il nuovo Piano di controllo
                cpRep.Insert(cp);
                cpRep.SaveChanges();
            }
            else
            {
                // Valori inseriti errati
                errorLabel.Text = "Il formato della data non è corretto.<br>Le modifiche non sono state salvate.";
                dlgPlanErrorDialog.OpenDialog();
            }
        }

        protected void cpTable_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);

            JobRepository jRep = new JobRepository();
            List<Job> jobList = jRep.GetAllByControlPlanId(Convert.ToInt32(editableItem.GetDataKeyValue("Id")));

            if (jobList.Count == 0)
            {
                ControlPlanRepository cpRep = new ControlPlanRepository();
                ControlPlan cp = cpRep.FindByID(editableItem.GetDataKeyValue("Id"));

                ControlPlanDetailRepository cpdRep = new ControlPlanDetailRepository();
                List<ControlPlanDetail> cpdList = cpdRep.GetAllByControlPlanId(Convert.ToInt32(editableItem.GetDataKeyValue("Id")));

                foreach (ControlPlanDetail cpd in cpdList)
                {
                    cpdRep.Delete(cpd);
                }
                cpdRep.SaveChanges();

                cpRep.Delete(cp);
                cpRep.SaveChanges();
            }
            else
            {
                // Errore: Il piano di controllo è collegato ad un Articolo/Fase, l'eliminazione è impossibile
                errorLabel.Text = "Questo piano di controllo non può essere eliminato perchè è collegato ad uno o più Articoli/Fase.<br>Rimuovi il riferimento e riprova.";
                // This Control Plan is present in the definition of one or more Article/Stage, so it can't be deleted.<br>Remove its reference and retry.
                dlgPlanErrorDialog.OpenDialog();
            }
        }

        protected void cpdTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            string IdPlan = ((GridNestedViewItem)(sender as RadGrid).Parent.BindingContainer).ParentItem.KeyValues.Replace("{Id:\"", "").Replace("\"}", "");
            ControlPlanDetailRepository cpDRep = new ControlPlanDetailRepository();
            List<ControlPlanDetail> cpDList = cpDRep.GetAllByControlPlanId(Convert.ToInt32(IdPlan));
            (sender as RadGrid).DataSource = cpDList;
        }

        protected void cpdTable_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                //Add new button clicked
                e.Canceled = true;
                //Prepare an IDictionary with the predefined values
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();
                newValues["Description"] = "";
                newValues["Tolerance1Value"] = "0,00";
                newValues["Tolerance2Value"] = "0,00";
                newValues["BaseValue"] = "0,00";
                //Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }

        protected void cpdTable_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);

            ControlPlanDetailRepository cpdRep = new ControlPlanDetailRepository();
            ControlPlanDetail cpd = cpdRep.FindByID(editableItem.GetDataKeyValue("Id"));

            if (cpd != null)
            {
                cpd.Description = (editableItem.FindControl("txtDetailDescription") as RadTextBox).Text;
                cpd.ControlType = (editableItem.FindControl("comboDetailControlType") as RadDropDownList).SelectedText;
                cpd.Operator = (editableItem.FindControl("comboDetailOperator") as RadDropDownList).SelectedText;
                cpd.Udm = (editableItem.FindControl("comboDetailUdm") as RadComboBox).Text;
                cpd.Tolerance1Sign = (editableItem.FindControl("comboDetailTolerance1Sign") as RadDropDownList).SelectedText;
                cpd.Tolerance1Value = Convert.ToDecimal((editableItem.FindControl("txtDetailTolerance1") as RadNumericTextBox).Value);
                cpd.BaseValue = Convert.ToDecimal((editableItem.FindControl("txtDetailBaseValue") as RadNumericTextBox).Value);
                cpd.Tolerance2Sign = (editableItem.FindControl("comboDetailTolerance2Sign") as RadDropDownList).SelectedText;
                cpd.Tolerance2Value = Convert.ToDecimal((editableItem.FindControl("txtDetailTolerance2") as RadNumericTextBox).Value);
                cpd.ToolCode = (editableItem.FindControl("txtDetailToolCode") as RadTextBox).Text;
                cpd.DetailNotes = (editableItem.FindControl("txtDetailDetailNotes") as RadTextBox).Text;

                editableItem.UpdateValues(cpd);
                cpdRep.SaveChanges();
            }
        }

        protected void cpdTable_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            string IdPlan = ((GridNestedViewItem)(sender as RadGrid).Parent.BindingContainer).ParentItem.KeyValues.Replace("{Id:\"", "").Replace("\"}", "");

            ControlPlanDetailRepository cpdRep = new ControlPlanDetailRepository();
            ControlPlanDetail cpd = new ControlPlanDetail();

            cpd.ControlPlanId = Convert.ToInt32(IdPlan);
            cpd.Description = (editableItem.FindControl("txtDetailDescription") as RadTextBox).Text;
            cpd.ControlType = (editableItem.FindControl("comboDetailControlType") as RadDropDownList).SelectedText;
            cpd.Operator = (editableItem.FindControl("comboDetailOperator") as RadDropDownList).SelectedText;
            cpd.Udm = (editableItem.FindControl("comboDetailUdm") as RadComboBox).Text;
            cpd.Tolerance1Sign = (editableItem.FindControl("comboDetailTolerance1Sign") as RadDropDownList).SelectedText;
            cpd.Tolerance1Value = Convert.ToDecimal((editableItem.FindControl("txtDetailTolerance1") as RadNumericTextBox).Value);
            cpd.BaseValue = Convert.ToDecimal((editableItem.FindControl("txtDetailBaseValue") as RadNumericTextBox).Value);
            cpd.Tolerance2Sign = (editableItem.FindControl("comboDetailTolerance2Sign") as RadDropDownList).SelectedText;
            cpd.Tolerance2Value = Convert.ToDecimal((editableItem.FindControl("txtDetailTolerance2") as RadNumericTextBox).Value);
            cpd.ToolCode = (editableItem.FindControl("txtDetailToolCode") as RadTextBox).Text;
            cpd.DetailNotes = (editableItem.FindControl("txtDetailDetailNotes") as RadTextBox).Text;
            cpd.ApplicationId = ApplicationId;

            cpdRep.Insert(cpd);
            cpdRep.SaveChanges();

            // Aggiungo il Client Id
            ControlPlanDetail cpdN = cpdRep.FindByID(cpd.Id);
            cpdN.ClientId = cpd.Id;
            cpdRep.Update(cpdN);
            cpdRep.SaveChanges();
        }

        protected void cpdTable_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);

            ControlPlanDetailRepository cpdRep = new ControlPlanDetailRepository();
            ControlPlanDetail cpd = cpdRep.FindByID(editableItem.GetDataKeyValue("Id"));

            cpdRep.Delete(cpd);
            cpdRep.SaveChanges();
        }

        public bool checkDate(DateTime date)
        {
            return !(date < DateTime.MinValue || date > DateTime.MaxValue);
        }

        protected void dlgPlanError_Ok_Click(object sender, EventArgs e)
        {
            dlgPlanErrorDialog.CloseDialog();
        }
    }
}
