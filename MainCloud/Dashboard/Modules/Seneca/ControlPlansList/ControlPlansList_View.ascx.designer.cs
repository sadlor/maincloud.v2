﻿//------------------------------------------------------------------------------
// <generato automaticamente>
//     Codice generato da uno strumento.
//
//     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
//     il codice viene rigenerato. 
// </generato automaticamente>
//------------------------------------------------------------------------------

namespace MainCloud.Dashboard.Modules.Seneca.ControlPlansList {
    
    
    public partial class ControlPlansList_View {
        
        /// <summary>
        /// Controllo progressPnl.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.UpdateProgress progressPnl;
        
        /// <summary>
        /// Controllo cpTable.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::Telerik.Web.UI.RadGrid cpTable;
    }
}
