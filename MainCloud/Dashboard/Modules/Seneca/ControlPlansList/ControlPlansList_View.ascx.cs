﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.Seneca.ControlPlansList
{
    public partial class ControlPlansList_View : DataWidget<ControlPlansList_View>
    {
        public ControlPlansList_View() : base(typeof(ControlPlansList_View)) { }

        protected void cpTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ControlPlanRepository cpRep = new ControlPlanRepository();
            List<ControlPlan> cpList = cpRep.ReadAll().ToList();
            cpTable.DataSource = cpList;
        }

        protected void cpTable_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
                cpTable.Rebind();
        }

        protected void cpdTable_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            string IdPlan = ((GridNestedViewItem)(sender as RadGrid).Parent.BindingContainer).ParentItem.KeyValues.Replace("{Id:\"", "").Replace("\"}", "");
            ControlPlanDetailRepository cpDRep = new ControlPlanDetailRepository();
            List<ControlPlanDetail> cpDList = cpDRep.GetAllByControlPlanId(Convert.ToInt32(IdPlan));
            (sender as RadGrid).DataSource = cpDList;
        }
    }
}