﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace MainCloud.Dashboard.Modules.Seneca.WebService
{
    [ServiceContract]
    //[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SenecaWebService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("RollingLogFile");

        // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
        // To create an operation that returns XML,
        //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     and include the following line in the operation body:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            UriTemplate = "GetBoxList")]
        public string GetBoxList()
        {
            string senderIP = HttpContext.Current.Request.UserHostAddress;
            //string senderDNS = HttpContext.Current.Request.UserHostName;
            string appId = null;
            List<SenecaModule.Models.Box> returnList = new List<SenecaModule.Models.Box>();
            using (SenecaModule.Models.SenecaDbContext db = new SenecaModule.Models.SenecaDbContext())
            {
                try
                {
                    appId = db.Gateways.Where(x => x.NetworkAddress == senderIP).First().ApplicationId;
                    returnList = db.Boxes.Where(x => x.ApplicationId == appId).ToList();
                    //string gatewayId = db.Gateways.Where(x => x.NetworkAddress == senderIP).First().Id;
                }
                catch (InvalidOperationException)
                {
                    //gateway non registrato
                    log.Info("Gateway non registrato, IP: " + senderIP);
                    return HttpStatusCode.Forbidden.ToString();
                }
                return JsonConvert.SerializeObject(returnList);
            }
        }

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            UriTemplate = "GetGatewayList")]
        public string GetGatewayList()
        {
            string senderIP = HttpContext.Current.Request.UserHostAddress;
            //string senderDNS = HttpContext.Current.Request.UserHostName;
            string appId = null;
            List<SenecaModule.Models.Gateway> returnList = new List<SenecaModule.Models.Gateway>();
            using (SenecaModule.Models.SenecaDbContext db = new SenecaModule.Models.SenecaDbContext())
            {
                try
                {
                    appId = db.Gateways.Where(x => x.NetworkAddress == senderIP).First().ApplicationId;
                    returnList = db.Gateways.Where(x => x.ApplicationId == appId).ToList();
                    //string gatewayId = db.Gateways.Where(x => x.NetworkAddress == senderIP).First().Id;
                }
                catch (InvalidOperationException)
                {
                    //gateway non registrato
                    log.Info("Gateway non registrato, IP: " + senderIP);
                    return HttpStatusCode.Forbidden.ToString();
                }
                return JsonConvert.SerializeObject(returnList);
            }
        }

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            UriTemplate = "GetDeviceList")]
        public string GetDeviceList()
        {
            string senderIP = HttpContext.Current.Request.UserHostAddress;
            //string senderDNS = HttpContext.Current.Request.UserHostName;
            string appId = null;
            List<SenecaModule.Models.Device> returnList = new List<SenecaModule.Models.Device>();
            using (SenecaModule.Models.SenecaDbContext db = new SenecaModule.Models.SenecaDbContext())
            {
                try
                {
                    appId = db.Gateways.Where(x => x.NetworkAddress == senderIP).First().ApplicationId;
                    returnList = db.Devices.Where(x => x.ApplicationId == appId).ToList();
                    //string gatewayId = db.Gateways.Where(x => x.NetworkAddress == senderIP).First().Id;
                }
                catch (InvalidOperationException)
                {
                    //gateway non registrato
                    log.Info("Gateway non registrato, IP: " + senderIP);
                    return HttpStatusCode.Forbidden.ToString();
                }
                return JsonConvert.SerializeObject(returnList);
            }
        }
    }
}
