﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateLabel_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.UpdateLabel.UpdateLabel_View" %>

<link href="/Dashboard/Modules/Seneca/UpdateLabel/UpdateLabel.css" rel="stylesheet" />

<div id="updateContainer" style="width: 100%;">
    <div class="row nop">
        <asp:Label runat="server" ID="Label1" text="Selezionare la nuova struttura dell'etichetta (output.zpl):" />
        <br /><br />
    </div>
    <div class="row nop">
        <div class="col-md-3 nop" style="margin-bottom: 10px !important">
            <asp:FileUpload id="FileUploadControl" accept="zpl" multiple="false" runat="server" />
        </div>
        <div class="col-md-9 nop" style="margin-bottom: 10px !important">
            <asp:Button runat="server" id="UploadButton" text="Upload" CssClass="inputBtn" onclick="UploadButton_Click" />
        </div>
        <br /><br />
    </div>
    <div class="row nop">
        <asp:Label runat="server" ID="StatusLabel" text="" />
    </div>
</div>
