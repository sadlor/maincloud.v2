﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using System;
using System.IO;

namespace MainCloud.Dashboard.Modules.Seneca.UpdateLabel
{
    public partial class UpdateLabel_View : DataWidget<UpdateLabel_View>
    {
        public UpdateLabel_View() : base(typeof(UpdateLabel_View)) { }
        public string ApplicationId
        {
            get
            {
                return (Page as PageEdit).GetApplicationId();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (FileUploadControl.HasFile && FileUploadControl.FileName == "output.zpl")
            {
                try
                {
                    string filename = Path.GetFileName(FileUploadControl.FileName);
                    FileUploadControl.SaveAs(Server.MapPath("~/HMI/PrintLabel/output.zpl"));
                    StatusLabel.Text = "Upload status: File uploaded!";
                }
                catch (Exception ex)
                {
                    StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                }
            }
        }
    }
}
