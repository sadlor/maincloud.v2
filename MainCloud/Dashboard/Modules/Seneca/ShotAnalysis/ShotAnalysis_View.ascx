﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShotAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.Seneca.ShotAnalysis.ShotAnalysis_View" %>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function PopUpShowing(sender, eventArgs) {
            var popUp = eventArgs.get_popUp();
            var scrollPosition = $('#<%= Page.Master.FindControl("ScrollPosition").ClientID %>').val();
            popUp.style.top = scrollPosition + "px";
        }
    </script>
</telerik:RadCodeBlock>

<style type="text/css">
    .RadPicker.RadDateTimePicker.RadPicker_Bootstrap.rfControl{
        width: 211px !important;
    }
</style>

<br />

<div class="panel panel-info">
    <%--<div class="panel-heading">Filtro selezione</div>--%>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="machineFilter" EmptyMessage="Seleziona" EnableClientFiltering="true" LabelWidth="87px"
                    TextSettings-SelectionMode="Single" DataSourceID="edsMachine" DataTextField="Code" DataValueField="Id" InputType="Text" Filter="Contains" Label="Macchina: ">
                </telerik:RadAutoCompleteBox>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickStart" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Inizio:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <telerik:RadDatePicker runat="server" ID="dtPickEnd" Width="100%">
                    <DateInput runat="server" LabelWidth="27%" Label="Fine:" DisplayDateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
            </div>
        </div>
        <div style="text-align:right">
            <asp:Button ID="btnConfirmSelection" runat="server" class="btn btn-primary" Text="Applica" OnClick="btnConfirmSelection_Click" />
        </div>
    </div>
</div>

<br />

<telerik:RadHtmlChart runat="server" ID="ProgressiveChart" Skin="Silk">
    <PlotArea>
        <Series>
            <telerik:ScatterLineSeries>
                <%--<LineAppearance Width="5" />--%>
                <TooltipsAppearance>
                    <ClientTemplate>
                        #= kendo.format(\'{0:HH:mm}\', new Date(value.x)) #,  #= value.y #
                    </ClientTemplate>
                </TooltipsAppearance>
            </telerik:ScatterLineSeries>
        </Series>
        <YAxis>
            <TitleAppearance Text="Qtà colpi" />
            <%--<MajorGridLines Visible="false" />--%>
            <%--<MinorGridLines Visible="false" />--%>
        </YAxis>
        <XAxis Type="Date">
            <%--<MajorGridLines Visible="false" />--%>
            <%--<MinorGridLines Visible="false" />--%>
        </XAxis>
    </PlotArea>
</telerik:RadHtmlChart>

<telerik:RadHtmlChart runat="server" ID="StepChart" Skin="Silk">
    <Legend>
        <Appearance Position="Bottom"></Appearance>
    </Legend>
    <PlotArea>
        <Series>
            <telerik:ScatterLineSeries>
                <TooltipsAppearance>
                    <ClientTemplate>
                        #= kendo.format(\'{0:HH:mm}\', new Date(value.x)) #,  #= value.y #
                    </ClientTemplate>
                </TooltipsAppearance>
            </telerik:ScatterLineSeries>
            <telerik:ScatterLineSeries>
                <TooltipsAppearance>
                    <ClientTemplate>
                        Cadenza oraria prevista = #= value.y #
                    </ClientTemplate>
                </TooltipsAppearance>
                <MarkersAppearance Size="0" />
                <LabelsAppearance Visible="false"></LabelsAppearance>
                <Appearance>
                    <FillStyle BackgroundColor="#e8481b" />  <%--#2dbbff--%>
                </Appearance>
            </telerik:ScatterLineSeries>
        </Series>
        <YAxis>
            <TitleAppearance Text="Produzione oraria">
                <TextStyle Margin="9" />
            </TitleAppearance>
        </YAxis>
        <XAxis Type="Date">
        </XAxis>
    </PlotArea>
</telerik:RadHtmlChart>

<br />
        
<asp:UpdatePanel runat="server">
    <ContentTemplate>
    
<telerik:RadGrid runat="server" ID="GridCounting" RenderMode="Lightweight" AutoGenerateColumns="false" AllowAutomaticUpdates="false" AllowPaging="true"
    OnItemDataBound="GridCounting_ItemDataBound" OnNeedDataSource="GridCounting_NeedDataSource"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" EditMode="PopUp">
        <Columns>
            <telerik:GridDateTimeColumn DataField="Start" HeaderText="Inizio" PickerType="DateTimePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="End" HeaderText="Fine" PickerType="DateTimePicker" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn DataField="Duration" HeaderText="Durata [m]">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Cause.Description" HeaderText="Causale">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridDropDownColumn UniqueName="ddlCause" HeaderText="Causale" DataField="CauseId"
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText=""
                ListTextField="Description" ListValueField="Id" DataSourceID="edsCause">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>--%>
            <%--<telerik:GridDropDownColumn UniqueName="ddlOperator" HeaderText="Operatore" DataField="OperatorId"
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText=""
                ListTextField="UserName" ListValueField="Id" DataSourceID="edsOperator">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>--%>
            <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="Operatore">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Job.Order.CustomerOrder.OrderCode" HeaderText="Commessa">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Job.Order.Article.Code" HeaderText="Articolo">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <%--<telerik:GridDropDownColumn UniqueName="ddlArticle" HeaderText="Articolo" DataField="Job.Order.IdArticle"
                DropDownControlType="RadComboBox" AllowSorting="true" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText=""
                ListTextField="Code" ListValueField="Id" DataSourceID="edsArticle">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDropDownColumn>--%>
            <telerik:GridBoundColumn DataField="PartialCounting" HeaderText="Parziale" DataFormatString="{0:N0}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CounterEnd" HeaderText="Progressivo" DataFormatString="{0:f0}" DataType="System.Int32">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="Media" HeaderText="Media">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ProductionWaste" HeaderText="Scarti">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="StandardRate" HeaderText="Produzione<br/>oraria">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <ClientEvents OnPopUpShowing="PopUpShowing" />
    </ClientSettings>
</telerik:RadGrid>

    </ContentTemplate>
</asp:UpdatePanel>

<ef:EntityDataSource ID="edsMachine" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets"
    AutoGenerateWhereClause="true" OrderBy="it.Description">
</ef:EntityDataSource>