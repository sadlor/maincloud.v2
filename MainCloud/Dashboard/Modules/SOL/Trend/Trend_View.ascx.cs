﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using SOLModule.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.SOL.Trend
{
    public partial class Trend_View : DataWidget<Trend_View>
    {
        public Trend_View() : base(typeof(Trend_View)) {}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack || DateFilter.RootGroup.IsEmpty)
            {
                var dateExpression = new RadFilterBetweenFilterExpression<DateTime>("Date");
                DateFilter.RootGroup.AddExpression(dateExpression);
            }
        }

        protected void DateFilter_ApplyExpressions(object sender, Telerik.Web.UI.RadFilterApplyExpressionsEventArgs e)
        {
            DateTime start = new DateTime();
            DateTime end = new DateTime();
            RadFilterExpressionsCollection exprList = e.ExpressionRoot.Expressions;
            if (exprList.Count == 0)
            {
                return;
            }
            if (exprList[0] is RadFilterEqualToFilterExpression<DateTime>)
            {
                var item = exprList[0] as RadFilterEqualToFilterExpression<DateTime>;
                start = item.Value;
                end = item.Value;
            }
            if (exprList[0] is RadFilterBetweenFilterExpression<DateTime>)
            {
                var item = exprList[0] as RadFilterBetweenFilterExpression<DateTime>;
                start = item.LeftValue;
                if (item.RightValue == DateTime.MinValue)
                {
                    end = item.LeftValue;
                }
                else
                {
                    end = item.RightValue;
                }
            }

            Createcharts(start, end);
        }

        protected void DateFilter_ExpressionItemCreated(object sender, RadFilterExpressionItemCreatedEventArgs e)
        {
            // Removes the AddGroupExpressionButton for group expression other than the root group
            RadFilterGroupExpressionItem groupItem = e.Item as RadFilterGroupExpressionItem;
            if (groupItem != null)
            {
                if (groupItem.IsRootGroup)
                {
                    groupItem.AddExpressionButton.Visible = false;
                    groupItem.AddGroupExpressionButton.Visible = false;
                    groupItem.RemoveButton.Visible = false;
                }
            }
        }

        protected void Createcharts(DateTime start, DateTime end)
        {
            end = end.AddDays(1);
            WeldService WS = new WeldService();

            List<TrendChart> dataSourceWeldTime = new List<TrendChart>();
            List<TrendChart> dataSourceWireLength = new List<TrendChart>();
            List<TrendChart> dataSourceGas = new List<TrendChart>();
            //List<TrendChart> dataSourceWarning = new List<TrendChart>();
            //List<TrendChart> dataSourceError = new List<TrendChart>();
            List<TrendChart> dataSourceEnergy = new List<TrendChart>();
            List<TrendChart> dataSourceWeldNumber = new List<TrendChart>();

            DataSet dataSourceWrngErr = new DataSet();
            DataTable dtWrngErr = new DataTable();
            dtWrngErr.Columns.Add("DateWrng", Type.GetType("System.DateTime"));
            dtWrngErr.Columns.Add("ParamWrng", Type.GetType("System.Object"));
            dtWrngErr.Columns.Add("DateErr", Type.GetType("System.DateTime"));
            dtWrngErr.Columns.Add("ParamErr", Type.GetType("System.Object"));

            TrendChart item = null;
            for (DateTime dayStart = start; dayStart < end; dayStart = dayStart.AddDays(1))
            {
                item = new TrendChart(dayStart, WS.WeldTime(dayStart, dayStart));
                if (item.Param != null)
                {
                    TimeSpan time = TimeSpan.FromSeconds((double)((decimal?)item.Param).Value);
                    item.Param = Math.Round(time.TotalHours, 2);
                    dataSourceWeldTime.Add(item);
                }

                item = new TrendChart(dayStart, WS.WireLength(dayStart, dayStart));
                if (item.Param != null)
                {
                    dataSourceWireLength.Add(item);
                }

                item = new TrendChart(dayStart, WS.ShieldingGas(dayStart, dayStart));
                if (item.Param != null)
                {
                    dataSourceGas.Add(item);
                }

                item = new TrendChart(dayStart, WS.WeldWarningsNum(dayStart, dayStart));
                if (item.Param != null && (int)item.Param != 0)
                {
                    dtWrngErr.Rows.Add(item.Date, item.Param, null, null);
                    //dataSourceWarning.Add(item);
                }
                item = new TrendChart(dayStart, WS.WeldErrorsNum(dayStart, dayStart));
                if (item.Param != null && (int)item.Param != 0)
                {
                    dtWrngErr.Rows.Add(null, null, item.Date, item.Param);
                    //dataSourceError.Add(item);
                }

                item = new TrendChart(dayStart, WS.Energy(dayStart, dayStart));
                if (item.Param != null)
                {
                    dataSourceEnergy.Add(item);
                }

                item = new TrendChart(dayStart, WS.WeldNumber(dayStart, dayStart));
                if (item.Param != null && (int)item.Param != 0)
                {
                    dataSourceWeldNumber.Add(item);
                }
            }

            ChartWeldTime.PlotArea.XAxis.MinDateValue = start;
            ChartWeldTime.PlotArea.XAxis.MaxDateValue = end;
            ChartWeldTime.DataSource = dataSourceWeldTime;
            ChartWeldTime.DataBind();

            ChartWireLength.PlotArea.XAxis.MinDateValue = start;
            ChartWireLength.PlotArea.XAxis.MaxDateValue = end;
            ChartWireLength.DataSource = dataSourceWireLength;
            ChartWireLength.DataBind();

            ChartGas.PlotArea.XAxis.MinDateValue = start;
            ChartGas.PlotArea.XAxis.MaxDateValue = end;
            ChartGas.DataSource = dataSourceGas;
            ChartGas.DataBind();

            dataSourceWrngErr.Tables.Add(dtWrngErr);
            //Dictionary<string, List<TrendChart>> dataSourceWrngErr = new Dictionary<string, List<TrendChart>>();
            //dataSourceWrngErr.Add("Warning", dataSourceWarning);
            //dataSourceWrngErr.Add("Error", dataSourceError);
            ChartWrngErr.PlotArea.XAxis.MinDateValue = start;
            ChartWrngErr.PlotArea.XAxis.MaxDateValue = end;
            ChartWrngErr.DataSource = dataSourceWrngErr;
            ChartWrngErr.DataBind();

            ChartEnergy.PlotArea.XAxis.MinDateValue = start;
            ChartEnergy.PlotArea.XAxis.MaxDateValue = end;
            ChartEnergy.DataSource = dataSourceEnergy;
            ChartEnergy.DataBind();

            ChartWeldNumber.PlotArea.XAxis.MinDateValue = start;
            ChartWeldNumber.PlotArea.XAxis.MaxDateValue = end;
            ChartWeldNumber.DataSource = dataSourceWeldNumber;
            ChartWeldNumber.DataBind();
        }
    }

    class TrendChart
    {
        public DateTime Date { get; set; }
        public object Param { get; set; }

        public TrendChart(DateTime date, object param)
        {
            Date = date;
            Param = param;
        }
    }
}