﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Trend_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.SOL.Trend.Trend_View" %>

<script type="text/javascript">
    function FilterCreated(sender, eventArgs) {
        var filterMenu = sender.get_contextMenu();
        filterMenu.add_showing(FilterMenuShowing);
    }

    function FilterMenuShowing(sender, eventArgs) {
        var filter = $find("<%= DateFilter.ClientID %>");
        var currentExpandedItem = sender.get_attributes()._data.ItemHierarchyIndex;
        sender.findItemByValue("And").set_visible(false);
        sender.findItemByValue("Or").set_visible(false);
        sender.findItemByValue("NotOr").set_visible(false);
        sender.findItemByValue("NotAnd").set_visible(false);
        if (filter._expressionItems[currentExpandedItem] == "Date") {
            sender.findItemByValue("IsNull").set_visible(false);
            sender.findItemByValue("NotIsNull").set_visible(false);
            sender.findItemByValue("IsEmpty").set_visible(false);
            sender.findItemByValue("NotIsEmpty").set_visible(false);
            sender.findItemByValue("Contains").set_visible(false);
            sender.findItemByValue("DoesNotContain").set_visible(false);
            sender.findItemByValue("GreaterThan").set_visible(false);
            sender.findItemByValue("GreaterThanOrEqualTo").set_visible(false);
            sender.findItemByValue("LessThan").set_visible(false);
            sender.findItemByValue("LessThanOrEqualTo").set_visible(false);
            sender.findItemByValue("NotBetween").set_visible(false);
            sender.findItemByValue("NotEqualTo").set_visible(false);
            sender.findItemByValue("StartsWith").set_visible(false);
            sender.findItemByValue("EndsWith").set_visible(false);
        }
    }

    window.onresize = function () {
        $find("<%=ChartWeldTime.ClientID%>").get_kendoWidget().resize();
        $find("<%=ChartWireLength.ClientID%>").get_kendoWidget().resize();
        $find("<%=ChartGas.ClientID%>").get_kendoWidget().resize();
        $find("<%=ChartWrngErr.ClientID%>").get_kendoWidget().resize();
        $find("<%=ChartEnergy.ClientID%>").get_kendoWidget().resize();
        $find("<%=ChartWeldNumber.ClientID%>").get_kendoWidget().resize();
    }
</script>

<style type="text/css">
    .MyFilterClass .rfTools {
        display: none;
    }

    .RadPicker.RadDateTimePicker.RadPicker_Bootstrap.rfControl{
        width: 211px !important;
    }
</style>

<telerik:RadFilter ID="DateFilter" runat="server" OperationMode="ServerAndClient" CssClass="MyFilterClass"
    OnApplyExpressions="DateFilter_ApplyExpressions" OnExpressionItemCreated="DateFilter_ExpressionItemCreated"
    LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <FieldEditors>
        <telerik:RadFilterDateFieldEditor FieldName="Date" DisplayName="Date" DefaultFilterFunction="Between" DateFormat="dd/MM/yyyy HH:mm:ss" PickerType="DateTimePicker" />
    </FieldEditors>
    <ClientSettings>
        <ClientEvents OnFilterCreated="FilterCreated" />
    </ClientSettings>
</telerik:RadFilter>

<br />

<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="DateFilter" EventName="ApplyExpressions" />
    </Triggers>
    <ContentTemplate>
        <div class="row" style="width:100%;">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <telerik:RadHtmlChart runat="server" ID="ChartWeldTime">
                    <ChartTitle Text="Weld Time [h]"></ChartTitle>
                    <PlotArea>
                        <Series>
                            <telerik:ScatterLineSeries MissingValues="Gap" DataFieldX="Date" DataFieldY="Param">
                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                <TooltipsAppearance>
                                    <ClientTemplate>
                                        #= kendo.format(\'{0:dd/MM/yyyy}\', new Date(value.x)) #,  #= value.y # h
                                    </ClientTemplate>
                                </TooltipsAppearance>
                            </telerik:ScatterLineSeries>
                        </Series>
                        <XAxis Type="Date" BaseUnit="Days">
                            <LabelsAppearance DataFormatString="{0:dd/MM/yyyy}" RotationAngle="-45" />
                        </XAxis>
                    </PlotArea>
                </telerik:RadHtmlChart>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <telerik:RadHtmlChart runat="server" ID="ChartWireLength">
                    <ChartTitle Text="Wire Length [m]"></ChartTitle>
                    <PlotArea>
                        <Series>
                            <telerik:ScatterLineSeries DataFieldX="Date" DataFieldY="Param">
                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                <TooltipsAppearance>
                                    <ClientTemplate>
                                        #= kendo.format(\'{0:dd/MM/yyyy}\', new Date(value.x)) #,  #= value.y #
                                    </ClientTemplate>
                                </TooltipsAppearance>
                            </telerik:ScatterLineSeries>
                        </Series>
                        <XAxis Type="Date" BaseUnit="Days">
                            <LabelsAppearance DataFormatString="{0:dd/MM/yyyy}" RotationAngle="-45" />
                        </XAxis>
                    </PlotArea>
                </telerik:RadHtmlChart>
            </div>
        </div>
        <br />
        <br />
        <div class="row" style="width:100%;">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <telerik:RadHtmlChart runat="server" ID="ChartGas">
                    <ChartTitle Text="Shielding Gas [L]"></ChartTitle>
                    <PlotArea>
                        <Series>
                            <telerik:ScatterLineSeries DataFieldX="Date" DataFieldY="Param">
                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                <TooltipsAppearance>
                                    <ClientTemplate>
                                        #= kendo.format(\'{0:dd/MM/yyyy}\', new Date(value.x)) #,  #= value.y # L
                                    </ClientTemplate>
                                </TooltipsAppearance>
                            </telerik:ScatterLineSeries>
                        </Series>
                        <XAxis Type="Date" BaseUnit="Days">
                            <LabelsAppearance DataFormatString="{0:dd/MM/yyyy}" RotationAngle="-45" />
                        </XAxis>
                    </PlotArea>
                </telerik:RadHtmlChart>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <telerik:RadHtmlChart runat="server" ID="ChartWrngErr">
                    <Legend>
                        <Appearance Position="Bottom"></Appearance>
                    </Legend>
                    <ChartTitle Text="Welds with error(s) or warning(s)"></ChartTitle>
                    <PlotArea>
                        <Series>
                            <telerik:ScatterLineSeries Name="Welds with warning(s)" DataFieldX="DateWrng" DataFieldY="ParamWrng">
                                <Appearance>
                                    <FillStyle BackgroundColor="Orange" />
                                </Appearance>
                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                <TooltipsAppearance>
                                    <ClientTemplate>
                                        #= kendo.format(\'{0:dd/MM/yyyy}\', new Date(value.x)) #,  #= value.y #
                                    </ClientTemplate>
                                </TooltipsAppearance>
                            </telerik:ScatterLineSeries>
                            <telerik:ScatterLineSeries Name="Welds with error(s)" DataFieldX="DateErr" DataFieldY="ParamErr">
                                <Appearance>
                                    <FillStyle BackgroundColor="Red" />
                                </Appearance>
                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                <TooltipsAppearance>
                                    <ClientTemplate>
                                        #= kendo.format(\'{0:dd/MM/yyyy}\', new Date(value.x)) #,  #= value.y #
                                    </ClientTemplate>
                                </TooltipsAppearance>
                            </telerik:ScatterLineSeries>
                        </Series>
                        <XAxis Type="Date" BaseUnit="Days">
                            <LabelsAppearance DataFormatString="{0:dd/MM/yyyy}" RotationAngle="-45" />
                        </XAxis>
                    </PlotArea>
                </telerik:RadHtmlChart>
            </div>
        </div>
        <br />
        <br />
        <div class="row" style="width:100%;">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <telerik:RadHtmlChart runat="server" ID="ChartEnergy">
                    <ChartTitle Text="Energy [kWh]"></ChartTitle>
                    <PlotArea>
                        <Series>
                            <telerik:ScatterLineSeries DataFieldX="Date" DataFieldY="Param">
                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                <TooltipsAppearance>
                                    <ClientTemplate>
                                        #= kendo.format(\'{0:dd/MM/yyyy}\', new Date(value.x)) #,  #= value.y # kWh
                                    </ClientTemplate>
                                </TooltipsAppearance>
                            </telerik:ScatterLineSeries>
                        </Series>
                        <XAxis Type="Date" BaseUnit="Days">
                            <LabelsAppearance DataFormatString="{0:dd/MM/yyyy}" RotationAngle="-45" />
                        </XAxis>
                    </PlotArea>
                </telerik:RadHtmlChart>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <telerik:RadHtmlChart runat="server" ID="ChartWeldNumber">
                    <ChartTitle Text="Number of welds"></ChartTitle>
                    <PlotArea>
                        <Series>
                            <telerik:ScatterLineSeries DataFieldX="Date" DataFieldY="Param">
                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                <TooltipsAppearance>
                                    <ClientTemplate>
                                        #= kendo.format(\'{0:dd/MM/yyyy}\', new Date(value.x)) #,  #= value.y #
                                    </ClientTemplate>
                                </TooltipsAppearance>
                            </telerik:ScatterLineSeries>
                        </Series>
                        <XAxis Type="Date" BaseUnit="Days">
                            <LabelsAppearance DataFormatString="{0:dd/MM/yyyy}" RotationAngle="-45" />
                        </XAxis>
                    </PlotArea>
                </telerik:RadHtmlChart>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>