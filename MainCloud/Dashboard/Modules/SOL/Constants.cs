﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.SOL
{
    public class Constants
    {
        public static string SETTINGS_DESTINATION_AREA_ID = "settings-destination-area-id";
        public const string JOB_ID = "JobId";
        public const string MACHINE_ID = "MachineId";
        public const string ERR_MESSAGE = "ERR";
        public const string WARNING_MESSAGE = "WARN";
        public const string OK_MESSAGE = "OK";
    }
}