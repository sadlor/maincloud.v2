﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.SOL.Summary
{
    public class SummaryDataSource
    {
        public SummaryDataSource()
        {}

        public string Description { get; set; }

        public TimeSpan JobTime { get; set; }
        public TimeSpan WeldTime { get; set; }
        public double PctWeldTime { get; set; }
        public TimeSpan StopTime { get; set; }
        public double PctStopTime { get; set; }
        public decimal WireLength { get; set; }
        public decimal GasAmount { get; set; }
        public double AverageDepRate { get; set; }
        public decimal Energy { get; set; }

        public int WeldNum { get; set; }
        public int WeldWarningNum { get; set; }
        public int WeldErrorNum { get; set; }
    }
}