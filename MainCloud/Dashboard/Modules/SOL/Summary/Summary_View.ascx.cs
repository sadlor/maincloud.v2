﻿using AssetManagement.Models;
using AssetManagement.Services;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using SOLModule.Models;
using SOLModule.Repositories;
using SOLModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.SOL.Summary
{
    public partial class Summary_View : DataWidget<Summary_View>
    {
        public Summary_View() : base(typeof(Summary_View)) { }

        List<SummaryPlant> TableDataSource { get; set; }

        const string GRID_SAVED_STATE = "GridSavedState";

        public override bool EnableExport
        {
            get
            {
                return true;
            }
        }

        public override void OnExport()
        {
            grid.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            grid.ExportSettings.IgnorePaging = false;
            grid.ExportSettings.ExportOnlyData = true;
            grid.ExportSettings.OpenInNewWindow = true;

            //grid.MasterTableView.EnableHierarchyExpandAll = true;
            grid.MasterTableView.HierarchyDefaultExpanded = true; // for the first level
            //grid.MasterTableView.DetailTables[0].HierarchyDefaultExpanded = true; // for the second level
            foreach (GridDataItem item in grid.MasterTableView.Items)
            {
                //if (!item.Expanded)
                //{
                    GridNestedViewItem nestedItem = (GridNestedViewItem)item.ChildItem;
                    RadGrid ChildGrid = (RadGrid)nestedItem.FindControl("gridDep");
                ChildGrid.MasterTableView.HierarchyDefaultExpanded = true;
                //}
            }

            //grid.Rebind();
            grid.MasterTableView.ExportToExcel();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // caricamento dati
            //SOLModule.Enermix.ParserCSV.ParserCSV p = new SOLModule.Enermix.ParserCSV.ParserCSV(@"C:\Users\Mattia.MYCROS\Downloads\CSV Enermix\weld_20170223_123902.csv");
            //SOLModule.Enermix.ParserCSV.ParserCSV p1 = new SOLModule.Enermix.ParserCSV.ParserCSV(@"C:\Users\Mattia.MYCROS\Downloads\CSV Enermix\weld_20170223_123910.csv");
            //SOLModule.Enermix.ParserCSV.ParserCSV p2 = new SOLModule.Enermix.ParserCSV.ParserCSV(@"C:\Users\Mattia.MYCROS\Downloads\CSV Enermix\weld_20170223_123916.csv");
            //SOLModule.Enermix.ParserCSV.ParserCSV p3 = new SOLModule.Enermix.ParserCSV.ParserCSV(@"C:\Users\Mattia.MYCROS\Downloads\CSV Enermix\weld_20170223_124315.csv");
            //SOLModule.Enermix.ParserCSV.ParserCSV p4 = new SOLModule.Enermix.ParserCSV.ParserCSV(@"C:\Users\Mattia.MYCROS\Downloads\CSV Enermix\weld_20170223_124325.csv");

            //using (WeldDbContext db = new WeldDbContext())
            //{
            //    DailyWeldJobService DWJS = new DailyWeldJobService();
            //    foreach (Job item in db.Jobs.ToList())
            //    {
            //        DWJS.StorageDailySummary(item.Id);
            //    }
            //}
            /////////////////////////////

            if (!IsPostBack || DateFilter.RootGroup.IsEmpty)
            {
                var dateExpression = new RadFilterBetweenFilterExpression<DateTime>("Date");
                DateFilter.RootGroup.AddExpression(dateExpression);
            }
        }

        protected void CreateDataSource(DateTime start, DateTime end)
        {
            List<SummaryPlant> summaryList = new List<SummaryPlant>();
            PlantService PS = new PlantService();
            DepartmentService DS = new DepartmentService();
            AssetService AS = new AssetService();

            foreach (Plant plant in PS.PlantList())
            {
                SummaryPlant p = new SummaryPlant();
                p.PlantId = plant.Id;
                p.Description = plant.Description;
                summaryList.Add(p);

                List<Department> depList = DS.DepartmentList().Where(x => x.PlantId == plant.Id).ToList();
                foreach (Department department in depList)
                {
                    SummaryDepartment d = new SummaryDepartment();
                    d.DepartmentId = department.Id;
                    d.Description = department.Description;
                    p.Departments.Add(d);

                    DailyWeldJobService DWJS = new DailyWeldJobService();
                    JobRepository rep = new JobRepository();
                    List<Asset> assetList = AS.GetAssetList().Where(x => x.DepartmentId == department.Id).ToList();
                    foreach (Asset asset in assetList)
                    {
                        SummaryAsset item = new SummaryAsset();
                        item.AssetId = asset.Id;
                        item.Description = asset.Description;
                        List<DailyWeldJob> list = DWJS.GetListByJobList(rep.GetJobListByAsset(asset.Id), start, end);
                        if (list.Count > 0)
                        {
                            item.AverageDepRate = list.Average(x => x.AverageDepRate);
                            item.Energy = list.Sum(x => x.Energy);
                            item.GasAmount = list.Sum(x => x.GasAmount);
                            item.WireLength = list.Sum(x => x.WireLength);
                            item.JobTime = new TimeSpan(list.Sum(x => x.JobTime.Ticks));
                            item.WeldTime = new TimeSpan(list.Sum(x => x.WeldTime.Ticks));
                            item.StopTime = new TimeSpan(list.Sum(x => x.StopTime.Ticks));
                            item.PctWeldTime = Math.Round(item.WeldTime.TotalSeconds / item.JobTime.TotalSeconds * 100, 2);
                            item.PctStopTime = Math.Round(item.StopTime.TotalSeconds / item.JobTime.TotalSeconds * 100, 2);
                            item.WeldNum = list.Sum(x => x.WeldNum);
                            item.WeldWarningNum = list.Sum(x => x.WeldWarningNum);
                            item.WeldErrorNum = list.Sum(x => x.WeldErrorNum);
                            d.Assets.Add(item);
                        }
                        else
                        {
                            item.AverageDepRate = 0;
                            item.Energy = 0;
                            item.GasAmount = 0;
                            item.WireLength = 0;
                            item.JobTime = new TimeSpan(0);
                            item.WeldTime = new TimeSpan(0);
                            item.StopTime = new TimeSpan(0);
                            item.PctWeldTime = 0;
                            item.PctStopTime = 0;
                            item.WeldNum = 0;
                            item.WeldWarningNum = 0;
                            item.WeldErrorNum = 0;
                            d.Assets.Add(item);
                        }
                    }
                    d.AverageDepRate = Math.Round(d.Assets.Average(x => x.AverageDepRate), 3);
                    d.Energy = d.Assets.Sum(x => x.Energy);
                    d.GasAmount = d.Assets.Sum(x => x.GasAmount);
                    d.WireLength = d.Assets.Sum(x => x.WireLength);
                    d.JobTime = new TimeSpan(d.Assets.Sum(x => x.JobTime.Ticks));
                    d.WeldTime = new TimeSpan(d.Assets.Sum(x => x.WeldTime.Ticks));
                    d.StopTime = new TimeSpan(d.Assets.Sum(x => x.StopTime.Ticks));
                    d.PctWeldTime = Math.Round(d.WeldTime.TotalSeconds / d.JobTime.TotalSeconds * 100, 2);
                    d.PctStopTime = Math.Round(d.StopTime.TotalSeconds / d.JobTime.TotalSeconds * 100, 2);
                    d.WeldNum = d.Assets.Sum(x => x.WeldNum);
                    d.WeldWarningNum = d.Assets.Sum(x => x.WeldWarningNum);
                    d.WeldErrorNum = d.Assets.Sum(x => x.WeldErrorNum);
                }
                p.AverageDepRate = Math.Round(p.Departments.Average(x => x.AverageDepRate), 3);
                p.Energy = p.Departments.Sum(x => x.Energy);
                p.GasAmount = p.Departments.Sum(x => x.GasAmount);
                p.WireLength = p.Departments.Sum(x => x.WireLength);
                p.JobTime = new TimeSpan(p.Departments.Sum(x => x.JobTime.Ticks));
                p.WeldTime = new TimeSpan(p.Departments.Sum(x => x.WeldTime.Ticks));
                p.StopTime = new TimeSpan(p.Departments.Sum(x => x.StopTime.Ticks));
                p.PctWeldTime = Math.Round(p.WeldTime.TotalSeconds / p.JobTime.TotalSeconds * 100, 2);
                p.PctStopTime = Math.Round(p.StopTime.TotalSeconds / p.JobTime.TotalSeconds * 100, 2);
                p.WeldNum = p.Departments.Sum(x => x.WeldNum);
                p.WeldWarningNum = p.Departments.Sum(x => x.WeldWarningNum);
                p.WeldErrorNum = p.Departments.Sum(x => x.WeldErrorNum);
            }

            TableDataSource = summaryList;
        }

        protected void grid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //PlantService PS = new PlantService();
            //grid.DataSource = PS.PlantList();
            if (TableDataSource == null && this.ViewState["FilterDateStart"] != null)
            {
                CreateDataSource((DateTime)this.ViewState["FilterDateStart"], (DateTime)this.ViewState["FilterDateEnd"]);
            }
            grid.DataSource = TableDataSource;
        }

        protected void gridDep_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //DepartmentService DS = new DepartmentService();
            string id = ((GridEditableItem)(((GridNestedViewItem)((sender as RadGrid).Parent.BindingContainer)).ParentItem)).KeyValues.Replace("{PlantId:\"", "").Replace("\"}", "");
            //(sender as RadGrid).DataSource = DS.DepartmentList().Where(x => x.PlantId == id).ToList();
            if (!string.IsNullOrEmpty(id))
            {
                if (TableDataSource == null && this.ViewState["FilterDateStart"] != null)
                {
                    CreateDataSource((DateTime)this.ViewState["FilterDateStart"], (DateTime)this.ViewState["FilterDateEnd"]);
                }
                (sender as RadGrid).DataSource = TableDataSource.Where(x => x.PlantId == id).First().Departments;
            }
        }

        protected void gridAsset_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            string id = ((GridEditableItem)(((GridNestedViewItem)((sender as RadGrid).Parent.BindingContainer)).ParentItem)).KeyValues.Replace("{DepartmentId:\"", "").Replace("\"}", "");
            if (!string.IsNullOrEmpty(id))
            {
                if (TableDataSource == null && this.ViewState["FilterDateStart"] != null)
                {
                    CreateDataSource((DateTime)this.ViewState["FilterDateStart"], (DateTime)this.ViewState["FilterDateEnd"]);
                }
                (sender as RadGrid).DataSource = TableDataSource.Where(p => p.Departments.Exists(d => d.DepartmentId == id)).First().Departments.Where(x => x.DepartmentId == id).First().Assets;
            }
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            {
                GridDataItem item = e.Item as GridDataItem;
                if (!item.Expanded)
                {
                    GridNestedViewItem nestedItem = (GridNestedViewItem)item.ChildItem;
                    //string dataKeyValue = Convert.ToString(((GridDataItem)(nestedItem.ParentItem)).GetDataKeyValue("PlantId"));
                    RadGrid tempGrid = (RadGrid)nestedItem.FindControl("gridDep");
                    tempGrid.Rebind();
                }

                this.SessionMultitenant[GRID_SAVED_STATE] = grid;
            }
        }

        protected void gridDep_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            {
                GridDataItem item = e.Item as GridDataItem;
                if (!item.Expanded)
                {
                    GridNestedViewItem nestedItem = (GridNestedViewItem)item.ChildItem;
                    //string dataKeyValue = Convert.ToString(((GridDataItem)(nestedItem.ParentItem)).GetDataKeyValue("DepartmentId"));
                    RadGrid tempGrid = (RadGrid)nestedItem.FindControl("gridAsset");
                    tempGrid.Rebind();
                }

                this.SessionMultitenant[GRID_SAVED_STATE] = grid;
            }
        }

        protected void DateFilter_ApplyExpressions(object sender, RadFilterApplyExpressionsEventArgs e)
        {
            DateTime start = new DateTime();
            DateTime end = new DateTime();
            //RadFilterDynamicLinqQueryProvider provider = new RadFilterDynamicLinqQueryProvider();
            //provider.ProcessGroup(e.ExpressionRoot);
            RadFilterExpressionsCollection exprList = e.ExpressionRoot.Expressions;
            if (exprList.Count == 0)
            {
                return;
            }
            if (exprList[0] is RadFilterEqualToFilterExpression<DateTime>)
            {
                var item = exprList[0] as RadFilterEqualToFilterExpression<DateTime>;
                start = item.Value;
                end = item.Value;
            }
            if (exprList[0] is RadFilterBetweenFilterExpression<DateTime>)
            {
                var item = exprList[0] as RadFilterBetweenFilterExpression<DateTime>;
                start = item.LeftValue;
                if (item.RightValue == DateTime.MinValue)
                {
                    end = item.LeftValue;
                }
                else
                {
                    end = item.RightValue;
                }
            }

            this.ViewState["FilterDateStart"] = start;
            this.ViewState["FilterDateEnd"] = end;
            CreateDataSource(start, end);
            grid.Rebind();
        }

        protected void DateFilter_ExpressionItemCreated(object sender, RadFilterExpressionItemCreatedEventArgs e)
        {
            // Removes the AddGroupExpressionButton for group expression other than the root group
            RadFilterGroupExpressionItem groupItem = e.Item as RadFilterGroupExpressionItem;
            if (groupItem != null)
            {
                if (groupItem.IsRootGroup)
                {
                    groupItem.AddExpressionButton.Visible = false;
                    groupItem.AddGroupExpressionButton.Visible = false;
                    groupItem.RemoveButton.Visible = false;
                }
            }
        }

        protected void gridAsset_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Redirect")
            {
                string machineId = (e.Item as GridDataItem).KeyValues.Split(':')[1].Replace("\"", "").Replace("}", "");
                SessionMultitenant[Constants.MACHINE_ID] = machineId;
                string destinationAreaId = CustomSettings.Find(Constants.SETTINGS_DESTINATION_AREA_ID) as string;
                if (!string.IsNullOrEmpty(destinationAreaId))
                {
                    MainCloudFramework.Repositories.WidgetAreaRepository areaService = new MainCloudFramework.Repositories.WidgetAreaRepository();
                    MainCloudFramework.Models.WidgetArea wa = areaService.FindByID(destinationAreaId);

                    Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
                }
            }
        }

        protected void grid_GridExporting(object sender, GridExportingArgs e)
        {
            string s = "<body>From " + ((DateTime)this.ViewState["FilterDateStart"]).ToString() + " To " + ((DateTime)this.ViewState["FilterDateEnd"]).ToString() + "\n";
            e.ExportOutput = e.ExportOutput.Replace("<body>", s);
        }
    }
}