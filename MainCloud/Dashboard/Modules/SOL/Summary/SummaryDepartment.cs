﻿using AssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.SOL.Summary
{
    public class SummaryDepartment : SummaryDataSource
    {
        public SummaryDepartment()
        {
            Assets = new List<SummaryAsset>();
        }

        public string DepartmentId { get; set; }
        public List<SummaryAsset> Assets { get; set; }
    }
}