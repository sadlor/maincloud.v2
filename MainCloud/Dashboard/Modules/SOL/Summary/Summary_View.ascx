﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Summary_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.SOL.Summary.Summary_View" %>

<script type="text/javascript">
    function FilterCreated(sender, eventArgs) {
        var filterMenu = sender.get_contextMenu();
        filterMenu.add_showing(FilterMenuShowing);
    }
    function FilterMenuShowing(sender, eventArgs) {
        var filter = $find("<%= DateFilter.ClientID %>");
        var currentExpandedItem = sender.get_attributes()._data.ItemHierarchyIndex;
        sender.findItemByValue("And").set_visible(false);
        sender.findItemByValue("Or").set_visible(false);
        sender.findItemByValue("NotOr").set_visible(false);
        sender.findItemByValue("NotAnd").set_visible(false);
        if (filter._expressionItems[currentExpandedItem] == "Date") {
            sender.findItemByValue("IsNull").set_visible(false);
            sender.findItemByValue("NotIsNull").set_visible(false);
            sender.findItemByValue("IsEmpty").set_visible(false);
            sender.findItemByValue("NotIsEmpty").set_visible(false);
            sender.findItemByValue("Contains").set_visible(false);
            sender.findItemByValue("DoesNotContain").set_visible(false);
            sender.findItemByValue("GreaterThan").set_visible(false);
            sender.findItemByValue("GreaterThanOrEqualTo").set_visible(false);
            sender.findItemByValue("LessThan").set_visible(false);
            sender.findItemByValue("LessThanOrEqualTo").set_visible(false);
            sender.findItemByValue("NotBetween").set_visible(false);
            sender.findItemByValue("NotEqualTo").set_visible(false);
            sender.findItemByValue("StartsWith").set_visible(false);
            sender.findItemByValue("EndsWith").set_visible(false);
        }
    }
</script>

<style type="text/css">
    .MyGridClass .rgDataDiv {
        height: auto !important;
        /*max-width: 1344px;*/
    }

    .MyFilterClass .rfTools {
        display: none;
    }

    .RadPicker.RadDateTimePicker.RadPicker_Bootstrap.rfControl{
        width: 211px !important;
    }
</style>

<%--<asp:UpdatePanel runat="server">
    <ContentTemplate>--%>
        <telerik:RadFilter ID="DateFilter" runat="server" OperationMode="ServerAndClient" CssClass="MyFilterClass"
            OnApplyExpressions="DateFilter_ApplyExpressions" OnExpressionItemCreated="DateFilter_ExpressionItemCreated"
            LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <FieldEditors>
                <telerik:RadFilterDateFieldEditor FieldName="Date" DisplayName="Date" DefaultFilterFunction="Between" DateFormat="dd/MM/yyyy HH:mm:ss" PickerType="DateTimePicker" />
            </FieldEditors>
            <ClientSettings>
                <ClientEvents OnFilterCreated="FilterCreated" />
            </ClientSettings>
        </telerik:RadFilter>
 <%--   </ContentTemplate>
</asp:UpdatePanel>--%>

<br />

<%--<asp:UpdatePanel runat="server">
    <ContentTemplate>--%>

        <telerik:RadGrid runat="server" ID="grid" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="false"
            OnNeedDataSource="grid_NeedDataSource" OnItemCommand="grid_ItemCommand" CssClass="MyGridClass" OnGridExporting="grid_GridExporting">
            <MasterTableView DataKeyNames="PlantId" InsertItemPageIndexAction="ShowItemOnCurrentPage" Caption="Plants"
                ShowHeadersWhenNoRecords="true" FilterExpression="" TableLayout="Fixed" RetainExpandStateOnRebind="true" HierarchyDefaultExpanded="true">
                <HeaderStyle BackColor="#428BCA" ForeColor="White" Font-Bold="true" />
                <ColumnGroups>
                    <telerik:GridColumnGroup Name="pTime" HeaderText="Time" HeaderStyle-HorizontalAlign="Center" />
                    <telerik:GridColumnGroup Name="pAmount" HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" />
                    <telerik:GridColumnGroup Name="pConsumables" HeaderText="Consumables" HeaderStyle-HorizontalAlign="Center" />
                </ColumnGroups>
                <Columns>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Plant">
                        <HeaderStyle HorizontalAlign="Center" Width="190px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="JobTime" HeaderText="ƩJobTime" ColumnGroupName="pTime" DataFormatString="{0:hh\:mm\:ss}">
                        <HeaderStyle HorizontalAlign="Center" Width="95px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="WeldTime" HeaderText="Weld Time" ColumnGroupName="pTime" DataFormatString="{0:hh\:mm\:ss}">
                        <HeaderStyle HorizontalAlign="Center" Width="95px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PctWeldTime" HeaderText="Weld Time (%)" ColumnGroupName="pTime">
                        <HeaderStyle HorizontalAlign="Center" Width="85px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="StopTime" HeaderText="Stop Time" ColumnGroupName="pTime" DataFormatString="{0:hh\:mm\:ss}">
                        <HeaderStyle HorizontalAlign="Center" Width="95px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PctStopTime" HeaderText="Stop Time (%)" ColumnGroupName="pTime">
                        <HeaderStyle HorizontalAlign="Center" Width="85px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="WeldNum" HeaderText="Weld Number" ColumnGroupName="pAmount">
                        <HeaderStyle HorizontalAlign="Center" Width="85px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="WeldWarningNum" HeaderText="Weld Warning Number" ColumnGroupName="pAmount">
                        <HeaderStyle HorizontalAlign="Center" Width="85px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="WeldErrorNum" HeaderText="Weld Error Number" ColumnGroupName="pAmount">
                        <HeaderStyle HorizontalAlign="Center" Width="85px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="WireLength" HeaderText="Wire Length [m]" ColumnGroupName="pConsumables">
                        <HeaderStyle HorizontalAlign="Center" Width="85px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="GasAmount" HeaderText="Gas Amount [l]" ColumnGroupName="pConsumables">
                        <HeaderStyle HorizontalAlign="Center" Width="85px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AverageDepRate" HeaderText="Average Dep Rate [kg/h]" ColumnGroupName="pConsumables">
                        <HeaderStyle HorizontalAlign="Center" Width="85px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Energy" HeaderText="Energy [kWh]" ColumnGroupName="pConsumables">
                        <HeaderStyle HorizontalAlign="Center" Width="85px" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
                <NestedViewSettings>
                    <ParentTableRelation>
                        <telerik:GridRelationFields DetailKeyField="DepartmentId" MasterKeyField="PlantId" />
                    </ParentTableRelation>
                </NestedViewSettings>
                <NestedViewTemplate>
                    <telerik:RadGrid runat="server" ID="gridDep" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="false"
                        OnNeedDataSource="gridDep_NeedDataSource" OnItemCommand="gridDep_ItemCommand">
                        <MasterTableView DataKeyNames="DepartmentId" FilterExpression="" Caption="" TableLayout="Fixed"
                            HierarchyDefaultExpanded="true" RetainExpandStateOnRebind="true">
                            <HeaderStyle BackColor="#fcf8e3" Font-Bold="true" />
                            <ColumnGroups>
                                <telerik:GridColumnGroup Name="dTime" HeaderText="Time" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup Name="dAmount" HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup Name="dConsumables" HeaderText="Consumables" HeaderStyle-HorizontalAlign="Center" />
                            </ColumnGroups>
                            <Columns>
                                <telerik:GridBoundColumn DataField="Description" HeaderText="Department">
                                    <HeaderStyle HorizontalAlign="Center" Width="148px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="JobTime" HeaderText="ƩJobTime" ColumnGroupName="dTime" DataFormatString="{0:hh\:mm\:ss}">
                                    <HeaderStyle HorizontalAlign="Center" Width="95px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="WeldTime" HeaderText="Weld Time" ColumnGroupName="dTime" DataFormatString="{0:hh\:mm\:ss}">
                                    <HeaderStyle HorizontalAlign="Center" Width="95px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PctWeldTime" HeaderText="Weld Time (%)" ColumnGroupName="dTime">
                                    <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="StopTime" HeaderText="Stop Time" ColumnGroupName="dTime" DataFormatString="{0:hh\:mm\:ss}">
                                    <HeaderStyle HorizontalAlign="Center" Width="95px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PctStopTime" HeaderText="Stop Time (%)" ColumnGroupName="dTime">
                                    <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="WeldNum" HeaderText="Weld Number" ColumnGroupName="dAmount">
                                    <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="WeldWarningNum" HeaderText="Weld Warning Number" ColumnGroupName="dAmount">
                                    <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="WeldErrorNum" HeaderText="Weld Error Number" ColumnGroupName="dAmount">
                                    <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="WireLength" HeaderText="Wire Length [m]" ColumnGroupName="dConsumables">
                                    <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="GasAmount" HeaderText="Gas Amount [l]" ColumnGroupName="dConsumables">
                                    <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AverageDepRate" HeaderText="Average Dep Rate [kg/h]" ColumnGroupName="dConsumables">
                                    <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Energy" HeaderText="Energy [kWh]" ColumnGroupName="dConsumables">
                                    <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                            </Columns>
                            <%--<NestedViewSettings>
                                <ParentTableRelation>
                                    <telerik:GridRelationFields DetailKeyField="Id" MasterKeyField="Id" />
                                </ParentTableRelation>
                            </NestedViewSettings>--%>
                            <NestedViewTemplate>
                                <telerik:RadGrid runat="server" ID="gridAsset" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="false"
                                    OnNeedDataSource="gridAsset_NeedDataSource" OnItemCommand="gridAsset_ItemCommand">
                                    <MasterTableView FilterExpression="" Caption="" TableLayout="Auto" RetainExpandStateOnRebind="true" DataKeyNames="AssetId">
                                        <ColumnGroups>
                                            <telerik:GridColumnGroup Name="aTime" HeaderText="Time" HeaderStyle-HorizontalAlign="Center" />
                                            <telerik:GridColumnGroup Name="aAmount" HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" />
                                            <telerik:GridColumnGroup Name="aConsumables" HeaderText="Consumables" HeaderStyle-HorizontalAlign="Center" />
                                        </ColumnGroups>
                                        <HeaderStyle Font-Bold="true" />
                                        <Columns>
                                            <telerik:GridButtonColumn DataTextField="Description" HeaderText="Asset" ButtonType="LinkButton" CommandName="Redirect">
                                                <HeaderStyle HorizontalAlign="Center" Width="147px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridButtonColumn>
                                            <telerik:GridBoundColumn DataField="JobTime" HeaderText="ƩJobTime" ColumnGroupName="aTime" DataFormatString="{0:hh\:mm\:ss}">
                                                <HeaderStyle HorizontalAlign="Center" Width="95px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="WeldTime" HeaderText="Weld Time" ColumnGroupName="aTime" DataFormatString="{0:hh\:mm\:ss}">
                                                <HeaderStyle HorizontalAlign="Center" Width="95px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PctWeldTime" HeaderText="Weld Time (%)" ColumnGroupName="aTime">
                                                <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="StopTime" HeaderText="Stop Time" ColumnGroupName="aTime" DataFormatString="{0:hh\:mm\:ss}">
                                                <HeaderStyle HorizontalAlign="Center" Width="95px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PctStopTime" HeaderText="Stop Time (%)" ColumnGroupName="aTime">
                                                <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="WeldNum" HeaderText="Weld Number" ColumnGroupName="aAmount">
                                                <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="WeldWarningNum" HeaderText="Weld Warning Number" ColumnGroupName="aAmount">
                                                <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="WeldErrorNum" HeaderText="Weld Error Number" ColumnGroupName="aAmount">
                                                <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="WireLength" HeaderText="Wire Length [m]" ColumnGroupName="aConsumables">
                                                <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="GasAmount" HeaderText="Gas Amount [l]" ColumnGroupName="aConsumables">
                                                <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="AverageDepRate" HeaderText="Average Dep Rate [kg/h]" ColumnGroupName="aConsumables">
                                                <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Energy" HeaderText="Energy [kWh]" ColumnGroupName="aConsumables">
                                                <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Resizing AllowColumnResize="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </NestedViewTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <Resizing AllowColumnResize="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </NestedViewTemplate>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="true" SaveScrollPosition="true" />
                <Resizing AllowColumnResize="true" />
            </ClientSettings>
        </telerik:RadGrid>

<%--    </ContentTemplate>
</asp:UpdatePanel>--%>