﻿using AssetManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.Dashboard.Modules.SOL.Summary
{
    public class SummaryPlant : SummaryDataSource
    {
        public SummaryPlant()
        {
            Departments = new List<SummaryDepartment>();
        }

        public string PlantId { get; set; }
        public List<SummaryDepartment> Departments { get; set; }
    }
}