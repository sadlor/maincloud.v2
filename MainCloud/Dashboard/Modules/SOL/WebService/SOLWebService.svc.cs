﻿using Newtonsoft.Json;
using SOLModule.Models;
using SOLModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace MainCloud.Dashboard.Modules.SOL.WebService
{
    // NOTE: In order to launch WCF Test Client for testing this service, please select SOLWebService.svc or SOLWebService.svc.cs at the Solution Explorer and start debugging.
    [ServiceContract]
    public class SOLWebService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("RollingLogFile");

        const string JOB_INFO = "JobInfo";
        const string WELD_LIST = "WeldList";

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            UriTemplate = "SentToCloud")]
        public string SentToCloud(string UId, string JobId, string JobName, string MachineName, string Address, string DataPayload, string ObjectType)
        {
            string senderIP = HttpContext.Current.Request.UserHostAddress;
            string senderDNS = HttpContext.Current.Request.UserHostName;
            string appId = null;
            string analyzerId;
            using (AssetManagement.Models.AssetManagementDbContext db = new AssetManagement.Models.AssetManagementDbContext())
            {
                try
                {
                    appId = db.Gateways.Where(x => x.NetworkAddress == senderIP || x.DNSName == senderDNS).First().ApplicationId;
                    analyzerId = db.Analyzers.Where(x => x.ApplicationId == appId && x.NetworkAddress == Address).First().Id;
                }
                catch (InvalidOperationException)
                {
                    if (string.IsNullOrEmpty(appId))
                    {
                        //gateway non registrato
                        log.Warn("SentToCloud -> Gateway non registrato, IP: " + senderIP);
                        return HttpStatusCode.Forbidden.ToString();
                    }
                    else
                    {
                        //enermix non registrato
                        log.Warn("SentToCloud -> Enermix non registrato, IP: " + Address);
                        return HttpStatusCode.Forbidden.ToString();
                        //return HttpStatusCode.PreconditionFailed.ToString();
                    }
                }
            }
            using (WeldDbContext db = new WeldDbContext())
            {
                if (ObjectType == JOB_INFO)
                {
                    //add to job if not exist
                    if (!db.Jobs.Where(x => x.JobId == JobId && x.JobName == JobName && x.MachineName == MachineName && x.ApplicationId == appId && x.AnalyzerId == analyzerId).Any())
                    {
                        AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
                        try
                        {
                            Job j = JsonConvert.DeserializeObject<Job>(DataPayload);
                            j.Id = UId;
                            j.ApplicationId = appId;
                            j.AnalyzerId = analyzerId;
                            using (AssetManagement.Models.AssetManagementDbContext dbAsset = new AssetManagement.Models.AssetManagementDbContext())
                            {
                                AssetManagement.Models.Asset asset = dbAsset.Assets.Where(x => x.Description == j.MachineName).FirstOrDefault();
                                if (asset != null)
                                {
                                    j.AssetId = asset.Id;
                                }
                            }
                            //TODO: aggiungere la macchina se non esiste
                            //if (string.IsNullOrEmpty(assetId))
                            //{
                            //    AssetManagement.Models.Asset asset = new AssetManagement.Models.Asset();
                            //    asset.ApplicationId = appId;
                            //    asset.AnalyzerId = analyzerId;
                            //    assetId = asset.Id;
                            //    AR.Insert(asset);
                            //}
                            Operator oper = db.Operators.Where(x => x.ApplicationId == appId && x.Name == j.Oper).FirstOrDefault();
                            if (oper == null)
                            {
                                oper = new Operator();
                                oper.ApplicationId = appId;
                                oper.Name = j.Oper;
                                db.Operators.Add(oper);
                                db.SaveChanges();
                            }
                            j.OperatorId = oper.Id;

                            if (j.WpsName != "NOT-SET")
                            {
                                WeldingProcedureSpecification wps = db.WeldingProcedureSpecifications.Where(x => x.ApplicationId == appId && x.Name == j.WpsName).FirstOrDefault();
                                if (wps == null)
                                {
                                    wps = new WeldingProcedureSpecification();
                                    wps.ApplicationId = appId;
                                    wps.Name = j.WpsName;
                                    db.WeldingProcedureSpecifications.Add(wps);
                                    db.SaveChanges();
                                }
                                j.WPSId = wps.Id;
                            }

                            db.Jobs.Add(j);
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error Enermix IP: " + Address + " - Error in saving Job to DB - Message: " + ex.Message);
                            return HttpStatusCode.Conflict.ToString();
                        }
                    }
                }
                if (ObjectType == WELD_LIST)
                {
                    //add to weld
                    List<Weld> list = JsonConvert.DeserializeObject<List<Weld>>(DataPayload);
                    Job job = db.Jobs.Where(x => x.JobId == JobId && x.JobName == JobName && x.MachineName == MachineName && x.ApplicationId == appId && x.AnalyzerId == analyzerId).First();
                    foreach (var item in list)
                    {
                        try
                        {
                            item.JobId = job.Id;
                            item.Job = job;
                            if (item.WeldTime.HasValue)
                            {
                                item.DateEnd = item.Date.AddSeconds((double)item.WeldTime.Value);
                            }
                            else
                            {
                                item.DateEnd = item.Date;
                            }
                            //TODO: add WPS

                            db.Welds.Add(item);
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error Enermix IP: " + Address + " - Error in saving Weld to DB - Message: " + ex.Message);
                            return HttpStatusCode.Conflict.ToString();
                        }
                    }

                    WeldSerieService WSS = new WeldSerieService();
                    WSS.CreateOrUpdateSerie(JobId, list);

                    DailyWeldJobService DWJS = new DailyWeldJobService();
                    foreach (DateTime day in list.Select(x => x.Date.Date).Distinct())
                    {
                        DWJS.StorageDailySummary(JobId, day);
                    }
                }
            }
            return HttpStatusCode.OK.ToString();
        }

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            UriTemplate = "GetAnalyzerList")]
        public string GetAnalyzerList()
        {
            string senderIP = HttpContext.Current.Request.UserHostAddress;
            string senderDNS = HttpContext.Current.Request.UserHostName;
            string appId = null;
            List<string> returnList = new List<string>();
            using (AssetManagement.Models.AssetManagementDbContext db = new AssetManagement.Models.AssetManagementDbContext())
            {
                try
                {
                    appId = db.Gateways.Where(x => x.NetworkAddress == senderIP || x.DNSName == senderDNS).First().ApplicationId;
                    string gatewayId = db.Gateways.Where(x => x.NetworkAddress == senderIP || x.DNSName == senderDNS).First().Id;
                    returnList = db.Analyzers.Where(x => x.ApplicationId == appId && x.GatewayId == gatewayId).Select(x => x.NetworkAddress).ToList();
                }
                catch (InvalidOperationException)
                {
                    //gateway non registrato
                    log.Info("Gateway non registrato, IP: " + senderIP);
                    return HttpStatusCode.Forbidden.ToString();
                }
                return JsonConvert.SerializeObject(returnList);
            }
        }
    }
}