﻿using AssetManagement.Repositories;
using MainCloud.Dashboard.Modules.SOL.EnermixAnalysis.CustomEditors;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using SOLModule.Models;
using SOLModule.Repositories;
using SOLModule.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.SOL.EnermixAnalysisNew
{
    public partial class EnermixAnalysisNew_View : DataWidget<EnermixAnalysisNew_View>
    {
        public EnermixAnalysisNew_View() : base(typeof(EnermixAnalysisNew_View)) { }

        public override bool EnableExport
        {
            get
            {
                return true;
            }
        }

        public override void OnExport()
        {
            DailyJobGrid.ExportSettings.Excel.Format = GridExcelExportFormat.Xlsx;
            DailyJobGrid.ExportSettings.IgnorePaging = true;
            DailyJobGrid.ExportSettings.ExportOnlyData = true;
            DailyJobGrid.ExportSettings.OpenInNewWindow = true;

            DailyJobGrid.MasterTableView.ExportToExcel();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack || JobFilter.RootGroup.IsEmpty)
            {
                JobGrid.Visible = false;
                DailyJobGrid.Visible = false;

                var machineExpression = new RadFilterEqualToFilterExpression<string>("Machine");
                AssetRepository AR = new AssetRepository();
                if (SessionMultitenant.ContainsKey(Constants.MACHINE_ID) && !string.IsNullOrEmpty((string)SessionMultitenant[Constants.MACHINE_ID]))
                {
                    AssetManagement.Models.Asset a = AR.FindByID((string)SessionMultitenant[Constants.MACHINE_ID]);
                    if (a != null)
                    {
                        machineExpression.Value = a.Id;
                    }
                }
                else
                {
                    AssetManagement.Models.Asset a = AR.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Description).FirstOrDefault();
                    if (a != null)
                    {
                        machineExpression.Value = a.Id;
                    }
                }
                JobFilter.RootGroup.AddExpression(machineExpression);

                var dateExpression = new RadFilterBetweenFilterExpression<DateTime>("Date");
                dateExpression.LeftValue = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 01);
                dateExpression.RightValue = DateTime.Today;
                JobFilter.RootGroup.AddExpression(dateExpression);
            }

            RadFilterExpression expr = JobFilter.RootGroupItem.Expression.Expressions.Where(x => (x as RadFilterEqualToFilterExpression<string>)?.FieldName == "Machine").FirstOrDefault();
            if (expr != null)
            {
                if ((expr as RadFilterEqualToFilterExpression<string>).Value != null)
                {
                    string machineId = (expr as RadFilterEqualToFilterExpression<string>).Value;

                    edsJob.WhereParameters.Clear();
                    edsJob.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
                    edsJob.WhereParameters.Add("AssetId", machineId);
                }
            }
            else
            {
                edsJob.WhereParameters.Clear();
                edsJob.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            }
            edsOperator.WhereParameters.Clear();
            edsOperator.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsMachine.WhereParameters.Clear();
            edsMachine.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsEquipment.WhereParameters.Clear();
            edsEquipment.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsWPS.WhereParameters.Clear();
            edsWPS.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        protected void JobGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            JobRepository repos = new JobRepository();
            List<Job> dataSource = new List<Job>();
            dataSource = repos.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.JobId).ToList();
            JobGrid.DataSource = dataSource;
        }

        protected void JobGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.SelectCommandName)
            {
                GridDataItem item = e.Item as GridDataItem;
                string jobId = item.KeyValues.Split(':')[1].Replace("\"", "").Replace("}", "");
                if (!string.IsNullOrEmpty(jobId))
                {
                    WeldRepository repos = new WeldRepository();
                    List<Weld> dataSource = new List<Weld>();
                    dataSource = repos.ReadAll(x => x.JobId == jobId).ToList();
                }
            }
            if (e.CommandName == "Redirect")
            {
                string jobId = (e.Item as GridDataItem).KeyValues.Split(':')[1].Replace("\"", "").Replace("}", "");
                SessionMultitenant[Constants.JOB_ID] = jobId;
                string destinationAreaId = CustomSettings.Find(Constants.SETTINGS_DESTINATION_AREA_ID) as string;
                if (!string.IsNullOrEmpty(destinationAreaId))
                {
                    MainCloudFramework.Repositories.WidgetAreaRepository areaService = new MainCloudFramework.Repositories.WidgetAreaRepository();
                    MainCloudFramework.Models.WidgetArea wa = areaService.FindByID(destinationAreaId);

                    Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
                }
            }
        }

        protected void JobFilter_ExpressionItemCreated(object sender, RadFilterExpressionItemCreatedEventArgs e)
        {
            //RadFilterSingleExpressionItem singleItem = e.Item as RadFilterSingleExpressionItem;
            //if (singleItem != null && singleItem.FieldName == "Operator" && singleItem.IsSingleValue)
            //{
            //    OperatorRepository opRep = new OperatorRepository();

            //    RadComboBox dropDownList = singleItem.InputControl as RadComboBox;
            //    dropDownList.DataSource = opRep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Name).ToList();
            //    dropDownList.DataBind();
            //}
            //if (singleItem != null && singleItem.FieldName == "Machine" && singleItem.IsSingleValue)
            //{
            //    AssetRepository assetRep = new AssetRepository();

            //    RadComboBox dropDownList = singleItem.InputControl as RadComboBox;
            //    dropDownList.DataSource = assetRep.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Description).ToList();
            //    dropDownList.DataBind();
            //}

            if (e.Item is RadFilterSingleExpressionItem)
            {
                RadFilterSingleExpressionItem item = e.Item as RadFilterSingleExpressionItem;
                if (item != null && (item.FieldName == "Machine" || item.FieldName == "Date"))
                {
                    item.RemoveButton.Visible = false;
                }
            }
            if (e.Item is RadFilterGroupExpressionItem)
            {
                RadFilterGroupExpressionItem groupItem = e.Item as RadFilterGroupExpressionItem;
                if (groupItem != null)
                {
                    if (groupItem.IsRootGroup)
                    {
                        groupItem.AddGroupExpressionButton.Visible = false;
                        groupItem.RemoveButton.Visible = false;
                    }
                }
            }
        }

        protected void JobFilter_ApplyExpressions(object sender, RadFilterApplyExpressionsEventArgs e)
        {
            //http://stackoverflow.com/questions/4044385/linq-how-to-query-if-a-value-is-between-a-list-of-ranges  da guardare

            DateTime? start = null;
            DateTime? end = null;
            string operatorId = string.Empty;
            string assetId = string.Empty;
            string wpsId = string.Empty;
            string equipmentId = string.Empty;
            List<string> jobIdList = new List<string>();

            RadFilterExpressionsCollection exprList = e.ExpressionRoot.Expressions;
            if (exprList.Count == 0)
            {
                return;
            }
            foreach (var expr in exprList)
            {
                if (expr is RadFilterEqualToFilterExpression<string>)
                {
                    var item = expr as RadFilterEqualToFilterExpression<string>;
                    switch (item.FieldName)
                    {
                        case "Machine":
                            assetId = item.Value;
                            break;
                        case "Operator":
                            operatorId = item.Value;
                            break;
                        case "WPS":
                            wpsId = item.Value;
                            break;
                        case "Equipment":
                            equipmentId = item.Value;
                            break;
                        case "Job":
                            jobIdList.Add(item.Value);
                            break;
                    }
                }
                if (expr is RadFilterEqualToFilterExpression<DateTime>)
                {
                    var item = expr as RadFilterEqualToFilterExpression<DateTime>;
                    start = item.Value;
                    end = item.Value.AddDays(1);
                }
                if (expr is RadFilterBetweenFilterExpression<DateTime>)
                {
                    var item = expr as RadFilterBetweenFilterExpression<DateTime>;
                    start = item.LeftValue;
                    if (item.RightValue == DateTime.MinValue)
                    {
                        end = item.LeftValue.AddDays(1);
                    }
                    else
                    {
                        end = item.RightValue.AddDays(1);
                    }
                }
            }

            DailyWeldJobRepository repos = new DailyWeldJobRepository();
            List<DailyWeldJob> dailyJobList = new List<DailyWeldJob>();
            if (!string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(assetId) &&
                start != null && end != null && !string.IsNullOrEmpty(wpsId) && !string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
            {
                dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                  x.Job.OperatorId == operatorId &&
                                                  x.Job.AssetId == assetId &&
                                                  x.Job.WPSId == wpsId &&
                                                  x.Job.EquipmentId == equipmentId &&
                                                  x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                goto StartFilter;
            }
            else
            {
                if (start != null && end != null)
                {
                    if (string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                    {
                        //only Date
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(operatorId) && jobIdList.Count > 0 && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId))
                    {
                        //Operator and Asset and Job(s)
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.AssetId == assetId &&
                                                          x.Job.OperatorId == operatorId &&
                                                          jobIdList.Contains(x.JobId) &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(assetId) && jobIdList.Count > 0 && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(equipmentId))
                    {
                        //Asset and Job(s) and WPS
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.AssetId == assetId &&
                                                          jobIdList.Contains(x.JobId) &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(assetId) && jobIdList.Count > 0 && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(operatorId))
                    {
                        //Asset and Job(s) and Equipment
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.AssetId == assetId &&
                                                          jobIdList.Contains(x.JobId) &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (jobIdList.Count > 0 && !string.IsNullOrEmpty(wpsId) && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId))
                    {
                        //Job(s) and WPS and Equipment
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          jobIdList.Contains(x.JobId) &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(operatorId) && jobIdList.Count > 0 && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId))
                    {
                        //Job(s) and Operator and Equipment
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          jobIdList.Contains(x.JobId) &&
                                                          x.Job.OperatorId == operatorId &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(operatorId) && jobIdList.Count > 0 && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(assetId))
                    {
                        //Job(s) and Operator and WPS
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          jobIdList.Contains(x.JobId) &&
                                                          x.Job.OperatorId == operatorId &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                    {
                        //Operator and Asset and WPS
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.AssetId == assetId &&
                                                          x.Job.OperatorId == operatorId &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && jobIdList.Count == 0)
                    {
                        //Operator and Asset and Equipment
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.AssetId == assetId &&
                                                          x.Job.OperatorId == operatorId &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(equipmentId) && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && jobIdList.Count == 0)
                    {
                        //Operator and WPS and Equipment
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.OperatorId == operatorId &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(equipmentId) && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(operatorId) && jobIdList.Count == 0)
                    {
                        //Asset and WPS and Equipment
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.AssetId == assetId &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                    {
                        //Operator and Asset
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.AssetId == assetId &&
                                                          x.Job.OperatorId == operatorId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(operatorId) && jobIdList.Count > 0 && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId))
                    {
                        //Operator and Job(s)
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.OperatorId == operatorId &&
                                                          jobIdList.Contains(x.JobId) &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                    {
                        // Operator and WPS
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.OperatorId == operatorId &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && jobIdList.Count == 0)
                    {
                        // Operator and Equipment
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.OperatorId == operatorId &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(assetId) && jobIdList.Count > 0 && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId))
                    {
                        //Asset and Job(s)
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.AssetId == assetId &&
                                                          jobIdList.Contains(x.JobId) &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                    {
                        //Asset and WPS
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.AssetId == assetId &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(operatorId) && jobIdList.Count == 0)
                    {
                        //Asset and Equipment
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.AssetId == assetId &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(wpsId) && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId) && jobIdList.Count == 0)
                    {
                        //WPS and Equipment
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (jobIdList.Count > 0 && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId))
                    {
                        //Job(s) and Equipment
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          jobIdList.Contains(x.JobId) &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (jobIdList.Count > 0 && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId))
                    {
                        //Job(s) and WPS
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          jobIdList.Contains(x.JobId) &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (jobIdList.Count > 0 && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId))
                    {
                        //Job only
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          jobIdList.Contains(x.JobId) &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                    {
                        //Operator only
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.OperatorId == operatorId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                    {
                        //Asset only
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.AssetId == assetId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                    {
                        //WPS only
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    if (!string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId) && jobIdList.Count == 0)
                    {
                        //Equipment only
                        dailyJobList = repos.ReadAll(x => x.Date >= start.Value && x.Date < end.Value &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                }
                else
                {
                    // if date not selected
                    if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(wpsId) && !string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                    {
                        dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                          x.Job.OperatorId == operatorId &&
                                                          x.Job.WPSId == wpsId &&
                                                          x.Job.EquipmentId == equipmentId &&
                                                          x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                        goto StartFilter;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(operatorId) && jobIdList.Count > 0 && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId))
                        {
                            //Operator and Asset and Job(s)
                            dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                              x.Job.OperatorId == operatorId &&
                                                              jobIdList.Contains(x.JobId) &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(assetId) && jobIdList.Count > 0 && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(equipmentId))
                        {
                            //Asset and Job(s) and WPS
                            dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                              jobIdList.Contains(x.JobId) &&
                                                              x.Job.WPSId == wpsId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(assetId) && jobIdList.Count > 0 && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(operatorId))
                        {
                            //Asset and Job(s) and Equipment
                            dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                              jobIdList.Contains(x.JobId) &&
                                                              x.Job.EquipmentId == equipmentId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (jobIdList.Count > 0 && !string.IsNullOrEmpty(wpsId) && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId))
                        {
                            //Job(s) and WPS and Equipment
                            dailyJobList = repos.ReadAll(x => jobIdList.Contains(x.JobId) &&
                                                              x.Job.WPSId == wpsId &&
                                                              x.Job.EquipmentId == equipmentId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(operatorId) && jobIdList.Count > 0 && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId))
                        {
                            //Job(s) and Operator and Equipment
                            dailyJobList = repos.ReadAll(x => jobIdList.Contains(x.JobId) &&
                                                              x.Job.OperatorId == operatorId &&
                                                              x.Job.EquipmentId == equipmentId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(operatorId) && jobIdList.Count > 0 && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(assetId))
                        {
                            //Job(s) and Operator and WPS
                            dailyJobList = repos.ReadAll(x => jobIdList.Contains(x.JobId) &&
                                                              x.Job.OperatorId == operatorId &&
                                                              x.Job.WPSId == wpsId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                        {
                            //Operator and Asset and WPS
                            dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                              x.Job.OperatorId == operatorId &&
                                                              x.Job.WPSId == wpsId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && jobIdList.Count == 0)
                        {
                            //Operator and Asset and Equipment
                            dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                              x.Job.OperatorId == operatorId &&
                                                              x.Job.EquipmentId == equipmentId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(equipmentId) && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && jobIdList.Count == 0)
                        {
                            //Operator and WPS and Equipment
                            dailyJobList = repos.ReadAll(x => x.Job.OperatorId == operatorId &&
                                                              x.Job.WPSId == wpsId &&
                                                              x.Job.EquipmentId == equipmentId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(equipmentId) && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(operatorId) && jobIdList.Count == 0)
                        {
                            //Asset and WPS and Equipment
                            dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                              x.Job.WPSId == wpsId &&
                                                              x.Job.EquipmentId == equipmentId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                        {
                            //Operator and Asset
                            dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                              x.Job.OperatorId == operatorId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(operatorId) && jobIdList.Count > 0 && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId))
                        {
                            //Operator and Job(s)
                            dailyJobList = repos.ReadAll(x => x.Job.OperatorId == operatorId &&
                                                              jobIdList.Contains(x.JobId) &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                        {
                            //Operator and WPS
                            dailyJobList = repos.ReadAll(x => x.Job.OperatorId == operatorId &&
                                                              x.Job.WPSId == wpsId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && jobIdList.Count == 0)
                        {
                            //Operator and Equipment
                            dailyJobList = repos.ReadAll(x => x.Job.OperatorId == operatorId &&
                                                              x.Job.EquipmentId == equipmentId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(assetId) && jobIdList.Count > 0 && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId))
                        {
                            //Asset and Job(s)
                            dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                              jobIdList.Contains(x.JobId) &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId) && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                        {
                            //Asset and WPS
                            dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                              x.Job.WPSId == wpsId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(assetId) && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(wpsId) && jobIdList.Count == 0)
                        {
                            //Asset and Equipment
                            dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                              x.Job.EquipmentId == equipmentId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(wpsId) && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId) && jobIdList.Count == 0)
                        {
                            //WPS and Equipment
                            dailyJobList = repos.ReadAll(x => x.Job.WPSId == wpsId &&
                                                              x.Job.EquipmentId == equipmentId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (jobIdList.Count > 0 && !string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId))
                        {
                            //Job(s) and Equipment
                            dailyJobList = repos.ReadAll(x => jobIdList.Contains(x.JobId) &&
                                                              x.Job.EquipmentId == equipmentId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (jobIdList.Count > 0 && !string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId))
                        {
                            //Job(s) and WPS
                            dailyJobList = repos.ReadAll(x => jobIdList.Contains(x.JobId) &&
                                                              x.Job.WPSId == wpsId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (jobIdList.Count > 0 && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId))
                        {
                            //Job only
                            dailyJobList = repos.ReadAll(x => jobIdList.Contains(x.JobId) &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                        {
                            //Operator only
                            dailyJobList = repos.ReadAll(x => x.Job.OperatorId == operatorId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                        {
                            //Asset only
                            dailyJobList = repos.ReadAll(x => x.Job.AssetId == assetId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(operatorId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                        {
                            //WPS only
                            dailyJobList = repos.ReadAll(x => x.Job.WPSId == wpsId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                        if (!string.IsNullOrEmpty(equipmentId) && string.IsNullOrEmpty(wpsId) && string.IsNullOrEmpty(assetId) && string.IsNullOrEmpty(equipmentId) && jobIdList.Count == 0)
                        {
                            //Equipment only
                            dailyJobList = repos.ReadAll(x => x.Job.EquipmentId == equipmentId &&
                                                              x.Job.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
                            goto StartFilter;
                        }
                    }
                }
            }

            StartFilter:
            List<DailyWeldJob> dailyDataSource = new List<DailyWeldJob>();
            JobRepository jobRep = new JobRepository();
            foreach (string jobId in dailyJobList.Select(x => x.JobId).Distinct())
            {
                DailyWeldJob item = new DailyWeldJob();
                item.JobId = jobId;
                item.Job = jobRep.FindByID(jobId);
                item.JobTime = new TimeSpan(dailyJobList.Where(x => x.JobId == jobId).Sum(x => x.JobTime.Ticks));
                item.WeldTime = new TimeSpan(dailyJobList.Where(x => x.JobId == jobId).Sum(x => x.WeldTime.Ticks));
                item.StopTime = new TimeSpan(dailyJobList.Where(x => x.JobId == jobId).Sum(x => x.StopTime.Ticks));
                item.PctWeldTime = Math.Round(item.WeldTime.TotalSeconds / item.JobTime.TotalSeconds * 100, 2);
                item.PctStopTime = Math.Round(item.StopTime.TotalSeconds / item.JobTime.TotalSeconds * 100, 2);
                item.AverageDepRate = dailyJobList.Where(x => x.JobId == jobId).Average(x => x.AverageDepRate);
                item.Energy = dailyJobList.Where(x => x.JobId == jobId).Sum(x => x.Energy);
                item.GasAmount = dailyJobList.Where(x => x.JobId == jobId).Sum(x => x.GasAmount);
                item.WireLength = dailyJobList.Where(x => x.JobId == jobId).Sum(x => x.WireLength);
                item.WeldNum = dailyJobList.Where(x => x.JobId == jobId).Sum(x => x.WeldNum);
                item.WeldWarningNum = dailyJobList.Where(x => x.JobId == jobId).Sum(x => x.WeldWarningNum);
                item.WeldErrorNum = dailyJobList.Where(x => x.JobId == jobId).Sum(x => x.WeldErrorNum);
                dailyDataSource.Add(item);
            }
            List<Job> jobDataSource = dailyDataSource.Select(x => x.Job).ToList();

            Analisi(jobDataSource, start.Value, end.Value);
            CreateChart(jobDataSource);

            JobGrid.Visible = true;
            JobGrid.DataSource = jobDataSource;
            JobGrid.DataBind();
            DailyJobGrid.Visible = true;
            DailyJobGrid.DataSource = dailyDataSource;
            DailyJobGrid.DataBind();
        }

        protected void DailyJobGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Redirect")
            {
                string jobId = (e.Item as GridDataItem).KeyValues.Split(':')[1].Replace("\"", "").Replace("}", "");
                SessionMultitenant[Constants.JOB_ID] = jobId;
                string destinationAreaId = CustomSettings.Find(Constants.SETTINGS_DESTINATION_AREA_ID) as string;
                if (!string.IsNullOrEmpty(destinationAreaId))
                {
                    MainCloudFramework.Repositories.WidgetAreaRepository areaService = new MainCloudFramework.Repositories.WidgetAreaRepository();
                    MainCloudFramework.Models.WidgetArea wa = areaService.FindByID(destinationAreaId);

                    Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
                }
            }
        }

        protected void JobFilter_ItemCommand(object sender, RadFilterCommandEventArgs e)
        {
            if (e.CommandName == RadFilter.AddExpressionCommandName || e.CommandName == RadFilter.ApplyCommandName)
            {
                RadFilterExpression expr = JobFilter.RootGroupItem.Expression.Expressions.Where(x => (x as RadFilterEqualToFilterExpression<string>)?.FieldName == "Machine").FirstOrDefault();
                if (expr != null)
                {
                    if ((expr as RadFilterEqualToFilterExpression<string>).Value != null)
                    {
                        string machineId = (expr as RadFilterEqualToFilterExpression<string>).Value;

                        edsJob.WhereParameters.Clear();
                        edsJob.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
                        edsJob.WhereParameters.Add("AssetId", machineId);
                    }
                }
            }
        }

        protected void JobFilter_FieldEditorCreating(object sender, RadFilterFieldEditorCreatingEventArgs e)
        {
            if (e.EditorType == "RadCustomFilterDropDownEditor")
            {
                e.Editor = new RadCustomFilterDropDownEditor();
            }
        }

        public void Analisi(List<Job> jobList, DateTime start, DateTime end)
        {
            txtJobTotTime.Text = "";
            txtJobWeldTime.Text = "";
            txtPercWeldTime.Text = "";
            txtJobStopTime.Text = "";
            txtPercStopTime.Text = "";
            txtWireLength.Text = "";
            txtAverageDepRate.Text = "";
            txtEnergy.Text = "";
            txtGasAmount.Text = "";
            txtWeldNum.Text = "";
            txtWeldWarning.Text = "";
            txtWeldError.Text = "";
            txtWeldClear.Text = "";

            WeldSerieRepository repos = new WeldSerieRepository();
            var jobIdList = jobList.Select(j => j.Id);
            List<WeldSerie> dataSource = repos.ReadAll(x => jobIdList.Contains(x.JobId)).ToList();
            WeldSerieService WSS = new WeldSerieService();

            if (dataSource.Count > 0)
            {
                #region Time
                TimeSpan totTime = WSS.TotalTime(dataSource).Value;
                txtJobTotTime.Text = string.Format("{0}:{1}:{2}", (int)totTime.TotalHours, totTime.Minutes, totTime.Seconds);
                TimeSpan weldTime = WSS.WeldTime(dataSource).Value;
                txtJobWeldTime.Text = string.Format("{0}:{1}:{2}", (int)weldTime.TotalHours, weldTime.Minutes, weldTime.Seconds);
                double percWeldTime = Math.Round(weldTime.TotalSeconds / totTime.TotalSeconds * 100, 2);
                txtPercWeldTime.Text = percWeldTime.ToString() + " %";
                TimeSpan stopTime = totTime - weldTime;
                txtJobStopTime.Text = string.Format("{0}:{1}:{2}", (int)stopTime.TotalHours, stopTime.Minutes, stopTime.Seconds);
                double percStopTime = Math.Round(stopTime.TotalSeconds / totTime.TotalSeconds * 100, 2);
                txtPercStopTime.Text = percStopTime.ToString() + " %";

                PieSeriesItem pieItem = new PieSeriesItem((decimal)percWeldTime);
                pieItem.Name = "WeldTime";
                (Chart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(pieItem);
                pieItem = new PieSeriesItem((decimal)percStopTime);
                pieItem.Name = "StopTime";
                (Chart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(pieItem);
                #endregion

                decimal wireLength = WSS.WireLength(dataSource);
                txtWireLength.Text = wireLength.ToString();
                //txtAverageSpeed.Text = Math.Round((dataSource.Sum(x => x.WeldLen) / dataSource.Sum(x => x.WeldTime).Value).Value, 2).ToString();
                txtEnergy.Text = WSS.Energy(dataSource).ToString();
                decimal gasAmount = WSS.ShieldingGas(dataSource);
                txtGasAmount.Text = gasAmount.ToString();

                var avgDepRate = WSS.AverageDepRate(dataSource);
                txtAverageDepRate.Text = avgDepRate.ToString();

                int weldNum = WSS.WeldNumber(dataSource);
                int weldWarningNum = WSS.WeldWarningsNum(dataSource);
                int weldErrorNum = WSS.WeldErrorsNum(dataSource);
                txtWeldNum.Text = weldNum.ToString();
                txtWeldWarning.Text = weldWarningNum.ToString();
                txtWeldError.Text = weldErrorNum.ToString();
                txtWeldClear.Text = WSS.WeldClearNum(dataSource).ToString();
            }
        }

        public void CreateChart(List<Job> jobList)
        {
            DailyWeldJobRepository repos = new DailyWeldJobRepository();
            var jobIdList = jobList.Select(j => j.Id);
            List<DailyWeldJob> dataSource = repos.ReadAll(x => jobIdList.Contains(x.JobId)).OrderBy(x => x.Date).ToList();

            if (dataSource.Count > 0)
            {
                DataSet dataSourceChartSerie = new DataSet();
                DataTable dtChartSerie = new DataTable();
                dtChartSerie.Columns.Add("Date", Type.GetType("System.DateTime"));
                dtChartSerie.Columns.Add("NumJob", Type.GetType("System.Int32"));
                dtChartSerie.Columns.Add("WeldTime", Type.GetType("System.Double"));

                foreach (DateTime day in dataSource.Select(x => x.Date).Distinct())
                {
                    dtChartSerie.Rows.Add(day, dataSource.Where(x => x.Date == day).Count(), Math.Round(dataSource.Where(x => x.Date == day).Aggregate(TimeSpan.Zero, (subtotal, x) => subtotal.Add(x.WeldTime)).TotalHours, 2));
                }

                dataSourceChartSerie.Tables.Add(dtChartSerie);
                ChartJobWeldTime.PlotArea.XAxis.MinDateValue = dataSource.Min(x => x.Date);
                ChartJobWeldTime.PlotArea.XAxis.MaxDateValue = dataSource.Max(x => x.Date);
                ChartJobWeldTime.DataSource = dataSourceChartSerie;
                ChartJobWeldTime.DataBind();
            }
        }
    }
}