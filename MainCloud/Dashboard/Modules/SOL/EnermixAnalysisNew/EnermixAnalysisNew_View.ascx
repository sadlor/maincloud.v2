﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EnermixAnalysisNew_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.SOL.EnermixAnalysisNew.EnermixAnalysisNew_View" %>
<%--<%@ Register Namespace="MainCloud.Dashboard.Modules.SOL.EnermixAnalysis.CustomEditors" TagPrefix="custom" %>--%>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function FilterCreated(sender, eventArgs) {
            var filterMenu = sender.get_contextMenu();
            filterMenu.add_showing(FilterMenuShowing);
        }
        function FilterMenuShowing(sender, eventArgs) {
            var filter = $find("<%= JobFilter.ClientID %>");
            var currentExpandedItem = sender.get_attributes()._data.ItemHierarchyIndex;

            sender.findItemByValue("And").set_visible(false);
            sender.findItemByValue("Or").set_visible(false);
            sender.findItemByValue("NotOr").set_visible(false);
            sender.findItemByValue("NotAnd").set_visible(false);

            sender.findItemByValue("IsNull").set_visible(false);
            sender.findItemByValue("NotIsNull").set_visible(false);
            sender.findItemByValue("IsEmpty").set_visible(false);
            sender.findItemByValue("NotIsEmpty").set_visible(false);
            sender.findItemByValue("Contains").set_visible(false);
            sender.findItemByValue("DoesNotContain").set_visible(false);
            sender.findItemByValue("GreaterThan").set_visible(false);
            sender.findItemByValue("GreaterThanOrEqualTo").set_visible(false);
            sender.findItemByValue("LessThan").set_visible(false);
            sender.findItemByValue("LessThanOrEqualTo").set_visible(false);
            sender.findItemByValue("NotEqualTo").set_visible(false);
            sender.findItemByValue("EqualTo").set_visible(false);
            sender.findItemByValue("StartsWith").set_visible(false);
            sender.findItemByValue("EndsWith").set_visible(false);
            sender.findItemByValue("Between").set_visible(false);
            sender.findItemByValue("NotBetween").set_visible(false);
            //if (filter._expressionItems[currentExpandedItem] == "Machine")
            //{
            //    sender.findItemByValue("Between").set_visible(true);
            //    sender.findItemByValue("EqualTo").set_visible(true);
            //}
            sender.findItemByValue("Machine").set_visible(false);
            sender.findItemByValue("Date").set_visible(false);
        }

        window.onresize = function () {
        $find("<%=ChartJobWeldTime.ClientID%>").get_kendoWidget().resize();
    }
    </script>
</telerik:RadCodeBlock>

<style type="text/css">
    .RadPicker.RadDateTimePicker.RadPicker_Bootstrap.rfControl{
        width: 211px !important;
    }

    .formRow {
        display: inline-block;
        *zoom: 1;
        *display: inline;
        vertical-align: top;
        height: 270px;
        padding-left: 16px;
    }

    .demo-container fieldset {
        display: inline-block;
        *zoom: 1;
        *display: inline;
        vertical-align: top;
        margin-top: 20px;
    }
</style>

<telerik:RadFilter RenderMode="Lightweight" runat="server" ID="JobFilter" ExpressionPreviewPosition="None" OperationMode="ServerAndClient"
    OnApplyExpressions="JobFilter_ApplyExpressions" OnExpressionItemCreated="JobFilter_ExpressionItemCreated" OnItemCommand="JobFilter_ItemCommand"
    OnFieldEditorCreating="JobFilter_FieldEditorCreating" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <FieldEditors>
        <telerik:RadFilterDropDownEditor FieldName="Job" DisplayName="Job" DataSourceID="edsJob"
            DataTextField="JobName" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />
        <customFilter:RadCustomFilterDropDownEditor FieldName="Machine" DataTextField="Description" DataValueField="Id" />
        <telerik:RadFilterDateFieldEditor FieldName="Date" DisplayName="Date" PickerType="DateTimePicker" DateFormat="dd/MM/yyyy HH:mm:ss"
            DefaultFilterFunction="Between" />
        <telerik:RadFilterDropDownEditor FieldName="Equipment" DisplayName="Equipment" DataSourceID="edsEquipment"
            DataTextField="EquipName" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />
        <%--<telerik:RadFilterDropDownEditor FieldName="Machine" DisplayName="Machine" DataSourceID="edsMachine"
            DataTextField="Description" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />--%>
        <telerik:RadFilterDropDownEditor FieldName="Operator" DisplayName="Operator" DataSourceID="edsOperator"
            DataTextField="Name" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />
        <telerik:RadFilterDropDownEditor FieldName="WPS" DisplayName="WPS" DataSourceID="edsWPS"
            DataTextField="Name" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />
    </FieldEditors>
    <ClientSettings>
        <ClientEvents OnFilterCreated="FilterCreated" />
    </ClientSettings>
</telerik:RadFilter>

<telerik:RadGrid ID="JobGrid" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="true"
    OnNeedDataSource="JobGrid_NeedDataSource" OnItemCommand="JobGrid_ItemCommand" AllowFilteringByColumn="false">
    <MasterTableView Caption="Production info : Job" FilterExpression="" DataKeyNames="Id">
        <Columns>
            <telerik:GridButtonColumn CommandName="Redirect" HeaderText="Ref. Number" DataTextField="JobName" ButtonType="LinkButton">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
            </telerik:GridButtonColumn>
            <telerik:GridDropDownColumn HeaderText="Operator" UniqueName="ddcOperator" DataField="OperatorId"
                DataSourceID="edsOperator" ListTextField="Name" ListValueField="Id" DropDownControlType="RadComboBox"
                EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="Machine" UniqueName="ddcMachine" DataField="AssetId"
                DataSourceID="edsMachine" ListTextField="Description" ListValueField="Id" DropDownControlType="RadComboBox"
                EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="Equipment" UniqueName="ddcEquipment" DataField="EquipmentId"
                DataSourceID="edsEquipment" ListTextField="EquipName" ListValueField="Id" DropDownControlType="RadComboBox"
                EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="NOT SET">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="WPS" DataField="WPSId" UniqueName="ddcWPS" DataSourceID="edsWPS"
                ListTextField="Name" ListValueField="Id" DropDownControlType="RadComboBox" EnableEmptyListItem="true" EmptyListItemText="NOT SET">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<br />

<telerik:RadGrid ID="DailyJobGrid" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" OnItemCommand="DailyJobGrid_ItemCommand">
    <MasterTableView Caption="" FilterExpression="" DataKeyNames="JobId">
        <ColumnGroups>
            <telerik:GridColumnGroup Name="Time" HeaderText="Time" HeaderStyle-HorizontalAlign="Center" />
            <telerik:GridColumnGroup Name="Amount" HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" />
            <telerik:GridColumnGroup Name="Consumables" HeaderText="Consumables" HeaderStyle-HorizontalAlign="Center" />
        </ColumnGroups>
        <Columns>
            <%--<telerik:GridBoundColumn DataField="Job.JobName" HeaderText="JobName" DataFormatString="{0:hh\:mm\:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>--%>
            <telerik:GridButtonColumn DataTextField="Job.JobName" HeaderText="JobName" ButtonType="LinkButton" CommandName="Redirect">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridButtonColumn>
            <telerik:GridBoundColumn DataField="JobTime" HeaderText="ƩJobTime" DataFormatString="{0:hh\:mm\:ss}" ColumnGroupName="Time">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WeldTime" HeaderText="Weld Time" DataFormatString="{0:hh\:mm\:ss}" ColumnGroupName="Time">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="PctWeldTime" HeaderText="Weld Time (%)" ColumnGroupName="Time">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="StopTime" HeaderText="Stop Time" DataFormatString="{0:hh\:mm\:ss}" ColumnGroupName="Time">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="PctStopTime" HeaderText="Stop Time (%)" ColumnGroupName="Time">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WeldNum" HeaderText="Weld Number" ColumnGroupName="Amount">
                <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WeldWarningNum" HeaderText="Weld Warning Number" ColumnGroupName="Amount">
                <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WeldErrorNum" HeaderText="Weld Error Number" ColumnGroupName="Amount">
                <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WireLength" HeaderText="Wire Length [m]" ColumnGroupName="Consumables">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="GasAmount" HeaderText="Gas Amount [l]" ColumnGroupName="Consumables">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AverageDepRate" HeaderText="Average Dep Rate [kg/h]" ColumnGroupName="Consumables">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Energy" HeaderText="Energy [kWh]" ColumnGroupName="Consumables">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<ef:EntityDataSource runat="server" ID="edsJob" ContextTypeName="SOLModule.Models.WeldDbContext"
    EntitySetName="Jobs" AutoGenerateWhereClause="true" OrderBy="it.JobName">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsOperator" ContextTypeName="SOLModule.Models.WeldDbContext"
    EntitySetName="Operators" AutoGenerateWhereClause="true" OrderBy="it.Name">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsMachine" ContextTypeName="AssetManagement.Models.AssetManagementDbContext"
    EntitySetName="Assets" AutoGenerateWhereClause="true" OrderBy="it.Description">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsEquipment" ContextTypeName="SOLModule.Models.WeldDbContext"
    EntitySetName="Equipments" AutoGenerateWhereClause="true" OrderBy="it.EquipName">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsWPS" ContextTypeName="SOLModule.Models.WeldDbContext"
    EntitySetName="WeldingProcedureSpecifications" AutoGenerateWhereClause="true" OrderBy="it.Name">
</ef:EntityDataSource>




<telerik:RadFormDecorator RenderMode="Lightweight" ID="FormDecorator1" runat="server" DecoratedControls="all" DecorationZoneID="decorationZone">
</telerik:RadFormDecorator>
<div class="container-fluid">
    <div class="demo-container" id="decorationZone">
        <h4>Analysis</h4>
        <fieldset style="width:630px;">
            <legend>Times</legend>
            <div class="formRow" style="padding-right: 10px; padding-left: 10px; width:100%; height: 362px;">
                <div class="row" style="width:100%;">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label Text="ƩJobTime" runat="server" AssociatedControlID="txtJobTotTime" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtJobTotTime" CssClass="rfdTextInput" Width="102%" />
                    </div>
                </div>
                <div class="row" style="width:100%; padding-top:5px; padding-bottom:5px;">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label Text="WeldTime" runat="server" AssociatedControlID="txtJobWeldTime" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtJobWeldTime" CssClass="rfdTextInput" Width="102%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="%WeldTime" runat="server" AssociatedControlID="txtPercWeldTime" Width="100%" CssClass="padding" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtPercWeldTime" style="max-width:120px;" />
                    </div>
                </div>
                <div class="row" style="width:100%;">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label Text="StopTime" runat="server" AssociatedControlID="txtJobStopTime" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtJobStopTime" CssClass="rfdTextInput" Width="102%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="%StopTime" runat="server" AssociatedControlID="txtPercStopTime" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtPercStopTime" style="max-width:120px;" />
                    </div>
                </div>
                <br />
                <div class="row" style="height:235px;">
                    <telerik:RadHtmlChart ID="Chart" runat="server" Width="100%" Height="100%">
                        <PlotArea>
                            <Series>
                                <telerik:PieSeries Name="TimeSerie">
                                </telerik:PieSeries>
                            </Series>
                        </PlotArea>
                    </telerik:RadHtmlChart>
                </div>
            </div>
        </fieldset>
        <fieldset style="width:630px;">
            <legend>Technical Data</legend>
            <div class="formRow" style="padding-right: 10px; padding-left: 10px; width:100%; height: 362px;">
                <div class="row" style="width:100%;">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right:0!important;">
                        <asp:Label Text="WireLength [m]" runat="server" AssociatedControlID="txtWireLength" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0!important;">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtWireLength" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="AverageDepRate [kg/h]" runat="server" AssociatedControlID="txtAverageDepRate" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <%--<asp:TextBox runat="server" ReadOnly="true" ID="txtAverageSpeed" style="max-width:120px;" />--%>
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtAverageDepRate" style="max-width:120px;" />
                    </div>
                </div>
                <div class="row" style="width:100%;">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right:0!important;">
                        <asp:Label Text="Energy [kWh]" runat="server" AssociatedControlID="txtEnergy" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0!important;">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtEnergy" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="GasAmount [l]" runat="server" AssociatedControlID="txtGasAmount" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtGasAmount" style="max-width:120px;" />
                    </div>
                </div>
                <br />
                <div class="row" style="width:100%;">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right:0!important;">
                        <asp:Label Text="Weld Num" runat="server" AssociatedControlID="txtWeldNum" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0!important;">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtWeldNum" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="Weld w/o err/wrng" runat="server" AssociatedControlID="txtWeldClear" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtWeldClear" style="max-width:120px;" />
                    </div>
                </div>
                <div class="row" style="width:100%; padding-top:5px; padding-bottom:5px;">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right:0!important;">
                        <asp:Label Text="Weld Warning" runat="server" AssociatedControlID="txtWeldWarning" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0!important;">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtWeldWarning" Width="100%" />
                    </div>
                     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="Weld Error" runat="server" AssociatedControlID="txtWeldError" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtWeldError" style="max-width:120px;" />
                    </div>
                </div>
                <br />
                <%--<telerik:RadGrid runat="server" RenderMode="Lightweight" ID="TechDataGrid" AutoGenerateColumns="false"
                    OnNeedDataSource="TechDataGrid_NeedDataSource" OnGridExporting="TechDataGrid_GridExporting" OnInfrastructureExporting="TechDataGrid_InfrastructureExporting">
                    <MasterTableView FilterExpression="" Caption="">
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Number of welds" DataField="WeldNum">
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                           <telerik:GridBoundColumn HeaderText="Weld time" DataField="WeldTime" DataFormatString="{0:hh\:mm\:ss}">
                               <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Average weld time" DataField="AvgWeldTime">
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Average wire dep (per weld)" DataField="AvgWireDep">
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Average gas consumption (per weld)" DataField="AvgGasComp">
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>--%>
            </div>
        </fieldset>
    </div>
</div>

<telerik:RadHtmlChart runat="server" ID="ChartJobWeldTime">
    <Legend>
        <Appearance Position="Bottom"></Appearance>
    </Legend>
    <%--<ChartTitle Text="Weld Time [h]"></ChartTitle>--%>
    <PlotArea>
        <Series>
            <telerik:ScatterLineSeries Name="NumJob" MissingValues="Gap" DataFieldX="Date" DataFieldY="NumJob">
                <LabelsAppearance Visible="false"></LabelsAppearance>
                <TooltipsAppearance>
                    <ClientTemplate>
                       #= kendo.format(\'{0:dd/MM/yyyy}\', new Date(value.x)) #,  #= value.y #
                    </ClientTemplate>
                </TooltipsAppearance>
            </telerik:ScatterLineSeries>
            <telerik:ScatterLineSeries Name="WeldTime(h)" MissingValues="Gap" DataFieldX="Date" DataFieldY="WeldTime">
                <LabelsAppearance Visible="false"></LabelsAppearance>
                <TooltipsAppearance>
                    <ClientTemplate>
                        #= kendo.format(\'{0:dd/MM/yyyy}\', new Date(value.x)) #, #= value.y # h
                    </ClientTemplate>
                </TooltipsAppearance>
            </telerik:ScatterLineSeries>
        </Series>
        <XAxis Type="Date" BaseUnit="Days">
            <LabelsAppearance DataFormatString="{0:dd/MM/yyyy}" RotationAngle="-45" />
        </XAxis>
    </PlotArea>
</telerik:RadHtmlChart>