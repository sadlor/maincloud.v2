﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WeldList_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.SOL.WeldList.WeldList_View" %>

<style type="text/css">
.myClassSelectedRow .rgSelectedRow td
{
	background-color:#91cef7 !important;
}
</style>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function MenuShowing(sender, args) {
            var menu = sender; var items = menu.get_items();
            if (column.get_dataType() == "System.String") {
                var i = 0;
                while (i < items.get_count()) {
                    if (!(items.getItem(i).get_value() in { 'NoFilter': '', 'Contains': '', 'NotIsEmpty': '', 'IsEmpty': '', 'NotEqualTo': '', 'EqualTo': '', 'GreaterThan': '', 'LessThan': '' })) {
                        var item = items.getItem(i);
                        if (item != null)
                            item.set_visible(false);
                    }
                    else {
                        var item = items.getItem(i);
                        if (item != null)
                            item.set_visible(true);
                    } i++;
                }
            }
            //if (column.get_dataType() == "System.Int64") {
            //    var j = 0; while (j < items.get_count()) {
            //        if (!(items.getItem(j).get_value() in { 'NoFilter': '', 'GreaterThan': '', 'LessThan': '', 'NotEqualTo': '', 'EqualTo': '' })) {
            //            var item = items.getItem(j); if (item != null)
            //                item.set_visible(false);
            //        }
            //        else { var item = items.getItem(j); if (item != null) item.set_visible(true); } j++;
            //    }
            //}
            menu.repaint();
        }
    </script>
</telerik:RadCodeBlock>

<telerik:RadGrid ID="JobGrid" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="true"
    OnNeedDataSource="JobGrid_NeedDataSource" OnItemCommand="JobGrid_ItemCommand" OnItemDataBound="JobGrid_ItemDataBound" OnUpdateCommand="JobGrid_UpdateCommand"
    AllowFilteringByColumn="true" AllowMultiRowSelection="false" CssClass="myClassSelectedRow" AllowPaging="true">
    <ClientSettings>
        <Selecting AllowRowSelect="true" UseClientSelectColumnOnly="true" />
    </ClientSettings>
    <GroupingSettings CaseSensitive="false" />
    <%--<FilterMenu OnClientShowing="MenuShowing" />--%>
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView Caption="Production info : Job" FilterExpression="" DataKeyNames="Id">
        <Columns>
            <telerik:GridTemplateColumn UniqueName="SelectCol" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBoxSelect" runat="server" OnCheckedChanged="ToggleRowSelectionJob"
                        AutoPostBack="True" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridEditCommandColumn>
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
            </telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn HeaderText="Ref. number" DataField="JobName"
                CurrentFilterFunction="StartsWith" FilterControlWidth="75%">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
            </telerik:GridBoundColumn>
            <telerik:GridDropDownColumn HeaderText="Operator" UniqueName="ddcOperator" AllowFiltering="false"
                DataField="OperatorId" DataSourceID="edsOperator" ListTextField="Name" ListValueField="Id"
                DropDownControlType="RadComboBox">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="Machine" UniqueName="ddcMachine" AllowFiltering="false"
                DataField="AssetId" DataSourceID="edsMachine" ListTextField="Description" ListValueField="Id" 
                DropDownControlType="RadComboBox">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="Equipment" UniqueName="ddcEquipment" AllowFiltering="false"
                DataField="EquipmentId" DataSourceID="edsEquipment" ListTextField="EquipName" ListValueField="Id" 
                DropDownControlType="RadComboBox" EnableEmptyListItem="true" EmptyListItemText="NOT SET">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="WPS" UniqueName="ddcWPS" DataField="WPSId" DataSourceID="edsWPS" AllowFiltering="false"
                ListTextField="Name" ListValueField="Id" DropDownControlType="RadComboBox" EnableEmptyListItem="true" EmptyListItemText="NOT SET">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<br />

<telerik:RadGrid ID="DailyJobGrid" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="true"
    AllowMultiRowSelection="false" OnItemCommand="DailyJobGrid_ItemCommand" CssClass="myClassSelectedRow" OnNeedDataSource="DailyJobGrid_NeedDataSource">
    <MasterTableView DataKeyNames="JobId, Date" Caption="" FilterExpression="">
        <Columns>
            <telerik:GridTemplateColumn UniqueName="SelectCol" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBoxSelect" runat="server" OnCheckedChanged="ToggleRowSelectionDailyJob"
                        AutoPostBack="True" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridDateTimeColumn DataField="Date" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn DataField="JobTime" HeaderText="ƩJobTime" DataFormatString="{0:hh\:mm\:ss}">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WeldTime" HeaderText="WeldTime" DataFormatString="{0:hh\:mm\:ss}">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="PctWeldTime" HeaderText="%WeldTime">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="StopTime" HeaderText="StopTime" DataFormatString="{0:hh\:mm\:ss}">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="PctStopTime" HeaderText="%StopTime">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WireLength" HeaderText="Wire Length [m]">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="GasAmount" HeaderText="Gas Amount [l]">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AverageSpeed" HeaderText="Average Speed [m/s]">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Energy" HeaderText="Energy [kWh]">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<asp:UpdatePanel runat="server">
    <ContentTemplate>

<telerik:RadGrid ID="WeldGrid" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
    OnItemDataBound="WeldGrid_ItemDataBound" OnNeedDataSource="WeldGrid_NeedDataSource" AllowPaging="true" CssClass="myClassSelectedRow">
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView Caption="Data : Weld List" FilterExpression="">
        <SortExpressions>
            <telerik:GridSortExpression FieldName="Ref" SortOrder="Descending" />
        </SortExpressions>
        <Columns>
            <telerik:GridBoundColumn HeaderText="Ref N°" DataField="Ref">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridDateTimeColumn HeaderText="Date" DataField="Date" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn HeaderText="Current [A]" DataField="Current" UniqueName="Current">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Voltage [V]" DataField="Volt" UniqueName="Voltage">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Wire Speed [m/min]" DataField="WireSpeed" UniqueName="WireSpeed">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Gas Flow [l/min]" DataField="GasFlow" UniqueName="GasFlow">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Temp [°C]" DataField="Temp">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Weld Time [s]" DataField="WeldTime">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Power [kW]" DataField="Energy">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Heat Input [kJ/mm]" DataField="HeatInput">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Weld Length [mm]" DataField="WeldLen">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
            <telerik:GridDropDownColumn HeaderText="WPS" DataField="WPSId" UniqueName="ddcWPS" DataSourceID="edsWPS"
                ListTextField="Name" ListValueField="Id" DropDownControlType="RadComboBox" EnableEmptyListItem="true" EmptyListItemText="NOT SET">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<telerik:RadHtmlChart runat="server" ID="StepChart" Skin="Silk">
    <PlotArea>
        <Series>
            <telerik:ScatterLineSeries>
                <TooltipsAppearance>
                    <ClientTemplate>
                        #= kendo.format(\'{0:mm:ss}\', new Date(value.x)) #,  #= value.y #
                    </ClientTemplate>
                </TooltipsAppearance>
            </telerik:ScatterLineSeries>
        </Series>
        <YAxis>
            <TitleAppearance Text="Wire Used (m)">
                <TextStyle Margin="9" />
            </TitleAppearance>
        </YAxis>
        <XAxis Type="Date">
        </XAxis>
    </PlotArea>
</telerik:RadHtmlChart>

    </ContentTemplate>
</asp:UpdatePanel>

<ef:EntityDataSource runat="server" ID="edsOperator" ContextTypeName="SOLModule.Models.WeldDbContext" EntitySetName="Operators" AutoGenerateWhereClause="true">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsMachine" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets" AutoGenerateWhereClause="true">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsEquipment" ContextTypeName="SOLModule.Models.WeldDbContext" EntitySetName="Equipments" AutoGenerateWhereClause="true">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsWPS" ContextTypeName="SOLModule.Models.WeldDbContext" EntitySetName="WeldingProcedureSpecifications" AutoGenerateWhereClause="true">
</ef:EntityDataSource>