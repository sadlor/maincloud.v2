﻿using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using SOLModule.Models;
using SOLModule.Repositories;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.SOL.WeldList
{
    public partial class WeldList_View : DataWidget<WeldList_View>
    {
        public WeldList_View() : base(typeof(WeldList_View)) { }

        const string DAILYJOB_DATESTART = "DailyJobDateStart";
        const string DAILYJOB_DATEEND = "DailyJobDateEnd";

        public override bool EnableExport
        {
            get
            {
                return true;
            }
        }

        public override void OnExport()
        {
            if (this.ViewState[DAILYJOB_DATESTART] != null)
            {
                WeldGrid.ExportSettings.Excel.Format = GridExcelExportFormat.Xlsx;
                WeldGrid.ExportSettings.IgnorePaging = true;
                WeldGrid.ExportSettings.ExportOnlyData = true;
                WeldGrid.ExportSettings.OpenInNewWindow = true;
                WeldGrid.MasterTableView.ExportToExcel();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DailyJobGrid.Visible = false;
                WeldGrid.Visible = false;
                StepChart.Visible = false;
                this.ViewState[Constants.JOB_ID] = null;
            }

            edsOperator.WhereParameters.Clear();
            edsOperator.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsMachine.WhereParameters.Clear();
            edsMachine.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsEquipment.WhereParameters.Clear();
            edsEquipment.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsWPS.WhereParameters.Clear();
            edsWPS.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        protected void WeldGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            // change color:
            // WARNING -> orange
            // ERROR -> red

            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem dataBoundItem = e.Item as GridDataItem;
                Weld row = dataBoundItem.DataItem as Weld;
                if (row.Current_WPS == Constants.ERR_MESSAGE)
                {
                    dataBoundItem["Current"].BackColor = Color.Red; // change particular cell
                    dataBoundItem["Current"].ForeColor = Color.White;
                }
                if (row.Current_WPS == Constants.WARNING_MESSAGE)
                {
                    dataBoundItem["Current"].BackColor = Color.Orange;
                    dataBoundItem["Current"].ForeColor = Color.Black;
                }
                if (row.GasFlow_WPS == Constants.ERR_MESSAGE)
                {
                    dataBoundItem["GasFlow"].BackColor = Color.Red;
                    dataBoundItem["GasFlow"].ForeColor = Color.White;
                }
                if (row.GasFlow_WPS == Constants.WARNING_MESSAGE)
                {
                    dataBoundItem["GasFlow"].BackColor = Color.Orange;
                    dataBoundItem["GasFlow"].ForeColor = Color.Black;
                }
                if (row.Volt_WPS == Constants.ERR_MESSAGE)
                {
                    dataBoundItem["Voltage"].BackColor = Color.Red;
                    dataBoundItem["Voltage"].ForeColor = Color.White;
                }
                if (row.Volt_WPS == Constants.WARNING_MESSAGE)
                {
                    dataBoundItem["Voltage"].BackColor = Color.Orange;
                    dataBoundItem["Voltage"].ForeColor = Color.Black;
                }
                if (row.WireSpeed_WPS == Constants.ERR_MESSAGE)
                {
                    dataBoundItem["WireSpeed"].BackColor = Color.Red;
                    dataBoundItem["WireSpeed"].ForeColor = Color.White;
                }
                if (row.WireSpeed_WPS == Constants.WARNING_MESSAGE)
                {
                    dataBoundItem["WireSpeed"].BackColor = Color.Orange;
                    dataBoundItem["WireSpeed"].ForeColor = Color.Black;
                }

                //e.Item.BackColor = Color.LightGoldenrodYellow; // for whole row
            }
        }

        protected void JobGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            JobRepository repos = new JobRepository();
            List<Job> dataSource = new List<Job>();
            dataSource = repos.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.JobId).ToList();
            JobGrid.DataSource = dataSource;
        }

        protected void JobGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.SelectCommandName)
            {
                GridDataItem item = e.Item as GridDataItem;
                string jobId = item.KeyValues.Split(':')[1].Replace("\"", "").Replace("}", "");
                if (!string.IsNullOrEmpty(jobId))
                {
                    #region Table Daily Job
                    DailyWeldJobRepository repos = new DailyWeldJobRepository();
                    List<DailyWeldJob> dataSource = new List<DailyWeldJob>();
                    dataSource = repos.ReadAll(x => x.JobId == jobId).ToList();
                    DailyJobGrid.DataSource = dataSource;
                    DailyJobGrid.Visible = true;
                    this.ViewState[Constants.JOB_ID] = jobId;
                    this.ViewState[DAILYJOB_DATESTART] = null;
                    this.ViewState[DAILYJOB_DATEEND] = null;
                    WeldGrid.Visible = false;
                    StepChart.Visible = false;
                    DailyJobGrid.DataBind();
                    #endregion

                    #region Table and chart weld
                    //WeldRepository repos = new WeldRepository();
                    //List<Weld> dataSource = new List<Weld>();
                    //dataSource = repos.ReadAll(x => x.JobId == jobId).ToList();
                    //WeldGrid.DataSource = dataSource;
                    //WeldGrid.Visible = true;
                    //WeldGrid.DataBind();

                    //StepChart.Visible = true;
                    //CreateChart(dataSource);
                    #endregion
                }
            }

            //if (e.CommandName == RadGrid.FilterCommandName)
            //{
            //    Pair command = (Pair)e.CommandArgument;
            //    if (command.Second.ToString() == "ddcOperator")
            //    {
            //        e.Canceled = true;
            //        GridFilteringItem filter = (GridFilteringItem)e.Item;
            //        ((filter["OperatorName"].Controls[0]) as TextBox).Text = ((filter["ddcOperator"].Controls[0]) as TextBox).Text;
            //        command.Second = "OperatorName";
            //        filter.FireCommandEvent("Filter", new Pair(command.First, "Operator.Name"));
            //    }
            //}
        }

        protected void ToggleRowSelectionJob(object sender, EventArgs e)
        {
            GridDataItem parent = (sender as CheckBox).NamingContainer as GridDataItem;
            parent.Selected = (sender as CheckBox).Checked;
            //((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
            foreach (GridDataItem dataItem in JobGrid.MasterTableView.Items)
            {
                if ((dataItem.FindControl("CheckBoxSelect") as CheckBox).Checked && dataItem != parent)
                {
                    (dataItem.FindControl("CheckBoxSelect") as CheckBox).Checked = false;
                    dataItem.Selected = false;
                }
            }

            ((sender as CheckBox).NamingContainer as GridDataItem).FireCommandEvent("Select", new GridSelectCommandEventArgs((GridDataItem)(sender as CheckBox).NamingContainer, null, null));
        }

        protected void ToggleRowSelectionDailyJob(object sender, EventArgs e)
        {
            GridDataItem parent = (sender as CheckBox).NamingContainer as GridDataItem;
            parent.Selected = (sender as CheckBox).Checked;
            //((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
            foreach (GridDataItem dataItem in DailyJobGrid.MasterTableView.Items)
            {
                if ((dataItem.FindControl("CheckBoxSelect") as CheckBox).Checked && dataItem != parent)
                {
                    (dataItem.FindControl("CheckBoxSelect") as CheckBox).Checked = false;
                    dataItem.Selected = false;
                }
            }

            ((sender as CheckBox).NamingContainer as GridDataItem).FireCommandEvent("Select", new GridSelectCommandEventArgs((GridDataItem)(sender as CheckBox).NamingContainer, null, null));
        }

        protected void JobGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataBoundItem = e.Item as GridDataItem;
                if (SessionMultitenant.ContainsKey(Constants.JOB_ID) && ((Job)e.Item.DataItem).Id == (string)SessionMultitenant[Constants.JOB_ID])
                {
                    (dataBoundItem["SelectCol"].FindControl("CheckBoxSelect") as CheckBox).Checked = true;
                    dataBoundItem.FireCommandEvent("Select", new GridSelectCommandEventArgs(dataBoundItem, null, null));
                }
            }
        }

        protected void JobGrid_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            var jobId = (string)editableItem.GetDataKeyValue("Id");

            JobRepository repos = new JobRepository();

            Job job = repos.FindByID(jobId);
            if (job != null)
            {
                editableItem.UpdateValues(job);

                if (string.IsNullOrEmpty(job.OperatorId))
                {
                    job.OperatorId = null;
                }
                if (string.IsNullOrEmpty(job.AssetId))
                {
                    job.AssetId = null;
                }
                if (string.IsNullOrEmpty(job.EquipmentId))
                {
                    job.EquipmentId = null;
                }

                repos.Update(job);
                repos.SaveChanges();
            }
        }

        protected void DailyJobGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (this.ViewState[Constants.JOB_ID] != null)
            {
                string jobId = (string)this.ViewState[Constants.JOB_ID];
                DailyWeldJobRepository repos = new DailyWeldJobRepository();
                List<DailyWeldJob> dataSource = new List<DailyWeldJob>();
                dataSource = repos.ReadAll(x => x.JobId == jobId).ToList();
                DailyJobGrid.DataSource = dataSource;
                DailyJobGrid.Visible = true;
            }
        }

        protected void DailyJobGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.SelectCommandName)
            {
                GridDataItem item = e.Item as GridDataItem;
                string jobId = item.KeyValues.Split(',')[0].Split(':')[1].Replace("\"", "").Replace("}", "");
                DateTime start = Convert.ToDateTime(item.KeyValues.Split(',')[1].Replace("Date:\"", "").Replace("\"}", ""));
                DateTime end = start.AddDays(1);
                this.ViewState[Constants.JOB_ID] = jobId;
                this.ViewState[DAILYJOB_DATESTART] = start;
                this.ViewState[DAILYJOB_DATEEND] = end;
                if (!string.IsNullOrEmpty(jobId))
                {
                    #region Table and chart weld
                    WeldRepository repos = new WeldRepository();
                    List<Weld> dataSource = new List<Weld>();
                    dataSource = repos.ReadAll(x => x.JobId == jobId && x.Date >= start && x.Date < end).ToList();
                    WeldGrid.DataSource = dataSource;
                    WeldGrid.Visible = true;
                    WeldGrid.DataBind();

                    StepChart.Visible = true;
                    CreateChart(dataSource);
                    #endregion
                }
            }
        }

        protected void WeldGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (this.ViewState["JobId"] != null && this.ViewState[DAILYJOB_DATESTART] != null)
            {
                string jobId = (string)this.ViewState["JobId"];
                DateTime start = (DateTime)this.ViewState[DAILYJOB_DATESTART];
                DateTime end = (DateTime)this.ViewState[DAILYJOB_DATEEND];
                WeldRepository repos = new WeldRepository();
                List<Weld> dataSource = new List<Weld>();
                dataSource = repos.ReadAll(x => x.JobId == jobId && x.Date >= start && x.Date < end).ToList();

                WeldGrid.DataSource = dataSource;
                WeldGrid.Visible = true;

                StepChart.Visible = true;
                CreateChart(dataSource);
            }
        }

        protected void CreateChart(List<Weld> dataSource)
        {
            if (dataSource.Count > 0)
            {
                DateTime epoch = new DateTime(1970, 1, 1);
                DateTime dtStart = dataSource.First().Date;//.ToUniversalTime();
                DateTime dtEnd = dataSource.Last().DateEnd;//.ToUniversalTime();

                //Set min and max date for a datetime x-axis type
                StepChart.PlotArea.XAxis.MinValue = (decimal)dtStart.Subtract(epoch).TotalMilliseconds;
                StepChart.PlotArea.XAxis.MaxValue = (decimal)dtEnd.Subtract(epoch).TotalMilliseconds;

                ScatterLineSeries stepSerie = StepChart.PlotArea.Series[0] as ScatterLineSeries;
                stepSerie.MarkersAppearance.Visible = false;
                stepSerie.LabelsAppearance.Visible = false;
                stepSerie.LineAppearance.LineStyle = Telerik.Web.UI.HtmlChart.Enums.LineStyle.Smooth;//ExtendedLineStyle.Step;
                foreach (var item in dataSource)
                {
                    stepSerie.SeriesItems.Add((decimal)item.Date.ToUniversalTime().Subtract(epoch).TotalMilliseconds, 0);
                    stepSerie.SeriesItems.Add((decimal)item.Date.AddMilliseconds(1).ToUniversalTime().Subtract(epoch).TotalMilliseconds, item.WireSpeed * (item.WeldTime / 60));
                    stepSerie.SeriesItems.Add((decimal)item.DateEnd.ToUniversalTime().Subtract(epoch).TotalMilliseconds, item.WireSpeed * (item.WeldTime / 60));
                    stepSerie.SeriesItems.Add((decimal)item.DateEnd.AddMilliseconds(1).ToUniversalTime().Subtract(epoch).TotalMilliseconds, 0);
                }

                //asse x
                if ((dtEnd - dtStart) <= TimeSpan.FromMinutes(10))
                {
                    StepChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Seconds;
                    StepChart.PlotArea.XAxis.TitleAppearance.Text = "Time (s)";
                    StepChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "ss";
                    StepChart.PlotArea.XAxis.Step = 10;
                }
                else
                {
                    if ((dtEnd - dtStart) <= TimeSpan.FromHours(1))
                    {
                        StepChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Minutes;
                        StepChart.PlotArea.XAxis.TitleAppearance.Text = "Time (m)";
                        StepChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "mm";
                        StepChart.PlotArea.XAxis.Step = 1;
                    }
                    else
                    {
                        if ((dtEnd - dtStart) <= TimeSpan.FromDays(1))
                        {
                            StepChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Hours;
                            StepChart.PlotArea.XAxis.TitleAppearance.Text = "Time (h)";
                            StepChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "hh";
                            StepChart.PlotArea.XAxis.Step = 1;
                        }
                        else
                        {
                            StepChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Days;
                            StepChart.PlotArea.XAxis.TitleAppearance.Text = "Time (d)";
                            StepChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "dd";
                            StepChart.PlotArea.XAxis.Step = 1;
                        }
                    }
                }
            }
        }
    }
}