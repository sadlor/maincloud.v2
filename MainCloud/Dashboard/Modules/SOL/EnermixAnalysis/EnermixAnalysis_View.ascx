﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EnermixAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.SOL.EnermixAnalysis.EnermixAnalysis_View" %>
<%--<%@ Register Namespace="MainCloud.Dashboard.Modules.SOL.EnermixAnalysis.CustomEditors" TagPrefix="custom" %>--%>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function FilterCreated(sender, eventArgs) {
            var filterMenu = sender.get_contextMenu();
            filterMenu.add_showing(FilterMenuShowing);
        }
        function FilterMenuShowing(sender, eventArgs) {
            var filter = $find("<%= JobFilter.ClientID %>");
            var currentExpandedItem = sender.get_attributes()._data.ItemHierarchyIndex;

            sender.findItemByValue("And").set_visible(false);
            sender.findItemByValue("Or").set_visible(false);
            sender.findItemByValue("NotOr").set_visible(false);
            sender.findItemByValue("NotAnd").set_visible(false);

            sender.findItemByValue("IsNull").set_visible(false);
            sender.findItemByValue("NotIsNull").set_visible(false);
            sender.findItemByValue("IsEmpty").set_visible(false);
            sender.findItemByValue("NotIsEmpty").set_visible(false);
            sender.findItemByValue("Contains").set_visible(false);
            sender.findItemByValue("DoesNotContain").set_visible(false);
            sender.findItemByValue("GreaterThan").set_visible(false);
            sender.findItemByValue("GreaterThanOrEqualTo").set_visible(false);
            sender.findItemByValue("LessThan").set_visible(false);
            sender.findItemByValue("LessThanOrEqualTo").set_visible(false);
            sender.findItemByValue("NotEqualTo").set_visible(false);
            sender.findItemByValue("EqualTo").set_visible(false);
            sender.findItemByValue("StartsWith").set_visible(false);
            sender.findItemByValue("EndsWith").set_visible(false);
            sender.findItemByValue("Between").set_visible(false);
            sender.findItemByValue("NotBetween").set_visible(false);
            //if (filter._expressionItems[currentExpandedItem] == "Machine")
            //{
            //    sender.findItemByValue("Between").set_visible(true);
            //    sender.findItemByValue("EqualTo").set_visible(true);
            //}
            sender.findItemByValue("Machine").set_visible(false);
        }
    </script>
</telerik:RadCodeBlock>

<style type="text/css">
    .RadPicker.RadDateTimePicker.RadPicker_Bootstrap.rfControl{
        width: 211px !important;
    }
</style>

<telerik:RadFilter RenderMode="Lightweight" runat="server" ID="JobFilter" ExpressionPreviewPosition="None" OperationMode="ServerAndClient"
    OnApplyExpressions="JobFilter_ApplyExpressions" OnExpressionItemCreated="JobFilter_ExpressionItemCreated" OnItemCommand="JobFilter_ItemCommand"
    OnFieldEditorCreating="JobFilter_FieldEditorCreating" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
    <FieldEditors>
        <telerik:RadFilterDropDownEditor FieldName="Job" DisplayName="Job" DataSourceID="edsJob"
            DataTextField="JobName" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />
        <customFilter:RadCustomFilterDropDownEditor FieldName="Machine" DataTextField="Description" DataValueField="Id" />
        <telerik:RadFilterDateFieldEditor FieldName="Date" DisplayName="Date" PickerType="DateTimePicker" DateFormat="dd/MM/yyyy HH:mm:ss"
            DefaultFilterFunction="Between" />
        <telerik:RadFilterDropDownEditor FieldName="Equipment" DisplayName="Equipment" DataSourceID="edsEquipment"
            DataTextField="EquipName" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />
        <%--<telerik:RadFilterDropDownEditor FieldName="Machine" DisplayName="Machine" DataSourceID="edsMachine"
            DataTextField="Description" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />--%>
        <telerik:RadFilterDropDownEditor FieldName="Operator" DisplayName="Operator" DataSourceID="edsOperator"
            DataTextField="Name" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />
        <telerik:RadFilterDropDownEditor FieldName="WPS" DisplayName="WPS" DataSourceID="edsWPS"
            DataTextField="Name" DataValueField="Id" DropDownType="RadComboBox" DefaultFilterFunction="EqualTo" />
    </FieldEditors>
    <ClientSettings>
        <ClientEvents OnFilterCreated="FilterCreated" />
    </ClientSettings>
</telerik:RadFilter>

<telerik:RadGrid ID="JobGrid" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="true"
    OnNeedDataSource="JobGrid_NeedDataSource" OnItemCommand="JobGrid_ItemCommand" AllowFilteringByColumn="false">
    <MasterTableView Caption="Production info : Job" FilterExpression="" DataKeyNames="Id">
        <Columns>
            <telerik:GridButtonColumn CommandName="Redirect" HeaderText="Ref. Number" DataTextField="JobName" ButtonType="LinkButton">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
            </telerik:GridButtonColumn>
            <telerik:GridDropDownColumn HeaderText="Operator" UniqueName="ddcOperator" DataField="OperatorId"
                DataSourceID="edsOperator" ListTextField="Name" ListValueField="Id" DropDownControlType="RadComboBox"
                EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="Machine" UniqueName="ddcMachine" DataField="AssetId"
                DataSourceID="edsMachine" ListTextField="Description" ListValueField="Id" DropDownControlType="RadComboBox"
                EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="Equipment" UniqueName="ddcEquipment" DataField="EquipmentId"
                DataSourceID="edsEquipment" ListTextField="EquipName" ListValueField="Id" DropDownControlType="RadComboBox"
                EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="NOT SET">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="WPS" DataField="WPSId" UniqueName="ddcWPS" DataSourceID="edsWPS"
                ListTextField="Name" ListValueField="Id" DropDownControlType="RadComboBox" EnableEmptyListItem="true" EmptyListItemText="NOT SET">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<br />

<telerik:RadGrid ID="DailyJobGrid" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" OnItemCommand="DailyJobGrid_ItemCommand">
    <MasterTableView Caption="" FilterExpression="" DataKeyNames="JobId">
        <ColumnGroups>
            <telerik:GridColumnGroup Name="Time" HeaderText="Time" HeaderStyle-HorizontalAlign="Center" />
            <telerik:GridColumnGroup Name="Amount" HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" />
            <telerik:GridColumnGroup Name="Consumables" HeaderText="Consumables" HeaderStyle-HorizontalAlign="Center" />
        </ColumnGroups>
        <Columns>
            <%--<telerik:GridBoundColumn DataField="Job.JobName" HeaderText="JobName" DataFormatString="{0:hh\:mm\:ss}">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>--%>
            <telerik:GridButtonColumn DataTextField="Job.JobName" HeaderText="JobName" ButtonType="LinkButton" CommandName="Redirect">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridButtonColumn>
            <telerik:GridBoundColumn DataField="JobTime" HeaderText="ƩJobTime" DataFormatString="{0:hh\:mm\:ss}" ColumnGroupName="Time">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WeldTime" HeaderText="Weld Time" DataFormatString="{0:hh\:mm\:ss}" ColumnGroupName="Time">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="PctWeldTime" HeaderText="Weld Time (%)" ColumnGroupName="Time">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="StopTime" HeaderText="Stop Time" DataFormatString="{0:hh\:mm\:ss}" ColumnGroupName="Time">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="PctStopTime" HeaderText="Stop Time (%)" ColumnGroupName="Time">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WeldNum" HeaderText="Weld Number" ColumnGroupName="Amount">
                <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WeldWarningNum" HeaderText="Weld Warning Number" ColumnGroupName="Amount">
                <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WeldErrorNum" HeaderText="Weld Error Number" ColumnGroupName="Amount">
                <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WireLength" HeaderText="Wire Length [m]" ColumnGroupName="Consumables">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="GasAmount" HeaderText="Gas Amount [l]" ColumnGroupName="Consumables">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AverageDepRate" HeaderText="Average Dep Rate [kg/h]" ColumnGroupName="Consumables">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Energy" HeaderText="Energy [kWh]" ColumnGroupName="Consumables">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<ef:EntityDataSource runat="server" ID="edsJob" ContextTypeName="SOLModule.Models.WeldDbContext"
    EntitySetName="Jobs" AutoGenerateWhereClause="true" OrderBy="it.JobName">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsOperator" ContextTypeName="SOLModule.Models.WeldDbContext"
    EntitySetName="Operators" AutoGenerateWhereClause="true" OrderBy="it.Name">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsMachine" ContextTypeName="AssetManagement.Models.AssetManagementDbContext"
    EntitySetName="Assets" AutoGenerateWhereClause="true" OrderBy="it.Description">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsEquipment" ContextTypeName="SOLModule.Models.WeldDbContext"
    EntitySetName="Equipments" AutoGenerateWhereClause="true" OrderBy="it.EquipName">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsWPS" ContextTypeName="SOLModule.Models.WeldDbContext"
    EntitySetName="WeldingProcedureSpecifications" AutoGenerateWhereClause="true" OrderBy="it.Name">
</ef:EntityDataSource>