﻿using MainCloudFramework.UI.Modules;
using MainCloudFramework.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.SOL.EnermixAnalysis
{
    public partial class EnermixAnalysis_Settings : WidgetSetting
    {
        public override bool ApplyChanges(DataCustomSettings widgetCustomSettings)
        {
            widgetCustomSettings.AddOrUpdate(Constants.SETTINGS_DESTINATION_AREA_ID, ddlArea.SelectedValue);
            return true;
        }

        public override void SyncChanges(DataCustomSettings widgetCustomSettings)
        {
            ddlArea.LoadDefualtAreaData();
            ddlArea.DataBind();
            ddlArea.SelectedValue = widgetCustomSettings.Find(Constants.SETTINGS_DESTINATION_AREA_ID) as string;
        }
    }
}