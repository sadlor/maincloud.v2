﻿using MainCloudFramework.Web.Helpers;
using Microsoft.AspNet.EntityDataSource;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Telerik.Web.UI;

namespace MainCloud.Dashboard.Modules.SOL.EnermixAnalysis.CustomEditors
{
    public class RadCustomFilterDropDownEditor : RadFilterDataFieldEditor
    {
        private RadComboBox _combo;

        protected override void CopySettings(RadFilterDataFieldEditor baseEditor)
        {
            base.CopySettings(baseEditor);
            var editor = baseEditor as RadCustomFilterDropDownEditor;
            if (editor != null)
            {
                //DataSource = editor.DataSource;
                DataTextField = editor.DataTextField;
                DataValueField = editor.DataValueField;
            }
        }

        public override System.Collections.ArrayList ExtractValues()
        {
            ArrayList list = new ArrayList();
            list.Add(_combo.SelectedValue);
            return list;
        }

        public override void InitializeEditor(System.Web.UI.Control container)
        {
            RadComboBox c = new RadComboBox();
            c.SelectedIndexChanged += C_SelectedIndexChanged;

            _combo = c;
            _combo.ID = "MyCombo";
            _combo.DataTextField = DataTextField;
            _combo.DataValueField = DataValueField;
            _combo.DataSource = DataSource;
            _combo.DataBind();
            container.Controls.Add(_combo);
        }

        private void C_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Reset Filter if there is a Job
            bool clear = false;
            foreach (var expr in this.Owner.RootGroup.Expressions)
            {
                if (expr is RadFilterEqualToFilterExpression<string>)
                {
                    if ((expr as RadFilterEqualToFilterExpression<string>).FieldName == "Job")
                    {
                        clear = true;
                        break;
                    }
                }
            }
            if (clear)
            {
                int count = this.Owner.RootGroup.Expressions.Count;
                for (int i = count - 1; i > 0; i--)
                {
                    this.Owner.RootGroup.Expressions.RemoveAt(i);
                }
                this.Owner.RecreateControl();
            }
        }

        public override void SetEditorValues(System.Collections.ArrayList values)
        {
            if (values != null && values.Count > 0)
            {
                if (values[0] == null)
                    return;
                var item = _combo.FindItemByValue(values[0].ToString());
                if (item != null)
                    item.Selected = true;
            }
        }

        public string DataTextField
        {
            get
            {
                return (string)ViewState["DataTextField"] ?? string.Empty;
            }
            set
            {
                ViewState["DataTextField"] = value;
            }
        }
        public string DataValueField
        {
            get
            {
                return (string)ViewState["DataValueField"] ?? string.Empty;
            }
            set
            {
                ViewState["DataValueField"] = value;
            }
        }

        public List<AssetManagement.Models.Asset> DataSource
        {
            get
            {
                AssetManagement.Repositories.AssetRepository AR = new AssetManagement.Repositories.AssetRepository();
                return AR.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Description).ToList();
            }
        }
    }
}