﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobAnalysis_View.ascx.cs" Inherits="MainCloud.Dashboard.Modules.SOL.JobAnalysis.JobAnalysis_View" %>

<link href="<%= "/Dashboard/Modules/SOL/JobAnalysis/Style.css?" + System.IO.File.GetLastWriteTime(Server.MapPath("~/Dashboard/Modules/SOL/JobAnalysis/Style.css")).Ticks %>" rel="stylesheet" type="text/css" />

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function MenuShowing(sender, args) {
            var menu = sender; var items = menu.get_items();
            if (column.get_dataType() == "System.String") {
                var i = 0;
                while (i < items.get_count()) {
                    if (!(items.getItem(i).get_value() in { 'NoFilter': '', 'Contains': '', 'NotIsEmpty': '', 'IsEmpty': '', 'NotEqualTo': '', 'EqualTo': '', 'GreaterThan': '', 'LessThan': '' })) {
                        var item = items.getItem(i);
                        if (item != null)
                            item.set_visible(false);
                    }
                    else {
                        var item = items.getItem(i);
                        if (item != null)
                            item.set_visible(true);
                    } i++;
                }
            }
            //if (column.get_dataType() == "System.Int64") {
            //    var j = 0; while (j < items.get_count()) {
            //        if (!(items.getItem(j).get_value() in { 'NoFilter': '', 'GreaterThan': '', 'LessThan': '', 'NotEqualTo': '', 'EqualTo': '' })) {
            //            var item = items.getItem(j); if (item != null)
            //                item.set_visible(false);
            //        }
            //        else { var item = items.getItem(j); if (item != null) item.set_visible(true); } j++;
            //    }
            //}
            menu.repaint();
        }
    </script>
</telerik:RadCodeBlock>

<telerik:RadGrid ID="JobGrid" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" Visible="true"
    OnNeedDataSource="JobGrid_NeedDataSource" OnItemCommand="JobGrid_ItemCommand" OnUpdateCommand="JobGrid_UpdateCommand" OnItemDataBound="JobGrid_ItemDataBound"
    AllowFilteringByColumn="true" AllowMultiRowSelection="false" CssClass="myClassSelectedRow" AllowPaging="true">
    <ClientSettings>
        <Selecting AllowRowSelect="true" UseClientSelectColumnOnly="true" />
    </ClientSettings>
    <GroupingSettings CaseSensitive="false" />
    <%--<FilterMenu OnClientShowing="MenuShowing" />--%>
    <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
    <MasterTableView Caption="Production info : Job" FilterExpression="" DataKeyNames="Id">
        <Columns>
            <telerik:GridTemplateColumn UniqueName="SelectCol" Exportable="false" AllowFiltering="false">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBoxSelect" runat="server" OnCheckedChanged="ToggleRowSelection"
                        AutoPostBack="True" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridEditCommandColumn UniqueName="EditColumn" Exportable="false">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
            </telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn HeaderText="Ref. number" DataField="JobName"
                CurrentFilterFunction="StartsWith" FilterControlWidth="75%">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
            </telerik:GridBoundColumn>
            <telerik:GridDropDownColumn HeaderText="Operator" UniqueName="ddcOperator" AllowFiltering="false"
                DataField="OperatorId" DataSourceID="edsOperator" ListTextField="Name" ListValueField="Id"
                DropDownControlType="RadComboBox" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="Machine" UniqueName="ddcMachine" AllowFiltering="false"
                DataField="AssetId" DataSourceID="edsMachine" ListTextField="Description" ListValueField="Id"
                DropDownControlType="RadComboBox" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="Equipment" UniqueName="ddcEquipment" AllowFiltering="false"
                DataField="EquipmentId" DataSourceID="edsEquipment" ListTextField="EquipName" ListValueField="Id"
                DropDownControlType="RadComboBox" EnableEmptyListItem="true" EmptyListItemValue="" EmptyListItemText="NOT SET">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn HeaderText="WPS" UniqueName="ddcWPS" AllowFiltering="false"
                DataField="WPSId" DataSourceID="edsWPS" ListTextField="Name" ListValueField="Id"
                DropDownControlType="RadComboBox" EnableEmptyListItem="true" EmptyListItemText="NOT SET">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Center" />
            </telerik:GridDropDownColumn>
            <telerik:GridNumericColumn HeaderText="Wire Diameter [mm]" DataField="WireDiameter" 
                AllowFiltering="false" DefaultInsertValue="0" DecimalDigits="2">
                <HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />
                <ItemStyle HorizontalAlign="Right" />
            </telerik:GridNumericColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<br />

<ef:EntityDataSource runat="server" ID="edsOperator" ContextTypeName="SOLModule.Models.WeldDbContext" EntitySetName="Operators" AutoGenerateWhereClause="true">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsMachine" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Assets" AutoGenerateWhereClause="true">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsEquipment" ContextTypeName="SOLModule.Models.WeldDbContext" EntitySetName="Equipments" AutoGenerateWhereClause="true">
</ef:EntityDataSource>
<ef:EntityDataSource runat="server" ID="edsWPS" ContextTypeName="SOLModule.Models.WeldDbContext" EntitySetName="WeldingProcedureSpecifications" AutoGenerateWhereClause="true">
</ef:EntityDataSource>

<br />

<telerik:RadButton ID="btnRedirect" Text="<%$ Resources:CustomSOL, ViewWeldList %>" runat="server" OnClick="btnRedirect_Click" />

<telerik:RadFormDecorator RenderMode="Lightweight" ID="FormDecorator1" runat="server" DecoratedControls="all" DecorationZoneID="decorationZone">
</telerik:RadFormDecorator>
<div class="container-fluid">
    <div class="demo-container" id="decorationZone">
        <h4>Analysis</h4>
        <fieldset style="width:630px;">
            <legend>Times</legend>
            <div class="formRow" style="padding-right: 10px; padding-left: 10px; width:100%; height: 362px;">
                <div class="row" style="width:100%;">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label Text="ƩJobTime" runat="server" AssociatedControlID="txtJobTotTime" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtJobTotTime" CssClass="rfdTextInput" Width="102%" />
                    </div>
                </div>
                <div class="row" style="width:100%; padding-top:5px; padding-bottom:5px;">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label Text="WeldTime" runat="server" AssociatedControlID="txtJobWeldTime" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtJobWeldTime" CssClass="rfdTextInput" Width="102%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="%WeldTime" runat="server" AssociatedControlID="txtPercWeldTime" Width="100%" CssClass="padding" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtPercWeldTime" style="max-width:120px;" />
                    </div>
                </div>
                <div class="row" style="width:100%;">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <asp:Label Text="StopTime" runat="server" AssociatedControlID="txtJobStopTime" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtJobStopTime" CssClass="rfdTextInput" Width="102%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="%StopTime" runat="server" AssociatedControlID="txtPercStopTime" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtPercStopTime" style="max-width:120px;" />
                    </div>
                </div>
                <br />
                <div class="row" style="height:235px;">
                    <telerik:RadHtmlChart ID="Chart" runat="server" Width="100%" Height="100%">
                        <PlotArea>
                            <Series>
                                <telerik:PieSeries Name="TimeSerie">
                                </telerik:PieSeries>
                            </Series>
                        </PlotArea>
                    </telerik:RadHtmlChart>
                </div>
            </div>
        </fieldset>
        <fieldset style="width:630px;">
            <legend>Technical Data</legend>
            <div class="formRow" style="padding-right: 10px; padding-left: 10px; width:100%; height: 362px;">
                <div class="row" style="width:100%;">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right:0!important;">
                        <asp:Label Text="WireLength [m]" runat="server" AssociatedControlID="txtWireLength" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0!important;">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtWireLength" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="AverageDepRate [kg/h]" runat="server" AssociatedControlID="txtAverageDepRate" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <%--<asp:TextBox runat="server" ReadOnly="true" ID="txtAverageSpeed" style="max-width:120px;" />--%>
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtAverageDepRate" style="max-width:120px;" />
                    </div>
                </div>
                <div class="row" style="width:100%;">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right:0!important;">
                        <asp:Label Text="Energy [kWh]" runat="server" AssociatedControlID="txtEnergy" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0!important;">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtEnergy" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="GasAmount [l]" runat="server" AssociatedControlID="txtGasAmount" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtGasAmount" style="max-width:120px;" />
                    </div>
                </div>
                <br />
                <div class="row" style="width:100%;">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right:0!important;">
                        <asp:Label Text="Weld Num" runat="server" AssociatedControlID="txtWeldNum" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0!important;">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtWeldNum" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="Weld w/o err/wrng" runat="server" AssociatedControlID="txtWeldClear" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtWeldClear" style="max-width:120px;" />
                    </div>
                </div>
                <div class="row" style="width:100%; padding-top:5px; padding-bottom:5px;">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right:0!important;">
                        <asp:Label Text="Weld Warning" runat="server" AssociatedControlID="txtWeldWarning" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left:0!important;">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtWeldWarning" Width="100%" />
                    </div>
                     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                        <asp:Label Text="Weld Error" runat="server" AssociatedControlID="txtWeldError" Width="100%" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtWeldError" style="max-width:120px;" />
                    </div>
                </div>
                <br />
                <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="TechDataGrid" AutoGenerateColumns="false"
                    OnNeedDataSource="TechDataGrid_NeedDataSource" OnGridExporting="TechDataGrid_GridExporting" OnInfrastructureExporting="TechDataGrid_InfrastructureExporting">
                    <MasterTableView FilterExpression="" Caption="">
                        <%--<HeaderStyle HorizontalAlign="Center" BackColor="#428bca" ForeColor="White" Font-Bold="true" />--%>
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Number of welds" DataField="WeldNum">
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                           <telerik:GridBoundColumn HeaderText="Weld time" DataField="WeldTime" DataFormatString="{0:hh\:mm\:ss}">
                               <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Average weld time" DataField="AvgWeldTime">
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Average wire dep (per weld)" DataField="AvgWireDep">
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Average gas consumption (per weld)" DataField="AvgGasComp">
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
        </fieldset>
    </div>
</div>

<br />

<div runat="server" id="divTableWarningError" class="panel panel-default" style="min-width:450px;">
    <table runat="server" id="tableWarningError" class="table table-bordered table-condensed info" style="width:100%;">
        <tr>
            <td></td>
            <td style="background-color:#428bca; color:white; text-align:center"><b>Current</b></td>
            <td style="background-color:#428bca; color:white; text-align:center"><b>Voltage</b></td>
            <td style="background-color:#428bca; color:white; text-align:center"><b>Wire Speed</b></td>
            <td style="background-color:#428bca; color:white; text-align:center"><b>Gas Flow</b></td>
            <td style="background-color:#428bca; color:white; text-align:center"><b>Temperature</b></td>
            <td style="background-color:#428bca; color:white; text-align:center"><b>Total</b></td>
        </tr>
        <tr>
            <td><b>Warning(s)</b></td>
            <td runat="server" id="CurrentWrng" style="text-align: right"></td>
            <td runat="server" id="VoltageWrng" style="text-align: right"></td>
            <td runat="server" id="WireSpeedWrng" style="text-align: right"></td>
            <td runat="server" id="GasFlowWrng" style="text-align: right"></td>
            <td runat="server" id="TemperatureWrng" style="text-align: right"></td>
            <td runat="server" id="TotalWrng" style="text-align: right"></td>
        </tr>
        <tr>
            <td><b>Error(s)</b></td>
            <td runat="server" id="CurrentErr" style="text-align: right"></td>
            <td runat="server" id="VoltageErr" style="text-align: right"></td>
            <td runat="server" id="WireSpeedErr" style="text-align: right"></td>
            <td runat="server" id="GasFlowErr" style="text-align: right"></td>
            <td runat="server" id="TemperatureErr" style="text-align: right"></td>
            <td runat="server" id="TotalErr" style="text-align: right"></td>
        </tr>
        <tr>
            <td style="text-wrap:normal;"><b>Welds with warning(s) %</b></td>
            <td runat="server" id="CurrentWrngPct"></td>
            <td runat="server" id="VoltageWrngPct"></td>
            <td runat="server" id="WireSpeedWrngPct"></td>
            <td runat="server" id="GasFlowWrngPct"></td>
            <td runat="server" id="TemperatureWrngPct"></td>
            <td runat="server" id="TotalWrngPct" style="text-align: right"></td>
        </tr>
        <tr>
            <td style="text-wrap:normal;"><b>Welds with error(s) %</b></td>
            <td runat="server" id="CurrentErrPct"></td>
            <td runat="server" id="VoltageErrPct"></td>
            <td runat="server" id="WireSpeedErrPct"></td>
            <td runat="server" id="GasFlowErrPct"></td>
            <td runat="server" id="TemperatureErrPct"></td>
            <td runat="server" id="TotalErrPct" style="text-align: right"></td>
        </tr>
    </table>
</div>