﻿using AssetManagement.Repositories;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.BaseWidgets.DataWidget;
using MainCloudFramework.Web.Helpers;
using SOLModule.Models;
using SOLModule.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.ExportInfrastructure;

namespace MainCloud.Dashboard.Modules.SOL.JobAnalysis
{
    public partial class JobAnalysis_View : DataWidget<JobAnalysis_View>
    {
        public JobAnalysis_View() : base(typeof(JobAnalysis_View)) { }

        public override bool EnableExport
        {
            get
            {
                return true;
            }
        }

        public override void OnExport()
        {
            TechDataGrid.ExportSettings.Excel.Format = GridExcelExportFormat.Xlsx;
            TechDataGrid.ExportSettings.IgnorePaging = true;
            TechDataGrid.ExportSettings.ExportOnlyData = true;
            TechDataGrid.ExportSettings.OpenInNewWindow = true;

            TechDataGrid.MasterTableView.ExportToExcel();
        }

        [Serializable]
        private class TechDataGridStruct
        {
            public int WeldNum { get; set; }
            public TimeSpan WeldTime { get; set; }
            public decimal AvgWeldTime { get; set; }
            public decimal AvgWireDep { get; set; }
            public decimal AvgGasComp { get; set; }

            public TechDataGridStruct(int weldNum, TimeSpan weldTime, decimal avgWeldTime, decimal avgWireDep, decimal avgGasComp)
            {
                WeldNum = weldNum;
                WeldTime = weldTime;
                AvgWeldTime = avgWeldTime;
                AvgWireDep = avgWireDep;
                AvgGasComp = avgGasComp;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnRedirect.Enabled = false;
            }

            edsOperator.WhereParameters.Clear();
            edsOperator.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsMachine.WhereParameters.Clear();
            edsMachine.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsEquipment.WhereParameters.Clear();
            edsEquipment.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
            edsWPS.WhereParameters.Clear();
            edsWPS.WhereParameters.Add("ApplicationId", MultiTenantsHelper.ApplicationId);
        }

        protected void JobGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            JobRepository repos = new JobRepository();
            List<Job> dataSource = new List<Job>();
            dataSource = repos.ReadAll(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.JobId).ToList();
            JobGrid.DataSource = dataSource;
        }

        protected void JobGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.SelectCommandName)
            {
                GridDataItem item = e.Item as GridDataItem;
                string jobId = item.KeyValues.Split(':')[1].Replace("\"", "").Replace("}", "");
                if (!string.IsNullOrEmpty(jobId))
                {
                    WeldRepository repos = new WeldRepository();
                    List<Weld> dataSource = new List<Weld>();
                    dataSource = repos.ReadAll(x => x.JobId == jobId).ToList();

                    SessionMultitenant[Constants.JOB_ID] = jobId;
                    btnRedirect.Enabled = true;
                    Analisi(dataSource);
                }
            }

            //if (e.CommandName == RadGrid.FilterCommandName)
            //{
            //    Pair command = (Pair)e.CommandArgument;
            //    if (command.Second.ToString() == "ddcOperator")
            //    {
            //        e.Canceled = true;
            //        GridFilteringItem filter = (GridFilteringItem)e.Item;
            //        ((filter["OperatorName"].Controls[0]) as TextBox).Text = ((filter["ddcOperator"].Controls[0]) as TextBox).Text;
            //        command.Second = "OperatorName";
            //        filter.FireCommandEvent("Filter", new Pair(command.First, "Operator.Name"));
            //    }
            //}
        }

        protected void ToggleRowSelection(object sender, EventArgs e)
        {
            GridDataItem parent = (sender as CheckBox).NamingContainer as GridDataItem;
            parent.Selected = (sender as CheckBox).Checked;
            //((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
            foreach (GridDataItem dataItem in JobGrid.MasterTableView.Items)
            {
                if ((dataItem.FindControl("CheckBoxSelect") as CheckBox).Checked && dataItem != parent)
                {
                    (dataItem.FindControl("CheckBoxSelect") as CheckBox).Checked = false;
                    dataItem.Selected = false;
                }
            }

            ((sender as CheckBox).NamingContainer as GridDataItem).FireCommandEvent("Select", new GridSelectCommandEventArgs((GridDataItem)(sender as CheckBox).NamingContainer, null, null));
        }

        public void Analisi(List<Weld> dataSource)
        {
            //DataSource -> JobList
            //tempo effettivo di saldatura -> somma weld time per job
            //tempo totale job -> Data fine(order by descending DateTime .First) - Data inizio(order by Date .First)
            //tempo di fermo -> Data inizio - data fine precedente

            txtJobTotTime.Text = "";
            txtJobWeldTime.Text = "";
            txtPercWeldTime.Text = "";
            txtJobStopTime.Text = "";
            txtPercStopTime.Text = "";
            txtWireLength.Text = "";
            txtAverageDepRate.Text = "";
            txtEnergy.Text = "";
            txtGasAmount.Text = "";
            txtWeldNum.Text = "";
            txtWeldWarning.Text = "";
            txtWeldError.Text = "";
            txtWeldClear.Text = "";

            if (dataSource.Count > 0)
            {
                TimeSpan totTime = (dataSource.OrderByDescending(x => x.DateEnd).First().DateEnd - dataSource.OrderBy(x => x.Date).First().Date);
                txtJobTotTime.Text = string.Format("{0}:{1}:{2}", (int)totTime.TotalHours, totTime.Minutes, totTime.Seconds);
                //txtJobTotTime.Text = string.Format(@"{0:hh\:mm\:ss}", totTime);
                TimeSpan weldTime = TimeSpan.FromSeconds((double)dataSource.Sum(x => x.WeldTime).Value);
                txtJobWeldTime.Text = string.Format("{0}:{1}:{2}", (int)weldTime.TotalHours, weldTime.Minutes, weldTime.Seconds);
                //txtJobWeldTime.Text = string.Format(@"{0:hh\:mm\:ss}", weldTime);
                double percWeldTime = Math.Round(
                    TimeSpan.FromSeconds((double)dataSource.Sum(x => x.WeldTime).Value).TotalSeconds /
                    (dataSource.OrderByDescending(x => x.DateEnd).First().DateEnd - dataSource.OrderBy(x => x.Date).First().Date).TotalSeconds
                     * 100, 2);
                txtPercWeldTime.Text = percWeldTime.ToString() + " %";

                //TimeSpan stopTime = new TimeSpan(0, 0, 0);
                //DateTime finePrec = dataSource.Min(x => x.Date);
                //foreach (var item in dataSource.OrderBy(x => x.Date))
                //{
                //    stopTime = stopTime + (item.Date - finePrec);
                //    finePrec = item.DateEnd;
                //}                
                TimeSpan stopTime = 
                    (dataSource.OrderByDescending(x => x.DateEnd).First().DateEnd - dataSource.OrderBy(x => x.Date).First().Date) -
                    TimeSpan.FromSeconds((double)dataSource.Sum(x => x.WeldTime).Value);
                txtJobStopTime.Text = string.Format("{0}:{1}:{2}", (int)stopTime.TotalHours, stopTime.Minutes, stopTime.Seconds);
                //txtJobStopTime.Text = string.Format(@"{0:hh\:mm\:ss}", stopTime);
                double percStopTime = Math.Round(
                    stopTime.TotalSeconds /
                    (dataSource.OrderByDescending(x => x.DateEnd).First().DateEnd - dataSource.OrderBy(x => x.Date).First().Date).TotalSeconds
                    * 100, 2);
                txtPercStopTime.Text = percStopTime.ToString() + " %";

                PieSeriesItem pieItem = new PieSeriesItem((decimal)percWeldTime);
                pieItem.Name = "WeldTime";
                (Chart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(pieItem);

                pieItem = new PieSeriesItem((decimal)percStopTime);
                pieItem.Name = "StopTime";
                (Chart.PlotArea.Series[0] as PieSeries).SeriesItems.Add(pieItem);

                decimal wireLength = Math.Round(dataSource.Sum(x => x.WireSpeed * x.WeldTime / 60).Value, 2);
                txtWireLength.Text = wireLength.ToString();
                //txtAverageSpeed.Text = Math.Round((dataSource.Sum(x => x.WeldLen) / dataSource.Sum(x => x.WeldTime).Value).Value, 2).ToString();
                txtEnergy.Text = Math.Round(dataSource.Sum(x => x.Energy * x.WeldTime / 3600).Value, 2).ToString();
                decimal gasAmount = Math.Round(dataSource.Sum(x => x.GasFlow * x.WeldTime / 60).Value, 2);
                txtGasAmount.Text = gasAmount.ToString();

                JobRepository repos = new JobRepository();
                Job job = repos.FindByID(dataSource.First().JobId);
                var avgDepRate = Math.Round(((double)Math.Round(dataSource.Sum(x => x.WireSpeed * x.WeldTime / 60).Value, 2) *
                                  (Math.PI * Math.Pow((double)(job.WireDiameter / 1000) / 2, 2)) *
                                  7800) /
                                 (double)(dataSource.Sum(x => x.WeldTime).Value / 3600), 2);
                txtAverageDepRate.Text = avgDepRate.ToString();

                int weldNum = dataSource.Count();
                int weldWarningNum = dataSource.Where(x => x.Current_WPS == Constants.WARNING_MESSAGE || x.GasFlow_WPS == Constants.WARNING_MESSAGE || x.Volt_WPS == Constants.WARNING_MESSAGE || x.WireSpeed_WPS == Constants.WARNING_MESSAGE).Count();
                int weldErrorNum = dataSource.Where(x => x.Current_WPS == Constants.ERR_MESSAGE || x.GasFlow_WPS == Constants.ERR_MESSAGE || x.Volt_WPS == Constants.ERR_MESSAGE || x.WireSpeed_WPS == Constants.ERR_MESSAGE).Count();
                txtWeldNum.Text = weldNum.ToString();
                txtWeldWarning.Text = weldWarningNum.ToString();
                txtWeldError.Text = weldErrorNum.ToString();
                txtWeldClear.Text = dataSource.Where(x => x.Current_WPS == Constants.OK_MESSAGE && x.GasFlow_WPS == Constants.OK_MESSAGE && x.Volt_WPS == Constants.OK_MESSAGE && x.WireSpeed_WPS == Constants.OK_MESSAGE).Count().ToString();

                int currWrng = dataSource.Where(x => x.Current_WPS == Constants.WARNING_MESSAGE).Count();
                CurrentWrng.InnerText = currWrng.ToString();
                int currErr = dataSource.Where(x => x.Current_WPS == Constants.ERR_MESSAGE).Count();
                CurrentErr.InnerText = currErr.ToString();
                int voltWrng = dataSource.Where(x => x.Volt_WPS == Constants.WARNING_MESSAGE).Count();
                VoltageWrng.InnerText = voltWrng.ToString();
                int voltErr = dataSource.Where(x => x.Volt_WPS == Constants.ERR_MESSAGE).Count();
                VoltageErr.InnerText = voltErr.ToString();
                int wirespeedWrng = dataSource.Where(x => x.WireSpeed_WPS == Constants.WARNING_MESSAGE).Count();
                WireSpeedWrng.InnerText = wirespeedWrng.ToString();
                int wirespeedErr = dataSource.Where(x => x.WireSpeed_WPS == Constants.ERR_MESSAGE).Count();
                WireSpeedErr.InnerText = wirespeedErr.ToString();
                int gasflowWrng = dataSource.Where(x => x.GasFlow_WPS == Constants.WARNING_MESSAGE).Count();
                GasFlowWrng.InnerText = gasflowWrng.ToString();
                int gasflowErr = dataSource.Where(x => x.GasFlow_WPS == Constants.ERR_MESSAGE).Count();
                GasFlowErr.InnerText = gasflowErr.ToString();
                TotalWrng.InnerText = (currWrng + voltWrng + wirespeedWrng + gasflowWrng).ToString();
                TotalErr.InnerText = (currErr + voltErr + wirespeedErr + gasflowErr).ToString();
                CurrentWrngPct.InnerText = Math.Round((decimal)currWrng / (decimal)weldNum * 100).ToString() + "%";
                CurrentErrPct.InnerText = Math.Round((decimal)currErr / (decimal)weldNum * 100).ToString() + "%";
                VoltageWrngPct.InnerText = Math.Round((decimal)voltWrng / (decimal)weldNum * 100).ToString() + "%";
                VoltageErrPct.InnerText = Math.Round((decimal)voltErr / (decimal)weldNum * 100).ToString() + "%";
                WireSpeedWrngPct.InnerText = Math.Round((decimal)wirespeedWrng / (decimal)weldNum * 100).ToString() + "%";
                WireSpeedErrPct.InnerText = Math.Round((decimal)wirespeedErr / (decimal)weldNum * 100).ToString() + "%";
                GasFlowWrngPct.InnerText = Math.Round((decimal)gasflowWrng / (decimal)weldNum * 100).ToString() + "%";
                GasFlowErrPct.InnerText = Math.Round((decimal)gasflowErr / (decimal)weldNum * 100, 2).ToString() + "%";
                TotalWrngPct.InnerText = weldWarningNum.ToString();
                TotalErrPct.InnerText = weldErrorNum.ToString();

                this.ViewState["TechDataGridDataSource"] = new List<TechDataGridStruct>() { new TechDataGridStruct(weldNum, weldTime, Math.Round((decimal)weldTime.TotalSeconds / weldNum, 2), Math.Round(wireLength / weldNum, 2), Math.Round(gasAmount / weldNum, 2)) };
                TechDataGrid.Rebind();
            }
        }

        protected void btnRedirect_Click(object sender, EventArgs e)
        {
            string destinationAreaId = CustomSettings.Find(Constants.SETTINGS_DESTINATION_AREA_ID) as string;
            if (!string.IsNullOrEmpty(destinationAreaId))
            {
                WidgetAreaRepository areaService = new WidgetAreaRepository();
                WidgetArea wa = areaService.FindByID(destinationAreaId);

                Response.Redirect(MultiTenantsHelper.MountMultiTenantAreaUrl(wa.RoutePath));
            }
        }

        protected void JobGrid_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            var jobId = (string)editableItem.GetDataKeyValue("Id");

            JobRepository repos = new JobRepository();

            Job job = repos.FindByID(jobId);
            if (job != null)
            {
                editableItem.UpdateValues(job);

                if (string.IsNullOrEmpty(job.OperatorId))
                {
                    job.OperatorId = null;
                }
                if (string.IsNullOrEmpty(job.AssetId))
                {
                    job.AssetId = null;
                }
                if (string.IsNullOrEmpty(job.EquipmentId))
                {
                    job.EquipmentId = null;
                }

                repos.Update(job);
                repos.SaveChanges();
            }
        }

        protected void JobGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataBoundItem = e.Item as GridDataItem;
                if (SessionMultitenant.ContainsKey(Constants.JOB_ID) && ((Job)e.Item.DataItem).Id == (string)SessionMultitenant[Constants.JOB_ID])
                {
                    (dataBoundItem["SelectCol"].FindControl("CheckBoxSelect") as CheckBox).Checked = true;
                    dataBoundItem.FireCommandEvent("Select", new GridSelectCommandEventArgs(dataBoundItem, null, null));
                }
            }
        }

        protected void TechDataGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (this.ViewState["TechDataGridDataSource"] != null)
            {
                List<TechDataGridStruct> dataSource = this.ViewState["TechDataGridDataSource"] as List<TechDataGridStruct>;
                TechDataGrid.DataSource = dataSource;
            }
        }

        protected void TechDataGrid_GridExporting(object sender, GridExportingArgs e)
        {
            if (!string.IsNullOrEmpty((string)SessionMultitenant[Constants.JOB_ID]))
            {
                string jobId = (string)SessionMultitenant[Constants.JOB_ID];
                JobRepository repos = new JobRepository();
                Job job = repos.FindByID(jobId);

                AssetRepository aRep = new AssetRepository();
                AssetManagement.Models.Asset a = aRep.FindByID(job.AssetId);

                OperatorRepository opRep = new OperatorRepository();
                Operator op = opRep.FindByID(job.OperatorId);
                WPSRepository wpsRep = new WPSRepository();
                WeldingProcedureSpecification wps = wpsRep.FindByID(job.WPSId);
                EquipmentRepository eqRep = new EquipmentRepository();
                Equipment eq = eqRep.FindByID(job.EquipmentId);

                string jobText = "<body><table border=\"1\"><tr>" +
                                 "<td><b>Job</b></td>" +
                                 "<td><b>Operator</b></td>" +
                                 "<td><b>Machine</b></td>" +
                                 "<td><b>Equipment</b></td>" +
                                 "<td><b>WPS</b></td>" +
                                 "<td><b>Wire Diameter [mm]</b></td></tr><tr>" +
                                 "<td>" + job.JobName + "</td>" +
                                 "<td>" + op.Name + "</td>" +
                                 "<td>" + a.Description + "</td>" +
                                 "<td>" + ((eq == null) ? "NOT SET" : eq.EquipName) + "</td>" +
                                 "<td>" + ((wps == null) ? "NOT SET" : wps.Name) + "</td>" +
                                 "<td>" + job.WireDiameter + "</td></tr></table>";

                string analysisDataText = "<h4>Analysis</h4><table border=\"1\"><caption><b>Times</b></caption><tr>" +
                                          "<td><b>ƩJobTime</b></td>" +
                                          "<td><b>WeldTime</b></td>" +
                                          "<td><b>%WeldTime</b></td>" +
                                          "<td><b>StopTime</b></td>" +
                                          "<td><b>%StopTime</b></td></tr><tr>" +
                                          "<td>" + txtJobTotTime.Text + "</td>" +
                                          "<td>" + txtJobWeldTime.Text + "</td>" +
                                          "<td>" + txtPercWeldTime.Text + "</td>" +
                                          "<td>" + txtJobStopTime.Text + "</td>" +
                                          "<td>" + txtPercStopTime.Text + "</td></tr></table><br /><table border=\"1\"><caption><b>Technical Data</b></caption><tr>" +
                                          "<td><b>WireLength [m]</b></td>" +
                                          "<td><b>AverageDepRate [kg/h]</b></td>" +
                                          "<td><b>Energy [kWh]</b></td>" +
                                          "<td><b>GasAmount [l]</b></td></tr><tr>" +
                                          "<td>" + txtWireLength.Text + "</td>" +
                                          "<td>" + txtAverageDepRate.Text + "</td>" +
                                          "<td>" + txtEnergy.Text + "</td>" +
                                          "<td>" + txtGasAmount.Text + "</td></tr></table>";

                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                tableWarningError.RenderControl(htw);

                string s = jobText + "<br />" + analysisDataText + "<br />" + sw + "<br />";
                e.ExportOutput = e.ExportOutput.Replace("<body>", s);
            }
        }

        protected void TechDataGrid_InfrastructureExporting(object sender, GridInfrastructureExportingEventArgs e)
        {

            if (!string.IsNullOrEmpty((string)SessionMultitenant[Constants.JOB_ID]))
            {
                string jobId = (string)SessionMultitenant[Constants.JOB_ID];
                JobRepository repos = new JobRepository();
                Job job = repos.FindByID(jobId);

                AssetRepository aRep = new AssetRepository();
                AssetManagement.Models.Asset a = aRep.FindByID(job.AssetId);

                OperatorRepository opRep = new OperatorRepository();
                Operator op = opRep.FindByID(job.OperatorId);
                WPSRepository wpsRep = new WPSRepository();
                WeldingProcedureSpecification wps = wpsRep.FindByID(job.WPSId);
                EquipmentRepository eqRep = new EquipmentRepository();
                Equipment eq = eqRep.FindByID(job.EquipmentId);

                Telerik.Web.UI.ExportInfrastructure.Table table = e.ExportStructure.Tables[0];
                table.ShiftRowsDown(1, 19);
                table.Columns[1].Width = 150;
                table.Columns[2].Width = 150;
                table.Columns[3].Width = 150;
                table.Columns[4].Width = 150;
                table.Columns[5].Width = 150;
                table.Columns[6].Width = 150;
                table.Cells[1, 1].Value = "Job";
                table.Cells[1, 1].Style.Font.Bold = true;
                table.Cells[1, 2].Value = job.JobName;
                table.Cells[2, 1].Value = "Operator";
                table.Cells[2, 1].Style.Font.Bold = true;
                table.Cells[2, 2].Value = op.Name;
                table.Cells[3, 1].Value = "Machine";
                table.Cells[3, 1].Style.Font.Bold = true;
                table.Cells[3, 2].Value = a.Description;
                table.Cells[4, 1].Value = "Equipment";
                table.Cells[4, 1].Style.Font.Bold = true;
                table.Cells[4, 2].Value = (eq == null) ? "NOT SET" : eq.EquipName;
                table.Cells[5, 1].Value = "WPS";
                table.Cells[5, 1].Style.Font.Bold = true;
                table.Cells[5, 2].Value = (wps == null) ? "NOT SET" : wps.Name;
                table.Cells[6, 1].Value = "Wire Diameter [mm]";
                table.Cells[6, 1].Style.Font.Bold = true;
                table.Cells[6, 2].Value = job.WireDiameter;

                table.Cells[1, 4].Value = "Analysis";
                table.Cells[1, 4].Style.Font.Bold = true;

                table.Cells[1, 6].Value = "Times";
                table.Cells[1, 6].Colspan = 5;
                table.Cells[1, 6].Style.Font.Bold = true;
                table.Cells[1, 6].Style.HorizontalAlign = HorizontalAlign.Center;
                table.Cells[1, 7].Value = "ƩJobTime";
                table.Cells[1, 7].Style.Font.Bold = true;
                table.Cells[1, 8].Value = txtJobTotTime.Text;
                table.Cells[2, 7].Value = "WeldTime";
                table.Cells[2, 7].Style.Font.Bold = true;
                table.Cells[2, 8].Value = txtJobWeldTime.Text;
                table.Cells[3, 7].Value = "%WeldTime";
                table.Cells[3, 7].Style.Font.Bold = true;
                table.Cells[3, 8].Value = txtPercWeldTime.Text;
                table.Cells[4, 7].Value = "StopTime";
                table.Cells[4, 7].Style.Font.Bold = true;
                table.Cells[4, 8].Value = txtJobStopTime.Text;
                table.Cells[5, 7].Value = "%StopTime";
                table.Cells[5, 7].Style.Font.Bold = true;
                table.Cells[5, 8].Value = txtPercStopTime.Text;

                table.Cells[1, 10].Value = "Technical Data";
                table.Cells[1, 10].Colspan = 4;
                table.Cells[1, 10].Style.Font.Bold = true;
                table.Cells[1, 10].Style.HorizontalAlign = HorizontalAlign.Center;
                table.Cells[1, 11].Value = "WireLength [m]";
                table.Cells[1, 11].Style.Font.Bold = true;
                table.Cells[1, 12].Value = txtWireLength.Text;
                table.Cells[2, 11].Value = "AverageDepRate [kg/h]";
                table.Cells[2, 11].Style.Font.Bold = true;
                table.Cells[2, 12].Value = txtAverageDepRate.Text;
                table.Cells[3, 11].Value = "Energy [kWh]";
                table.Cells[3, 11].Style.Font.Bold = true;
                table.Cells[3, 12].Value = txtEnergy.Text;
                table.Cells[4, 11].Value = "GasAmount [l]";
                table.Cells[4, 11].Style.Font.Bold = true;
                table.Cells[4, 12].Value = txtGasAmount.Text;

                table.Cells[1, 15].Value = "Warning(s)";
                table.Cells[1, 15].Style.Font.Bold = true;
                table.Cells[1, 16].Value = "Error(s)";
                table.Cells[1, 16].Style.Font.Bold = true;
                table.Cells[1, 17].Value = "Welds with warning(s) %";
                table.Cells[1, 17].Style.Font.Bold = true;
                table.Cells[1, 18].Value = "Welds with error(s) %";
                table.Cells[1, 18].Style.Font.Bold = true;

                table.Cells[2, 14].Value = "Current";
                table.Cells[2, 14].Style.Font.Bold = true;
                table.Cells[2, 14].Style.ForeColor = Color.White;
                table.Cells[2, 14].Style.BackColor = ColorTranslator.FromHtml("#428bca");
                table.Cells[3, 14].Value = "Voltage";
                table.Cells[3, 14].Style.Font.Bold = true;
                table.Cells[3, 14].Style.ForeColor = Color.White;
                table.Cells[3, 14].Style.BackColor = ColorTranslator.FromHtml("#428bca");
                table.Cells[4, 14].Value = "Wire Speed";
                table.Cells[4, 14].Style.Font.Bold = true;
                table.Cells[4, 14].Style.ForeColor = Color.White;
                table.Cells[4, 14].Style.BackColor = ColorTranslator.FromHtml("#428bca");
                table.Cells[5, 14].Value = "Gas Flow";
                table.Cells[5, 14].Style.Font.Bold = true;
                table.Cells[5, 14].Style.ForeColor = Color.White;
                table.Cells[5, 14].Style.BackColor = ColorTranslator.FromHtml("#428bca");
                table.Cells[6, 14].Value = "Temperature";
                table.Cells[6, 14].Style.Font.Bold = true;
                table.Cells[6, 14].Style.ForeColor = Color.White;
                table.Cells[6, 14].Style.BackColor = ColorTranslator.FromHtml("#428bca");
                table.Cells[7, 14].Value = "Total";
                table.Cells[7, 14].Style.Font.Bold = true;
                table.Cells[7, 14].Style.ForeColor = Color.White;
                table.Cells[7, 14].Style.BackColor = ColorTranslator.FromHtml("#428bca");

                table.Cells[2, 15].Value = CurrentWrng.InnerText;
                table.Cells[2, 16].Value = CurrentErr.InnerText;
                table.Cells[3, 15].Value = VoltageWrng.InnerText;
                table.Cells[3, 16].Value = VoltageErr.InnerText;
                table.Cells[4, 15].Value = WireSpeedWrng.InnerText;
                table.Cells[4, 16].Value = WireSpeedErr.InnerText;
                table.Cells[5, 15].Value = GasFlowWrng.InnerText;
                table.Cells[5, 16].Value = GasFlowErr.InnerText;
                table.Cells[7, 15].Value = TotalWrng.InnerText;
                table.Cells[7, 16].Value = TotalErr.InnerText;
                table.Cells[2, 17].Value = CurrentWrngPct.InnerText;
                table.Cells[2, 18].Value = CurrentErrPct.InnerText;
                table.Cells[3, 17].Value = VoltageWrngPct.InnerText;
                table.Cells[3, 18].Value = VoltageErrPct.InnerText;
                table.Cells[4, 17].Value = WireSpeedWrngPct.InnerText;
                table.Cells[4, 18].Value = WireSpeedErrPct.InnerText;
                table.Cells[5, 17].Value = GasFlowWrngPct.InnerText;
                table.Cells[5, 18].Value = GasFlowErrPct.InnerText;
                table.Cells[7, 17].Value = TotalWrngPct.InnerText;
                table.Cells[7, 18].Value = TotalErrPct.InnerText;

                table.Cells[4, 20].TextWrap = true;
                table.Cells[5, 20].TextWrap = true;

                e.ExportStructure.Tables.Add(table);
            }
        }

        protected void ExportProcedure()
        {
            Response.Clear();

            //Response.AddHeader("content-disposition", "attachment;filename=Report_"+ DateTime.Now.ToShortDateString() +".xlsx");
            Response.AddHeader("content-disposition", "attachment;filename=Report.xlsx");
            Response.Charset = "";

            // If you want the option to open the Excel file without saving than
            // comment out the line below
            // Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            //Response.ContentType = "application/vnd.xls";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            tableWarningError.RenderControl(htw);

            Response.Write(sw.ToString());
            Response.End();
        }
    }
}