﻿<%@ Page Language="C#"  MasterPageFile="~/Dashboard/MainDashboard.Master" Title="Aree"  AutoEventWireup="true" CodeBehind="DashboardAreas.aspx.cs" Inherits="MainCloudFramework.Dashboard.DashboardAreas" enableEventValidation="false"%>
<%@ Register Src="~/Dashboard/Controls/IconSelector.ascx" TagPrefix="mcf" TagName="IconSelector" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="NavBarContent" ContentPlaceHolderID="NavBarPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="SubMenuContent" ContentPlaceHolderID="SubMenuContent" runat="Server">
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <mcf:PopUpDialog ID="dlgArea" runat="server">
        <Body>
            <asp:UpdatePanel ID="updpnlAreaDialog" runat="server">
                <ContentTemplate>
                    <asp:HiddenField ID="IdTextBox" runat="server" />
                    <asp:HiddenField ID="WidgetGroupAreaField" runat="server" />
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="TitleTextBox">Nome area</asp:Label>
                        <asp:TextBox ID="TitleTextBox" runat="server" CssClass="form-control" placeholder="Nome area" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="TitleTextBox"
                            ErrorMessage="Nome area è un campo obbligatorio"
                            ForeColor="Red" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                        <asp:CustomValidator id="NameAlreadyExistsValidator" 
                             runat="server" ControlToValidate="TitleTextBox" 
                             ErrorMessage="Nome già esistente, cambiare nome." 
                             OnServerValidate="NameAlreadyExistsValidator_ServerValidate"
                             ForeColor="Red" Display="Dynamic">
                        </asp:CustomValidator>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Icon" runat="server" AssociatedControlID="IconSelector">Icona</asp:Label>
                        <mcf:IconSelector runat="server" ID="IconSelector" />
                    </div>
                    <div class="form-group">
                        <asp:Label ID="DescriptionLabel" runat="server" AssociatedControlID="DescriptionBox">Descrizione</asp:Label>
                        <asp:TextBox ID="DescriptionBox" runat="server" CssClass="form-control" placeholder="Descrizione" />
                    </div>
                    <div class="form-group" runat="server" id="AreaGroupsFields">
                        <asp:Label ID="AreaGroupsFieldLabel" runat="server" AssociatedControlID="AreaGroupsField">Gruppo</asp:Label>
                        <asp:DropDownList ID="AreaGroupsField" runat="server" OnDataBound="AreaGroups_DataBound" 
                            DataTextField="Title" DataValueField="Id" CssClass="form-control" CausesValidation="false"/>
                    </div>
                    <div class="form-group" runat="server" id="AreaLayoutFields">
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="AreaLayout">Layout</asp:Label>
                        <asp:DropDownList ID="AreaLayout" runat="server" OnDataBound="AreaLayout_DataBound" 
                            DataTextField="Name" DataValueField="Template" CssClass="form-control"/>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label3" runat="server" AssociatedControlID="OrderBox">Posizione</asp:Label>
                        <asp:TextBox ID="OrderBox" runat="server" CssClass="form-control" placeholder="Posizione" type="number" required="required" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                            ControlToValidate="OrderBox"
                            ErrorMessage="Posizione è un campo obbligatorio, inserire un valore numerico"
                            ForeColor="Red" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div id="PublicAreaVisibility" runat="server" class="form-group">
                        <asp:Label ID="Label4" runat="server" AssociatedControlID="chkPublicAreaVisibility">Area</asp:Label>
                        <div>
                            <asp:RadioButtonList id="ChkPublicAreaVisibility" runat="server" RepeatDirection="Horizontal" DataTextField="Text" DataValueField="Value" />
                        </div>
                        <hr />
                        <label>Roles</label>
                        <asp:GridView ID="DgvRoles" runat="server" DataKeyNames="ID" AutoGenerateColumns="False" CssClass="table table-hover table-striped" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="IdRole" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"/>
                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:TemplateField ShowHeader="False">
                                    <HeaderTemplate>View</HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chkAllowView" OnCheckedChanged="chkAllowView_CheckedChanged" AutoPostBack="true" AccessKey="<%# Container.DataItemIndex %>"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <HeaderTemplate>Edit</HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chkAllowEdit"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="cursor-pointer" />
                        </asp:GridView>

                    </div>
                    <%-- Footer --%>
                    <div class="modal-footer">
                        <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" Text="Insert" CssClass="btn btn-primary" OnCommand="InsertButton_Command" />
                        <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" Text="Update" CssClass="btn btn-primary" OnCommand="UpdateButton_Command"/>
                        <asp:LinkButton ID="CancelButton" runat="server" CausesValidation="False" Text="Cancel" CssClass="btn btn-default" data-dismiss="modal" />
                    </div>

                    <script type="text/javascript">
                        function pageLoad() {
                            $(function () {
                                try {
                                    setTimeout(function () { $("select").msDropDown(); }, 200);
                                } catch (e) {
                                    //console.log(e.message);
                                }
                            });
                        }
                    </script>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="lvAreas" EventName="ItemCommand" />
                    <asp:AsyncPostBackTrigger ControlID="insertArea" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="lvGroupArea" EventName="ItemCommand" />
<%--                    <asp:PostBackTrigger ControlID="updpnlAreaDialog" />--%>
                </Triggers>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>


    <div class="row" style="margin-bottom: 10px">
        <div class="col-lg-12 col-md-12">
            <mcf:CommandButton ID="insertGroup" runat="server" CssClass="btn btn-primary pull-right" 
                OnClick="insertGroupArea_Click"
                CommandName="Insert"
                CausesValidation="false"
                style="margin-left: 2px; margin-right: 2px"
                >
                <span><i class="fa fa-object-group"></i></span>
                <span>Aggiungi gruppo</span>
            </mcf:CommandButton>

            <mcf:CommandButton ID="insertArea" runat="server" CssClass="btn btn-primary pull-right" 
                OnClick="insertArea_Click"
                CommandName="Insert"
                CausesValidation="false"
                style="margin-left: 2px; margin-right: 2px"
                >
                <span><i class="fa fa-plus"></i></span>
                <span>Aggiungi area</span>
            </mcf:CommandButton>
        </div>
    </div>

    <%-- Aree non raggruppate --%>
    <asp:ListView ID="lvAreas" runat="server" DataKeyNames="Id, ApplicationId" OnItemCommand="lvAreas_ItemCommand">
        <LayoutTemplate>
            <div ID="itemPlaceholderContainer" runat="server" class="row">
                <div runat="server" id="itemPlaceholder"></div>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="col-lg-4 col-md-6">
                <div class='panel <%# ((bool)Eval("PublicArea")) ?  "panel-default" : "panel-warning" %>'>
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class='fa <%# Eval("Icon") %> fa-5x'></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge-sm"><%# Eval("Title") %></div>
                                <div><%# Eval("Description") %></div>
                                <div>
                                    <mcf:CommandButton ID="starredArea" runat="server" CssClass="btn btn-default btn-xs btn-outline"
                                        CommandName="Starred" CommandArgument='<%# Eval("ID") %>' 
                                        CausesValidation="false" 
                                        ClientIDMode="AutoID" 
                                        Visible='<%# AllowModifyArea(Eval("OwnerUserId")) %>'>
                                        <span>Preferiti</span>
                                        <span><i id="icon" runat="server" style='<%# "color:" + ((bool)DataBinder.Eval(Container.DataItem, "Starred") ? "#FFBF00" : "grey") + ";" %>' class="fa fa-star"></i></span>
                                    </mcf:CommandButton>
                                </div>
                            </div>
                        </div>
                        <small><i class='fa <%# ((bool)Eval("PublicArea")) ? "fa-eye" : "fa-eye-slash" %>'></i> <%# GetApplicationUserNameById((string)Eval("OwnerUserId")) %></small>
                    </div>
                    <div class="panel-footer" runat="server">
                        <mcf:CommandButton ID="deleteArea" runat="server" CssClass="pull-left btn btn-danger btn-xs btn-outline" style="margin-right: 5px;"
                            CommandName="Delete" CommandArgument='<%# Eval("ID") %>' 
                            ClientIDMode="AutoID" 
                            CausesValidation="false" 
                            Visible='<%# AllowDeleteArea(Eval("OwnerUserId")) %>'>
                            <span>Elimina</span>
                            <span><i class="fa fa-trash-o"></i></span>
                        </mcf:CommandButton>
                        <mcf:CommandButton ID="editArea" runat="server" CssClass="pull-left btn btn-default btn-xs btn-outline" 
                            CommandName="Edit" CommandArgument='<%# Eval("ID") %>' 
                            CausesValidation="false" 
                            ClientIDMode="AutoID"
                            Visible='<%# AllowModifyArea(Eval("OwnerUserId")) %>'>
                            <span>Modifica</span>
                            <span><i class="fa fa-edit"></i></span>
                        </mcf:CommandButton>
                        <mcf:NavigateToAreaLinkButton ID="navigateToArea" runat="server" CssClass="pull-right" RouthPath='<%# Eval("RoutePath") %>' CausesValidation="false">
                            <span>Visualizza</span>
                            <span><i class="fa fa-arrow-circle-right"></i></span>
                        </mcf:NavigateToAreaLinkButton>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:ListView>

    <%-- Aree raggruppate --%>
    <asp:ListView ID="lvGroupArea" runat="server" DataKeyNames="Id, ApplicationId" OnItemCommand="lvAreas_ItemCommand">
        <LayoutTemplate>
            <div ID="itemPlaceholderContainer" runat="server" class="row">
                <div runat="server" id="itemPlaceholder"></div>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class='panel <%# ((bool)Eval("PublicArea")) ?  "panel-default" : "panel-warning" %>' style="border-color: steelblue">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="huge-sm">
                                <i class='fa <%# Eval("Icon") %> fa-1x'></i>
                                <%# Eval("Title") %>
                            </div>
                            <div><%# Eval("Description") %></div>
                        </div>
                        <div class="col-xs-9">
                            <div class="text-right">
                                <small><i class='fa <%# ((bool)Eval("PublicArea")) ? "fa-eye" : "fa-eye-slash" %>'></i> <%# GetApplicationUserNameById((string)Eval("OwnerUserId")) %></smal>
                                <mcf:CommandButton ID="starredArea" runat="server" CssClass="pull-right btn btn-default btn-xs btn-outline" style="margin-left: 5px;"
                                    CommandName="Starred" CommandArgument='<%# Eval("ID") %>' 
                                    CausesValidation="false" 
                                    ClientIDMode="AutoID" 
                                    Visible='<%# AllowModifyArea(Eval("OwnerUserId")) %>'>
                                    <span>Preferiti&nbsp;</span>
                                    <span><i id="icon" runat="server" style='<%# "color:" + ((bool)DataBinder.Eval(Container.DataItem, "Starred") ? "#FFBF00" : "grey") + ";" %>' class="fa fa-star"></i></span>
                                </mcf:CommandButton>
                            </div>
                            <div style="margin-top: 3px;">
                                <mcf:CommandButton ID="deleteArea" runat="server" CssClass="pull-left btn btn-danger btn-xs btn-outline" style="margin-right: 5px;"
                                    CommandName="Delete" CommandArgument='<%# Eval("ID") %>' 
                                    ClientIDMode="AutoID" 
                                    CausesValidation="false" 
                                    Visible='<%# AllowDeleteArea(Eval("OwnerUserId")) && ((MainCloudFramework.Models.WidgetArea)Container.DataItem).WidgetAreaChildren.Count == 0 %>'>
                                    <span>Elimina</span>
                                    <span><i class="fa fa-trash-o"></i></span>
                                </mcf:CommandButton>
                                <mcf:CommandButton ID="editArea" runat="server" CssClass="pull-right btn btn-default btn-xs btn-outline" 
                                    CommandName="Edit" CommandArgument='<%# Eval("ID") %>' 
                                    CausesValidation="false" 
                                    ClientIDMode="AutoID"
                                    Visible='<%# AllowModifyArea(Eval("OwnerUserId")) %>'>
                                    <span>Modifica</span>
                                    <span><i class="fa fa-edit"></i></span>
                                </mcf:CommandButton>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                     <div class="panel-body" runat="server">

                    <asp:ListView ID="lvAreas" datasource='<%# ((MainCloudFramework.Models.WidgetArea)Container.DataItem).WidgetAreaChildren %>' runat="server" DataKeyNames="Id, ApplicationId" OnItemCommand="lvAreas_ItemCommand">
                        <LayoutTemplate>
                            <div ID="itemPlaceholderContainer" runat="server" class="row">
                                <div runat="server" id="itemPlaceholder"></div>
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <div class="col-lg-4 col-md-6">
                                <div class='panel <%# ((bool)Eval("PublicArea")) ?  "panel-default" : "panel-warning" %>'>
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class='fa <%# Eval("Icon") %> fa-5x'></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge-sm"><%# Eval("Title") %></div>
                                                <div><%# Eval("Description") %></div>
                                                <div>
                                                    <mcf:CommandButton ID="starredArea" runat="server" CssClass="btn btn-default btn-xs btn-outline"
                                                        CommandName="Starred" CommandArgument='<%# Eval("ID") %>' 
                                                        CausesValidation="false" 
                                                        ClientIDMode="AutoID" 
                                                        Visible='<%# AllowModifyArea(Eval("OwnerUserId")) %>'>
                                                        <span>Preferiti</span>
                                                        <span><i id="icon" runat="server" style='<%# "color:" + ((bool)DataBinder.Eval(Container.DataItem, "Starred") ? "#FFBF00" : "grey") + ";" %>' class="fa fa-star"></i></span>
                                                    </mcf:CommandButton>
                                                </div>
                                            </div>
                                        </div>
                                        <small><i class='fa <%# ((bool)Eval("PublicArea")) ? "fa-eye" : "fa-eye-slash" %>'></i> <%# GetApplicationUserNameById((string)Eval("OwnerUserId")) %></small>
                                    </div>
                                    <div class="panel-footer" runat="server">
                                        <mcf:CommandButton ID="deleteArea" runat="server" CssClass="pull-left btn btn-danger btn-xs btn-outline" style="margin-right: 5px;"
                                            CommandName="Delete" CommandArgument='<%# Eval("ID") %>' 
                                            ClientIDMode="AutoID" 
                                            CausesValidation="false" 
                                            Visible='<%# AllowDeleteArea(Eval("OwnerUserId")) %>'>
                                            <span>Elimina</span>
                                            <span><i class="fa fa-trash-o"></i></span>
                                        </mcf:CommandButton>
                                        <mcf:CommandButton ID="editArea" runat="server" CssClass="pull-left btn btn-default btn-xs btn-outline" 
                                            CommandName="Edit" CommandArgument='<%# Eval("ID") %>' 
                                            CausesValidation="false" 
                                            ClientIDMode="AutoID"
                                            Visible='<%# AllowModifyArea(Eval("OwnerUserId")) %>'>
                                            <span>Modifica</span>
                                            <span><i class="fa fa-edit"></i></span>
                                        </mcf:CommandButton>
                                        <mcf:NavigateToAreaLinkButton ID="navigateToArea" runat="server" CssClass="pull-right" RouthPath='<%# Eval("RoutePath") %>' CausesValidation="false">
                                            <span>Visualizza</span>
                                            <span><i class="fa fa-arrow-circle-right"></i></span>
                                        </mcf:NavigateToAreaLinkButton>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                    </div> <!-- panel-body -->
                </div>
            </div>

        </ItemTemplate>
    </asp:ListView>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>