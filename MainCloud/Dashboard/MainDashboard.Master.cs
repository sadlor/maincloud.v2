﻿using MainCloudFramework.Core.Utilities;
using MainCloudFramework.Models;
using MainCloudFramework.Repository;
using MainCloudFramework.Services;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Web;

namespace MainCloud.Dashboard
{
    public partial class MainDashboard : BaseMasterPage
    {
        private void AreaDataBind()
        {
            WidgetAreaService widgetAreaService = new WidgetAreaService();
            string applicationId = MultiTenantsHelper.ApplicationId;
            string userId = Page.User.Identity.GetUserId();
            IList<WidgetArea> menu = widgetAreaService.FindAllGroupedActiveStarredAreaByApplicationAndUserId(applicationId, userId, ReflectionUtility.GetPropertyName(() => new WidgetArea().Order), true);

            rptAreas.DataSource = menu;
            rptAreas.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AreaDataBind();
            }

            //if (ApplicationSettingsHelper.IsLicenseExpired() == true)
            //{
            //    Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            //    Response.Redirect(Request.RawUrl);
            //}
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            menuItemAlert.Visible = false;
        }
    }
}