﻿using EnergyModule;
using EnergyModule.Core;
using EnergyModule.Models;
using EnergyModule.Services;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web.Multitenants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.Dashboard.Pages
{
    public partial class DefaultPage : BasePage
    {
        private WidgetAreaRepository widgetAreaRepository = new WidgetAreaRepository();

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            string routePath = Page.RouteData.Values["name"] as string;
            WidgetArea page = widgetAreaRepository.FindWidgetAreaByRoutePath(routePath);

            if (page != null)
            {
                Page.Title = page.Title;
                string templateUrl = ResolveUrl(System.IO.Path.Combine(MCFConstants.BASE_PAGES_TEMPLATE_PATH, page.Template));
                Control template = LoadControl(templateUrl);
                pagelayout.Controls.Clear();
                pagelayout.Controls.Add(template);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        //protected void ConnectAll(List<WebPart> wpList)
        //{
        //    foreach (WebPart wp in wpList)
        //    {
        //        ProviderConnectionPoint pc = webPartManager.GetProviderConnectionPoints(wp)[0];
        //        foreach (WebPart wp2 in wpList)
        //        {
        //            if (wp != wp2)
        //            {
        //                ConsumerConnectionPoint cc = webPartManager.GetConsumerConnectionPoints(wp2)[0];
        //                WebPartConnection conn = webPartManager.ConnectWebParts(wp, pc, wp2, cc);
        //            }
        //        }
        //    }
        //}

        protected void webPartManager_SelectedWebPartChanging(object sender, WebPartCancelEventArgs e)
        {
        }

        protected void webPartManager_SelectedWebPartChanged(object sender, WebPartEventArgs e)
        {
            if (e.WebPart != null)
            {
                dlgWidgetConfig.OpenDialog();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            displayManager.Visible = AllowInsertWidgetLayout;
        }
    }
}