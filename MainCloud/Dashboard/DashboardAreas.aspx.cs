﻿using MainCloudFramework.Core.Utilities;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Repository;
using MainCloudFramework.Services;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web.Templates;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace MainCloudFramework.Dashboard
{
    public partial class DashboardAreas : BasePage
    {
        private string applicationId = MultiTenantsHelper.ApplicationId; // TODO: Gestire errore in caso di aplication NULL
        private GrantService grantService = new GrantService();
        private ApplicationService applicationService = new ApplicationService();
        ApplicationUser user = null;

        private void GridViewDataBind()
        {
            DgvRoles.DataSource = applicationService.RolesInCurrentApplication();
            DgvRoles.DataBind();
            DgvRoles.UseAccessibleHeader = true;
            if (DgvRoles.HeaderRow != null)
            {
                DgvRoles.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        public string GetApplicationUserNameById(string userId)
        {
            if (userId == null)
            {
                return "Public Area";
            }
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                user = db.Users.Where(u => u.Id == userId).SingleOrDefault();
            }
            return user != null ? user.UserName : "Anonymous";
        }

        public bool AllowModifyArea(object ownerUserId)
        {
            return grantService.AllowModifyArea(ownerUserId as string);
        }

        public bool AllowDeleteArea(object ownerUserId)
        {
            return grantService.AllowDeleteArea(ownerUserId as string);
        }

        public List<WidgetArea> UserWidgetAreas()
        {
            List<WidgetArea> userWidgetAreas = new List<WidgetArea>();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //db.
            }

           return userWidgetAreas;
        }

        private void AreaDataList()
        {
            WidgetAreaService widgetAreaService = new WidgetAreaService();
            string userId = User.Identity.GetUserId();
            var areas = widgetAreaService.FindAllUngrupedActiveAreaByApplicationAndUserId(applicationId, userId,
                                    ReflectionUtility.GetPropertyName(() => new WidgetArea().Title), true);

            lvGroupArea.DataSource = widgetAreaService.FindAllGroupedActiveAreaByApplicationAndUserId(applicationId, userId,
                                    ReflectionUtility.GetPropertyName(() => new WidgetArea().Title), true);

            List<WidgetArea> areasNotVisible = new List<WidgetArea>();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var user = db.Users.Where(u => u.Id == userId).SingleOrDefault();
                var userRoleId = user.Roles.FirstOrDefault().RoleId;
                var userWidgets = db.WidgetAreaRoleSet.Where(i => i.ApplicationRole.Id.Equals(userRoleId)).ToList();
                var roles = db.Roles.FirstOrDefault(i => i.Id.Equals(userRoleId) && applicationId.Equals(applicationId));

                if (roles.CanViewAllWidgets == false)
                {
                    foreach (var a in areas)
                    {
                        foreach (WidgetAreaRoles w in userWidgets)
                        {
                            if (!a.Id.Equals(w.WidgetArea.Id))
                            {
                                areasNotVisible.Add(a);
                            }
                        }
                    }
                    areas = areas.Except(areasNotVisible).ToList();
                }
            }
            
            lvAreas.DataSource = areas;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                AreaDataList();
                lvAreas.DataBind();
                lvGroupArea.DataBind();
            }

            PublicAreaVisibility.Visible = grantService.AllowPublicArea();
            insertArea.Visible = grantService.AllowAddArea();
        }

        protected void lvAreas_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            var index = e.CommandArgument.ToString();

            if (e.CommandName == "Starred")
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var area = (from a in db.WidgetAreaSet
                                where a.Id == index
                                select a).SingleOrDefault();

                    if (area != null)
                    {
                        area.Starred = !area.Starred;
                        db.SaveChanges();
                    }
                }
                e.Handled = true;
                Response.Redirect(Request.Url.ToString());
            }
            else if (e.CommandName == "Edit")
            {
                EditForm(index);
                dlgArea.OpenDialog();
                e.Handled = true;
            }
            else if (e.CommandName == "Delete")
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var area = db.WidgetAreaSet.Include("WidgetAreaRoles").Where(a => a.Id.Equals(index)).SingleOrDefault();

                    if (area != null)
                    {
                        string urlPath = MultiTenantsHelper.MountMultiTenantAreaUrl(area.RoutePath);
                        var pathToDelete = (from p in db.AspNetPaths
                                            where p.Path == urlPath
                                            select p).SingleOrDefault();

                        if (pathToDelete != null)
                        {
                            // Rimuove tuitti i widget per le aree shared
                            var allUser = from au in db.AspNetPersonalizationAllUsers
                                          where au.PathId == pathToDelete.Id
                                          select au;
                            db.AspNetPersonalizationAllUsers.RemoveRange(allUser);

                            // Rimuove tuitti i widget per le aree personali dell'utente
                            string userId = User.Identity.GetUserId();
                            var perUser = from au in db.AspNetPersonalizationPerUser
                                          where au.PathId == pathToDelete.Id && au.UserId == userId
                                          select au;
                            db.AspNetPersonalizationPerUser.RemoveRange(perUser);

                            // Remove path
                            db.AspNetPaths.Remove(pathToDelete);
                        }
                        
                        //if (area.WidgetGroupArea)
                        //{
                            foreach (WidgetGroupRoles dbW in db.WidgetGroupRoleSet.ToList())
                            {
                                foreach (WidgetGroupRoles areaW in area.WidgetGroupRoles)
                                {
                                    if (areaW.Id.Equals(dbW.Id))
                                    {
                                        db.WidgetGroupRoleSet.Remove(dbW);
                                        break;
                                    }
                                }
                            }
                        //}
                        //else
                        //{
                            foreach (WidgetAreaRoles dbW in db.WidgetAreaRoleSet.ToList())
                            {
                                foreach (WidgetAreaRoles areaW in area.WidgetAreaRoles)
                                {
                                    if (areaW.Id.Equals(dbW.Id))
                                    {
                                        db.WidgetAreaRoleSet.Remove(dbW);
                                        break;
                                    }
                                }
                            }
                        //}

                        db.WidgetAreaSet.Remove(area);

                        db.SaveChanges();
                    }
                }
                e.Handled = true;
                Response.Redirect(Request.Url.ToString());
            }
        }

        protected void AreaLayout_DataBound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            List<TemplateData> layouts = TemplateHelper.ListLayout;
            for (int i = 0; i < ddl.Items.Count; i++)
            {
                ListItem item = ddl.Items[i];
                item.Attributes["data-image"] = TemplateHelper.GetAbsoluteResourcePath(layouts[i].Icon);
            }
        }


        protected void AreaGroups_DataBound(object sender, EventArgs e)
        {

        }

        private void DataSourceLists()
        {
            List<TemplateData> layouts = TemplateHelper.ListLayout;
            AreaLayout.DataSource = layouts;
            AreaLayout.DataBind();

            List<ListItem> dataPublicAreaVisibility = new List<ListItem>();
            dataPublicAreaVisibility.Add(new ListItem("Public", "*"));
            dataPublicAreaVisibility.Add(new ListItem("Private", User.Identity.GetUserId()));
            ChkPublicAreaVisibility.DataSource = dataPublicAreaVisibility;
            ChkPublicAreaVisibility.DataBind();

            WidgetAreaService widgetAreaService = new WidgetAreaService();
            string userId = User.Identity.GetUserId();
            List<WidgetArea> groups = widgetAreaService.FindAllGroupedActiveAreaByApplicationAndUserId(applicationId, userId,
                        ReflectionUtility.GetPropertyName(() => new WidgetArea().Title), true);
            groups.Insert(0, new WidgetArea() { Id = "null", Title = "-- Nessun gruppo --" });
            AreaGroupsField.DataSource = groups;
            AreaGroupsField.DataBind();

            GridViewDataBind();
        }

        protected void InsertForm(bool groupArea)
        {
            DataSourceLists();
            initializeRolesPermission();
            TitleTextBox.Text = "";
            IconSelector.SelectedIcon = "";
            DescriptionBox.Text = "";
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    OrderBox.Text = ((from a in db.WidgetAreaSet
                                      where a.ApplicationId == MultiTenantsHelper.ApplicationId
                                      select a.Order).Max() + 1).ToString();
                }
                catch (InvalidOperationException)
                {
                    //non ci sono aree
                    OrderBox.Text = 0.ToString();
                }
            }

            WidgetGroupAreaField.Value = groupArea.ToString();

            if (groupArea)
            {
                AreaGroupsFields.Visible = false;
                AreaLayoutFields.Visible = false;
                Label1.Text = "Nome Gruppo";
                Label4.Text = "Gruppo";
            }
            else
            {
                Label1.Text = "Nome Area";
                Label4.Text = "Area";
                AreaGroupsFields.Visible = true;
                AreaLayoutFields.Visible = true;
                AreaLayout.SelectedValue = "Default.ascx";
            }

            ChkPublicAreaVisibility.SelectedValue = User.Identity.GetUserId();

            TitleTextBox.Enabled = true;
            UpdateButton.Visible = false;
            InsertButton.Visible = true;
            CancelButton.Visible = true;
        }

        private void initializeRolesPermission()
        {
            foreach (GridViewRow rw in DgvRoles.Rows)
            {
                CheckBox chkEditBx = (CheckBox)rw.FindControl("chkAllowEdit");
                if (chkEditBx != null)
                {
                    chkEditBx.Enabled = false;
                    chkEditBx.Checked = false;
                }
            }
        }

        protected void EditForm(string id)
        {
            DataSourceLists();

            using (var db = new ApplicationDbContext())
            {
                var area = (from a in db.WidgetAreaSet
                            where a.Id == id
                            select a).SingleOrDefault();

                IdTextBox.Value = id;
                TitleTextBox.Text = area.Title;
                IconSelector.SelectedIcon = area.Icon;
                DescriptionBox.Text = area.Description;
                OrderBox.Text = area.Order.ToString();

                AreaLayout.SelectedValue = area.Template;
                ChkPublicAreaVisibility.SelectedValue = area.OwnerUserId == null ? "*" : area.OwnerUserId;
                AreaGroupsField.SelectedValue = area.WidgetAreaParentId == null ? "null" : area.WidgetAreaParentId;
                WidgetGroupAreaField.Value = area.WidgetGroupArea.ToString();

                foreach (GridViewRow rw in DgvRoles.Rows)
                {
                    CheckBox chkViewBx = (CheckBox)rw.FindControl("chkAllowView");
                    CheckBox chkVEditBx = (CheckBox)rw.FindControl("chkAllowEdit");

                    if (area.WidgetGroupArea)
                    {
                        if (area.WidgetGroupRoles.Any(i => i.ApplicationRole.Id.Equals(rw.Cells[0].Text)))
                        {
                            chkViewBx.Checked = area.WidgetGroupRoles.FirstOrDefault(i => i.ApplicationRole.Id.Equals(rw.Cells[0].Text)).View;

                            if (chkViewBx.Checked == false)
                            {
                                chkVEditBx.Enabled = false;
                                chkVEditBx.Checked = false;
                            }
                            else
                            {
                                chkVEditBx.Checked = area.WidgetGroupRoles.FirstOrDefault(i => i.ApplicationRole.Id.Equals(rw.Cells[0].Text)).Edit;
                            }
                        }
                        Label1.Text = "Nome Gruppo";
                        Label4.Text = "Gruppo";
                    }
                    else
                    {
                        Label1.Text = "Nome Area";
                        Label4.Text = "Area";
                        if (area.WidgetAreaRoles.Any(i => i.ApplicationRole.Id.Equals(rw.Cells[0].Text)))
                        {
                            chkViewBx.Checked = area.WidgetAreaRoles.FirstOrDefault(i => i.ApplicationRole.Id.Equals(rw.Cells[0].Text)).View;

                            if (chkViewBx.Checked == false)
                            {
                                chkVEditBx.Enabled = false;
                                chkVEditBx.Checked = false;
                            }
                            else
                            {
                                chkVEditBx.Checked = area.WidgetAreaRoles.FirstOrDefault(i => i.ApplicationRole.Id.Equals(rw.Cells[0].Text)).Edit;
                            }
                        }
                    }
                }

                if (area.WidgetGroupArea)
                {
                    AreaGroupsFields.Visible = false;
                    AreaLayoutFields.Visible = false;
                }
                else
                {
                    AreaGroupsFields.Visible = true;
                    AreaLayoutFields.Visible = true;
                    AreaLayout.SelectedValue = "Default.ascx";
                }

                TitleTextBox.Enabled = false;
                UpdateButton.Visible = true;
                UpdateButton.CommandArgument = id;
                InsertButton.Visible = false;
                CancelButton.Visible = true;
            }
        }

        private bool isAreaOpened = false;
        protected void insertArea_Click(object sender, EventArgs e)
        {
            isAreaOpened = true;
            InsertForm(false);
            dlgArea.OpenDialog();
        }

        protected void insertGroupArea_Click(object sender, EventArgs e)
        {
            isAreaOpened = false;
            InsertForm(true);
            dlgArea.OpenDialog();
        }

        private void InsertOrUpdate(WidgetArea widgetArea, bool insert)
        {
            int areaOrder = 0;
            if (!int.TryParse(OrderBox.Text, out areaOrder))
            {
                areaOrder = 0;
            }
            widgetArea.Title = TitleTextBox.Text;
            widgetArea.Icon = IconSelector.SelectedIcon;
            widgetArea.Description = DescriptionBox.Text;
            widgetArea.Order = areaOrder;
            widgetArea.Template = AreaLayout.SelectedValue ?? "Default.ascx";
            widgetArea.WidgetAreaParentId = AreaGroupsField.SelectedValue == "null" ? null : AreaGroupsField.SelectedValue;
            widgetArea.OwnerUserId = ChkPublicAreaVisibility.SelectedValue == "*" ? null : ChkPublicAreaVisibility.SelectedValue;

            if (insert)
            {
                widgetArea.ApplicationId = applicationId;
                widgetArea.Starred = false;
                widgetArea.Visible = true;
                widgetArea.RoutePath = TitleTextBox.Text.Trim().Replace(" ", "_");
                widgetArea.PublishDate = DateTime.Now;
                widgetArea.ExpirationDate = null;
                widgetArea.WidgetGroupArea = bool.Parse(WidgetGroupAreaField.Value);
            }
        }

        protected void InsertButton_Command(object sender, CommandEventArgs e)
        {
            if (NameAlreadyExistsValidator.IsValid)
            {
                WidgetArea widgetArea = new WidgetArea();
                widgetArea.WidgetAreaRoles = new List<WidgetAreaRoles>();
                using (var db = new ApplicationDbContext())
                {
                    if (AreaGroupsFields.Visible)
                    {
                        GetWidgetAreaRoles(widgetArea, db);
                    }
                    else
                    {
                        widgetArea.WidgetGroupRoles = new List<WidgetGroupRoles>();
                        GetWidgetGroupRoles(widgetArea, db); ;
                    }
                    
                    InsertOrUpdate(widgetArea, true);
                    db.WidgetAreaSet.Add(widgetArea);
                    db.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
                    db.SaveChanges();                    
                }
                Response.Redirect(Request.Url.ToString());
            }
        }

        protected void GetWidgetAreaRoles(WidgetArea widgetArea, ApplicationDbContext db)
        {
            foreach (GridViewRow rw in DgvRoles.Rows)
             {
                var roleId = rw.Cells[0].Text;
                WidgetAreaRoles war = db.WidgetAreaRoleSet.FirstOrDefault(x => x.WidgetArea.Id.Equals(widgetArea.Id) && x.ApplicationRole.Id.Equals(roleId));
                ApplicationRole appRole = db.Roles.FirstOrDefault(x => x.Id == roleId);
                
                if (war == null)
                {
                    war = new WidgetAreaRoles();
                    war.Id = Guid.NewGuid().ToString();
                    war.ApplicationRole = appRole;
                }

                CheckBox chkViewBx = (CheckBox)rw.FindControl("chkAllowView");
                if (chkViewBx != null)
                {
                    war.View = chkViewBx.Checked;
                }

                CheckBox chkEditBx = (CheckBox)rw.FindControl("chkAllowEdit");
                if (chkEditBx != null)
                {
                    war.Edit = chkEditBx.Checked;
                }

                war.WidgetArea = widgetArea;
                widgetArea.WidgetAreaRoles.Add(war);
                //war.WidgetArea.WidgetAreaRoles.Add(war);
            }
        }

        protected void GetWidgetGroupRoles(WidgetArea widgetArea, ApplicationDbContext db)
        {
            foreach (GridViewRow rw in DgvRoles.Rows)
            {
                var roleId = rw.Cells[0].Text;
                WidgetGroupRoles wgr = db.WidgetGroupRoleSet.FirstOrDefault(x => x.WidgetArea.Id.Equals(widgetArea.Id) && x.ApplicationRole.Id.Equals(roleId));
                ApplicationRole appRole = db.Roles.FirstOrDefault(x => x.Id == roleId);

                if (wgr == null)
                {
                    wgr = new WidgetGroupRoles();
                    wgr.ApplicationRole = appRole;
                    wgr.Id = Guid.NewGuid().ToString();
                }

                CheckBox chkViewBx = (CheckBox)rw.FindControl("chkAllowView");
                if (chkViewBx != null)
                {
                    wgr.View = chkViewBx.Checked;
                }

                CheckBox chkEditBx = (CheckBox)rw.FindControl("chkAllowEdit");
                if (chkEditBx != null)
                {
                    wgr.Edit = chkEditBx.Checked;
                }

                wgr.WidgetArea = widgetArea;

                widgetArea.WidgetGroupRoles.Add(wgr);
            }
        }

        protected void UpdateButton_Command(object sender, CommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            using (var db = new ApplicationDbContext())
            {
                var widgetArea = (from a in db.WidgetAreaSet where a.Id == id select a).SingleOrDefault();

                if (widgetArea.WidgetGroupArea)
                {
                    GetWidgetGroupRoles(widgetArea, db);
                }
                else
                {
                    GetWidgetAreaRoles(widgetArea, db);
                }

                InsertOrUpdate(widgetArea, false);

                db.SaveChanges();
                Response.Redirect(Request.Url.ToString());
            }
        }

        protected void NameAlreadyExistsValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            WidgetAreaRepository widgetAreaRepository = new WidgetAreaRepository();
            args.IsValid = widgetAreaRepository.FindWidgetAreaByRoutePath(args.Value) == null;
        }

        protected void chkAllowView_CheckedChanged(object sender, EventArgs e)
        {
            var viewChkBox = DgvRoles.Rows[Convert.ToInt32((sender as CheckBox).AccessKey)].Cells[3];
            var editChkBox = DgvRoles.Rows[Convert.ToInt32((sender as CheckBox).AccessKey)].Cells[3];
            CheckBox chk = (CheckBox)editChkBox.FindControl("chkAllowEdit");
            
            if ((sender as CheckBox).Checked == true)
            {
                chk.Enabled = true;
            }
            else
            {
                chk.Enabled = false;
                chk.Checked = false;
            }
        }
    }
}