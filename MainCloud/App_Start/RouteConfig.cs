﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace MainCloudFramework
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Permanent;
            routes.EnableFriendlyUrls(settings);

            routes.Ignore("{*allaxd}", new { allaxd = @".*\.axd(/.*)?" });
            //routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //routes.Ignore("{resource}.svc/{*pathInfo}");
            //routes.Ignore(("{file}.svn"));
            //routes.Ignore("{*allcss}", new { allcss = @".*\.css(/.*)?" });
            //routes.Ignore("{*alljpg}", new { alljpg = @".*\.jpg(/.*)?" });
            routes.Ignore("{*svc}", new { alljs = @".*\.svc(/.*)?" });
            //routes.Ignore("{*aspx}", new { allaspx = @".*\.aspx(/.*)?" });
            //routes.Ignore("{*MsAjaxJs}", new { allaspx = @".*\.MsAjaxJs(/.*)?" });
            //routes.Ignore("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });


            Rooting.RoutingPageMap.RegisterAreaRoutes(routes);

            //routes.MapRoute(
            //        name: "Default",
            //        url: "{controller}/{action}/{id}",
            //        defaults: new { action = "Index", id = UrlParameter.Optional }
            //    );


        }
    }
}