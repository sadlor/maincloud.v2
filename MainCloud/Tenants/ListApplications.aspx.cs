﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Tenants
{
    public partial class ListApplications : System.Web.UI.Page
    {
        private ApplicationService applicationService = new ApplicationService();

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (applicationService.GetApplications().Any(x => x.Name == "Sattec"))
            //{
            //    var r = applicationService.GetCurrentApplicationUserRole();
            //    try
            //    {
            //        var userId = HttpContext.Current.User.Identity.GetUserId();
            //        var user = applicationService.FindUserById(userId);
            //        if (user.Roles.Any(x => x.RoleId == "cf945cce-93ac-41a3-b568-b2dc1a720609"))  //if ruolo = operatore
            //        {
            //            //redirect area rilievi
            //            MainCloudFramework.Repositories.WidgetAreaRepository areaService = new MainCloudFramework.Repositories.WidgetAreaRepository();
            //            MainCloudFramework.Models.WidgetArea wa = areaService.FindByID("edfbe16b-5b88-4a3d-b3cd-9d3817e4b6a3");  //Id area rilievi
            //            Response.Redirect("App/Sattec/Area/" + wa.RoutePath);
            //        }
            //    }
            //    catch (Exception ex)
            //    { }
            //}

            rptApplications.DataSource = applicationService.GetApplications();
            rptApplications.DataBind();
        }
    }
}