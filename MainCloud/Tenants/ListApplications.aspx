﻿<%@ Page Title="Applicazioni" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListApplications.aspx.cs" Inherits="MainCloud.Tenants.ListApplications" %>
<%@ Import Namespace="System.Threading" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3><%: GetLocalResourceObject("ApplicationsList.Title") %></h3>
    <p><%: GetLocalResourceObject("ApplicationsList.SubTitle") %></p>
<%--
    <div>
        Current Culture: <b><%= Thread.CurrentThread.CurrentCulture.IetfLanguageTag %></b>
    </div>
    <div>
        Current UI Culture: <b><%= Thread.CurrentThread.CurrentUICulture.IetfLanguageTag %></b>
    </div>
--%>
    <asp:Repeater ID="rptApplications" runat="server">
        <ItemTemplate>
            <mcf:MultitenantNavigateLinkButton runat="server" Text='<%# Eval("Name") %>' RouthPath='<%# Eval("Url") %>' />
            <br>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
