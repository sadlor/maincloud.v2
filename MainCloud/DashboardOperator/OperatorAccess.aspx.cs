﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web.Multitenants;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using static MES.Core.MesEnum;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using MES.Helper;
using AssetManagement.Services;
using System.Web.UI.HtmlControls;

namespace MainCloudFramework.DashboardOperator
{
    public partial class OperatorAccess : BasePage
    {
        TransactionService transService = new TransactionService();
        TransactionOperatorService transOperService = new TransactionOperatorService();
        GrantOperator grantService = new GrantOperator();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region MachineTable
        private class AssetTransaction
        {
            public string AssetId { get; set; }
            public string Asset { get; set; }
            public long Counter { get; set; }
            public string Status { get; set; }
            public string Job { get; set; }
            public string Article { get; set; }
            public string Operator { get; set; }
            public string Cause { get; set; }
            public string Order { get; set; }
            public string Quality { get; set; }

            public AssetTransaction(string assetId, string assetDescription, long counter, string status, string job, string article, string op, string order, string cause, string quality)
            {
                AssetId = assetId;
                Asset = assetDescription;
                Counter = counter;
                Status = status;
                Job = job;
                Article = article;
                Order = order;
                Operator = op;
                Cause = cause;
                Quality = quality;
            }
        }

        protected void machineTable_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            string zoneId = string.Empty;
            if (Session.SessionMultitenant().ContainsKey(MesConstants.DEPARTMENT_ZONE) && !string.IsNullOrEmpty(Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString()))
            {
                zoneId = Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString();
            }
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            List<Asset> assetList = AS.GetAssetListByZoneId(zoneId);
            List<AssetTransaction> dataSource = new List<AssetTransaction>();
            foreach (var asset in assetList)
            {
                Transaction lastTransOpen = transService.GetLastTransactionOpen(asset.Id);
                if (lastTransOpen != null)
                {
                    string operatorId = lastTransOpen.OperatorId;
                    if (!string.IsNullOrEmpty(operatorId))
                    {
                        OperatorService OS = new OperatorService();
                        operatorId = OS.GetOperatorNameById(operatorId, OperatorHelper.ApplicationId);
                    }
                    string jobId = lastTransOpen.JobId;
                    string orderId = "";
                    string articleId = lastTransOpen.IdArticle.ToString();
                    if (!string.IsNullOrEmpty(jobId))
                    {
                        JobRepository JRep = new JobRepository();
                        Job job = JRep.FindByID(jobId);
                        jobId = job.Code;
                        orderId = job.Order?.CustomerOrder?.OrderCode;  //get commessa by job
                        articleId = job.Order?.Article?.Code;   //get articolo by job
                    }
                    //if (!string.IsNullOrEmpty(articleId))
                    //{
                    //    ArticleService ArtS = new ArticleService();
                    //    articleId = ArtS.GetArticleDescriptionById(articleId);
                    //}
                    string causeId = lastTransOpen.CauseId;
                    if (!string.IsNullOrEmpty(causeId))
                    {
                        CauseService CauseS = new CauseService();
                        causeId = CauseS.GetCauseDescriptionById(causeId, OperatorHelper.ApplicationId);
                    }

                    QualityLevelService qlService = new QualityLevelService();
                    MES.Models.QualityLevel ql = qlService.GetQualityLevel(asset.Id, jobId);
                    string quality = ql != null ? ql.Level : "";

                    dataSource.Add(new AssetTransaction(asset.Id,
                        asset.Code.Trim() + " - " + asset.Description.Trim(),
                        (long)lastTransOpen.PartialCounting,
                        lastTransOpen.Status ? "On" : "Off",
                        jobId, articleId, operatorId, orderId, causeId, quality));
                }
                else
                {
                    //la macchina non ha transazioni o è automatica
                    dataSource.Add(new AssetTransaction(asset.Id,
                        asset.Code + " - " + asset.Description,
                        0,
                        "Off",
                        null, null, null, null, null, null));
                }
            }

            machineTable.DataSource = dataSource;
        }

        protected void machineTable_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;

                string assetId = item.GetDataKeyValue("AssetId").ToString();

                SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
                if (!boxService.IsMachineConnected(assetId))
                {
                    item["Status"].Text = "M";
                }

                (item["Asset"].Controls[0] as Button).PostBackUrl = "MachinePage.aspx?asset=" + assetId; //TODO: criptare assetId se necessario

                (item["Asset"].Controls[0] as Button).BackColor = transService.GetCauseColor(assetId, OperatorHelper.ApplicationId);
                //item["Asset"].BackColor = transService.GetCauseColor(assetId, OperatorHelper.ApplicationId);

                QualityLevelService qlService = new QualityLevelService();
                (item["QualityLevel"].Controls[1] as HtmlGenericControl).Style.Add(HtmlTextWriterStyle.BackgroundColor, qlService.GetColor(assetId, transService.GetLastTransactionOpen(assetId)?.JobId).Name);
            }
        }
        #endregion
    }
}