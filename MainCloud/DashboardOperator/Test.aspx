﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="MainCloud.DashboardOperator.Test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="TestStyle.css" />
    <link href="Style.css" rel="stylesheet" type="text/css" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery"/>
                <asp:ScriptReference Name="jquery.ui.combined" />
                <asp:ScriptReference Name="bootstrap"/>
                <asp:ScriptReference Name="respond"/>
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <asp:HiddenField runat="server" ID="ScrollPosition" /> <%-- x info http://stackoverflow.com/questions/7712555/persist-browser-scroll-position --%>

        <telerik:RadAjaxManager ID="AjaxManagerMaster" runat="server">
        </telerik:RadAjaxManager>

        <div class="pageLayout">
            <telerik:RadPageLayout runat="server" GridType="Fluid" ShowGrid="true" HtmlTag="None" >
                <telerik:LayoutRow RowType="Generic">
                    <Rows>
                        <%--<telerik:LayoutRow RowType="Generic" CssClass="header">
                            <Rows>
                                <telerik:LayoutRow RowType="Container" WrapperHtmlTag="Div">
                                    <Columns>
                                        <telerik:LayoutColumn Span="6" SpanMd="6" SpanSm="12" SpanXs="12">
                                            <h1>Responsive Web Design</h1>
                                            <p>The concept of RWD suggests that the layout of the project needs to adapt to the media that renders it</p>
                                        </telerik:LayoutColumn>
 
                                        <telerik:LayoutColumn HiddenXs="true" Span="6" SpanMd="6" SpanSm="12" SpanXs="12">
                                            <img src="Images/BigScreen.png" />
                                        </telerik:LayoutColumn>
                                    </Columns>
                                </telerik:LayoutRow>
                            </Rows>
                        </telerik:LayoutRow>--%>
 
                        <telerik:LayoutRow RowType="Generic" CssClass="content">
                            <Rows>
                                <telerik:LayoutRow RowType="Container" WrapperHtmlTag="Div">
                                    <Columns>
                                        <telerik:CompositeLayoutColumn Span="8" SpanMd="8" SpanSm="8" SpanXs="8">
                                            <Rows>
                                                <telerik:LayoutRow RowType="Container" WrapperHtmlTag="Div">
                                                    <Columns>
                                                        <telerik:LayoutColumn Span="3" SpanSm="3" SpanXs="3">
                                                            <telerik:RadButton runat="server" ID="btnOperatorLogin" CommandName="Login" CssClass="btnActivity" OnClick="btnOperatorLogin_Click" EnableViewState="false">
                                                                <ContentTemplate>
                                                                    <center><i class="fa fa-sign-in" aria-hidden="true"></i> Inizio</center>
                                                                </ContentTemplate>
                                                            </telerik:RadButton>
                                                        </telerik:LayoutColumn>
                                                        <telerik:LayoutColumn Span="3" SpanSm="3" SpanXs="3">
                                                            <telerik:RadButton runat="server" ID="btnOperatorLogout" CssClass="btnActivityTable" DisabledButtonCssClass="btnActivityTableDisabled" CommandName="Logout" OnClick="btnOperatorLogout_Click" EnableViewState="false">
                                                                <ContentTemplate>
                                                                    <center><i class="fa fa-sign-out" aria-hidden="true"></i> Fine</center>
                                                                </ContentTemplate>
                                                            </telerik:RadButton>
                                                        </telerik:LayoutColumn>
                                                        <telerik:LayoutColumn Span="3" SpanSm="3" SpanXs="3" Push="3">
                                                            <telerik:RadButton runat="server" ID="btnAddOperator" CommandName="AddOperator" CssClass="btnActivity" OnClick="btnAddOperator_Click" EnableViewState="false">
                                                                <ContentTemplate>
                                                                    <center><i class="fa fa-plus-circle" aria-hidden="true"></i> Altri</center>
                                                                </ContentTemplate>
                                                            </telerik:RadButton>
                                                        </telerik:LayoutColumn>
                                                    </Columns>
                                                </telerik:LayoutRow>
                                            </Rows>
                                            <Rows>
                                                <telerik:LayoutRow RowType="Container" WrapperHtmlTag="Div">
                                                    <Columns>
                                                        <telerik:LayoutColumn Span="2" SpanMd="2" SpanSm="2" SpanXs="2" CssClass="noPadding">
                                                            <asp:Label Text="Commessa:" runat="server" AssociatedControlID="txtCustomerOrder" Width="85px" style="line-height:2" EnableViewState="false" />
                                                        </telerik:LayoutColumn>
                                                        <telerik:LayoutColumn Span="4" SpanMd="4" SpanSm="4" SpanXs="4" CssClass="noPadding">
                                                            <telerik:RadButton runat="server" ID="txtCustomerOrder" Enabled="false" BackColor="White" ForeColor="Black" Width="201px" Font-Bold="true" EnableViewState="false" /> 
                                                        </telerik:LayoutColumn>
                                                        <telerik:LayoutColumn Span="2" SpanMd="2" SpanSm="2" SpanXs="2" CssClass="noPadding">
                                                            <asp:Label Text="Ord:" runat="server" AssociatedControlID="txtOrderQtyOrdered" style="line-height:2" />
                                                            <telerik:RadButton runat="server" ID="txtOrderQtyOrdered" Enabled="false" BackColor="White" ForeColor="Black" Font-Bold="true" EnableViewState="false" />
                                                        </telerik:LayoutColumn>
                                                        <telerik:LayoutColumn Span="2" SpanMd="2" SpanSm="2" SpanXs="2" CssClass="noPadding">
                                                            <asp:Label Text="Prod:" runat="server" AssociatedControlID="txtOrderQtyProduced" style="line-height:2" />
                                                            <telerik:RadButton runat="server" ID="txtOrderQtyProduced" Enabled="false" BackColor="White" ForeColor="Black" Font-Bold="true" EnableViewState="false" />
                                                        </telerik:LayoutColumn>
                                                        <telerik:LayoutColumn Span="2" SpanMd="2" SpanSm="2" SpanXs="2" CssClass="noPadding">
                                                            <asp:Label Text="Res:" runat="server" AssociatedControlID="txtOrderQtyLeft" style="line-height:2" />
                                                            <telerik:RadButton runat="server" ID="txtOrderQtyLeft" Enabled="false" BackColor="White" ForeColor="Black" Font-Bold="true" EnableViewState="false" />
                                                        </telerik:LayoutColumn>
                                                    </Columns>
                                                </telerik:LayoutRow>
                                            </Rows>
                                            <Rows>
                                                <telerik:LayoutRow RowType="Container" WrapperHtmlTag="Div">
                                                    <Columns>
                                                        <telerik:LayoutColumn Span="2" SpanMd="2" SpanSm="2" SpanXs="2" CssClass="noPadding">
                                                            <asp:Label Text="Articolo:" runat="server" AssociatedControlID="txtArticleCode" Width="85px" style="line-height:2" EnableViewState="false" />
                                                        </telerik:LayoutColumn>
                                                        <telerik:LayoutColumn Span="4" SpanMd="4" SpanSm="4" SpanXs="4" CssClass="noPadding">
                                                            <telerik:RadButton runat="server" ID="txtArticleCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" EnableViewState="false" />
                                                        </telerik:LayoutColumn>
                                                        <telerik:LayoutColumn Span="6" SpanMd="6" SpanSm="6" SpanXs="6" CssClass="noPadding">
                                                            <telerik:RadButton runat="server" ID="txtArticleDescription" Width="100%" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" EnableViewState="false" />
                                                        </telerik:LayoutColumn>
                                                    </Columns>
                                                </telerik:LayoutRow>
                                            </Rows>
                                            <Rows>
                                                <telerik:LayoutRow RowType="Container" WrapperHtmlTag="Div">
                                                    <Columns>
                                                        <telerik:LayoutColumn Span="2" SpanMd="2" SpanSm="2" SpanXs="2" CssClass="noPadding">
                                                            <asp:Label Text="Stampo:" runat="server" AssociatedControlID="txtMold" Width="85px" style="line-height:2" EnableViewState="false" />
                                                        </telerik:LayoutColumn>
                                                        <telerik:LayoutColumn Span="4" SpanMd="4" SpanSm="4" SpanXs="4" CssClass="noPadding">
                                                            <telerik:RadButton runat="server" ID="txtMold" Enabled="false" BackColor="White" ForeColor="Black" Width="201px" EnableViewState="false" />
                                                        </telerik:LayoutColumn>
                                                        <telerik:LayoutColumn Span="2" SpanMd="2" SpanSm="2" SpanXs="2" CssClass="noPadding">
                                                            <telerik:RadButton runat="server" ID="btnCavityMold" Enabled="false" BackColor="White" ForeColor="Black" EnableViewState="false" />
                                                        </telerik:LayoutColumn>
                                                        <telerik:LayoutColumn Span="4" SpanMd="4" SpanSm="4" SpanXs="4" CssClass="noPadding">
                                                            <telerik:RadButton runat="server" ID="txtNStampateOra" Enabled="false" BackColor="White" ForeColor="Black" ReadOnly="true" Width="100%" EnableViewState="false" />
                                                        </telerik:LayoutColumn>
                                                    </Columns>
                                                </telerik:LayoutRow>
                                            </Rows>
                                            <Rows>
                                                <telerik:LayoutRow RowType="Container" WrapperHtmlTag="Div">
                                                    <Columns>
                                                    </Columns>
                                                </telerik:LayoutRow>
                                            </Rows>
                                        </telerik:CompositeLayoutColumn>
                                        <telerik:LayoutColumn Span="4" SpanMd="4" SpanSm="4" SpanXs="4">
                                            <asp:Panel runat="server" ID="pnlChart" onclick="RefreshChart()" style="z-index:1000;" EnableViewState="false">
                                                <asp:Button runat="server" ID="btnRefreshChart" OnClick="btnRefreshChart_Click" style="display:none;" EnableViewState="false" />
                                                <telerik:RadHtmlChart runat="server" ID="chartOEE" Height="225px" EnableViewState="false">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:ColumnSeries DataFieldY="IndexValue" ColorField="IndexColor" Stacked="false" Gap="1.5" Spacing="0.4">
                                                                <LabelsAppearance DataFormatString="{0}" Position="OutsideEnd">
                                                                    <TextStyle Bold="true" Margin="-5" />
                                                                </LabelsAppearance>
                                                                <TooltipsAppearance DataFormatString="{0}" Color="White"></TooltipsAppearance>
                                                            </telerik:ColumnSeries>
                                                        </Series>
                                                        <Appearance>
                                                            <FillStyle BackgroundColor="Transparent"></FillStyle>
                                                        </Appearance>
                                                        <XAxis AxisCrossingValue="0" Color="black" MajorTickType="Outside" MinorTickType="Outside"
                                                            Reversed="false">
                                                            <Items>
                                                                <telerik:AxisItem LabelText="D"></telerik:AxisItem>
                                                                <telerik:AxisItem LabelText="E"></telerik:AxisItem>
                                                                <telerik:AxisItem LabelText="Q"></telerik:AxisItem>
                                                                <telerik:AxisItem LabelText="OEE"></telerik:AxisItem>
                                                            </Items>
                                                            <LabelsAppearance DataFormatString="{0}" RotationAngle="0" Skip="0" Step="1"></LabelsAppearance>
                                                            <TitleAppearance Position="Center" RotationAngle="0" Text="">
                                                            </TitleAppearance>
                                                        </XAxis>
                                                        <YAxis AxisCrossingValue="0" Color="black" MajorTickSize="1" MajorTickType="Outside" MinValue="0" MaxValue="100"
                                                            MinorTickType="None" Reversed="false">
                                                            <LabelsAppearance DataFormatString="{0}" RotationAngle="0" Skip="0" Step="1"></LabelsAppearance>
                                                            <TitleAppearance Position="Center" RotationAngle="0" Text="%">
                                                            </TitleAppearance>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Appearance>
                                                        <FillStyle BackgroundColor="Transparent"></FillStyle>
                                                    </Appearance>
                                                    <Legend>
                                                        <Appearance Visible="false" BackgroundColor="Transparent" Position="Bottom">
                                                        </Appearance>
                                                    </Legend>
                                                </telerik:RadHtmlChart>
                                            </asp:Panel>
                                        </telerik:LayoutColumn>
                                    </Columns>
                                </telerik:LayoutRow>
                            </Rows>
                        </telerik:LayoutRow>
                    </Rows>
                </telerik:LayoutRow>
            </telerik:RadPageLayout>
        </div>



        <%--
                            
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2 textNoPadding">
                                    <asp:Label Text="MP:" runat="server" AssociatedControlID="txtMPCode" Width="85px" style="line-height:2" EnableViewState="false" />
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 noPaddingLeft">
                                    <telerik:RadButton runat="server" ID="txtMPCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" EnableViewState="false" />
                                </div>
                                <div class="col-xs-custom-6 col-sm-custom-6 col-md-custom-6 col-lg-custom-6 textNoPadding">
                                    <telerik:RadButton runat="server" ID="txtMPDescription" Width="100%" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" EnableViewState="false" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2 textNoPadding">
                                    <asp:Label Text="Lotto:" runat="server" AssociatedControlID="txtMPCode" Width="85px" style="line-height:2" EnableViewState="false" />
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 noPaddingLeft">
                                    <telerik:RadTextBox runat="server" ID="txtBatchCode" Width="201px" Height="34px" EnableViewState="false" />--%>
    

    </form>
</body>
</html>