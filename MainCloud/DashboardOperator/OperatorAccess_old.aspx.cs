﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web.Multitenants;
using MES.Core;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using static MES.Core.MesEnum;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using MES.Helper;
using AssetManagement.Services;
using System.Web.UI.HtmlControls;

namespace MainCloudFramework.DashboardOperator
{
    public partial class OperatorAccess_old : BasePage
    {
        TransactionService transService = new TransactionService();
        TransactionOperatorService transOperService = new TransactionOperatorService();
        GrantOperator grantService = new GrantOperator();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region MachineTable
        private class AssetTransaction
        {
            public string AssetId { get; set; }
            public string Asset { get; set; }
            public long Counter { get; set; }
            public string Status { get; set; }
            public string Job { get; set; }
            public string Article { get; set; }
            public string Operator { get; set; }
            public string Cause { get; set; }
            public string Order { get; set; }
            public string Quality { get; set; }

            public AssetTransaction(string assetId, string assetDescription, long counter, string status, string job, string article, string op, string order, string cause, string quality)
            {
                AssetId = assetId;
                Asset = assetDescription;
                Counter = counter;
                Status = status;
                Job = job;
                Article = article;
                Order = order;
                Operator = op;
                Cause = cause;
                Quality = quality;
            }
        }

        protected void machineTable_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            string zoneId = string.Empty;
            if (Session.SessionMultitenant().ContainsKey(MesConstants.DEPARTMENT_ZONE) && !string.IsNullOrEmpty(Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString()))
            {
                zoneId = Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString();
            }
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            List<Asset> assetList = AS.GetAssetListByZoneId(zoneId);
            List<AssetTransaction> dataSource = new List<AssetTransaction>();
            foreach (var asset in assetList)
            {
                Transaction lastTransOpen = transService.GetLastTransactionOpen(asset.Id);
                if (lastTransOpen != null)
                {
                    string operatorId = lastTransOpen.OperatorId;
                    if (!string.IsNullOrEmpty(operatorId))
                    {
                        OperatorService OS = new OperatorService();
                        operatorId = OS.GetOperatorNameById(operatorId, OperatorHelper.ApplicationId);
                    }
                    string jobId = lastTransOpen.JobId;
                    string orderId = "";
                    string articleId = lastTransOpen.IdArticle.ToString();
                    if (!string.IsNullOrEmpty(jobId))
                    {
                        JobRepository JRep = new JobRepository();
                        Job job = JRep.FindByID(jobId);
                        jobId = job.Code;
                        orderId = job.Order?.CustomerOrder?.OrderCode;  //get commessa by job
                        articleId = job.Order?.Article?.Code;   //get articolo by job
                    }
                    //if (!string.IsNullOrEmpty(articleId))
                    //{
                    //    ArticleService ArtS = new ArticleService();
                    //    articleId = ArtS.GetArticleDescriptionById(articleId);
                    //}
                    string causeId = lastTransOpen.CauseId;
                    if (!string.IsNullOrEmpty(causeId))
                    {
                        CauseService CauseS = new CauseService();
                        causeId = CauseS.GetCauseDescriptionById(causeId, OperatorHelper.ApplicationId);
                    }

                    QualityLevelService qlService = new QualityLevelService();
                    MES.Models.QualityLevel ql = qlService.GetQualityLevel(asset.Id, jobId);
                    string quality = ql != null ? ql.Level : "";

                    dataSource.Add(new AssetTransaction(asset.Id,
                        asset.Code.Trim() + " - " + asset.Description.Trim(),
                        (long)lastTransOpen.PartialCounting,
                        lastTransOpen.Status ? "On" : "Off",
                        jobId, articleId, operatorId, orderId, causeId, quality));
                }
                else
                {
                    //la macchina non ha transazioni o è automatica
                    dataSource.Add(new AssetTransaction(asset.Id,
                        asset.Code + " - " + asset.Description,
                        0,
                        "Off",
                        null, null, null, null, null, null));
                }
            }

            machineTable.DataSource = dataSource;
        }

        protected void machineTable_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName != RadGrid.ExpandCollapseCommandName)
            {
                //TransactionService TS = new TransactionService();

                string assetId = (e.Item as GridDataItem).GetDataKeyValue("AssetId").ToString();
                hiddenMachineId.Value = assetId;
                AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
                Transaction lastTransOpen = transService.GetLastTransactionOpen(assetId);

                switch (e.CommandName)
                {
                    case "Login":
                        ModifyMachineTransaction.Value = bool.TrueString;

                        btnSetUp.Enabled = false;
                        btnSetUp.CssClass = "btnActivityDisabled";
                        btnProduction.Enabled = false;
                        btnProduction.CssClass = "btnActivityDisabled";
                        btnMaintenance.Enabled = false;
                        btnMaintenance.CssClass = "btnActivityDisabled";

                        btnConfirmConnect.Enabled = false;
                        btnConfirmConnect.CssClass = "btnActivityDisabled";

                        txtCustomerOrder.Enabled = false;
                        btnCavityMold.Enabled = false;
                        txtMold.Enabled = false;

                        //txtBadge.Text = transService.GetLastOperatorOnMachine(assetId)?.Badge;
                        //txtBadge_TextChanged(txtBadge, EventArgs.Empty);

                        txtCustomerOrder.Text = lastTransOpen?.Job?.Order?.CustomerOrder?.OrderCode;
                        txtCustomerOrder.Value = "";
                        txtNStampateOra.Text = string.Format("N stampate ora = {0}", lastTransOpen?.Job?.StandardRate);
                        txtNStampateOra.Value = lastTransOpen?.Job?.StandardRate.ToString();
                        btnCavityMold.Text = string.Format("N impronte = {0}", lastTransOpen?.Job?.CavityMoldNum);
                        btnCavityMold.Value = lastTransOpen?.Job?.CavityMoldNum.ToString();
                        txtArticleCode.Text = lastTransOpen?.Job?.Order?.Article?.Code;
                        txtArticleDescription.Text = lastTransOpen?.Job?.Order?.Article?.Description;

                        ArticleMoldService AMS = new ArticleMoldService();
                        if (!string.IsNullOrEmpty(lastTransOpen?.MoldId))
                        {
                            txtMold.Text = AMS.GetMoldById(lastTransOpen?.MoldId).Code;
                        }
                        txtMold.Value = "";

                        OperatorService OS = new OperatorService();
                        if (!string.IsNullOrEmpty(lastTransOpen?.OperatorId))
                        {
                            ConnectWindow.Title = string.Format("Macchina: {0} - Operatore attuale: {1}", AS.GetAssedById(assetId, OperatorHelper.ApplicationId).Description, OS.GetOperatorNameById(lastTransOpen.OperatorId, OperatorHelper.ApplicationId));
                        }
                        else
                        {
                            ConnectWindow.Title = string.Format("Macchina: {0}", AS.GetAssedById(assetId, OperatorHelper.ApplicationId).Description);
                        }
                        ConnectWindow.OpenDialog();
                        break;
                    case "Logout":
                        btnConfirmLogout.Enabled = false;
                        btnConfirmLogout.CssClass = "btnActivityDisabled";

                        hiddenQualityLevelValue.Value = null;
                        pnlAllowConfirmProduction.Visible = false;

                        LogoutWindow.Title = string.Format("Logout - Macchina: {0}", AS.GetAssedById(assetId, OperatorHelper.ApplicationId).Description);
                        LogoutWindow.OpenDialog();
                        gridOperator.Rebind();
                        break;
                    case "AddOperator":
                        ModifyMachineTransaction.Value = bool.FalseString;

                        btnSetUp.Enabled = false;
                        btnSetUp.CssClass = "btnActivityDisabled";
                        btnProduction.Enabled = false;
                        btnProduction.CssClass = "btnActivityDisabled";
                        btnMaintenance.Enabled = false;
                        btnMaintenance.CssClass = "btnActivityDisabled";
                        
                        txtCustomerOrder.Text = lastTransOpen?.Job?.Order?.CustomerOrder?.OrderCode;
                        txtCustomerOrder.Value = "";
                        txtCustomerOrder.Enabled = false;
                        txtNStampateOra.Text = string.Format("N stampate ora = {0}", lastTransOpen?.Job?.StandardRate);
                        txtNStampateOra.Value = "";
                        btnCavityMold.Text = string.Format("N impronte = {0}", lastTransOpen?.Job?.CavityMoldNum);
                        btnCavityMold.Value = "";
                        btnCavityMold.Enabled = false;
                        txtArticleCode.Text = lastTransOpen?.Job?.Order?.Article?.Code;
                        txtArticleDescription.Text = lastTransOpen?.Job?.Order?.Article?.Description;

                        ArticleMoldService AMS2 = new ArticleMoldService();
                        if (!string.IsNullOrEmpty(lastTransOpen?.MoldId))
                        {
                            txtMold.Text = AMS2.GetMoldById(lastTransOpen?.MoldId).Code;
                        }
                        txtMold.Value = "";
                        btnConfirmConnect.Enabled = false;
                        btnConfirmConnect.CssClass = "btnActivityDisabled";
                        ConnectWindow.Title = string.Format("Operatore Aggiuntivo - Macchina: {0}", AS.GetAssedById(assetId, OperatorHelper.ApplicationId).Description);
                        ConnectWindow.OpenDialog();
                        break;
                }
            }
        }

        protected void machineTable_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;

                string assetId = item.GetDataKeyValue("AssetId").ToString();

                SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
                if (!boxService.IsMachineConnected(assetId))
                {
                    item["Status"].Text = "M";
                }

                (item["Asset"].Controls[0] as Button).PostBackUrl = "MachinePage.aspx?asset=" + assetId; //TODO: criptare assetId se necessario

                if (transService.IsOperatorOnMachine(assetId))
                {
                    ((item["btnLogin"].Controls[1] as RadButton).Controls[0] as LiteralControl).Text = ((item["btnLogin"].Controls[1] as RadButton).Controls[0] as LiteralControl).Text.Replace("Inizio", "Cambio");

                    //(item["Asset"].Controls[0] as HyperLink).NavigateUrl = "MachineDetail.aspx?asset=" + assetId; //TODO: criptare assetId se necessario
                    //(item["Asset"].Controls[0] as Button).PostBackUrl = "MachinePage.aspx?asset=" + assetId; //TODO: criptare assetId se necessario
                }
                else
                {
                    ((item["btnLogin"].Controls[1] as RadButton).Controls[0] as LiteralControl).Text = ((item["btnLogin"].Controls[1] as RadButton).Controls[0] as LiteralControl).Text.Replace("Cambio", "Inizio");

                    //(item["Asset"].Controls[0] as Button).Enabled = false;
                    //(item["Asset"].Controls[0] as Button).PostBackUrl = "";
                    //(item["Asset"].Controls[0] as HyperLink).Enabled = false;
                    //(item["Asset"].Controls[0] as HyperLink).NavigateUrl = "";
                }
                if (transOperService.OperatorsOnMachine(assetId).Count > 0)
                {
                    (item["btnLogout"].Controls[1] as RadButton).Enabled = true;
                    (item["btnLogout"].Controls[1] as RadButton).CssClass = "btnActivityTable";
                    ((item["btnLogout"].Controls[1] as RadButton).Controls[0] as LiteralControl).Text = ((item["btnLogout"].Controls[1] as RadButton).Controls[0] as LiteralControl).Text.Replace("Fine", string.Format("Fine <b>{0}</b>",transOperService.OperatorsOnMachine(assetId).Count));
                }
                else
                {
                    (item["btnLogout"].Controls[1] as RadButton).Enabled = false;
                    (item["btnLogout"].Controls[1] as RadButton).CssClass = "btnActivityTableDisabled";
                }

                (item["Asset"].Controls[0] as Button).BackColor = transService.GetCauseColor(assetId, OperatorHelper.ApplicationId);
                //item["Asset"].BackColor = transService.GetCauseColor(assetId, OperatorHelper.ApplicationId);

                QualityLevelService qlService = new QualityLevelService();
                (item["QualityLevel"].Controls[1] as HtmlGenericControl).Style.Add(HtmlTextWriterStyle.BackgroundColor, qlService.GetColor(assetId, transService.GetLastTransactionOpen(assetId)?.JobId).Name);
            }
        }
        #endregion

        #region ConnectOperator
        protected void txtBadge_TextChanged(object sender, EventArgs e)
        {
            //Controlla se esiste la matricola
            //Se esiste mostro il nome riferito alla matricola e il ruolo
            OperatorRepository OR = new OperatorRepository();
            string s = (sender as RadTextBox).Text.Trim();
            if (OR.ReadAll(x => x.ApplicationId == OperatorHelper.ApplicationId && x.Badge == s).Any())
            {
                Operator op = OR.ReadAll(x => x.ApplicationId == OperatorHelper.ApplicationId && x.Badge == s).First();
                txtRegistrationId.Text = op.RegistrationId;
                txtUserName.Text = op.UserName;

                ddlRole.Items.Clear();
                if (grantService.AllowSetup(op.Id))
                {
                    btnSetUp.Enabled = true;
                    btnSetUp.CssClass = "btnActivity";
                    ddlRole.Items.Add("Attrezzaggio");
                }
                if (grantService.AllowMaintenance(op.Id))
                {
                    btnMaintenance.Enabled = true;
                    btnMaintenance.CssClass = "btnActivity";
                    ddlRole.Items.Add("Manutenzione");
                }
                if (grantService.AllowProduction(op.Id))
                {
                    btnProduction.Enabled = true;
                    btnProduction.CssClass = "btnActivity";
                    ddlRole.Items.Add("Produzione");
                }

                if (ddlRole.Items.Count == 1)
                {
                    if (btnSetUp.Enabled)
                    {
                        btnSetUp.CssClass = "setUpColor";
                        btnSetUp.Enabled = false;
                        hiddenMachineState.Value = "Attrezzaggio";
                    }
                    else
                    {
                        if (btnProduction.Enabled)
                        {
                            btnProduction.CssClass = "productionColor";
                            btnProduction.Enabled = false;
                            hiddenMachineState.Value = "Produzione";
                        }
                        else
                        {
                            if (btnMaintenance.Enabled)
                            {
                                btnMaintenance.CssClass = "maintenanceColor";
                                btnMaintenance.Enabled = false;
                                hiddenMachineState.Value = "Manutenzione";
                            }
                        }
                    }
                }

                ddlRole.Items.Clear();
                OperatorRoleService opRoleService = new OperatorRoleService();
                ddlRole.Items.Add(opRoleService.FindUserRoleCurrentApp(op).Name);
                //ddlRole.Enabled = ddlRole.Items.Count == 1 ? false : true;

                if (grantService.AllowSelectJob(op.Id) && ModifyMachineTransaction.Value == bool.TrueString)
                {
                    txtCustomerOrder.Enabled = true;
                }
                if ((grantService.AllowSelectJob(op.Id) || grantService.AllowProduction(op.Id)) && ModifyMachineTransaction.Value == bool.TrueString)
                {
                    btnCavityMold.Enabled = true;
                }
                if (grantService.AllowSelectMold(op.Id) && ModifyMachineTransaction.Value == bool.TrueString)
                {
                    txtMold.Enabled = true;
                }
                //if (grantService.AllowSelectMP(op.Id))
                //{
                //}

                if (ModifyMachineTransaction.Value == bool.FalseString && grantService.AllowQuality(op.Id))
                {
                    btnSelectWorkshift.Visible = true;
                }

                btnConfirmConnect.Enabled = true;
                btnConfirmConnect.CssClass = "btnActivity";
            }
            else
            {
                txtRegistrationId.Text = "";
                txtUserName.Text = "";
                ddlRole.Items.Clear();
                ddlRole.Items.Insert(0, "");
                ddlRole.Enabled = false;
                txtCustomerOrder.Enabled = false;
                btnCavityMold.Enabled = false;
                txtMold.Enabled = false;
                btnSetUp.Enabled = false;
                btnSetUp.CssClass = "btnActivityDisabled";
                btnProduction.Enabled = false;
                btnProduction.CssClass = "btnActivityDisabled";
                btnMaintenance.Enabled = false;
                btnMaintenance.CssClass = "btnActivityDisabled";
                btnConfirmConnect.Enabled = false;
                btnConfirmConnect.CssClass = "btnActivityDisabled";
            }
        }

        protected void btnConfirmConnect_Click(object sender, EventArgs e)
        {
            //// Validate the operator password
            OperatorService OS = new OperatorService();
            //if (OS.ValidateOperatorAuthentication(txtOperatorSN.Text, txtOperatorPIN.Text == "" ? null : txtOperatorPIN.Text, OperatorHelper.ApplicationId))
            //{
            string operatorId = OS.GetOperatorByRegisterId(txtRegistrationId.Text, OperatorHelper.ApplicationId).Id;

            KeyValuePair<MesEnum.TransactionParam, object>? cause = null;
            //switch (ddlRole.SelectedItem.Text)
            switch (hiddenMachineState.Value)
            {
                case "Attrezzaggio":
                    cause = new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, MesEnum.Cause.SetUp);
                    break;
                case "Manutenzione":
                    cause = new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, MesEnum.Cause.Maintenance);
                    break;
                case "Produzione":
                    cause = new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, MesEnum.Cause.Production);
                    break;
            }

            if (!transOperService.IsOperatorOnMachine(hiddenMachineId.Value, operatorId))
            {
                string jobId = !string.IsNullOrEmpty(txtCustomerOrder.Value) ? txtCustomerOrder.Value : transService.GetLastTransactionOpen(hiddenMachineId.Value)?.JobId;
                transOperService.OpenTransaction(hiddenMachineId.Value, operatorId, MesEnum.OperationName.LogIn, jobId);
                if (!string.IsNullOrEmpty(hiddenWorkshiftSelected.Value))
                {
                    //è stato selezionato un turno precedente per il controllo qualità
                    transOperService.OpenTransaction(hiddenMachineId.Value, operatorId, MesEnum.OperationName.Qualità, jobId, Convert.ToInt32(hiddenWorkshiftSelected.Value));
                }
                else
                {
                    if (ModifyMachineTransaction.Value == bool.FalseString && grantService.AllowQuality(operatorId))
                    {
                        //non è stato selezionato un turno precedente per il controllo qualità ma è un operatore qualità
                        transOperService.OpenTransaction(hiddenMachineId.Value, operatorId, MesEnum.OperationName.Qualità, jobId, null);
                    }
                }
                //switch (ddlRole.SelectedItem.Text)
                switch (hiddenMachineState.Value)
                {
                    case "Attrezzaggio":
                        transOperService.OpenTransaction(hiddenMachineId.Value, operatorId, MesEnum.OperationName.Attrezzaggio, jobId);
                        break;
                    case "Manutenzione":
                        transOperService.OpenTransaction(hiddenMachineId.Value, operatorId, MesEnum.OperationName.Manutenzione, jobId);
                        break;
                }
                //TODO:verificare che non ci siano transazioni di attrezzaggio o manutenzione aperte, in caso chiuderle
            }

            if (ModifyMachineTransaction.Value == bool.TrueString)
            {
                if (!string.IsNullOrEmpty(txtCustomerOrder.Value))
                {
                    if (cause.HasValue)
                    {
                        transService.CloseAndOpenTransaction(
                        hiddenMachineId.Value,
                        new KeyValuePair<MesEnum.TransactionParam, object>[] {
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, operatorId),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, txtCustomerOrder.Value),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.StandardRate, txtNStampateOra.Value),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.CavityMoldNum, btnCavityMold.Value),
                        cause.Value
                        });
                    }
                    else
                    {
                        transService.CloseAndOpenTransaction(
                        hiddenMachineId.Value,
                        new KeyValuePair<MesEnum.TransactionParam, object>[] {
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, operatorId),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, txtCustomerOrder.Value),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.StandardRate, txtNStampateOra.Value),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.CavityMoldNum, btnCavityMold.Value)
                        });
                    }
                }
                else
                {
                    //il job non è cambiato
                    if (cause.HasValue)
                    {
                        transService.CloseAndOpenTransaction(
                            hiddenMachineId.Value,
                            new KeyValuePair<MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, operatorId),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.StandardRate, txtNStampateOra.Value),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.CavityMoldNum, btnCavityMold.Value),
                            cause.Value
                            });
                    }
                    else
                    {
                        transService.CloseAndOpenTransaction(
                            hiddenMachineId.Value,
                            new KeyValuePair<MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, operatorId),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.StandardRate, txtNStampateOra.Value),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.CavityMoldNum, btnCavityMold.Value)
                            });
                    }
                }
            }

            hiddenMachineId.Value = null;
            txtBadge.Text = "";
            txtRegistrationId.Text = "";
            txtUserName.Text = "";
            ddlRole.Items.Clear();
            txtCustomerOrder.Value = null;
            txtNStampateOra.Value = null;
            btnCavityMold.Value = null;
            txtMold.Value = null;
            ModifyMachineTransaction.Value = null;
            btnSelectWorkshift.Visible = false;
            hiddenWorkshiftSelected.Value = null;
            machineTable.Rebind();  //refresh table
            ConnectWindow.CloseDialog();
            //Response.Redirect("MachineDetail.aspx?asset=" + hiddenMachineId.Value);
            //}
            //else
            //{
            //    ErrorMessageOperator.Text = "Invalid login attempt";
            //    ErrorMessageOperator.Visible = true;
            //}
        }

        protected void btnCancelConnect_Click(object sender, EventArgs e)
        {
            hiddenMachineId.Value = null;
            txtBadge.Text = "";
            txtRegistrationId.Text = "";
            txtUserName.Text = "";
            ddlRole.Items.Clear();
            txtCustomerOrder.Value = null;
            txtNStampateOra.Value = null;
            btnCavityMold.Value = null;
            txtMold.Value = null;
            ModifyMachineTransaction.Value = null;
            btnSelectWorkshift.Visible = false;
            hiddenWorkshiftSelected.Value = null;
            machineTable.Rebind();
            ConnectWindow.CloseDialog();
        }

        protected void btnChangeMachineState_Click(object sender, EventArgs e)
        {
            hiddenMachineState.Value = (sender as RadButton).Text;
            switch ((sender as RadButton).Text)
            {
                case "Attrezzaggio":
                    (sender as RadButton).CssClass = "setUpColor";
                    if (btnProduction.Enabled)
                    {
                        btnProduction.CssClass = "btnActivity";
                    }
                    if (btnMaintenance.Enabled)
                    {
                        btnMaintenance.CssClass = "btnActivity";
                    }
                    break;
                case "Produzione":
                    (sender as RadButton).CssClass = "productionColor";
                    if (btnMaintenance.Enabled)
                    {
                        btnMaintenance.CssClass = "btnActivity";
                    }
                    if (btnSetUp.Enabled)
                    {
                        btnSetUp.CssClass = "btnActivity";
                    }
                    break;
                case "Manutenzione":
                    (sender as RadButton).CssClass = "maintenanceColor";
                    if (btnProduction.Enabled)
                    {
                        btnProduction.CssClass = "btnActivity";
                    }
                    if (btnSetUp.Enabled)
                    {
                        btnSetUp.CssClass = "btnActivity";
                    }
                    break;
            }
        }

        protected void gridWorkshift_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (dlgSelectWorkshift.IsOpen())
            {
                //gridWorkshift.DataSource = transOperService.PreviousWorkshift(hiddenMachineId.Value);
                List<TransactionOperator> list = transOperService.PreviousWorkshift(hiddenMachineId.Value);
                List<TransactionOperator> dataSource = new List<TransactionOperator>();
                TransactionRepository repos = new TransactionRepository();
                foreach (var item in list)
                {
                    decimal tot = repos.ReadAll(x => x.MachineId == item.MachineId &&
                                                      x.JobId == item.JobId &&
                                                      x.OperatorId == item.OperatorId &&
                                                      x.Start >= item.DateStart)
                                   .Select(x => x.PartialCounting)
                                   .DefaultIfEmpty(0)
                                   .Sum();
                    if (tot > 0)
                    {
                        item.Job.Order.CustomerOrder.QtyProduced = tot;
                        dataSource.Add(item);
                    }
                }
                gridWorkshift.DataSource = dataSource;
            }
        }

        protected void btnSelectWorkshift_Click(object sender, EventArgs e)
        {
            dlgSelectWorkshift.OpenDialog();
            gridWorkshift.Rebind();
        }

        protected void btnCancelSelectWorkshift_Click(object sender, EventArgs e)
        {
            dlgSelectWorkshift.CloseDialog();
        }

        protected void btnConfirmSelectWorkshift_Click(object sender, EventArgs e)
        {
            if (gridWorkshift.SelectedItems.Count > 0)
            {
                GridDataItem item = (GridDataItem)gridWorkshift.SelectedItems[0]; //get selected row
                string transId = item.GetDataKeyValue("Id").ToString();
                hiddenWorkshiftSelected.Value = transId;
            }

            dlgSelectWorkshift.CloseDialog();
        }
        #endregion

        #region Logout
        protected void btnConfirmLogout_Click(object sender, EventArgs e)
        {
            string assetId = hiddenMachineId.Value;
            OperatorService OS = new OperatorService();
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            GridDataItem item = (GridDataItem)gridOperator.SelectedItems[0];//get selected row
            string operatorId = item.GetDataKeyValue("OperatorId").ToString();

            if (!string.IsNullOrEmpty(hiddenQualityLevelValue.Value))
            {
                //dichiaro il livello qualità
                TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
                QualityLevelRepository qlRepos = new QualityLevelRepository();
                MES.Models.QualityLevel ql = new MES.Models.QualityLevel();
                ql.MachineId = assetId;
                ql.OperatorId = operatorId;
                ql.JobId = transOperRepos.ReadAll(x => x.MachineId == assetId && x.OperatorId == operatorId && x.Open && x.OperationName == "LogIn").ToList().First().JobId;
                switch (hiddenQualityLevelValue.Value)
                {
                    case "Stop":
                        ql.Level = MesEnum.QualityLevel.Stop.ToString();
                        break;
                    case "Warning":
                        ql.Level = MesEnum.QualityLevel.Warning.ToString();
                        break;
                    case "Ok":
                        ql.Level = MesEnum.QualityLevel.Ok.ToString();
                        break;
                }
                ql.Date = DateTime.Now;

                qlRepos.Insert(ql);
                qlRepos.SaveChanges();
            }

            //transOperService.CloseTransaction(assetId, operatorId, OperationName.LogIn);
            //if (transOperService.TransactionOpenPerOperator(assetId, operatorId))
            //{
            //    transOperService.CloseTransaction(assetId, operatorId, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
            //}

            //verifico che chi dichiara il logout sia lo stesso che è sulla macchina
            if (operatorId == transService.GetLastTransactionOpen(assetId)?.OperatorId)
            {
                CauseService causeService = new CauseService();
                SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
                if (causeService.GetListCauseIdByGroupName(new string[] { "Attrezzaggio", "Manutenzione" }, OperatorHelper.ApplicationId).Contains(transService.GetLastTransactionOpen(assetId).CauseId) || !boxService.IsMachineConnected(assetId))
                {
                    transService.CloseAndOpenTransaction(
                        assetId,
                        new KeyValuePair<MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, null),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, null)
                        });
                }
                else
                {
                    transService.CloseAndOpenTransaction(
                        assetId,
                        new KeyValuePair<MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, null)
                        });
                }

                machineTable.Rebind();
            }

            LogoutWindow.CloseDialog();

            hiddenOperatorId.Value = operatorId;

            //mostrare gli scarti se il ruolo lo consente
            if (grantService.AllowWaste(operatorId))
            {
                TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
                string jobId = transOperRepos.ReadAll(x => x.MachineId == hiddenMachineId.Value && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).FirstOrDefault()?.JobId;

                ///////variabili per turno///////////
                string operatorWorkshift;
                TransactionOperator workshiftToWatch;
                List<Transaction> transactionWorkshift;
                string jobIdWorkshift;
                List<JobProductionWaste> wasteWorkshift;
                /////////////////////////////////////

                TransactionRepository transRepos = new TransactionRepository();
                JobProductionWasteService jpwService = new JobProductionWasteService();
                TransactionOperator t = transOperRepos.ReadAll(x => x.MachineId == hiddenMachineId.Value && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "Qualità").OrderByDescending(x => x.DateStart).FirstOrDefault();
                if (t != null)
                {
                    //controllo qualità
                    if (t.QualityTransactionId.HasValue)
                    {
                        //da verificare il turno scelto
                        workshiftToWatch = transOperRepos.FindByID(t.QualityTransactionId);

                        if (string.IsNullOrEmpty(jobId) && string.IsNullOrEmpty(workshiftToWatch.JobId))
                        {
                            goto GoToFermi;
                        }

                        operatorWorkshift = workshiftToWatch.OperatorId;
                        jobIdWorkshift = workshiftToWatch.JobId;
                        transactionWorkshift = transRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobIdWorkshift && x.OperatorId == workshiftToWatch.OperatorId && x.Start >= workshiftToWatch.DateStart && x.Start <= workshiftToWatch.DateEnd).ToList();
                        wasteWorkshift = jpwService.GetWasteListPerWorkshift(jobIdWorkshift, operatorWorkshift, workshiftToWatch.DateStart, workshiftToWatch.DateEnd, OperatorHelper.ApplicationId);
                        //TransactionOperator workshiftToWatch = transOperRepos.FindByID(t.QualityTransactionId);
                        //jobId = workshiftToWatch.JobId;
                        //List<Transaction> currentJobTList = transRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobId && x.OperatorId == workshiftToWatch.OperatorId && x.Start >= workshiftToWatch.DateStart && x.Start <= workshiftToWatch.DateEnd).ToList();
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(jobId))
                        {
                            goto GoToFermi;
                        }

                        //da verificare l'ultimo turno
                        jobIdWorkshift = jobId;
                        operatorWorkshift = transService.GetLastOperatorOnMachinePerJob(assetId, jobIdWorkshift)?.Id;
                        workshiftToWatch = transOperRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobIdWorkshift && x.OperatorId == operatorWorkshift && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First();
                        if (workshiftToWatch.DateEnd.HasValue)
                        {
                            transactionWorkshift = transRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobIdWorkshift && x.OperatorId == operatorWorkshift && x.Start >= workshiftToWatch.DateStart && x.Start <= workshiftToWatch.DateEnd).ToList();
                        }
                        else
                        {
                            transactionWorkshift = transRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobIdWorkshift && x.OperatorId == operatorWorkshift && x.Start >= workshiftToWatch.DateStart).ToList();
                        }
                        wasteWorkshift = jpwService.GetWasteListPerWorkshift(jobIdWorkshift, operatorWorkshift, workshiftToWatch.DateStart, workshiftToWatch.DateEnd, OperatorHelper.ApplicationId);
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(jobId))
                    {
                        goto GoToFermi;
                    }

                    //dichiarazione scarti normale (non qualità)
                    jobIdWorkshift = jobId;
                    operatorWorkshift = hiddenOperatorId.Value;
                    workshiftToWatch = transOperRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobIdWorkshift && x.OperatorId == operatorWorkshift && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First();
                    transactionWorkshift = transRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobIdWorkshift && x.OperatorId == operatorWorkshift && x.Start >= workshiftToWatch.DateStart).ToList();
                    wasteWorkshift = new List<JobProductionWaste>(); //jpwService.GetWasteListPerWorkshift(jobIdWorkshift, operatorWorkshift, workshiftToWatch.DateStart, workshiftToWatch.DateEnd.Value, OperatorHelper.ApplicationId);
                }

                hiddenTransactionToWatch.Value = workshiftToWatch.Id.ToString();

                if (!string.IsNullOrEmpty(jobIdWorkshift))
                {
                    JobRepository jobRepos = new JobRepository();
                    Job currentJob = jobRepos.FindByID(jobIdWorkshift);
                    var numImpronte = currentJob.CavityMoldNum.HasValue && currentJob.CavityMoldNum.Value > 0 ? currentJob.CavityMoldNum.Value : 1;
                    List<Transaction> currentJobTList = transService.GetTransactionListByJobId(assetId, jobId);
                    if (currentJobTList != null && currentJobTList.Count > 0)
                    {
                        if (currentJobTList.Sum(x => x.PartialCounting) == 0)
                        {
                            btnNumPadOk.Enabled = false;
                            btnConfirmScarti.Enabled = false;
                        }
                        if (!grantService.AllowQuality(hiddenOperatorId.Value))
                        {
                            btnRecuperoScarti.Enabled = false;
                        }
                        else
                        {
                            btnNumPadOk.Enabled = true;
                            btnConfirmScarti.Enabled = true;
                        }

                        //txtQtyProducedTot.Text = Math.Round(currentJobTList.Sum(x => x.PartialCounting), 0).ToString();
                        //txtQtyShotProducedOperator.Text = Math.Round(currentJobTList.Where(x => x.OperatorId == operatorId).Sum(x => x.PartialCounting), 0).ToString();
                        //txtQtyProducedOperator.Text = Math.Round(currentJobTList.Where(x => x.OperatorId == operatorId).Sum(x => x.PartialCounting) * numImpronte, 0).ToString();

                        txtQtyProducedTot.Text = Math.Round(currentJobTList.Sum(x => x.PartialCounting), 0).ToString();
                        txtQtyShotProducedOperator.Text = Math.Round(transactionWorkshift.Sum(x => x.PartialCounting), 0).ToString();
                        txtQtyProducedOperator.Text = Math.Round(transactionWorkshift.Sum(x => x.PartialCounting) * numImpronte, 0).ToString();
                        txtQtyWasteTransaction.Text = wasteWorkshift.Sum(x => x.QtyProductionWaste).ToString();

                        //JobProductionWasteService jpwService = new JobProductionWasteService();
                        //List<JobProductionWaste> wasteList = jpwService.GetWasteListByJobId(transService.GetLastTransactionOpen(assetId).JobId, OperatorHelper.ApplicationId);
                        //txtQtyWasteTransaction.Text = wasteList.Sum(x => x.QtyProductionWaste).ToString();

                        CauseTypeRepository CTR = new CauseTypeRepository();
                        List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == OperatorHelper.ApplicationId).ToList();
                        List<ScartiRecord> dataSource = new List<ScartiRecord>();
                        foreach (CauseType ct in causeList)
                        {
                            foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode))
                            {
                                if (grantService.AllowQuality(hiddenOperatorId.Value))
                                {
                                    //controllo qualità -> mostro gli scarti dichiarati in precedenza
                                    //if (wasteList.Where(x => x.CauseId == c.Id).Any())
                                    if (wasteWorkshift.Where(x => x.CauseId == c.Id).Any())
                                    {
                                        //dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteList.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste)));
                                        dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteWorkshift.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste)));
                                    }
                                    else
                                    {
                                        dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                                    }
                                }
                                else
                                {
                                    dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                                }
                            }
                        }

                        gridScarti.DataSource = dataSource;
                        gridScarti.DataBind();

                        dlgScarti.Title = string.Format("SCARTI - Macchina: {0} - Operatore: {1} - Commessa: {3} - Articolo: {2}",
                            AS.GetAssedById(assetId, OperatorHelper.ApplicationId).Description,
                            OS.GetOperatorNameById(operatorId, OperatorHelper.ApplicationId),
                            currentJobTList.First(x => !string.IsNullOrEmpty(x.JobId)).Job?.Order?.Article?.Code,
                            currentJobTList.First(x => !string.IsNullOrEmpty(x.JobId)).Job?.Order?.CustomerOrder?.OrderCode);

                        SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
                        txtQtyProducedOperator.ReadOnly = boxService.IsMachineConnected(assetId);
                        if (!boxService.IsMachineConnected(assetId))
                        {
                            btnNumPadOk.Enabled = true;
                            btnConfirmScarti.Enabled = true;
                        }
                        //txtQtyWasteTransaction.ReadOnly = boxService.IsMachineConnected(assetId);

                        dlgScarti.OpenDialog();
                    }
                    else
                    {
                        //apro direttamente i fermi
                        OpenFermi(assetId, operatorId);
                    }
                }
                else
                {
                    //apro direttamente i fermi
                    OpenFermi(assetId, operatorId);
                }
            }
            else
            {
                //apro direttamente i fermi
                OpenFermi(assetId, operatorId);
            }

            GoToFermi:
            OpenFermi(assetId, operatorId);
        }

        protected void btnCancelLogout_Click(object sender, EventArgs e)
        {
            pnlAllowConfirmProduction.Visible = false;
            machineTable.Rebind();
            LogoutWindow.CloseDialog();
        }
        #endregion

        #region Scarti
        [Serializable]
        protected class ScartiRecord
        {
            public string CauseId { get; set; }
            public string Description { get; set; } // code - description
            public decimal Num { get; set; } //num scarti
            public decimal Quality { get; set; }

            public ScartiRecord(string id, string description)
            {
                CauseId = id;
                Description = description;
            }

            public ScartiRecord(string id, string description, decimal num, decimal quality = 0)
            {
                CauseId = id;
                Description = description;
                Num = num;
                Quality = quality;
            }
        }

        const string JOB_PRODUCTIONWASTE_TO_MODIFY = "JobProductionWasteListToModify";
        protected List<ScartiRecord> JobWasteListToModify
        {
            get
            {
                if (this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] == null)
                {
                    this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] = new List<ScartiRecord>();
                }
                return (List<ScartiRecord>)(this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY]);
            }
            set
            {
                this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] = value;
            }
        }

        protected void btnConfirmScarti_Click(object sender, EventArgs e)
        {
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();
            TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
            TransactionRepository tr = new TransactionRepository();

            Transaction lastTrans;
            string jobId = null;
            decimal nImpronte = 1;
            decimal QtyProducedTot = 0;

            SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
            if (!boxService.IsMachineConnected(hiddenMachineId.Value))
            {
                //macchina manuale
                TransactionOperator t = transOperRepos.ReadAll(x => x.MachineId == hiddenMachineId.Value && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First();
                lastTrans = transService.GetLastTransactionPerOperator(hiddenMachineId.Value, hiddenOperatorId.Value);
                if (lastTrans != null)
                {
                    //l'operatore era sulla macchina, può dichiarare quantità prodotte
                    QtyProducedTot = Convert.ToDecimal(txtQtyProducedOperator.Text);
                    lastTrans.PartialCounting = Convert.ToDecimal(txtQtyProducedOperator.Text);
                    tr.Update(lastTrans);
                    tr.SaveChanges();

                    jobId = lastTrans.JobId;
                    nImpronte = lastTrans.Job != null && lastTrans.Job.CavityMoldNum.HasValue && lastTrans.Job.CavityMoldNum > 0 ? lastTrans.Job.CavityMoldNum.Value : 1;
                }
                else
                {
                    //l'operatore non era sulla macchina, è aggiuntivo, non dichiara quindi quantità prodotte
                    jobId = t.JobId;
                    nImpronte = t.Job != null && t.Job.CavityMoldNum.HasValue && t.Job.CavityMoldNum > 0 ? t.Job.CavityMoldNum.Value : 1;

                    //TODO: riportare l'ultimo lavoro con produzione o il turno scelto all'ingresso per sapere la quantità prodotta
                }
            }
            else
            {
                //macchina automatica
                jobId = transOperRepos.ReadAll(x => x.MachineId == hiddenMachineId.Value && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First().JobId;

                List<Transaction> currentJobTList = transService.GetTransactionListByJobId(hiddenMachineId.Value, jobId);
                if (currentJobTList != null && currentJobTList.Count > 0)
                {
                    QtyProducedTot = Math.Round(currentJobTList.Sum(x => x.PartialCounting), 0);

                    nImpronte = currentJobTList.First().Job.CavityMoldNum.HasValue && currentJobTList.First().Job.CavityMoldNum > 0 ? currentJobTList.First().Job.CavityMoldNum.Value : 1;
                }

                lastTrans = transService.GetLastTransactionOpen(hiddenMachineId.Value);
            }

            foreach (ScartiRecord sr in JobWasteListToModify)
            {
                JobProductionWaste jpw = new JobProductionWaste();
                jpw.AssetId = hiddenMachineId.Value;
                jpw.CauseId = sr.CauseId;
                jpw.Date = DateTime.Now;
                jpw.JobId = jobId; //lastTrans.JobId;
                jpw.OperatorId = hiddenOperatorId.Value; //lastTrans.OperatorId;
                jpw.QtyShotProduced = QtyProducedTot;
                jpw.QtyProduced = QtyProducedTot * nImpronte;
                jpw.QtyProductionWaste = grantService.AllowQuality(hiddenOperatorId.Value) ? sr.Quality : sr.Num;
                jpw.ArticleId = lastTrans?.Job?.Order?.IdArticle;
                jpwRep.Insert(jpw);
            }
            jpwRep.SaveChanges();

            //aggiungo gli scarti ull'ultima transazione di produzione dell'operatore
            Transaction tWaste = transService.GetLastTransactionPerOperatorInProd(hiddenMachineId.Value, hiddenOperatorId.Value, OperatorHelper.ApplicationId);
            if (tWaste != null)
            {
                tWaste.QtyProductionWaste = JobWasteListToModify.Sum(x => x.Num + x.Quality);
                tWaste.QtyOK = tWaste.QtyProduced - tWaste.QtyProductionWaste;
                tr.Update(tWaste);
                tr.SaveChanges();
            }
            /////////////////////////////////////////////////////////////////////////

            JobWasteListToModify.Clear();
            dlgScarti.CloseDialog();

            //apro i fermi
            OpenFermi(hiddenMachineId.Value, hiddenOperatorId.Value);
        }

        protected void btnCancelScarti_Click(object sender, EventArgs e)
        {
            JobWasteListToModify.Clear();
            dlgScarti.CloseDialog();

            //apro i fermi
            OpenFermi(hiddenMachineId.Value, hiddenOperatorId.Value);
        }

        protected void btnNumPadOk_Click(object sender, EventArgs e)
        {
            float num;
            if (float.TryParse(txtInsValue.Text, out num))
            {
                txtInsValue.Text = num.ToString();
            }

            if (!string.IsNullOrEmpty(hiddenCompileManualQty.Value) && hiddenCompileManualQty.Value == bool.TrueString)
            {
                //inserire qtà per macchina manuale
                txtQtyProducedOperator.Text = Math.Round(Convert.ToDecimal(txtInsValue.Text), 0).ToString();
                hiddenCompileManualQty.Value = null;
            }
            else
            {
                if (gridScarti.SelectedItems.Count > 0)
                {
                    GridDataItem selectedItem = (GridDataItem)gridScarti.SelectedItems[0]; //get selected row
                    string causeId = selectedItem.GetDataKeyValue("CauseId").ToString();
                    if (grantService.AllowQuality(hiddenOperatorId.Value))
                    {
                        //controllo qualità
                        if (JobWasteListToModify.Where(x => x.CauseId == causeId).Any())
                        {
                            JobWasteListToModify.Where(x => x.CauseId == causeId).First().Quality = Convert.ToDecimal(txtInsValue.Text);
                        }
                        else
                        {
                            JobWasteListToModify.Add(new ScartiRecord(selectedItem.GetDataKeyValue("CauseId").ToString(), "", 0, Convert.ToDecimal(txtInsValue.Text)));
                        }
                    }
                    else
                    {
                        //dichiarazione scarti operatore
                        if (JobWasteListToModify.Where(x => x.CauseId == causeId).Any())
                        {
                            JobWasteListToModify.Where(x => x.CauseId == causeId).First().Num = Convert.ToDecimal(txtInsValue.Text);
                        }
                        else
                        {
                            JobWasteListToModify.Add(new ScartiRecord(selectedItem.GetDataKeyValue("CauseId").ToString(), "", Convert.ToDecimal(txtInsValue.Text)));
                        }
                    }

                    JobProductionWasteService jpwService = new JobProductionWasteService();
                    TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
                    TransactionOperator workshiftToWatch = transOperRepos.FindByID(Convert.ToInt32(hiddenTransactionToWatch.Value));
                    List<JobProductionWaste> wasteList = jpwService.GetWasteListPerWorkshift(workshiftToWatch.JobId, workshiftToWatch.OperatorId, workshiftToWatch.DateStart, workshiftToWatch.DateEnd, OperatorHelper.ApplicationId);
                    //List<JobProductionWaste> wasteList = jpwService.GetWasteListByJobId(transService.GetLastTransactionOpen(hiddenMachineId.Value).JobId, OperatorHelper.ApplicationId);

                    //update datasource
                    CauseTypeRepository CTR = new CauseTypeRepository();
                    List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == OperatorHelper.ApplicationId).OrderBy(x => x.Description).ToList();
                    List<ScartiRecord> dataSource = new List<ScartiRecord>();
                    foreach (CauseType ct in causeList)
                    {
                        foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode))
                        {
                            if (grantService.AllowQuality(hiddenOperatorId.Value))
                            {
                                //controllo qualità -> mostro gli scarti dichiarati in precedenza
                                if (wasteList.Where(x => x.CauseId == c.Id).Any())
                                {
                                    dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, Math.Round(wasteList.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste), 0)));
                                }
                                else
                                {
                                    dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                                }
                            }
                            else
                            {
                                dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                            }
                        }
                    }

                    foreach (ScartiRecord sr in JobWasteListToModify)
                    {
                        int index = dataSource.IndexOf(dataSource.Where(x => x.CauseId == sr.CauseId).First());
                        if (grantService.AllowQuality(hiddenOperatorId.Value))
                        {
                            //controllo qualità, modifica la colonna qualità
                            dataSource.ElementAt(index).Quality = sr.Quality;
                        }
                        else
                        {
                            //dichiarazione scarti operatore, modifico la colonna operatore
                            dataSource.ElementAt(index).Num = sr.Num;
                        }
                    }
                    gridScarti.DataSource = dataSource;
                    gridScarti.DataBind();

                    txtQtyWasteTransaction.Text = dataSource.Sum(x => x.Num + x.Quality).ToString();

                    //Aggiornamento quantità totale in corso
                    //List<Transaction> currentJobTList = transService.GetTransactionListByCurrentJob(AssetId);
                    //txtQtyProducedTransaction.Text = currentJobTList.Sum(x => x.PartialCounting).ToString();

                }
            }
            txtInsValue.Text = "";
        }
        #endregion

        #region Fermi da giustificare in logout
        [Serializable]
        protected class FermiRecord
        {
            public int TransactionId { get; set; }
            public string CauseId { get; set; }
            public string Description { get; set; }
            public decimal Duration { get; set; }

            public FermiRecord(int tId, string causeId)
            {
                TransactionId = tId;
                CauseId = causeId;
            }

            public FermiRecord(string causeId, string description, decimal duration)
            {
                CauseId = causeId;
                Description = description;
                Duration = duration;
            }
        }

        const string TRANSACTION_LIST_TO_MODIFY = "TransactionListToModify";
        protected List<FermiRecord> TransactionListToModify
        {
            get
            {
                if (this.ViewState[TRANSACTION_LIST_TO_MODIFY] == null)
                {
                    this.ViewState[TRANSACTION_LIST_TO_MODIFY] = new List<FermiRecord>();
                }
                return (List<FermiRecord>)(this.ViewState[TRANSACTION_LIST_TO_MODIFY]);
            }
            set
            {
                this.ViewState[TRANSACTION_LIST_TO_MODIFY] = value;
            }
        }

        private void OpenFermi(string assetId, string operatorId)
        {
            OperatorService OS = new OperatorService();
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();

            SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
            if (boxService.IsMachineConnected(assetId))
            {
                //macchina automatica -> mostro dlgFermi

                List<Transaction> datasource = transService.TransactionToJustifyList(assetId);
                gridFermi.DataSource = datasource;
                gridFermi.DataBind();

                //if (transOperService.OperatorsOnMachine(assetId).Count == 1) //è l'ultimo operatore rimasto
                //{
                if (datasource.Count > TransactionListToModify.Count)
                {
                    btnConfirmCausaleFermo.Enabled = false;
                    btnConfirmCausaleFermo.CssClass = "btnActivityDisabled";
                }
                else
                {
                    btnConfirmCausaleFermo.Enabled = true;
                    btnConfirmCausaleFermo.CssClass = "btnActivity";
                }
                //}

                LoadCauseTreeView();
                updatePnlFermi.Update();

                dlgFermi.Title = string.Format("FERMI - Macchina: {0} - Operatore: {1}", AS.GetAssedById(assetId, OperatorHelper.ApplicationId).Description, OS.GetOperatorNameById(operatorId, OperatorHelper.ApplicationId));
                dlgFermi.OpenDialog();
            }
            else
            {
                //macchina manuale -> mostro dlgFermiManuale

                CauseTypeRepository CTR = new CauseTypeRepository();
                List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Fermi" && x.ApplicationId == OperatorHelper.ApplicationId).OrderBy(x => x.Description).ToList();
                List<FermiRecord> dataSource = new List<FermiRecord>();
                foreach (CauseType ct in causeList)
                {
                    foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode.Length).ThenBy(x => x.ExternalCode))
                    {
                        dataSource.Add(new FermiRecord(c.Id, c.ExternalCode + " - " + c.Description, 0));
                    }
                }

                gridFermiManuali.DataSource = dataSource;
                gridFermiManuali.DataBind();

                dlgFermiManuali.Title = string.Format("FERMI - Macchina: {0} - Operatore: {1}", AS.GetAssedById(assetId, OperatorHelper.ApplicationId).Description, OS.GetOperatorNameById(operatorId, OperatorHelper.ApplicationId));
                dlgFermiManuali.OpenDialog();
            }
        }

        protected void gridFermi_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                item["Status"].Text = item["Status"].Text == "False" ? "Off" : "On";
            }
        }

        protected void LoadCauseTreeView()
        {
            causeTreeView.Nodes.Clear();
            CauseService CS = new CauseService();
            //CauseTypeRepository CTR = new CauseTypeRepository();
            //List<CauseType> causeList = CTR.ReadAll(x => x.ApplicationId == OperatorHelper.ApplicationId && (x.Description == "Fermi" || x.Description == "Attrezzaggio" || x.Description == "Manutenzione")).ToList();
            //foreach (CauseType ct in causeList)
            //{
            //    //Node CauseType
            //    RadTreeNode nodeCT = new RadTreeNode(ct.Description, ct.Id);
            //    nodeCT.PostBack = false;
            //    nodeCT.Expanded = true;
            //    foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.Code))
            //    {
            //        RadTreeNode nodeC = new RadTreeNode(c.Code + " - " + c.Description, c.Id);
            //        nodeC.PostBack = true;
            //        nodeC.BorderStyle = BorderStyle.Solid;
            //        nodeC.BorderColor = System.Drawing.Color.Black;
            //        nodeC.Height = Unit.Percentage(30);
            //        //nodeC.Width = Unit.Percentage(66);
            //        nodeCT.Nodes.Add(nodeC);
            //    }
            //    causeTreeView.Nodes.Add(nodeCT);
            //}

            //Node CauseType
            RadTreeNode nodeCT = new RadTreeNode("Causali");
            nodeCT.PostBack = false;
            nodeCT.Expanded = true;
            List<MES.Models.Cause> causeList = CS.GetListCauseByGroupName(new string[] { "Attrezzaggio", "Manutenzione", "Fermi" }, OperatorHelper.ApplicationId);
            foreach (MES.Models.Cause c in causeList.OrderBy(x => x.Code))
            {
                RadTreeNode nodeC = new RadTreeNode(c.Code + " - " + c.Description, c.Id);
                nodeC.PostBack = true;
                nodeC.BorderStyle = BorderStyle.Solid;
                nodeC.BorderColor = System.Drawing.Color.Black;
                nodeC.Height = Unit.Percentage(30);
                //nodeC.Width = Unit.Percentage(66);
                nodeCT.Nodes.Add(nodeC);
            }
            causeTreeView.Nodes.Add(nodeCT);
            //causeTreeView.DataBind();
        }

        protected void causeTreeView_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            TransactionRepository tRep = new TransactionRepository();
            //List<Transaction> transactionListToModify = new List<Transaction>();
            //if (CustomSettings.Params.ContainsKey(TRANSACTION_LIST_TO_MODIFY))
            //{
            //    transactionListToModify = JsonConvert.DeserializeObject<List<Transaction>>(CustomSettings.Params[TRANSACTION_LIST_TO_MODIFY].ToString());
            //}
            GridItemCollection listItem = gridFermi.SelectedItems; //get selected rows
            foreach (GridDataItem item in listItem)
            {
                int id = (int)item.GetDataKeyValue("Id");
                Transaction t = tRep.FindByID(id);
                t.CauseId = e.Node.Value;
                //transactionListToModify.Add(t);
                TransactionListToModify.Add(new FermiRecord(t.Id, t.CauseId));
            }
            //CustomSettings.AddOrUpdate(TRANSACTION_LIST_TO_MODIFY, transactionListToModify);

            //update table fermi
            TransactionService TS = new TransactionService();
            List<Transaction> datasource = TS.TransactionToJustifyList(hiddenMachineId.Value);
            foreach (var t in TransactionListToModify)
            {
                int index = datasource.IndexOf(datasource.Where(x => x.Id == t.TransactionId).First());
                datasource.ElementAt(index).CauseId = t.CauseId;
            }
            gridFermi.DataSource = datasource;
            gridFermi.DataBind();

            if (TransactionListToModify.Count > 0)
            {
                btnConfirmCausaleFermo.Enabled = true;
                btnConfirmCausaleFermo.CssClass = "btnActivity";
            }
            else
            {
                btnConfirmCausaleFermo.Enabled = false;
                btnConfirmCausaleFermo.CssClass = "btnActivityDisabled";
            }
        }

        protected void btnConfirmCausaleFermo_Click(object sender, EventArgs e)
        {
            TransactionRepository tRep = new TransactionRepository();
            if (TransactionListToModify.Count > 0)
            {
                TransactionService TS = new TransactionService();
                List<Transaction> datasource = TS.TransactionToJustifyList(hiddenMachineId.Value);
                foreach (var t in TransactionListToModify)
                {
                    int index = datasource.IndexOf(datasource.Where(x => x.Id == t.TransactionId).First());
                    datasource.ElementAt(index).CauseId = t.CauseId;
                }
                tRep.UpdateAll(datasource);
                tRep.SaveChanges();
                TransactionListToModify.Clear();
            }

            transOperService.CloseTransaction(hiddenMachineId.Value, hiddenOperatorId.Value, OperationName.LogIn);
            if (transOperService.TransactionOpenPerOperator(hiddenMachineId.Value, hiddenOperatorId.Value))
            {
                transOperService.CloseTransaction(hiddenMachineId.Value, hiddenOperatorId.Value, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
            }

            machineTable.Rebind();
            dlgFermi.CloseDialog();

            //if (grantService.AllowProduction(hiddenOperatorId.Value))
            //{
            //    //chiede se la commessa è terminata oppure no
            //    dlgCloseCustomerOrder.OpenDialog();
            //}
        }

        protected void btnCancelCausaleFermo_Click(object sender, EventArgs e)
        {
            TransactionListToModify.Clear();

            transOperService.CloseTransaction(hiddenMachineId.Value, hiddenOperatorId.Value, OperationName.LogIn);
            if (transOperService.TransactionOpenPerOperator(hiddenMachineId.Value, hiddenOperatorId.Value))
            {
                transOperService.CloseTransaction(hiddenMachineId.Value, hiddenOperatorId.Value, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
            }

            machineTable.Rebind();
            dlgFermi.CloseDialog();

            //if (grantService.AllowProduction(hiddenOperatorId.Value))
            //{
            //    //chiede se la commessa è terminata oppure no
            //    dlgCloseCustomerOrder.OpenDialog();
            //}
        }

        protected void btnOKFermo_Click(object sender, EventArgs e)
        {
            float num;
            if (float.TryParse(txtInsDuration.Text, out num))
            {
                txtInsDuration.Text = num.ToString();
            }

            if (gridFermiManuali.SelectedItems.Count > 0)
            {
                GridDataItem selectedItem = (GridDataItem)gridFermiManuali.SelectedItems[0]; //get selected row
                string causeId = selectedItem.GetDataKeyValue("CauseId").ToString();

                if (TransactionListToModify.Where(x => x.CauseId == causeId).Any())
                {
                    TransactionListToModify.Where(x => x.CauseId == causeId).First().Duration = Convert.ToDecimal(txtInsDuration.Text);
                }
                else
                {
                    TransactionListToModify.Add(new FermiRecord(selectedItem.GetDataKeyValue("CauseId").ToString(), null, Convert.ToDecimal(txtInsDuration.Text)));
                }

                //update datasource
                CauseTypeRepository CTR = new CauseTypeRepository();
                List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Fermi" && x.ApplicationId == OperatorHelper.ApplicationId).OrderBy(x => x.Description).ToList();
                List<FermiRecord> dataSource = new List<FermiRecord>();
                foreach (CauseType ct in causeList)
                {
                    foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode.Length).ThenBy(x => x.ExternalCode))
                    {
                        dataSource.Add(new FermiRecord(c.Id, c.ExternalCode + " - " + c.Description, 0));
                    }
                }

                foreach (FermiRecord fr in TransactionListToModify)
                {
                    int index = dataSource.IndexOf(dataSource.Where(x => x.CauseId == fr.CauseId).First());
                    dataSource.ElementAt(index).Duration = fr.Duration;
                }

                gridFermiManuali.DataSource = dataSource;
                gridFermiManuali.DataBind();
            }
            txtInsDuration.Text = "";
        }

        protected void btnConfirmFermiManuali_Click(object sender, EventArgs e)
        {
            TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
            TransactionOperator t = transOperRepos.ReadAll(x => x.MachineId == hiddenMachineId.Value && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First();
            Transaction lastTrans = transService.GetLastTransactionPerOperatorWorkshift(hiddenMachineId.Value, hiddenOperatorId.Value, t.DateStart, t.DateEnd);
            if (lastTrans != null)
            {
                //l'operatore era sulla macchina
                if (TransactionListToModify.Count > 0)
                {
                    TransactionRepository tRep = new TransactionRepository();
                    CauseTypeRepository CTR = new CauseTypeRepository();
                    List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Fermi" && x.ApplicationId == OperatorHelper.ApplicationId).OrderBy(x => x.Description).ToList();
                    List<FermiRecord> dataSource = new List<FermiRecord>();
                    List<Transaction> transToAdd = new List<Transaction>();
                    foreach (CauseType ct in causeList)
                    {
                        foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode.Length).ThenBy(x => x.ExternalCode))
                        {
                            dataSource.Add(new FermiRecord(c.Id, c.ExternalCode + " - " + c.Description, 0));
                        }
                    }
                    foreach (var item in TransactionListToModify)
                    {
                        Transaction newItem = new Transaction();
                        newItem = lastTrans;
                        newItem.CauseId = item.CauseId;
                        newItem.Duration = item.Duration;
                        newItem.PartialCounting = 0;
                        tRep.Insert(newItem);
                    }
                    tRep.SaveChanges();
                    TransactionListToModify.Clear();
                }
            }
            else
            {
                //operatore è aggiuntivo -> riportare l'ultimo turno o il turno selezionato all'ingresso
            }

            TransactionListToModify.Clear();

            transOperService.CloseTransaction(hiddenMachineId.Value, hiddenOperatorId.Value, OperationName.LogIn);
            if (transOperService.TransactionOpenPerOperator(hiddenMachineId.Value, hiddenOperatorId.Value))
            {
                transOperService.CloseTransaction(hiddenMachineId.Value, hiddenOperatorId.Value, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
            }

            machineTable.Rebind();
            dlgFermiManuali.CloseDialog();
        }

        protected void btnCancelFermiManuali_Click(object sender, EventArgs e)
        {
            TransactionListToModify.Clear();

            transOperService.CloseTransaction(hiddenMachineId.Value, hiddenOperatorId.Value, OperationName.LogIn);
            if (transOperService.TransactionOpenPerOperator(hiddenMachineId.Value, hiddenOperatorId.Value))
            {
                transOperService.CloseTransaction(hiddenMachineId.Value, hiddenOperatorId.Value, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
            }

            machineTable.Rebind();
            dlgFermiManuali.CloseDialog();
        }
        #endregion

        #region gridOperator
        protected void gridOperator_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //TransactionOperatorRepository repos = new TransactionOperatorRepository();
            //List<TransactionOperator> dataSource = repos.ReadAll(x => x.MachineId == AssetId && x.OperationName == MesEnum.OperationName.LogIn.ToString() && x.Open).ToList();
            if (LogoutWindow.IsOpen())
            {
                gridOperator.DataSource = transOperService.OperatorsOnMachine(hiddenMachineId.Value);// dataSource;
            }
        }

        protected void gridOperator_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;

                if (!string.IsNullOrEmpty(item["OperatorId"].Text))
                {
                    OperatorService OperService = new OperatorService();
                    string opId = item["OperatorId"].Text;
                    item["OperatorId"].Text = OperService.GetOperatorNameById(opId, OperatorHelper.ApplicationId);
                    if ((gridOperator.DataSource as List<TransactionOperator>).Count == 1)
                    {
                        //seleziona la prima riga
                        item.FireCommandEvent("Select", new GridSelectCommandEventArgs(item, null, null));
                    }
                }
                if (!string.IsNullOrEmpty(item["RoleId"].Text) && item["RoleId"].Text != "&nbsp;")
                {
                    OperatorRoleService operRoleService = new OperatorRoleService();
                    item["RoleId"].Text = operRoleService.FindRoleById(item["RoleId"].Text).Name;
                }
            }
        }

        protected void gridOperator_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.SelectCommandName)
            {
                string operatorId = (e.Item as GridDataItem).GetDataKeyValue("OperatorId").ToString();
                if (grantService.AllowConfirmStartUpProduction(operatorId))
                {
                    pnlAllowConfirmProduction.Visible = true;

                    SetTrafficLight(hiddenMachineId.Value, operatorId);
                }
                else
                {
                    pnlAllowConfirmProduction.Visible = false;
                }
            }
        }

        private void SetTrafficLight(string machineId, string operatorId)
        {
            QualityLevelService qlService = new QualityLevelService();
            TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
            MES.Models.QualityLevel ql = qlService.GetQualityLevel(
                machineId,
                transOperRepos.ReadAll(x => x.MachineId == machineId && x.OperatorId == operatorId && x.Open && x.OperationName == "LogIn").ToList().First().JobId);

            redLight.Attributes["class"] = redLight.Attributes["class"].Replace(" on", "");
            yellowLight.Attributes["class"] = yellowLight.Attributes["class"].Replace(" on", "");
            greenLight.Attributes["class"] = greenLight.Attributes["class"].Replace(" on", "");
            if (ql != null)
            {
                switch (ql.Level)
                {
                    case "Stop":
                        redLight.Attributes["class"] = redLight.Attributes["class"] + " on";
                        break;
                    case "Warning":
                        yellowLight.Attributes["class"] = yellowLight.Attributes["class"] + " on";
                        break;
                    case "Ok":
                        greenLight.Attributes["class"] = greenLight.Attributes["class"] + " on";
                        break;
                }
            }
        }

        protected void gridOperator_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (gridOperator.SelectedItems.Count > 0)
                {
                    GridDataItem item = (GridDataItem)gridOperator.SelectedItems[0];//get selected row
                    string operatorId = item.GetDataKeyValue("OperatorId").ToString();
                    if (grantService.AllowConfirmStartUpProduction(operatorId))
                    {
                        pnlAllowConfirmProduction.Visible = true;

                        SetTrafficLight(hiddenMachineId.Value, operatorId);
                    }
                    else
                    {
                        pnlAllowConfirmProduction.Visible = false;
                    }
                }
            }
            catch (ArgumentOutOfRangeException)
            {}
            btnConfirmLogout.Enabled = true;
            btnConfirmLogout.CssClass = "btnActivity";
        }
        #endregion

        #region Select Mold
        protected void txtMold_Click(object sender, EventArgs e)
        {
            dlgSelectMold.OpenDialog();
            gridSelectMold.Rebind();
        }

        protected void gridSelectMold_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (dlgSelectMold.IsOpen())
            {
                ArticleMoldService AMS = new ArticleMoldService();
                string jobId = !string.IsNullOrEmpty(txtCustomerOrder.Value) ? txtCustomerOrder.Value : transService.GetLastTransactionOpen(hiddenMachineId.Value)?.JobId;
                gridSelectMold.DataSource = AMS.GetMoldListByJobId(jobId, OperatorHelper.ApplicationId); //se un job non è già stato scelto, il datasource sarà vuoto
            }
        }

        protected void btnConfirmSelectMold_Click(object sender, EventArgs e)
        {
            if (gridSelectMold.SelectedItems.Count > 0)
            {
                GridDataItem item = (GridDataItem)gridSelectMold.SelectedItems[0];//get selected row
                string moldId = item.GetDataKeyValue("MoldId").ToString();
                ArticleMoldService AMS = new ArticleMoldService();
                txtMold.Value = moldId;
                txtMold.Text = AMS.GetMoldById(moldId).Code;
            }

            dlgSelectMold.CloseDialog();
        }

        protected void btnCancelSelectMold_Click(object sender, EventArgs e)
        {
            dlgSelectMold.CloseDialog();
        }
        #endregion

        #region SelectOrder
        protected void txtCustomerOrder_Click(object sender, EventArgs e)
        {
            dlgSelectOrder.OpenDialog();
            gridSelectOrder.Rebind();
        }

        protected void btnCancelSelectOrder_Click(object sender, EventArgs e)
        {
            dlgSelectOrder.CloseDialog();
            txtSearchOrder.Text = "";
            txtSearchOrder.Visible = false;
            pnlTastieraCommessa.Visible = false;
        }

        protected void gridSelectOrder_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (dlgSelectOrder.IsOpen())
            {
                JobService jobService = new JobService();
                if (txtSearchOrder.Visible && txtSearchOrder.Text.Length > 0)
                {
                    gridSelectOrder.DataSource = jobService.GetJobListOrderCodeFiltered(txtSearchOrder.Text);
                }
                else
                {
                    gridSelectOrder.DataSource = jobService.GetJobListByAsset(hiddenMachineId.Value);
                }
            }
        }

        protected void btnConfirmSelectOrder_Click(object sender, EventArgs e)
        {
            if (gridSelectOrder.SelectedItems.Count > 0)
            {
                GridDataItem item = (GridDataItem)gridSelectOrder.SelectedItems[0];//get selected row
                string jobId = item.GetDataKeyValue("Id").ToString();
                txtCustomerOrder.Value = jobId;
                JobRepository jobRep = new JobRepository();
                Job j = jobRep.FindByID(jobId);
                txtCustomerOrder.Text = j?.Order?.CustomerOrder?.OrderCode;
                txtArticleCode.Text = j?.Order?.Article?.Code;
                txtArticleDescription.Text = j?.Order?.Article?.Description;
                txtNStampateOra.Text = string.Format("N stampate ora = {0}", j?.StandardRate);
                txtNStampateOra.Value = j?.StandardRate.ToString();
                btnCavityMold.Text = string.Format("N impronte = {0}", j?.CavityMoldNum);
                btnCavityMold.Value = j?.CavityMoldNum.ToString();

                ArticleMoldService AMS = new ArticleMoldService();
                if (AMS.GetMoldListByJobId(j.Id, OperatorHelper.ApplicationId).Count == 1)
                {
                    txtMold.Value = AMS.GetMoldListByJobId(j.Id, OperatorHelper.ApplicationId)[0].MoldId;
                    txtMold.Text = AMS.GetMoldById(txtMold.Value).Code;
                }

                //ArticleMoldService AMS = new ArticleMoldService();
                //if (AMS.GetMoldListByJobId(jobId, OperatorHelper.ApplicationId).Count == 1)
                //{
                //    transService.CloseAndOpenTransaction(
                //        AssetId,
                //        new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                //        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, jobId),
                //        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, AMS.GetMoldListByJobId(jobId, OperatorHelper.ApplicationId)[0].MoldId)});
                //}
                //else
                //{
                //    transService.CloseAndOpenTransaction(
                //        AssetId,
                //        new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                //        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, jobId) });
                //}
            }

            dlgSelectOrder.CloseDialog();
            txtSearchOrder.Text = "";
            txtSearchOrder.Visible = false;
            pnlTastieraCommessa.Visible = false;
        }

        protected void btnSearchOrder_Click(object sender, EventArgs e)
        {
            txtSearchOrder.Visible = true;
            pnlTastieraCommessa.Visible = true;
        }

        protected void txtSearchOrder_TextChanged(object sender, EventArgs e)
        {
            gridSelectOrder.Rebind();
        }
        #endregion

        #region ChangeCavityMoldNum
        protected void btnCavityMold_Click(object sender, EventArgs e)
        {
            txtCavityMoldNum.Text = "";
            dlgSetCavityMoldNum.OpenDialog();
        }

        protected void btnCancelCavityMoldNum_Click(object sender, EventArgs e)
        {
            txtCavityMoldNum.Text = "";
            dlgSetCavityMoldNum.CloseDialog();
        }

        protected void btnConfirmCavityMoldNum_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCavityMoldNum.Text))
            {
                btnCavityMold.Text = string.Format("N impronte = {0}", txtCavityMoldNum.Text);
                btnCavityMold.Value = txtCavityMoldNum.Text;
            }
            txtCavityMoldNum.Text = "";
            dlgSetCavityMoldNum.CloseDialog();
        }
        #endregion

        //protected void VersamentiProduzione(string machineId, string operatorId)
        //{
        //    JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();
        //    TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
        //    JobProductionWasteService jpwService = new JobProductionWasteService();

        //    TransactionOperator operatorWorkshift;
        //    List<JobProductionWaste> wasteWorkshift;

        //    TransactionOperator t = transOperRepos.ReadAll(x => x.MachineId == hiddenMachineId.Value && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First();

        //    string jobId = t.JobId;

        //    if (t.QualityTransactionId.HasValue)
        //    {
        //        //da verificare il turno scelto
        //        TransactionOperator workshiftToWatch = transOperRepos.FindByID(t.QualityTransactionId);

        //        jobId = workshiftToWatch.JobId;

        //        if (string.IsNullOrEmpty(jobId) && string.IsNullOrEmpty(workshiftToWatch.JobId))
        //        {
        //            goto GoToFermi;
        //        }

        //        transactionWorkshift = transRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobIdWorkshift && x.OperatorId == workshiftToWatch.OperatorId && x.Start >= workshiftToWatch.DateStart && x.Start <= workshiftToWatch.DateEnd).ToList();
        //        wasteWorkshift = jpwService.GetWasteListPerWorkshift(jobId, operatorId, t.DateStart, DateTime.Now, OperatorHelper.ApplicationId);


        //        SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
        //    if (!boxService.IsMachineConnected(hiddenMachineId.Value))
        //    {
        //        //macchina manuale
        //        TransactionOperator t = transOperRepos.ReadAll(x => x.MachineId == hiddenMachineId.Value && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First();
        //        lastTrans = transService.GetLastTransactionPerOperator(hiddenMachineId.Value, hiddenOperatorId.Value);
        //        if (lastTrans != null)
        //        {
        //            //l'operatore era sulla macchina, può dichiarare quantità prodotte
        //            QtyProducedTot = Convert.ToDecimal(txtQtyProducedOperator.Text);
        //            lastTrans.PartialCounting = Convert.ToDecimal(txtQtyProducedOperator.Text);
        //            TransactionRepository tr = new TransactionRepository();
        //            tr.Update(lastTrans);
        //            tr.SaveChanges();

        //            jobId = lastTrans.JobId;
        //            nImpronte = lastTrans.Job != null && lastTrans.Job.CavityMoldNum.HasValue && lastTrans.Job.CavityMoldNum > 0 ? lastTrans.Job.CavityMoldNum.Value : 1;
        //        }
        //        else
        //        {
        //            //l'operatore non era sulla macchina, è aggiuntivo, non dichiara quindi quantità prodotte
        //            jobId = t.JobId;
        //            nImpronte = t.Job != null && t.Job.CavityMoldNum.HasValue && t.Job.CavityMoldNum > 0 ? t.Job.CavityMoldNum.Value : 1;

        //            //TODO: riportare l'ultimo lavoro con produzione o il turno scelto all'ingresso per sapere la quantità prodotta
        //        }
        //    }
        //    else
        //    {
        //        //macchina automatica
        //        jobId = transOperRepos.ReadAll(x => x.MachineId == hiddenMachineId.Value && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First().JobId;

        //        List<Transaction> currentJobTList = transService.GetTransactionListByJobId(hiddenMachineId.Value, jobId);
        //        if (currentJobTList != null && currentJobTList.Count > 0)
        //        {
        //            QtyProducedTot = Math.Round(currentJobTList.Sum(x => x.PartialCounting), 0);

        //            nImpronte = currentJobTList.First().Job.CavityMoldNum.HasValue && currentJobTList.First().Job.CavityMoldNum > 0 ? currentJobTList.First().Job.CavityMoldNum.Value : 1;
        //        }

        //        lastTrans = transService.GetLastTransactionOpen(hiddenMachineId.Value);
        //    }




        //    ProductionPayment versamento = new ProductionPayment();
        //    versamento.MachineId = hiddenMachineId.Value;
        //    versamento.JobId = jobId;
        //    versamento.OperatorId = hiddenOperatorId.Value;
        //    versamento.QtyOperatorShotProduced = QtyProducedTot;
        //    versamento.QtyOperatorProduced = QtyProducedTot * nImpronte;
        //    versamento.QtyProductionWaste = grantService.AllowQuality(hiddenOperatorId.Value) ? sr.Quality : sr.Num;
        //}

        protected void btnConfirmCloseOrder_Click(object sender, EventArgs e)
        {
            dlgCloseCustomerOrder.CloseDialog();
        }

        protected void btnCancelCloseOrder_Click(object sender, EventArgs e)
        {
            dlgCloseCustomerOrder.CloseDialog();
        }
    }
}