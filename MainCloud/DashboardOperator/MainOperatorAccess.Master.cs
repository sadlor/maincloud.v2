﻿using AssetManagement.Repositories;
using MainCloudFramework;
using MainCloudFramework.Core.Utilities;
using MainCloudFramework.Models;
using MainCloudFramework.Repository;
using MainCloudFramework.Services;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web.Multitenants;
using MES.Core;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.DashboardOperator
{
    public partial class MainOperatorAccess : BaseMasterPage
    {
        private void AreaDataBind()
        {
            //WidgetAreaService widgetAreaService = new WidgetAreaService();
            //string applicationId = MultiTenantsHelper.ApplicationId;
            //string userId = Page.User.Identity.GetUserId();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session.SessionMultitenant().ContainsKey(MesConstants.DEPARTMENT_ZONE) && !string.IsNullOrEmpty(Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString()))
            {
                string zoneId = Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString();
                ZoneRepository zoneRep = new ZoneRepository();
                Page.Title = zoneRep.FindByID(zoneId).Description;

                btnSelectZone.Text = "Altre zone";
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
        }

        #region Zone
        protected void btnSelectZone_Click(object sender, EventArgs e)
        {
            dlgSelectZone.OpenDialog();
        }

        protected void gridSelectZone_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            AssetManagement.Repositories.ZoneRepository zoneRep = new AssetManagement.Repositories.ZoneRepository();
            var dataSource = zoneRep.ReadAll().OrderBy(x => x.Code).ToList();
            gridSelectZone.DataSource = dataSource;
        }

        protected void btnConfirmZone_Click(object sender, EventArgs e)
        {
            //Salvare la zona in sessione e procedere con l'autologin

            GridDataItem item = (GridDataItem)gridSelectZone.SelectedItems[0]; //get selected zone row
            string zoneId = item.GetDataKeyValue("Id").ToString();
            //se ti serve, questo è l'id della zona selezionata

            AutoLogIn(zoneId, Page.Request.Path);

            //puoi implementare il salvataggio in sessione

            dlgSelectZone.CloseDialog();
        }

        protected void btnCancelZone_Click(object sender, EventArgs e)
        {
            dlgSelectZone.CloseDialog();
        }

        protected void AutoLogIn(string zoneId, string returnUrl)
        {
            if (!string.IsNullOrEmpty(zoneId))
            {
                AssetManagement.Repositories.ZoneRepository zoneRep = new AssetManagement.Repositories.ZoneRepository();
                AssetManagement.Models.Zone zone = zoneRep.FindByID(zoneId);
                string userName = "zona" + zone.Code + "@mainedge.it";
                string password = "V%43HF.>[uzw'UkA" + userName.Split('@')[0];
                try
                {
                    Context.GetOwinContext().Authentication.SignOut();

                    // Validate the user password
                    var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                    var loggedinUser = manager.Find(userName, password);
                    if (loggedinUser != null)
                    {
                        // Now user have entered correct username and password.
                        // Time to change the security stamp
                        //                    manager.UpdateSecurityStamp(loggedinUser.Id);
                    }

                    // This doen't count login failures towards account lockout
                    // To enable password failures to trigger lockout, change to shouldLockout: true
                    // do sign-in AFTER we have done the update of the security stamp, so the new stamp goes into the cookie
                    var result = signinManager.PasswordSignIn(userName, password, false, shouldLockout: false);

                    //log.Info("LogIn result" + result.ToString());
                    switch (result)
                    {
                        case SignInStatus.Success:
                            //string applicationName = ApplicationSettingsHelper.GetConfiguration(ApplicationSettingsKey.OperatorDefaultApplicationUID) as string;
                            string applicationId = loggedinUser.Applications.FirstOrDefault()?.Id;
                            if (string.IsNullOrEmpty(applicationId))
                            {
                                //FailureText.Text = "Impossibile effettuare l'accesso, application non identificata";
                                //ErrorMessage.Visible = true;
                            }
                            else
                            {
                                Session.SessionMultitenant()[MES.Core.MesConstants.DEPARTMENT_ZONE] = zoneId;
                                Session.SessionMultitenant()[MES.Core.MesConstants.APPLICATION_ID] = applicationId;
                                IdentityHelper.RedirectToReturnUrl(returnUrl, Response);
                            }
                            break;
                        case SignInStatus.LockedOut:
                            Response.Redirect("/Account/Lockout");
                            break;
                        case SignInStatus.Failure:
                        default:
                            //FailureText.Text = "Impossibile effettuare l'accesso";
                            //ErrorMessage.Visible = true;
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    //log.Info("errore login" + ex.Message);
                }
            }
        }
        #endregion

        #region Machine
        protected void btnSelectMachine_Click(object sender, EventArgs e)
        {
            btnConfirmMachine.Enabled = false;
            dlgSelectMachine.OpenDialog();
            gridSelectMachine.Rebind();
        }

        protected void gridSelectMachine_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (dlgSelectMachine.IsOpen())
            {
                AssetRepository assetRep = new AssetRepository();
                var dataSource = assetRep.ReadAll(x => !string.IsNullOrEmpty(x.ZoneId)).OrderBy(x => x.Code.Length).ThenBy(x => x.Code).ToList();
                gridSelectMachine.DataSource = dataSource;
            }
        }

        protected void btnConfirmMachine_Click(object sender, EventArgs e)
        {
            if (gridSelectMachine.SelectedItems.Count > 0)
            {
                string zoneId = ((GridDataItem)gridSelectMachine.SelectedItems[0]).GetDataKeyValue("ZoneId").ToString();
                string assetId = ((GridDataItem)gridSelectMachine.SelectedItems[0]).GetDataKeyValue("Id").ToString();
                AutoLogIn(zoneId, string.Format("/DashboardOperator/MachinePage.aspx?asset={0}", assetId));
            }
            dlgSelectMachine.CloseDialog();
        }

        protected void btnCancelMachine_Click(object sender, EventArgs e)
        {
            dlgSelectMachine.CloseDialog();
        }
        #endregion
    }
}