﻿<%@ Page Language="C#" MasterPageFile="~/DashboardOperator/MainDashboardOperator.Master" AutoEventWireup="true" CodeBehind="MachineDetail.aspx.cs" Inherits="MainCloudFramework.DashboardOperator.MachineDetail" %>
<%--<%@ Register TagPrefix="operator" TagName="ControlPlanExecution" Src="~/DashboardOperator/ControlPlanExecutionList.ascx" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="Style.css" rel="stylesheet" type="text/css">
    <link href="ControlPlan.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:UpdatePanel runat="server" ID="updatePanelIntestazione" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnConfirmUpdateTransaction" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlJob">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:Label Text="Commessa:" runat="server" AssociatedControlID="txtCustomerOrder" Width="85px" style="line-height:2" />
                        <telerik:RadButton runat="server" ID="txtCustomerOrder" BackColor="White" ForeColor="Black" Width="201px" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:Label Text="Articolo:" runat="server" AssociatedControlID="txtArticleCode" Width="85px" style="line-height:2" />
                        <telerik:RadButton runat="server" ID="txtArticleCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" />
                        <telerik:RadButton runat="server" ID="txtArticleDescription" Width="304px" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:Label Text="Stampo:" runat="server" AssociatedControlID="txtMold" Width="85px" style="line-height:2" />
                        <telerik:RadButton runat="server" ID="txtMold" Enabled="false" BackColor="White" ForeColor="Black" Width="201px"  />
                        <telerik:RadTextBox runat="server" ID="txtNImpronte" Width="304px" Enabled="false" ReadOnly="true" BackColor="White" BorderColor="#cccccc" DisabledStyle-HorizontalAlign="Center" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:Label Text="MP:" runat="server" AssociatedControlID="txtMPCode" Width="85px" style="line-height:2" />
                        <telerik:RadButton runat="server" ID="txtMPCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" />
                        <telerik:RadButton runat="server" ID="txtMPDescription" Width="304px" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <telerik:RadTextBox runat="server" ID="txtBatchCode" Width="290px" Label="Lotto:" LabelWidth="83px" Height="34px" />
                        <%--<asp:Label Text="Lotto:" runat="server" AssociatedControlID="txtBatchCode" Width="85px" style="line-height:2" />
                        <telerik:RadButton runat="server" ID="txtBatchCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" />--%>
                    </div>
                </div>
            </asp:Panel>
            <%--<div class="row">
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <b><%: GetGlobalResourceObject("Mes","Order") %>:</b>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                    <telerik:RadTextBox runat="server" ID="txtCodeOrder" ReadOnlyStyle-BackColor="White" ForeColor="Black" EmptyMessage="<%$ Resources:Mes,Code %>" ReadOnly="true" Width="100%" />
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                    <telerik:RadTextBox runat="server" ID="txtCodeCustomerOrder" ReadOnlyStyle-BackColor="White" ForeColor="Black" ReadOnly="true" Width="100%" />
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                    <telerik:RadTextBox runat="server" ID="txtCodeArticle" ReadOnlyStyle-BackColor="White" ForeColor="Black" EmptyMessage="<%$ Resources:Mes,Code %>" ReadOnly="true" Width="100%" />
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 textNoPadding">
                    <telerik:RadTextBox runat="server" ID="txtDescriptionArticle" ReadOnlyStyle-BackColor="White" ForeColor="Black" EmptyMessage="<%$ Resources:Mes,Description %>" ReadOnly="true" Width="100%" />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <b><%: GetGlobalResourceObject("Mes","Mold") %>:</b>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                    <telerik:RadTextBox runat="server" ID="txtCodeMold" ReadOnlyStyle-BackColor="White" ForeColor="Black" EmptyMessage="<%$ Resources:Mes,Code %>" ReadOnly="true" Width="100%" />
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                    <telerik:RadTextBox runat="server" ID="txtNImpronte" ReadOnlyStyle-BackColor="White" ForeColor="Black" ReadOnly="true" Width="100%" />
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                    <telerik:RadTextBox runat="server" ID="txtCodeMP" ReadOnlyStyle-BackColor="White" ForeColor="Black" EmptyMessage="MP" ReadOnly="true" Width="100%" />
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 textNoPadding">
                    <telerik:RadTextBox runat="server" ID="txtDescriptionMP" ReadOnlyStyle-BackColor="White" ForeColor="Black" ReadOnly="true" Width="100%" />
                </div>
            </div>--%>
        </ContentTemplate>
    </asp:UpdatePanel>

    <br /><br />

    <div class="row">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <telerik:RadButton ID="RadButton1" runat="server" Text="Disegno" CssClass="btnActivityRead" />
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <telerik:RadButton ID="RadButton2" runat="server" Text="Piani di setup" CssClass="btnActivityRead" />
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <telerik:RadButton ID="RadButton3" runat="server" Text="Note sicurezza" CssClass="btnActivityRead" />
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <telerik:RadButton ID="btnOEE" runat="server" Text="OEE" CssClass="btnActivityRead" OnClick="btnOEE_Click" />
        </div>
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
        </div>
    </div>

    <br />

    <asp:UpdatePanel runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnConfirmUpdateTransaction" EventName="Click" />
        </Triggers>
        <ContentTemplate>

    <div class="row">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <telerik:RadButton ID="btnProduzione" runat="server" Text="Produzione" OnClick="btnProduzione_Click" Visible="false" />
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <telerik:RadButton ID="btnAttrezzaggio" runat="server" Text="Attrezzaggio" OnClick="btnAttrezzaggio_Click" CssClass="btnActivityDisabled" Visible="false" />
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding-right:0;">
            <telerik:RadButton ID="btnManutenzione" runat="server" Text="Manutenzione" OnClick="btnManutenzione_Click" Visible="false">
                <%--<ContentTemplate>
                    <center>Manutenzione<i class="fa fa-briefcase" aria-hidden="true"></i></center>
                </ContentTemplate>--%>
            </telerik:RadButton>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding-left:0;">
            <telerik:RadButton ID="btnWarningMaintenance" runat="server" Text="Avvisi" OnClick="btnWarningMaintenance_Click" CssClass="btnActivityWarning" Visible="false" />
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <telerik:RadButton ID="btnFermi" runat="server" Text="Fermi" OnClick="btnFermi_Click" CssClass="btnActivityDisabled" BackColor="#dddddd" Enabled="false" />
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <telerik:RadButton ID="btnScarti" runat="server" Text="Scarti" OnClick="btnScarti_Click" CssClass="btnActivityDisabled" Enabled="false" />
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <telerik:RadButton ID="btnQualità" runat="server" Text="Piani di controllo" OnClick="btnQualità_Click" CssClass="btnActivity" />
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <telerik:RadButton ID="btnExit" runat="server" BackColor="#d9534f" CssClass="btnActivity" OnClick="btnExit_Click">
                <ContentTemplate>
                    <center><i class="fa fa-window-close" aria-hidden="true" style="color:black"></i> Esci</center>
                </ContentTemplate>
            </telerik:RadButton>
        </div>
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <telerik:RadButton ID="btnWarningQuality" runat="server" Text="Avvisi" OnClick="btnWarningQuality_Click" CssClass="btnActivityWarning" Visible="false" />
        </div>
    </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <mcf:PopUpDialog ID="dlgUpdateTransaction" Title="Esegui" runat="server">
        <Body>
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <Triggers>
                    <%--<asp:AsyncPostBackTrigger ControlID="btnAttrezzaggio" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnProduzione" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="PopUpDialogSelection$btnConfirmSelection" EventName="Click" />--%>
                </Triggers>
                <ContentTemplate>
                    <telerik:RadButton Text="Job" ID="btnShowJob" runat="server" CssClass="btnActivity" OnClick="btnShowJob_Click" />
                    <telerik:RadTextBox runat="server" ID="txtSelectedJob" ReadOnly="true" />
                    <asp:HiddenField runat="server" ID="hiddenSelectedJobId" />
                    <br /><br />
                    <telerik:RadButton Text="<%$ Resources:Mes,Mold %>" ID="btnShowMold" runat="server" CssClass="btnActivity" OnClick="btnShowMold_Click" />
                    <telerik:RadTextBox runat="server" ID="txtSelectedMold" ReadOnly="true" />
                    <asp:HiddenField runat="server" ID="hiddenSelectedMoldId" />
                    <br /><br />
                    <telerik:RadButton Text="MP" ID="btnShowMP" runat="server" CssClass="btnActivity" OnClick="btnShowMP_Click" />
                    <telerik:RadTextBox runat="server" ID="txtSelectedMP" ReadOnly="true" />
                    <asp:HiddenField runat="server" ID="hiddenSelectedMPId" />
                    <br /><br />
                    <div class="row alDx">
                        <telerik:RadButton ID="btnConfirmUpdateTransaction" runat="server" OnClick="btnConfirmUpdateTransaction_Click" CssClass="btnActivity">
                            <ContentTemplate>
                                <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnCancelUpdateTransaction" runat="server" OnClick="btnCancelUpdateTransaction_Click" CssClass="btnActivity">
                            <ContentTemplate>
                                <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>

    <%--<asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowJob" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowMold" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowMP" EventName="Click" />
        </Triggers>
        <ContentTemplate>--%>
    <mcf:PopUpDialog ID="PopUpDialogSelection" Title="Seleziona" runat="server" Width="80%" MarginLeft="10%">
        <Body>
            <br />
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowJob" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowMold" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="dlgUpdateTransaction$btnShowMP" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <telerik:RadGrid runat="server" ID="tableSelection" RenderMode="Lightweight" AutoGenerateColumns="false"
                        LocalizationPath="~/App_GlobalResources/">
                        <MasterTableView FilterExpression="" Caption="">
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <asp:Panel ID="panelMP" runat="server" Visible="false">
                        <asp:TextBox runat="server" ID="txtNewCodeRawMaterial" />
                        <telerik:RadGrid runat="server" ID="tableMP" RenderMode="Lightweight" AutoGenerateColumns="false"
                            LocalizationPath="~/App_GlobalResources/" DataSourceID="edsMP">
                            <MasterTableView FilterExpression="" Caption="MP" DataKeyNames="Id">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Code" HeaderText="MP">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Description" HeaderText="Lotto">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </asp:Panel>
                    <br />
                    <div class="row alDx">
                        <telerik:RadButton ID="btnConfirmSelection" runat="server" OnClick="btnConfirmSelection_Click" CssClass="btnActivity">
                            <ContentTemplate>
                                <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnCancelSelection" runat="server" OnClick="btnCancelSelection_Click" CssClass="btnActivity">
                            <ContentTemplate>
                                <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>
      <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>

    <mcf:PopUpDialog ID="dlgManutenzione" Title="Manutenzione" runat="server">
        <Body>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <b>Scegli causale manutenzione</b>
                    <br /><br />
                    <telerik:RadTreeView RenderMode="Lightweight" runat="Server" ID="manutenzioneTreeView">
                    </telerik:RadTreeView>
                    <br /><br />
                    <div class="alDx">
                        <telerik:RadButton ID="btnConfirmManutenzione" runat="server" CssClass="btnActivity" OnClick="btnConfirmManutenzione_Click">
                            <ContentTemplate>
                                <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnCancelManutenzione" runat="server" CssClass="btnActivity" OnClick="btnCancelManutenzione_Click">
                            <ContentTemplate>
                                <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>

    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnFermi" />
        </Triggers>
        <ContentTemplate>
    <mcf:PopUpDialog ID="dlgFermi" runat="server" Width="100%" MarginLeft="0%">
        <Body>
            <div class="row">
                <asp:UpdatePanel runat="server" ID="updatePnlFermi" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="causeTreeView" EventName="NodeClick" />
                    </Triggers>
                    <ContentTemplate>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <telerik:RadGrid runat="server" ID="gridFermi" RenderMode="Lightweight" AutoGenerateColumns="false" AllowMultiRowSelection="false"
                                OnItemDataBound="gridFermi_ItemDataBound">
                                <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
                                <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id">
                                    <Columns>
                                        <telerik:GridDateTimeColumn DataField="Start" HeaderText="<%$ Resources:Mes,Start %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridDateTimeColumn DataField="End" HeaderText="<%$ Resources:Mes,End %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridTemplateColumn DataField="Duration" HeaderText="Durata[m]">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem,"Duration")) / 60, 0) %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:Mes,Status %>">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="<%$ Resources:Mes,Operator %>">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridDropDownColumn DataField="CauseId" HeaderText="<%$ Resources:Mes,Cause %>" UniqueName="ddlCause"
                                            ListTextField="Code" ListValueField="Id" DataSourceID="edsCause" DropDownControlType="RadComboBox"
                                            ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                                        </telerik:GridDropDownColumn>
                                        <telerik:GridBoundColumn DataField="Job.Order.Article.Code" HeaderText="<%$ Resources:Mes,Article %>">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True" ScrollHeight="650px" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <telerik:RadTreeView RenderMode="Lightweight" runat="Server" ID="causeTreeView" OnNodeClick="causeTreeView_NodeClick">
                            </telerik:RadTreeView>
                            <br /><br />
                            <div style="text-align:right">
                                <telerik:RadButton ID="btnConfirmCausaleFermo" runat="server" OnClick="btnConfirmCausaleFermo_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                <telerik:RadButton ID="btnCancelCausaleFermo" runat="server" CssClass="btnActivity" OnClick="btnCancelCausaleFermo_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </Body>
    </mcf:PopUpDialog>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%--<mcf:PopUpDialog ID="dlgFermi" runat="server" Title="Fermi" Width="100%" MarginLeft="0%">
        <Body>
            <div class="row">
                <asp:UpdatePanel runat="server" ID="updatePnlFermi" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnFermi" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="causeTreeView" EventName="NodeClick" />
                        </Triggers>
                        <ContentTemplate>
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    
                            <telerik:RadGrid runat="server" ID="gridFermi" RenderMode="Lightweight" AutoGenerateColumns="false" AllowMultiRowSelection="false"
                                OnItemDataBound="gridFermi_ItemDataBound">
                                <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
                                <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id">
                                    <Columns>
                                        <telerik:GridDateTimeColumn DataField="Start" HeaderText="<%$ Resources:Mes,Start %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridDateTimeColumn DataField="End" HeaderText="<%$ Resources:Mes,End %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridCalculatedColumn DataFields="Duration" HeaderText="Durata[m]" Expression="Duration / 60" DataType="System.Decimal" DataFormatString="{0:F2}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </telerik:GridCalculatedColumn>
                                        <telerik:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:Mes,Status %>">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridDropDownColumn DataField="OperatorId" HeaderText="<%$ Resources:Mes,Operator %>" UniqueName="ddlOperator"
                                            ListTextField="UserName" ListValueField="Id" DataSourceID="edsOperator" DropDownControlType="RadComboBox"
                                            ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                                        </telerik:GridDropDownColumn>
                                        <telerik:GridDropDownColumn DataField="CauseId" HeaderText="<%$ Resources:Mes,Cause %>" UniqueName="ddlCause"
                                            ListTextField="Description" ListValueField="Id" DataSourceID="edsCause" DropDownControlType="RadComboBox"
                                            ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                                        </telerik:GridDropDownColumn>
                                        <telerik:GridDropDownColumn DataField="IdArticle" HeaderText="<%$ Resources:Mes,Article %>" UniqueName="ddlArticle"
                                            ListTextField="Code" ListValueField="Id" DataSourceID="edsCause" DropDownControlType="RadComboBox"
                                            ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                                        </telerik:GridDropDownColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True" ScrollHeight="650px" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <b><%: GetGlobalResourceObject("Mes", "Causes") %>:</b>
                    <br /><br />
                    <telerik:RadTreeView RenderMode="Lightweight" runat="Server" ID="causeTreeView" OnNodeClick="causeTreeView_NodeClick">
                    </telerik:RadTreeView>
                    <br /><br />
                    <telerik:RadButton ID="btnConfirmCausaleFermo" runat="server" CssClass="btnActivity" OnClick="btnConfirmCausaleFermo_Click">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnCancelCausaleFermo" runat="server" CssClass="btnActivity" OnClick="btnCancelCausaleFermo_Click">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
                </ContentTemplate>
                    </asp:UpdatePanel>
            </div>
        </Body>
    </mcf:PopUpDialog>--%>

    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnScarti" />
        </Triggers>
        <ContentTemplate>
    <mcf:PopUpDialog ID="dlgScarti" runat="server" Width="90%" MarginLeft="5%">
        <Body>
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <asp:HiddenField runat="server" ID="hiddenOperatorId" />
                            <telerik:RadTextBox runat="server" ID="txtQtyProducedTot" ReadOnly="true" Label="Qta prodotta totale:" LabelWidth="136px" Width="230px" />
                            &nbsp; &nbsp; &nbsp;
                            <telerik:RadTextBox runat="server" ID="txtQtyProducedOperator" ReadOnly="true" Label="Qta prodotta:" LabelWidth="95px" Width="190px" />
                            &nbsp; &nbsp; &nbsp;
                            <telerik:RadTextBox runat="server" ID="txtQtyWasteTransaction" ReadOnly="true" Label="Qta scarti:" LabelWidth="76px" Width="185px" />
                            <br /><br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <telerik:RadGrid runat="server" ID="gridScarti" RenderMode="Lightweight" AutoGenerateColumns="false">
                                <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridNumericColumn DataField="Num" HeaderText="Num">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </telerik:GridNumericColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <br /><br /><br /><br />
                            <asp:Panel Width="330px" ID="pnlTastieraNum" runat="server" Visible="true">
                                <telerik:RadTextBox runat="server" ID="txtInsValue" ReadOnly="true" RenderMode="Lightweight" Width="278px" Visible="true">
                                    <ReadOnlyStyle BackColor="White" ForeColor="Black" />
                                </telerik:RadTextBox>
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn7" runat="server" Text="7" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn8" runat="server" Text="8" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn9" runat="server" Text="9" OnClientClick="ModifyNumber(this)" />

                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn4" runat="server" Text="4" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn5" runat="server" Text="5" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn6" runat="server" Text="6" OnClientClick="ModifyNumber(this)" />

                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn1" runat="server" Text="1" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn2" runat="server" Text="2" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn3" runat="server" Text="3" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="184px" Height="60px" ID="btn0" runat="server" Text="0" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btnVir" runat="server" Text="," OnClientClick="ModifyNumber(this)" />
                                <asp:Button Width="184px" Height="60px" ID="btnNumPadOk" runat="server" Primary="false" Text="Ok" OnClick="btnNumPadOk_Click" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btnCanc" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyNumber(this)" />
                             </asp:Panel>

                            <script type="text/javascript">
                                function ModifyNumber(sender) {
                                    var textBox = $find("<%= txtInsValue.ClientID %>");
                                    var txtValue = textBox.get_value();
                                    var char = sender.value;//sender.get_text();
                                    if (char == "Canc") {
                                        if (txtValue.length > 0) {
                                            textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                                        }
                                    }
                                    else {
                                        if (char == ',' || char == '.') {
                                            if (txtValue.includes(',') || txtValue.includes('.')) { }
                                            else {
                                                if (txtValue.length == 0) {
                                                    textBox.set_value('0' + char);
                                                }
                                                else {
                                                    textBox.set_value(txtValue + char);
                                                }
                                            }
                                        }
                                        else {
                                            textBox.set_value(txtValue + char);
                                        }
                                    }
                                }
                            </script>

                            <br /><br /><br />
                            <div class="row" style="text-align:center;">
                                <telerik:RadButton ID="btnConfirmScarti" runat="server" CssClass="btnActivity" OnClick="btnConfirmScarti_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                <telerik:RadButton ID="btnCancelScarti" runat="server" CssClass="btnActivity" OnClick="btnCancelScarti_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%--<mcf:PopUpDialog ID="dlgScarti" runat="server" Title="Scarti" Width="90%" MarginLeft="5%">
        <Body>
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <telerik:RadTextBox runat="server" ID="txtQtyProducedTransaction" ReadOnly="true" Label="Qta prodotta:" LabelWidth="95px" />
                            <br />
                            <telerik:RadTextBox runat="server" ID="txtQtyWasteTransaction" ReadOnly="true" Label="Qta scarti:" LabelWidth="95px" />
                            <br /><br />
                            <telerik:RadGrid runat="server" ID="gridScarti" RenderMode="Lightweight" AutoGenerateColumns="false">
                                <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridNumericColumn DataField="Num" HeaderText="Num">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </telerik:GridNumericColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <br /><br /><br /><br />
                            <asp:Panel Width="330px" ID="pnlTastieraNum" runat="server" Visible="true">
                                <telerik:RadTextBox runat="server" ID="txtInsValue" ReadOnly="true" RenderMode="Lightweight" Width="278px" Visible="true">
                                    <ReadOnlyStyle BackColor="White" ForeColor="Black" />
                                </telerik:RadTextBox>
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn7" runat="server" Text="7" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn8" runat="server" Text="8" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn9" runat="server" Text="9" OnClientClick="ModifyNumber(this)" />

                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn4" runat="server" Text="4" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn5" runat="server" Text="5" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn6" runat="server" Text="6" OnClientClick="ModifyNumber(this)" />

                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn1" runat="server" Text="1" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn2" runat="server" Text="2" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn3" runat="server" Text="3" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="184px" Height="60px" ID="btn0" runat="server" Text="0" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btnVir" runat="server" Text="," OnClientClick="ModifyNumber(this)" />
                                <asp:Button Width="184px" Height="60px" ID="btnNumPadOk" runat="server" Primary="false" Text="Ok" OnClick="btnNumPadOk_Click" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btnCanc" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyNumber(this)" />
                             </asp:Panel>
                            <br /><br />
                            <div class="row" style="text-align:center;">
                                <telerik:RadButton ID="btnConfirmScarti" runat="server" CssClass="btnActivity" OnClick="btnConfirmScarti_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                <telerik:RadButton ID="btnCancelScarti" runat="server" CssClass="btnActivity" OnClick="btnCancelScarti_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>--%>

<%--    <mcf:PopUpDialog ID="dlgControlPlan" Title="Piani di controllo" runat="server" Width="90%" MarginLeft="5%">
        <Body>
            <operator:ControlPlanExecution ID="controlPlan" runat="server">
            </operator:ControlPlanExecution>
        </Body>
    </mcf:PopUpDialog>--%>
        
    <mcf:PopUpDialog ID="dlgWarningMaintenance" Title="Avvisi manutenzione" Width="80%" MarginLeft="10%" runat="server">
        <Body>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="panel panel-default">
                        <table class="table table-bordered table-condensed info" style="width:100%;">
                            <tr>
                                <td colspan="3"></td>
                                <td style="background-color:#428bca; color:white; text-align:center"><b>Colpi</b></td>
                                <td style="background-color:#428bca; color:white; text-align:center"><b>Tempo</b></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>Ultima manutenzione</b></td>
                                <td runat="server" id="TotalShotsLastField" style="text-align:right;"></td>
                                <td runat="server" style="text-align:right;"></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>Eseguito</b></td>
                                <td runat="server" style="text-align:right;"></td>
                                <td runat="server" style="text-align:right;"></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>Warning1</b></td>
                                <td runat="server" style="text-align:right;"></td>
                                <td runat="server" style="text-align:right;"></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>Warning2</b></td>
                                <td runat="server" style="text-align:right;"></td>
                                <td runat="server" style="text-align:right;"></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>Prossima manutenzione</b></td>
                                <td runat="server" style="text-align:right;"></td>
                                <td runat="server" style="text-align:right;"></td>
                            </tr>
                        </table>
                    </div>
                    <br />
                    <div class="row alDx">
                        <telerik:RadButton ID="btnCloseWarningMaintenance" runat="server" CssClass="btnActivity" OnClick="btnCloseWarningMaintenance_Click">
                            <ContentTemplate>
                                <center><i class="fa fa-times" aria-hidden="true"></i> Chiudi </center>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>

    <mcf:PopUpDialog ID="dlgWarningQuality" Title="Avvisi qualità" Width="80%" MarginLeft="10%" runat="server">
        <Body>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="panel panel-default">
                        <table class="table table-bordered table-condensed info" style="width:100%;">
                            <tr>
                                <td colspan="3"></td>
                                <td style="background-color:#428bca; color:white; text-align:center"><b>Colpi</b></td>
                                <td style="background-color:#428bca; color:white; text-align:center"><b>Tempo</b></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>Ultimo controllo</b></td>
                                <td runat="server" id="Td1" style="text-align:right;"></td>
                                <td runat="server" style="text-align:right;"></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>Colpi eseguiti dall'ultimo controllo</b></td>
                                <td runat="server" style="text-align:right;"></td>
                                <td runat="server" style="text-align:right;"></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>Prossimo controllo</b></td>
                                <td runat="server" style="text-align:right;"></td>
                                <td runat="server" style="text-align:right;"></td>
                            </tr>
                        </table>
                    </div>
                    <br />
                    <div class="row alDx">
                        <telerik:RadButton ID="btnCloseWarningQuality" runat="server" CssClass="btnActivity" OnClick="btnCloseWarningQuality_Click">
                            <ContentTemplate>
                                <center><i class="fa fa-times" aria-hidden="true"></i> Chiudi </center>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>

    <mcf:PopUpDialog ID="dlgOEE" Title="OEE" runat="server" Width="90%" MarginLeft="5%">
        <Body>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>

                    <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="jobGrid" AutoGenerateColumns="false" GridLines="none" Skin="Silk" 
                        OnNeedDataSource="jobGrid_NeedDataSource" OnItemDataBound="jobGrid_ItemDataBound"
                        LocalizationPath="~/App_GlobalResources/" Culture="en-US">
                        <MasterTableView FilterExpression="" Caption="">
                            <ColumnGroups>
                                <telerik:GridColumnGroup Name="Tempo" HeaderText="Tempo">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </telerik:GridColumnGroup>
                            </ColumnGroups>
                            <GroupByExpressions>
                                <telerik:GridGroupByExpression>
                                    <SelectFields>
                                        <telerik:GridGroupByField FieldName="GroupType"></telerik:GridGroupByField>
                                    </SelectFields>
                                    <GroupByFields>
                                        <telerik:GridGroupByField FieldName="GroupType" SortOrder="None"></telerik:GridGroupByField>
                                    </GroupByFields>
                                </telerik:GridGroupByExpression>
                            </GroupByExpressions>
                            <Columns>
                                <telerik:GridBoundColumn DataField="Label" HeaderText="">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Pezzi" HeaderText="Pezzi">
                                    <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Colpi" HeaderText="Cicli">
                                    <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Teorico" HeaderText="Teorico">
                                    <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Eseguito" HeaderText="Eseguito">
                                    <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Tempo" HeaderText="Tempo">
                                    <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Efficienza" HeaderText="Efficienza %">
                                    <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                    </telerik:RadGrid>

                    <br />

                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="row" style="text-align:center;">
                                <center><asp:Label Font-Bold="true" Text="Disponibilità" runat="server" /></center>
                            </div>
                            <div class="row">
                                <center>
                                    <telerik:RadRadialGauge runat="server" ID="meterResa">
                                        <Pointer Value="0">
                                            <Cap Size="0.1" />
                                        </Pointer>
                                        <Scale Min="0" Max="100" MajorUnit="20">
                                            <Labels Format="{0}" />
                                            <Ranges>
                                                <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                                <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                                <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                                <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                            </Ranges>
                                        </Scale>
                                    </telerik:RadRadialGauge>
                                    <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtResa" style="text-align:center" />
                                </center>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="row" style="text-align:center;">
                                <asp:Label Font-Bold="true" Text="Efficienza" runat="server" />
                            </div>
                            <div class="row">
                                <center>
                                    <telerik:RadRadialGauge runat="server" ID="meterEfficienza">
                                        <Pointer Value="0">
                                            <Cap Size="0.1" />
                                        </Pointer>
                                        <Scale Min="0" Max="100" MajorUnit="20">
                                            <Labels Format="{0}" />
                                            <Ranges>
                                                <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                                <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                                <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                                <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                            </Ranges>
                                        </Scale>
                                    </telerik:RadRadialGauge>
                                    <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtEfficienza" style="text-align:center" />
                                </center>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="row" style="text-align:center;">
                                <center><asp:Label Font-Bold="true" Text="Qualità" runat="server" /></center>
                            </div>
                            <div class="row">
                                <center>
                                    <telerik:RadRadialGauge runat="server" ID="meterQualità">
                                        <Pointer Value="0">
                                            <Cap Size="0.1" />
                                        </Pointer>
                                        <Scale Min="0" Max="100" MajorUnit="20">
                                            <Labels Format="{0}" />
                                            <Ranges>
                                                <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                                <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                                <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                                <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                            </Ranges>
                                        </Scale>
                                    </telerik:RadRadialGauge>
                                    <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtQualità" style="text-align:center" />
                                </center>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="row">
                                <center><asp:Label Font-Bold="true" Text="OEE" runat="server" /></center>
                            </div>
                            <div class="row">
                                <center>
                                    <telerik:RadRadialGauge runat="server" ID="meterOEE">
                                        <Pointer Value="0">
                                            <Cap Size="0.1" />
                                        </Pointer>
                                        <Scale Min="0" Max="100" MajorUnit="20">
                                            <Labels Format="{0}" />
                                            <Ranges>
                                                <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                                <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                                <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                                <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                            </Ranges>
                                        </Scale>
                                    </telerik:RadRadialGauge>
                                    <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtOEE" style="text-align:center" />
                                </center>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row alDx">
                        <telerik:RadButton ID="btnCloseOEE" runat="server" CssClass="btnActivity" OnClick="btnCloseOEE_Click">
                            <ContentTemplate>
                                <center><i class="fa fa-times" aria-hidden="true"></i> Chiudi </center>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>

    <ef:EntityDataSource ID="edsJob" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Jobs"
        OrderBy="it.StartDate,it.Order.OrderCode" AutoGenerateWhereClause="true" />
    <ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
        OrderBy="it.Description" AutoGenerateWhereClause="true" />
    <ef:EntityDataSource ID="edsCause" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Causes"
        OrderBy="it.Description" AutoGenerateWhereClause="true" />
    <ef:EntityDataSource ID="edsArticle" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Articles"
        OrderBy="it.Code" AutoGenerateWhereClause="true" />
    <ef:EntityDataSource ID="edsOrder" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Orders"
        OrderBy="it.OrderCode" AutoGenerateWhereClause="true" />
    <ef:EntityDataSource ID="edsCustomerOrder" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="CustomerOrders"
        OrderBy="it.OrderCode" AutoGenerateWhereClause="true" />
    <ef:EntityDataSource ID="edsOperator" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Operators"
        OrderBy="it.UserName" AutoGenerateWhereClause="true" />
    <ef:EntityDataSource ID="edsMP" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="RawMaterials"
        OrderBy="it.Name" AutoGenerateWhereClause="true" />

    <script type="text/javascript">
        function ModifyNumber(sender) {
            var textBox = $find("<%= txtInsValue.ClientID %>");
            var txtValue = textBox.get_value();
            var char = sender.value;//sender.get_text();
            if (char == "Canc") {
                if (txtValue.length > 0) {
                    textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                }
            }
            else {
                if (char == ',' || char == '.') {
                    if (txtValue.includes(',') || txtValue.includes('.')) { }
                    else {
                        if (txtValue.length == 0) {
                            textBox.set_value('0' + char);
                        }
                        else {
                            textBox.set_value(txtValue + char);
                        }
                    }
                }
                else {
                    textBox.set_value(txtValue + char);
                }
            }
        }
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>