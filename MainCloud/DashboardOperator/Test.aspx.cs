﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Multitenants;
using MES.Core;
using MES.Helper;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.DashboardOperator
{
    public partial class Test : BasePage
    {
        protected string AssetId
        {
            get
            {
                return Request.QueryString["asset"];
            }
        }

        private GrantOperator grantService = new GrantOperator();
        //private TransactionRepository transRepos = new TransactionRepository();
        private TransactionService transService = new TransactionService();
        private TransactionOperatorService transOperService = new TransactionOperatorService();
        private IndexService indexService = new IndexService();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Expires = 0;
            //Response.Buffer = true;
            //Response.Clear();
            //Response.AddHeader("Refresh", "10");

            if (!string.IsNullOrEmpty(AssetId))
            {
                if (Session.SessionMultitenant().ContainsKey(MesConstants.DEPARTMENT_ZONE) && !string.IsNullOrEmpty(Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString()))
                {
                    LoadTitle();
                }
                else
                {
                    Response.Redirect("OperatorAccess.aspx");
                }

                //if (!IsPostBack)
                //{
                RefreshPage();
                //}
            }
            else
            {
                Response.Redirect("OperatorAccess.aspx");
            }

            //edsMold.WhereParameters.Clear();
            //edsMold.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
            //edsCause.WhereParameters.Clear();
            //edsCause.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
            //edsMP.WhereParameters.Clear();
            //edsMP.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
        }

        private void LoadTitle()
        {
            string zoneId = Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString();
            ZoneRepository zoneRep = new ZoneRepository();
            AssetRepository assetRep = new AssetRepository();
            OperatorRepository operRep = new OperatorRepository();
            TransactionService TService = new TransactionService();
            Page.Title = string.Format("{0} - {1} - Operatore: {2}", zoneRep.GetDescription(zoneId), assetRep.GetDescription(AssetId), operRep.GetUserName(TService.GetLastTransactionOpen(AssetId)?.OperatorId));
        }

        private void LoadOperatorButtonStyle()
        {
            if (transService.IsOperatorOnMachine(AssetId))
            {
                (btnOperatorLogin.Controls[0] as LiteralControl).Text = (btnOperatorLogin.Controls[0] as LiteralControl).Text.Replace("Inizio", "Cambio");
            }
            else
            {
                (btnOperatorLogin.Controls[0] as LiteralControl).Text = (btnOperatorLogin.Controls[0] as LiteralControl).Text.Replace("Cambio", "Inizio");
            }
            if (transOperService.OperatorsOnMachine(AssetId).Count > 0)
            {
                btnOperatorLogout.Enabled = true;
                if ((btnOperatorLogout.Controls[0] as LiteralControl).Text.Contains("</b>"))
                {
                    (btnOperatorLogout.Controls[0] as LiteralControl).Text = Regex.Replace((btnOperatorLogout.Controls[0] as LiteralControl).Text, "[0-9]{1,}", "").Replace("<b></b>", "");
                }
                (btnOperatorLogout.Controls[0] as LiteralControl).Text = (btnOperatorLogout.Controls[0] as LiteralControl).Text.Replace("Fine", string.Format("Fine <b>{0}</b>", transOperService.OperatorsOnMachine(AssetId).Count));
            }
            else
            {
                btnOperatorLogout.Enabled = false;
                if ((btnOperatorLogout.Controls[0] as LiteralControl).Text.Contains("</b>"))
                {
                    (btnOperatorLogout.Controls[0] as LiteralControl).Text = Regex.Replace((btnOperatorLogout.Controls[0] as LiteralControl).Text, "[0-9]{1,}", "").Replace("<b></b>", "");
                }
            }
        }

        private void LoadTestata()
        {
            Transaction lastTrans = transService.GetLastTransactionOpen(AssetId);

            txtCustomerOrder.Text = lastTrans?.Job?.Order?.CustomerOrder?.OrderCode;
            txtCustomerOrder.Value = "";
            txtOrderQtyOrdered.Text = string.Format("{0:f0}", lastTrans?.Job?.QtyOrdered);
            txtOrderQtyProduced.Text = string.Format("{0:f0}", transService.GetQtyProducedPerJob(lastTrans?.JobId));
            txtOrderQtyLeft.Text = string.Format("{0:f0}", lastTrans?.Job?.QtyOrdered - transService.GetQtyProducedPerJob(lastTrans?.JobId));
            txtNStampateOra.Text = string.Format("Stampate ora = {0}", lastTrans?.Job?.StandardRate);
            txtNStampateOra.Value = lastTrans?.Job?.StandardRate.ToString();
            btnCavityMold.Text = string.Format("Impronte = {0}", lastTrans?.Job?.CavityMoldNum);
            btnCavityMold.Value = lastTrans?.Job?.CavityMoldNum.ToString();
            txtArticleCode.Text = lastTrans?.Job?.Order?.Article?.Code;
            txtArticleDescription.Text = lastTrans?.Job?.Order?.Article?.Description;

            MoldRepository moldRep = new MoldRepository();
            Mold mold = moldRep.FindByID(lastTrans?.MoldId);
            txtMold.Text = mold?.Name; //Code
        }

        private void LoadGrantOperator()
        {
            TransactionService TService = new TransactionService();
            Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);

            //btnScarti.Enabled = grantService.AllowWaste(lastTrans?.OperatorId);
            //SetCssClass(btnQualità);

            //btnFermi.Enabled = grantService.AllowProduction(lastTrans?.OperatorId);
            //SetCssClass(btnScarti);

            //txtCustomerOrder.Enabled = grantService.AllowSelectJob(lastTrans?.OperatorId);
            //SetCssClass(btnFermi);

            //btnOEE.Enabled = grantService.AllowOEE(lastTrans.OperatorId);
        }

        private void RefreshPage()
        {
            LoadOperatorButtonStyle();
            LoadTestata();
            LoadGrantOperator();
            //CreateIndexes();
        }

        private void SetCssClass(RadButton button)
        {
            if (button.Enabled)
            {
                button.CssClass = "btnActivity";
            }
            else
            {
                button.CssClass = "btnActivityDisabled";
            }
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            Response.Redirect("OperatorAccess.aspx");
        }

        #region Operator buttons
        protected void btnOperatorLogin_Click(object sender, EventArgs e)
        {
            //AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            //Transaction lastTransOpen = transService.GetLastTransactionOpen(AssetId);

            //ModifyMachineTransaction.Value = bool.TrueString;

            //btnSetUp.Enabled = false;
            //btnSetUp.CssClass = "btnActivityDisabled";
            //btnProduction.Enabled = false;
            //btnProduction.CssClass = "btnActivityDisabled";
            //btnMaintenance.Enabled = false;
            //btnMaintenance.CssClass = "btnActivityDisabled";

            //btnConfirmConnect.Enabled = false;
            //btnConfirmConnect.CssClass = "btnActivityDisabled";

            //txtCustomerOrder_Login.Enabled = false;
            //btnCavityMold_Login.Enabled = false;
            //txtMold_Login.Enabled = false;

            ////txtBadge.Text = transService.GetLastOperatorOnMachine(assetId)?.Badge;
            ////txtBadge_TextChanged(txtBadge, EventArgs.Empty);

            //txtCustomerOrder_Login.Text = lastTransOpen?.Job?.Order?.CustomerOrder?.OrderCode;
            //txtCustomerOrder_Login.Value = "";
            //txtNStampateOra_Login.Text = string.Format("Stampate ora = {0}", lastTransOpen?.Job?.NStampateOra);
            //txtNStampateOra_Login.Value = lastTransOpen?.Job?.NStampateOra.ToString();
            //btnCavityMold_Login.Text = string.Format("Impronte = {0}", lastTransOpen?.Job?.CavityMoldNum);
            //btnCavityMold_Login.Value = lastTransOpen?.Job?.CavityMoldNum.ToString();
            //txtArticleCode_Login.Text = lastTransOpen?.Job?.Order?.Article?.Code;
            //txtArticleDescription_Login.Text = lastTransOpen?.Job?.Order?.Article?.Description;

            //ArticleMoldService AMS = new ArticleMoldService();
            //if (!string.IsNullOrEmpty(lastTransOpen?.MoldId))
            //{
            //    txtMold_Login.Text = AMS.GetMoldById(lastTransOpen?.MoldId).Code;
            //}
            //txtMold_Login.Value = "";

            //OperatorService OS = new OperatorService();
            //if (!string.IsNullOrEmpty(lastTransOpen?.OperatorId))
            //{
            //    ConnectWindow.Title = string.Format("Macchina: {0} - Operatore attuale: {1}", AS.GetAssedById(AssetId, OperatorHelper.ApplicationId).Description, OS.GetOperatorNameById(lastTransOpen.OperatorId, OperatorHelper.ApplicationId));
            //}
            //else
            //{
            //    ConnectWindow.Title = string.Format("Macchina: {0}", AS.GetAssedById(AssetId, OperatorHelper.ApplicationId).Description);
            //}
            //ConnectWindow.OpenDialog();
        }

        protected void btnOperatorLogout_Click(object sender, EventArgs e)
        {
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();

            //btnConfirmLogout.Enabled = false;
            //btnConfirmLogout.CssClass = "btnActivityDisabled";

            //hiddenQualityLevelValue.Value = null;
            //pnlAllowConfirmProduction.Visible = false;

            //LogoutWindow.Title = string.Format("Logout - Macchina: {0}", AS.GetAssedById(AssetId, OperatorHelper.ApplicationId).Description);
            //LogoutWindow.OpenDialog();
            //gridOperator.Rebind();
        }

        protected void btnAddOperator_Click(object sender, EventArgs e)
        {
            //AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            //Transaction lastTransOpen = transService.GetLastTransactionOpen(AssetId);

            //ModifyMachineTransaction.Value = bool.FalseString;

            //btnSetUp.Enabled = false;
            //btnSetUp.CssClass = "btnActivityDisabled";
            //btnProduction.Enabled = false;
            //btnProduction.CssClass = "btnActivityDisabled";
            //btnMaintenance.Enabled = false;
            //btnMaintenance.CssClass = "btnActivityDisabled";

            //txtCustomerOrder_Login.Text = lastTransOpen?.Job?.Order?.CustomerOrder?.OrderCode;
            //txtCustomerOrder_Login.Value = "";
            //txtCustomerOrder_Login.Enabled = false;
            //txtNStampateOra_Login.Text = string.Format("Stampate ora = {0}", lastTransOpen?.Job?.NStampateOra);
            //txtNStampateOra_Login.Value = "";
            //btnCavityMold_Login.Text = string.Format("Impronte = {0}", lastTransOpen?.Job?.CavityMoldNum);
            //btnCavityMold_Login.Value = "";
            //btnCavityMold_Login.Enabled = false;
            //txtArticleCode_Login.Text = lastTransOpen?.Job?.Order?.Article?.Code;
            //txtArticleDescription_Login.Text = lastTransOpen?.Job?.Order?.Article?.Description;

            //ArticleMoldService AMS2 = new ArticleMoldService();
            //if (!string.IsNullOrEmpty(lastTransOpen?.MoldId))
            //{
            //    txtMold_Login.Text = AMS2.GetMoldById(lastTransOpen?.MoldId).Code;
            //}
            //txtMold_Login.Value = "";
            //btnConfirmConnect.Enabled = false;
            //btnConfirmConnect.CssClass = "btnActivityDisabled";
            //ConnectWindow.Title = string.Format("Operatore Aggiuntivo - Macchina: {0}", AS.GetAssedById(AssetId, OperatorHelper.ApplicationId).Description);
            //ConnectWindow.OpenDialog();
        }
        #endregion

        protected void btnRefreshChart_Click(object sender, EventArgs e)
        {
        }
    }
}