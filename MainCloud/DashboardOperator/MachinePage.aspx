﻿<%@ Page Language="C#" MasterPageFile="~/DashboardOperator/MainDashboardOperator.Master" AutoEventWireup="true" CodeBehind="MachinePage.aspx.cs" Inherits="MainCloud.DashboardOperator.MachinePage" %>
<%--<%@ Register TagPrefix="operator" TagName="ControlPlanExecution" Src="~/DashboardOperator/ControlPlanExecutionList.ascx" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="Style.css?2" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="Script.js"></script>
    <%--<link href="ControlPlan.css" rel="stylesheet" />--%>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:UpdateProgress runat="server" ID="progressPnl">
        <ProgressTemplate>
            <div class="loading">Loading&#8230;</div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    
    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnConfirmConnect" />
            <asp:PostBackTrigger ControlID="btnConfirmLogout" />
            <asp:AsyncPostBackTrigger ControlID="btnCancelConnect" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancelLogout" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnConfirmScarti" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancelScarti" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnConfirmCausaleFermo" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancelCausaleFermo" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnConfirmFermiManuali" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancelFermiManuali" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRefreshChart" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCloseOEE" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCloseArticleImage" EventName="Click" />
<%--        <asp:AsyncPostBackTrigger ControlID="controlPlan$btnSaveData" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="controlPlan$btnCloseData" EventName="Click" />--%>
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlPrimary">
                <div class="row">
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 noPaddingRight">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadButton runat="server" ID="btnOperatorLogin" CommandName="Login" CssClass="btnActivity" OnClick="btnOperatorLogin_Click" EnableViewState="false" PressedCssClass="btnActivityClick">
                                    <ContentTemplate>
                                        <center><i class="fa fa-sign-in" aria-hidden="true"></i> Inizio</center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadButton runat="server" ID="btnOperatorLogout" CssClass="btnActivityTable" DisabledButtonCssClass="btnActivityTableDisabled" CommandName="Logout" OnClick="btnOperatorLogout_Click" EnableViewState="false">
                                    <ContentTemplate>
                                        <center><i class="fa fa-sign-out" aria-hidden="true"></i> Fine</center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                                <telerik:RadButton runat="server" ID="btnAddOperator" CommandName="AddOperator" CssClass="btnActivity" OnClick="btnAddOperator_Click" EnableViewState="false">
                                    <ContentTemplate>
                                        <center><i class="fa fa-plus-circle" aria-hidden="true"></i> Altri</center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                        </div>
                        <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="60000"></asp:Timer>
                        <br />
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                    <asp:Label Text="Stato:" runat="server" Font-Bold="true" Width="85px" style="line-height:2" EnableViewState="false" />
                                </div>
                                <div class="col-xs-custom-3 col-sm-custom-3 col-md-custom-3 col-lg-custom-3" style="padding-right:15px;">
                                    <telerik:RadButton runat="server" ID="btnStateSetup" Text="Attrezzaggio" Width="110" AutoPostBack="false" Enabled="false" EnableViewState="false" />
                                </div>
                                <div class="col-xs-custom-3 col-sm-custom-3 col-md-custom-3 col-lg-custom-3 text-center"">
                                    <telerik:RadButton runat="server" ID="btnStateProd" Text="Produzione" Width="110" AutoPostBack="false" Enabled="false" EnableViewState="false" />
                                </div>
                                <div class="col-xs-custom-3 col-sm-custom-3 col-md-custom-3 col-lg-custom-3 text-center">
                                    <telerik:RadButton runat="server" ID="btnStateMan" Text="Manutenzione" Width="110" AutoPostBack="false" Enabled="false" EnableViewState="false" />
                                </div>
                                <div class="col-xs-custom-3 col-sm-custom-3 col-md-custom-3 col-lg-custom-3 text-right">
                                    <telerik:RadButton runat="server" ID="btnStateStop" Text="Fermo" Width="110" AutoPostBack="false" Enabled="false" EnableViewState="false" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2 textNoPadding">
                                    <asp:Label Text="Commessa:" runat="server" AssociatedControlID="txtCustomerOrder" Width="85px" style="line-height:2" EnableViewState="false" />
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 noPaddingLeft">
                                    <telerik:RadButton runat="server" ID="txtCustomerOrder" Enabled="false" BackColor="White" ForeColor="Black" Width="201px" Font-Bold="true" EnableViewState="false" /> 
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding" style="width:18%;">
                                    <asp:Label Text="Ord:" runat="server" AssociatedControlID="txtOrderQtyOrdered" style="line-height:2" />
                                    <telerik:RadButton runat="server" ID="txtOrderQtyOrdered" Enabled="false" BackColor="White" ForeColor="Black" Font-Bold="true" EnableViewState="false" />
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding" style="width:18%;">
                                    <asp:Label Text="Prod:" runat="server" AssociatedControlID="txtOrderQtyProduced" style="line-height:2" />
                                    <telerik:RadButton runat="server" ID="txtOrderQtyProduced" Enabled="false" BackColor="White" ForeColor="Black" Font-Bold="true" EnableViewState="false" />
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding text-right" style="width:18%;">
                                    <asp:Label Text="Res:" runat="server" AssociatedControlID="txtOrderQtyLeft" style="line-height:2" />
                                    <telerik:RadButton runat="server" ID="txtOrderQtyLeft" Enabled="false" BackColor="White" ForeColor="Black" Font-Bold="true" EnableViewState="false" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2 textNoPadding">
                                    <asp:Label Text="Articolo:" runat="server" AssociatedControlID="txtArticleCode" Width="85px" style="line-height:2" EnableViewState="false" />
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 noPaddingLeft">
                                    <telerik:RadButton runat="server" ID="txtArticleCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" EnableViewState="false" />
                                </div>
                                <div class="col-xs-custom-6 col-sm-custom-6 col-md-custom-6 col-lg-custom-6 textNoPadding">
                                    <telerik:RadButton runat="server" ID="txtArticleDescription" Width="100%" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" EnableViewState="false" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2 textNoPadding">
                                    <asp:Label Text="Stampo:" runat="server" AssociatedControlID="txtMold" Width="85px" style="line-height:2" EnableViewState="false" />
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 noPaddingLeft">
                                    <telerik:RadButton runat="server" ID="txtMold" Enabled="false" BackColor="White" ForeColor="Black" Width="201px" EnableViewState="false" />
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                                    <telerik:RadButton runat="server" ID="btnCavityMold" Enabled="false" BackColor="White" ForeColor="Black" EnableViewState="false" />
                                </div>
                                <div class="col-xs-custom-4 col-sm-custom-4 col-md-custom-4 col-lg-custom-4 textNoPadding">
                                    <telerik:RadButton runat="server" ID="txtNStampateOra" Enabled="false" BackColor="White" ForeColor="Black" ReadOnly="true" Width="100%" EnableViewState="false" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2 textNoPadding">
                                    <asp:Label Text="MP:" runat="server" AssociatedControlID="txtMPCode" Width="85px" style="line-height:2" EnableViewState="false" />
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 noPaddingLeft">
                                    <telerik:RadButton runat="server" ID="txtMPCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" EnableViewState="false" />
                                </div>
                                <div class="col-xs-custom-6 col-sm-custom-6 col-md-custom-6 col-lg-custom-6 textNoPadding">
                                    <telerik:RadButton runat="server" ID="txtMPDescription" Width="100%" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" EnableViewState="false" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2 textNoPadding">
                                    <asp:Label Text="Lotto:" runat="server" AssociatedControlID="txtMPCode" Width="85px" style="line-height:2" EnableViewState="false" />
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 noPaddingLeft">
                                    <telerik:RadTextBox runat="server" ID="txtBatchCode" Width="201px" Height="34px" EnableViewState="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" EnableViewState="false" ChildrenAsTriggers="true">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                            </Triggers>
                            <ContentTemplate>
                                <asp:Panel runat="server" ID="pnlChart" onclick="RefreshChart()" style="z-index:1000;" EnableViewState="false">
                                    <asp:Button runat="server" ID="btnRefreshChart" OnClick="btnRefreshChart_Click" style="display:none;" EnableViewState="false" />
                                    <telerik:RadHtmlChart runat="server" ID="chartOEE" Height="225px" EnableViewState="false">
                                        <PlotArea>
                                            <Series>
                                                <telerik:ColumnSeries DataFieldY="IndexValue" ColorField="IndexColor" Stacked="false" Gap="1.5" Spacing="0.4">
                                                    <LabelsAppearance DataFormatString="{0}" Position="OutsideEnd">
                                                        <TextStyle Bold="true" Margin="-5" />
                                                    </LabelsAppearance>
                                                    <TooltipsAppearance DataFormatString="{0}" Color="White"></TooltipsAppearance>
                                                </telerik:ColumnSeries>
                                            </Series>
                                            <Appearance>
                                                <FillStyle BackgroundColor="Transparent"></FillStyle>
                                            </Appearance>
                                            <XAxis AxisCrossingValue="0" Color="black" MajorTickType="Outside" MinorTickType="Outside"
                                                Reversed="false">
                                                <Items>
                                                    <telerik:AxisItem LabelText="D"></telerik:AxisItem>
                                                    <telerik:AxisItem LabelText="E"></telerik:AxisItem>
                                                    <telerik:AxisItem LabelText="Q"></telerik:AxisItem>
                                                    <telerik:AxisItem LabelText="OEE"></telerik:AxisItem>
                                                </Items>
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="0" Skip="0" Step="1"></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="">
                                                </TitleAppearance>
                                            </XAxis>
                                            <YAxis AxisCrossingValue="0" Color="black" MajorTickSize="1" MajorTickType="Outside" MinValue="0" MaxValue="100"
                                                MinorTickType="None" Reversed="false">
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="0" Skip="0" Step="1"></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="%">
                                                </TitleAppearance>
                                            </YAxis>
                                        </PlotArea>
                                        <Appearance>
                                            <FillStyle BackgroundColor="Transparent"></FillStyle>
                                        </Appearance>
                                        <Legend>
                                            <Appearance Visible="false" BackgroundColor="Transparent" Position="Bottom">
                                            </Appearance>
                                        </Legend>
                                    </telerik:RadHtmlChart>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="progress progressWithoutMargin">
                            <div runat="server" id="barEstimatedTime" class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="100"
                                aria-valuemin="0" aria-valuemax="100" style="width:100%">
                                <div runat="server" id="barTheoreticalTime" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100"
                                    aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                </div>
                                <div runat="server" class="progress-bar progress-bar-warning" id="textEstimatedTime" style="width:0%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="progress progressWithoutMargin" style="text-align:right">
                            <div runat="server" id="barProducedTime" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax="100" style="text-align:center; float:left; width:0%;"></div>
                            <div runat="server" id="barLeftTime" class="progress-bar" role="progressbar" aria-valuenow="100"
                                    aria-valuemin="0" aria-valuemax="100" style="background-color:#f5f5f5 !important; text-align:center; width:100%; border:solid; float:left;"></div>
                        </div>
                    </div>
                </div>
                <div class="vefs-milestone-wrapper">
                <div class="milestone-container">
                    <div class="chart-container">
                        <div class="line-container">
                            <div class="line"></div>
                            <div class="line left" style="width: 67%;"></div>
                        </div>
                        <div class="dot-container">
                            <div class="milestones milestone__10">
                                <div class="dot completed colored"></div>
                            </div>
                            <div class="milestones milestone__27">
                                <div class="dot completed colored"></div>
                            </div>
                            <div class="milestones milestone__50">
                                <div class="dot completed colored"></div>
                            </div>
                            <div class="milestones milestone__80">
                                <div class="dot"></div>
                            </div>
                            <div class="milestones milestone__100">
                                <div class="dot"></div>
                            </div>
                            <div id="controlplanMilestoneDot" runat="server">
                                <div class="dot completed colored" style="background-color: #64aec5"></div>
                            </div>
                        </div>
                    </div>
                    <div class="label-container">
                        <div class="milestones milestone__10">
                            <div class="label colored">10%</div>
                        </div>
                        <div class="milestones milestone__27">
                            <div class="label colored">25%</div>
                        </div>
                        <div class="milestones milestone__50">
                            <div class="label colored">50%</div>
                        </div>
                        <div class="milestones milestone__80">
                            <div class="label">80%</div>
                        </div>
                        <div class="milestones milestone__100">
                            <div class="label">100%</div>
                        </div>
                        <div id="controlplanMilestoneLabel" runat="server">
                            <div class="label colored">Controllo Qualità</div>
                        </div>
                    </div>
                </div>
            </div>
                <br />
                <div class="row" style="margin-bottom:3px;">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
                        <telerik:RadButton ID="btnArticleImage" runat="server" Text="Disegno" CssClass="btnActivityRead" OnClick="btnArticleImage_Click" EnableViewState="false" />
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
                        <telerik:RadButton ID="RadButton2" runat="server" Text="Piani di setup" CssClass="btnActivityRead" EnableViewState="false" />
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
                        <telerik:RadButton ID="RadButton4" runat="server" Text="Part Program" CssClass="btnActivityRead" EnableViewState="false" />
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
                        <telerik:RadButton ID="RadButton3" runat="server" Text="Distinta base" CssClass="btnActivityRead" EnableViewState="false" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
                        <telerik:RadButton ID="btnFermi" runat="server" Text="Fermi" CssClass="btnActivityDisabled" BackColor="#dddddd" Enabled="false" OnClick="btnFermi_Click" EnableViewState="false" />
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
                        <telerik:RadButton ID="btnScarti" runat="server" Text="Scarti" CssClass="btnActivityDisabled" Enabled="false" OnClick="btnScarti_Click" EnableViewState="false" />
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
                        <telerik:RadButton ID="btnQualità" runat="server" Text="Piani controllo" CssClass="btnActivity" OnClick="btnQualità_Click" EnableViewState="false" />
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
                        <telerik:RadButton ID="RadButton5" runat="server" Text="Manutenzioni" CssClass="btnActivityDisabled" Enabled="false" EnableViewState="false" />
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1"></div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
                        <telerik:RadButton ID="btnExit" runat="server" BackColor="#d9534f" CssClass="btnActivity" OnClick="btnExit_Click" EnableViewState="false">
                            <ContentTemplate>
                                <center><i class="fa fa-window-close" aria-hidden="true" style="color:black"></i> Esci</center>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOperatorLogin" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAddOperator" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnConfirmSelectWorkshift" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancelSelectWorkshift" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnConfirmSelectMold" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancelSelectMold" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnConfirmCavityMoldNum" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancelCavityMoldNum" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnConfirmSelectOrder" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancelSelectOrder" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlLogin" Visible="false">
                <asp:HiddenField runat="server" ID="ModifyMachineTransaction" />
                <div class="row">
                    <div class="col-xs-7 col-sm-7 col-md-8 col-lg-8">
                        <telerik:RadTextBox runat="server" ID="txtBadge" AutoPostBack="true" OnTextChanged="txtBadge_TextChanged"
                            Label="Badge:" LabelWidth="89px" LabelCssClass="labelComboBox" ValidationGroup="OperatorValidation"
                            Width="290px" Height="34px" EmptyMessage="Inserisci" CssClass="textNoPaddingLeft">
                            <ClientEvents OnFocus="Clear" OnValueChanged="PreventPostback" />
                        </telerik:RadTextBox>
                        <br />
                        <telerik:RadTextBox runat="server" ID="txtRegistrationId" ReadOnly="true" ValidationGroup="OperatorValidation"
                            Label="Matricola:" LabelWidth="89px" Width="290px" Height="34px" />
                        <telerik:RadTextBox runat="server" ID="txtUserName" ReadOnly="true" ValidationGroup="OperatorValidation" Width="201px"
                            Label="Nome:" LabelWidth="51px" Height="34px" />
                        <asp:RequiredFieldValidator ID="OperatorNameRequiredFieldValidator" runat="server" ControlToValidate="txtUserName"
                            EnableClientScript="true" ValidationGroup="OperatorValidation" ForeColor="red" />
                        <br />
                        <telerik:RadComboBox runat="server" ID="ddlRole" RenderMode="Lightweight" ValidationGroup="OperatorValidation" Width="290px"
                            Label="Ruolo:" LabelCssClass="labelComboBox" Enabled="false" />
                        <asp:RequiredFieldValidator ID="RoleRequiredFieldValidator" runat="server" ControlToValidate="ddlRole"
                            EnableClientScript="true" ValidationGroup="OperatorValidation" ForeColor="red" InitialValue="" />
                        <br /><br />
                        <div class="row">
                            <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11 noPaddingRight">
                                <asp:HiddenField runat="server" ID="hiddenMachineState" />
                                <asp:Label Text="Stato:" runat="server" Font-Bold="true" Width="85px" style="line-height:2" EnableViewState="false" />
                                <telerik:RadButton runat="server" ID="btnSetUp" Text="Attrezzaggio" AutoPostBack="false" OnClientClicked="ChangeMachineState" />
                                <telerik:RadButton runat="server" ID="btnProduction" Text="Produzione" AutoPostBack="false" OnClientClicked="ChangeMachineState" />
                                <telerik:RadButton runat="server" ID="btnMaintenance" Text="Manutenzione" AutoPostBack="false" OnClientClicked="ChangeMachineState" />
                            </div>
                        </div>
                        <br /><br />
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-4 col-lg-4">
                        <asp:Panel Width="330px" runat="server" Visible="true" EnableViewState="false">
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="7" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="8" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="9" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="4" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="5" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="6" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="1" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="2" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="3" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="194px" Height="60px" runat="server" Text="0" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                        </asp:Panel>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:Label Text="Commessa:" runat="server" AssociatedControlID="txtCustomerOrder_Login" Width="85px" style="line-height:2" EnableViewState="false" />
                        <telerik:RadButton runat="server" ID="txtCustomerOrder_Login" Enabled="false" BackColor="White" ForeColor="Black" OnClick="txtCustomerOrder_Login_Click" Width="201px" /> 
                        <telerik:RadButton runat="server" ID="txtNStampateOra_Login" Enabled="false" BackColor="White" ForeColor="Black" ReadOnly="true" />
                        <telerik:RadButton runat="server" ID="btnSelectWorkshift_Login" Text="Turni precedenti" Visible="false" BackColor="#dddddd" Font-Bold="true" BorderColor="Black" OnClick="btnSelectWorkshift_Login_Click" />
                        <asp:HiddenField runat="server" ID="hiddenWorkshiftSelected" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:Label Text="Articolo:" runat="server" AssociatedControlID="txtArticleCode_Login" Width="85px" style="line-height:2" EnableViewState="false" />
                        <telerik:RadButton runat="server" ID="txtArticleCode_Login" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" />
                        <telerik:RadButton runat="server" ID="txtArticleDescription_Login" Width="304px" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:Label Text="Stampo:" runat="server" AssociatedControlID="txtMold_Login" Width="85px" style="line-height:2" EnableViewState="false" />
                        <telerik:RadButton runat="server" ID="txtMold_Login" Enabled="false" BackColor="White" ForeColor="Black" Width="201px" OnClick="txtMold_Login_Click" />
                        <telerik:RadButton runat="server" ID="btnCavityMold_Login" Enabled="false" BackColor="White" ForeColor="Black" OnClick="btnCavityMold_Login_Click" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:Label Text="MP:" runat="server" AssociatedControlID="txtMPCode_Login" Width="85px" style="line-height:2" EnableViewState="false" />
                        <telerik:RadButton runat="server" ID="txtMPCode_Login" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" EnableViewState="false" />
                        <telerik:RadButton runat="server" ID="txtMPDescription_Login" Width="304px" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" EnableViewState="false" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <telerik:RadTextBox runat="server" ID="txtBatchCode_Login" Width="290px" Label="Lotto:" LabelWidth="83px" Height="34px" EnableViewState="false" />
                        <%--<asp:Label Text="Lotto:" runat="server" AssociatedControlID="txtBatchCode" Width="85px" style="line-height:2" />
                        <telerik:RadButton runat="server" ID="txtBatchCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" />--%>
                    </div>
                </div>          
                <div class="row alDx">
                    <telerik:RadButton runat="server" ID="btnConfirmConnect" OnClick="btnConfirmConnect_Click" ValidationGroup="OperatorValidation">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton runat="server" ID="btnCancelConnect" CssClass="btnActivity" OnClick="btnCancelConnect_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOperatorLogout" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlLogout" Visible="false">
                <telerik:RadGrid runat="server" ID="gridOperator" RenderMode="Lightweight" AutoGenerateColumns="false" OnItemCommand="gridOperator_ItemCommand"
                    OnNeedDataSource="gridOperator_NeedDataSource" OnItemDataBound="gridOperator_ItemDataBound" OnSelectedIndexChanged="gridOperator_SelectedIndexChanged"
                    LocalizationPath="~/App_GlobalResources/">
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" UseClientSelectColumnOnly="false" />
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Id, OperatorId" Caption="Operatori" NoMasterRecordsText="Non ci sono operatori">
                        <Columns>
                            <telerik:GridBoundColumn DataField="OperatorId" HeaderText="Nome">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RoleId" HeaderText="Ruolo">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn DataField="DateStart" HeaderText="DataOra ingresso" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridDateTimeColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <br /><br />
                <asp:HiddenField runat="server" ID="hiddenQualityLevelValue" ClientIDMode="Static" />
                <asp:Panel ID="pnlAllowConfirmProduction" runat="server" GroupingText="Qualità" Visible="false">
                    <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1">
                        <div id="box-out">
			                <div id="box-inside">
                                <div runat="server" id="redLight" onclick="switchLight(0)" class="light stop"></div>
                                <div runat="server" id="yellowLight" onclick="switchLight(1)" class="light slow"></div>
                                <div runat="server" id="greenLight" onclick="switchLight(2)" class="light go"></div>
			                </div>
		                </div>
                    </div>
                    <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2">
                        <br />
                        <asp:Label Text="Stop" Font-Bold="true" runat="server" Font-Size="X-Large" />
                        <br /><br /><br />
                        <asp:Label Text="Warning" Font-Bold="true" runat="server" Font-Size="X-Large" />
                        <br /><br /><br />
                        <asp:Label Text="Ok" Font-Bold="true" runat="server" Font-Size="X-Large" />
                    </div>
                </asp:Panel>
                <br /><br />
                <div class="row alDx">
                    <telerik:RadButton runat="server" ID="btnConfirmLogout" OnClick="btnConfirmLogout_Click">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton runat="server" ID="btnCancelLogout" CssClass="btnActivity" OnClick="btnCancelLogout_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="btnConfirmLogout" EventName="Click" />--%>
            <asp:AsyncPostBackTrigger ControlID="btnFermi" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnConfirmScarti" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancelScarti" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlFermi" Visible="false">
                <div class="row">
                    <asp:HiddenField runat="server" ID="hiddenLogoutOperator" />
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <telerik:RadGrid runat="server" ID="gridFermi" RenderMode="Lightweight" AutoGenerateColumns="false" AllowMultiRowSelection="true"
                            OnItemDataBound="gridFermi_ItemDataBound">
                            <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
                            <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id">
                                <Columns>
                                    <telerik:GridClientSelectColumn></telerik:GridClientSelectColumn>
                                    <telerik:GridDateTimeColumn DataField="Start" HeaderText="<%$ Resources:Mes,Start %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridDateTimeColumn DataField="End" HeaderText="<%$ Resources:Mes,End %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridTemplateColumn DataField="Duration" HeaderText="Durata[m]">
                                        <ItemTemplate>
                                            <asp:Label runat="server" Text='<%# Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem,"Duration")) / 60, 0) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:Mes,Status %>">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="<%$ Resources:Mes,Operator %>">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridDropDownColumn DataField="CauseId" HeaderText="<%$ Resources:Mes,Cause %>" UniqueName="ddlCause"
                                        ListTextField="Code" ListValueField="Id" DataSourceID="edsCause" DropDownControlType="RadComboBox"
                                        ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                                    </telerik:GridDropDownColumn>
                                    <telerik:GridBoundColumn DataField="Job.Order.Article.Code" HeaderText="<%$ Resources:Mes,Article %>">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="true" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True" ScrollHeight="430px" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <telerik:RadTreeView RenderMode="Lightweight" runat="Server" ID="causeTreeView" OnNodeClick="causeTreeView_NodeClick" Height="350px">
                        </telerik:RadTreeView>
                        <br /><br />
                        <div style="text-align:right">
                            <telerik:RadButton ID="btnConfirmCausaleFermo" runat="server" OnClick="btnConfirmCausaleFermo_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnCancelCausaleFermo" runat="server" CssClass="btnActivity" OnClick="btnCancelCausaleFermo_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlFermiManuali" Visible="false">
                <div class="row">
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                        <telerik:RadGrid runat="server" ID="gridFermiManuali" RenderMode="Lightweight" AutoGenerateColumns="false">
                            <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridNumericColumn DataField="Duration" HeaderText="Durata [m]">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <br /><br />
                        <asp:Panel Width="330px" runat="server" Visible="true" EnableViewState="false">
                            <telerik:RadTextBox runat="server" ID="txtInsDuration" ReadOnly="true" RenderMode="Lightweight" Width="278px" Visible="true">
                                <ReadOnlyStyle BackColor="White" ForeColor="Black" />
                            </telerik:RadTextBox>
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="7" OnClientClick="ModifyDuration(this)" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="8" OnClientClick="ModifyDuration(this)" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="9" OnClientClick="ModifyDuration(this)" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="4" OnClientClick="ModifyDuration(this)" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="5" OnClientClick="ModifyDuration(this)" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="6" OnClientClick="ModifyDuration(this)" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="1" OnClientClick="ModifyDuration(this)" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="2" OnClientClick="ModifyDuration(this)" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="3" OnClientClick="ModifyDuration(this)" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="184px" Height="60px" runat="server" Text="0" OnClientClick="ModifyDuration(this)" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="," OnClientClick="ModifyDuration(this)" EnableViewState="false" />
                            <asp:Button Width="184px" Height="60px" ID="btnOKFermo" runat="server" Primary="false" Text="Ok" OnClick="btnOKFermo_Click" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyDuration(this)" EnableViewState="false" />
                        </asp:Panel>
                        <br /><br /><br />
                        <div class="row" style="text-align:center;">
                            <telerik:RadButton ID="btnConfirmFermiManuali" runat="server" CssClass="btnActivity" OnClick="btnConfirmFermiManuali_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnCancelFermiManuali" runat="server" CssClass="btnActivity" OnClick="btnCancelFermiManuali_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnScarti" EventName="Click" />
            <%--<asp:AsyncPostBackTrigger ControlID="btnConfirmLogout" EventName="Click" />--%>
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlScarti" Visible="false">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:HiddenField runat="server" ID="hiddenOpenFermi" />
                        <asp:HiddenField runat="server" ID="hiddenTransactionToWatch" />
                        <asp:HiddenField runat="server" ID="hiddenOperatorId" />
                        <asp:HiddenField runat="server" ID="hiddenCompileManualQty" />
                        <telerik:RadTextBox runat="server" ID="txtQtyProducedTot" ReadOnly="true" Label="Qta colpi totale:" LabelWidth="113px" Width="191px" style="text-align:right;" />
                        &nbsp; &nbsp; &nbsp;
                        <telerik:RadTextBox runat="server" ID="txtQtyShotProducedOperator" ReadOnly="true" Label="Qta colpi turno:" LabelWidth="111px" Width="189px" style="text-align:right;">
                            <ClientEvents OnFocus="Focus" />
                        </telerik:RadTextBox>
                        &nbsp; &nbsp; &nbsp;
                        <telerik:RadTextBox runat="server" ID="txtQtyProducedOperator" ReadOnly="true" Label="Qta pezzi turno:" LabelWidth="112px" Width="190px" style="text-align:right;" />
                        &nbsp; &nbsp; &nbsp;
                        <telerik:RadTextBox runat="server" ID="txtQtyWasteTransaction" ReadOnly="true" Label="Qta pezzi scarto:" LabelWidth="118px" Width="196px" style="text-align:right;" />
                        <br /><br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                        <asp:Panel runat="server" ID="pnlWasteTable">
                        <telerik:RadGrid runat="server" ID="gridScarti" RenderMode="Lightweight" AutoGenerateColumns="false">
                            <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId, Num" CommandItemDisplay="Top">
                                <CommandItemTemplate>
                                    <telerik:RadButton runat="server" ID="btnAddBlankShot" Text="Colpi a vuoto" Font-Bold="true" OnClick="btnAddBlankShot_Click">
                                        <Icon PrimaryIconCssClass="rbAdd" />
                                    </telerik:RadButton>
                                </CommandItemTemplate>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridNumericColumn DataField="Num" HeaderText="Dichiarazioni precedenti" DataFormatString="{0:n0}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="OperatorWaste" HeaderText="Operatore" DataFormatString="{0:n0}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="Quality" HeaderText="Qualità" DataFormatString="{0:n0}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                        </telerik:RadGrid>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlBlankShot" Visible="false">
                            <telerik:RadGrid runat="server" ID="gridBlankShot" RenderMode="Lightweight" AutoGenerateColumns="false">
                            <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId, Num">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridNumericColumn DataField="Num" HeaderText="Dichiarazioni precedenti" DataFormatString="{0:n0}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="OperatorWaste" HeaderText="Operatore" DataFormatString="{0:n0}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                        </telerik:RadGrid>
                            <%--<asp:TextBox runat="server" ID="txtBlankShotWithMP" />--%>
                            <br />
                            <telerik:RadButton runat="server" ID="btnBackToWaste" Text="Mostra scarti" Font-Bold="true" OnClick="btnBackToWaste_Click">
                                <Icon PrimaryIconCssClass="rbPrevious" />
                            </telerik:RadButton>
                        </asp:Panel>
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <br /><br />
                        <asp:Panel Width="330px" ID="pnlTastieraNum" runat="server" Visible="true">
                            <telerik:RadTextBox runat="server" ID="txtInsValue" ReadOnly="true" RenderMode="Lightweight" Width="278px" Visible="true">
                                <ReadOnlyStyle BackColor="White" ForeColor="Black" />
                            </telerik:RadTextBox>
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn7" runat="server" Text="7" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn8" runat="server" Text="8" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn9" runat="server" Text="9" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn4" runat="server" Text="4" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn5" runat="server" Text="5" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn6" runat="server" Text="6" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn1" runat="server" Text="1" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn2" runat="server" Text="2" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn3" runat="server" Text="3" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="184px" Height="60px" ID="btn0" runat="server" Text="0" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btnVir" runat="server" Text="," OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button Width="184px" Height="60px" ID="btnNumPadOk" runat="server" Primary="false" Text="Ok" OnClick="btnNumPadOk_Click" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btnCanc" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="278px" Height="60px" ID="btnRecuperoScarti" runat="server" Primary="false" Text="Recupero" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                        </asp:Panel>
                        <br /><br /><br />
                        <div class="row" style="text-align:center;">
                            <telerik:RadButton ID="btnConfirmScarti" runat="server" CssClass="btnActivity" OnClick="btnConfirmScarti_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnCancelScarti" runat="server" CssClass="btnActivity" OnClick="btnCancelScarti_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnRefreshChart" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlOEE" Visible="false">
                <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="jobGrid" AutoGenerateColumns="false" GridLines="none" Skin="Silk" 
                    OnNeedDataSource="jobGrid_NeedDataSource" OnItemDataBound="jobGrid_ItemDataBound"
                    LocalizationPath="~/App_GlobalResources/" Culture="en-US" EnableViewState="false">
                    <MasterTableView FilterExpression="" Caption="">
                        <ColumnGroups>
                            <telerik:GridColumnGroup Name="Tempo" HeaderText="Tempo">
                                <HeaderStyle HorizontalAlign="Center" />
                            </telerik:GridColumnGroup>
                        </ColumnGroups>
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldName="GroupType"></telerik:GridGroupByField>
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldName="GroupType" SortOrder="None"></telerik:GridGroupByField>
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Label" HeaderText="">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Pezzi" HeaderText="Pezzi" DataFormatString="{0:f0}">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Colpi" HeaderText="Cicli" DataFormatString="{0:f0}">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Teorico" HeaderText="Cicli / h">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Eseguito" HeaderText="Tempo<br/>Effettivo">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Tempo" HeaderText="Tempo<br/>Totale">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Efficienza" HeaderText="Indice %">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                </telerik:RadGrid>
                <br />
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="Disponibilità" runat="server" EnableViewState="false" />
                            <telerik:RadRadialGauge runat="server" ID="meterAvailability" EnableViewState="false">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtAvailability" style="text-align:center" EnableViewState="false" />
                        </center>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="Efficienza" runat="server" EnableViewState="false" />
                            <telerik:RadRadialGauge runat="server" ID="meterEfficiency" EnableViewState="false">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtEfficiency" style="text-align:center" EnableViewState="false" />
                        </center>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="Qualità" runat="server" EnableViewState="false" />
                            <telerik:RadRadialGauge runat="server" ID="meterQuality" EnableViewState="false">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtQuality" style="text-align:center" EnableViewState="false" />
                        </center>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="OEE" runat="server" EnableViewState="false" />
                            <telerik:RadRadialGauge runat="server" ID="meterOEE" EnableViewState="false">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtOEE" style="text-align:center" EnableViewState="false" />
                        </center>
                    </div>
                </div>
                <br />
                <div class="row alDx">
                    <telerik:RadButton ID="btnCloseOEE" runat="server" CssClass="btnActivity" OnClick="btnCloseOEE_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> Chiudi </center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="txtCustomerOrder_Login" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlOrder" Visible="false">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <telerik:RadButton ID="btnSearchOrder" runat="server" CssClass="btnActivity" OnClick="btnSearchOrder_Click" style="float:left;" EnableViewState="false">
                            <ContentTemplate>
                                <center><i class="fa fa-search" aria-hidden="true"></i> Altre commesse</center>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadTextBox ID="txtSearchOrder" runat="server" EmptyMessage="Inserisci codice" Visible="false" AutoPostBack="true" OnTextChanged="txtSearchOrder_TextChanged" />
                        <asp:Panel ID="pnlTastieraCommessa" Width="300px" runat="server" Visible="false" style="float:left;">
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="7" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="8" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="9" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="4" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="5" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="6" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="1" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="2" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="3" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="174px" Height="50px" runat="server" Text="0" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                        </asp:Panel>
                        <div style="text-align:right; display:inline-block; float:right;">
                            <telerik:RadButton ID="btnConfirmSelectOrder" runat="server" CssClass="btnActivity" OnClick="btnConfirmSelectOrder_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnCancelSelectOrder" runat="server" CssClass="btnActivity" OnClick="btnCancelSelectOrder_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                    </div>
                </div>
                <br />
                <telerik:RadGrid runat="server" ID="gridSelectOrder" RenderMode="Lightweight" AutoGenerateColumns="false"
                    OnNeedDataSource="gridSelectOrder_NeedDataSource">
                    <MasterTableView DataKeyNames="Id" Caption="" FilterExpression="" NoMasterRecordsText="Non ci sono commesse per questa macchina">
                        <Columns>
                            <telerik:GridBoundColumn DataField="Code" HeaderText="Fase">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Order.CustomerOrder.OrderCode" HeaderText="<%$ Resources:Mes,Order %>">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Order.Article.Code" HeaderText="<%$ Resources:Mes,Article %>">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn DataField="StartDate" HeaderText="Inizio previsto" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="Fine prevista" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridDateTimeColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnCavityMold_Login" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlCavityMold" Visible="false">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <telerik:RadTextBox ID="txtCavityMoldNum" runat="server" EmptyMessage="Inserisci n impronte" />
                        <asp:Panel Width="300px" runat="server" style="float:left;">
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="7" OnClientClick="ModifyNumCavity(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="8" OnClientClick="ModifyNumCavity(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="9" OnClientClick="ModifyNumCavity(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="4" OnClientClick="ModifyNumCavity(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="5" OnClientClick="ModifyNumCavity(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="6" OnClientClick="ModifyNumCavity(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="1" OnClientClick="ModifyNumCavity(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="2" OnClientClick="ModifyNumCavity(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="3" OnClientClick="ModifyNumCavity(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="174px" Height="50px" runat="server" Text="0" OnClientClick="ModifyNumCavity(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyNumCavity(this);return false;" EnableViewState="false" />
                        </asp:Panel>
                        <div style="text-align:right; display:inline-block; float:right;">
                            <telerik:RadButton ID="btnConfirmCavityMoldNum" runat="server" CssClass="btnActivity" OnClick="btnConfirmCavityMoldNum_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnCancelCavityMoldNum" runat="server" CssClass="btnActivity" OnClick="btnCancelCavityMoldNum_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="txtMold_Login" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlMold" Visible="false">
                <telerik:RadGrid runat="server" ID="gridSelectMold" RenderMode="Lightweight" AutoGenerateColumns="false"
                    OnNeedDataSource="gridSelectMold_NeedDataSource">
                    <MasterTableView DataKeyNames="MoldId" Caption="" FilterExpression="">
                        <Columns>
                            <telerik:GridDropDownColumn DataField="MoldId" HeaderText="Stampo"
                                DataSourceID="edsMold" ListTextField="Code" ListValueField="Id">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn DataField="Article.Code" HeaderText="Codice Articolo">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Article.Description" HeaderText="Descrizione Articolo">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <br /><br />
                <div class="row alDx">
                    <telerik:RadButton ID="btnConfirmSelectMold" runat="server" CssClass="btnActivity" OnClick="btnConfirmSelectMold_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnCancelSelectMold" runat="server" CssClass="btnActivity" OnClick="btnCancelSelectMold_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSelectWorkshift_Login" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlSelectWorkshift" Visible="false">
                <telerik:RadGrid runat="server" ID="gridWorkshift" RenderMode="Lightweight" AutoGenerateColumns="false"
                    OnNeedDataSource="gridWorkshift_NeedDataSource">
                    <MasterTableView DataKeyNames="Id" Caption="" FilterExpression="" NoMasterRecordsText="Non ci sono turni precedenti">
                        <Columns>
                            <telerik:GridBoundColumn DataField="Job.Order.CustomerOrder.OrderCode" HeaderText="<%$ Resources:Mes,Order %>">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="Operatore">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn DataField="Start" HeaderText="Inizio" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy HH:mm}">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn DataField="End" HeaderText="Fine" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy HH:mm}">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn DataField="QtyProduced" HeaderText="Pezzi prodotti" DataFormatString="{0:f0}">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="QtyProductionWaste" HeaderText="Quantità scarti" DataFormatString="{0:f0}">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <br /><br />
                <div class="row alDx">
                    <telerik:RadButton ID="btnConfirmSelectWorkshift" runat="server" CssClass="btnActivity" OnClick="btnConfirmSelectWorkshift_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnCancelSelectWorkshift" runat="server" CssClass="btnActivity" OnClick="btnCancelSelectWorkshift_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

<%--    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlControlPlan" Visible="false">
                <operator:ControlPlanExecution ID="controlPlan" runat="server">
                </operator:ControlPlanExecution>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>--%>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnArticleImage" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlArticle" Visible="false">
                <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <asp:Label ID="lblArticleImageCode" runat="server" Font-Bold="true" EnableViewState="false" />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <asp:Image ID="imgArticle" runat="server" EnableViewState="false" />
                </div>
            </div>
            <div class="row alDx">
                <telerik:RadButton ID="btnCloseArticleImage" runat="server" CssClass="btnActivity" OnClick="btnCloseArticleImage_Click" EnableViewState="false">
                    <ContentTemplate>
                        <center><i class="fa fa-times" aria-hidden="true"></i> Chiudi </center>
                    </ContentTemplate>
                </telerik:RadButton>
            </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function RefreshChart() {
            document.getElementById('<%= btnRefreshChart.ClientID %>').click();
        }

        function ModifyRegistrationId(sender) {
            var textBox = $find('<%= txtBadge.ClientID %>');
            var txtValue = textBox.get_value();
            var char = sender.value;//sender.get_text();
            if (char == "Canc") {
                if (txtValue.length > 0) {
                    textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                }
            }
            else {
                if (char == ',' || char == '.') {
                    if (txtValue.includes(',') || txtValue.includes('.')) { }
                    else {
                        if (txtValue.length == 0) {
                            textBox.set_value('0' + char);
                        }
                        else {
                            textBox.set_value(txtValue + char);
                        }
                    }
                }
                else {
                    textBox.set_value(txtValue + char);
                }
            }
        }

        function Clear(sender, eventArgs) {
            sender.set_value("");
        }

        function ModifyDuration(sender) {
            var textBox = $find("<%= txtInsDuration.ClientID %>");
            var txtValue = textBox.get_value();
            var char = sender.value;//sender.get_text();
            if (char == "Canc") {
                if (txtValue.length > 0) {
                    textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                }
            }
            else {
                if (char == ',' || char == '.') {
                    if (txtValue.includes(',') || txtValue.includes('.')) { }
                    else {
                        if (txtValue.length == 0) {
                            textBox.set_value('0' + char);
                        }
                        else {
                            textBox.set_value(txtValue + char);
                        }
                    }
                }
                else {
                    textBox.set_value(txtValue + char);
                }
            }
        }

        function ModifyNumber(sender) {
            var btnOK = document.getElementById('<%= btnNumPadOk.ClientID %>');
            var textBox = $find("<%= txtInsValue.ClientID %>");
            var txtValue = textBox.get_value();
            var char = sender.value;//sender.get_text();
            if (char == "Recupero") {
                if (txtValue.includes('-')) { }
                else {
                    textBox.set_value('-' + txtValue);
                }
            }
            else {
                if (char == "Canc") {
                    if (txtValue.length > 0) {
                        textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                    }
                }
                else {
                    if (char == ',' || char == '.') {
                        if (txtValue.includes(',') || txtValue.includes('.')) { }
                        else {
                            if (txtValue.length == 0) {
                                textBox.set_value('0' + char);
                            }
                            else {
                                textBox.set_value(txtValue + char);
                            }
                        }
                    }
                    else {
                        textBox.set_value(txtValue + char);
                    }
                }
            }
            btnOK.classList.add("btnOkToClick");
            //btnOK.className += " btnOkToClick";
            //return false;
        }

        function ModifyOrderCode(sender) {
            var textBox = $find('<%= txtSearchOrder.ClientID %>');
            var txtValue = textBox.get_value();
            var char = sender.value;//sender.get_text();
            if (char == "Canc") {
                if (txtValue.length > 0) {
                    textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                }
            }
            else {
                if (char == ',' || char == '.') {
                    if (txtValue.includes(',') || txtValue.includes('.')) { }
                    else {
                        if (txtValue.length == 0) {
                            textBox.set_value('0' + char);
                        }
                        else {
                            textBox.set_value(txtValue + char);
                        }
                    }
                }
                else {
                    textBox.set_value(txtValue + char);
                }
            }
        }

        function ModifyNumCavity(sender) {
            var textBox = $find('<%= txtCavityMoldNum.ClientID %>');
            var txtValue = textBox.get_value();
            var char = sender.value;//sender.get_text();
            if (char == "Canc") {
                if (txtValue.length > 0) {
                    textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                }
            }
            else {
                if (char == ',' || char == '.') {
                    if (txtValue.includes(',') || txtValue.includes('.')) { }
                    else {
                        if (txtValue.length == 0) {
                            textBox.set_value('0' + char);
                        }
                        else {
                            textBox.set_value(txtValue + char);
                        }
                    }
                }
                else {
                    textBox.set_value(txtValue + char);
                }
            }
        }

        function Focus(sender, eventArgs) {
            document.getElementById("<%= hiddenCompileManualQty.ClientID %>").value = "True";
        }

        function PreventPostback(sender, eventArgs) {
            if (eventArgs.get_newValue().length < 3)
                eventArgs.set_cancel(true);
        }

        function ChangeMachineState(sender, args) {
            var text = sender.get_text();
            document.getElementById('<%=hiddenMachineState.ClientID%>').value = text;
            switch (text) {
                case "Attrezzaggio":
                    sender.addCssClass("setUpColor");
                    $find("<%= btnProduction.ClientID %>").removeCssClass("productionColor");
                    $find("<%= btnMaintenance.ClientID %>").removeCssClass("maintenanceColor");
                    break;
                case "Manutenzione":
                    sender.addCssClass("maintenanceColor");
                    $find("<%= btnSetUp.ClientID %>").removeCssClass("setUpColor");
                    $find("<%= btnProduction.ClientID %>").removeCssClass("productionColor");
                    break;
                case "Produzione":
                    sender.addCssClass("productionColor");
                    $find("<%= btnSetUp.ClientID %>").removeCssClass("setUpColor");
                    $find("<%= btnMaintenance.ClientID %>").removeCssClass("maintenanceColor");
                    break;
            }
            return false;
        }
    </script>

    <ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
        OrderBy="it.Description" AutoGenerateWhereClause="true" />
    <ef:EntityDataSource ID="edsCause" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Causes"
        OrderBy="it.Description" AutoGenerateWhereClause="true" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>