﻿<%@ Page Language="C#" MasterPageFile="~/DashboardOperator/MainOperatorAccess.Master" AutoEventWireup="true" CodeBehind="OperatorAccess.aspx.cs" Inherits="MainCloudFramework.DashboardOperator.OperatorAccess" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="Style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="Script.js"></script>
    <%--<link rel="stylesheet" href="jQuery.NumPad-master/jquery.numpad.css">
    <script type="text/javascript" src="jQuery.NumPad-master/jquery.numpad.js"></script>--%>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
        <p class="text-danger">
            <asp:Literal runat="server" ID="FailureText" />
        </p>
    </asp:PlaceHolder>

    <div style="float:left;"><b>Legenda macchina:</b>&nbsp; &nbsp; &nbsp;</div>
    <div style="float:left;"><div style="background-color:yellow; width:20px; height:20px; float:left;"></div>&nbsp;<b>Attrezzaggio</b>&nbsp; &nbsp; &nbsp;</div>
    <div style="float:left;"><div style="background-color:lightgreen; width:20px; height:20px; float:left;"></div>&nbsp;<b>Produzione</b>&nbsp; &nbsp; &nbsp;</div>
    <div style="float:left;"><div style="background-color:lightblue; width:20px; height:20px; float:left;"></div>&nbsp;<b>Manutenzione</b>&nbsp; &nbsp; &nbsp;</div>
    <div style="float:left;"><div style="background-color:#f75555; width:20px; height:20px; float:left;"></div>&nbsp;<b>Ferma</b>&nbsp; &nbsp; &nbsp;</div>
    <div style="float:left;"><div style="border:1px solid; width:20px; height:20px; float:left;"></div>&nbsp;<b>Spenta</b></div>
    <br /><br />

<asp:UpdatePanel runat="server" UpdateMode="Always">
    <ContentTemplate>
        <div class="row" style="width:100%;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <telerik:RadGrid runat="server" ID="machineTable" RenderMode="Lightweight" AutoGenerateColumns="false" CssClass="tableNoCollapse"
                    OnNeedDataSource="machineTable_NeedDataSource" OnItemDataBound="machineTable_ItemDataBound">
                    <MasterTableView FilterExpression="" Caption="" DataKeyNames="AssetId" ShowHeadersWhenNoRecords="false" NoMasterRecordsText="Non ci sono macchine per la zona selezionata">
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldAlias="Operator" FieldName="Operator"></telerik:GridGroupByField>
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldName="Operator" HeaderText="ok" SortOrder="Descending"></telerik:GridGroupByField>
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                        <Columns>
                            <telerik:GridButtonColumn DataTextField="Asset" HeaderText="Macchina" UniqueName="Asset"
                                ButtonType="PushButton" ButtonCssClass="hyperLinkText">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" CssClass="hyperLinkText" Font-Bold="true" Font-Size="Medium" />
                            </telerik:GridButtonColumn>
                             <telerik:GridBoundColumn DataField="Status" HeaderText="Stato">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <%--<telerik:GridHyperLinkColumn DataTextField="Asset" HeaderText="Macchina">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" CssClass="hyperLinkText" />
                            </telerik:GridHyperLinkColumn>--%>
                            <telerik:GridBoundColumn DataField="Order" HeaderText="<%$ Resources:Mes,Order %>">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Article" HeaderText="<%$ Resources:Mes,Article %>">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Quality" HeaderText="Q" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn DataField="Quality" HeaderText="Q" UniqueName="QualityLevel">
                                <ItemTemplate>
                                    <div runat="server" class="tableLight"></div>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="False"></Selecting>
                        <%--<Scrolling AllowScroll="true" UseStaticHeaders="true" />--%>
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

    <ef:EntityDataSource ID="edsCause" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Causes"
        OrderBy="it.Description" AutoGenerateWhereClause="true" />
    <ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
        OrderBy="it.Description" AutoGenerateWhereClause="true" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>