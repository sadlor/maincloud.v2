﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web.Multitenants;
using MES.Core;
using MES.Helper;
using MES.Models;
using MES.Repositories;
using MES.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloudFramework.DashboardOperator
{
    public partial class MachineDetail : BasePage
    {
        protected string AssetId
        {
            get
            {
                return Request.QueryString["asset"];
            }
        }

        [Serializable]
        protected class FermiRecord
        {
            public int TransactionId { get; set; }
            public string CauseId { get; set; }

            public FermiRecord(int tId, string causeId)
            {
                TransactionId = tId;
                CauseId = causeId;
            }
        }

        [Serializable]
        protected class ScartiRecord
        {
            public string CauseId { get; set; }
            public string Description { get; set; } // code - description
            public decimal Num { get; set; } //num scarti

            public ScartiRecord(string id, string description)
            {
                CauseId = id;
                Description = description;
            }

            public ScartiRecord(string id, string description, decimal num)
            {
                CauseId = id;
                Description = description;
                Num = num;
            }
        }

        const string JOB_PRODUCTIONWASTE_TO_MODIFY = "JobProductionWasteListToModify";
        protected List<ScartiRecord> JobWasteListToModify
        {
            get
            {
                if (this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] == null)
                {
                    this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] = new List<ScartiRecord>();
                }
                return (List<ScartiRecord>)(this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY]);
            }
            set
            {
                this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] = value;
            }
        }

        const string TRANSACTION_LIST_TO_MODIFY = "TransactionListToModify";
        protected List<FermiRecord> TransactionListToModify
        {
            get
            {
                if (this.ViewState[TRANSACTION_LIST_TO_MODIFY] == null)
                {
                    this.ViewState[TRANSACTION_LIST_TO_MODIFY] = new List<FermiRecord>();
                }
                return (List<FermiRecord>)(this.ViewState[TRANSACTION_LIST_TO_MODIFY]);
            }
            set
            {
                this.ViewState[TRANSACTION_LIST_TO_MODIFY] = value;
            }
        }

        private GrantOperator grantService = new GrantOperator();
        TransactionService transService = new TransactionService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(AssetId))
            {
                if (Session.SessionMultitenant().ContainsKey(MesConstants.DEPARTMENT_ZONE) && !string.IsNullOrEmpty(Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString()))
                {
                    string zoneId = Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString();
                    ZoneRepository zoneRep = new ZoneRepository();
                    AssetRepository assetRep = new AssetRepository();
                    OperatorRepository operRep = new OperatorRepository();
                    TransactionService TService = new TransactionService();
                    Page.Title = "Realtime - " + zoneRep.FindByID(zoneId).Description + " - " + assetRep.FindByID(AssetId).Description + " - Operatore: " + operRep.FindByID(TService.GetLastTransactionOpen(AssetId).OperatorId).UserName;
                }
                LoadTestata();
                LoadGrantOperator();
            }

            edsJob.WhereParameters.Clear();
            edsJob.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
            edsJob.WhereParameters.Add("AssetId", AssetId);
            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
            edsArticle.WhereParameters.Clear();
            edsArticle.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
            edsOrder.WhereParameters.Clear();
            edsOrder.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
            edsCause.WhereParameters.Clear();
            edsCause.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
            edsOperator.WhereParameters.Clear();
            edsOperator.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
            edsMP.WhereParameters.Clear();
            edsMP.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
        }

        private void LoadTestata()
        {
            TransactionService TService = new TransactionService();
            Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);

            JobRepository jobRep = new JobRepository();
            Job jobCurrent = jobRep.FindByID(lastTrans.JobId);

            txtCustomerOrder.Text = jobCurrent?.Order?.CustomerOrder?.OrderCode;
            txtArticleCode.Text = jobCurrent?.Order?.Article?.Code;
            txtArticleDescription.Text = jobCurrent?.Order?.Article?.Description;
            MoldRepository moldRep = new MoldRepository();
            Mold mold = moldRep.FindByID(lastTrans.MoldId);
            txtMold.Text = mold?.Code;
            txtNImpronte.Text = "N° impronte = " + mold?.CavityMoldNum.ToString();

            //txtCodeOrder.Text = jobCurrent?.Order?.OrderCode;
            ////txtDescriptionJob.Text = jobCurrent?.Description;
            //txtCodeCustomerOrder.Text = jobCurrent?.Order?.CustomerOrder?.OrderCode;
            //txtCodeArticle.Text = jobCurrent?.Order?.Article?.Code;
            //txtDescriptionArticle.Text = jobCurrent?.Order?.Article?.Description;

            //MoldRepository moldRep = new MoldRepository();
            //Mold mold = moldRep.FindByID(lastTrans.MoldId);
            //txtCodeMold.Text = mold?.Code;
            ////txtDescriptionMold.Text = mold?.Description;
            //txtNImpronte.Text = "N° impronte = " + mold?.CavityMoldNum.ToString();
        }

        private void LoadGrantOperator()
        {
            TransactionService TService = new TransactionService();
            Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);

            btnAttrezzaggio.Enabled = grantService.AllowSetup(lastTrans.OperatorId);
            SetCssClass(btnAttrezzaggio);

            btnManutenzione.Enabled = grantService.AllowMaintenance(lastTrans.OperatorId);
            SetCssClass(btnManutenzione);

            btnProduzione.Enabled = grantService.AllowProduction(lastTrans.OperatorId);
            SetCssClass(btnProduzione);

            btnQualità.Enabled = grantService.AllowQuality(lastTrans.OperatorId);
            SetCssClass(btnQualità);

            btnScarti.Enabled = grantService.AllowWaste(lastTrans.OperatorId);
            SetCssClass(btnScarti);

            btnFermi.Enabled = grantService.AllowProduction(lastTrans.OperatorId);
            SetCssClass(btnFermi);

            txtCustomerOrder.Enabled = grantService.AllowSelectJob(lastTrans.OperatorId);

            //btnOEE.Enabled = grantService.AllowOEE(lastTrans.OperatorId);
        }

        private void SetCssClass(RadButton button)
        {
            if (button.Enabled)
            {
                button.CssClass = "btnActivity";
            }
            else
            {
                button.CssClass = "btnActivityDisabled";
            }
        }

        public bool EnableExitButton()
        {
            TransactionService TService = new TransactionService();
            Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);
            CauseService cService = new CauseService();
            
            if (!string.IsNullOrEmpty(lastTrans.CauseId) &&
                (cService.GetListCauseIdByGroupName("Attrezzaggio", OperatorHelper.ApplicationId).Contains(lastTrans.CauseId) ||
                cService.GetListCauseIdByGroupName("Manutenzione", OperatorHelper.ApplicationId).Contains(lastTrans.CauseId)))
            {
                return false;
            }
            return true;
        }

        private class JobRecord
        {
            public string Label { get; set; }
            public decimal? Pezzi { get; set; }
            public decimal? Colpi { get; set; }
            public decimal? Teorico { get; set; }
            public decimal? Eseguito { get; set; }
            public string Tempo { get; set; }
            public string Efficienza { get; set; }
            public string GroupType { get; set; }

            public JobRecord(string label, decimal? pezzi, decimal? colpi, decimal? teorico, decimal? eseguito, TimeSpan? tempo, string efficienza, string groupType)
            {
                Label = label;
                Pezzi = pezzi;
                Colpi = colpi;
                Teorico = teorico;
                Eseguito = eseguito;
                if (tempo != null)
                {
                    Tempo = string.Format("{0}:{1}:{2}", (int)tempo?.TotalHours, tempo?.Minutes, tempo?.Seconds);
                }
                Efficienza = efficienza;
                GroupType = groupType;
            }
        }

        protected void jobGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (dlgOEE.IsOpen())
            {
                if (!string.IsNullOrEmpty(AssetId))
                {
                    List<JobRecord> dataSource = new List<JobRecord>();
                    //    //6 righe fisse per il datasource
                    //    //1 -> new JobTable("Da produrre colpi", 0, 0, 0, "100%", "Last");

                    TransactionService TService = new TransactionService();
                    Transaction lastTrans = TService.GetLastTransactionOpen(AssetId); //passare assetId dall'area rilievi
                    if (!string.IsNullOrEmpty(lastTrans.JobId))
                    {
                        //JobRepository jobRep = new JobRepository();
                        Job job = lastTrans.Job; //jobRep.FindByID(lastTrans.JobId);
                        int nImpronte = 1;
                        if (!string.IsNullOrEmpty(lastTrans.MoldId))
                        {
                            MoldRepository mRep = new MoldRepository();
                            nImpronte = mRep.FindByID(lastTrans.MoldId).CavityMoldNum;
                        }
                        if (nImpronte == 0)
                        {
                            nImpronte = 1;
                        }

                        CauseService causeService = new CauseService();
                        string causeId = causeService.GetCauseByCode("200", OperatorHelper.ApplicationId).Id;

                        TransactionRepository TRep = new TransactionRepository();
                        Transaction endLastWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId != lastTrans.OperatorId && !x.Open).OrderByDescending(x => x.Start).FirstOrDefault();

                        if (job.StandardRate == 0)
                        {
                            job.StandardRate = 1;
                        }

                        //generalità job
                        dataSource.Add(new JobRecord("Quantità da produrre", job.QtyOrdered, job.QtyOrdered / nImpronte, job.StandardRate, null, null, "", "Efficienza"));

                        //turni precedenti
                        List<Transaction> lastWorkshift;
                        if (endLastWorkshift == null)
                        {
                            lastWorkshift = new List<Transaction>();
                        }
                        else
                        {
                            lastWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && !x.Open && x.Start <= endLastWorkshift.Start).ToList();
                            lastWorkshift.Add(endLastWorkshift);
                        }
                        decimal lastWorkshiftEffettivo = 0;
                        if (lastWorkshift.Count > 0 && lastWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration) > 0)
                        {
                            lastWorkshiftEffettivo = Math.Round(lastWorkshift.Sum(x => x.PartialCounting) / Convert.ToDecimal(TimeSpan.FromSeconds((double)lastWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)).TotalHours), 2);
                        }
                        if (job.StandardRate > 0)
                        {
                            dataSource.Add(
                                new JobRecord(
                                    "Eseguito turni precedenti",
                                    lastWorkshift.Sum(x => x.PartialCounting) * nImpronte,
                                    lastWorkshift.Sum(x => x.PartialCounting),
                                    null,
                                    lastWorkshiftEffettivo,
                                    TimeSpan.FromSeconds((double)lastWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)),
                                    Math.Round(lastWorkshiftEffettivo / job.StandardRate * 100, 2) + "%",
                                    "Efficienza"));
                        }
                        else
                        {
                            dataSource.Add(
                                new JobRecord(
                                    "Eseguito turni precedenti",
                                    lastWorkshift.Sum(x => x.PartialCounting) * nImpronte,
                                    lastWorkshift.Sum(x => x.PartialCounting),
                                    null,
                                    lastWorkshiftEffettivo,
                                    TimeSpan.FromSeconds((double)lastWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)),
                                    "0%",
                                    "Efficienza"));
                        }

                        //turno corrente
                        List<Transaction> currentWorkshift = new List<Transaction>();
                        if (TService.LastTransactionOpenIsInProduction(AssetId))
                        {
                            if (endLastWorkshift == null)
                            {
                                //non c'è turno precedente
                                currentWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId == lastTrans.OperatorId).ToList();
                            }
                            else
                            {
                                currentWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId == lastTrans.OperatorId && x.Start > endLastWorkshift.Start).ToList();
                            }
                        }
                        decimal currentEffettivo = 0;
                        if (currentWorkshift.Sum(x => x.Duration) > 0)
                        {
                            currentEffettivo = Math.Round(currentWorkshift.Sum(x => x.PartialCounting) / Convert.ToDecimal(TimeSpan.FromSeconds((double)currentWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)).TotalHours), 2);
                        }
                        if (job.StandardRate > 0)
                        {
                            dataSource.Add(
                                    new JobRecord(
                                        "Eseguito turno corrente",
                                        currentWorkshift.Sum(x => x.PartialCounting) * nImpronte,
                                        currentWorkshift.Sum(x => x.PartialCounting),
                                        null,
                                        currentEffettivo,
                                        TimeSpan.FromSeconds((double)currentWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)),
                                        Math.Round(currentEffettivo / job.StandardRate * 100, 2) + "%",
                                        "Efficienza"));
                        }
                        else
                        {
                            dataSource.Add(
                                    new JobRecord(
                                        "Eseguito turno corrente",
                                        currentWorkshift.Sum(x => x.PartialCounting) * nImpronte,
                                        currentWorkshift.Sum(x => x.PartialCounting),
                                        null,
                                        currentEffettivo,
                                        TimeSpan.FromSeconds((double)currentWorkshift.Where(x => x.CauseId == causeId).Sum(x => x.Duration)),
                                        "0%",
                                        "Efficienza"));
                        }

                        decimal totProduzione = 0;
                        if (lastWorkshift.Count > 0)
                        {
                            totProduzione = lastWorkshift.Sum(x => x.PartialCounting);
                        }
                        if (currentWorkshift.Count > 0)
                        {
                            totProduzione += currentWorkshift.Sum(x => x.PartialCounting);
                        }

                        //scarti
                        JobProductionWasteService jpws = new JobProductionWasteService();
                        List<JobProductionWaste> scarti = jpws.GetWasteListByJobId(job.Id, OperatorHelper.ApplicationId);
                        dataSource.Add(new JobRecord("Scarti", scarti.Sum(x => x.QtyProductionWaste), scarti.Sum(x => x.QtyProductionWaste) / nImpronte, null, null, null, "", "Efficienza"));

                        //residuo
                        decimal pezziResidui = job.QtyOrdered - (totProduzione * nImpronte) + scarti.Sum(x => x.QtyProductionWaste);
                        if (job.StandardRate > 0)
                        {
                            dataSource.Add(
                                new JobRecord(
                                    "Residuo da produrre (eccedenza)",
                                    pezziResidui,
                                    pezziResidui / nImpronte,
                                    null,
                                    null,
                                    TimeSpan.FromHours((double)((pezziResidui / nImpronte) / job.StandardRate)),
                                    "",
                                    "Efficienza"));
                        }
                        else
                        {
                            dataSource.Add(
                               new JobRecord(
                                   "Residuo da produrre (eccedenza)",
                                   pezziResidui,
                                   pezziResidui / nImpronte,
                                   null,
                                   null,
                                   TimeSpan.FromHours((double)(0)),
                                   "",
                                   "Efficienza"));
                        }

                        //Resa
                        List<Transaction> currentJobTrans = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id).ToList();
                        dataSource.Add(
                            new JobRecord(
                                "Tempo totale disponibile", null, null, null, null, TimeSpan.FromSeconds((double)currentJobTrans.Sum(x => x.Duration)), "", "Resa"));
                        dataSource.Add(
                            new JobRecord(
                                "Tempo totale produzione",
                                null,
                                null,
                                null,
                                null,
                                TimeSpan.FromSeconds((double)currentJobTrans.Where(x => x.CauseId == causeId).Sum(x => x.Duration)),
                                Math.Round(TimeSpan.FromSeconds((double)currentJobTrans.Where(x => x.CauseId == causeId).Sum(x => x.Duration)).TotalSeconds /
                                TimeSpan.FromSeconds((double)currentJobTrans.Sum(x => x.Duration)).TotalSeconds * 100, 2) + "%",
                                "Resa"));

                        //Qualità
                        if (totProduzione > 0)
                        {
                            dataSource.Add(
                                new JobRecord(
                                    "Totale pezzi prodotti",
                                    totProduzione * nImpronte,
                                    null,
                                    null,
                                    null,
                                    null,
                                    Math.Round((((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte)) * 100, 2) + "%",
                                    "Qualità"));
                        }
                        else
                        {
                            dataSource.Add(new JobRecord("Totale pezzi prodotti", totProduzione, null, null, null, null, "", "Qualità"));
                        }
                        dataSource.Add(new JobRecord("Totale non conformità", scarti.Sum(x => x.QtyProductionWaste), null, null, null, null, "", "Qualità"));
                    }
                    else
                    {
                        //nessun job assegnato
                        dataSource.Add(new JobRecord("Quantità da produrre", null, null, null, null, null, "", "Efficienza"));
                        dataSource.Add(new JobRecord("Eseguito turni precedenti", null, null, null, null, null, "", "Efficienza"));
                        dataSource.Add(new JobRecord("Eseguito turno corrente", null, null, null, null, null, "", "Efficienza"));
                        dataSource.Add(new JobRecord("Scarti", null, null, null, null, null, "", "Efficienza"));
                        dataSource.Add(new JobRecord("Residuo da produrre", null, null, null, null, null, "", "Efficienza"));
                        dataSource.Add(new JobRecord("Tempo totale disponibile", null, null, null, null, null, "", "Disponibilità"));
                        dataSource.Add(new JobRecord("Tempo totale produzione", null, null, null, null, null, "", "Disponibilità"));
                        dataSource.Add(new JobRecord("Totale pezzi prodotti", null, null, null, null, null, "", "Qualità"));
                        dataSource.Add(new JobRecord("Totale non conformità", null, null, null, null, null, "", "Qualità"));
                    }

                    jobGrid.DataSource = dataSource;
                }
            }
        }

        protected void jobGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridGroupHeaderItem)
            {
                GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;
                DataRowView groupDataRow = (DataRowView)e.Item.DataItem;
                item.DataCell.Text = groupDataRow["GroupType"].ToString();
            }
        }

        protected void btnAttrezzaggio_Click(object sender, EventArgs e)
        {
            //chiudo e apro nuova transazione
            btnConfirmUpdateTransaction.Enabled = false;
            if (!string.IsNullOrEmpty(AssetId))
            {
                TransactionService TService = new TransactionService();
                Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);
                JobRepository jobRep = new JobRepository();
                txtSelectedJob.Text = jobRep.FindByID(lastTrans.JobId)?.Description;
                if (!string.IsNullOrEmpty(txtSelectedJob.Text))
                {
                    btnConfirmUpdateTransaction.Enabled = true;
                }
                MoldRepository moldRep = new MoldRepository();
                txtSelectedMold.Text = moldRep.FindByID(lastTrans.MoldId)?.Name;
            }
            hiddenSelectedJobId.Value = null;
            hiddenSelectedMoldId.Value = null;
            dlgUpdateTransaction.Title = "Attrezzaggio";
            dlgUpdateTransaction.OpenDialog();
        }

        protected void btnProduzione_Click(object sender, EventArgs e)
        {
            //chiudo e apro nuova transazione
            btnConfirmUpdateTransaction.Enabled = false;
            if (!string.IsNullOrEmpty(AssetId))
            {
                TransactionService TService = new TransactionService();
                Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);
                JobRepository jobRep = new JobRepository();
                txtSelectedJob.Text = jobRep.FindByID(lastTrans.JobId)?.Description;
                if (!string.IsNullOrEmpty(txtSelectedJob.Text))
                {
                    btnConfirmUpdateTransaction.Enabled = true;
                }
                MoldRepository moldRep = new MoldRepository();
                txtSelectedMold.Text = moldRep.FindByID(lastTrans.MoldId)?.Name;
            }
            hiddenSelectedJobId.Value = null;
            hiddenSelectedMoldId.Value = null;
            dlgUpdateTransaction.Title = "Produzione";
            dlgUpdateTransaction.OpenDialog();
        }

        protected void btnShowJob_Click(object sender, EventArgs e)
        {
            PopUpDialogSelection.Title = "Job";
            PopUpDialogSelection.OpenDialog();
            //tableMP.Visible = false;
            panelMP.Visible = false;
            tableSelection.Visible = true;

            tableSelection.MasterTableView.Name = "Job";
            tableSelection.MasterTableView.Caption = "Job";
            tableSelection.MasterTableView.DataKeyNames = new string[] { "Id" };
            tableSelection.MasterTableView.Columns.Clear();

            GridBoundColumn bCol = new GridBoundColumn();
            bCol.DataField = "Code";
            bCol.HeaderText = GetGlobalResourceObject("Mes", "Code").ToString();
            tableSelection.MasterTableView.Columns.Add(bCol);

            bCol = new GridBoundColumn();
            bCol.DataField = "Description";
            bCol.HeaderText = GetGlobalResourceObject("Mes", "Description").ToString();
            tableSelection.MasterTableView.Columns.Add(bCol);

            GridDateTimeColumn dtCol = new GridDateTimeColumn();
            dtCol.DataField = "StartDate";
            dtCol.HeaderText = "Data inizio";
            dtCol.DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}";
            dtCol.PickerType = GridDateTimeColumnPickerType.DatePicker;
            tableSelection.MasterTableView.Columns.Add(dtCol);

            GridDropDownColumn col = new GridDropDownColumn();
            col.DataField = "Order.CustomerOrderId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Order").ToString();
            col.ListTextField = "OrderCode";
            col.ListValueField = "Id";
            col.DataSourceID = "edsCustomerOrder";
            tableSelection.MasterTableView.Columns.Add(col);

            col = new GridDropDownColumn();
            col.DataField = "Order.IdArticle";
            col.HeaderText = GetGlobalResourceObject("Mes", "Code").ToString() + GetGlobalResourceObject("Mes", "Article").ToString();
            col.ListTextField = "Code";
            col.ListValueField = "Id";
            col.DataSourceID = "edsArticle";
            tableSelection.MasterTableView.Columns.Add(col);

            col = new GridDropDownColumn();
            col.DataField = "Order.IdArticle";
            col.HeaderText = GetGlobalResourceObject("Mes", "Description").ToString() + GetGlobalResourceObject("Mes", "Article").ToString();
            col.ListTextField = "Description";
            col.ListValueField = "Id";
            col.DataSourceID = "edsArticle";
            tableSelection.MasterTableView.Columns.Add(col);

            tableSelection.DataSource = edsJob;
            //tableSelection.MasterTableView.SortExpressions.AddSortExpression(new GridSortExpression() { FieldName = "StartDate" });
            tableSelection.DataBind();
        }

        protected void btnCancelSelection_Click(object sender, EventArgs e)
        {
            PopUpDialogSelection.CloseDialog();
        }

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            if (tableSelection.Visible)
            {
                if (tableSelection.SelectedItems.Count > 0)
                {
                    GridDataItem item = (GridDataItem)tableSelection.SelectedItems[0];//get selected row

                    ArticleMoldService AMS = new ArticleMoldService();
                    switch (tableSelection.MasterTableView.Name)
                    {
                        case "Job":
                            string id = item.GetDataKeyValue("Id").ToString();
                            hiddenSelectedJobId.Value = id;
                            JobRepository jobRep = new JobRepository();
                            txtSelectedJob.Text = jobRep.FindByID(id).Description;
                            if (AMS.GetMoldListByJobId(id, OperatorHelper.ApplicationId).Count == 1)
                            {
                                hiddenSelectedMoldId.Value = AMS.GetMoldListByJobId(id, OperatorHelper.ApplicationId)[0].MoldId;
                                txtSelectedMold.Text = AMS.GetMoldById(hiddenSelectedMoldId.Value).Name;
                            }
                            btnConfirmUpdateTransaction.Enabled = true;
                            break;
                        case "Mold":
                            string moldId = item.GetDataKeyValue("MoldId").ToString();
                            hiddenSelectedMoldId.Value = moldId;
                            txtSelectedMold.Text = AMS.GetMoldById(moldId).Name;
                            break;
                    }
                }
            }
            else
            {
                //MP
                GridDataItem item = (GridDataItem)tableMP.SelectedItems[0];//get selected row
                string id = item.GetDataKeyValue("Id").ToString();
                hiddenSelectedMPId.Value = id;
                //RawMaterialRepository rmRep = new RawMaterialRepository();
                //txtSelectedMP.Text = rmRep.FindByID(id).Code;
            }
            PopUpDialogSelection.CloseDialog();
        }

        protected void btnCancelUpdateTransaction_Click(object sender, EventArgs e)
        {
            hiddenSelectedJobId.Value = null;
            hiddenSelectedMoldId.Value = null;
            hiddenSelectedMPId.Value = null;
            dlgUpdateTransaction.CloseDialog();
        }

        protected void btnConfirmUpdateTransaction_Click(object sender, EventArgs e)
        {
            //chiudo e apro nuova transazione

            TransactionService TService = new TransactionService();

            KeyValuePair<MesEnum.TransactionParam, object> cause = new KeyValuePair<MesEnum.TransactionParam, object>();
            switch (dlgUpdateTransaction.Title)
            {
                case "Attrezzaggio":
                    cause = new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, MesEnum.Cause.SetUp);

                    if (!string.IsNullOrEmpty(TService.GetLastTransactionOpen(AssetId).JobId) && string.IsNullOrEmpty(hiddenSelectedJobId.Value) && string.IsNullOrEmpty(TService.GetLastTransactionOpen(AssetId).CauseId))
                    {
                        //se il job era già presente, e non è stato cambiato, ed è senza causale, aggiorno la transazione in corso senza chiuderla
                        if (!string.IsNullOrEmpty(hiddenSelectedMoldId.Value))
                        {
                            // se c'è un cambio di stampo, aggiorno anche quello nella transaction corrente
                            TService.UpdateLastTransactionOpen(AssetId,
                                new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                            cause });
                            goto EndChangeTransaction;
                        }
                        else
                        {
                            //c'è solo la causale da aggiornare
                            TService.UpdateLastTransactionOpen(AssetId, new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] { cause });
                            goto EndChangeTransaction;
                        }
                    }

                    break;
                case "Produzione":
                    cause = new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, MesEnum.Cause.Production);

                    if (!string.IsNullOrEmpty(TService.GetLastTransactionOpen(AssetId).JobId) &&
                        string.IsNullOrEmpty(hiddenSelectedJobId.Value) &&
                        (string.IsNullOrEmpty(TService.GetLastTransactionOpen(AssetId).CauseId) || TService.LastTransactionOpenIsInProduction(AssetId)))
                    {
                        //se il job era già presente, e non è stato cambiato, ed è senza causale o è in produzione, aggiorno la transazione in corso senza chiuderla
                        if (!string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && !TService.LastTransactionOpenIsInProduction(AssetId))
                        {
                            // se c'è un cambio di stampo, aggiorno anche quello nella transaction corrente
                            TService.UpdateLastTransactionOpen(AssetId,
                                new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                    new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                                    cause });
                            goto EndChangeTransaction;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(hiddenSelectedMoldId.Value))
                            {
                                TService.UpdateLastTransactionOpen(AssetId,
                                new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                    new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value) });
                                goto EndChangeTransaction;
                            }
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(TService.GetLastTransactionOpen(AssetId).JobId) && TService.LastTransactionOpenIsInProduction(AssetId))
                        {
                            //se il job non era presente ed è in produzione
                            TService.UpdateJobIdInProdTransaction(AssetId, hiddenSelectedJobId.Value);
                            if (!string.IsNullOrEmpty(hiddenSelectedMoldId.Value))
                            {
                                //se c'è anche lo stampo aggiorno solo l'ultima transazione
                                TService.UpdateLastTransactionOpen(AssetId,
                                new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                    new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value) });
                            }
                            goto EndChangeTransaction;
                        }
                    }

                    break;
            }
            if (!string.IsNullOrEmpty(hiddenSelectedJobId.Value) || !string.IsNullOrEmpty(hiddenSelectedMoldId.Value) || !string.IsNullOrEmpty(hiddenSelectedMPId.Value))
            {
                if (!string.IsNullOrEmpty(hiddenSelectedJobId.Value) && !string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && !string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                {
                    //Job, mold, MP
                    TService.CloseAndOpenTransaction(
                    AssetId,
                    new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, hiddenSelectedJobId.Value),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.MP, hiddenSelectedMPId.Value),
                            cause});
                }
                else
                {
                    if (!string.IsNullOrEmpty(hiddenSelectedJobId.Value) && !string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //Job e mold
                        TService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, hiddenSelectedJobId.Value),
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                                cause});
                        goto EndChangeTransaction;
                    }
                    if (!string.IsNullOrEmpty(hiddenSelectedJobId.Value) && string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && !string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //Job e MP
                        TService.CloseAndOpenTransaction(
                        AssetId,
                        new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, hiddenSelectedJobId.Value),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.MP, hiddenSelectedMPId.Value),
                            cause});
                        goto EndChangeTransaction;
                    }
                    if (string.IsNullOrEmpty(hiddenSelectedJobId.Value) && !string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && !string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //Mold e MP
                        TService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.MP, hiddenSelectedMPId.Value),
                                cause});
                        goto EndChangeTransaction;
                    }
                    if (!string.IsNullOrEmpty(hiddenSelectedJobId.Value) && string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //Job
                        TService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, hiddenSelectedJobId.Value),
                                cause});
                        goto EndChangeTransaction;
                    }
                    if (string.IsNullOrEmpty(hiddenSelectedJobId.Value) && !string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //Mold
                        TService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Mold, hiddenSelectedMoldId.Value),
                                cause});
                        goto EndChangeTransaction;
                    }
                    if (string.IsNullOrEmpty(hiddenSelectedJobId.Value) && string.IsNullOrEmpty(hiddenSelectedMoldId.Value) && !string.IsNullOrEmpty(hiddenSelectedMPId.Value))
                    {
                        //MP
                        TService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                                new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.MP, hiddenSelectedMPId.Value),
                                cause});
                        goto EndChangeTransaction;
                    }
                }
            }
            else
            {
                //c'è solo la causale da cambiare
                TService.CloseAndOpenTransaction(
                    AssetId,
                    new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] { cause });
                goto EndChangeTransaction;
            }

            EndChangeTransaction:
            //updatePanelIntestazione.Update();
            LoadTestata();
            dlgUpdateTransaction.CloseDialog();
        }

        protected void btnShowMold_Click(object sender, EventArgs e)
        {
            PopUpDialogSelection.Title = GetGlobalResourceObject("Mes", "Mold").ToString();
            PopUpDialogSelection.OpenDialog();
            panelMP.Visible = false;
            tableSelection.Visible = true;

            tableSelection.MasterTableView.Name = "Mold";
            tableSelection.MasterTableView.Caption = GetGlobalResourceObject("Mes", "Mold").ToString();
            tableSelection.MasterTableView.DataKeyNames = new string[] { "MoldId" };
            tableSelection.MasterTableView.Columns.Clear();

            GridDropDownColumn col = new GridDropDownColumn();
            col.DataField = "MoldId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Code").ToString();
            col.ListTextField = "Code";
            col.ListValueField = "Id";
            col.DataSourceID = "edsMold";
            tableSelection.MasterTableView.Columns.Add(col);

            col = new GridDropDownColumn();
            col.DataField = "MoldId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Name").ToString();
            col.ListTextField = "Name";
            col.ListValueField = "Id";
            col.DataSourceID = "edsMold";
            tableSelection.MasterTableView.Columns.Add(col);

            col = new GridDropDownColumn();
            col.DataField = "ArticleId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Code").ToString() + GetGlobalResourceObject("Mes", "Article").ToString();
            col.ListTextField = "Code";
            col.ListValueField = "Id";
            col.DataSourceID = "edsArticle";
            tableSelection.MasterTableView.Columns.Add(col);

            col = new GridDropDownColumn();
            col.DataField = "ArticleId";
            col.HeaderText = GetGlobalResourceObject("Mes", "Article").ToString();
            col.ListTextField = "Description";
            col.ListValueField = "Id";
            col.DataSourceID = "edsArticle";
            tableSelection.MasterTableView.Columns.Add(col);

            ArticleMoldService AMS = new ArticleMoldService();
            TransactionService TService = new TransactionService();
            string jobId = TService.GetLastTransactionOpen(AssetId)?.JobId;
            if (!string.IsNullOrEmpty(jobId))
            {
                jobId = hiddenSelectedJobId.Value;
            }
            tableSelection.DataSource = AMS.GetMoldListByJobId(jobId, OperatorHelper.ApplicationId); //se un job non è già stato scelto, il datasource sarà vuoto
            tableSelection.DataBind();
        }

        protected void btnShowMP_Click(object sender, EventArgs e)
        {
            PopUpDialogSelection.Title = "MP";
            PopUpDialogSelection.OpenDialog();
            panelMP.Visible = true;
            tableSelection.Visible = false;
            tableMP.DataBind();
        }

        #region Fermi
        protected void btnFermi_Click(object sender, EventArgs e)
        {
            OpenFermi(AssetId, transService.GetLastTransactionOpen(AssetId).OperatorId);
        }

        private void OpenFermi(string assetId, string operatorId)
        {
            OperatorService OS = new OperatorService();
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();

            List<Transaction> datasource = transService.TransactionToJustifyList(assetId);
            gridFermi.DataSource = datasource;
            gridFermi.DataBind();

            //if (transOperService.OperatorsOnMachine(assetId).Count == 1) //è l'ultimo operatore rimasto
            //{
            if (datasource.Count > TransactionListToModify.Count)
            {
                btnConfirmCausaleFermo.Enabled = false;
                btnConfirmCausaleFermo.CssClass = "btnActivityDisabled";
            }
            else
            {
                btnConfirmCausaleFermo.Enabled = true;
                btnConfirmCausaleFermo.CssClass = "btnActivity";
            }
            //}

            LoadCauseTreeView();
            updatePnlFermi.Update();

            dlgFermi.Title = string.Format("Fermi - Macchina: {0} - Operatore: {1}", AS.GetAssedById(assetId, OperatorHelper.ApplicationId).Description, OS.GetOperatorNameById(operatorId, OperatorHelper.ApplicationId));
            dlgFermi.OpenDialog();
        }

        protected void gridFermi_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                item["Status"].Text = item["Status"].Text == "False" ? "Off" : "On";
            }
        }

        protected void LoadCauseTreeView()
        {
            causeTreeView.Nodes.Clear();
            CauseService CS = new CauseService();
            //CauseTypeRepository CTR = new CauseTypeRepository();
            //List<CauseType> causeList = CTR.ReadAll(x => x.ApplicationId == OperatorHelper.ApplicationId && (x.Description == "Fermi" || x.Description == "Attrezzaggio" || x.Description == "Manutenzione")).ToList();
            //foreach (CauseType ct in causeList)
            //{
            //    //Node CauseType
            //    RadTreeNode nodeCT = new RadTreeNode(ct.Description, ct.Id);
            //    nodeCT.PostBack = false;
            //    nodeCT.Expanded = true;
            //    foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.Code))
            //    {
            //        RadTreeNode nodeC = new RadTreeNode(c.Code + " - " + c.Description, c.Id);
            //        nodeC.PostBack = true;
            //        nodeC.BorderStyle = BorderStyle.Solid;
            //        nodeC.BorderColor = System.Drawing.Color.Black;
            //        nodeC.Height = Unit.Percentage(30);
            //        //nodeC.Width = Unit.Percentage(66);
            //        nodeCT.Nodes.Add(nodeC);
            //    }
            //    causeTreeView.Nodes.Add(nodeCT);
            //}

            //Node CauseType
            RadTreeNode nodeCT = new RadTreeNode("Causali");
            nodeCT.PostBack = false;
            nodeCT.Expanded = true;
            List<MES.Models.Cause> causeList = CS.GetListCauseByGroupName(new string[] { "Attrezzaggio", "Manutenzione", "Fermi" }, OperatorHelper.ApplicationId);
            foreach (MES.Models.Cause c in causeList.OrderBy(x => x.Code))
            {
                RadTreeNode nodeC = new RadTreeNode(c.Code + " - " + c.Description, c.Id);
                nodeC.PostBack = true;
                nodeC.BorderStyle = BorderStyle.Solid;
                nodeC.BorderColor = System.Drawing.Color.Black;
                nodeC.Height = Unit.Percentage(30);
                //nodeC.Width = Unit.Percentage(66);
                nodeCT.Nodes.Add(nodeC);
            }
            causeTreeView.Nodes.Add(nodeCT);
            //causeTreeView.DataBind();
        }

        protected void causeTreeView_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            TransactionRepository tRep = new TransactionRepository();
            //List<Transaction> transactionListToModify = new List<Transaction>();
            //if (CustomSettings.Params.ContainsKey(TRANSACTION_LIST_TO_MODIFY))
            //{
            //    transactionListToModify = JsonConvert.DeserializeObject<List<Transaction>>(CustomSettings.Params[TRANSACTION_LIST_TO_MODIFY].ToString());
            //}
            GridItemCollection listItem = gridFermi.SelectedItems; //get selected rows
            foreach (GridDataItem item in listItem)
            {
                int id = (int)item.GetDataKeyValue("Id");
                Transaction t = tRep.FindByID(id);
                t.CauseId = e.Node.Value;
                //transactionListToModify.Add(t);
                TransactionListToModify.Add(new FermiRecord(t.Id, t.CauseId));
            }
            //CustomSettings.AddOrUpdate(TRANSACTION_LIST_TO_MODIFY, transactionListToModify);

            //update table fermi
            TransactionService TS = new TransactionService();
            List<Transaction> datasource = TS.TransactionToJustifyList(AssetId);
            foreach (var t in TransactionListToModify)
            {
                int index = datasource.IndexOf(datasource.Where(x => x.Id == t.TransactionId).First());
                datasource.ElementAt(index).CauseId = t.CauseId;
            }
            gridFermi.DataSource = datasource;
            gridFermi.DataBind();

            if (TransactionListToModify.Count > 0)
            {
                btnConfirmCausaleFermo.Enabled = true;
                btnConfirmCausaleFermo.CssClass = "btnActivity";
            }
            else
            {
                btnConfirmCausaleFermo.Enabled = false;
                btnConfirmCausaleFermo.CssClass = "btnActivityDisabled";
            }
        }

        protected void btnConfirmCausaleFermo_Click(object sender, EventArgs e)
        {
            TransactionRepository tRep = new TransactionRepository();
            if (TransactionListToModify.Count > 0)
            {
                TransactionService TS = new TransactionService();
                List<Transaction> datasource = TS.TransactionToJustifyList(AssetId);
                foreach (var t in TransactionListToModify)
                {
                    int index = datasource.IndexOf(datasource.Where(x => x.Id == t.TransactionId).First());
                    datasource.ElementAt(index).CauseId = t.CauseId;
                }
                tRep.UpdateAll(datasource);
                tRep.SaveChanges();
                TransactionListToModify.Clear();
            }
            dlgFermi.CloseDialog();

            //if (grantService.AllowProduction(hiddenOperatorId.Value))
            //{
            //    //chiede se la commessa è terminata oppure no
            //    dlgCloseCustomerOrder.OpenDialog();
            //}
        }

        protected void btnCancelCausaleFermo_Click(object sender, EventArgs e)
        {
            TransactionListToModify.Clear();
            dlgFermi.CloseDialog();

            //if (grantService.AllowProduction(hiddenOperatorId.Value))
            //{
            //    //chiede se la commessa è terminata oppure no
            //    dlgCloseCustomerOrder.OpenDialog();
            //}
        }


        //protected void gridFermi_ItemDataBound(object sender, GridItemEventArgs e)
        //{
        //    if (e.Item is GridDataItem)
        //    {
        //        GridDataItem item = (GridDataItem)e.Item;
        //        item["Status"].Text = item["Status"].Text == "False" ? "Off" : "On";
        //    }
        //}

        //protected void LoadCauseTreeView()
        //{
        //    causeTreeView.Nodes.Clear();
        //    CauseTypeRepository CTR = new CauseTypeRepository();
        //    List<CauseType> causeList = CTR.ReadAll(x => x.ApplicationId == OperatorHelper.ApplicationId && (x.Description == "Fermi" || x.Description == "Attrezzaggio" || x.Description == "Manutenzione")).ToList();
        //    foreach (CauseType ct in causeList)
        //    {
        //        //Node CauseType
        //        RadTreeNode nodeCT = new RadTreeNode(ct.Description, ct.Id);
        //        nodeCT.PostBack = false;
        //        nodeCT.Expanded = true;
        //        foreach (Cause c in ct.Cause.OrderBy(x => x.Code))
        //        {
        //            RadTreeNode nodeC = new RadTreeNode(c.Code + " - " + c.Description, c.Id);
        //            nodeC.PostBack = true;
        //            nodeC.BorderStyle = BorderStyle.Solid;
        //            nodeC.BorderColor = System.Drawing.Color.Black;
        //            nodeC.Height = Unit.Percentage(30);
        //            //nodeC.Width = Unit.Percentage(66);
        //            nodeCT.Nodes.Add(nodeC);
        //        }
        //        causeTreeView.Nodes.Add(nodeCT);
        //    }
        //    //causeTreeView.DataBind();
        //}

        //protected void causeTreeView_NodeClick(object sender, RadTreeNodeEventArgs e)
        //{
        //    TransactionRepository tRep = new TransactionRepository();
        //    //List<Transaction> transactionListToModify = new List<Transaction>();
        //    //if (CustomSettings.Params.ContainsKey(TRANSACTION_LIST_TO_MODIFY))
        //    //{
        //    //    transactionListToModify = JsonConvert.DeserializeObject<List<Transaction>>(CustomSettings.Params[TRANSACTION_LIST_TO_MODIFY].ToString());
        //    //}
        //    GridItemCollection listItem = gridFermi.SelectedItems; //get selected rows
        //    foreach (GridDataItem item in listItem)
        //    {
        //        int id = (int)item.GetDataKeyValue("Id");
        //        Transaction t = tRep.FindByID(id);
        //        t.CauseId = e.Node.Value;
        //        //transactionListToModify.Add(t);
        //        TransactionListToModify.Add(new FermiRecord(t.Id, t.CauseId));
        //    }
        //    //CustomSettings.AddOrUpdate(TRANSACTION_LIST_TO_MODIFY, transactionListToModify);

        //    //update table fermi
        //    TransactionService TS = new TransactionService();
        //    List<Transaction> datasource = TS.TransactionToJustifyList(AssetId);
        //    foreach (var t in TransactionListToModify)
        //    {
        //        int index = datasource.IndexOf(datasource.Where(x => x.Id == t.TransactionId).First());
        //        datasource.ElementAt(index).CauseId = t.CauseId;
        //    }
        //    gridFermi.DataSource = datasource;
        //    gridFermi.DataBind();
        //}

        //protected void btnConfirmCausaleFermo_Click(object sender, EventArgs e)
        //{
        //    TransactionRepository tRep = new TransactionRepository();
        //    if (TransactionListToModify.Count > 0)
        //    {
        //        TransactionService TS = new TransactionService();
        //        List<Transaction> datasource = TS.TransactionToJustifyList(AssetId);
        //        foreach (var t in TransactionListToModify)
        //        {
        //            int index = datasource.IndexOf(datasource.Where(x => x.Id == t.TransactionId).First());
        //            datasource.ElementAt(index).CauseId = t.CauseId;
        //        }
        //        tRep.UpdateAll(datasource);
        //        tRep.SaveChanges();
        //        TransactionListToModify.Clear();
        //    }
        //    dlgFermi.CloseDialog();
        //}

        //protected void btnCancelCausaleFermo_Click(object sender, EventArgs e)
        //{
        //    TransactionListToModify.Clear();
        //    dlgFermi.CloseDialog();
        //}
        #endregion

        #region Scarti
        protected void btnConfirmScarti_Click(object sender, EventArgs e)
        {
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

            Transaction lastTrans = transService.GetLastTransactionOpen(AssetId);
            foreach (ScartiRecord sr in JobWasteListToModify)
            {
                JobProductionWaste jpw = new JobProductionWaste();
                jpw.AssetId = AssetId;
                jpw.CauseId = sr.CauseId;
                jpw.Date = DateTime.Now;
                jpw.JobId = lastTrans.JobId;
                jpw.OperatorId = lastTrans.OperatorId;
                jpw.QtyProductionWaste = sr.Num;
                jpw.ArticleId = lastTrans.Job?.Order?.IdArticle;
                jpwRep.Insert(jpw);
            }
            jpwRep.SaveChanges();

            JobWasteListToModify.Clear();
            dlgScarti.CloseDialog();

            //apro i fermi
            //OpenFermi(AssetId, hiddenOperatorId.Value);
        }

        protected void btnCancelScarti_Click(object sender, EventArgs e)
        {
            JobWasteListToModify.Clear();
            dlgScarti.CloseDialog();

            //apro i fermi
            //OpenFermi(AssetId, hiddenOperatorId.Value);
        }

        protected void btnNumPadOk_Click(object sender, EventArgs e)
        {
            float num;
            if (float.TryParse(txtInsValue.Text, out num))
            {
                txtInsValue.Text = num.ToString();
            }

            if (gridScarti.SelectedItems.Count > 0)
            {
                GridDataItem selectedItem = (GridDataItem)gridScarti.SelectedItems[0]; //get selected row
                JobWasteListToModify.Add(new ScartiRecord(selectedItem.GetDataKeyValue("CauseId").ToString(), "", Convert.ToDecimal(txtInsValue.Text)));

                JobProductionWasteService jpwService = new JobProductionWasteService();
                List<JobProductionWaste> wasteList = jpwService.GetWasteListByJobId(transService.GetLastTransactionOpen(AssetId).JobId, OperatorHelper.ApplicationId);


                //update datasource
                CauseTypeRepository CTR = new CauseTypeRepository();
                List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == OperatorHelper.ApplicationId).OrderBy(x => x.Description).ToList();
                List<ScartiRecord> dataSource = new List<ScartiRecord>();
                foreach (CauseType ct in causeList)
                {
                    foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode))
                    {
                        //if (wasteList.Where(x => x.CauseId == c.Id).Any())
                        //{
                        //    dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteList.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste)));
                        //}
                        //else
                        //{
                        dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                        //}
                    }
                }

                foreach (ScartiRecord sr in JobWasteListToModify)
                {
                    int index = dataSource.IndexOf(dataSource.Where(x => x.CauseId == sr.CauseId).First());
                    dataSource.ElementAt(index).Num = sr.Num;
                }
                gridScarti.DataSource = dataSource;
                gridScarti.DataBind();

                txtQtyWasteTransaction.Text = JobWasteListToModify.Sum(x => x.Num).ToString();

                //Aggiornamento quantità totale in corso
                //List<Transaction> currentJobTList = transService.GetTransactionListByCurrentJob(AssetId);
                //txtQtyProducedTransaction.Text = currentJobTList.Sum(x => x.PartialCounting).ToString();

                txtInsValue.Text = "";
            }
        }

        protected void btnScarti_Click(object sender, EventArgs e)
        {
            OperatorService OS = new OperatorService();
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            string operatorId = transService.GetLastTransactionOpen(AssetId).OperatorId;

            TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
            string jobId = transService.GetLastTransactionOpen(AssetId).JobId;

            List<Transaction> currentJobTList = transService.GetTransactionListByJobId(AssetId, jobId);
            if (currentJobTList != null)
            {
                if (currentJobTList.Sum(x => x.PartialCounting) == 0)
                {
                    btnNumPadOk.Enabled = false;
                    btnConfirmScarti.Enabled = false;
                }
                txtQtyProducedTot.Text = Math.Round(currentJobTList.Sum(x => x.PartialCounting), 0).ToString();
                txtQtyProducedOperator.Text = Math.Round(currentJobTList.Where(x => x.OperatorId == operatorId).Sum(x => x.PartialCounting), 0).ToString();

                JobProductionWasteService jpwService = new JobProductionWasteService();
                List<JobProductionWaste> wasteList = jpwService.GetWasteListByJobId(transService.GetLastTransactionOpen(AssetId).JobId, OperatorHelper.ApplicationId);
                txtQtyWasteTransaction.Text = wasteList.Sum(x => x.QtyProductionWaste).ToString();

                CauseTypeRepository CTR = new CauseTypeRepository();
                List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == OperatorHelper.ApplicationId).ToList();
                List<ScartiRecord> dataSource = new List<ScartiRecord>();
                foreach (CauseType ct in causeList)
                {
                    foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode))
                    {
                        //if (wasteList.Where(x => x.CauseId == c.Id).Any())
                        //{
                        //    dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteList.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste)));
                        //}
                        //else
                        //{
                        dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                        //}
                    }
                }

                gridScarti.DataSource = dataSource;
                gridScarti.DataBind();

                dlgScarti.Title = string.Format("Scarti - Macchina: {0} - Operatore: {1} - Commessa: {3} - Articolo: {2}",
                    AS.GetAssedById(AssetId, OperatorHelper.ApplicationId).Description,
                    OS.GetOperatorNameById(operatorId, OperatorHelper.ApplicationId),
                    currentJobTList.First(x => !string.IsNullOrEmpty(x.JobId)).Job?.Order?.Article?.Code,
                    currentJobTList.First(x => !string.IsNullOrEmpty(x.JobId)).Job?.Order?.OrderCode);
                dlgScarti.OpenDialog();
            }
        }

        //protected void btnScarti_Click(object sender, EventArgs e)
        //{
        //    TransactionService TService = new TransactionService();
        //    //Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);
        //    //txtQtyProducedTransaction.Text = lastTrans.PartialCounting.ToString();

        //    List<Transaction> currentJobTList = TService.GetTransactionListByCurrentJob(AssetId);
        //    if (currentJobTList.Sum(x => x.PartialCounting) == 0)
        //    {
        //        btnNumPadOk.Enabled = false;
        //        btnConfirmScarti.Enabled = false;
        //    }
        //    txtQtyProducedTransaction.Text = Math.Round(currentJobTList.Sum(x => x.PartialCounting), 0).ToString();

        //    JobProductionWasteService jpwService = new JobProductionWasteService();
        //    List<JobProductionWaste> wasteList = jpwService.GetWasteListByJobId(TService.GetLastTransactionOpen(AssetId).JobId, OperatorHelper.ApplicationId);
        //    txtQtyWasteTransaction.Text = wasteList.Sum(x => x.QtyProductionWaste).ToString();

        //    CauseTypeRepository CTR = new CauseTypeRepository();
        //    List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == OperatorHelper.ApplicationId).ToList();
        //    List<ScartiRecord> dataSource = new List<ScartiRecord>();
        //    foreach (CauseType ct in causeList)
        //    {
        //        foreach (Cause c in ct.Cause.OrderBy(x => x.ExternalCode))
        //        {
        //            //if (wasteList.Where(x => x.CauseId == c.Id).Any())
        //            //{
        //            //    dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteList.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste)));
        //            //}
        //            //else
        //            //{
        //                dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
        //            //}
        //        }
        //    }

        //    gridScarti.DataSource = dataSource;
        //    gridScarti.DataBind();

        //    dlgScarti.OpenDialog();
        //}

        //protected void btnConfirmScarti_Click(object sender, EventArgs e)
        //{
        //    JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();
        //    TransactionService TService = new TransactionService();
        //    Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);
        //    foreach (ScartiRecord sr in JobWasteListToModify)
        //    {
        //        JobProductionWaste jpw = new JobProductionWaste();
        //        jpw.AssetId = AssetId;
        //        jpw.CauseId = sr.CauseId;
        //        jpw.Date = DateTime.Now;
        //        jpw.JobId = lastTrans.JobId;
        //        jpw.OperatorId = lastTrans.OperatorId;
        //        jpw.QtyProductionWaste = sr.Num;
        //        jpw.ArticleId = lastTrans.Job?.ArticleId;
        //        jpwRep.Insert(jpw);
        //    }
        //    jpwRep.SaveChanges();

        //    JobWasteListToModify.Clear();
        //    dlgScarti.CloseDialog();
        //    jobGrid.Rebind();
        //}

        //protected void btnCancelScarti_Click(object sender, EventArgs e)
        //{
        //    JobWasteListToModify.Clear();
        //    dlgScarti.CloseDialog();
        //}
        #endregion

        protected void btnExit_Click(object sender, EventArgs e)
        {
            Response.Redirect("OperatorAccess.aspx");
        }

        #region Tastierino numrerico
        protected void num_Click(object sender, EventArgs e)
        {
            float num;
            RadButton btn = (RadButton)sender;
            txtInsValue.Text = txtInsValue.Text + btn.Text;
            if (float.TryParse(txtInsValue.Text, out num))
            {
                txtInsValue.Text = num.ToString();
            }
        }

        protected void vir_Click(object sender, EventArgs e)
        {
            RadButton btn = (RadButton)sender;
            if (txtInsValue.Text.IndexOf(",") < 0)
            {
                txtInsValue.Text = txtInsValue.Text + btn.Text;
            }
        }

        protected void canc_Click(object sender, EventArgs e)
        {
            float num;
            if (txtInsValue.Text.Length > 0)
            {
                txtInsValue.Text = txtInsValue.Text.Substring(0, txtInsValue.Text.Length - 1);
            }
            if (float.TryParse(txtInsValue.Text, out num))
            {
                txtInsValue.Text = num.ToString();
            }
        }

        //protected void btnNumPadOk_Click(object sender, EventArgs e)
        //{
        //    float num;
        //    if (float.TryParse(txtInsValue.Text, out num))
        //    {
        //        txtInsValue.Text = num.ToString();
        //    }

        //    if (gridScarti.SelectedItems.Count > 0)
        //    {
        //        GridDataItem selectedItem = (GridDataItem)gridScarti.SelectedItems[0]; //get selected row
        //        JobWasteListToModify.Add(new ScartiRecord(selectedItem.GetDataKeyValue("CauseId").ToString(), "", Convert.ToDecimal(txtInsValue.Text)));

        //        TransactionService TService = new TransactionService();
        //        JobProductionWasteService jpwService = new JobProductionWasteService();
        //        List<JobProductionWaste> wasteList = jpwService.GetWasteListByJobId(TService.GetLastTransactionOpen(AssetId).JobId, OperatorHelper.ApplicationId);


        //        //update datasource
        //        CauseTypeRepository CTR = new CauseTypeRepository();
        //        List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == OperatorHelper.ApplicationId).OrderBy(x => x.Description).ToList();
        //        List<ScartiRecord> dataSource = new List<ScartiRecord>();
        //        foreach (CauseType ct in causeList)
        //        {
        //            foreach (Cause c in ct.Cause.OrderBy(x => x.ExternalCode))
        //            {
        //                //if (wasteList.Where(x => x.CauseId == c.Id).Any())
        //                //{
        //                //    dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteList.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste)));
        //                //}
        //                //else
        //                //{
        //                    dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
        //                //}
        //            }
        //        }

        //        foreach (ScartiRecord sr in JobWasteListToModify)
        //        {
        //            int index = dataSource.IndexOf(dataSource.Where(x => x.CauseId == sr.CauseId).First());
        //            dataSource.ElementAt(index).Num = sr.Num;
        //        }
        //        gridScarti.DataSource = dataSource;
        //        gridScarti.DataBind();

        //        txtQtyWasteTransaction.Text = JobWasteListToModify.Sum(x => x.Num).ToString();

        //        //Aggiornamento quantità totale in corso
        //        //TransactionService TService = new TransactionService();
        //        //List<Transaction> currentJobTList = TService.GetTransactionListByCurrentJob(AssetId);
        //        //txtQtyProducedTransaction.Text = currentJobTList.Sum(x => x.PartialCounting).ToString();

        //        txtInsValue.Text = "";
        //    }
        //}
        #endregion

        protected void btnQualità_Click(object sender, EventArgs e)
        {
            Response.Redirect("ControlPlanExecutionList?asset=" + AssetId);
        }

        protected void btnManutenzione_Click(object sender, EventArgs e)
        {
            //chiudo e apro nuova transazione

            manutenzioneTreeView.Nodes.Clear();
            CauseTypeRepository CTR = new CauseTypeRepository();
            List<CauseType> causeList = CTR.ReadAll(x => x.ApplicationId == OperatorHelper.ApplicationId && x.Description == "Manutenzione").ToList();
            foreach (CauseType ct in causeList)
            {
                //Node CauseType
                RadTreeNode nodeCT = new RadTreeNode(ct.Description, ct.Id);
                nodeCT.PostBack = false;
                nodeCT.Expanded = true;
                foreach (Cause c in ct.Cause.OrderBy(x => x.Code))
                {
                    RadTreeNode nodeC = new RadTreeNode(c.Code + " - " + c.Description, c.Id);
                    nodeC.PostBack = false;
                    nodeC.BorderStyle = BorderStyle.Solid;
                    nodeC.BorderColor = System.Drawing.Color.Black;
                    nodeC.Height = Unit.Percentage(30);
                    //nodeC.Width = Unit.Percentage(66);
                    nodeCT.Nodes.Add(nodeC);
                }
                manutenzioneTreeView.Nodes.Add(nodeCT);
            }

            dlgManutenzione.OpenDialog();

        }

        protected void btnConfirmManutenzione_Click(object sender, EventArgs e)
        {
            string causeId = manutenzioneTreeView.SelectedValue;
            //TODO: aggiungere cambio transazione
            TransactionService TService = new TransactionService();
            TService.CloseAndOpenTransaction(
                AssetId,
                new KeyValuePair<MES.Core.MesEnum.TransactionParam, object>[] {
                    new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, causeId)});
            dlgManutenzione.CloseDialog();
        }

        protected void btnCancelManutenzione_Click(object sender, EventArgs e)
        {
            dlgManutenzione.CloseDialog();
        }

        protected void btnOEE_Click(object sender, EventArgs e)
        {
            CauseService causeService = new CauseService();
            string causeProdId = causeService.GetCauseByCode("200", OperatorHelper.ApplicationId).Id;

            TransactionRepository TRep = new TransactionRepository();
            TransactionService TService = new TransactionService();
            Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);
            Job job = lastTrans.Job;

            int nImpronte = 1;
            if (!string.IsNullOrEmpty(lastTrans.MoldId))
            {
                MoldRepository mRep = new MoldRepository();
                nImpronte = mRep.FindByID(lastTrans.MoldId).CavityMoldNum;
            }
            if (nImpronte == 0)
            {
                nImpronte = 1;
            }


            List<Transaction> jobTransList = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id).ToList();

            decimal effettivo;
            try
            {
                effettivo = Math.Round(jobTransList.Sum(x => x.PartialCounting) / Convert.ToDecimal(TimeSpan.FromSeconds((double)jobTransList.Where(x => x.CauseId == causeProdId).Sum(x => x.Duration)).TotalHours), 2);
            }
            catch (DivideByZeroException)
            {
                effettivo = 0;
            }
            var totProduzione = jobTransList.Sum(x => x.PartialCounting);
            JobProductionWasteService jpws = new JobProductionWasteService();
            List<JobProductionWaste> scarti = jpws.GetWasteListByJobId(job.Id, OperatorHelper.ApplicationId);

            var efficienza = job.StandardRate > 0 ? effettivo / job.StandardRate : 0;
            decimal qualità;
            if (totProduzione > 0)
            {
                qualità = ((totProduzione * nImpronte) - scarti.Sum(x => x.QtyProductionWaste)) / (totProduzione * nImpronte);
            }
            else
            {
                qualità = 0;
            }
            var resa = TimeSpan.FromSeconds((double)jobTransList.Where(x => x.CauseId == causeProdId).Sum(x => x.Duration)).TotalSeconds /
                       TimeSpan.FromSeconds((double)jobTransList.Sum(x => x.Duration)).TotalSeconds;
            if (Double.IsNaN(resa))
            {
                resa = 0;
            }

            meterEfficienza.Pointer.Value = Math.Round(efficienza * 100, 2);
            txtEfficienza.Text = Math.Round(efficienza * 100, 2).ToString();
            meterQualità.Pointer.Value = Math.Round(qualità * 100, 2);
            txtQualità.Text = Math.Round(qualità * 100, 2).ToString();
            meterResa.Pointer.Value = Math.Round((decimal)resa * 100, 2);
            txtResa.Text = Math.Round((decimal)resa * 100, 2).ToString();
            meterOEE.Pointer.Value = Math.Round(efficienza * qualità * (decimal)resa * 100, 2);
            txtOEE.Text = Math.Round(efficienza * qualità * (decimal)resa * 100, 2).ToString();
            dlgOEE.Title = "OEE - " + Page.Title;
            dlgOEE.OpenDialog();
            jobGrid.Rebind();
        }

        protected void btnCloseOEE_Click(object sender, EventArgs e)
        {
            dlgOEE.CloseDialog();
        }

        protected void btnWarningMaintenance_Click(object sender, EventArgs e)
        {
            dlgWarningMaintenance.Title += " - " + txtMold;
            dlgWarningMaintenance.OpenDialog();
        }

        protected void btnCloseWarningMaintenance_Click(object sender, EventArgs e)
        {
            dlgWarningMaintenance.CloseDialog();
        }

        protected void btnCloseWarningQuality_Click(object sender, EventArgs e)
        {
            dlgWarningQuality.CloseDialog();
        }

        protected void btnWarningQuality_Click(object sender, EventArgs e)
        {
            dlgWarningQuality.OpenDialog();
        }
    }
}