﻿<%@ Page Language="C#" MasterPageFile="~/DashboardOperator/MainOperatorAccess.Master" AutoEventWireup="true" CodeBehind="OperatorAccess_old.aspx.cs" Inherits="MainCloudFramework.DashboardOperator.OperatorAccess_old" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="Style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="Script.js"></script>
    <%--<link rel="stylesheet" href="jQuery.NumPad-master/jquery.numpad.css">
    <script type="text/javascript" src="jQuery.NumPad-master/jquery.numpad.js"></script>--%>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
        <p class="text-danger">
            <asp:Literal runat="server" ID="FailureText" />
        </p>
    </asp:PlaceHolder>

    <div style="float:left;"><b>Legenda macchina:</b>&nbsp; &nbsp; &nbsp;</div>
    <div style="float:left;"><div style="background-color:yellow; width:20px; height:20px; float:left;"></div>&nbsp;<b>Attrezzaggio</b>&nbsp; &nbsp; &nbsp;</div>
    <div style="float:left;"><div style="background-color:lightgreen; width:20px; height:20px; float:left;"></div>&nbsp;<b>Produzione</b>&nbsp; &nbsp; &nbsp;</div>
    <div style="float:left;"><div style="background-color:lightblue; width:20px; height:20px; float:left;"></div>&nbsp;<b>Manutenzione</b>&nbsp; &nbsp; &nbsp;</div>
    <div style="float:left;"><div style="background-color:#f75555; width:20px; height:20px; float:left;"></div>&nbsp;<b>Ferma</b>&nbsp; &nbsp; &nbsp;</div>
    <div style="float:left;"><div style="border:1px solid; width:20px; height:20px; float:left;"></div>&nbsp;<b>Spenta</b></div>
    <br /><br />

<asp:UpdatePanel runat="server" UpdateMode="Always">
    <ContentTemplate>
        <div class="row" style="width:100%;">
            <%--<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">--%>
                <telerik:RadGrid runat="server" ID="machineTable" RenderMode="Lightweight" AutoGenerateColumns="false" CssClass="tableNoCollapse"
                    OnNeedDataSource="machineTable_NeedDataSource" OnItemCommand="machineTable_ItemCommand" OnItemDataBound="machineTable_ItemDataBound">
                    <MasterTableView FilterExpression="" Caption="" DataKeyNames="AssetId" ShowHeadersWhenNoRecords="false" NoMasterRecordsText="Non ci sono macchine per la zona selezionata">
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldAlias="Operator" FieldName="Operator"></telerik:GridGroupByField>
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldName="Operator" HeaderText="ok" SortOrder="Descending"></telerik:GridGroupByField>
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                        <Columns>
                            <telerik:GridTemplateColumn UniqueName="btnLogin">
                                <ItemTemplate>
                                    <telerik:RadButton runat="server" CommandName="Login" RenderMode="Lightweight" CssClass="btnActivityTable">
                                        <ContentTemplate>
                                            <center><i class="fa fa-sign-in" aria-hidden="true"></i> Inizio</center>
                                        </ContentTemplate>
                                    </telerik:RadButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <%--<telerik:GridButtonColumn UniqueName="btnLogin" Text="Connect" ButtonType="PushButton" CommandName="Login" ButtonCssClass="btnActivity">
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridButtonColumn>--%>
                            <telerik:GridButtonColumn DataTextField="Asset" HeaderText="Macchina" UniqueName="Asset"
                                ButtonType="PushButton" ButtonCssClass="hyperLinkText">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" CssClass="hyperLinkText" Font-Bold="true" Font-Size="Medium" />
                            </telerik:GridButtonColumn>
                             <telerik:GridBoundColumn DataField="Status" HeaderText="Stato">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <%--<telerik:GridHyperLinkColumn DataTextField="Asset" HeaderText="Macchina">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" CssClass="hyperLinkText" />
                            </telerik:GridHyperLinkColumn>--%>
                            <telerik:GridTemplateColumn UniqueName="btnLogout">
                                <ItemTemplate>
                                    <telerik:RadButton runat="server" CommandName="Logout" RenderMode="Lightweight">
                                        <ContentTemplate>
                                            <center><i class="fa fa-sign-out" aria-hidden="true"></i> Fine</center>
                                        </ContentTemplate>
                                    </telerik:RadButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Order" HeaderText="<%$ Resources:Mes,Order %>">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Article" HeaderText="<%$ Resources:Mes,Article %>">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="btnChangeOperator" Visible="false">
                                <ItemTemplate>
                                    <telerik:RadButton runat="server" CommandName="Login" RenderMode="Lightweight">
                                        <ContentTemplate>
                                            <center><i class="fa fa-male" aria-hidden="true"></i> Cambia operatore</center>
                                        </ContentTemplate>
                                    </telerik:RadButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Quality" HeaderText="Q" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn DataField="Quality" HeaderText="Q" UniqueName="QualityLevel">
                                <ItemTemplate>
                                    <div runat="server" class="tableLight"></div>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="btnAddOperator">
                                <ItemTemplate>
                                    <telerik:RadButton runat="server" CommandName="AddOperator" CssClass="btnActivityTable" RenderMode="Lightweight">
                                        <ContentTemplate>
                                            <center><i class="fa fa-plus-circle" aria-hidden="true"></i> Altri</center>
                                        </ContentTemplate>
                                    </telerik:RadButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="False"></Selecting>
                        <%--<Scrolling AllowScroll="true" UseStaticHeaders="true" />--%>
                    </ClientSettings>
                </telerik:RadGrid>
            <%--</div>--%>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <%--<Triggers>
        <asp:AsyncPostBackTrigger ControlID="machineTable" EventName="ItemCommand" />
    </Triggers>--%>
    <ContentTemplate>

        <mcf:PopUpDialog ID="LogoutWindow" runat="server" Title="Avviso - Logout" Width="100%" MarginLeft="0%">
            <Body>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <%--<asp:AsyncPostBackTrigger ControlID="LogoutWindow$gridOperator" EventName="SelectedIndexChanged" />--%>
                    </Triggers>
                    <ContentTemplate>

                        <telerik:RadGrid runat="server" ID="gridOperator" RenderMode="Lightweight" AutoGenerateColumns="false" OnItemCommand="gridOperator_ItemCommand"
                            OnNeedDataSource="gridOperator_NeedDataSource" OnItemDataBound="gridOperator_ItemDataBound" OnSelectedIndexChanged="gridOperator_SelectedIndexChanged"
                            LocalizationPath="~/App_GlobalResources/">
                            <ClientSettings EnablePostBackOnRowClick="true">
                                <Selecting AllowRowSelect="true" UseClientSelectColumnOnly="false" />
                            </ClientSettings>
                            <MasterTableView DataKeyNames="Id, OperatorId" Caption="Operatori" NoMasterRecordsText="Non ci sono operatori">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="OperatorId" HeaderText="Nome">
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="RoleId" HeaderText="Ruolo">
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridDateTimeColumn DataField="DateStart" HeaderText="DataOra ingresso">
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridDateTimeColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>

                        <br /><br />

                        <asp:HiddenField runat="server" ID="hiddenQualityLevelValue" ClientIDMode="Static" />
                        <asp:Panel ID="pnlAllowConfirmProduction" runat="server" GroupingText="Qualità" Visible="false">
                            <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1">
                                <div id="box-out">
			                        <div id="box-inside">
				                        <%--<div id="circulo3" class="colorRed"></div>
				                        <div id="circulo2" class="colorYellow"></div>
				                        <div id="circulo1" class="colorGreen"></div>--%>
                                        <div runat="server" id="redLight" onclick="switchLight(0)" class="light stop"></div>
                                        <div runat="server" id="yellowLight" onclick="switchLight(1)" class="light slow"></div>
                                        <div runat="server" id="greenLight" onclick="switchLight(2)" class="light go"></div>
			                        </div>
		                        </div>
                            </div>
                            <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2">
                                <br />
                                <asp:Label Text="Stop" Font-Bold="true" runat="server" Font-Size="X-Large" />
                                <br /><br /><br />
                                <asp:Label Text="Warning" Font-Bold="true" runat="server" Font-Size="X-Large" />
                                <br /><br /><br />
                                <asp:Label Text="Ok" Font-Bold="true" runat="server" Font-Size="X-Large" />
                            </div>
                        </asp:Panel>

                        <br /><br />

                        <div class="row alDx">
                            <telerik:RadButton runat="server" ID="btnConfirmLogout" OnClick="btnConfirmLogout_Click">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton runat="server" ID="btnCancelLogout" CssClass="btnActivity" OnClick="btnCancelLogout_Click">
                                <ContentTemplate>
                                    <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </Body>
        </mcf:PopUpDialog>

    </ContentTemplate>
</asp:UpdatePanel>

    <br /><br />

<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:PostBackTrigger ControlID="machineTable" />
        <%--<asp:AsyncPostBackTrigger ControlID="ConnectWindow$btnSetUp" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="ConnectWindow$btnProduction" EventName="Click" />--%>
    </Triggers>
    <ContentTemplate>

<mcf:PopUpDialog ID="ConnectWindow" runat="server" Width="100%" MarginLeft="0%">
    <Body>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
            
                <%--<asp:Panel runat="server" ID="pnlConnectOperator">--%>
                    <asp:HiddenField runat="server" ID="hiddenMachineId" />
                    <asp:HiddenField runat="server" ID="ModifyMachineTransaction" />
                    <div class="row">
                        <div class="col-xs-7 col-sm-7 col-md-8 col-lg-8">
                            <%--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--%>
                            <telerik:RadTextBox runat="server" ID="txtBadge" AutoPostBack="true" OnTextChanged="txtBadge_TextChanged"
                                Label="Badge:" LabelWidth="89px" LabelCssClass="labelComboBox" ValidationGroup="OperatorValidation"
                                Width="290px" Height="34px" EmptyMessage="Inserisci">
                                <ClientEvents OnFocus="Clear" />
                            </telerik:RadTextBox>
                            <br />
                            <%--<telerik:RadTextBox runat="server" ID="txtRegistrationId" AutoPostBack="true" OnTextChanged="txtRegistrationId_TextChanged"
                                Label="Matricola:" LabelWidth="89px" LabelCssClass="labelComboBox" ValidationGroup="OperatorValidation"
                                Width="290px" Height="34px" EmptyMessage="Inserisci" />--%>
                            <telerik:RadTextBox runat="server" ID="txtRegistrationId" ReadOnly="true" ValidationGroup="OperatorValidation"
                                Label="Matricola:" LabelWidth="89px" Width="290px" Height="34px" />
                            <telerik:RadTextBox runat="server" ID="txtUserName" ReadOnly="true" ValidationGroup="OperatorValidation" Width="201px"
                                Label="Nome:" LabelWidth="51px" Height="34px" />
                            <asp:RequiredFieldValidator ID="OperatorNameRequiredFieldValidator" runat="server" ControlToValidate="txtUserName"
                                EnableClientScript="true" ValidationGroup="OperatorValidation" ForeColor="red" />
                            <br />
                            <telerik:RadComboBox runat="server" ID="ddlRole" RenderMode="Lightweight" ValidationGroup="OperatorValidation" Width="290px"
                                Label="Ruolo:" LabelCssClass="labelComboBox" Enabled="false" />
                            <%--<telerik:RadDropDownList runat="server" ID="ddlRole" RenderMode="Lightweight" ValidationGroup="OperatorValidation" />--%>
                            <asp:RequiredFieldValidator ID="RoleRequiredFieldValidator" runat="server" ControlToValidate="ddlRole"
                                EnableClientScript="true" ValidationGroup="OperatorValidation" ForeColor="red" InitialValue="" />

                            <br /><br />

                            <div class="row">
                                <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                                    <asp:HiddenField runat="server" ID="hiddenMachineState" />
                                    <asp:Label Text="Stato:" runat="server" Font-Bold="true" Width="85px" style="line-height:2" />
                                    <telerik:RadButton runat="server" ID="btnSetUp" Text="Attrezzaggio" OnClick="btnChangeMachineState_Click" />
                                    <telerik:RadButton runat="server" ID="btnProduction" Text="Produzione" OnClick="btnChangeMachineState_Click" />
                                    <telerik:RadButton runat="server" ID="btnMaintenance" Text="Manutenzione" OnClick="btnChangeMachineState_Click" />
                                </div>
                            </div>

                            <br /><br />

                            <asp:Panel runat="server" ID="pnlJob">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <asp:Label Text="Commessa:" runat="server" AssociatedControlID="txtCustomerOrder" Width="85px" style="line-height:2" />
                                        <telerik:RadButton runat="server" ID="txtCustomerOrder" Enabled="false" BackColor="White" ForeColor="Black" OnClick="txtCustomerOrder_Click" Width="201px" /> 
                                        <telerik:RadButton runat="server" ID="txtNStampateOra" Enabled="false" BackColor="White" ForeColor="Black" ReadOnly="true" />
                                        <telerik:RadButton runat="server" ID="btnSelectWorkshift" Text="Turni precedenti" Visible="false" BackColor="#dddddd" Font-Bold="true" BorderColor="Black" OnClick="btnSelectWorkshift_Click" />
                                        <asp:HiddenField runat="server" ID="hiddenWorkshiftSelected" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <asp:Label Text="Articolo:" runat="server" AssociatedControlID="txtArticleCode" Width="85px" style="line-height:2" />
                                        <telerik:RadButton runat="server" ID="txtArticleCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" />
                                        <telerik:RadButton runat="server" ID="txtArticleDescription" Width="304px" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <asp:Label Text="Stampo:" runat="server" AssociatedControlID="txtMold" Width="85px" style="line-height:2" />
                                        <telerik:RadButton runat="server" ID="txtMold" Enabled="false" BackColor="White" ForeColor="Black" Width="201px" OnClick="txtMold_Click" />
                                        <telerik:RadButton runat="server" ID="btnCavityMold" Enabled="false" BackColor="White" ForeColor="Black" OnClick="btnCavityMold_Click" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <asp:Label Text="MP:" runat="server" AssociatedControlID="txtMPCode" Width="85px" style="line-height:2" />
                                        <telerik:RadButton runat="server" ID="txtMPCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" />
                                        <telerik:RadButton runat="server" ID="txtMPDescription" Width="304px" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <telerik:RadTextBox runat="server" ID="txtBatchCode" Width="290px" Label="Lotto:" LabelWidth="83px" Height="34px" />
                                        <%--<asp:Label Text="Lotto:" runat="server" AssociatedControlID="txtBatchCode" Width="85px" style="line-height:2" />
                                        <telerik:RadButton runat="server" ID="txtBatchCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" />--%>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <%--<div class="col-xs-1 col-sm-1 col-md-2 col-lg-3"></div>--%>
                        <div class="col-xs-5 col-sm-5 col-md-4 col-lg-4">
                            <asp:Panel Width="330px" runat="server" Visible="true">
                                <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="7" OnClientClick="ModifyRegistrationId(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="8" OnClientClick="ModifyRegistrationId(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="9" OnClientClick="ModifyRegistrationId(this);return false;" />

                                <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="4" OnClientClick="ModifyRegistrationId(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="5" OnClientClick="ModifyRegistrationId(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="6" OnClientClick="ModifyRegistrationId(this);return false;" />

                                <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="1" OnClientClick="ModifyRegistrationId(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="2" OnClientClick="ModifyRegistrationId(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="3" OnClientClick="ModifyRegistrationId(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="194px" Height="60px" runat="server" Text="0" OnClientClick="ModifyRegistrationId(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyRegistrationId(this);return false;" />
                            </asp:Panel>
                        </div>
                    </div>

                    <%--</asp:Panel>--%>

                    <script type="text/javascript">
                        function ModifyRegistrationId(sender) {
                            var textBox = $find('<%= txtBadge.ClientID %>');
                            var txtValue = textBox.get_value();
                            var char = sender.value;//sender.get_text();
                            if (char == "Canc") {
                                if (txtValue.length > 0) {
                                    textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                                }
                            }
                            else {
                                if (char == ',' || char == '.') {
                                    if (txtValue.includes(',') || txtValue.includes('.')) { }
                                    else {
                                        if (txtValue.length == 0) {
                                            textBox.set_value('0' + char);
                                        }
                                        else {
                                            textBox.set_value(txtValue + char);
                                        }
                                    }
                                }
                                else {
                                    textBox.set_value(txtValue + char);
                                }
                            }
                        }

                        function Clear(sender, eventArgs) {
                            sender.set_value("");
                        }
                    </script>

                <%--<div class="row">
                    <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                        <asp:HiddenField runat="server" ID="hiddenMachineState" />
                        <asp:Label Text="Stato:" runat="server" Font-Bold="true" Width="85px" style="line-height:2" />
                        <telerik:RadButton runat="server" ID="btnSetUp" Text="Attrezzaggio" OnClick="btnChangeMachineState_Click" />
                        <telerik:RadButton runat="server" ID="btnProduction" Text="Produzione" OnClick="btnChangeMachineState_Click" />
                        <telerik:RadButton runat="server" ID="btnMaintenance" Text="Manutenzione" OnClick="btnChangeMachineState_Click" />
                    </div>
                </div>--%>

                <br /><br />

                <div class="row alDx">
                    <telerik:RadButton runat="server" ID="btnConfirmConnect" OnClick="btnConfirmConnect_Click" ValidationGroup="OperatorValidation">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton runat="server" ID="btnCancelConnect" CssClass="btnActivity" OnClick="btnCancelConnect_Click">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </Body>
</mcf:PopUpDialog>

    </ContentTemplate>
</asp:UpdatePanel>
 

    <mcf:PopUpDialog runat="server" ID="dlgSelectOrder" Title="Seleziona commessa" Width="100%" MarginLeft="0%">
        <Body>
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <telerik:RadButton ID="btnSearchOrder" runat="server" CssClass="btnActivity" OnClick="btnSearchOrder_Click" style="float:left;">
                                <ContentTemplate>
                                    <center><i class="fa fa-search" aria-hidden="true"></i> Altre commesse</center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadTextBox ID="txtSearchOrder" runat="server" EmptyMessage="Inserisci codice" Visible="false"
                                AutoPostBack="true" OnTextChanged="txtSearchOrder_TextChanged" />
                            <asp:Panel ID="pnlTastieraCommessa" Width="300px" runat="server" Visible="false" style="float:left;">
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="7" OnClientClick="ModifyOrderCode(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="8" OnClientClick="ModifyOrderCode(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="9" OnClientClick="ModifyOrderCode(this);return false;" />

                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="4" OnClientClick="ModifyOrderCode(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="5" OnClientClick="ModifyOrderCode(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="6" OnClientClick="ModifyOrderCode(this);return false;" />

                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="1" OnClientClick="ModifyOrderCode(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="2" OnClientClick="ModifyOrderCode(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="3" OnClientClick="ModifyOrderCode(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="174px" Height="50px" runat="server" Text="0" OnClientClick="ModifyOrderCode(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyOrderCode(this);return false;" />
                            </asp:Panel>
                            <script type="text/javascript">
                                    function ModifyOrderCode(sender) {
                                        var textBox = $find('<%= txtSearchOrder.ClientID %>');
                                        var txtValue = textBox.get_value();
                                        var char = sender.value;//sender.get_text();
                                        if (char == "Canc") {
                                            if (txtValue.length > 0) {
                                                textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                                            }
                                        }
                                        else {
                                            if (char == ',' || char == '.') {
                                                if (txtValue.includes(',') || txtValue.includes('.')) { }
                                                else {
                                                    if (txtValue.length == 0) {
                                                        textBox.set_value('0' + char);
                                                    }
                                                    else {
                                                        textBox.set_value(txtValue + char);
                                                    }
                                                }
                                            }
                                            else {
                                                textBox.set_value(txtValue + char);
                                            }
                                        }
                                    }
                                </script>
                            <div style="text-align:right; display:inline-block; float:right;">
                                <telerik:RadButton ID="btnConfirmSelectOrder" runat="server" CssClass="btnActivity" OnClick="btnConfirmSelectOrder_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                <telerik:RadButton ID="btnCancelSelectOrder" runat="server" CssClass="btnActivity" OnClick="btnCancelSelectOrder_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                    <br />
                    <telerik:RadGrid runat="server" ID="gridSelectOrder" RenderMode="Lightweight" AutoGenerateColumns="false"
                        OnNeedDataSource="gridSelectOrder_NeedDataSource">
                        <MasterTableView DataKeyNames="Id" Caption="" FilterExpression="" NoMasterRecordsText="Non ci sono commesse per questa macchina">
                            <Columns>
                                <telerik:GridBoundColumn DataField="Code" HeaderText="Fase">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Order.CustomerOrder.OrderCode" HeaderText="<%$ Resources:Mes,Order %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Order.Article.Code" HeaderText="<%$ Resources:Mes,Article %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="StartDate" HeaderText="Inizio previsto" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="Fine prevista" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridDateTimeColumn>
                                <%--<telerik:GridDropDownColumn DataField="OrderId" HeaderText="<%$ Resources:Mes,Order %>"
                                    DataSourceID="edsOrder" ListTextField="OrderCode" ListValueField="Id">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridDropDownColumn>
                                <telerik:GridDropDownColumn DataField="Order." HeaderText="<%$ Resources:Mes,Article %>"
                                    DataSourceID="edsArticle" ListTextField="Code" ListValueField="Id">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridDropDownColumn>--%>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>

    <mcf:PopUpDialog runat="server" ID="dlgSetCavityMoldNum" Title="Inserisci numero impronte" Width="100%" MarginLeft="0%">
        <Body>
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <telerik:RadTextBox ID="txtCavityMoldNum" runat="server" EmptyMessage="Inserisci n impronte" />
                            <asp:Panel Width="300px" runat="server" style="float:left;">
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="7" OnClientClick="ModifyNumCavity(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="8" OnClientClick="ModifyNumCavity(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="9" OnClientClick="ModifyNumCavity(this);return false;" />

                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="4" OnClientClick="ModifyNumCavity(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="5" OnClientClick="ModifyNumCavity(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="6" OnClientClick="ModifyNumCavity(this);return false;" />

                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="1" OnClientClick="ModifyNumCavity(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="2" OnClientClick="ModifyNumCavity(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="3" OnClientClick="ModifyNumCavity(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="174px" Height="50px" runat="server" Text="0" OnClientClick="ModifyNumCavity(this);return false;" />
                                <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyNumCavity(this);return false;" />
                            </asp:Panel>
                            <script type="text/javascript">
                                    function ModifyNumCavity(sender) {
                                        var textBox = $find('<%= txtCavityMoldNum.ClientID %>');
                                        var txtValue = textBox.get_value();
                                        var char = sender.value;//sender.get_text();
                                        if (char == "Canc") {
                                            if (txtValue.length > 0) {
                                                textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                                            }
                                        }
                                        else {
                                            if (char == ',' || char == '.') {
                                                if (txtValue.includes(',') || txtValue.includes('.')) { }
                                                else {
                                                    if (txtValue.length == 0) {
                                                        textBox.set_value('0' + char);
                                                    }
                                                    else {
                                                        textBox.set_value(txtValue + char);
                                                    }
                                                }
                                            }
                                            else {
                                                textBox.set_value(txtValue + char);
                                            }
                                        }
                                    }
                                </script>
                            <div style="text-align:right; display:inline-block; float:right;">
                                <telerik:RadButton ID="btnConfirmCavityMoldNum" runat="server" CssClass="btnActivity" OnClick="btnConfirmCavityMoldNum_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                <telerik:RadButton ID="btnCancelCavityMoldNum" runat="server" CssClass="btnActivity" OnClick="btnCancelCavityMoldNum_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>

    <mcf:PopUpDialog runat="server" ID="dlgSelectMold" Title="Seleziona stampo" Width="90%" MarginLeft="5%">
        <Body>
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <telerik:RadGrid runat="server" ID="gridSelectMold" RenderMode="Lightweight" AutoGenerateColumns="false"
                        OnNeedDataSource="gridSelectMold_NeedDataSource">
                        <MasterTableView DataKeyNames="MoldId" Caption="" FilterExpression="">
                            <Columns>
                                <telerik:GridDropDownColumn DataField="MoldId" HeaderText="Stampo"
                                    DataSourceID="edsMold" ListTextField="Code" ListValueField="Id">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridDropDownColumn>
                                <telerik:GridBoundColumn DataField="Article.Code" HeaderText="Codice Articolo">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Article.Description" HeaderText="Descrizione Articolo">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <br /><br />
                    <div class="row alDx">
                        <telerik:RadButton ID="btnConfirmSelectMold" runat="server" CssClass="btnActivity" OnClick="btnConfirmSelectMold_Click">
                            <ContentTemplate>
                                <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnCancelSelectMold" runat="server" CssClass="btnActivity" OnClick="btnCancelSelectMold_Click">
                            <ContentTemplate>
                                <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>

    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="LogoutWindow$btnConfirmLogout" />
            <asp:AsyncPostBackTrigger ControlID="dlgScarti$btnConfirmScarti" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="dlgScarti$btnCancelScarti" EventName="Click" />
            <asp:PostBackTrigger ControlID="dlgFermi$btnConfirmCausaleFermo" />
            <asp:PostBackTrigger ControlID="dlgFermi$btnCancelCausaleFermo" />
        </Triggers>
        <ContentTemplate>
    <mcf:PopUpDialog ID="dlgFermi" runat="server" Width="100%" MarginLeft="0%">
        <Body>
            <div class="row">
                <asp:UpdatePanel runat="server" ID="updatePnlFermi" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="causeTreeView" EventName="NodeClick" />
                    </Triggers>
                    <ContentTemplate>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <telerik:RadGrid runat="server" ID="gridFermi" RenderMode="Lightweight" AutoGenerateColumns="false" AllowMultiRowSelection="false"
                                OnItemDataBound="gridFermi_ItemDataBound">
                                <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeLabelText="Page Size:" />
                                <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id">
                                    <Columns>
                                        <telerik:GridDateTimeColumn DataField="Start" HeaderText="<%$ Resources:Mes,Start %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridDateTimeColumn DataField="End" HeaderText="<%$ Resources:Mes,End %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridTemplateColumn DataField="Duration" HeaderText="Durata[m]">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem,"Duration")) / 60, 0) %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:Mes,Status %>">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="<%$ Resources:Mes,Operator %>">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridDropDownColumn DataField="CauseId" HeaderText="<%$ Resources:Mes,Cause %>" UniqueName="ddlCause"
                                            ListTextField="Code" ListValueField="Id" DataSourceID="edsCause" DropDownControlType="RadComboBox"
                                            ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                                        </telerik:GridDropDownColumn>
                                        <telerik:GridBoundColumn DataField="Job.Order.Article.Code" HeaderText="<%$ Resources:Mes,Article %>">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True" ScrollHeight="650px" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <telerik:RadTreeView RenderMode="Lightweight" runat="Server" ID="causeTreeView" OnNodeClick="causeTreeView_NodeClick">
                            </telerik:RadTreeView>
                            <br /><br />
                            <div style="text-align:right">
                                <telerik:RadButton ID="btnConfirmCausaleFermo" runat="server" OnClick="btnConfirmCausaleFermo_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                <telerik:RadButton ID="btnCancelCausaleFermo" runat="server" CssClass="btnActivity" OnClick="btnCancelCausaleFermo_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </Body>
    </mcf:PopUpDialog>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <Triggers>
        </Triggers>
        <ContentTemplate>
    <mcf:PopUpDialog ID="dlgFermiManuali" runat="server" Width="100%" MarginLeft="0%">
        <Body>
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <telerik:RadGrid runat="server" ID="gridFermiManuali" RenderMode="Lightweight" AutoGenerateColumns="false">
                                <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridNumericColumn DataField="Duration" HeaderText="Durata [m]">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </telerik:GridNumericColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <br /><br />
                            <asp:Panel Width="330px" ID="Panel1" runat="server" Visible="true">
                                <telerik:RadTextBox runat="server" ID="txtInsDuration" ReadOnly="true" RenderMode="Lightweight" Width="278px" Visible="true">
                                    <ReadOnlyStyle BackColor="White" ForeColor="Black" />
                                </telerik:RadTextBox>
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="7" OnClientClick="ModifyDuration(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="8" OnClientClick="ModifyDuration(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="9" OnClientClick="ModifyDuration(this)" />

                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="4" OnClientClick="ModifyDuration(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="5" OnClientClick="ModifyDuration(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="6" OnClientClick="ModifyDuration(this)" />

                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="1" OnClientClick="ModifyDuration(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="2" OnClientClick="ModifyDuration(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="3" OnClientClick="ModifyDuration(this)" />
                                <asp:Button AutoPostBack="false" Width="184px" Height="60px" runat="server" Text="0" OnClientClick="ModifyDuration(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Text="," OnClientClick="ModifyDuration(this)" />
                                <asp:Button Width="184px" Height="60px" ID="btnOKFermo" runat="server" Primary="false" Text="Ok" OnClick="btnOKFermo_Click" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyDuration(this)" />
                            </asp:Panel>

                            <script type="text/javascript">
                                function ModifyDuration(sender) {
                                    var textBox = $find("<%= txtInsDuration.ClientID %>");
                                    var txtValue = textBox.get_value();
                                    var char = sender.value;//sender.get_text();
                                    if (char == "Canc") {
                                        if (txtValue.length > 0) {
                                            textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                                        }
                                    }
                                    else {
                                        if (char == ',' || char == '.') {
                                            if (txtValue.includes(',') || txtValue.includes('.')) { }
                                            else {
                                                if (txtValue.length == 0) {
                                                    textBox.set_value('0' + char);
                                                }
                                                else {
                                                    textBox.set_value(txtValue + char);
                                                }
                                            }
                                        }
                                        else {
                                            textBox.set_value(txtValue + char);
                                        }
                                    }
                                }
                            </script>

                            <br /><br /><br />
                            <div class="row" style="text-align:center;">
                                <telerik:RadButton ID="btnConfirmFermiManuali" runat="server" CssClass="btnActivity" OnClick="btnConfirmFermiManuali_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                <telerik:RadButton ID="btnCancelFermiManuali" runat="server" CssClass="btnActivity" OnClick="btnCancelFermiManuali_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <Triggers>
        </Triggers>
        <ContentTemplate>
    <mcf:PopUpDialog ID="dlgScarti" runat="server" Width="100%" MarginLeft="0%">
        <Body>
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <asp:HiddenField runat="server" ID="hiddenTransactionToWatch" />
                            <asp:HiddenField runat="server" ID="hiddenOperatorId" />
                            <asp:HiddenField runat="server" ID="hiddenCompileManualQty" />
                            <telerik:RadTextBox runat="server" ID="txtQtyProducedTot" ReadOnly="true" Label="Qta colpi totale:" LabelWidth="113px" Width="191px" style="text-align:right;" />
                            &nbsp; &nbsp; &nbsp;
                            <telerik:RadTextBox runat="server" ID="txtQtyShotProducedOperator" ReadOnly="true" Label="Qta colpi turno:" LabelWidth="111px" Width="189px" style="text-align:right;" />
                            &nbsp; &nbsp; &nbsp;
                            <telerik:RadTextBox runat="server" ID="txtQtyProducedOperator" ReadOnly="true" Label="Qta pezzi turno:" LabelWidth="112px" Width="190px" style="text-align:right;">
                                <ClientEvents OnFocus="Focus" />
                            </telerik:RadTextBox>
                            &nbsp; &nbsp; &nbsp;
                            <telerik:RadTextBox runat="server" ID="txtQtyWasteTransaction" ReadOnly="true" Label="Qta pezzi scarto:" LabelWidth="118px" Width="196px" style="text-align:right;" />
                            <br /><br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <telerik:RadGrid runat="server" ID="gridScarti" RenderMode="Lightweight" AutoGenerateColumns="false">
                                <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridNumericColumn DataField="Num" HeaderText="Operatore" DataFormatString="{0:n0}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </telerik:GridNumericColumn>
                                        <telerik:GridNumericColumn DataField="Quality" HeaderText="Qualità" DataFormatString="{0:n0}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </telerik:GridNumericColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <br /><br />
                            <asp:Panel Width="330px" ID="pnlTastieraNum" runat="server" Visible="true">
                                <telerik:RadTextBox runat="server" ID="txtInsValue" ReadOnly="true" RenderMode="Lightweight" Width="278px" Visible="true">
                                    <ReadOnlyStyle BackColor="White" ForeColor="Black" />
                                </telerik:RadTextBox>
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn7" runat="server" Text="7" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn8" runat="server" Text="8" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn9" runat="server" Text="9" OnClientClick="ModifyNumber(this)" />

                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn4" runat="server" Text="4" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn5" runat="server" Text="5" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn6" runat="server" Text="6" OnClientClick="ModifyNumber(this)" />

                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn1" runat="server" Text="1" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn2" runat="server" Text="2" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn3" runat="server" Text="3" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="184px" Height="60px" ID="btn0" runat="server" Text="0" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btnVir" runat="server" Text="," OnClientClick="ModifyNumber(this)" />
                                <asp:Button Width="184px" Height="60px" ID="btnNumPadOk" runat="server" Primary="false" Text="Ok" OnClick="btnNumPadOk_Click" />
                                <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btnCanc" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyNumber(this)" />
                                <asp:Button AutoPostBack="false" Width="278px" Height="60px" ID="btnRecuperoScarti" runat="server" Primary="false" Text="Recupero" OnClientClick="ModifyNumber(this)" />
                             </asp:Panel>

                            <script type="text/javascript">
                                function ModifyNumber(sender) {
                                    var textBox = $find("<%= txtInsValue.ClientID %>");
                                    var txtValue = textBox.get_value();
                                    var char = sender.value;//sender.get_text();
                                    if (char == "Recupero") {
                                        if (txtValue.includes('-')) { }
                                        else {
                                            textBox.set_value('-' + txtValue);
                                        }
                                    }
                                    else {
                                        if (char == "Canc") {
                                            if (txtValue.length > 0) {
                                                textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                                            }
                                        }
                                        else {
                                            if (char == ',' || char == '.') {
                                                if (txtValue.includes(',') || txtValue.includes('.')) { }
                                                else {
                                                    if (txtValue.length == 0) {
                                                        textBox.set_value('0' + char);
                                                    }
                                                    else {
                                                        textBox.set_value(txtValue + char);
                                                    }
                                                }
                                            }
                                            else {
                                                textBox.set_value(txtValue + char);
                                            }
                                        }
                                    }
                                }

                                function Focus(sender, eventArgs)
                                {
                                    document.getElementById("<%= hiddenCompileManualQty.ClientID %>").value = "True";
                                }
                            </script>

                            <br /><br /><br />
                            <div class="row" style="text-align:center;">
                                <telerik:RadButton ID="btnConfirmScarti" runat="server" CssClass="btnActivity" OnClick="btnConfirmScarti_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                <telerik:RadButton ID="btnCancelScarti" runat="server" CssClass="btnActivity" OnClick="btnCancelScarti_Click">
                                    <ContentTemplate>
                                        <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>
        </ContentTemplate>
    </asp:UpdatePanel>

    <mcf:PopUpDialog runat="server" ID="dlgSelectWorkshift" Title="Seleziona turno da controllare" Width="100%" MarginLeft="0%">
        <Body>
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <telerik:RadGrid runat="server" ID="gridWorkshift" RenderMode="Lightweight" AutoGenerateColumns="false"
                        OnNeedDataSource="gridWorkshift_NeedDataSource">
                        <MasterTableView DataKeyNames="Id" Caption="" FilterExpression="" NoMasterRecordsText="Non ci sono turni precedenti">
                            <Columns>
                                <telerik:GridBoundColumn DataField="Job.Order.CustomerOrder.OrderCode" HeaderText="<%$ Resources:Mes,Order %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="Operatore">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="DateStart" HeaderText="Inizio" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy HH:mm}">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="DateEnd" HeaderText="Fine" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy HH:mm}">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="Job.Order.CustomerOrder.QtyProduced" HeaderText="Quantità prodotta">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <br /><br />
                    <div class="row alDx">
                        <telerik:RadButton ID="btnConfirmSelectWorkshift" runat="server" CssClass="btnActivity" OnClick="btnConfirmSelectWorkshift_Click">
                            <ContentTemplate>
                                <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnCancelSelectWorkshift" runat="server" CssClass="btnActivity" OnClick="btnCancelSelectWorkshift_Click">
                            <ContentTemplate>
                                <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Body>
    </mcf:PopUpDialog>

    <mcf:PopUpDialog runat="server" ID="dlgCloseCustomerOrder" Title="Chiusura commessa" Width="90%" MarginLeft="5%">
        <Body>
            <asp:Label Text="La commessa è terminata?" runat="server" />
            <telerik:RadButton ID="btnConfirmCloseOrder" runat="server" Text="Si" OnClick="btnConfirmCloseOrder_Click"></telerik:RadButton>
            <telerik:RadButton ID="btnCancelCloseOrder" runat="server" Text="No" OnClick="btnCancelCloseOrder_Click"></telerik:RadButton>
        </Body>
    </mcf:PopUpDialog>

    <ef:EntityDataSource ID="edsCause" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Causes"
        OrderBy="it.Description" AutoGenerateWhereClause="true" />
    <ef:EntityDataSource ID="edsMold" runat="server" ContextTypeName="AssetManagement.Models.AssetManagementDbContext" EntitySetName="Molds"
        OrderBy="it.Description" AutoGenerateWhereClause="true" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>