﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web.Multitenants;
using MES.Configuration;
using MES.Core;
using MES.Helper;
using MES.Models;
using MES.Repositories;
using MES.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using static MES.Core.MesEnum;

namespace MainCloud.DashboardOperator
{
    public partial class MachinePage : BasePage
    {
        protected string AssetId
        {
            get
            {
                return Request.QueryString["asset"];
            }
        }

        private GrantOperator grantService = new GrantOperator();
        //private TransactionRepository transRepos = new TransactionRepository();
        private TransactionService transService = new TransactionService();
        private TransactionOperatorService transOperService = new TransactionOperatorService();
        private IndexService indexService = new IndexService();
        private static TimeSpan productionTime;
        private static decimal tempoPrevisto, tempoAssegnato, tempoRimasto;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(AssetId))
            {
                if (Session.SessionMultitenant().ContainsKey(MesConstants.DEPARTMENT_ZONE) && !string.IsNullOrEmpty(Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString()))
                {
                }
                else
                {
                    Response.Redirect("OperatorAccess.aspx");
                }
            }
            else
            {
                Response.Redirect("OperatorAccess.aspx");
            }

            edsMold.WhereParameters.Clear();
            edsMold.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
            edsCause.WhereParameters.Clear();
            edsCause.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);
            //edsMP.WhereParameters.Clear();
            //edsMP.WhereParameters.Add("ApplicationId", OperatorHelper.ApplicationId);

            ddlRole.EnableAjaxSkinRendering = true;
            gridOperator.EnableAjaxSkinRendering = true;
            gridFermi.EnableAjaxSkinRendering = true;
            causeTreeView.EnableAjaxSkinRendering = true;
            gridFermiManuali.EnableAjaxSkinRendering = true;
            txtInsValue.EnableAjaxSkinRendering = true;
            txtInsDuration.EnableAjaxSkinRendering = true;
            gridSelectOrder.EnableAjaxSkinRendering = true;

            //if (!IsPostBack)
            //{
            //    btnOperatorLogin_Click(btnOperatorLogin, EventArgs.Empty);
                //pnlPrimary.Visible = false;
                //pnlLogin.Visible = true;
            //}
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            LoadTitle();
            
            if (pnlPrimary.Visible)
            {
                RefreshPage();
            }
        }

        private void LoadTitle()
        {
            string zoneId = Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString();
            ZoneRepository zoneRep = new ZoneRepository();
            AssetRepository assetRep = new AssetRepository();
            OperatorRepository operRep = new OperatorRepository();
            TransactionService TService = new TransactionService();
            Page.Title = string.Format("{0} - {1} - Operatore: {2}", zoneRep.GetDescription(zoneId), assetRep.GetDescription(AssetId), operRep.GetUserName(TService.GetLastTransactionOpen(AssetId)?.OperatorId));
        }

        private void LoadOperatorButtonStyle()
        {
            if (transService.IsOperatorOnMachine(AssetId))
            {
                (btnOperatorLogin.Controls[0] as LiteralControl).Text = (btnOperatorLogin.Controls[0] as LiteralControl).Text.Replace("Inizio", "Cambio");
            }
            else
            {
                (btnOperatorLogin.Controls[0] as LiteralControl).Text = (btnOperatorLogin.Controls[0] as LiteralControl).Text.Replace("Cambio", "Inizio");
            }
            if (transOperService.OperatorsOnMachine(AssetId).Count > 0)
            {
                btnOperatorLogout.Enabled = true;
                if ((btnOperatorLogout.Controls[0] as LiteralControl).Text.Contains("</b>"))
                {
                    (btnOperatorLogout.Controls[0] as LiteralControl).Text = Regex.Replace((btnOperatorLogout.Controls[0] as LiteralControl).Text, "[0-9]{1,}", "").Replace("<b></b>", "");
                }
                (btnOperatorLogout.Controls[0] as LiteralControl).Text = (btnOperatorLogout.Controls[0] as LiteralControl).Text.Replace("Fine", string.Format("Fine <b>{0}</b>", transOperService.OperatorsOnMachine(AssetId).Count));
            }
            else
            {
                btnOperatorLogout.Enabled = false;
                if ((btnOperatorLogout.Controls[0] as LiteralControl).Text.Contains("</b>"))
                {
                    (btnOperatorLogout.Controls[0] as LiteralControl).Text = Regex.Replace((btnOperatorLogout.Controls[0] as LiteralControl).Text, "[0-9]{1,}", "").Replace("<b></b>", "");
                }
            }
        }

        private void LoadTestata()
        {
            Transaction lastTrans = transService.GetLastTransactionOpen(AssetId);
            if (lastTrans != null)
            {
                Color c = transService.GetCauseColorPerTransaction(lastTrans.Id, OperatorHelper.ApplicationId);
                if (lastTrans.Cause != null)
                {
                    switch (Convert.ToInt32(lastTrans.Cause.Code))
                    {
                        case (int)MesEnum.Cause.Production:
                            btnStateProd.BackColor = c;
                            break;
                        case (int)MesEnum.Cause.SetUp:
                            btnStateSetup.BackColor = c;
                            break;
                        case (int)MesEnum.Cause.Maintenance:
                            btnStateMan.BackColor = c;
                            break;
                    }
                }
                else
                {
                    btnStateStop.BackColor = c;
                }

                txtCustomerOrder.Text = lastTrans?.Job?.Order?.CustomerOrder?.OrderCode;
                txtCustomerOrder.Value = "";
                txtOrderQtyOrdered.Text = string.Format("{0:f0}", lastTrans?.Job?.QtyOrdered);
                decimal qtyProd = transService.GetQtyProducedPerJob(lastTrans?.JobId);
                txtOrderQtyProduced.Text = string.Format("{0:f0}", qtyProd);
                txtOrderQtyLeft.Text = string.Format("{0:f0}", lastTrans?.Job?.QtyOrdered - qtyProd);
                txtNStampateOra.Text = string.Format("Stampate ora = {0}", lastTrans?.Job?.StandardRate);
                txtNStampateOra.Value = lastTrans?.Job?.StandardRate.ToString();
                btnCavityMold.Text = string.Format("Impronte = {0}", lastTrans?.Job?.CavityMoldNum);
                btnCavityMold.Value = lastTrans?.Job?.CavityMoldNum.ToString();
                txtArticleCode.Text = lastTrans?.Job?.Order?.Article?.Code;
                txtArticleDescription.Text = lastTrans?.Job?.Order?.Article?.Description;

                MoldRepository moldRep = new MoldRepository();
                Mold mold = moldRep.FindByID(lastTrans?.MoldId);
                txtMold.Text = mold?.Name; //Code
            }
        }

        private void LoadGrantOperator()
        {
            TransactionService TService = new TransactionService();
            Transaction lastTrans = TService.GetLastTransactionOpen(AssetId);

            SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();

            btnScarti.Enabled = grantService.AllowWaste(lastTrans?.OperatorId) && boxService.IsMachineConnected(AssetId);
            SetCssClass(btnQualità);

            btnFermi.Enabled = grantService.AllowProduction(lastTrans?.OperatorId) && boxService.IsMachineConnected(AssetId);
            SetCssClass(btnScarti);

            txtCustomerOrder.Enabled = grantService.AllowSelectJob(lastTrans?.OperatorId);
            SetCssClass(btnFermi);

            //btnOEE.Enabled = grantService.AllowOEE(lastTrans.OperatorId);

            updateBtnQualità();
        }

        private void updateBtnQualità()
        {
            btnQualità.Enabled = isControlPlanRequired();
            SetCssClass(btnQualità);
        }

        private bool isControlPlanRequired()
        {
            try
            {
                Transaction lastTrans = transService.GetLastTransactionOpen(AssetId);

                if (lastTrans?.Job != null && lastTrans?.Job?.ControlPlan != null)
                {
                    ControlPlanDetailExecutedRepository cpdeRep = new ControlPlanDetailExecutedRepository();
                    ControlPlanDetailExecuted latestCpde = cpdeRep.GetLatestByJobId(lastTrans?.Job.Id);

                    TimeSpan latestCpdeProductionTime = new TimeSpan(0);
                    decimal qtyOld = 0;

                    if (latestCpde != null)
                    {
                        latestCpdeProductionTime = new TimeSpan(latestCpde.ProductionTime);
                        qtyOld = latestCpde.QtyProduced;
                    }

                    bool cpQtyFrequencyFlag = (bool)lastTrans?.Job?.ControlPlan?.QtyFrequencyFlag;

                    if (cpQtyFrequencyFlag)
                    {
                        // Controllo sul numero di pezzi prodotti dall'ultimo controllo
                        decimal qtyFreq = (decimal)lastTrans?.Job?.ControlPlan?.QtyFrequency,
                                qtyCurrent = transService.GetQtyProducedPerJob(lastTrans?.JobId),
                                qtyDiff = qtyCurrent - qtyOld,
                                qtyTotal = (decimal)lastTrans?.Job?.Order?.QtyOrdered;

                        var positionPerc = Math.Round(((qtyCurrent + (qtyFreq - qtyDiff)) / qtyTotal) * 100, 0);
                        if (positionPerc >= 0 && positionPerc <= 100)
                        {
                            controlplanMilestoneDot.Attributes["class"] = "milestones milestone__" + positionPerc;
                            controlplanMilestoneLabel.Attributes["class"] = "milestones milestone__" + positionPerc;
                            controlplanMilestoneDot.Visible = true;
                            controlplanMilestoneLabel.Visible = true;
                        }
                        else
                        {
                            controlplanMilestoneDot.Visible = false;
                            controlplanMilestoneLabel.Visible = false;
                        }

                        return (qtyDiff >= qtyFreq);
                    }
                    else
                    {
                        // Controllo sul tempo trascorso dall'ultimo controllo
                        TimeSpan tf = new TimeSpan((long)lastTrans?.Job?.ControlPlan?.TimeFrequency);

                        TimeSpan freqTime = new TimeSpan(0);
                        freqTime = freqTime.Add(tf);

                        TimeSpan newTime = latestCpdeProductionTime.Add(freqTime);
                        TimeSpan diffTime = productionTime - latestCpdeProductionTime;

                        var positionPerc = Math.Round((decimal)(productionTime.TotalHours + (freqTime - diffTime).TotalHours) / tempoPrevisto * 100, 0);
                        if (positionPerc >= 0 && positionPerc <= 100)
                        {
                            controlplanMilestoneDot.Attributes["class"] = "milestones milestone__" + positionPerc;
                            controlplanMilestoneLabel.Attributes["class"] = "milestones milestone__" + positionPerc;
                            controlplanMilestoneDot.Visible = true;
                            controlplanMilestoneLabel.Visible = true;
                        }
                        else
                        {
                            controlplanMilestoneDot.Visible = false;
                            controlplanMilestoneLabel.Visible = false;
                        }

                        return (productionTime >= newTime);
                    }
                }
            }
            catch (Exception ex) { }

            controlplanMilestoneDot.Visible = false;
            controlplanMilestoneLabel.Visible = false;

            return false;
        }

        private void RefreshPage()
        {
            LoadOperatorButtonStyle();
            LoadTestata();
            LoadGrantOperator();
            CreateIndexes();
        }

        private void SetCssClass(RadButton button)
        {
            if (button.Enabled)
            {
                button.CssClass = "btnActivity";
            }
            else
            {
                button.CssClass = "btnActivityDisabled";
            }
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            Response.Redirect("OperatorAccess.aspx");
        }

        #region Operator buttons
        protected void btnOperatorLogin_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlLogin.Visible = true;

            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            Transaction lastTransOpen = transService.GetLastTransactionOpen(AssetId);

            ModifyMachineTransaction.Value = bool.TrueString;

            btnSetUp.Enabled = false;
            btnSetUp.CssClass = "btnActivityDisabled";
            btnProduction.Enabled = false;
            btnProduction.CssClass = "btnActivityDisabled";
            btnMaintenance.Enabled = false;
            btnMaintenance.CssClass = "btnActivityDisabled";

            btnConfirmConnect.Enabled = false;
            btnConfirmConnect.CssClass = "btnActivityDisabled";

            txtCustomerOrder_Login.Enabled = false;
            btnCavityMold_Login.Enabled = false;
            txtMold_Login.Enabled = false;

            txtBadge.Text = transService.GetLastOperatorOnMachine(AssetId)?.Badge;
            txtBadge_TextChanged(txtBadge, EventArgs.Empty);

            //if (transService.GetLastOperatorOnMachine(AssetId) != null)
            //{
            //    btnConfirmConnect.Text = "Riprendi";
            //}

            txtCustomerOrder_Login.Text = lastTransOpen?.Job?.Order?.CustomerOrder?.OrderCode;
            txtCustomerOrder_Login.Value = "";
            txtNStampateOra_Login.Text = string.Format("Stampate ora = {0}", lastTransOpen?.Job?.StandardRate);
            txtNStampateOra_Login.Value = lastTransOpen?.Job?.StandardRate.ToString();
            btnCavityMold_Login.Text = string.Format("Impronte = {0}", lastTransOpen?.Job?.CavityMoldNum);
            btnCavityMold_Login.Value = lastTransOpen?.Job?.CavityMoldNum.ToString();
            txtArticleCode_Login.Text = lastTransOpen?.Job?.Order?.Article?.Code;
            txtArticleDescription_Login.Text = lastTransOpen?.Job?.Order?.Article?.Description;

            ArticleMoldService AMS = new ArticleMoldService();
            if (!string.IsNullOrEmpty(lastTransOpen?.MoldId))
            {
                txtMold_Login.Text = AMS.GetMoldById(lastTransOpen?.MoldId).Code;
            }
            txtMold_Login.Value = "";

            //OperatorService OS = new OperatorService();
            //if (!string.IsNullOrEmpty(lastTransOpen?.OperatorId))
            //{
            //    ConnectWindow.Title = string.Format("Macchina: {0} - Operatore attuale: {1}", AS.GetAssedById(AssetId, OperatorHelper.ApplicationId).Description, OS.GetOperatorNameById(lastTransOpen.OperatorId, OperatorHelper.ApplicationId));
            //}
            //else
            //{
            //    ConnectWindow.Title = string.Format("Macchina: {0}", AS.GetAssedById(AssetId, OperatorHelper.ApplicationId).Description);
            //}
            //ConnectWindow.OpenDialog();
        }

        protected void btnOperatorLogout_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlLogout.Visible = true;

            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();

            btnConfirmLogout.Enabled = false;
            btnConfirmLogout.CssClass = "btnActivityDisabled";

            hiddenQualityLevelValue.Value = null;
            pnlAllowConfirmProduction.Visible = false;

            //LogoutWindow.Title = string.Format("Logout - Macchina: {0}", AS.GetAssedById(AssetId, OperatorHelper.ApplicationId).Description);
            //LogoutWindow.OpenDialog();
            gridOperator.Rebind();
        }

        protected void btnAddOperator_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlLogin.Visible = true;

            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            Transaction lastTransOpen = transService.GetLastTransactionOpen(AssetId);

            ModifyMachineTransaction.Value = bool.FalseString;

            btnSetUp.Enabled = false;
            btnSetUp.CssClass = "btnActivityDisabled";
            btnProduction.Enabled = false;
            btnProduction.CssClass = "btnActivityDisabled";
            btnMaintenance.Enabled = false;
            btnMaintenance.CssClass = "btnActivityDisabled";

            txtCustomerOrder_Login.Text = lastTransOpen?.Job?.Order?.CustomerOrder?.OrderCode;
            txtCustomerOrder_Login.Value = "";
            txtCustomerOrder_Login.Enabled = false;
            txtNStampateOra_Login.Text = string.Format("Stampate ora = {0}", lastTransOpen?.Job?.StandardRate);
            txtNStampateOra_Login.Value = "";
            btnCavityMold_Login.Text = string.Format("Impronte = {0}", lastTransOpen?.Job?.CavityMoldNum);
            btnCavityMold_Login.Value = "";
            btnCavityMold_Login.Enabled = false;
            txtArticleCode_Login.Text = lastTransOpen?.Job?.Order?.Article?.Code;
            txtArticleDescription_Login.Text = lastTransOpen?.Job?.Order?.Article?.Description;

            ArticleMoldService AMS2 = new ArticleMoldService();
            if (!string.IsNullOrEmpty(lastTransOpen?.MoldId))
            {
                txtMold_Login.Text = AMS2.GetMoldById(lastTransOpen?.MoldId).Code;
            }
            txtMold_Login.Value = "";
            btnConfirmConnect.Enabled = false;
            btnConfirmConnect.CssClass = "btnActivityDisabled";
            //ConnectWindow.Title = string.Format("Operatore Aggiuntivo - Macchina: {0}", AS.GetAssedById(AssetId, OperatorHelper.ApplicationId).Description);
            //ConnectWindow.OpenDialog();
        }
        #endregion

        #region LoginOperator
        protected void txtBadge_TextChanged(object sender, EventArgs e)
        {
            //Controlla se esiste la matricola
            //Se esiste mostro il nome riferito alla matricola e il ruolo
            OperatorRepository OR = new OperatorRepository();
            string s = (sender as RadTextBox).Text.Trim();
            Operator op = OR.ReadAll(x => x.Badge == s && x.ApplicationId == OperatorHelper.ApplicationId).FirstOrDefault();
            if (op != null)
            {
                txtRegistrationId.Text = op.Code;
                txtUserName.Text = op.UserName;

                ddlRole.Items.Clear();
                if (grantService.AllowSetup(op.Id))
                {
                    btnSetUp.Enabled = true;
                    btnSetUp.CssClass = "btnActivity";
                    ddlRole.Items.Add("Attrezzaggio");
                }
                if (grantService.AllowMaintenance(op.Id))
                {
                    btnMaintenance.Enabled = true;
                    btnMaintenance.CssClass = "btnActivity";
                    ddlRole.Items.Add("Manutenzione");
                }
                if (grantService.AllowProduction(op.Id))
                {
                    btnProduction.Enabled = true;
                    btnProduction.CssClass = "btnActivity";
                    ddlRole.Items.Add("Produzione");
                }

                if (ddlRole.Items.Count == 1)
                {
                    if (btnSetUp.Enabled)
                    {
                        btnSetUp.CssClass = "setUpColor";
                        btnSetUp.Enabled = false;
                        hiddenMachineState.Value = "Attrezzaggio";
                    }
                    else
                    {
                        if (btnProduction.Enabled)
                        {
                            btnProduction.CssClass = "productionColor";
                            btnProduction.Enabled = false;
                            hiddenMachineState.Value = "Produzione";
                        }
                        else
                        {
                            if (btnMaintenance.Enabled)
                            {
                                btnMaintenance.CssClass = "maintenanceColor";
                                btnMaintenance.Enabled = false;
                                hiddenMachineState.Value = "Manutenzione";
                            }
                        }
                    }
                }

                ddlRole.Items.Clear();
                OperatorRoleService opRoleService = new OperatorRoleService();
                ddlRole.Items.Add(opRoleService.FindUserRoleCurrentApp(op).Name);
                //ddlRole.Enabled = ddlRole.Items.Count == 1 ? false : true;

                if (grantService.AllowSelectJob(op.Id) && ModifyMachineTransaction.Value == bool.TrueString)
                {
                    txtCustomerOrder_Login.Enabled = true;
                }
                if ((grantService.AllowSelectJob(op.Id) || grantService.AllowProduction(op.Id)) && ModifyMachineTransaction.Value == bool.TrueString)
                {
                    btnCavityMold_Login.Enabled = true;
                }
                if (grantService.AllowSelectMold(op.Id) && ModifyMachineTransaction.Value == bool.TrueString)
                {
                    txtMold_Login.Enabled = true;
                }
                //if (grantService.AllowSelectMP(op.Id))
                //{
                //}

                if (ModifyMachineTransaction.Value == bool.FalseString && grantService.AllowQuality(op.Id))
                {
                    btnSelectWorkshift_Login.Visible = true;
                }

                btnConfirmConnect.Enabled = true;
                btnConfirmConnect.CssClass = "btnActivity";
            }
            else
            {
                txtRegistrationId.Text = "";
                txtUserName.Text = "";
                ddlRole.Items.Clear();
                ddlRole.Items.Insert(0, "");
                ddlRole.Enabled = false;
                txtCustomerOrder_Login.Enabled = false;
                btnCavityMold_Login.Enabled = false;
                txtMold_Login.Enabled = false;
                btnSetUp.Enabled = false;
                btnSetUp.CssClass = "btnActivityDisabled";
                btnProduction.Enabled = false;
                btnProduction.CssClass = "btnActivityDisabled";
                btnMaintenance.Enabled = false;
                btnMaintenance.CssClass = "btnActivityDisabled";
                btnConfirmConnect.Enabled = false;
                btnConfirmConnect.CssClass = "btnActivityDisabled";
            }
        }

        protected void btnConfirmConnect_Click(object sender, EventArgs e)
        {
            //// Validate the operator password
            OperatorService OS = new OperatorService();
            //if (OS.ValidateOperatorAuthentication(txtOperatorSN.Text, txtOperatorPIN.Text == "" ? null : txtOperatorPIN.Text, OperatorHelper.ApplicationId))
            //{
            string operatorId = OS.GetOperatorByRegisterId(txtRegistrationId.Text, OperatorHelper.ApplicationId).Id;

            KeyValuePair<MesEnum.TransactionParam, object>? cause = null;
            //switch (ddlRole.SelectedItem.Text)
            switch (hiddenMachineState.Value)
            {
                case "Attrezzaggio":
                    cause = new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, MesEnum.Cause.SetUp);
                    break;
                case "Manutenzione":
                    cause = new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, MesEnum.Cause.Maintenance);
                    break;
                case "Produzione":
                    cause = new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, MesEnum.Cause.Production);
                    break;
            }

            if (!transOperService.IsOperatorOnMachine(AssetId, operatorId))
            {
                string jobId = !string.IsNullOrEmpty(txtCustomerOrder_Login.Value) ? txtCustomerOrder_Login.Value : transService.GetLastTransactionOpen(AssetId)?.JobId;
                transOperService.OpenTransaction(AssetId, operatorId, MesEnum.OperationName.LogIn, jobId);
                TransactionRepository transRepos = new TransactionRepository();
                if (!string.IsNullOrEmpty(hiddenWorkshiftSelected.Value))
                {
                    //è stato selezionato un turno precedente per il controllo qualità
                    string jobIdQuality = transRepos.GetJobId(Convert.ToInt32(hiddenWorkshiftSelected.Value));
                    transOperService.OpenTransaction(AssetId, operatorId, MesEnum.OperationName.Qualità, jobIdQuality, Convert.ToInt32(hiddenWorkshiftSelected.Value));
                }
                else
                {
                    if (ModifyMachineTransaction.Value == bool.FalseString && grantService.AllowQuality(operatorId))
                    {
                        //non è stato selezionato un turno precedente per il controllo qualità ma è un operatore qualità
                        Transaction tQuality = transService.GetLastProdTransaction(AssetId, OperatorHelper.ApplicationId);
                        transOperService.OpenTransaction(AssetId, operatorId, MesEnum.OperationName.Qualità, tQuality.JobId, tQuality.Id);
                    }
                }
                //switch (ddlRole.SelectedItem.Text)
                switch (hiddenMachineState.Value)
                {
                    case "Attrezzaggio":
                        transOperService.OpenTransaction(AssetId, operatorId, MesEnum.OperationName.Attrezzaggio, jobId);
                        break;
                    case "Manutenzione":
                        transOperService.OpenTransaction(AssetId, operatorId, MesEnum.OperationName.Manutenzione, jobId);
                        break;
                }
                //TODO:verificare che non ci siano transazioni di attrezzaggio o manutenzione aperte, in caso chiuderle
            }

            if (ModifyMachineTransaction.Value == bool.TrueString)
            {
                if (!string.IsNullOrEmpty(txtCustomerOrder_Login.Value))
                {
                    if (cause.HasValue)
                    {
                        transService.CloseAndOpenTransaction(
                        AssetId,
                        new KeyValuePair<MesEnum.TransactionParam, object>[] {
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, operatorId),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, txtCustomerOrder_Login.Value),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.StandardRate, txtNStampateOra_Login.Value),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.CavityMoldNum, btnCavityMold_Login.Value),
                        cause.Value
                        });
                    }
                    else
                    {
                        transService.CloseAndOpenTransaction(
                        AssetId,
                        new KeyValuePair<MesEnum.TransactionParam, object>[] {
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, operatorId),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Job, txtCustomerOrder_Login.Value),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.StandardRate, txtNStampateOra_Login.Value),
                        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.CavityMoldNum, btnCavityMold_Login.Value)
                        });
                    }
                }
                else
                {
                    //il job non è cambiato
                    if (cause.HasValue)
                    {
                        transService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, operatorId),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.StandardRate, txtNStampateOra_Login.Value),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.CavityMoldNum, btnCavityMold_Login.Value),
                            cause.Value
                            });
                    }
                    else
                    {
                        transService.CloseAndOpenTransaction(
                            AssetId,
                            new KeyValuePair<MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, operatorId),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.StandardRate, txtNStampateOra_Login.Value),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.CavityMoldNum, btnCavityMold_Login.Value)
                            });
                    }
                }
            }

            txtBadge.Text = "";
            txtRegistrationId.Text = "";
            txtUserName.Text = "";
            ddlRole.Items.Clear();
            txtCustomerOrder_Login.Value = null;
            txtNStampateOra_Login.Value = null;
            btnCavityMold_Login.Value = null;
            txtMold_Login.Value = null;
            ModifyMachineTransaction.Value = null;
            btnSelectWorkshift_Login.Visible = false;
            hiddenWorkshiftSelected.Value = null;
            //RefreshPage();
            //ConnectWindow.CloseDialog();
            pnlLogin.Visible = false;
            pnlPrimary.Visible = true;
        }

        protected void btnCancelConnect_Click(object sender, EventArgs e)
        {
            txtBadge.Text = "";
            txtRegistrationId.Text = "";
            txtUserName.Text = "";
            ddlRole.Items.Clear();
            txtCustomerOrder_Login.Value = null;
            txtNStampateOra_Login.Value = null;
            btnCavityMold_Login.Value = null;
            txtMold_Login.Value = null;
            ModifyMachineTransaction.Value = null;
            btnSelectWorkshift_Login.Visible = false;
            hiddenWorkshiftSelected.Value = null;
            //RefreshPage();
            //ConnectWindow.CloseDialog();
            pnlLogin.Visible = false;
            pnlPrimary.Visible = true;
        }

        //protected void btnChangeMachineState_Click(object sender, EventArgs e)
        //{
        //    hiddenMachineState.Value = (sender as RadButton).Text;
        //    switch ((sender as RadButton).Text)
        //    {
        //        case "Attrezzaggio":
        //            (sender as RadButton).CssClass = "setUpColor";
        //            if (btnProduction.Enabled)
        //            {
        //                btnProduction.CssClass = "btnActivity";
        //            }
        //            if (btnMaintenance.Enabled)
        //            {
        //                btnMaintenance.CssClass = "btnActivity";
        //            }
        //            break;
        //        case "Produzione":
        //            (sender as RadButton).CssClass = "productionColor";
        //            if (btnMaintenance.Enabled)
        //            {
        //                btnMaintenance.CssClass = "btnActivity";
        //            }
        //            if (btnSetUp.Enabled)
        //            {
        //                btnSetUp.CssClass = "btnActivity";
        //            }
        //            break;
        //        case "Manutenzione":
        //            (sender as RadButton).CssClass = "maintenanceColor";
        //            if (btnProduction.Enabled)
        //            {
        //                btnProduction.CssClass = "btnActivity";
        //            }
        //            if (btnSetUp.Enabled)
        //            {
        //                btnSetUp.CssClass = "btnActivity";
        //            }
        //            break;
        //    }
        //}

        protected void gridWorkshift_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //if (dlgSelectWorkshift.IsOpen())
            //{
            List<Transaction> dataSource = transService.GetProdTransactionForQuality(AssetId, OperatorHelper.ApplicationId);
            gridWorkshift.DataSource = dataSource;

            ////gridWorkshift.DataSource = transOperService.PreviousWorkshift(AssetId);
            ////List<TransactionOperator> list = transOperService.PreviousWorkshift(AssetId);
            ////List<TransactionOperator> dataSource = new List<TransactionOperator>();
            ////TransactionRepository repos = new TransactionRepository();
            ////foreach (var item in list)
            ////{
            ////    decimal tot = repos.ReadAll(x => x.MachineId == item.MachineId &&
            ////                                      x.JobId == item.JobId &&
            ////                                      x.OperatorId == item.OperatorId &&
            ////                                      x.Start >= item.DateStart)
            ////                   .Select(x => x.PartialCounting)
            ////                   .DefaultIfEmpty(0)
            ////                   .Sum();
            ////    if (tot > 0)
            ////    {
            ////        item.Job.Order.CustomerOrder.QtyProduced = tot;
            ////        dataSource.Add(item);
            ////    }
            ////}
            ////gridWorkshift.DataSource = dataSource;
            //}
        }

        protected void btnSelectWorkshift_Login_Click(object sender, EventArgs e)
        {
            pnlLogin.Visible = false;
            pnlSelectWorkshift.Visible = true;
            //dlgSelectWorkshift.OpenDialog();
            gridWorkshift.Rebind();
        }

        protected void btnCancelSelectWorkshift_Click(object sender, EventArgs e)
        {
            //dlgSelectWorkshift.CloseDialog();
            pnlSelectWorkshift.Visible = false;
            pnlLogin.Visible = true;
        }

        protected void btnConfirmSelectWorkshift_Click(object sender, EventArgs e)
        {
            if (gridWorkshift.SelectedItems.Count > 0)
            {
                GridDataItem item = (GridDataItem)gridWorkshift.SelectedItems[0]; //get selected row
                string transId = item.GetDataKeyValue("Id").ToString();
                hiddenWorkshiftSelected.Value = transId;
            }

            //dlgSelectWorkshift.CloseDialog();
            pnlSelectWorkshift.Visible = false;
            pnlLogin.Visible = true;
        }
        #endregion

        #region gridOperator
        protected void gridOperator_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //if (LogoutWindow.IsOpen())
            //{
            gridOperator.DataSource = transOperService.OperatorsOnMachine(AssetId);// dataSource;
            //}
        }

        protected void gridOperator_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;

                if (!string.IsNullOrEmpty(item["OperatorId"].Text))
                {
                    OperatorService OperService = new OperatorService();
                    string opId = item["OperatorId"].Text;
                    item["OperatorId"].Text = OperService.GetOperatorNameById(opId, OperatorHelper.ApplicationId);
                    if ((gridOperator.DataSource as List<TransactionOperator>).Count == 1)
                    {
                        //seleziona la prima riga
                        item.FireCommandEvent("Select", new GridSelectCommandEventArgs(item, null, null));
                    }
                }
                if (!string.IsNullOrEmpty(item["RoleId"].Text) && item["RoleId"].Text != "&nbsp;")
                {
                    OperatorRoleService operRoleService = new OperatorRoleService();
                    item["RoleId"].Text = operRoleService.FindRoleById(item["RoleId"].Text).Name;
                }
            }
        }

        protected void gridOperator_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.SelectCommandName)
            {
                string operatorId = (e.Item as GridDataItem).GetDataKeyValue("OperatorId").ToString();
                if (grantService.AllowConfirmStartUpProduction(operatorId))
                {
                    pnlAllowConfirmProduction.Visible = true;

                    SetTrafficLight(AssetId, operatorId);
                }
                else
                {
                    pnlAllowConfirmProduction.Visible = false;
                }
            }
        }

        private void SetTrafficLight(string machineId, string operatorId)
        {
            QualityLevelService qlService = new QualityLevelService();
            TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
            MES.Models.QualityLevel ql = qlService.GetQualityLevel(
                machineId,
                transOperRepos.ReadAll(x => x.MachineId == machineId && x.OperatorId == operatorId && x.Open && x.OperationName == "LogIn").ToList().First().JobId);

            redLight.Attributes["class"] = redLight.Attributes["class"].Replace(" on", "");
            yellowLight.Attributes["class"] = yellowLight.Attributes["class"].Replace(" on", "");
            greenLight.Attributes["class"] = greenLight.Attributes["class"].Replace(" on", "");
            if (ql != null)
            {
                switch (ql.Level)
                {
                    case "Stop":
                        redLight.Attributes["class"] = redLight.Attributes["class"] + " on";
                        break;
                    case "Warning":
                        yellowLight.Attributes["class"] = yellowLight.Attributes["class"] + " on";
                        break;
                    case "Ok":
                        greenLight.Attributes["class"] = greenLight.Attributes["class"] + " on";
                        break;
                }
            }
        }

        protected void gridOperator_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (gridOperator.SelectedItems.Count > 0)
                {
                    GridDataItem item = (GridDataItem)gridOperator.SelectedItems[0];//get selected row
                    string operatorId = item.GetDataKeyValue("OperatorId").ToString();
                    if (grantService.AllowConfirmStartUpProduction(operatorId))
                    {
                        pnlAllowConfirmProduction.Visible = true;

                        SetTrafficLight(AssetId, operatorId);
                    }
                    else
                    {
                        pnlAllowConfirmProduction.Visible = false;
                    }
                }
            }
            catch (ArgumentOutOfRangeException)
            { }
            btnConfirmLogout.Enabled = true;
            btnConfirmLogout.CssClass = "btnActivity";
        }
        #endregion

        #region Select Mold
        protected void txtMold_Login_Click(object sender, EventArgs e)
        {
            pnlLogin.Visible = false;
            pnlMold.Visible = true;
            //dlgSelectMold.OpenDialog();
            gridSelectMold.Rebind();
        }

        protected void gridSelectMold_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //if (dlgSelectMold.IsOpen())
            //{
            ArticleMoldService AMS = new ArticleMoldService();
            string jobId = !string.IsNullOrEmpty(txtCustomerOrder_Login.Value) ? txtCustomerOrder_Login.Value : transService.GetLastTransactionOpen(AssetId)?.JobId;
            gridSelectMold.DataSource = AMS.GetMoldListByJobId(jobId, OperatorHelper.ApplicationId); //se un job non è già stato scelto, il datasource sarà vuoto
            //}
        }

        protected void btnConfirmSelectMold_Click(object sender, EventArgs e)
        {
            if (gridSelectMold.SelectedItems.Count > 0)
            {
                GridDataItem item = (GridDataItem)gridSelectMold.SelectedItems[0];//get selected row
                string moldId = item.GetDataKeyValue("MoldId").ToString();
                ArticleMoldService AMS = new ArticleMoldService();
                txtMold_Login.Value = moldId;
                txtMold_Login.Text = AMS.GetMoldById(moldId).Code;
            }

            //dlgSelectMold.CloseDialog();
            pnlMold.Visible = false;
            pnlLogin.Visible = true;
        }

        protected void btnCancelSelectMold_Click(object sender, EventArgs e)
        {
            //dlgSelectMold.CloseDialog();
            pnlMold.Visible = false;
            pnlLogin.Visible = true;
        }
        #endregion

        #region SelectOrder
        protected void txtCustomerOrder_Login_Click(object sender, EventArgs e)
        {
            pnlLogin.Visible = false;
            pnlOrder.Visible = true;

            //dlgSelectOrder.OpenDialog();
            gridSelectOrder.Rebind();
        }

        protected void btnCancelSelectOrder_Click(object sender, EventArgs e)
        {
            pnlOrder.Visible = false;
            pnlLogin.Visible = true;

            //dlgSelectOrder.CloseDialog();
            txtSearchOrder.Text = "";
            txtSearchOrder.Visible = false;
            pnlTastieraCommessa.Visible = false;
        }

        protected void gridSelectOrder_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //if (dlgSelectOrder.IsOpen())
            //{
            JobService jobService = new JobService();
            if (txtSearchOrder.Visible && txtSearchOrder.Text.Length > 0)
            {
                gridSelectOrder.DataSource = jobService.GetJobListOrderCodeFiltered(txtSearchOrder.Text);
            }
            else
            {
                gridSelectOrder.DataSource = jobService.GetProductionJobListByAsset(AssetId);
            }
            //}
        }

        protected void btnConfirmSelectOrder_Click(object sender, EventArgs e)
        {
            if (gridSelectOrder.SelectedItems.Count > 0)
            {
                GridDataItem item = (GridDataItem)gridSelectOrder.SelectedItems[0];//get selected row
                string jobId = item.GetDataKeyValue("Id").ToString();
                txtCustomerOrder_Login.Value = jobId;
                JobRepository jobRep = new JobRepository();
                Job j = jobRep.FindByID(jobId);
                txtCustomerOrder_Login.Text = j?.Order?.CustomerOrder?.OrderCode;
                txtArticleCode_Login.Text = j?.Order?.Article?.Code;
                txtArticleDescription_Login.Text = j?.Order?.Article?.Description;
                txtNStampateOra_Login.Text = string.Format("Stampate ora = {0}", j?.StandardRate);
                txtNStampateOra_Login.Value = j?.StandardRate.ToString();
                btnCavityMold_Login.Text = string.Format("Impronte = {0}", j?.CavityMoldNum);
                btnCavityMold_Login.Value = j?.CavityMoldNum.ToString();

                ArticleMoldService AMS = new ArticleMoldService();
                if (AMS.GetMoldListByJobId(j.Id, OperatorHelper.ApplicationId).Count == 1)
                {
                    txtMold_Login.Value = AMS.GetMoldListByJobId(j.Id, OperatorHelper.ApplicationId)[0].MoldId;
                    txtMold_Login.Text = AMS.GetMoldById(txtMold_Login.Value).Code;
                }
            }

            //dlgSelectOrder.CloseDialog();
            txtSearchOrder.Text = "";
            txtSearchOrder.Visible = false;
            pnlTastieraCommessa.Visible = false;

            pnlOrder.Visible = false;
            pnlLogin.Visible = true;
        }

        protected void btnSearchOrder_Click(object sender, EventArgs e)
        {
            txtSearchOrder.Visible = true;
            pnlTastieraCommessa.Visible = true;
        }

        protected void txtSearchOrder_TextChanged(object sender, EventArgs e)
        {
            gridSelectOrder.Rebind();
        }
        #endregion

        #region ChangeCavityMoldNum
        protected void btnCavityMold_Login_Click(object sender, EventArgs e)
        {
            pnlLogin.Visible = false;
            pnlCavityMold.Visible = true;

            txtCavityMoldNum.Text = "";
            //dlgSetCavityMoldNum.OpenDialog();
        }

        protected void btnCancelCavityMoldNum_Click(object sender, EventArgs e)
        {
            txtCavityMoldNum.Text = "";
            //dlgSetCavityMoldNum.CloseDialog();

            pnlCavityMold.Visible = false;
            pnlLogin.Visible = true;
        }

        protected void btnConfirmCavityMoldNum_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCavityMoldNum.Text))
            {
                btnCavityMold_Login.Text = string.Format("Impronte = {0}", txtCavityMoldNum.Text);
                btnCavityMold_Login.Value = txtCavityMoldNum.Text;
            }
            txtCavityMoldNum.Text = "";
            //dlgSetCavityMoldNum.CloseDialog();
            pnlCavityMold.Visible = false;
            pnlLogin.Visible = true;
        }
        #endregion

        #region Logout
        protected void btnConfirmLogout_Click(object sender, EventArgs e)
        {
            string assetId = AssetId;
            OperatorService OS = new OperatorService();
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();
            GridDataItem item = (GridDataItem)gridOperator.SelectedItems[0];//get selected row
            string operatorId = item.GetDataKeyValue("OperatorId").ToString();

            if (!string.IsNullOrEmpty(hiddenQualityLevelValue.Value))
            {
                //dichiaro il livello qualità
                TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
                QualityLevelRepository qlRepos = new QualityLevelRepository();
                MES.Models.QualityLevel ql = new MES.Models.QualityLevel();
                ql.MachineId = assetId;
                ql.OperatorId = operatorId;
                ql.JobId = transOperRepos.ReadAll(x => x.MachineId == assetId && x.OperatorId == operatorId && x.Open && x.OperationName == "LogIn").ToList().First().JobId;
                switch (hiddenQualityLevelValue.Value)
                {
                    case "Stop":
                        ql.Level = MesEnum.QualityLevel.Stop.ToString();
                        break;
                    case "Warning":
                        ql.Level = MesEnum.QualityLevel.Warning.ToString();
                        break;
                    case "Ok":
                        ql.Level = MesEnum.QualityLevel.Ok.ToString();
                        break;
                }
                ql.Date = DateTime.Now;

                qlRepos.Insert(ql);
                qlRepos.SaveChanges();
            }

            //transOperService.CloseTransaction(assetId, operatorId, OperationName.LogIn);
            //if (transOperService.TransactionOpenPerOperator(assetId, operatorId))
            //{
            //    transOperService.CloseTransaction(assetId, operatorId, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
            //}

            //verifico che chi dichiara il logout sia lo stesso che è sulla macchina
            if (operatorId == transService.GetLastTransactionOpen(assetId)?.OperatorId)
            {
                CauseService causeService = new CauseService();
                SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
                if (causeService.GetListCauseIdByGroupName(new string[] { "Attrezzaggio", "Manutenzione" }, OperatorHelper.ApplicationId).Contains(transService.GetLastTransactionOpen(assetId).CauseId) || !boxService.IsMachineConnected(assetId))
                {
                    transService.CloseAndOpenTransaction(
                        assetId,
                        new KeyValuePair<MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, null),
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, null)
                        });
                }
                else
                {
                    transService.CloseAndOpenTransaction(
                        assetId,
                        new KeyValuePair<MesEnum.TransactionParam, object>[] {
                            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, null)
                        });
                }
            }

            //LogoutWindow.CloseDialog();
            pnlLogout.Visible = false;

            hiddenOperatorId.Value = operatorId;
            hiddenOpenFermi.Value = bool.TrueString;
            hiddenLogoutOperator.Value = bool.TrueString;

            //mostrare gli scarti se il ruolo lo consente
            if (grantService.AllowWaste(operatorId))
            {
                pnlScarti.Visible = true;

                TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
                string jobId = transOperRepos.ReadAll(x => x.MachineId == AssetId && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).FirstOrDefault()?.JobId;

                ///////variabili per turno///////////
                string operatorWorkshift;
                Transaction workshiftToWatch;
                List<Transaction> transactionWorkshiftList = new List<Transaction>();
                string jobIdWorkshift;
                List<JobProductionWaste> wasteWorkshift;
                /////////////////////////////////////

                TransactionRepository transRepos = new TransactionRepository();
                JobProductionWasteService jpwService = new JobProductionWasteService();
                TransactionOperator t = transOperRepos.ReadAll(x => x.MachineId == AssetId && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "Qualità").OrderByDescending(x => x.DateStart).FirstOrDefault();
                if (t != null)
                {
                    //controllo qualità
                    workshiftToWatch = transRepos.FindByID(t.QualityTransactionId);

                    if (string.IsNullOrEmpty(jobId) && string.IsNullOrEmpty(workshiftToWatch.JobId))
                    {
                        pnlScarti.Visible = false;
                        goto GoToFermi;
                    }

                    jobIdWorkshift = workshiftToWatch.JobId;
                    operatorWorkshift = workshiftToWatch.OperatorId;
                    //transactionWorkshiftList = transRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobIdWorkshift && x.OperatorId == workshiftToWatch.OperatorId && x.Start >= workshiftToWatch.DateStart && x.Start <= workshiftToWatch.DateEnd).ToList();
                    //wasteWorkshift = jpwService.GetWasteListByJobId(workshiftToWatch.JobId, OperatorHelper.ApplicationId);
                }
                else
                {
                    if (string.IsNullOrEmpty(jobId))
                    {
                        pnlScarti.Visible = false;
                        goto GoToFermi;
                    }

                    //dichiarazione scarti normale (non qualità) -> ultima riga di produzione
                    jobIdWorkshift = jobId;
                    operatorWorkshift = hiddenOperatorId.Value;
                    workshiftToWatch = transService.GetLastProdTransactionPerOperator(operatorWorkshift, jobIdWorkshift, OperatorHelper.ApplicationId);
                    if (workshiftToWatch == null)
                    {
                        workshiftToWatch = transService.GetLastTransactionPerOperator(assetId, operatorWorkshift, jobIdWorkshift);
                    }
                    //workshiftToWatch = transOperRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobIdWorkshift && x.OperatorId == operatorWorkshift && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First();
                    TransactionOperator transWorkshift = transOperRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobIdWorkshift && x.OperatorId == operatorWorkshift && x.OperationName == "LogIn" && x.DateStart <= workshiftToWatch.Start).OrderByDescending(x => x.DateStart).First();
                    transactionWorkshiftList = transRepos.ReadAll(x => x.MachineId == assetId && x.JobId == jobIdWorkshift && x.OperatorId == operatorWorkshift && x.Start >= transWorkshift.DateStart).ToList();
                    //wasteWorkshift = new List<JobProductionWaste>(); //jpwService.GetWasteListPerWorkshift(jobIdWorkshift, operatorWorkshift, workshiftToWatch.DateStart, workshiftToWatch.DateEnd.Value, OperatorHelper.ApplicationId);
                    //wasteWorkshift = jpwService.GetWasteListPerTransaction(transactionWorkshiftList, jobIdWorkshift, OperatorHelper.ApplicationId);
                }
                wasteWorkshift = jpwService.GetWasteListByJobId(workshiftToWatch.JobId, OperatorHelper.ApplicationId);

                hiddenTransactionToWatch.Value = workshiftToWatch.Id.ToString();

                if (!string.IsNullOrEmpty(jobIdWorkshift))
                {
                    List<Transaction> currentJobTList = transService.GetTransactionListByJobId(assetId, jobId);
                    if (currentJobTList != null && currentJobTList.Count > 0)
                    {
                        if (currentJobTList.Sum(x => x.PartialCounting) == 0)
                        {
                            btnNumPadOk.Enabled = false;
                            btnConfirmScarti.Enabled = false;
                        }
                        if (!grantService.AllowQuality(hiddenOperatorId.Value))
                        {
                            btnRecuperoScarti.Enabled = false;
                        }
                        else
                        {
                            btnRecuperoScarti.Enabled = true;
                            btnNumPadOk.Enabled = true;
                            btnConfirmScarti.Enabled = true;
                        }

                        //txtQtyProducedTot.Text = Math.Round(currentJobTList.Sum(x => x.PartialCounting), 0).ToString();
                        //txtQtyShotProducedOperator.Text = Math.Round(currentJobTList.Where(x => x.OperatorId == operatorId).Sum(x => x.PartialCounting), 0).ToString();
                        //txtQtyProducedOperator.Text = Math.Round(currentJobTList.Where(x => x.OperatorId == operatorId).Sum(x => x.PartialCounting) * numImpronte, 0).ToString();

                        txtQtyProducedTot.Text = Math.Round(currentJobTList.Sum(x => x.PartialCounting), 0).ToString();
                        txtQtyShotProducedOperator.Text = Math.Round(transactionWorkshiftList.Sum(x => x.PartialCounting), 0).ToString();
                        txtQtyProducedOperator.Text = Math.Round(transactionWorkshiftList.Sum(x => x.PartialCounting * x.CavityMoldNum), 0).ToString();
                        txtQtyWasteTransaction.Text = wasteWorkshift.Sum(x => x.QtyProductionWaste).ToString();

                        //JobProductionWasteService jpwService = new JobProductionWasteService();
                        //List<JobProductionWaste> wasteList = jpwService.GetWasteListByJobId(transService.GetLastTransactionOpen(assetId).JobId, OperatorHelper.ApplicationId);
                        //txtQtyWasteTransaction.Text = wasteList.Sum(x => x.QtyProductionWaste).ToString();

                        CauseTypeRepository CTR = new CauseTypeRepository();
                        List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == OperatorHelper.ApplicationId).ToList();
                        List<ScartiRecord> dataSource = new List<ScartiRecord>();
                        foreach (CauseType ct in causeList)
                        {
                            foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode))
                            {
                                if (grantService.AllowQuality(hiddenOperatorId.Value))
                                {
                                    //controllo qualità -> mostro gli scarti dichiarati in precedenza
                                    //if (wasteList.Where(x => x.CauseId == c.Id).Any())
                                    if (wasteWorkshift.Where(x => x.CauseId == c.Id).Any())
                                    {
                                        //dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteList.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste)));
                                        dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteWorkshift.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste), 0, 0));
                                    }
                                    else
                                    {
                                        dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                                    }
                                }
                                else
                                {
                                    dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                                }
                            }
                        }

                        gridScarti.DataSource = dataSource;
                        gridScarti.DataBind();

                        //dlgScarti.Title = string.Format("SCARTI - Macchina: {0} - Operatore: {1} - Commessa: {3} - Articolo: {2}",
                        //    AS.GetAssedById(assetId, OperatorHelper.ApplicationId).Description,
                        //    OS.GetOperatorNameById(operatorId, OperatorHelper.ApplicationId),
                        //    currentJobTList.First(x => !string.IsNullOrEmpty(x.JobId)).Job?.Order?.Article?.Code,
                        //    currentJobTList.First(x => !string.IsNullOrEmpty(x.JobId)).Job?.Order?.CustomerOrder?.OrderCode);

                        SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
                        txtQtyShotProducedOperator.ReadOnly = boxService.IsMachineConnected(AssetId);
                        //txtQtyProducedOperator.ReadOnly = boxService.IsMachineConnected(assetId);
                        if (!boxService.IsMachineConnected(assetId))
                        {
                            btnNumPadOk.Enabled = true;
                            btnConfirmScarti.Enabled = true;
                        }
                        ////txtQtyWasteTransaction.ReadOnly = boxService.IsMachineConnected(assetId);

                        hiddenOpenFermi.Value = bool.TrueString;
                        //dlgScarti.OpenDialog();
                    }
                    else
                    {
                        //apro direttamente i fermi
                        OpenFermi(assetId, operatorId);
                    }
                }
                else
                {
                    //apro direttamente i fermi
                    OpenFermi(assetId, operatorId);
                }
            }
            else
            {
                //apro direttamente i fermi
                OpenFermi(assetId, operatorId);
            }

            GoToFermi:
            if (!pnlScarti.Visible)
            {
                OpenFermi(assetId, operatorId);
            }
        }

        protected void btnCancelLogout_Click(object sender, EventArgs e)
        {
            pnlAllowConfirmProduction.Visible = false;
            //RefreshPage();
            //LogoutWindow.CloseDialog();
            pnlLogout.Visible = false;
            pnlPrimary.Visible = true;
        }
        #endregion

        #region Scarti
        [Serializable]
        protected class ScartiRecord
        {
            public string CauseId { get; set; }
            public string Description { get; set; } // code - description
            /// <summary>
            /// Totale scarti dichiarati in precedenza
            /// </summary>
            public decimal Num { get; set; }
            /// <summary>
            /// Scarti dichiaarti dall'operatore corrente
            /// </summary>
            public decimal OperatorWaste { get; set; }
            /// <summary>
            /// Scarti dichiarati da un controllo qualità
            /// </summary>
            public decimal Quality { get; set; }

            public ScartiRecord(string id, string description)
            {
                CauseId = id;
                Description = description;
            }

            public ScartiRecord(string id, string description, decimal num, decimal operatorWaste, decimal quality = 0)
            {
                CauseId = id;
                Description = description;
                Num = num;
                OperatorWaste = operatorWaste;
                Quality = quality;
            }
        }

        const string JOB_PRODUCTIONWASTE_TO_MODIFY = "JobProductionWasteListToModify";
        protected List<ScartiRecord> JobWasteListToModify
        {
            get
            {
                if (this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] == null)
                {
                    this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] = new List<ScartiRecord>();
                }
                return (List<ScartiRecord>)(this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY]);
            }
            set
            {
                this.ViewState[JOB_PRODUCTIONWASTE_TO_MODIFY] = value;
            }
        }
        const string BLANK_SHOT_RECORD = "BlankShotRecord";
        protected List<ScartiRecord> BlankShotRecord
        {
            get
            {
                if (this.ViewState[BLANK_SHOT_RECORD] == null)
                {
                    this.ViewState[BLANK_SHOT_RECORD] = new List<ScartiRecord>();
                }
                return (List<ScartiRecord>)(this.ViewState[BLANK_SHOT_RECORD]);
            }
            set
            {
                this.ViewState[BLANK_SHOT_RECORD] = value;
            }
        }

        protected void btnScarti_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlScarti.Visible = true;

            OperatorService OS = new OperatorService();
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();

            Transaction lastTrans = transService.GetLastTransactionOpen(AssetId);
            string jobId = lastTrans.JobId;
            Transaction lastProdTrans;
            lastProdTrans = transService.GetLastProdTransactionPerOperator(lastTrans.OperatorId, jobId, OperatorHelper.ApplicationId);
            if (lastProdTrans == null)
            {
                lastProdTrans = transService.GetLastTransactionPerOperator(AssetId, lastTrans.OperatorId, jobId);
            }

            hiddenOperatorId.Value = lastTrans.OperatorId;
            hiddenOpenFermi.Value = bool.FalseString;

            TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
            //string jobId = transOperRepos.ReadAll(x => x.MachineId == AssetId && x.OperatorId == lastTrans.OperatorId && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).FirstOrDefault()?.JobId;

            ///////variabili per turno///////////
            string operatorWorkshift;
            Transaction workshiftToWatch;
            List<Transaction> transactionWorkshiftList;
            string jobIdWorkshift;
            List<JobProductionWaste> wasteWorkshift;
            /////////////////////////////////////

            TransactionRepository transRepos = new TransactionRepository();
            JobProductionWasteService jpwService = new JobProductionWasteService();

            if (string.IsNullOrEmpty(jobId))
            {
                return;
            }

            //dichiarazione scarti normale (non qualità)
            jobIdWorkshift = jobId;
            operatorWorkshift = lastTrans.OperatorId;
            //workshiftToWatch = transOperRepos.ReadAll(x => x.MachineId == AssetId && x.JobId == jobIdWorkshift && x.OperatorId == operatorWorkshift && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First();
            workshiftToWatch = lastProdTrans;
            TransactionOperator transWorkshift = transOperRepos.ReadAll(x => x.MachineId == AssetId && x.JobId == jobIdWorkshift && x.OperatorId == operatorWorkshift && x.OperationName == "LogIn" && x.DateStart <= workshiftToWatch.Start).OrderByDescending(x => x.DateStart).First();
            transactionWorkshiftList = transRepos.ReadAll(x => x.MachineId == AssetId && x.JobId == jobIdWorkshift && x.OperatorId == operatorWorkshift && x.Start >= transWorkshift.DateStart).ToList();
            wasteWorkshift = jpwService.GetWasteListByJobId(jobId);

            hiddenTransactionToWatch.Value = workshiftToWatch.Id.ToString();

            if (!string.IsNullOrEmpty(jobIdWorkshift))
            {
                List<Transaction> currentJobTList = transService.GetTransactionListByJobId(AssetId, jobId);
                if (currentJobTList != null && currentJobTList.Count > 0)
                {
                    if (currentJobTList.Sum(x => x.PartialCounting) == 0)
                    {
                        btnNumPadOk.Enabled = false;
                        btnConfirmScarti.Enabled = false;
                    }
                    if (!grantService.AllowQuality(lastTrans.OperatorId))
                    {
                        btnRecuperoScarti.Enabled = false;
                    }
                    else
                    {
                        btnNumPadOk.Enabled = true;
                        btnConfirmScarti.Enabled = true;
                    }

                    txtQtyProducedTot.Text = Math.Round(currentJobTList.Sum(x => x.PartialCounting), 0).ToString();
                    txtQtyShotProducedOperator.Text = Math.Round(transactionWorkshiftList.Sum(x => x.PartialCounting), 0).ToString();
                    txtQtyProducedOperator.Text = Math.Round(transactionWorkshiftList.Sum(x => x.PartialCounting * x.CavityMoldNum), 0).ToString();
                    txtQtyWasteTransaction.Text = jpwService.GetWasteListPerTransaction(currentJobTList, jobId, OperatorHelper.ApplicationId).Sum(x => x.QtyProductionWaste).ToString();
                    //txtQtyWasteTransaction.Text = wasteWorkshift.Sum(x => x.QtyProductionWaste).ToString();

                    CauseTypeRepository CTR = new CauseTypeRepository();
                    List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == OperatorHelper.ApplicationId).ToList();
                    List<ScartiRecord> dataSource = new List<ScartiRecord>();
                    foreach (CauseType ct in causeList)
                    {
                        foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode))
                        {
                            //mostro gli scarti dichiarati in precedenza
                            //if (wasteList.Where(x => x.CauseId == c.Id).Any())
                            if (wasteWorkshift.Where(x => x.CauseId == c.Id).Any())
                            {
                                //dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteList.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste)));
                                dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, wasteWorkshift.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste), 0, 0));
                            }
                            else
                            {
                                dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                            }
                        }
                    }

                    gridScarti.DataSource = dataSource;
                    gridScarti.DataBind();

                    //dlgScarti.Title = string.Format("SCARTI - Macchina: {0} - Operatore: {1} - Commessa: {3} - Articolo: {2}",
                    //    AS.GetAssedById(AssetId, OperatorHelper.ApplicationId).Description,
                    //    OS.GetOperatorNameById(lastTrans.OperatorId, OperatorHelper.ApplicationId),
                    //    currentJobTList.First(x => !string.IsNullOrEmpty(x.JobId)).Job?.Order?.Article?.Code,
                    //    currentJobTList.First(x => !string.IsNullOrEmpty(x.JobId)).Job?.Order?.CustomerOrder?.OrderCode);

                    SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
                    txtQtyShotProducedOperator.ReadOnly = boxService.IsMachineConnected(AssetId);
                    if (!boxService.IsMachineConnected(AssetId))
                    {
                        btnNumPadOk.Enabled = true;
                        btnConfirmScarti.Enabled = true;
                    }
                    ////txtQtyWasteTransaction.ReadOnly = boxService.IsMachineConnected(assetId);

                    //dlgScarti.OpenDialog();
                }
            }
        }

        protected void btnAddBlankShot_Click(object sender, EventArgs e)
        {
            pnlWasteTable.Visible = false;
            pnlBlankShot.Visible = true;

            if (BlankShotRecord.Count == 0)
            {
                gridBlankShot.DataSource = new List<ScartiRecord>(1) { new ScartiRecord(null, "Colpi a vuoto", transService.GetBlankShotByJobId(transService.GetLastTransactionOpen(AssetId).JobId), 0) };
            }
            else
            {
                gridBlankShot.DataSource = BlankShotRecord;
            }
            gridBlankShot.DataBind();
        }

        protected void btnBackToWaste_Click(object sender, EventArgs e)
        {
            pnlBlankShot.Visible = false;
            pnlWasteTable.Visible = true;
        }

        protected void btnNumPadOk_Click(object sender, EventArgs e)
        {
            float num;
            if (float.TryParse(txtInsValue.Text, out num))
            {
                txtInsValue.Text = num.ToString();
            }

            if (pnlBlankShot.Visible)
            {
                if (!string.IsNullOrEmpty(txtInsValue.Text))
                {
                    if (BlankShotRecord.Count == 0)
                    {
                        BlankShotRecord.Add(new ScartiRecord(null, "Colpi a vuoto", transService.GetBlankShotByJobId(transService.GetLastTransactionOpen(AssetId).JobId), Convert.ToDecimal(txtInsValue.Text)));
                    }
                    else
                    {
                        BlankShotRecord[0].OperatorWaste = Convert.ToDecimal(txtInsValue.Text);
                    }
                    gridBlankShot.DataSource = BlankShotRecord;//new List<ScartiRecord>(1) { new ScartiRecord(null, "Colpi a vuoto", transService.GetBlankShotByJobId(transService.GetLastTransactionOpen(AssetId).JobId), 0) };
                    gridBlankShot.DataBind();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(hiddenCompileManualQty.Value) && hiddenCompileManualQty.Value == bool.TrueString)
                {
                    //inserire qtà per macchina manuale
                    txtQtyShotProducedOperator.Text = Math.Round(Convert.ToDecimal(txtInsValue.Text), 0).ToString();

                    TransactionRepository transRepos = new TransactionRepository();
                    Transaction transToWatch = transRepos.FindByID(Convert.ToInt32(hiddenTransactionToWatch.Value));
                    if (transToWatch != null)
                    {
                        txtQtyProducedOperator.Text = (Math.Round(Convert.ToDecimal(txtInsValue.Text), 0) * (transToWatch.CavityMoldNum > 0 ? transToWatch.CavityMoldNum : 1)).ToString();
                    }

                    hiddenCompileManualQty.Value = null;
                }
                else
                {
                    if (gridScarti.SelectedItems.Count > 0)
                    {
                        GridDataItem selectedItem = (GridDataItem)gridScarti.SelectedItems[0]; //get selected row
                        string causeId = selectedItem.GetDataKeyValue("CauseId").ToString();
                        if (grantService.AllowQuality(hiddenOperatorId.Value))
                        {
                            //controllo qualità
                            if (JobWasteListToModify.Where(x => x.CauseId == causeId).Any())
                            {
                                JobWasteListToModify.Where(x => x.CauseId == causeId).First().Quality = Convert.ToDecimal(txtInsValue.Text);
                            }
                            else
                            {
                                JobWasteListToModify.Add(new ScartiRecord(selectedItem.GetDataKeyValue("CauseId").ToString(), "", 0, 0, Convert.ToDecimal(txtInsValue.Text)));
                            }
                        }
                        else
                        {
                            //dichiarazione scarti operatore
                            if (JobWasteListToModify.Where(x => x.CauseId == causeId).Any())
                            {
                                JobWasteListToModify.Where(x => x.CauseId == causeId).First().OperatorWaste = Convert.ToDecimal(txtInsValue.Text);
                            }
                            else
                            {
                                JobWasteListToModify.Add(new ScartiRecord(selectedItem.GetDataKeyValue("CauseId").ToString(), "", 0, Convert.ToDecimal(txtInsValue.Text)));
                            }
                        }

                        JobProductionWasteService jpwService = new JobProductionWasteService();
                        //TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
                        //TransactionOperator workshiftToWatch = transOperRepos.FindByID(Convert.ToInt32(hiddenTransactionToWatch.Value));
                        //List<JobProductionWaste> wasteList = jpwService.GetWasteListPerWorkshift(workshiftToWatch.JobId, workshiftToWatch.OperatorId, workshiftToWatch.DateStart, workshiftToWatch.DateEnd, OperatorHelper.ApplicationId);
                        TransactionRepository transRepos = new TransactionRepository();
                        Transaction transToWatch = transRepos.FindByID(Convert.ToInt32(hiddenTransactionToWatch.Value));
                        List<JobProductionWaste> wasteList = jpwService.GetWasteListByJobId(transToWatch.JobId, OperatorHelper.ApplicationId);

                        //update datasource
                        CauseTypeRepository CTR = new CauseTypeRepository();
                        List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Scarti" && x.ApplicationId == OperatorHelper.ApplicationId).OrderBy(x => x.Description).ToList();
                        List<ScartiRecord> dataSource = new List<ScartiRecord>();
                        foreach (CauseType ct in causeList)
                        {
                            foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode))
                            {
                                //mostro gli scarti dichiarati in precedenza
                                if (wasteList.Where(x => x.CauseId == c.Id).Any())
                                {
                                    dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description, Math.Round(wasteList.Where(x => x.CauseId == c.Id).Sum(x => x.QtyProductionWaste)), 0, 0));
                                }
                                else
                                {
                                    dataSource.Add(new ScartiRecord(c.Id, c.ExternalCode + " - " + c.Description));
                                }
                            }
                        }

                        foreach (ScartiRecord sr in JobWasteListToModify)
                        {
                            int index = dataSource.IndexOf(dataSource.Where(x => x.CauseId == sr.CauseId).First());
                            if (grantService.AllowQuality(hiddenOperatorId.Value))
                            {
                                //controllo qualità, modifica la colonna qualità
                                dataSource.Where(x => x.CauseId == sr.CauseId).First().Quality = sr.Quality;
                                //dataSource.ElementAt(index).Quality = sr.Quality;
                            }
                            else
                            {
                                //dichiarazione scarti operatore, modifico la colonna operatore
                                dataSource.Where(x => x.CauseId == sr.CauseId).First().OperatorWaste = sr.OperatorWaste;
                                //dataSource.ElementAt(index).Num = sr.Num;
                            }
                        }
                        gridScarti.DataSource = dataSource;
                        gridScarti.DataBind();

                        txtQtyWasteTransaction.Text = dataSource.Sum(x => x.Num + x.OperatorWaste + x.Quality).ToString();

                        //Aggiornamento quantità totale in corso
                        //List<Transaction> currentJobTList = transService.GetTransactionListByCurrentJob(AssetId);
                        //txtQtyProducedTransaction.Text = currentJobTList.Sum(x => x.PartialCounting).ToString();

                    }
                }
            }
            txtInsValue.Text = "";
        }

        protected void btnConfirmScarti_Click(object sender, EventArgs e)
        {
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();
            TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
            TransactionRepository tr = new TransactionRepository();

            Transaction lastTrans;
            string jobId = null;
            decimal nImpronte = 1;
            decimal QtyShotProducedTot = 0;
            decimal QtyProducedTot = 0;

            SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
            if (!boxService.IsMachineConnected(AssetId))
            {
                //macchina manuale
                TransactionOperator t = transOperRepos.ReadAll(x => x.MachineId == AssetId && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First();
                lastTrans = transService.GetLastTransactionPerOperator(AssetId, hiddenOperatorId.Value);
                if (lastTrans != null)
                {
                    //l'operatore era sulla macchina, può dichiarare quantità prodotte
                    QtyShotProducedTot = Convert.ToDecimal(txtQtyShotProducedOperator.Text);
                    //QtyShotProducedTot = Convert.ToDecimal(txtQtyProducedOperator.Text);

                    lastTrans.PartialCounting = Convert.ToDecimal(txtQtyShotProducedOperator.Text);
                    //lastTrans.PartialCounting = Convert.ToDecimal(txtQtyProducedOperator.Text);
                    lastTrans.QtyProduced = lastTrans.PartialCounting * (lastTrans.CavityMoldNum > 0 ? lastTrans.CavityMoldNum : 1);
                    tr.Update(lastTrans);
                    tr.SaveChanges();

                    jobId = lastTrans.JobId;
                    nImpronte = lastTrans.Job != null && lastTrans.Job.CavityMoldNum.HasValue && lastTrans.Job.CavityMoldNum > 0 ? lastTrans.Job.CavityMoldNum.Value : 1;
                }
                else
                {
                    //l'operatore non era sulla macchina, è aggiuntivo, non dichiara quindi quantità prodotte
                    jobId = t.JobId;
                    nImpronte = t.Job != null && t.Job.CavityMoldNum.HasValue && t.Job.CavityMoldNum > 0 ? t.Job.CavityMoldNum.Value : 1;

                    //TODO: riportare l'ultimo lavoro con produzione o il turno scelto all'ingresso per sapere la quantità prodotta
                }
            }
            else
            {
                //macchina automatica
                jobId = transOperRepos.ReadAll(x => x.MachineId == AssetId && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First().JobId;

                List<Transaction> currentJobTList = transService.GetTransactionListByJobId(AssetId, jobId);
                if (currentJobTList != null && currentJobTList.Count > 0)
                {
                    QtyShotProducedTot = Math.Round(currentJobTList.Sum(x => x.PartialCounting), 0);
                    QtyProducedTot = Math.Round(currentJobTList.Sum(x => x.PartialCounting * x.CavityMoldNum), 0);
                    nImpronte = 1;
                    //nImpronte = currentJobTList.First().Job.CavityMoldNum.HasValue && currentJobTList.First().Job.CavityMoldNum > 0 ? currentJobTList.First().Job.CavityMoldNum.Value : 1;
                }

                lastTrans = transService.GetLastTransactionOpen(AssetId);
            }

            Transaction tWaste;
            if (!string.IsNullOrEmpty(hiddenTransactionToWatch.Value))
            {
                tWaste = tr.FindByID(Convert.ToInt32(hiddenTransactionToWatch.Value));
            }
            else
            {
                tWaste = transService.GetLastTransactionPerOperatorInProd(AssetId, hiddenOperatorId.Value, OperatorHelper.ApplicationId);
            }

            //se sono stati dichiarati colpi a vuoto, vanno salvati separatamente
            if (BlankShotRecord.Count > 0)
            {
                tWaste.BlankShot += BlankShotRecord[0].OperatorWaste;
            }

            foreach (ScartiRecord sr in JobWasteListToModify)
            {
                JobProductionWaste jpw = new JobProductionWaste();
                jpw.AssetId = AssetId;
                jpw.CauseId = sr.CauseId;
                jpw.Date = DateTime.Now;
                jpw.JobId = jobId; //lastTrans.JobId;
                jpw.OperatorId = hiddenOperatorId.Value; //lastTrans.OperatorId;
                jpw.QtyShotProduced = QtyShotProducedTot;
                jpw.QtyProduced = QtyProducedTot > 0 ? QtyProducedTot : QtyShotProducedTot * nImpronte;
                jpw.QtyProductionWaste = grantService.AllowQuality(hiddenOperatorId.Value) ? sr.Quality : sr.OperatorWaste;
                jpw.ArticleId = lastTrans?.Job?.Order?.IdArticle;
                jpw.Quality = grantService.AllowQuality(hiddenOperatorId.Value);
                jpw.TransactionId = tWaste.Id;
                jpwRep.Insert(jpw);
            }
            jpwRep.SaveChanges();

            //aggiungo gli scarti sull'ultima transazione di produzione dell'operatore
            if (tWaste != null)
            {
                //if (tWaste.QtyProductionWaste > 0)
                //{}
                //else
                //{}
                tWaste.QtyProductionWaste += JobWasteListToModify.Sum(x => x.OperatorWaste + x.Quality);
                tWaste.QtyProduced = (tWaste.PartialCounting - tWaste.BlankShot) * tWaste.CavityMoldNum;
                tWaste.QtyOK = tWaste.QtyProduced - tWaste.QtyProductionWaste;
                tr.Update(tWaste);
                tr.SaveChanges();
            }
            /////////////////////////////////////////////////////////////////////////

            JobWasteListToModify.Clear();
            BlankShotRecord.Clear();
            //dlgScarti.CloseDialog();
            pnlBlankShot.Visible = false;
            pnlWasteTable.Visible = true;
            pnlScarti.Visible = false;

            if (hiddenOpenFermi.Value == bool.TrueString)
            {
                //apro i fermi
                OpenFermi(AssetId, hiddenOperatorId.Value);
            }
            else
            {
                pnlPrimary.Visible = true;
            }
        }

        protected void btnCancelScarti_Click(object sender, EventArgs e)
        {
            JobWasteListToModify.Clear();
            BlankShotRecord.Clear();
            //dlgScarti.CloseDialog();
            pnlBlankShot.Visible = false;
            pnlWasteTable.Visible = true;
            pnlScarti.Visible = false;

            if (hiddenOpenFermi.Value == bool.TrueString)
            {
                //apro i fermi
                OpenFermi(AssetId, hiddenOperatorId.Value);
            }
            else
            {
                pnlPrimary.Visible = true;
            }
        }
        #endregion

        #region Fermi da giustificare in logout
        [Serializable]
        protected class FermiRecord
        {
            public int TransactionId { get; set; }
            public string CauseId { get; set; }
            public string Description { get; set; }
            public decimal Duration { get; set; }

            public FermiRecord(int tId, string causeId)
            {
                TransactionId = tId;
                CauseId = causeId;
            }

            public FermiRecord(string causeId, string description, decimal duration)
            {
                CauseId = causeId;
                Description = description;
                Duration = duration;
            }
        }

        const string TRANSACTION_LIST_TO_MODIFY = "TransactionListToModify";
        protected List<FermiRecord> TransactionListToModify
        {
            get
            {
                if (this.ViewState[TRANSACTION_LIST_TO_MODIFY] == null)
                {
                    this.ViewState[TRANSACTION_LIST_TO_MODIFY] = new List<FermiRecord>();
                }
                return (List<FermiRecord>)(this.ViewState[TRANSACTION_LIST_TO_MODIFY]);
            }
            set
            {
                this.ViewState[TRANSACTION_LIST_TO_MODIFY] = value;
            }
        }

        protected void btnFermi_Click(object sender, EventArgs e)
        {
            Transaction lastTrans = transService.GetLastTransactionOpen(AssetId);
            hiddenLogoutOperator.Value = bool.FalseString;
            hiddenOperatorId.Value = lastTrans.OperatorId;
            OpenFermi(AssetId, lastTrans.OperatorId);
        }

        private void OpenFermi(string assetId, string operatorId)
        {
            pnlPrimary.Visible = false;
            pnlScarti.Visible = false;

            OperatorService OS = new OperatorService();
            AssetManagement.Services.AssetService AS = new AssetManagement.Services.AssetService();

            SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
            if (boxService.IsMachineConnected(assetId))
            {
                //macchina automatica -> mostro dlgFermi
                pnlFermi.Visible = true;

                List<Transaction> datasource = transService.TransactionToJustifyList(assetId);
                gridFermi.DataSource = datasource;
                gridFermi.DataBind();

                //if (transOperService.OperatorsOnMachine(assetId).Count == 1) //è l'ultimo operatore rimasto
                //{
                if (datasource.Count > TransactionListToModify.Count)
                {
                    btnConfirmCausaleFermo.Enabled = false;
                    btnConfirmCausaleFermo.CssClass = "btnActivityDisabled";
                }
                else
                {
                    btnConfirmCausaleFermo.Enabled = true;
                    btnConfirmCausaleFermo.CssClass = "btnActivity";
                }
                //}

                LoadCauseTreeView();
                //updatePnlFermi.Update();

                //dlgFermi.Title = string.Format("FERMI - Macchina: {0} - Operatore: {1}", AS.GetAssedById(assetId, OperatorHelper.ApplicationId).Description, OS.GetOperatorNameById(operatorId, OperatorHelper.ApplicationId));
                //dlgFermi.OpenDialog();
            }
            else
            {
                //macchina manuale -> mostro dlgFermiManuale
                pnlFermiManuali.Visible = true;

                CauseTypeRepository CTR = new CauseTypeRepository();
                List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Fermi" && x.ApplicationId == OperatorHelper.ApplicationId).OrderBy(x => x.Description).ToList();
                List<FermiRecord> dataSource = new List<FermiRecord>();
                foreach (CauseType ct in causeList)
                {
                    foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode.Length).ThenBy(x => x.ExternalCode))
                    {
                        dataSource.Add(new FermiRecord(c.Id, c.ExternalCode + " - " + c.Description, 0));
                    }
                }

                gridFermiManuali.DataSource = dataSource;
                gridFermiManuali.DataBind();

                //dlgFermiManuali.Title = string.Format("FERMI - Macchina: {0} - Operatore: {1}", AS.GetAssedById(assetId, OperatorHelper.ApplicationId).Description, OS.GetOperatorNameById(operatorId, OperatorHelper.ApplicationId));
                //dlgFermiManuali.OpenDialog();
            }
        }

        protected void gridFermi_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                item["Status"].Text = item["Status"].Text == "False" ? "Off" : "On";
            }
        }

        protected void LoadCauseTreeView()
        {
            causeTreeView.Nodes.Clear();
            CauseService CS = new CauseService();
            //CauseTypeRepository CTR = new CauseTypeRepository();
            //List<CauseType> causeList = CTR.ReadAll(x => x.ApplicationId == OperatorHelper.ApplicationId && (x.Description == "Fermi" || x.Description == "Attrezzaggio" || x.Description == "Manutenzione")).ToList();
            //foreach (CauseType ct in causeList)
            //{
            //    //Node CauseType
            //    RadTreeNode nodeCT = new RadTreeNode(ct.Description, ct.Id);
            //    nodeCT.PostBack = false;
            //    nodeCT.Expanded = true;
            //    foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.Code))
            //    {
            //        RadTreeNode nodeC = new RadTreeNode(c.Code + " - " + c.Description, c.Id);
            //        nodeC.PostBack = true;
            //        nodeC.BorderStyle = BorderStyle.Solid;
            //        nodeC.BorderColor = System.Drawing.Color.Black;
            //        nodeC.Height = Unit.Percentage(30);
            //        //nodeC.Width = Unit.Percentage(66);
            //        nodeCT.Nodes.Add(nodeC);
            //    }
            //    causeTreeView.Nodes.Add(nodeCT);
            //}

            //Node CauseType
            RadTreeNode nodeCT = new RadTreeNode("Causali");
            nodeCT.PostBack = false;
            nodeCT.Expanded = true;
            List<MES.Models.Cause> causeList = CS.GetListCauseByGroupName(new string[] { "Attrezzaggio", "Manutenzione", "Fermi" }, OperatorHelper.ApplicationId);
            foreach (MES.Models.Cause c in causeList.OrderBy(x => x.Code))
            {
                RadTreeNode nodeC = new RadTreeNode(c.Code + " - " + c.Description, c.Id);
                nodeC.PostBack = true;
                nodeC.BorderStyle = BorderStyle.Solid;
                nodeC.BorderColor = System.Drawing.Color.Black;
                nodeC.Height = Unit.Percentage(30);
                //nodeC.Width = Unit.Percentage(66);
                nodeCT.Nodes.Add(nodeC);
            }
            causeTreeView.Nodes.Add(nodeCT);
            //causeTreeView.DataBind();
        }

        protected void causeTreeView_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            TransactionRepository tRep = new TransactionRepository();
            //List<Transaction> transactionListToModify = new List<Transaction>();
            //if (CustomSettings.Params.ContainsKey(TRANSACTION_LIST_TO_MODIFY))
            //{
            //    transactionListToModify = JsonConvert.DeserializeObject<List<Transaction>>(CustomSettings.Params[TRANSACTION_LIST_TO_MODIFY].ToString());
            //}
            GridItemCollection listItem = gridFermi.SelectedItems; //get selected rows
            foreach (GridDataItem item in listItem)
            {
                int id = (int)item.GetDataKeyValue("Id");
                Transaction t = tRep.FindByID(id);
                t.CauseId = e.Node.Value;
                //transactionListToModify.Add(t);
                TransactionListToModify.Add(new FermiRecord(t.Id, t.CauseId));
            }
            //CustomSettings.AddOrUpdate(TRANSACTION_LIST_TO_MODIFY, transactionListToModify);

            //update table fermi
            TransactionService TS = new TransactionService();
            List<Transaction> datasource = TS.TransactionToJustifyList(AssetId);
            foreach (var t in TransactionListToModify)
            {
                int index = datasource.IndexOf(datasource.Where(x => x.Id == t.TransactionId).First());
                datasource.ElementAt(index).CauseId = t.CauseId;
            }
            gridFermi.DataSource = datasource;
            gridFermi.DataBind();

            if (TransactionListToModify.Count > 0)
            {
                btnConfirmCausaleFermo.Enabled = true;
                btnConfirmCausaleFermo.CssClass = "btnActivity";
            }
            else
            {
                btnConfirmCausaleFermo.Enabled = false;
                btnConfirmCausaleFermo.CssClass = "btnActivityDisabled";
            }
        }

        protected void btnConfirmCausaleFermo_Click(object sender, EventArgs e)
        {
            TransactionRepository tRep = new TransactionRepository();
            if (TransactionListToModify.Count > 0)
            {
                TransactionService TS = new TransactionService();
                List<Transaction> datasource = TS.TransactionToJustifyList(AssetId);
                foreach (var t in TransactionListToModify)
                {
                    int index = datasource.IndexOf(datasource.Where(x => x.Id == t.TransactionId).First());
                    datasource.ElementAt(index).CauseId = t.CauseId;
                }
                tRep.UpdateAll(datasource);
                tRep.SaveChanges();
                TransactionListToModify.Clear();
            }

            if (hiddenLogoutOperator.Value == bool.TrueString)
            {
                transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.LogIn);
                if (transOperService.TransactionOpenPerOperator(AssetId, hiddenOperatorId.Value))
                {
                    transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
                }
            }

            //RefreshPage();
            //dlgFermi.CloseDialog();
            pnlFermi.Visible = false;
            pnlPrimary.Visible = true;

            ////if (grantService.AllowProduction(hiddenOperatorId.Value))
            ////{
            ////    //chiede se la commessa è terminata oppure no
            ////    dlgCloseCustomerOrder.OpenDialog();
            ////}
        }

        protected void btnCancelCausaleFermo_Click(object sender, EventArgs e)
        {
            TransactionListToModify.Clear();

            if (hiddenLogoutOperator.Value == bool.TrueString)
            {
                transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.LogIn);
                if (transOperService.TransactionOpenPerOperator(AssetId, hiddenOperatorId.Value))
                {
                    transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
                }
            }

            //RefreshPage();
            //dlgFermi.CloseDialog();
            pnlFermi.Visible = false;
            pnlPrimary.Visible = true;

            //if (grantService.AllowProduction(hiddenOperatorId.Value))
            //{
            //    //chiede se la commessa è terminata oppure no
            //    dlgCloseCustomerOrder.OpenDialog();
            //}
        }

        protected void btnOKFermo_Click(object sender, EventArgs e)
        {
            float num;
            if (float.TryParse(txtInsDuration.Text, out num))
            {
                txtInsDuration.Text = num.ToString();
            }

            if (gridFermiManuali.SelectedItems.Count > 0)
            {
                GridDataItem selectedItem = (GridDataItem)gridFermiManuali.SelectedItems[0]; //get selected row
                string causeId = selectedItem.GetDataKeyValue("CauseId").ToString();

                if (TransactionListToModify.Where(x => x.CauseId == causeId).Any())
                {
                    TransactionListToModify.Where(x => x.CauseId == causeId).First().Duration = Convert.ToDecimal(txtInsDuration.Text);
                }
                else
                {
                    TransactionListToModify.Add(new FermiRecord(selectedItem.GetDataKeyValue("CauseId").ToString(), null, Convert.ToDecimal(txtInsDuration.Text)));
                }

                //update datasource
                CauseTypeRepository CTR = new CauseTypeRepository();
                List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Fermi" && x.ApplicationId == OperatorHelper.ApplicationId).OrderBy(x => x.Description).ToList();
                List<FermiRecord> dataSource = new List<FermiRecord>();
                foreach (CauseType ct in causeList)
                {
                    foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode.Length).ThenBy(x => x.ExternalCode))
                    {
                        dataSource.Add(new FermiRecord(c.Id, c.ExternalCode + " - " + c.Description, 0));
                    }
                }

                foreach (FermiRecord fr in TransactionListToModify)
                {
                    int index = dataSource.IndexOf(dataSource.Where(x => x.CauseId == fr.CauseId).First());
                    dataSource.ElementAt(index).Duration = fr.Duration;
                }

                gridFermiManuali.DataSource = dataSource;
                gridFermiManuali.DataBind();
            }
            txtInsDuration.Text = "";
        }

        protected void btnConfirmFermiManuali_Click(object sender, EventArgs e)
        {
            TransactionOperatorRepository transOperRepos = new TransactionOperatorRepository();
            TransactionOperator t = transOperRepos.ReadAll(x => x.MachineId == AssetId && x.OperatorId == hiddenOperatorId.Value && x.OperationName == "LogIn").OrderByDescending(x => x.DateStart).First();
            Transaction lastTrans = transService.GetLastTransactionPerOperatorWorkshift(AssetId, hiddenOperatorId.Value, t.DateStart, t.DateEnd);
            if (lastTrans != null)
            {
                //l'operatore era sulla macchina
                if (TransactionListToModify.Count > 0)
                {
                    TransactionRepository tRep = new TransactionRepository();
                    CauseTypeRepository CTR = new CauseTypeRepository();
                    List<CauseType> causeList = CTR.ReadAll(x => x.Description == "Fermi" && x.ApplicationId == OperatorHelper.ApplicationId).OrderBy(x => x.Description).ToList();
                    List<FermiRecord> dataSource = new List<FermiRecord>();
                    List<Transaction> transToAdd = new List<Transaction>();
                    foreach (CauseType ct in causeList)
                    {
                        foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.ExternalCode.Length).ThenBy(x => x.ExternalCode))
                        {
                            dataSource.Add(new FermiRecord(c.Id, c.ExternalCode + " - " + c.Description, 0));
                        }
                    }
                    foreach (var item in TransactionListToModify)
                    {
                        Transaction newItem = new Transaction();
                        newItem = lastTrans;
                        newItem.CauseId = item.CauseId;
                        newItem.Duration = item.Duration * 60;  //è in minuti la dichiarazione
                        newItem.End = newItem.Start.AddSeconds((double)newItem.Duration);
                        newItem.PartialCounting = 0;
                        newItem.QtyProductionWaste = 0;
                        newItem.QtyOK = 0;
                        newItem.Open = false;
                        newItem.LastUpdate = DateTime.Now;
                        tRep.Insert(newItem);
                    }
                    tRep.SaveChanges();
                    TransactionListToModify.Clear();
                }
            }
            else
            {
                //operatore è aggiuntivo -> riportare l'ultimo turno o il turno selezionato all'ingresso
            }

            TransactionListToModify.Clear();

            if (hiddenLogoutOperator.Value == bool.TrueString)
            {
                transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.LogIn);
                if (transOperService.TransactionOpenPerOperator(AssetId, hiddenOperatorId.Value))
                {
                    transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
                }
            }

            //RefreshPage();
            //dlgFermiManuali.CloseDialog();
            pnlFermiManuali.Visible = false;
            pnlPrimary.Visible = true;
        }

        protected void btnCancelFermiManuali_Click(object sender, EventArgs e)
        {
            TransactionListToModify.Clear();

            if (hiddenLogoutOperator.Value == bool.TrueString)
            {
                transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.LogIn);
                if (transOperService.TransactionOpenPerOperator(AssetId, hiddenOperatorId.Value))
                {
                    transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
                }
            }

            //RefreshPage();
            //dlgFermiManuali.CloseDialog();
            pnlFermiManuali.Visible = false;
            pnlPrimary.Visible = true;
        }
        #endregion

        #region Indici
        const string IS_OEE_OPEN = "IsOEEOpen";

        protected void btnRefreshChart_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlOEE.Visible = true;

            Transaction t = transService.GetLastTransactionOpen(AssetId);

            if (!string.IsNullOrEmpty(t.JobId))
            {
                AssetRepository assetRep = new AssetRepository();
                //dlgOEE.Title = string.Format("OEE - Macchina: {0} - Commessa: {1}", assetRep.GetDescription(t.MachineId), t.Job.Order.CustomerOrder.OrderCode);
                //dlgOEE.OpenDialog();
                this.ViewState[IS_OEE_OPEN] = bool.FalseString;
                jobGrid.Rebind();
            }
        }

        private void CreateIndexes()
        {
            TransactionRepository TRep = new TransactionRepository();
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

            Job job = transService.GetLastTransactionOpen(AssetId)?.Job;

            if (job != null)
            {
                List<Transaction> tList = new List<Transaction>();
                tList = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id).ToList();
                //tList = TRep.ReadAll(x => x.MachineId == AssetId && x.Start >= DateStart && x.Start <= DateEnd).ToList();

                TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                decimal oreProdTeorico = 0;
                decimal qtyShotProd = 0;
                decimal waste = 0;
                decimal qtyProd = 0;
                decimal cadency = 0;
                decimal availability = 0;
                decimal efficiency = 0;
                decimal quality = 0;
                decimal OEE = 0;

                decimal impronte = transService.GetLastTransactionOpen(AssetId).CavityMoldNum;

                if (productionTime.TotalSeconds > 0)
                {
                    qtyShotProd = tList.Sum(x => x.PartialCounting - x.BlankShot);
                    cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                    if (job.StandardRate > 0)
                    {
                        oreProdTeorico += Math.Round(tList.Sum(x => x.PartialCounting - x.BlankShot) / job.StandardRate, 2);
                    }
                    qtyProd = tList.Sum(x => (x.PartialCounting - x.BlankShot) * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                    try
                    {
                        waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.JobId == job.Id).Sum(x => x.QtyProductionWaste), 0);
                        //waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.Date >= DateStart && x.Date <= DateEnd).Sum(x => x.QtyProductionWaste), 0);
                    }
                    catch (InvalidOperationException) { }
                }

                availability = indexService.Availability(productionTime, machineOn);
                efficiency = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
                quality = indexService.Quality(qtyProd, waste);
                OEE = indexService.OEE(availability / 100, efficiency / 100, quality / 100);

                List<ChartIndex> dataSource = new List<ChartIndex>();
                dataSource.Add(new ChartIndex(Math.Round(availability, 1), ParamContext.Availability));
                dataSource.Add(new ChartIndex(Math.Round(efficiency, 1), ParamContext.Efficiency));
                dataSource.Add(new ChartIndex(Math.Round(quality, 1), ParamContext.Quality));
                dataSource.Add(new ChartIndex(Math.Round(OEE, 1), ParamContext.OEE));
                chartOEE.DataSource = dataSource;
                chartOEE.DataBind();

                LoadProgressBar(productionTime, efficiency);
            }
        }

        private class ChartIndex
        {
            private ConfigurationService configService = new ConfigurationService();

            private decimal _indexValue;
            public decimal IndexValue
            {
                get { return _indexValue; }
                set { _indexValue = value; }
            }

            private string _indexColor;
            public string IndexColor
            {
                get { return _indexColor; }
                set { _indexColor = value; }
            }

            public ChartIndex(decimal indexValue, ParamContext index)
            {
                _indexValue = indexValue;
                _indexColor = configService.GetColorByParameter(index, indexValue).Name;
            }
        }

        private class JobRecord
        {
            public string Label { get; set; }
            public decimal? Pezzi { get; set; }
            public decimal? Colpi { get; set; }
            public decimal? Teorico { get; set; }
            public string Eseguito { get; set; }
            public string Tempo { get; set; }
            public string Efficienza { get; set; }
            public string GroupType { get; set; }

            public JobRecord(string label, decimal? pezzi, decimal? colpi, decimal? teorico, TimeSpan? eseguito, TimeSpan? tempo, string efficienza, string groupType)
            {
                Label = label;
                Pezzi = pezzi;
                Colpi = colpi;
                Teorico = teorico;
                if (eseguito != null)
                {
                    Eseguito = string.Format("{0}:{1:d2}:{2:d2}", (int)eseguito?.TotalHours, Math.Abs(eseguito.Value.Minutes), Math.Abs(eseguito.Value.Seconds));
                }
                if (tempo != null)
                {
                    Tempo = string.Format("{0}:{1:d2}:{2:d2}", (int)tempo?.TotalHours, tempo?.Minutes, tempo?.Seconds);
                }
                Efficienza = efficienza;
                GroupType = groupType;
            }
        }

        protected void jobGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //if (dlgOEE.IsOpen())
            //{
            if (this.ViewState[IS_OEE_OPEN] != null && this.ViewState[IS_OEE_OPEN].ToString() != bool.TrueString)
            {
                TransactionRepository TRep = new TransactionRepository();
                JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

                Job job = transService.GetLastTransactionOpen(AssetId).Job;

                List<Transaction> tList = new List<Transaction>();
                tList = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id).ToList();
                //tList = TRep.ReadAll(x => x.MachineId == AssetId && x.Start >= DateStart && x.Start <= DateEnd).ToList();

                TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                decimal oreProdTeoricoTotale = 0;
                decimal oreProdTeorico = 0;
                decimal qtyShotProd = 0;
                decimal waste = 0;
                decimal qtyProd = 0;
                decimal cadency = 0;
                decimal availability = 0;
                decimal efficiency = 0;
                decimal quality = 0;
                decimal OEE = 0;

                decimal impronte = transService.GetLastTransactionOpen(AssetId).CavityMoldNum;

                if (productionTime.TotalSeconds > 0)
                {
                    qtyShotProd = tList.Sum(x => x.PartialCounting - x.BlankShot);
                    cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                    if (job.StandardRate > 0)
                    {
                        oreProdTeorico += Math.Round(tList.Sum(x => x.PartialCounting - x.BlankShot) / job.StandardRate, 2);
                        oreProdTeoricoTotale += Math.Round((decimal)(job.QtyOrdered / (job.CavityMoldNum > 0 ? job.CavityMoldNum : 1)) / job.StandardRate, 2);
                    }
                    qtyProd = tList.Sum(x => (x.PartialCounting - x.BlankShot) * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                    try
                    {
                        waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.JobId == job.Id).Sum(x => x.QtyProductionWaste), 0);
                        //waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.Date >= DateStart && x.Date <= DateEnd).Sum(x => x.QtyProductionWaste), 0);
                    }
                    catch (InvalidOperationException) { }
                }

                availability = indexService.Availability(productionTime, machineOn);
                efficiency = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
                quality = indexService.Quality(qtyProd, waste);
                OEE = indexService.OEE(availability / 100, efficiency / 100, quality / 100);

                List<JobRecord> dataSource = new List<JobRecord>();

                //generalità job
                dataSource.Add(new JobRecord("Ordinato da produrre", job.QtyOrdered, Math.Round(job.QtyOrdered / (impronte > 0 ? impronte : 1), 2), job.StandardRate, null, TimeSpan.FromHours((double)oreProdTeoricoTotale), "", "Efficienza"));

                //availability
                dataSource.Add(new JobRecord("Tempo totale produzione / disponibile", null, null, null, productionTime, machineOn, availability + "%", "Disponibilità"));

                //turni precedenti
                decimal lastWorkshiftOreProdTeorico = 0;
                decimal lastWorkshiftQtyShotProd = 0;
                decimal lastWorkshiftQtyProd = 0;
                decimal lastWorkshiftCadency = 0;
                decimal lastWorkshiftEfficiency = 0;
                Transaction lastTrans = transService.GetLastTransactionOpen(AssetId);
                Transaction endLastWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId != lastTrans.OperatorId && !x.Open).OrderByDescending(x => x.Start).FirstOrDefault();
                List<Transaction> lastWorkshift = new List<Transaction>();
                if (endLastWorkshift != null)
                {
                    lastWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && !x.Open && x.Start <= endLastWorkshift.Start).ToList();
                    //lastWorkshift.Add(endLastWorkshift);
                }
                if (lastWorkshift.Count > 0)  //se c'è un turno precedente per quella commessa
                {
                    TimeSpan lastWorkshiftProductionTime = TimeSpan.FromSeconds((double)lastWorkshift.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                    if (lastWorkshiftProductionTime.TotalSeconds > 0)
                    {
                        lastWorkshiftQtyShotProd = lastWorkshift.Sum(x => x.PartialCounting - x.BlankShot);
                        lastWorkshiftCadency = Math.Round(lastWorkshiftQtyShotProd / (decimal)lastWorkshiftProductionTime.TotalHours, 2);
                        lastWorkshiftQtyProd = lastWorkshift.Sum(x => (x.PartialCounting - x.BlankShot) * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                        if (job.StandardRate > 0)
                        {
                            lastWorkshiftOreProdTeorico += Math.Round(lastWorkshiftQtyShotProd / job.StandardRate, 2);
                        }
                    }
                    lastWorkshiftEfficiency = indexService.Efficiency((decimal)lastWorkshiftProductionTime.TotalHours, lastWorkshiftOreProdTeorico);
                    dataSource.Add(new JobRecord("Eseguito turni precedenti", lastWorkshiftQtyProd, lastWorkshiftQtyShotProd, lastWorkshiftCadency, lastWorkshiftProductionTime, null, lastWorkshiftEfficiency + "%", "Efficienza"));
                }

                //turno corrente
                decimal currentWorkshiftOreProdTeorico = 0;
                decimal currentWorkshiftQtyShotProd = 0;
                decimal currentWorkshiftQtyProd = 0;
                decimal currentWorkshiftCadency = 0;
                decimal currentWorkshiftEfficiency = 0;
                List<Transaction> currentWorkshift = new List<Transaction>();
                //if (transService.LastTransactionOpenIsInProduction(AssetId))
                //{
                if (endLastWorkshift == null)
                {
                    //non c'è turno precedente
                    currentWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId == lastTrans.OperatorId).ToList();
                }
                else
                {
                    currentWorkshift = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == job.Id && x.OperatorId == lastTrans.OperatorId && x.Start > endLastWorkshift.Start).ToList();
                }
                //}
                TimeSpan currentWorkshiftProductionTime = TimeSpan.FromSeconds((double)currentWorkshift.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                if (currentWorkshiftProductionTime.TotalSeconds > 0)
                {
                    currentWorkshiftQtyShotProd = currentWorkshift.Sum(x => x.PartialCounting - x.BlankShot);
                    currentWorkshiftCadency = Math.Round(currentWorkshiftQtyShotProd / (decimal)currentWorkshiftProductionTime.TotalHours, 2);
                    currentWorkshiftQtyProd = currentWorkshift.Sum(x => (x.PartialCounting - x.BlankShot) * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                    if (job.StandardRate > 0)
                    {
                        currentWorkshiftOreProdTeorico += Math.Round(currentWorkshiftQtyShotProd / job.StandardRate, 2);
                    }
                }
                currentWorkshiftEfficiency = indexService.Efficiency((decimal)currentWorkshiftProductionTime.TotalHours, currentWorkshiftOreProdTeorico);
                dataSource.Add(new JobRecord("Eseguito turno corrente", currentWorkshiftQtyProd, currentWorkshiftQtyShotProd, currentWorkshiftCadency, currentWorkshiftProductionTime, null, currentWorkshiftEfficiency + "%", "Efficienza"));

                ////efficiency
                //dataSource.Add(new JobRecord("Eseguito effettivo", qtyProd, qtyShotProd, cadency, null, productionTime, efficiency + "%", "Efficienza"));
                decimal pezziResidui = job.QtyOrdered - qtyProd + waste;
                TimeSpan tempoResiduo = TimeSpan.FromHours((double)(cadency > 0 ? ((pezziResidui / (impronte > 0 ? impronte : 1)) / cadency) : 0) + productionTime.TotalHours);
                decimal wasteShotResiduo = pezziResidui % (impronte > 0 ? impronte : 1) > 0 ? (int)(pezziResidui / (impronte > 0 ? impronte : 1)) + 1 : pezziResidui / (impronte > 0 ? impronte : 1);
                dataSource.Add(
                    new JobRecord(
                        "Residuo da produrre (eccedenza) compreso scarti",
                        pezziResidui,
                        wasteShotResiduo,//Math.Round(pezziResidui / (impronte > 0 ? impronte : 1), 2),
                        null,
                        cadency > 0 ? TimeSpan.FromHours((double)((pezziResidui / (impronte > 0 ? impronte : 1)) / cadency)) : TimeSpan.FromHours((double)(0)),
                        null,
                        "",
                        "Efficienza"));

                if (job.StandardRate > 0)
                {
                    decimal tempoAssegnato = Math.Round(job.QtyOrdered / (job.StandardRate * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1)), 2);
                    decimal tempoPrevisto = 0;
                    if (efficiency > 0)
                    {
                        tempoPrevisto = Math.Round(tempoAssegnato / efficiency * 100, 2);
                    }
                    else
                    {
                        tempoPrevisto = tempoAssegnato;
                    }
                    dataSource.Add(new JobRecord("Tempo eccedenza (recupero) e totale lotto", null, null, null, TimeSpan.FromHours((double)(tempoPrevisto - tempoAssegnato)), tempoResiduo, 100 - Math.Round(tempoAssegnato / tempoPrevisto * 100, 2) + "%", "Efficienza"));
                }

                //quality
                dataSource.Add(new JobRecord("Totale pezzi prodotti", qtyProd, null, null, null, null, quality + "%", "Qualità"));
                decimal wasteShot = waste % (impronte > 0 ? impronte : 1) > 0 ? (int)(waste / (impronte > 0 ? impronte : 1)) + 1 : waste / (impronte > 0 ? impronte : 1);
                if (job.StandardRate > 0)
                {
                    dataSource.Add(new JobRecord("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours((double)(wasteShot / job.StandardRate)), null, (100 - quality) + "%", "Qualità"));
                }
                else
                {
                    dataSource.Add(new JobRecord("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours(0), null, (100 - quality) + "%", "Qualità"));
                }

                jobGrid.DataSource = dataSource;

                meterAvailability.Pointer.Value = availability;
                txtAvailability.Text = availability.ToString();
                meterEfficiency.Pointer.Value = efficiency;
                txtEfficiency.Text = efficiency.ToString();
                meterQuality.Pointer.Value = quality;
                txtQuality.Text = quality.ToString();
                meterOEE.Pointer.Value = OEE;
                txtOEE.Text = OEE.ToString();
                //}
                this.ViewState[IS_OEE_OPEN] = bool.TrueString;
            }
        }

        protected void jobGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridGroupHeaderItem)
            {
                GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;
                DataRowView groupDataRow = (DataRowView)e.Item.DataItem;
                item.DataCell.Text = groupDataRow["GroupType"].ToString();
            }
        }

        protected void btnCloseOEE_Click(object sender, EventArgs e)
        {
            //dlgOEE.CloseDialog();
            this.ViewState.Remove(IS_OEE_OPEN);
            pnlOEE.Visible = false;
            pnlPrimary.Visible = true;
        }

        private void LoadProgressBar(TimeSpan productionTime, decimal efficiency)
        {
            //tempo assegnato
            Transaction lastTrans = transService.GetLastTransactionOpen(AssetId);
            Job j = lastTrans.Job;

            if (j.StandardRate > 0)
            {
                decimal tempoAssegnato = Math.Round(j.QtyOrdered / (j.StandardRate * (j.CavityMoldNum > 0 ? j.CavityMoldNum.Value : 1)), 2);
                decimal tempoPrevisto = 0;
                if (efficiency > 0)
                {
                    tempoPrevisto = Math.Round(tempoAssegnato / efficiency * 100, 2);
                }
                else
                {
                    tempoPrevisto = tempoAssegnato;
                }

                barTheoreticalTime.Attributes["aria-valuenow"] = Math.Round(tempoAssegnato / tempoPrevisto * 100, 2).ToString().Replace(",", ".");
                barTheoreticalTime.Attributes["style"] = string.Format("z-index:1000; font-size: large; font-weight: bold; width:{0}%", Math.Round(tempoAssegnato / tempoPrevisto * 100, 2)).Replace(",", ".");
                barTheoreticalTime.InnerText = string.Format("{0:n1} h", tempoAssegnato);

                textEstimatedTime.Attributes["style"] = string.Format("z-index:1000; font-size: large; font-weight: bold;  text-align:right; width:{0}%", 100 - Math.Round(tempoAssegnato / tempoPrevisto * 100, 2)).Replace(",", ".");
                textEstimatedTime.InnerText = string.Format("{0:n1} h", tempoPrevisto - tempoAssegnato);

                barProducedTime.Attributes["aria-valuenow"] = Math.Round((decimal)productionTime.TotalHours / tempoPrevisto * 100, 2).ToString().Replace(",", ".");
                barProducedTime.Attributes["style"] = string.Format("font-size: large; font-weight: bold; text-align:center; float:left; width:{0}%", Math.Round((decimal)productionTime.TotalHours / tempoPrevisto * 100, 2)).Replace(",", ".");
                barProducedTime.InnerText = string.Format("{0} h", Math.Round(productionTime.TotalHours, 1));

                decimal leftTime = tempoPrevisto - (decimal)productionTime.TotalHours;
                barLeftTime.Attributes["aria-valuenow"] = Math.Round(leftTime / tempoPrevisto * 100, 2).ToString().Replace(",", ".");
                barLeftTime.Attributes["style"] = string.Format("background-color:#f5f5f5 !important; font-size: large; font-weight: bold; color: black; text-align:right; float:left; width:{0}%", Math.Round(leftTime / tempoPrevisto * 100, 2)).Replace(",", ".");
                barLeftTime.InnerText = string.Format("{0:n1} h", leftTime);

                MachinePage.tempoAssegnato = tempoAssegnato;
                MachinePage.tempoPrevisto = tempoPrevisto;
                MachinePage.tempoRimasto = leftTime;
                updateBtnQualità();
            }
        }
        #endregion

        protected void Timer1_Tick(object sender, EventArgs e)
        {
        }

        protected void btnQualità_Click(object sender, EventArgs e)
        {
            Response.Redirect("ControlPlanExecutionList?asset=" + AssetId);
            //TransactionService TService = new TransactionService();
            //OperatorRepository operRep = new OperatorRepository();
            //string opId = operRep.FindByID(TService.GetLastTransactionOpen(AssetId).OperatorId)?.Id;
            //if (!string.IsNullOrEmpty(opId))
            //{
            //    string jobId = TService.GetLastTransactionOpen(AssetId).JobId;
            //    ControlPlanAssociatedRepository cpaRep = new ControlPlanAssociatedRepository();
            //    var cpId = TService.GetLastTransactionOpen(AssetId).Job.ControlPlanId;
            //    var cp = cpaRep.ReadAll(x => x.JobId == jobId && x.ControlPlanId == cpId).FirstOrDefault();
            //    if (cp != null)
            //    {
            //        pnlPrimary.Visible = false;
            //        pnlControlPlan.Visible = true;

            //        long newId = controlPlan.createNewCpe(cp.Id, opId);  //TODO:sistemare con i valori corretti
            //        controlPlan.loadData(newId);

            //        //dlgControlPlan.OpenDialog();
            //    }
            //}
        }

        protected void btnArticleImage_Click(object sender, EventArgs e)
        {
            Transaction lastTrans = transService.GetLastTransactionOpen(AssetId);

            Article a = lastTrans?.Job?.Order?.Article;
            if (a != null)
            {
                pnlPrimary.Visible = false;
                pnlArticle.Visible = true;

                lblArticleImageCode.Text = string.Format("Articolo: {0}", a.Code);
                imgArticle.ImageUrl = a.PathImg;
                //dlgArticleImage.OpenDialog();
            }
        }

        protected void btnCloseArticleImage_Click(object sender, EventArgs e)
        {
            //dlgArticleImage.CloseDialog();
            pnlArticle.Visible = false;
            pnlPrimary.Visible = true;
        }
    }
}