﻿using MainCloudFramework.Models;
using MainCloudFramework.Services;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Host
{
    public partial class ManageUsers : BaseHostPage
    {
        private ApplicationService applicationService = new ApplicationService();

        public string GetUserRole(string userId)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                var r = applicationService.FindUserRoleCurrentApp(userId);
                if (r != null)
                {
                    return r.Name;
                }
            }
            return "";
        }

        private void GridViewDataBind()
        {
            if (MultiTenantsHelper.ApplicationName == null)
            {
                DgvUsers.DataSource = applicationService.GetAllUsers();
            }
            else
            {
                DgvUsers.DataSource = applicationService.GetUserForCurrentApplication();
            }
            DgvUsers.DataBind();

            DgvUsers.UseAccessibleHeader = true;
            if (DgvUsers.HeaderRow != null)
            {
                DgvUsers.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridViewDataBind();
        }

        protected void DgvUsers_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
        }

        protected void DgvUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string id = e.Keys["ID"] as string;
            applicationService.RemoveUserCurrentAppAndGroup(id);
            GridViewDataBind();
        }

        protected void DgvUsers_RowEditing(object sender, GridViewEditEventArgs e)
        {
            string id = DgvUsers.DataKeys[e.NewEditIndex].Value as string; //.Keys["ID"] as string;
            string fromUrl = string.Format("~/{0}/{1}", MultiTenantsHelper.HostAdministrationUrl, "ManageUser".TrimStart('/'));
            string returnUrl = string.Format("~/{0}/{1}", MultiTenantsHelper.HostAdministrationUrl, "ManageUsers".TrimStart('/'));
            Response.Redirect(string.Format("{0}?Id={1}&appId={2}&ReturnUrl={3}", fromUrl, id, MultiTenantsHelper.ApplicationId, returnUrl));
        }

        protected void DgvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Password")
            {
                int rowIndex = int.Parse(e.CommandArgument.ToString());
                string id = DgvUsers.DataKeys[rowIndex].Value as string; //.Keys["ID"] as string;
                string fromUrl = string.Format("~/{0}/{1}", MultiTenantsHelper.HostAdministrationUrl, "ManageUserPassword".TrimStart('/'));
                string returnUrl = string.Format("~/{0}/{1}", MultiTenantsHelper.HostAdministrationUrl, "ManageUsers".TrimStart('/'));
                Response.Redirect(string.Format(fromUrl + "?Id={0}&ReturnUrl={1}", id, returnUrl));
            }
        }
    }
}