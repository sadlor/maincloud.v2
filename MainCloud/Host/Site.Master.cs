﻿using MainCloudFramework;
using MainCloudFramework.Web.BasePages;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace MainCloud.Host
{
    public partial class SiteMaster : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataBind();
            if (!string.IsNullOrEmpty(TenantName))
            {
                Page.Title += " [Application " + TenantName + "]";
            }
        }

        protected void lnkBtnBackHost_Click(object sender, EventArgs e)
        {
            var returnUrl = "~/Host/ManageApplications";
            if (Request.QueryString["ReturnUrl"] != null)
            {
                returnUrl = Request.QueryString["ReturnUrl"];
            }
            else if (Request.AppRelativeCurrentExecutionFilePath == "~/Host/ManageApplications")
            {
                returnUrl = "~/ListApplications";
            }

            IdentityHelper.RedirectToReturnUrl(returnUrl, Response);
        }

        protected void hostMenu_Load(object sender, EventArgs e)
        {
            (sender as HtmlGenericControl).Visible = IsHostUser;
        }

        protected void hostMainMenu_Load(object sender, EventArgs e)
        {
            (sender as HtmlGenericControl).Visible = IsHostUser && Request.AppRelativeCurrentExecutionFilePath == "~/Host/ManageApplications";
        }
    }
}