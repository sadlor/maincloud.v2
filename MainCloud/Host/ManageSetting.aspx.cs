﻿using JsonRequest;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Services;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Host
{
    public partial class ManageSetting : BaseHostPage
    {
        private ApplicationService aspNetApplicationService = new ApplicationService();
        private MainCustomerRepository mainCustomerRepository = new MainCustomerRepository();

        private void GridViewDataBind()
        {
            DgvSettings.DataSource = ApplicationSettingsHelper.GetConfigurations();
            DgvSettings.DataBind();

            DgvSettings.UseAccessibleHeader = true;
            if (DgvSettings.HeaderRow != null)
            {
                DgvSettings.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            ddlCustomer.DataSource = mainCustomerRepository.FindAll().ToList();
            var customerId = aspNetApplicationService.GetCustomerId();
            if (customerId != null)
            {
                ddlCustomer.SelectedValue = customerId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridViewDataBind();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            GridViewDataBind();
            ButtonsStatusService();
        }

        protected void DgvRoles_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
        }

        protected void DgvRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        //    var usersContext = new ApplicationDbContext();
        //    string id = e.Keys["ID"] as string;
        //    var role = usersContext.Roles.FirstOrDefault(u => u.Id == id);
        //    if (role != null)
        //    {
        //        usersContext.Roles.Remove(role);
        //        //usersContext.Entry(user).State = EntityState.Deleted;
        //        usersContext.SaveChanges();
        //    }
        //    GridViewDataBind();
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ApplicationSettingsHelper.UpdateSettings(txtNewKey.Text, txtNewValue.Text);
                txtNewKey.Text = "";
                txtNewValue.Text = "";
                GridViewDataBind();
            }
            catch(Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }

        protected void gridAction_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                string key = btn.CommandArgument ?? null;
                if (!string.IsNullOrEmpty(key))
                {
                    switch (btn.CommandName)
                    {
                        case "EditConfig":
                            DgvSettings.Visible = InsertPanel.Visible = false;
                            EditPanel.Visible = true;
                            txtEditKey.Text = key;
                            txtEditVal.Text = "" + ApplicationSettingsHelper.GetConfigurations()[key];
                            break;
                        case "DeleteConfig":
                            //                        ApplicationSettingsHelper.UpdateSettings()
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DgvSettings.Visible = InsertPanel.Visible = true;
                EditPanel.Visible = false;
                ApplicationSettingsHelper.UpdateSettings(txtEditKey.Text, txtEditVal.Text);
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }

        protected override void btnCancel_Click(object sender, EventArgs e)
        {
            DgvSettings.Visible = InsertPanel.Visible = true;
            EditPanel.Visible = false;
        }

        protected void BtnSaveCustomer_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                aspNetApplicationService.SetCustomer(null);
            }
            else
            {
                aspNetApplicationService.SetCustomer(ddlCustomer.SelectedValue);
            } 
        }

        protected void BtnSyncronizeSettings_Click(object sender, EventArgs e)
        {
            ApplicationSettingsHelper.SyncronizeSettings(MultiTenantsHelper.ApplicationId);
            GridViewDataBind();
        }

        protected void BtnSyncronizeSettingsStartService_Click(object sender, EventArgs e)
        {
            ApplicationSettingsHelper.StartSyncronizationBackgroundService(MultiTenantsHelper.ApplicationId);
        }

        protected void BtnSyncronizeSettingsStopService_Click(object sender, EventArgs e)
        {
            ApplicationSettingsHelper.StopSyncronizationBackgroundService(MultiTenantsHelper.ApplicationId);
        }

        private void ButtonsStatusService()
        {
            if (ApplicationSettingsHelper.IsRunningSyncronizationBackgroundService(MultiTenantsHelper.ApplicationId))
            {
                BtnSyncronizeSettingsStartService.Visible = false;
                BtnSyncronizeSettingsStopService.Visible = true;
            }
            else
            {
                BtnSyncronizeSettingsStartService.Visible = true;
                BtnSyncronizeSettingsStopService.Visible = false;
            }
        }
    }
}