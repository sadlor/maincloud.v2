﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Host/Site.Master" AutoEventWireup="true" CodeBehind="ManageCustomer.aspx.cs" Inherits="MainCloud.Host.ManageCustomer" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <mcf:Alert runat="server" id="AlertMessage" />

    <h3><%: Title %></h3>

    <div class="form-horizontal">
        <h4>Edit customer</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtCode" CssClass="col-md-2 control-label">Code</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtCode" CssClass="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtName" CssClass="col-md-2 control-label">Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtName" CssClass="form-control"/>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtName"
                    CssClass="text-danger" ErrorMessage="Required filed." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtPIVA" CssClass="col-md-2 control-label">PIVA</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtPIVA" CssClass="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtFiscalCode" CssClass="col-md-2 control-label">FiscalCode</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtFiscalCode" CssClass="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtAddress" CssClass="col-md-2 control-label">Address</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtAddress" CssClass="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtCity" CssClass="col-md-2 control-label">City</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtCity" CssClass="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtZipCode" CssClass="col-md-2 control-label">ZipCode</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtZipCode" CssClass="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtDescription" CssClass="col-md-2 control-label">Description</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtDescription" CssClass="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ddlReseller" CssClass="col-md-2 control-label">Reseller</asp:Label>
            <div class="col-md-10">
                <asp:DropDownList runat="server" ID="ddlReseller" DataTextField="Name" DataValueField="Id" AppendDataBoundItems="true" CssClass="form-control">
                    <asp:ListItem Value="" Text="--"/>
                </asp:DropDownList>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ddlAgent" CssClass="col-md-2 control-label">Agent</asp:Label>
            <div class="col-md-10">
                <asp:DropDownList runat="server" ID="ddlAgent" DataTextField="FullName" DataValueField="Id" AppendDataBoundItems="true" CssClass="form-control">
                    <asp:ListItem Value="" Text="--"/>
                </asp:DropDownList>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="btn btn-primary" />
                <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" CssClass="btn btn-default" CausesValidation="false" />
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
