﻿using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Host
{
    public partial class ManageCustomers : BaseHostPage
    {
        private MainCustomerRepository mainCustomerRepository = new MainCustomerRepository();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                GridViewDataBind();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            GridViewDataBind();
        }

        private void GridViewDataBind()
        {
            DgvMaster.DataSource = mainCustomerRepository.FindAll().ToList();
            DgvMaster.DataBind();

            DgvMaster.UseAccessibleHeader = true;
            if (DgvMaster.HeaderRow != null)
            {
                DgvMaster.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            mainCustomerRepository.Insert(new MainCustomer()
            {
                Name = txtNewName.Text

            });
            mainCustomerRepository.SaveChanges();
            txtNewName.Text = "";
        }

        protected void DgvMaster_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string id = e.Keys["ID"] as string;
            var itemToDelete = mainCustomerRepository.FindByID(id);
            if (itemToDelete != null)
            {
                mainCustomerRepository.Delete(itemToDelete);
                mainCustomerRepository.SaveChanges();
            }
        }

        protected void DgvMaster_RowEditing(object sender, GridViewEditEventArgs e)
        {
            string id = DgvMaster.DataKeys[e.NewEditIndex].Value as string;
            string fromUrl = string.Format("~/{0}/{1}", MultiTenantsHelper.HostAdministrationUrl, "ManageCustomer".TrimStart('/'));
            string returnUrl = string.Format("~/{0}/{1}", MultiTenantsHelper.HostAdministrationUrl, "ManageCustomers".TrimStart('/'));
            Response.Redirect(string.Format("{0}?Id={1}&ReturnUrl={2}", fromUrl, id, returnUrl));
        }
    }
}