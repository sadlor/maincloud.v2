﻿using MainCloudFramework;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.BasePages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Host
{
    public partial class ManageReseller : BaseHostPage
    {
        public MainReseller Reseller { get; private set; }
        private MainResellerRepository mainCustomerRepository = new MainResellerRepository();

        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request["Id"] as string;

            Reseller = mainCustomerRepository.FindByID(id);

            if (!IsPostBack)
            {
                txtCode.Text = Reseller.Code;
                txtName.Text = Reseller.Name;
                txtPIVA.Text = Reseller.PIVA;
                txtFiscalCode.Text = Reseller.FiscalCode;
                txtAddress.Text = Reseller.Address;
                txtCity.Text = Reseller.City;
                txtZipCode.Text = Reseller.ZipCode;
                txtDescription.Text = Reseller.Description;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Reseller.Code = txtCode.Text;
                Reseller.Name = txtName.Text;
                Reseller.PIVA = txtPIVA.Text;
                Reseller.FiscalCode = txtFiscalCode.Text;
                Reseller.Address = txtAddress.Text;
                Reseller.City = txtCity.Text;
                Reseller.ZipCode = txtZipCode.Text;
                Reseller.Description = txtDescription.Text;
                mainCustomerRepository.Update(Reseller);
                mainCustomerRepository.SaveChanges();

                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }

    }
}