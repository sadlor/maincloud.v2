﻿<%@ Page Title="Manage Widgets" Language="C#" MasterPageFile="~/Host/Site.Master" AutoEventWireup="true" CodeBehind="ManageWidgets.aspx.cs" Inherits="MainCloud.Host.ManageWidgets" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <h3><%: Title %></h3>

    <div class="panel panel-default">
        <div class="panel-heading">Upload widget</div>
        <div class="panel-body">
            
            <div class="form-inline">
                <div class="form-group">
                    <asp:FileUpload ID="FileUploadControl" runat="server"  />
                </div>
                <asp:Button runat="server" ID="UploadButton" class="btn btn-default" Text="Upload" OnClick="UploadButton_Click" />
                <asp:Label runat="server" ID="StatusLabel" class="help-block" Text="Upload status: " />
            </div>

        </div>
    </div>

    <asp:LinkButton ID="BtnSync" runat="server" class="btn btn-primary btn-xs" OnClick="BtnSync_Click">
        <i class="fa fa-refresh fa-fw"></i> Refresh list
    </asp:LinkButton>
    <asp:GridView ID="DgvWidgets" runat="server" DataKeyNames="WidgetId"
        CssClass="table table-hover table-striped" GridLines="None" 
        OnRowDeleting="DgvWidgets_RowDeleting" OnRowDeleted="DgvWidgets_RowDeleted"
        AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="WidgetId" HeaderText="WidgetId" Visible="false" />
            <asp:BoundField DataField="Name" HeaderText="Name" />
            <asp:BoundField DataField="Description" HeaderText="Description" />
            <asp:BoundField DataField="WidgetName" HeaderText="Widget Name" />
            <asp:BoundField DataField="WidgetPath" HeaderText="Widget Path" />
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
<%--                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select" class="btn btn-primary btn-xs"><i class="fa fa-info-circle"></i> Select</asp:LinkButton>--%>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="cursor-pointer" />
    </asp:GridView>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
