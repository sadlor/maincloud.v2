﻿<%@ Page Title="Manage Roles" Language="C#" MasterPageFile="~/Host/Site.Master" AutoEventWireup="true" CodeBehind="ManageRoles.aspx.cs" Inherits="MainCloud.Host.ManageRoles" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <mcf:Alert runat="server" id="AlertMessage" />

    <h3><%: Title %></h3>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-inline">
                <div class="form-group">
                    <asp:TextBox ID="txtNewRoleName" runat="server"  />
                </div>
                <asp:Button runat="server" ID="BtnAdd" class="btn btn-default" Text="Add Role" OnClick="BtnAdd_Click" />
            </div>

        </div>
    </div>

    <asp:GridView ID="DgvRoles" runat="server" DataKeyNames="ID"
        CssClass="table table-hover table-striped" GridLines="None" 
        OnRowDeleting="DgvRoles_RowDeleting" OnRowDeleted="DgvRoles_RowDeleted"
        AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
            <asp:BoundField DataField="Name" HeaderText="Name" />
           <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <%--<asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select" class="btn btn-primary btn-xs"><i class="fa fa-info-circle"></i> Select</asp:LinkButton>--%>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" class="btn btn-danger btn-xs" Visible='<%# ("Host Administrators Guests".Contains((string)Eval("Name"))) ? false : true %>'><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="cursor-pointer" />
    </asp:GridView>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
