﻿using MainCloudFramework.Repositories;
using MainCloudFramework.Services;
using MainCloudFramework.Web;
using MainCloudFramework.Web.BasePages;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Host
{
    public partial class ManageWidgets : BaseHostPage
    {
        private void GridViewDataBind()
        {
            var widgetDao = new WidgetsRepository();
            DgvWidgets.DataSource = widgetDao.GetInstalledWidgets();
            DgvWidgets.DataBind();

            DgvWidgets.UseAccessibleHeader = true;
            if (DgvWidgets.HeaderRow != null)
            {
                DgvWidgets.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void SyncWidgetsList()
        {
            WidgetService widgetService = new WidgetService();
            widgetService.SyncWidgetsList();
            GridViewDataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridViewDataBind();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            GridViewDataBind();
        }

        protected void BtnSync_Click(object sender, EventArgs e)
        {
            SyncWidgetsList();
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (FileUploadControl.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(FileUploadControl.FileName);
                    string zipPath = Server.MapPath(MCFConstants.BASE_MODULES_PATH) + filename;
                    string extractPath = Server.MapPath(MCFConstants.BASE_MODULES_PATH);

                    FileUploadControl.SaveAs(zipPath);

                    using (ZipArchive archive = ZipFile.Open(zipPath, ZipArchiveMode.Update))
                    {
                        //archive.CreateEntryFromFile(newFile, "NewEntry.txt");
                        archive.ExtractToDirectory(extractPath);
                    }

                    SyncWidgetsList();
                    StatusLabel.Text = "Upload status: File uploaded!";
                }
                catch (Exception ex)
                {
                    StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                }
            }
        }

        protected void DgvWidgets_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
        }

        protected void DgvWidgets_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var widgetDao = new WidgetsRepository();
            widgetDao.DeleteInstalledWidgets("" + e.Keys["WidgetId"]);
        }
    }
}