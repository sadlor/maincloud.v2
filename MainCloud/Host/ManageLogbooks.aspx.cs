﻿using EnergyModule.Models;
using MainCloudFramework.Web.BasePages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Host
{
    public partial class ManageLogbooks : BaseHostPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new EnergyDbContext())
            {
                var logs = (from log in db.Logs
                           orderby log.Date ascending
                           select log)
                           .Skip(0)
                           .Take(200)
                           .ToList();

                rptLogbooks.DataSource = logs;
            }
            rptLogbooks.DataBind();
        }
    }
}