﻿using MainCloudFramework;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.BasePages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Host
{
    public partial class ManageCustomer : BaseHostPage
    {
        public MainCustomer Customer { get; private set; }
        private MainCustomerRepository mainCustomerRepository = new MainCustomerRepository();
        private MainResellerRepository mainResellerRepository = new MainResellerRepository();
        private MainAgentRepository mainAgentRepository = new MainAgentRepository();

        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request["Id"] as string;

            Customer = mainCustomerRepository.FindByID(id);
            ddlReseller.DataSource = mainResellerRepository.FindAll().ToList();
            ddlAgent.DataSource = mainAgentRepository.FindAll().ToList();

            if (!IsPostBack)
            {
                txtCode.Text = Customer.Code;
                txtName.Text = Customer.Name;
                txtPIVA.Text = Customer.PIVA;
                txtFiscalCode.Text = Customer.FiscalCode;
                txtAddress.Text = Customer.Address;
                txtCity.Text = Customer.City;
                txtZipCode.Text = Customer.ZipCode;
                txtDescription.Text = Customer.Description;
                if (Customer.MainReseller != null)
                {
                    ddlReseller.SelectedValue = Customer.MainReseller.Id;
                }
                if (Customer.MainAgent != null)
                {
                    ddlAgent.SelectedValue = Customer.MainAgent.Id;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Customer.Code = txtCode.Text;
                Customer.Name = txtName.Text;
                Customer.PIVA = txtPIVA.Text;
                Customer.FiscalCode = txtFiscalCode.Text;
                Customer.Address = txtAddress.Text;
                Customer.City = txtCity.Text;
                Customer.ZipCode = txtZipCode.Text;
                Customer.Description = txtDescription.Text;

                if (String.IsNullOrEmpty(ddlReseller.SelectedValue))
                {
                    Customer.MainReseller = null;
                }
                else
                {
                    Customer.MainReseller = mainResellerRepository.FindByID(ddlReseller.SelectedValue);
                }

                if (String.IsNullOrEmpty(ddlAgent.SelectedValue))
                {
                    Customer.MainAgent = null;
                }
                else
                {
                    Customer.MainAgent = mainAgentRepository.FindByID(ddlAgent.SelectedValue);
                }

                mainCustomerRepository.Update(Customer);
                mainCustomerRepository.SaveChanges();

                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }

    }
}