﻿<%@ Page Title="Gestione Logbooks" Language="C#" MasterPageFile="~/Host/Site.Master" AutoEventWireup="true" CodeBehind="ManageLogbooks.aspx.cs" Inherits="MainCloud.Host.ManageLogbooks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3><%: Title %></h3>

    <style>
        .logbook_info { color: green; }
        .logbook_error { color: red; }
    </style>
    <table class="table">
        <asp:Repeater ID="rptLogbooks" runat="server">
            <HeaderTemplate>
                <tr>
                    <th>Id</th>
                    <th>Date</th>
                    <th>Thread</th>
                    <th>Level</th>
                    <th>Logger</th>       
                    <th>Message</th>       
                    <th>Exception</th> 
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class='logbook_<%# Eval("Level").ToString().ToLower() %>'>
                    <td><%# Eval("Id") %></td>
                    <td><%# Eval("Date") %></td>
                    <td><%# Eval("Thread") %></td>
                    <td><%# Eval("Level") %></td>
                    <td><%# Eval("Logger") %></td>
                    <td><%# Eval("Message") %></td>
                    <td><%# Eval("Exception") %></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
