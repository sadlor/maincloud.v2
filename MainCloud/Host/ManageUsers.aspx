﻿<%@ Page Title="Manage Users" Language="C#" MasterPageFile="~/Host/Site.Master" AutoEventWireup="true" CodeBehind="ManageUsers.aspx.cs" Inherits="MainCloud.Host.ManageUsers" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <mcf:Alert runat="server" id="AlertMessage" />

    <h3><%: Title %></h3>
    <mcf:MultitenantHostNavigateLinkButton ID="LinkButton2" runat="server" class="btn btn-primary btn-xs pull-right" RouthPath="ManageUserAdd"><i class="fa fa-plus"></i> Add Existing User</mcf:MultitenantHostNavigateLinkButton>
    <mcf:MultitenantHostNavigateLinkButton ID="LinkButton1" runat="server" class="btn btn-primary btn-xs pull-right" style="margin-right: 10px" RouthPath="ManageUserRegister"><i class="fa fa-plus"></i> Add New User</mcf:MultitenantHostNavigateLinkButton>
    <br />
    <br />
    <br />
    <asp:GridView ID="DgvUsers" runat="server" DataKeyNames="ID"
        CssClass="table table-hover table-striped" GridLines="None" 
        OnRowDeleting="DgvUsers_RowDeleting" OnRowDeleted="DgvUsers_RowDeleted" OnRowEditing="DgvUsers_RowEditing" OnRowCommand="DgvUsers_RowCommand"
        AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
            <asp:BoundField DataField="UserName" HeaderText="UserName" />
            <asp:BoundField DataField="Email" HeaderText="Email" />
<%--            
            <asp:BoundField DataField="LockoutEndDateUtc" HeaderText="LockoutEndDateUtc" />
            <asp:BoundField DataField="LockoutEnabled" HeaderText="LockoutEnabled" />
--%>
            <asp:BoundField DataField="AccessFailedCount" HeaderText="AccessFailedCount" />
            <asp:TemplateField>
                <HeaderTemplate>Role</HeaderTemplate>
                <ItemTemplate>
                    <%# GetUserRole(DataBinder.Eval(Container.DataItem, "ID") as string) %>
                </ItemTemplate>
            </asp:TemplateField>
           <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" CommandName="Password" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Password</asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" class="btn btn-danger btn-xs" Visible='<%# DataBinder.Eval(Container.DataItem, "Id") as string != CurrentUserId %>'><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="cursor-pointer" />
    </asp:GridView>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
