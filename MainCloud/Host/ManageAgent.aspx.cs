﻿using MainCloudFramework;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.BasePages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Host
{
    public partial class ManageAgent : BaseHostPage
    {
        public MainAgent Agent { get; private set; }
        private MainAgentRepository mainAgentRepository = new MainAgentRepository();

        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request["Id"] as string;

            Agent = mainAgentRepository.FindByID(id);

            if (!IsPostBack)
            {
                txtName.Text = Agent.Name;
                txtSurname.Text = Agent.Surname;
                txtDescription.Text = Agent.Description;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Agent.Name = txtName.Text;
                Agent.Description = txtDescription.Text;
                Agent.Surname = txtSurname.Text;
                mainAgentRepository.Update(Agent);
                mainAgentRepository.SaveChanges();

                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }

    }
}