﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Host/Site.Master" AutoEventWireup="true" CodeBehind="ManageCustomers.aspx.cs" Inherits="MainCloud.Host.ManageCustomers" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3><%: Title %></h3>
    <mcf:Alert runat="server" id="Alert" />

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-inline">
                <div class="form-group">
                    <asp:Label runat="server" Text="Nome" AssociatedControlID="txtNewName" />
                    <asp:TextBox ID="txtNewName" runat="server"  />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNewName" CssClass="text-danger" ErrorMessage="Required filed." />
                </div>
                <asp:Button runat="server" ID="BtnAdd" class="btn btn-default" Text="Add Customer" OnClick="BtnAdd_Click" />
            </div>
        </div>
    </div>

    <asp:GridView ID="DgvMaster" runat="server" DataKeyNames="ID"
        CssClass="table table-hover table-striped" GridLines="None"
        OnRowDeleting="DgvMaster_RowDeleting" OnRowEditing="DgvMaster_RowEditing"
        AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
            <asp:BoundField DataField="Code" HeaderText="Code" />
            <asp:BoundField DataField="Name" HeaderText="Name" />
            <asp:BoundField DataField="PIVA" HeaderText="PIVA" />
            <asp:BoundField DataField="FiscalCode" HeaderText="FiscalCode" />
            <asp:BoundField DataField="Address" HeaderText="Address" />
            <asp:BoundField DataField="City" HeaderText="City" />
            <asp:BoundField DataField="MainReseller.Name" HeaderText="Reseller" />
            <asp:BoundField DataField="MainAgent.FullName" HeaderText="Agent" />
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="cursor-pointer" />
    </asp:GridView>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
