﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Host
{
    public partial class ManageApplications : BaseHostPage
    {
        private ApplicationService applicationService = new ApplicationService();

        protected void Page_PreRender(object sender, EventArgs e)
        {
            rptApplications.DataSource = applicationService.GetApplications();
            rptApplications.DataBind();
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid)
                {
                    applicationService.CreateApplication(txtApplicationName.Text, txtEmail.Text, txtAdminUserPwd.Text);
                    txtApplicationName.Text = null;
                    txtEmail.Text = null;
                    txtAdminUserPwd.Text = null;
                }
                rptApplications.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
    }
}