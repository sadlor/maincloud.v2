﻿using MainCloudFramework.Controls;
using MainCloudFramework.Models;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MainCloudFramework;
using MainCloudFramework.Services;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Validation;

namespace MainCloud.Host
{
    public partial class ManageUser : BaseHostPage
    {
        protected ApplicationUser AppUser { get; set; }
        private ApplicationService applicationService = new ApplicationService();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string userId = Request["Id"] as string;
                string appId = MultiTenantsHelper.ApplicationId;

                AppUser = applicationService.FindUserById(userId);

                if (!IsPostBack)
                {
                    txtUserName.Text = AppUser.UserName;
                    txtEmail.Text = AppUser.Email;
                    chkAllowUnlimitedSimultaneousLogin.Checked = AppUser.AllowUnlimitedSimultaneousLogin;

                    if (!string.IsNullOrEmpty(appId))
                    {
                        var appRoles = applicationService.RolesInApplication(appId);
                        lstRoles.DataSource = appRoles;
                        lstRoles.DataBind();

                        var userRole = applicationService.FindUserRoleCurrentApp(AppUser);
                        lstRoles.SelectedValue = userRole != null ? userRole.Id : null;

                        lstRoles.Visible = true;
                    }
                    else
                    {
                        lstRoles.Visible = false;
                    }
                }
            }
            catch (DbEntityValidationException exdb)
            {
                string msg = "";
                foreach (var e1 in exdb.EntityValidationErrors)
                {
                    foreach (var error in e1.ValidationErrors)
                    {
                        msg = string.Join(",", error.ErrorMessage);
                    }
                }
                AlertMessage.ShowError(msg);
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }

        protected void btnSaveUser_Click(object sender, EventArgs e)
        {
            try
            {
                AppUser.UserName = txtUserName.Text;
                AppUser.Email = txtEmail.Text;
                AppUser.AllowUnlimitedSimultaneousLogin = chkAllowUnlimitedSimultaneousLogin.Checked;
                applicationService.ChangeRoleAndUpdateUser(lstRoles.SelectedValue, AppUser);
                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            catch (DbEntityValidationException exdb)
            {
                string msg = "";
                foreach (var e1 in exdb.EntityValidationErrors)
                {
                    foreach (var error in e1.ValidationErrors)
                    {
                        msg = string.Join(",", error.ErrorMessage);
                    }
                }
                AlertMessage.ShowError(msg);
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }
    }
}