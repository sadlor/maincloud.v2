﻿<%@ Page Title="Gestione Applicazioni" Language="C#" MasterPageFile="~/Host/Site.Master" AutoEventWireup="true" CodeBehind="ManageApplications.aspx.cs" Inherits="MainCloud.Host.ManageApplications" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-inline">
                <div class="form-group">
                    <asp:Label runat="server" Text="Nome" AssociatedControlID="txtApplicationName"></asp:Label><asp:TextBox ID="txtApplicationName" runat="server" /><asp:RequiredFieldValidator ID="RequiredTxtApplicationName" runat="server" ControlToValidate="txtApplicationName" />
                </div>
                <div class="form-group">
                    <asp:Label runat="server" Text="Email" AssociatedControlID="txtEmail"></asp:Label><asp:TextBox ID="txtEmail" runat="server" /><asp:RequiredFieldValidator ID="RequiredTxtEmail" runat="server" ControlToValidate="txtEmail" />
                </div>
                <div class="form-group">
                    <asp:Label runat="server" Text="Password utente Admin" AssociatedControlID="txtAdminUSerPwd"></asp:Label><asp:TextBox ID="txtAdminUserPwd" runat="server" /><asp:RequiredFieldValidator ID="RequiredTxtAdminUserPwd" runat="server" ControlToValidate="txtAdminUserPwd" />
                </div>
                <asp:Button runat="server" ID="BtnAdd" class="btn btn-default" Text="Add Application" OnClick="BtnAdd_Click" />
            </div>
            <asp:Label runat="server" ID="lblError" ForeColor="Red" />
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><h3><%: Title %></h3></div>
        <div class="panel-body">
            <p>Selezionare una delle seguenti applicazioni o azioni per accedere.</p>
        </div>
        <table class="table">
            <asp:Repeater ID="rptApplications" runat="server">
                <HeaderTemplate>
                    <tr>
                        <th>Nome/Link</th>
                        <th>Impostazioni</th>
                        <th>Ruoli</th>
                        <th>Utenti</th>
                        <th>Logbook</th>
                        <th>Allarmi</th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><mcf:MultitenantNavigateLinkButton runat="server" Text='<%# Eval("Name") %>' RouthPath='<%# Eval("Url") %>' /></td>
                        <td><mcf:MultitenantHostNavigateLinkButton runat="server" RouthPath='<%# Eval("Url") + "/ManageSetting" %>'><i class="fa fa-gear fa-fw"></i> Settings</mcf:MultitenantHostNavigateLinkButton></td>
                        <td><mcf:MultitenantAdminNavigateLinkButton runat="server" RouthPath='<%# Eval("Url") + "/ManageRoles" %>'><i class="fa fa-users"></i> Ruoli</mcf:MultitenantAdminNavigateLinkButton></td>
                        <td><mcf:MultitenantHostNavigateLinkButton runat="server" RouthPath='<%# Eval("Url") + "/ManageUsers" %>'><i class="fa fa-user"></i> Utenti</mcf:MultitenantHostNavigateLinkButton></td>
                        <td><mcf:MultitenantHostNavigateLinkButton runat="server" RouthPath='<%# Eval("Url") + "/ManageLogbooks" %>'><i class="fa fa-book fa-fw"></i> Logbook</mcf:MultitenantHostNavigateLinkButton></td>
                        <td><mcf:MultitenantHostNavigateLinkButton runat="server" RouthPath='<%# Eval("Url") + "/ManageAlarms" %>'><i class="fa fa-bell fa-fw"></i> Allarmi</mcf:MultitenantHostNavigateLinkButton></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
