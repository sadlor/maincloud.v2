﻿using AssetManagement.Repositories;
using MainCloud.HMI.Models;
using MainCloudFramework;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Multitenants;
using MES.Configuration;
using MES.Core;
using MES.Helper;
using MES.HMI.ViewModel;
using MES.Models;
using MES.Repositories;
using MES.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using static MES.Core.MesEnum;

namespace MainCloud.HMI
{
    public partial class Taglio_Special : BasePage
    {
        #region Constants
        const string APPLICATION_ID = "application_tenant_id";
        const string MACHINE_ID = "MesMachineId";
        const string TRANSACTION_ID = "MesTransactionId";
        const string OPERATOR_ID = "MesOperatorId";
        const string JOB_ID = "MesJobId";
        const string BATCH_COUNT = "MesBatchCount";
        const string BATCH_LIST = "MesBatchList";
        const string OPERATOR_LIST = "MesOperatorList";
        const string CONFIRM_TRANSACTION = "MesConfirmTransaction";
        const string TRANSACTION_LIST_TO_MODIFY = "TransactionListToModify";
        const string IS_OEE_OPEN = "MesIsOEEOpen";
        #endregion

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("HMILogFile");

        private TransactionService tService = new TransactionService();
        private TransactionOperatorService tOperService = new TransactionOperatorService();
        private IndexService indexService = new IndexService();

        protected void Page_Load(object sender, EventArgs e)
        {
            UpdateBtnQualità();

            if (IsPostBack)
            {
                if (Request["__EVENTTARGET"] == "ctl00_MainContent_btnConfirmClosedOrder")
                {
                    if (Request["__EVENTARGUMENT"].ToString() == ((int)OrderStatusCode.Evasa).ToString())
                    {
                        CloseOrder();
                    }
                    else if (Request["__EVENTARGUMENT"].ToString() == ((int)OrderStatusCode.Sospesa).ToString())
                    {
                        SuspendOrder();
                    }
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //leggere i dati dalla sessione - la sessione può durare massimo come il turno (8 ore)
            if (Session.SessionMultitenant().ContainsKey(MACHINE_ID))
            {
                AssetRepository assetRep = new AssetRepository();
                string assetId = Session.SessionMultitenant()[MACHINE_ID].ToString();
                btnMachine.Text = assetRep.GetDescription(assetId);
                UpdateMachineState();

                btnOperator.Enabled = true;
            }
            if (Session.SessionMultitenant().ContainsKey(OPERATOR_ID))
            {
                OperatorRepository opRep = new OperatorRepository();
                string operatorId = Session.SessionMultitenant()[OPERATOR_ID].ToString();
                btnOperator.Text = opRep.GetUserName(operatorId);

                btnOrder.Enabled = true;
                btnFermi.Enabled = true;
            }
            else
            {
                btnOperator.Text = "";

                btnOrder.Enabled = false;
                btnFermi.Enabled = false;
            }
            if (Session.SessionMultitenant().ContainsKey(JOB_ID))
            {
                JobRepository jobRep = new JobRepository();
                string jobId = Session.SessionMultitenant()[JOB_ID].ToString();
                Order o = jobRep.GetOrder(jobId);
                btnOrder.Text = o?.CustomerOrder.OrderCode;//jobRep.GetCustomerOrderCode(jobId);

                txtArticleCode.Text = o?.Article?.Code;
                txtArticleDescription.Text = o?.Article?.Description;

                if (!Session.SessionMultitenant().ContainsKey(CONFIRM_TRANSACTION))
                {
                    btnBOM.Enabled = true;
                    btnPrelievo.Enabled = true;
                    btnArticleImage.Enabled = true;
                    btnPartProgram.Enabled = true;

                    btnQualità.Enabled = true;
                }

                if (pnlPrimary.Visible)
                {
                    CreateIndexes();
                }
            }
            else
            {
                btnOrder.Text = "";

                txtArticleCode.Text = "";
                txtArticleDescription.Text = "";

                btnBOM.Enabled = false;
                btnPrelievo.Enabled = false;
                btnArticleImage.Enabled = false;
                btnPartProgram.Enabled = false;

                btnQualità.Enabled = false;

                if (pnlPrimary.Visible)
                {
                    CreateIndexes();
                }
            }

            btnConfirmTransaction.Visible = Session.SessionMultitenant().ContainsKey(CONFIRM_TRANSACTION);
        }

        protected void btnConfirmTransaction_Click(object sender, EventArgs e)
        {
            //Cambia la transazione con le modifiche selezionate
            string assetId = Session.SessionMultitenant()[MACHINE_ID].ToString();
            string operatorId = string.Empty;
            if (Session.SessionMultitenant().ContainsKey(OPERATOR_ID))
            {
                operatorId = Session.SessionMultitenant()[OPERATOR_ID].ToString();
            }
            string jobId = string.Empty;
            if (Session.SessionMultitenant().ContainsKey(JOB_ID))
            {
                jobId = Session.SessionMultitenant()[JOB_ID].ToString();
            }
            if (!string.IsNullOrEmpty(jobId))
            {
                ConfigurationService config = new ConfigurationService();
                KeyValuePair<TransactionParam, object> standardRate;
                if (Convert.ToBoolean(config.GetByParameter(ParamContext.StandardRate).FirstOrDefault().Value))
                {
                    //true -> produzione oraria dalla macchina
                    AssetRepository rep = new AssetRepository();
                    standardRate = new KeyValuePair<TransactionParam, object>(TransactionParam.StandardRate, rep.GetStandardRate(assetId));
                }
                else
                {
                    //false -> produzione oraria dal job
                    JobRepository rep = new JobRepository();
                    standardRate = new KeyValuePair<TransactionParam, object>(TransactionParam.StandardRate, rep.GetStandardRate(jobId));
                }
                tService.CloseAndOpenTransaction(
                    assetId,
                    new KeyValuePair<TransactionParam, object>[] {
                    new KeyValuePair<TransactionParam, object>(TransactionParam.Operator, operatorId),
                    new KeyValuePair<TransactionParam, object>(TransactionParam.Job, jobId),
                    standardRate
                    });
            }
            else
            {
                tService.CloseAndOpenTransaction(assetId, new KeyValuePair<TransactionParam, object>(TransactionParam.Operator, operatorId));
            }

            object obj;
            Session.SessionMultitenant().TryRemove(CONFIRM_TRANSACTION, out obj);
            btnConfirmTransaction.Visible = false;
            CreateIndexes();
        }

        #region Bottoni scelta
        protected void btnMachine_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlSelectMachine.Visible = true;

            gridSelectMachine.Rebind();
        }

        protected void btnOperator_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlSelectOperator.Visible = true;

            gridOperator.Rebind();

            txtBadge.Text = tService.GetLastOperatorOnMachine(Session.SessionMultitenant()[MACHINE_ID].ToString())?.Badge;
            //gridOperator.DataSource = null;
            txtBadge_TextChanged(txtBadge, EventArgs.Empty);
        }

        protected void btnOrder_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlSelectOrder.Visible = true;

            gridSelectOrder.DataBind(); //Per azzerare la tabella
            gridOrderClosed.Rebind();
            //txtSearchOrder.Text = tService.GetLastCustomerOrderCode(Session.SessionMultitenant()[MACHINE_ID]?.ToString());
            //gridSelectOrder.Rebind();
        }

        protected void btnBOM_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlBOM.Visible = true;

            GetBOM();
        }

        protected void btnPrelievo_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlPrelievo.Visible = true;
            BindOldBatchList();
            btnAddBatch_Click(btnAddBatch, EventArgs.Empty);
        }

        protected void btnWaste_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlWaste.Visible = true;
        }

        protected void btnQualità_Click(object sender, EventArgs e)
        {
            // TODO: Modificare l'ID dell'operatore
            Response.Redirect("ControlPlan/ControlPlanExecutionList?asset=" + Session.SessionMultitenant()[MACHINE_ID].ToString() + "&operator=" + Session.SessionMultitenant()[OPERATOR_ID].ToString());
        }

        protected void btnFermi_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlFermi.Visible = true;
            //Transaction lastTrans = tService.GetLastTransactionOpen(Session.SessionMultitenant()[MACHINE_ID].ToString());
            //hiddenLogoutOperator.Value = bool.FalseString;
            //hiddenOperatorId.Value = lastTrans.OperatorId;
            //OpenFermi(AssetId, lastTrans.OperatorId);

            LoadCauseTreeView();
            gridFermi.Rebind();
        }

        protected void btnArticleImage_Click(object sender, EventArgs e)
        {
            string jobId = Session.SessionMultitenant()[JOB_ID].ToString();
            JobService service = new JobService();
            Article a = service.GetArticleByJobId(jobId);
            if (a != null)
            {
                pnlPrimary.Visible = false;
                pnlArticle.Visible = true;

                lblArticleImageCode.Text = string.Format("Articolo: {0}", a.Code);
                imgArticle.ImageUrl = a.PathImg;
            }
        }

        protected void btnPartProgram_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlPartProgram.Visible = true;

            string jobId = Session.SessionMultitenant()[JOB_ID].ToString();
            JobService service = new JobService();
            Article a = service.GetArticleByJobId(jobId);

            PartProgramService ppService = new PartProgramService();
            PartProgram pp = ppService.GetPartProgram(a.Id);
            string partProgramCode = pp?.PartProgramCode;
            if (string.IsNullOrEmpty(partProgramCode))
            {
                partProgramCode = a.Code;
            }
            txtPartProgram.Text = partProgramCode;
        }
        #endregion

        #region Machine
        private void UpdateMachineState()
        {
            Transaction lastTrans = tService.GetLastTransactionOpen(Session.SessionMultitenant()[MACHINE_ID].ToString());
            if (lastTrans != null)
            {
                Color c = tService.GetCauseColorPerTransaction(lastTrans.Id, OperatorHelper.ApplicationId);
                if (lastTrans.Cause != null)
                {
                    switch (Convert.ToInt32(lastTrans.Cause.Code))
                    {
                        case (int)MesEnum.Cause.Production:
                            btnStateProd.BackColor = c;
                            break;
                        case (int)MesEnum.Cause.Alarm:
                            btnStateAlarm.BackColor = c;
                            break;
                        case (int)MesEnum.Cause.MachineOff:
                            break;
                            //case (int)MesEnum.Cause.SetUp:
                            //    btnStateSetup.BackColor = c;
                            //    break;
                    }
                }
                else
                {
                    btnStateStop.BackColor = c;
                }
            }
        }

        protected void gridSelectMachine_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (pnlSelectMachine.Visible)
            {
                AssetRepository assetRep = new AssetRepository();
                var dataSource = assetRep.ReadAll(x => !string.IsNullOrEmpty(x.ZoneId)).OrderBy(x => x.Code.Length).ThenBy(x => x.Code).ToList();
                gridSelectMachine.DataSource = dataSource;
            }
        }

        protected void btnConfirmMachine_Click(object sender, EventArgs e)
        {
            if (gridSelectMachine.SelectedItems.Count > 0)
            {
                string zoneId = ((GridDataItem)gridSelectMachine.SelectedItems[0]).GetDataKeyValue("ZoneId").ToString();
                string assetId = ((GridDataItem)gridSelectMachine.SelectedItems[0]).GetDataKeyValue("Id").ToString();
                AutoLogIn(zoneId, assetId, "");

                //Session.SessionMultitenant().AddOrUpdate(MACHINE_ID, assetId, (key, oldValue) => assetId);
                //Session.SessionMultitenant().AddOrUpdate(TRANSACTION_ID, assetId, (key, oldValue) => assetId);
            }

            btnOperator.Enabled = true;
            pnlSelectMachine.Visible = false;
            pnlPrimary.Visible = true;
        }

        protected void btnCancelMachine_Click(object sender, EventArgs e)
        {
            pnlSelectMachine.Visible = false;
            pnlPrimary.Visible = true;
        }

        protected void AutoLogIn(string zoneId, string assetId, string returnUrl)
        {
            if (!string.IsNullOrEmpty(zoneId))
            {
                AssetManagement.Repositories.ZoneRepository zoneRep = new AssetManagement.Repositories.ZoneRepository();
                AssetManagement.Models.Zone zone = zoneRep.FindByID(zoneId);
                string userName = "zona" + zone.Code + "@mainedge.it";
                string password = "V%43HF.>[uzw'UkA" + userName.Split('@')[0];
                try
                {
                    Context.GetOwinContext().Authentication.SignOut();

                    // Validate the user password
                    var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                    var loggedinUser = manager.Find(userName, password);
                    if (loggedinUser != null)
                    {
                        // Now user have entered correct username and password.
                        // Time to change the security stamp
                        //                    manager.UpdateSecurityStamp(loggedinUser.Id);
                    }

                    // This doen't count login failures towards account lockout
                    // To enable password failures to trigger lockout, change to shouldLockout: true
                    // do sign-in AFTER we have done the update of the security stamp, so the new stamp goes into the cookie
                    var result = signinManager.PasswordSignIn(userName, password, false, shouldLockout: false);

                    //log.Info("LogIn result" + result.ToString());
                    switch (result)
                    {
                        case SignInStatus.Success:
                            //string applicationName = ApplicationSettingsHelper.GetConfiguration(ApplicationSettingsKey.OperatorDefaultApplicationUID) as string;
                            string applicationId = loggedinUser.Applications.FirstOrDefault()?.Id;
                            if (string.IsNullOrEmpty(applicationId))
                            {
                                //FailureText.Text = "Impossibile effettuare l'accesso, application non identificata";
                                //ErrorMessage.Visible = true;
                            }
                            else
                            {
                                //Session.SessionMultitenant()[MES.Core.MesConstants.DEPARTMENT_ZONE] = zoneId;
                                Session.SessionMultitenant()[MES.Core.MesConstants.APPLICATION_ID] = applicationId;
                                //IdentityHelper.RedirectToReturnUrl(returnUrl, Response);

                                Session.SessionMultitenant().AddOrUpdate(MACHINE_ID, assetId, (key, oldValue) => assetId);
                                //la macchina è cambiata quindi deve cambiare anche la commessa se era già stata inserita una in precedenza
                                object obj;
                                Session.SessionMultitenant().TryRemove(JOB_ID, out obj);
                                Session.SessionMultitenant().TryRemove(CONFIRM_TRANSACTION, out obj);

                                //Transaction t = tService.GetLastTransactionOpen(assetId);
                                //Session.SessionMultitenant().AddOrUpdate(TRANSACTION_ID, t, (key, oldValue) => t);
                                Response.Redirect(Request.RawUrl);
                            }
                            break;
                        case SignInStatus.LockedOut:
                            Response.Redirect("/Account/Lockout");
                            break;
                        case SignInStatus.Failure:
                        default:
                            //FailureText.Text = "Impossibile effettuare l'accesso";
                            //ErrorMessage.Visible = true;
                            break;
                    }
                }
                catch (Exception ex)
                {
                    //log.Error(ex.Message, ex);
                    //log.Info("errore login" + ex.Message);
                }
            }
        }
        #endregion

        #region Operator
        protected void txtBadge_TextChanged(object sender, EventArgs e)
        {
            //Controlla se esiste la matricola
            //Se esiste mostro il nome riferito alla matricola e il ruolo
            OperatorRepository OR = new OperatorRepository();
            string s = (sender as RadTextBox).Text.Trim();
            Operator op = OR.ReadAll(x => x.Badge == s && x.ApplicationId == OperatorHelper.ApplicationId).FirstOrDefault();
            if (op != null)
            {
                txtRegistrationId.Text = op.Code;
                txtUserName.Text = op.UserName;

                AddOperator(op.Id);
                gridOperator.Rebind();

                //if (ModifyMachineTransaction.Value == bool.FalseString && grantService.AllowQuality(op.Id))
                //{
                //    btnSelectWorkshift_Login.Visible = true;
                //}

                btnConfirmConnect.Enabled = true;
                btnConfirmConnect.CssClass = "btnActivity";
            }
            else
            {
                txtRegistrationId.Text = "";
                txtUserName.Text = "";

                btnConfirmConnect.Enabled = false;
                btnConfirmConnect.CssClass = "btnActivityDisabled";
            }
        }

        protected void btnConfirmConnect_Click(object sender, EventArgs e)
        {
            string assetId = Session.SessionMultitenant()[MACHINE_ID].ToString();
            SaveOperators(assetId);
            //Il primo che è entrato in macchina è l'operatore di riferimento per la macchina
            Operator op = tOperService.FirstOperatorOnMachine(assetId);
            if (op == null)
            {
                object obj;
                Session.SessionMultitenant().TryRemove(OPERATOR_ID, out obj);
                Session.SessionMultitenant().TryRemove(JOB_ID, out obj);
            }
            else
            {
                Session.SessionMultitenant().AddOrUpdate(OPERATOR_ID, op.Id, (key, oldValue) => op.Id);

                Job job = tService.GetLastTransactionOpen(assetId)?.Job;
                if (job != null && job?.Order?.StatusCode == ((int)MesEnum.OrderStatusCode.Esecutiva).ToString())
                {
                    Session.SessionMultitenant().AddOrUpdate(JOB_ID, job.Id, (key, oldValue) => job.Id);
                }
            }
            Session.SessionMultitenant().AddOrUpdate(CONFIRM_TRANSACTION, true, (key, oldValue) => true);

            txtBadge.Text = "";
            txtRegistrationId.Text = "";
            txtUserName.Text = "";

            ViewState.Remove(OPERATOR_LIST);

            btnConfirmTransaction.Visible = true;
            pnlSelectOperator.Visible = false;
            pnlPrimary.Visible = true;
        }

        protected void btnCancelConnect_Click(object sender, EventArgs e)
        {
            //object obj;
            //Session.SessionMultitenant().TryRemove(OPERATOR_ID, out obj);
            txtBadge.Text = "";
            txtRegistrationId.Text = "";
            txtUserName.Text = "";

            ViewState.Remove(OPERATOR_LIST);

            //ModifyMachineTransaction.Value = null;
            pnlSelectOperator.Visible = false;
            pnlPrimary.Visible = true;
        }

        private void AddOperator(string opId, DateTime? start = null)
        {
            start = start ?? DateTime.Now;
            if (ViewState[OPERATOR_LIST] == null)
            {
                ViewState[OPERATOR_LIST] = new List<OperatorViewModel>();
            }
            List<OperatorViewModel> list = ViewState[OPERATOR_LIST] as List<OperatorViewModel>;
            if (!list.Where(x => x.OperatorId == opId && x.Open).Any())
            {
                list.Add(new OperatorViewModel(opId, start.Value, null, true));
            }
            ViewState[OPERATOR_LIST] = list;
        }

        private void RemoveOperator(string opId)
        {
            //Chiude una transazione aperta
            List<OperatorViewModel> list = ViewState[OPERATOR_LIST] as List<OperatorViewModel>;
            list.Where(x => x.OperatorId == opId && x.Open).FirstOrDefault().DateEnd = DateTime.Now;
            list.Where(x => x.OperatorId == opId && x.Open).FirstOrDefault().Open = false;
            ViewState[OPERATOR_LIST] = list;
        }

        private void SaveOperators(string assetId)
        {
            List<OperatorViewModel> list = ViewState[OPERATOR_LIST] as List<OperatorViewModel>;
            foreach (OperatorViewModel item in list.Where(x => x.Open))
            {
                if (!tOperService.IsOperatorOnMachine(assetId, item.OperatorId))
                {
                    tOperService.OpenTransaction(assetId, item.OperatorId, MesEnum.OperationName.LogIn); //TODO: quando confermo una transazione dopo la selezione della commessa, andrebbe modificata questa riga, aggiungendo il job scelto per gli operatori collegati sulla macchina
                }
            }
            foreach (OperatorViewModel item in list.Where(x => !x.Open))
            {
                tOperService.CloseTransaction(assetId, item.OperatorId, MesEnum.OperationName.LogIn);
                if (tOperService.TransactionOpenPerOperator(assetId, item.OperatorId))
                {
                    tOperService.CloseTransaction(assetId, item.OperatorId, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
                }
            }
        }

        protected void gridOperator_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (pnlSelectOperator.Visible)
            {
                if (ViewState[OPERATOR_LIST] == null)
                {
                    List<TransactionOperator> dataSource = tOperService.OperatorsOnMachine(Session.SessionMultitenant()[MACHINE_ID].ToString());
                    //gridOperator.DataSource = dataSource;
                    foreach (var item in dataSource)
                    {
                        AddOperator(item.OperatorId, item.DateStart);
                    }
                }
                else
                {
                    gridOperator.DataSource = (ViewState[OPERATOR_LIST] as List<OperatorViewModel>).Where(x => x.Open);
                }
            }
        }

        protected void gridOperator_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;

                if (!string.IsNullOrEmpty(item["OperatorId"].Text))
                {
                    OperatorService operService = new OperatorService();
                    string opId = item["OperatorId"].Text;
                    item["OperatorId"].Text = operService.GetOperatorNameById(opId, OperatorHelper.ApplicationId);
                    //if ((gridOperator.DataSource as List<TransactionOperator>).Count == 1)
                    //{
                    //    //seleziona la prima riga
                    //    item.FireCommandEvent("Select", new GridSelectCommandEventArgs(item, null, null));
                    //}
                }
                //if (!string.IsNullOrEmpty(item["RoleId"].Text) && item["RoleId"].Text != "&nbsp;")
                //{
                //    OperatorRoleService operRoleService = new OperatorRoleService();
                //    item["RoleId"].Text = operRoleService.FindRoleById(item["RoleId"].Text).Name;
                //}
            }
        }

        protected void gridOperator_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string assetId = Session.SessionMultitenant()[MACHINE_ID].ToString();
            if (e.CommandName == RadGrid.DeleteCommandName)
            {
                //Logout
                string operatorId = (e.Item as GridDataItem).GetDataKeyValue("OperatorId").ToString();
                RemoveOperator(operatorId);

                ////verifico che chi dichiara il logout sia lo stesso che è sulla macchina
                //if (operatorId == tService.GetLastTransactionOpen(assetId)?.OperatorId)
                //{
                //    //CauseService causeService = new CauseService();
                //    //SenecaModule.Services.BoxService boxService = new SenecaModule.Services.BoxService();
                //    //if (causeService.GetListCauseIdByGroupName(new string[] { "Attrezzaggio", "Manutenzione" }, OperatorHelper.ApplicationId).Contains(tService.GetLastTransactionOpen(assetId).CauseId) || !boxService.IsMachineConnected(assetId))
                //    //{
                //    //    tService.CloseAndOpenTransaction(
                //    //        assetId,
                //    //        new KeyValuePair<MesEnum.TransactionParam, object>[] {
                //    //        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, null),
                //    //        new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Cause, null)
                //    //        });
                //    //}
                //    //else
                //    //{
                //        tService.CloseAndOpenTransaction(
                //            assetId,
                //            new KeyValuePair<MesEnum.TransactionParam, object>[] {
                //            new KeyValuePair<MesEnum.TransactionParam, object>(MesEnum.TransactionParam.Operator, null)
                //            });

                //    object obj;
                //    Session.SessionMultitenant().TryRemove(OPERATOR_ID, out obj);
                //    //}
                //}

                //tOperService.CloseTransaction(assetId, operatorId, OperationName.LogIn);
                //if (tOperService.TransactionOpenPerOperator(assetId, operatorId))
                //{
                //    tOperService.CloseTransaction(assetId, operatorId, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
                //}
            }
            //if (e.CommandName == RadGrid.SelectCommandName)
            //{
            //    string operatorId = (e.Item as GridDataItem).GetDataKeyValue("OperatorId").ToString();
            //    if (grantService.AllowConfirmStartUpProduction(operatorId))
            //    {
            //        pnlAllowConfirmProduction.Visible = true;

            //        SetTrafficLight(AssetId, operatorId);
            //    }
            //    else
            //    {
            //        pnlAllowConfirmProduction.Visible = false;
            //    }
            //}
        }
        #endregion

        #region Order
        //protected void gridSelectOrder_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        //{
        //    if (pnlSelectOrder.Visible)
        //    {
        //        JobService jobService = new JobService();
        //        if (txtSearchOrder.Visible && txtSearchOrder.Text.Length > 0)
        //        {
        //            gridSelectOrder.DataSource = jobService.GetJobListOrderCodeFiltered(txtSearchOrder.Text);
        //        }
        //        else
        //        {
        //            gridSelectOrder.DataSource = jobService.GetProductionJobListByAsset(Session.SessionMultitenant()[MACHINE_ID].ToString());
        //        }
        //    }
        //}

        protected void gridSelectOrder_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;

                if ((item.DataItem as Job).Id == tService.GetLastJobId(Session.SessionMultitenant()[MACHINE_ID].ToString()))
                {
                    //seleziona la riga
                    item.FireCommandEvent("Select", new GridSelectCommandEventArgs(item, null, null));
                }
            }
        }

        protected void btnCancelSelectOrder_Click(object sender, EventArgs e)
        {
            pnlSearchOrder.Visible = false;
            txtSearchOrder.Text = "";

            pnlSelectOrder.Visible = false;
            pnlPrimary.Visible = true;
        }

        protected void btnConfirmSelectOrder_Click(object sender, EventArgs e)
        {
            if (gridSelectOrder.SelectedItems.Count > 0)
            {
                GridDataItem item = (GridDataItem)gridSelectOrder.SelectedItems[0]; //get selected row
                string jobId = item.GetDataKeyValue("Id").ToString();
                Session.SessionMultitenant().AddOrUpdate(JOB_ID, jobId, (key, oldValue) => jobId);
                Session.SessionMultitenant().AddOrUpdate(CONFIRM_TRANSACTION, true, (key, oldValue) => true);
                JobRepository jobRep = new JobRepository();
                Order o = jobRep.GetOrder(jobId);
                if (o.StatusCode == ((int)MesEnum.OrderStatusCode.Sospesa).ToString())
                {
                    o.StatusCode = ((int)MesEnum.OrderStatusCode.Esecutiva).ToString();
                    o.StatusDescription = MesEnum.OrderStatusCode.Esecutiva.ToString();
                    OrderRepository ordRepo = new OrderRepository();
                    ordRepo.Update(o);
                    ordRepo.SaveChanges();
                }
            }

            pnlSearchOrder.Visible = false;
            txtSearchOrder.Text = "";

            btnConfirmTransaction.Visible = true;
            pnlSelectOrder.Visible = false;
            pnlPrimary.Visible = true;
        }

        protected void btnSearchOrder_Click(object sender, EventArgs e)
        {
            pnlSearchOrder.Visible = true;
        }

        protected void txtSearchOrder_TextChanged(object sender, EventArgs e)
        {
            //gridSelectOrder.Rebind();
            JobService jobService = new JobService();
            if (txtSearchOrder.Visible && txtSearchOrder.Text.Length > 0)
            {
                gridSelectOrder.DataSource = jobService.GetJobListOrderCodeFiltered(txtSearchOrder.Text);
                gridSelectOrder.DataBind();
            }
        }

        protected void btnJobGroup_Click(object sender, EventArgs e)
        {
            JobService jobService = new JobService();
            AssetRepository repo = new AssetRepository();
            string groupId = repo.GetAssetGroupId(Session.SessionMultitenant()[MACHINE_ID].ToString());
            gridSelectOrder.DataSource = jobService.GetProductionJobListByAssetGroup(groupId);
            gridSelectOrder.DataBind();
        }

        protected void btnJobAsset_Click(object sender, EventArgs e)
        {
            JobService jobService = new JobService();
            gridSelectOrder.DataSource = jobService.GetProductionJobListByAsset(Session.SessionMultitenant()[MACHINE_ID].ToString());
            gridSelectOrder.DataBind();
        }

        protected void gridOrderClosed_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (pnlSelectOrder.Visible)
            {
                Job job = tService.GetLastJob(Session.SessionMultitenant()[MACHINE_ID].ToString());
                if (job != null)
                {
                    bool closed = job.Order.StatusCode == ((int)OrderStatusCode.Evasa).ToString(),
                         suspended = job.Order.StatusCode == ((int)OrderStatusCode.Sospesa).ToString();

                    if (!closed && !suspended)
                    {
                        List<OrderCloseViewModel> dataSource = new List<OrderCloseViewModel>() { new OrderCloseViewModel(job.Id, job.Order.OrderCode, job.Order.CustomerOrder.OrderCode, job.Order.Article.Code, job.Order.Article.Description, closed, suspended) };
                        foreach (GridDataItem item in gridOrderClosed.Items)
                        {
                            if (dataSource.Where(x => x.JobId == item.GetDataKeyValue("JobId").ToString()).Any())
                            {
                                try
                                {
                                    dataSource.Where(x => x.JobId == item.GetDataKeyValue("JobId").ToString()).First().IsOrderClosed = (item.Controls[6].Controls[0] as CheckBox).Checked;
                                    dataSource.Where(x => x.JobId == item.GetDataKeyValue("JobId").ToString()).First().IsOrderSuspended = (item.Controls[7].Controls[0] as CheckBox).Checked;
                                }
                                catch (Exception ex)
                                {
                                    log.Error(ex.Message, ex);
                                }
                            }
                        }
                        gridOrderClosed.DataSource = dataSource;
                    }
                    else
                    {
                        gridOrderClosed.DataSource = new List<OrderCloseViewModel>();
                    }
                }
                else
                {
                    //gridOrderClosed.Visible = false;
                    //pnlViewOrder.Visible = true;
                    gridOrderClosed.DataSource = new List<OrderCloseViewModel>();
                }
            }
        }

        protected void SuspendOrder()
        {
            string jobId = Session.SessionMultitenant()[JOB_ID].ToString();

            ProductionPaymentService ppService = new ProductionPaymentService();
            //sospensione commessa
            ppService.SuspendOrder(jobId);
            object obj;
            Session.SessionMultitenant().TryRemove(JOB_ID, out obj);
        }

        protected void CloseOrder()
        {
            string jobId = Session.SessionMultitenant()[JOB_ID].ToString();

            ProductionPaymentService ppService = new ProductionPaymentService();
            //chiusura commessa
            ppService.CloseOrder(jobId);
            object obj;
            Session.SessionMultitenant().TryRemove(JOB_ID, out obj);

            //Creazione versamento QtyOrdered al gestionale
            ppService.CreateProductionPaymentPerJob(jobId);
            //Versamento al gestionale
            ProductionPaymentRepository repos = new ProductionPaymentRepository();
            OperatorRepository opRepos = new OperatorRepository();
            List<MES.Models.ProductionPayment> list = ppService.GetAllVerifiedNotSent();
            string connectionString = ConfigurationManager.ConnectionStrings["FluentisConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlTransaction trans = conn.BeginTransaction();
                string sql = "INSERT INTO dbo.OP_SPE_MES_Segnalazioni (OPSMS_OPFS_Id, OPSMS_CICL_Cdl, OPSMS_QtaPezziConformi, OPSMS_QtaPezziScarti, OPSMS_OrdineChiuso, OPSMS_DataRegistrazione, OPSMS_TempoMacchina, OPSMS_TempoUomo, OPSMS_CIMC_Macchina, OPSMS_Operatore)" +
                    "VALUES (@param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10)";//, getdate())";

                SqlCommand cmd = new SqlCommand(sql, conn, trans);

                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                cmd.Parameters.AddWithValue("@param1", DbType.Int32);
                cmd.Parameters.AddWithValue("@param2", DbType.String);
                cmd.Parameters.AddWithValue("@param3", DbType.Decimal);
                cmd.Parameters.AddWithValue("@param4", DbType.Decimal);
                cmd.Parameters.AddWithValue("@param5", DbType.Boolean);
                cmd.Parameters.AddWithValue("@param6", DbType.DateTime);
                cmd.Parameters.AddWithValue("@param7", DbType.Int32);
                cmd.Parameters.AddWithValue("@param8", DbType.Int32);
                cmd.Parameters.AddWithValue("@param9", DbType.String);
                cmd.Parameters.AddWithValue("@param10", DbType.String);

                try
                {
                    foreach (var item in list)//exportList)
                    {
                        cmd.Parameters[0].Value = item.JobId;
                        cmd.Parameters[1].Value = item.MachineCode;
                        cmd.Parameters[2].Value = item.QtyOK; //item.QtyProduced;
                        cmd.Parameters[3].Value = item.QtyProductionWaste;
                        cmd.Parameters[4].Value = item.IsOrderClose;
                        cmd.Parameters[5].Value = item.Start;
                        cmd.Parameters[6].Value = (int)item.Duration;
                        cmd.Parameters[7].Value = (int)item.Duration;
                        cmd.Parameters[8].Value = item.MachineCode;
                        var op = opRepos.GetBadge(item.OperatorId);
                        if (string.IsNullOrEmpty(op))
                        {
                            op = string.Empty;
                        }
                        cmd.Parameters[9].Value = op;

                        cmd.ExecuteNonQuery();
                    }
                    trans.Commit();

                    foreach (var item in list)
                    {
                        item.Verified = true;
                        item.Sent = DateTime.Now;
                        repos.Update(item);
                    }
                    repos.SaveChanges();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                }
                finally
                {
                    //foreach (var item in list)
                    //{
                    //    item.Verified = true;
                    //    item.Sent = DateTime.Now;
                    //    repos.Update(item);
                    //}
                    //repos.SaveChanges();
                    conn.Close();
                }
            }
        }
        #endregion

        #region BOM
        protected void GetBOM()
        {
            treeBOM.Nodes.Clear();
            if (Session.SessionMultitenant().ContainsKey(JOB_ID))
            {
                //JobRepository jobRep = new JobRepository();
                string jobId = Session.SessionMultitenant()[JOB_ID].ToString();
                JobService jService = new JobService();
                var dataSource = jService.GetBOM(jobId);
                RadTreeNode n = new RadTreeNode(dataSource.First().Key.Description);
                n.Expanded = true;
                foreach (Article a in dataSource.First().Value)
                {
                    n.Nodes.Add(new RadTreeNode(string.Format("{0} - {1} - {2}", a.Code, a.Description, a.Quantity)));
                }
                treeBOM.Nodes.Add(n);
            }
        }

        protected void btnCloseBOM_Click(object sender, EventArgs e)
        {
            pnlBOM.Visible = false;
            pnlPrimary.Visible = true;
        }
        #endregion

        #region Prelievo lotti materie prime
        protected void btnConfirmPrelievo_Click(object sender, EventArgs e)
        {
            //Inviare le registrazioni lotti fatte al gestionale, quindi settare a true il campo Verified dei lotti
            ReturnToERP();

            ViewState.Remove(BATCH_COUNT);
            ViewState.Remove(BATCH_LIST);

            pnlPrelievo.Visible = false;
            pnlPrimary.Visible = true;
        }

        protected void btnCancelPrelievo_Click(object sender, EventArgs e)
        {
            //Tutti i lotti letti che non sono stati inviati al gestionali (non è stato fatto un confirm in precedenza) vanno eliminati dal db
            JobBatchRepository repo = new JobBatchRepository();
            repo.DeleteBatches(Session.SessionMultitenant()[MACHINE_ID].ToString());

            //BatchList.Clear();
            ViewState.Remove(BATCH_COUNT);
            ViewState.Remove(BATCH_LIST);

            pnlPrelievo.Visible = false;
            pnlPrimary.Visible = true;
        }

        /// <summary>
        /// Invio dati di registrazione lotti al gestionale
        /// </summary>
        private void ReturnToERP()
        {
            try
            {
                JobBatchRepository repos = new JobBatchRepository();
                List<JobBatch> list = repos.GetAllNotVerified(Session.SessionMultitenant()[MACHINE_ID].ToString());

                //TODO: aggiungere funzione per invio al gestionale
                OperatorRepository opRepos = new OperatorRepository();
                AssetRepository assetRepos = new AssetRepository();
                string connectionString = ConfigurationManager.ConnectionStrings["FluentisConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlTransaction trans = conn.BeginTransaction();
                    string sql = "INSERT INTO dbo.OP_SPE_MES_Segnalazioni (OPSMS_OPFS_Id, OPSMS_CICL_Cdl, OPSMS_QtaPezziConformi, OPSMS_QtaPezziScarti, OPSMS_DataRegistrazione, OPSMS_CIMC_Macchina, OPSMS_Operatore)" +
                        "VALUES (@param1, @param2, @param3, @param4, @param5, @param6, @param7)";//, getdate())";

                    SqlCommand cmd = new SqlCommand(sql, conn, trans);

                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@param1", DbType.Int32);
                    cmd.Parameters.AddWithValue("@param2", DbType.String);
                    cmd.Parameters.AddWithValue("@param3", DbType.Decimal);
                    cmd.Parameters.AddWithValue("@param4", DbType.Decimal);
                    cmd.Parameters.AddWithValue("@param5", DbType.DateTime);
                    cmd.Parameters.AddWithValue("@param6", DbType.String);
                    cmd.Parameters.AddWithValue("@param7", DbType.String);

                    try
                    {
                        foreach (var item in list)//exportList)
                        {
                            cmd.Parameters[0].Value = item.JobId;
                            cmd.Parameters[1].Value = assetRepos.GetCode(item.MachineId);
                            cmd.Parameters[2].Value = item.Quantity;
                            cmd.Parameters[3].Value = 0;
                            //cmd.Parameters[4].Value = false;
                            cmd.Parameters[4].Value = item.Start;
                            cmd.Parameters[5].Value = assetRepos.GetCode(item.MachineId);
                            var op = opRepos.GetBadge(item.OperatorId);
                            if (string.IsNullOrEmpty(op))
                            {
                                op = string.Empty;
                            }
                            cmd.Parameters[6].Value = op;

                            cmd.ExecuteNonQuery();
                        }
                        trans.Commit();

                        foreach (JobBatch batch in list)
                        {
                            batch.Verified = true;
                            batch.Exported = DateTime.Now;
                            repos.Update(batch);
                        }
                        repos.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        log.Error(ex.Message, ex);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }
        }

        protected void btnAddBatch_Click(object sender, EventArgs e)
        {
            AddBatch();
            BindBatchList();
            (this.lvDynamicBatch.Items.Last().FindControl("txtBatch") as RadTextBox).Focus();
            btnAddBatch.Enabled = false;
        }

        /// <summary>
        /// Mostra tutti i lotti utilizzati in precedenza che sono stati confermati
        /// </summary>
        private void BindOldBatchList()
        {
            JobBatchRepository repo = new JobBatchRepository();
            List<JobBatch> dataSource = repo.GetListByJob(Session.SessionMultitenant()[JOB_ID].ToString(), true);
            lvOldBatch.DataSource = dataSource;
            lvOldBatch.DataBind();

            SetBatchToExport(dataSource);
        }

        private void BindBatchList()
        {
            try
            {
                //get the current textbox
                int count = 1;
                if (ViewState[BATCH_COUNT] != null)
                {
                    count = (int)ViewState[BATCH_COUNT];
                }

                //create an enumerable range based on the current count
                IEnumerable<int> enumerable = Enumerable.Range(1, count);

                //bind the listview
                this.lvDynamicBatch.DataSource = enumerable;
                this.lvDynamicBatch.DataBind();

                IEnumerable<ListViewDataItem> list;
                if (BatchList.Count == this.lvDynamicBatch.Items.Count)
                {
                    list = this.lvDynamicBatch.Items;
                }
                else
                {
                    list = this.lvDynamicBatch.Items.Except(this.lvDynamicBatch.Items.Where(x => x.DataItemIndex == this.lvDynamicBatch.Items.Count - 1));
                }
                foreach (ListViewItem item in list)
                {
                    if (item is ListViewDataItem && BatchList.Any())
                    {
                        BatchViewModel bvm = BatchList[item.DataItemIndex];
                        (item.FindControl("txtBatch") as RadTextBox).Text = bvm.Code;
                        (item.FindControl("txtBatchArticle") as RadTextBox).Text = bvm.ArticleCode;
                        (item.FindControl("txtBatchWidth") as RadTextBox).Text = bvm.Width.ToString();
                        (item.FindControl("txtBatchHeight") as RadTextBox).Text = bvm.Height.ToString();
                        (item.FindControl("txtBatchWeight") as RadTextBox).Text = bvm.Weight.ToString();
                        (item.FindControl("btnCancelBatch") as Button).Visible = item == list.Last();
                    }
                }
                SetBatchToExport();
            }
            catch(Exception ex)
            {
                log.Error(ex.Message, ex);
            }
        }

        private void AddBatch()
        {
            int count = 0;
            if (ViewState[BATCH_COUNT] != null)
            {
                count = (int)ViewState[BATCH_COUNT];
            }
            count++;
            ViewState[BATCH_COUNT] = count;
        }

        private void RemoveBatch()
        {
            int count = (int)ViewState[BATCH_COUNT];
            count--;
            ViewState[BATCH_COUNT] = count;
        }

        protected void btnCancelBatch_Click(object sender, EventArgs e)
        {
            JobBatchRepository repo = new JobBatchRepository();
            repo.DeleteLastBatch(Session.SessionMultitenant()[MACHINE_ID].ToString());
            RemoveBatch();
            (sender as Button).Parent.Controls.Clear();
            BatchList.RemoveAt(BatchList.Count - 1);
            BindBatchList();
        }

        private void SetBatchToExport(List<JobBatch> dataSource = null)
        {
            if (BatchList.Count > 0)
            {
                pnlBatchExport.Visible = true;
                if (BatchList.Count > 0)
                {
                    txtBatchToExport.Text = BatchList.Last().Code;
                }
                else
                {
                    if (dataSource != null && dataSource.Count > 0)
                    {
                        txtBatchToExport.Text = dataSource.Last().ArticleBatch.Code;
                    }
                }
            }
        }

        private List<BatchViewModel> BatchList
        {
            get
            {
                if (this.ViewState[BATCH_LIST] == null)
                {
                    this.ViewState[BATCH_LIST] = new List<BatchViewModel>();
                }
                return (List<BatchViewModel>)this.ViewState[BATCH_LIST];
                //return JsonConvert.DeserializeObject<List<Models.BatchViewModel>>(ViewState[BATCH_LIST].ToString());
            }
            set
            {
                this.ViewState[BATCH_LIST] = value;
            }
        }

        protected void txtBatch_TextChanged(object sender, EventArgs e)
        {
            string txtBatch = (sender as RadTextBox).Text;
            if (!string.IsNullOrEmpty(txtBatch))
            {
                using (MesDbContext db = new MesDbContext())
                //using (GenericDbContext db = new GenericDbContext())
                {
                    BatchViewModel batch = null;
                    //try
                    //{
                    //[192.168.1.4].[Fluentis2015].
                    //acquisizione altezza e peso da magazzino gestionale
                    //var batch = db.Database.SqlQuery<BatchViewModel>(
                    //batch = db.Database.SqlQuery<BatchViewModel>(
                    //    "select Lotto as Code, Articolo as ArticleCode, CAST(Quantita as decimal) as Weight, CAST(Spessore as decimal(10,3)) as Width, CAST(Larghezza as decimal) as Height " +
                    //    "from [dbo].[MG_V_SPE_GiacenzeArticoli]" +
                    //    "where Lotto = @lotto", new SqlParameter("@lotto", txtBatch))
                    //    .FirstOrDefault();
                    //var batch = db.Database.SqlQuery<BatchViewModel>(
                    //batch = db.Database.SqlQuery<BatchViewModel>(
                    //    "select Lotto as Code, Articolo as ArticleCode, CAST(Quantita as decimal) as Weight, CAST(Spessore as decimal(10,3)) as Width, CAST(Larghezza as decimal) as Height " +
                    //    "from [SRVSQL-FLU].[Fluentis2015TEST].[dbo].[MG_V_SPE_GiacenzeArticoli]" +
                    //    "where Lotto = @lotto", new SqlParameter("@lotto", txtBatch))
                    //    .FirstOrDefault();
                    batch = db.Database.SqlQuery<BatchViewModel>(
                        "select Lotto as Code, Articolo as ArticleCode, DescrizioneArticolo as ArticleDescription, CAST(Quantita as decimal) as Weight, CAST(Spessore as decimal(10,3)) as Width, CAST(Larghezza as decimal) as Height " +
                        "from [SRVSQL-FLU].[Fluentis2015TEST].[dbo].[MG_V_SPE_GiacenzeArticoli]" +
                        "where Lotto = @lotto", new SqlParameter("@lotto", txtBatch))
                        .FirstOrDefault();
                    //}
                    //catch (Exception ex)
                    //{
                    //    File.AppendAllText(@"E:\WebSites\log.txt", ex.Message);
                    //    File.AppendAllText(@"E:\WebSites\log.txt", "Problema in query a fluentis");
                    //}
                    if (batch != null)
                    {
                        string assetId = Session.SessionMultitenant()[MACHINE_ID].ToString();
                        //salvataggio lotto
                        ArticleBatchRepository rep = new ArticleBatchRepository();
                        ArticleBatch articleBatch = rep.AddOrUpdate(batch, Session.SessionMultitenant()[JOB_ID].ToString());

                        //Colonna per la formula della quantità prodotta
                        //TODO: rinominare il campo CavityMoldNum e assegnarli un nome generico
                        //TODO: Il contenuto della colonna dovrà cambiare in base a una formula associata alla macchina
                        decimal calculatedColumn = Convert.ToDecimal(articleBatch.ExtraData[ExtraData.Width.ToString()]) * Convert.ToDecimal(articleBatch.ExtraData[ExtraData.Height.ToString()]) * (decimal)7.61;
                        tService.CloseAndOpenTransaction(
                            assetId,
                            new KeyValuePair<TransactionParam, object>[] {
                            new KeyValuePair<TransactionParam, object>(TransactionParam.CavityMoldNum, calculatedColumn) }); //Chiudo e apro una nuova transazione per separare i pezzi di produzione in base al lotto

                        JobService jobService = new JobService();
                        jobService.CloseAndOpenBatch(assetId, Session.SessionMultitenant()[JOB_ID].ToString(), articleBatch.Id, Session.SessionMultitenant()[OPERATOR_ID].ToString(), articleBatch.ArticleId, articleBatch.Quantity);

                        BatchList.Add(batch);
                        if (this.lvDynamicBatch.Items.Count > 1)
                        {
                            (this.lvDynamicBatch.Items[this.lvDynamicBatch.Items.Count - 2].FindControl("btnCancelBatch") as Button).Visible = false;
                        }
                            (this.lvDynamicBatch.Items.Last().FindControl("btnCancelBatch") as Button).Visible = true;

                        btnAddBatch.Enabled = true;
                    }
                }
                BindBatchList();
            }
        }

        protected void CreateBatch()
        {
            string newBatch = FluentisModule.Helper.Helper.CreateBatchCode("H080-23-1");
        }
        #endregion

        #region Waste
        protected void btnCancelWaste_Click(object sender, EventArgs e)
        {
            pnlWaste.Visible = false;
            pnlPrimary.Visible = true;
        }

        protected void btnConfirmWaste_Click(object sender, EventArgs e)
        {
            pnlWaste.Visible = false;
            pnlPrimary.Visible = true;
        }
        #endregion

        #region Etichette
        private string FixVal(string str)
        {
            // Elimina ",00" e ",0" quando inutili | "100,00" -> "100"; "100,0" -> "100"
            // Aggiunge i "." per separare le migliaia, i milioni, ecc. "1000" -> "1.000"; "10000,00" -> "10.000"; "10.000,2" -> "10.000,2"

            if (str.Contains(","))
            {
                if (str.Length > 3 && str.Substring(str.Length - 3, 3) == ",00")
                {
                    str = str.Replace(",00", "");
                }
                if (str.Length > 2 && str.Substring(str.Length - 2, 2) == ",0")
                {
                    str = str.Replace(",0", "");
                }
            }

            if (!str.Contains(".") && str.Length > 3)
            {
                string nstr = "";
                int i = str.Contains(",") ? str.IndexOf(",") - 1 : str.Length - 1, j = 0;
                for (; i >= 0; i--, j++)
                {
                    if (j == 3)
                    {
                        nstr += "." + str.Substring(i, 1);
                        j = 0;
                    }
                    else
                    {
                        nstr += str.Substring(i, 1);
                    }
                }

                char[] charArray = nstr.ToCharArray();
                Array.Reverse(charArray);
                nstr = new string(charArray);
                if (str.Contains(","))
                    nstr += str.Substring(str.IndexOf(","), str.Length - str.IndexOf(","));

                return nstr;
            }

            return str;
        }

        protected string GetZPL(Dictionary<string, string> data)
        {
            string path = Server.MapPath("~/HMI/PrintLabel/output.zpl");
            string ZPL = File.ReadAllText(path);

            ZPL = ZPL.Replace("_CODICEMATERIALE_", data["_CODICEMATERIALE_"]);
            ZPL = ZPL.Replace("_L_", data["_L_"]);
            ZPL = ZPL.Replace("_S_", data["_S_"]);
            ZPL = ZPL.Replace("_LOTTO_", data["_LOTTO_"]);
            ZPL = ZPL.Replace("%OBARCODE%O", data["_LOTTO_"]);
            ZPL = ZPL.Replace("_PESO_", data["_PESO_"]);

            return ZPL;
        }

        protected void PrintLabel()
        {
            /*azienda = "SpecialAcciai S.p.A.",*/
            //if (BatchList.Count > 0)
            //{
                string codiceMateriale = BatchList.Last().ArticleCode, //TODO: prendi dal DB
                       spessore = BatchList.Last().Width.ToString(), //TODO: prendi dal CodiceMateriale: XXX-023-X-LARGHEZZA
                       barcode = BatchList.Last().Code,//Request.Form[barcodeVal.UniqueID], //TODO: aggiungi codice sequenza
                       larghezza = BatchList.Last().Height.ToString(),//FixVal(Request.Form[larghezzaVal.UniqueID]),
                       peso = numTxtBatchRemaining.Text;//BatchList.Last().Weight.ToString();//FixVal(Request.Form[pesoVal.UniqueID]);
            //}
            //else
            //{
            //    string codiceMateriale = BatchList.Last().ArticleCode, //TODO: prendi dal DB
            //           spessore = BatchList.Last().Width.ToString(), //TODO: prendi dal CodiceMateriale: XXX-023-X-LARGHEZZA
            //           barcode = BatchList.Last().Code, //TODO: aggiungi codice sequenza
            //           larghezza = BatchList.Last().Height.ToString(),
            //           peso = numTxtBatchRemaining.Text;
            //}

            // Printer IP Address and communication port
            string ipAddress = "192.168.1.99";//"127.0.0.1";
            int port = 9100;

            Dictionary<string, string> labelData = new Dictionary<string, string>();
            labelData["_CODICEMATERIALE_"] = codiceMateriale;
            labelData["_L_"] = larghezza;
            labelData["_S_"] = spessore;
            labelData["_LOTTO_"] = barcode;
            labelData["_PESO_"] = peso;

            // ZPL Command(s)
            string ZPLstr = GetZPL(labelData);

            try
            {
                // Open connection
                using (System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient())
                {
                    client.Connect(ipAddress, port);

                    // Write ZPL String to connection
                    using (System.IO.StreamWriter writer = new System.IO.StreamWriter(client.GetStream()))
                    {
                        writer.Write(ZPLstr);
                        writer.Flush();

                        JobService jobService = new JobService();
                        ArticleBatchRepository repos = new ArticleBatchRepository();
                        ArticleBatch batch = repos.ReadAll(x => x.Code == barcode && x.Article.Code == codiceMateriale).FirstOrDefault();
                        jobService.CloseAndOpenBatch(Session.SessionMultitenant()[MACHINE_ID].ToString(), Session.SessionMultitenant()[JOB_ID].ToString(), batch.Id, Session.SessionMultitenant()[OPERATOR_ID].ToString(), batch.ArticleId, -Convert.ToDecimal(peso), false);
                    }
                }
                //// Close Connection
                //writer.Close();
                //client.Close();
            }
            catch (Exception ex)
            {
                //// Catch Exception
                //errorMsg.Text = ex.Message;
                //dlgErrorDialog.OpenDialog();
                log.Error(ex.Message, ex);
            }
        }

        protected void btnPrintLabel_Click(object sender, EventArgs e)
        {
            PrintLabel();
        }
        #endregion

        #region Qualità
        private void UpdateBtnQualità()
        {
            //btnQualità.Enabled = isControlPlanRequired();
            //SetCssClass(btnQualità);
        }

        private bool isControlPlanRequired()
        {
            try
            {
                string AssetId = Session.SessionMultitenant()[MACHINE_ID].ToString();
                Transaction lastTrans = tService.GetLastTransactionOpen(AssetId);

                if (lastTrans?.Job != null && lastTrans?.Job?.ControlPlan != null)
                {
                    ControlPlanDetailExecutedRepository cpdeRep = new ControlPlanDetailExecutedRepository();
                    ControlPlanDetailExecuted latestCpde = cpdeRep.GetLatestByJobId(lastTrans?.Job.Id);

                    TimeSpan latestCpdeProductionTime = new TimeSpan(0);
                    decimal qtyOld = 0;

                    if (latestCpde != null)
                    {
                        latestCpdeProductionTime = new TimeSpan(latestCpde.ProductionTime);
                        qtyOld = latestCpde.QtyProduced;
                    }

                    bool cpQtyFrequencyFlag = (bool)lastTrans?.Job?.ControlPlan?.QtyFrequencyFlag;

                    if (cpQtyFrequencyFlag)
                    {
                        // Controllo sul numero di pezzi prodotti dall'ultimo controllo
                        decimal qtyFreq = (decimal)lastTrans?.Job?.ControlPlan?.QtyFrequency,
                                qtyCurrent = tService.GetQtyProducedPerJob(lastTrans?.JobId),
                                qtyDiff = qtyCurrent - qtyOld,
                                qtyTotal = (decimal)lastTrans?.Job?.Order?.QtyOrdered;

                        ////Barra qualità
                        //var positionPerc = Math.Round(((qtyCurrent + (qtyFreq - qtyDiff)) / qtyTotal) * 100, 0);
                        //if (positionPerc >= 0 && positionPerc <= 100)
                        //{
                        //    controlplanMilestoneDot.Attributes["class"] = "milestones milestone__" + positionPerc;
                        //    controlplanMilestoneLabel.Attributes["class"] = "milestones milestone__" + positionPerc;
                        //    controlplanMilestoneDot.Visible = true;
                        //    controlplanMilestoneLabel.Visible = true;
                        //}
                        //else
                        //{
                        //    controlplanMilestoneDot.Visible = false;
                        //    controlplanMilestoneLabel.Visible = false;
                        //}

                        return (qtyDiff >= qtyFreq);
                    }
                    else
                    {
                        // Controllo sul tempo trascorso dall'ultimo controllo
                        TimeSpan tf = new TimeSpan((long)lastTrans?.Job?.ControlPlan?.TimeFrequency);

                        TimeSpan freqTime = new TimeSpan(0);
                        freqTime = freqTime.Add(tf);

                        ///////////
                        TransactionRepository TRep = new TransactionRepository();
                        //Job job = tService.GetLastTransactionOpen(AssetId).Job;
                        List<Transaction> tList = new List<Transaction>();
                        string jobId = Session.SessionMultitenant()[JOB_ID].ToString();
                        tList = TRep.ReadAll(x => x.MachineId == AssetId && x.JobId == jobId).ToList();
                        TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                        ///////////////

                        TimeSpan newTime = latestCpdeProductionTime.Add(freqTime);
                        TimeSpan diffTime = productionTime - latestCpdeProductionTime;

                        ////Barra qualità
                        //var positionPerc = Math.Round((decimal)(productionTime.TotalHours + (freqTime - diffTime).TotalHours) / tempoPrevisto * 100, 0);
                        //if (positionPerc >= 0 && positionPerc <= 100)
                        //{
                        //    controlplanMilestoneDot.Attributes["class"] = "milestones milestone__" + positionPerc;
                        //    controlplanMilestoneLabel.Attributes["class"] = "milestones milestone__" + positionPerc;
                        //    controlplanMilestoneDot.Visible = true;
                        //    controlplanMilestoneLabel.Visible = true;
                        //}
                        //else
                        //{
                        //    controlplanMilestoneDot.Visible = false;
                        //    controlplanMilestoneLabel.Visible = false;
                        //}

                        return (productionTime >= newTime);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            ////Barra qualità
            //controlplanMilestoneDot.Visible = false;
            //controlplanMilestoneLabel.Visible = false;

            return false;
        }
        #endregion

        #region Fermi
        protected List<StopViewModel> TransactionListToModify
        {
            get
            {
                if (this.ViewState[TRANSACTION_LIST_TO_MODIFY] == null)
                {
                    this.ViewState[TRANSACTION_LIST_TO_MODIFY] = new List<StopViewModel>();
                }
                return (List<StopViewModel>)(this.ViewState[TRANSACTION_LIST_TO_MODIFY]);
            }
            set
            {
                this.ViewState[TRANSACTION_LIST_TO_MODIFY] = value;
            }
        }

        protected void gridFermi_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (pnlFermi.Visible)
            {
                List<Transaction> datasource = tService.TransactionToJustifyList(Session.SessionMultitenant()[MACHINE_ID].ToString());
                foreach (var t in TransactionListToModify)
                {
                    int index = datasource.IndexOf(datasource.Where(x => x.Id == t.TransactionId).First());
                    datasource.ElementAt(index).CauseId = t.CauseId;
                }
                //gridFermi.VirtualItemCount = datasource.Count;
                //int count = (((gridFermi.CurrentPageIndex + 1) * gridFermi.PageSize) > datasource.Count ? datasource.Count - 1 : (gridFermi.CurrentPageIndex + 1) * gridFermi.PageSize) - (gridFermi.CurrentPageIndex * gridFermi.PageSize);
                //gridFermi.DataSource = datasource.GetRange(gridFermi.CurrentPageIndex * gridFermi.PageSize, count);
                gridFermi.DataSource = datasource;
                
                if (TransactionListToModify.Count > 0)
                {
                    btnConfirmCausaleFermo.Enabled = true;
                    //btnConfirmCausaleFermo.CssClass = "btnActivityDisabled";
                }
                else
                {
                    btnConfirmCausaleFermo.Enabled = false;
                    //btnConfirmCausaleFermo.CssClass = "btnActivity";
                }
            }
        }

        protected void gridFermi_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                item["Status"].Text = item["Status"].Text == "False" ? "Off" : "On";
            }
        }

        protected void LoadCauseTreeView()
        {
            causeTreeView.Nodes.Clear();
            CauseService CS = new CauseService();
            //CauseTypeRepository CTR = new CauseTypeRepository();
            //List<CauseType> causeList = CTR.ReadAll(x => x.ApplicationId == OperatorHelper.ApplicationId && (x.Description == "Fermi" || x.Description == "Attrezzaggio" || x.Description == "Manutenzione")).ToList();
            //foreach (CauseType ct in causeList)
            //{
            //    //Node CauseType
            //    RadTreeNode nodeCT = new RadTreeNode(ct.Description, ct.Id);
            //    nodeCT.PostBack = false;
            //    nodeCT.Expanded = true;
            //    foreach (MES.Models.Cause c in ct.Cause.OrderBy(x => x.Code))
            //    {
            //        RadTreeNode nodeC = new RadTreeNode(c.Code + " - " + c.Description, c.Id);
            //        nodeC.PostBack = true;
            //        nodeC.BorderStyle = BorderStyle.Solid;
            //        nodeC.BorderColor = System.Drawing.Color.Black;
            //        nodeC.Height = Unit.Percentage(30);
            //        //nodeC.Width = Unit.Percentage(66);
            //        nodeCT.Nodes.Add(nodeC);
            //    }
            //    causeTreeView.Nodes.Add(nodeCT);
            //}

            //Node CauseType
            RadTreeNode nodeCT = new RadTreeNode("Causali");
            nodeCT.PostBack = false;
            nodeCT.Expanded = true;
            List<MES.Models.Cause> causeList = CS.GetListCauseByGroupName(new string[] { "Attrezzaggio", "Manutenzione", "Fermi" }, OperatorHelper.ApplicationId);
            foreach (MES.Models.Cause c in causeList.OrderBy(x => x.ExternalCode.Length).ThenBy(x => x.ExternalCode))
            {
                RadTreeNode nodeC = new RadTreeNode(c.ExternalCode + " - " + c.Description, c.Id);
                nodeC.PostBack = true;
                nodeC.BorderStyle = BorderStyle.Solid;
                nodeC.BorderColor = System.Drawing.Color.Black;
                nodeC.Height = Unit.Percentage(30);
                //nodeC.Width = Unit.Percentage(66);
                nodeCT.Nodes.Add(nodeC);
            }
            causeTreeView.Nodes.Add(nodeCT);
            //causeTreeView.DataBind();
        }

        protected void causeTreeView_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            TransactionRepository tRep = new TransactionRepository();
            //List<Transaction> transactionListToModify = new List<Transaction>();
            //if (CustomSettings.Params.ContainsKey(TRANSACTION_LIST_TO_MODIFY))
            //{
            //    transactionListToModify = JsonConvert.DeserializeObject<List<Transaction>>(CustomSettings.Params[TRANSACTION_LIST_TO_MODIFY].ToString());
            //}
            GridItemCollection listItem = gridFermi.SelectedItems; //get selected rows
            foreach (GridDataItem item in listItem)
            {
                int id = (int)item.GetDataKeyValue("Id");
                Transaction t = tRep.FindByID(id);
                t.CauseId = e.Node.Value;
                //transactionListToModify.Add(t);
                TransactionListToModify.Add(new StopViewModel(t.Id, t.CauseId));
            }
            //CustomSettings.AddOrUpdate(TRANSACTION_LIST_TO_MODIFY, transactionListToModify);

            //update table fermi
            gridFermi.Rebind();

            ////update table fermi
            //List<Transaction> datasource = tService.TransactionToJustifyList(Session.SessionMultitenant()[MACHINE_ID].ToString());
            //foreach (var t in TransactionListToModify)
            //{
            //    int index = datasource.IndexOf(datasource.Where(x => x.Id == t.TransactionId).First());
            //    datasource.ElementAt(index).CauseId = t.CauseId;
            //}
            //gridFermi.DataSource = datasource;
            //gridFermi.DataBind();
        }

        protected void btnConfirmCausaleFermo_Click(object sender, EventArgs e)
        {
            string AssetId = Session.SessionMultitenant()[MACHINE_ID].ToString();
            TransactionRepository tRep = new TransactionRepository();
            if (TransactionListToModify.Count > 0)
            {
                //List<Transaction> datasource = tService.TransactionToJustifyList(AssetId);
                TransactionRepository rep = new TransactionRepository();
                IEnumerable<int> tIdList = TransactionListToModify.Select(t => t.TransactionId);
                List<Transaction> datasource = rep.ReadAll(x => tIdList.Contains(x.Id)).ToList();
                foreach (var t in TransactionListToModify)
                {
                    int index = datasource.IndexOf(datasource.Where(x => x.Id == t.TransactionId).First());
                    datasource.ElementAt(index).CauseId = t.CauseId;
                }
                tRep.UpdateAll(datasource);
                tRep.SaveChanges();
                TransactionListToModify.Clear();
            }

            //if (hiddenLogoutOperator.Value == bool.TrueString)
            //{
            //    transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.LogIn);
            //    if (transOperService.TransactionOpenPerOperator(AssetId, hiddenOperatorId.Value))
            //    {
            //        transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
            //    }
            //}

            //RefreshPage();
            //dlgFermi.CloseDialog();
            pnlFermi.Visible = false;
            pnlPrimary.Visible = true;

            ////if (grantService.AllowProduction(hiddenOperatorId.Value))
            ////{
            ////    //chiede se la commessa è terminata oppure no
            ////    dlgCloseCustomerOrder.OpenDialog();
            ////}
        }

        protected void btnCancelCausaleFermo_Click(object sender, EventArgs e)
        {
            TransactionListToModify.Clear();

            //if (hiddenLogoutOperator.Value == bool.TrueString)
            //{
            //    transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.LogIn);
            //    if (transOperService.TransactionOpenPerOperator(AssetId, hiddenOperatorId.Value))
            //    {
            //        transOperService.CloseTransaction(AssetId, hiddenOperatorId.Value, OperationName.Attrezzaggio, OperationName.Manutenzione, OperationName.Qualità);
            //    }
            //}

            //RefreshPage();
            //dlgFermi.CloseDialog();
            pnlFermi.Visible = false;
            pnlPrimary.Visible = true;

            //if (grantService.AllowProduction(hiddenOperatorId.Value))
            //{
            //    //chiede se la commessa è terminata oppure no
            //    dlgCloseCustomerOrder.OpenDialog();
            //}
        }
        #endregion

        #region Disegno articolo
        protected void btnCloseArticleImage_Click(object sender, EventArgs e)
        {
            pnlArticle.Visible = false;
            pnlPrimary.Visible = true;
        }
        #endregion

        #region Indici
        private void CreateIndexes()
        {
            if (Session.SessionMultitenant().ContainsKey(JOB_ID) && Session.SessionMultitenant()[JOB_ID] != null)
            {
                TransactionRepository tRep = new TransactionRepository();
                JobRepository jobRep = new JobRepository();
                JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();

                string assetId = Session.SessionMultitenant()[MACHINE_ID].ToString();
                string jobId = Session.SessionMultitenant()[JOB_ID].ToString();
                //Job job = tService.GetLastTransactionOpen(assetId)?.Job;
                Job job = jobRep.ReadAll(x => x.Id == jobId).FirstOrDefault();

                List<Transaction> tList = new List<Transaction>();
                tList = tRep.ReadAll(x => x.MachineId == assetId && x.JobId == job.Id).ToList();

                TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                decimal oreProdTeorico = 0;
                decimal qtyShotProd = 0;
                decimal waste = 0;
                decimal qtyProd = 0;
                decimal cadency = 0;
                decimal availability = 0;
                decimal efficiency = 0;
                decimal quality = 0;
                decimal OEE = 0;

                decimal impronte = 1;
                Transaction lastT = tService.GetLastTransactionOpen(assetId);
                if (lastT != null)
                {
                    impronte = lastT.CavityMoldNum;
                }

                if (productionTime.TotalSeconds > 0)
                {
                    qtyShotProd = tList.Sum(x => x.PartialCounting - x.BlankShot);
                    cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                    qtyProd = tList.Sum(x => (x.PartialCounting - x.BlankShot) * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));

                    ConfigurationService config = new ConfigurationService();
                    if (Convert.ToBoolean(config.GetByParameter(ParamContext.StandardRate).FirstOrDefault().Value))
                    {
                        AssetRepository aRep = new AssetRepository();
                        job.StandardRate = aRep.GetStandardRate(assetId);
                    }
                    if (job.StandardRate > 0)
                    {
                        //oreProdTeorico += Math.Round(tList.Sum(x => x.PartialCounting - x.BlankShot) / job.StandardRate, 2);
                        oreProdTeorico += Math.Round(qtyProd / job.StandardRate, 2);
                    }

                    try
                    {
                        waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == assetId && x.JobId == job.Id).Sum(x => x.QtyProductionWaste), 0);
                        //waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.Date >= DateStart && x.Date <= DateEnd).Sum(x => x.QtyProductionWaste), 0);
                    }
                    catch (InvalidOperationException) { }
                }

                availability = indexService.Availability(productionTime, machineOn);
                efficiency = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
                quality = indexService.Quality(qtyProd, waste);
                OEE = indexService.OEE(availability / 100, efficiency / 100, quality / 100);

                List<ChartIndex> dataSource = new List<ChartIndex>();
                dataSource.Add(new ChartIndex(Math.Round(availability, 1), ParamContext.Availability));
                dataSource.Add(new ChartIndex(Math.Round(efficiency, 1), ParamContext.Efficiency));
                dataSource.Add(new ChartIndex(Math.Round(quality, 1), ParamContext.Quality));
                dataSource.Add(new ChartIndex(Math.Round(OEE, 1), ParamContext.OEE));
                chartOEE.DataSource = dataSource;
                chartOEE.DataBind();

                LoadProgressBar(assetId, job, productionTime, efficiency);
            }
        }

        protected void btnViewOEE_Click(object sender, EventArgs e)
        {
            pnlPrimary.Visible = false;
            pnlOEE.Visible = true;

            Transaction t = tService.GetLastTransactionOpen(Session.SessionMultitenant()[MACHINE_ID].ToString());

            if (!string.IsNullOrEmpty(t.JobId))
            {
                AssetRepository assetRep = new AssetRepository();
                //dlgOEE.Title = string.Format("OEE - Macchina: {0} - Commessa: {1}", assetRep.GetDescription(t.MachineId), t.Job.Order.CustomerOrder.OrderCode);
                //dlgOEE.OpenDialog();
                this.ViewState[IS_OEE_OPEN] = bool.FalseString;
                jobGrid.Rebind();
            }
        }

        protected void jobGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (this.ViewState[IS_OEE_OPEN] != null && this.ViewState[IS_OEE_OPEN].ToString() != bool.TrueString)
            {
                string assetId = Session.SessionMultitenant()[MACHINE_ID].ToString();
                TransactionRepository TRep = new TransactionRepository();
                JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();
                JobService jobService = new JobService();

                Job job = tService.GetLastTransactionOpen(assetId).Job;

                List<Transaction> tList = new List<Transaction>();
                tList = TRep.ReadAll(x => x.MachineId == assetId && x.JobId == job.Id).ToList();
                //tList = TRep.ReadAll(x => x.MachineId == AssetId && x.Start >= DateStart && x.Start <= DateEnd).ToList();

                TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
                TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

                decimal oreProdTeoricoTotale = 0;
                decimal oreProdTeorico = 0;
                decimal qtyShotProd = 0;
                decimal qtyOrdered = 0;
                decimal waste = 0;
                decimal qtyProd = 0;
                decimal cadency = 0;
                decimal availability = 0;
                decimal efficiency = 0;
                decimal quality = 0;
                decimal OEE = 0;

                qtyOrdered = jobService.GetBOM(job.Id).FirstOrDefault().Value.Sum(x => x.Quantity);

                decimal impronte = tService.GetLastTransactionOpen(assetId).CavityMoldNum;

                if (productionTime.TotalSeconds > 0)
                {
                    qtyShotProd = tList.Sum(x => x.PartialCounting - x.BlankShot);
                    qtyProd = tList.Sum(x => (x.PartialCounting - x.BlankShot) * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                    //cadency = Math.Round(qtyShotProd / (decimal)productionTime.TotalHours, 2);
                    cadency = Math.Round(qtyProd / (decimal)productionTime.TotalHours, 2);

                    ConfigurationService config = new ConfigurationService();
                    if (Convert.ToBoolean(config.GetByParameter(ParamContext.StandardRate).FirstOrDefault().Value))
                    {
                        AssetRepository aRep = new AssetRepository();
                        job.StandardRate = aRep.GetStandardRate(assetId);
                    }
                    if (job.StandardRate > 0)
                    {
                        //oreProdTeorico += Math.Round(tList.Sum(x => x.PartialCounting - x.BlankShot) / job.StandardRate, 2);
                        //oreProdTeoricoTotale += Math.Round((decimal)(job.QtyOrdered / (job.CavityMoldNum > 0 ? job.CavityMoldNum : 1)) / job.StandardRate, 2);
                        oreProdTeorico += Math.Round(qtyProd / job.StandardRate, 2);
                        oreProdTeoricoTotale += Math.Round(qtyOrdered / job.StandardRate, 2);
                    }

                    try
                    {
                        waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == assetId && x.JobId == job.Id).Sum(x => x.QtyProductionWaste), 0);
                        //waste = Math.Round(jpwRep.ReadAll(x => x.AssetId == AssetId && x.Date >= DateStart && x.Date <= DateEnd).Sum(x => x.QtyProductionWaste), 0);
                    }
                    catch (InvalidOperationException) { }
                }

                availability = indexService.Availability(productionTime, machineOn);
                efficiency = indexService.Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
                quality = indexService.Quality(qtyProd, waste);
                OEE = indexService.OEE(availability / 100, efficiency / 100, quality / 100);

                List<JobRecordViewModel> dataSource = new List<JobRecordViewModel>();

                //generalità job
                //dataSource.Add(new JobRecordViewModel("Ordinato da produrre", job.QtyOrdered, Math.Round(job.QtyOrdered / (impronte > 0 ? impronte : 1), 2), job.StandardRate, null, TimeSpan.FromHours((double)oreProdTeoricoTotale), "", "Efficienza"));
                dataSource.Add(new JobRecordViewModel("Ordinato da produrre", qtyOrdered, null, job.StandardRate, null, TimeSpan.FromHours((double)oreProdTeoricoTotale), "", "Efficienza"));

                //availability
                dataSource.Add(new JobRecordViewModel("Tempo totale produzione / disponibile", null, null, null, productionTime, machineOn, availability + "%", "Disponibilità"));

                //performance
                dataSource.Add(new JobRecordViewModel("Eseguito totale", qtyProd, qtyShotProd, cadency, productionTime, null, efficiency + "%", "Efficienza"));
                ////turni precedenti
                //decimal lastWorkshiftOreProdTeorico = 0;
                //decimal lastWorkshiftQtyShotProd = 0;
                //decimal lastWorkshiftQtyProd = 0;
                //decimal lastWorkshiftCadency = 0;
                //decimal lastWorkshiftEfficiency = 0;
                //Transaction lastTrans = tService.GetLastTransactionOpen(assetId);
                //Transaction endLastWorkshift = TRep.ReadAll(x => x.MachineId == assetId && x.JobId == job.Id && x.OperatorId != lastTrans.OperatorId && !x.Open).OrderByDescending(x => x.Start).FirstOrDefault();
                //List<Transaction> lastWorkshift = new List<Transaction>();
                //if (endLastWorkshift != null)
                //{
                //    lastWorkshift = TRep.ReadAll(x => x.MachineId == assetId && x.JobId == job.Id && !x.Open && x.Start <= endLastWorkshift.Start).ToList();
                //    //lastWorkshift.Add(endLastWorkshift);
                //}
                //if (lastWorkshift.Count > 0)  //se c'è un turno precedente per quella commessa
                //{
                //    TimeSpan lastWorkshiftProductionTime = TimeSpan.FromSeconds((double)lastWorkshift.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                //    if (lastWorkshiftProductionTime.TotalSeconds > 0)
                //    {
                //        lastWorkshiftQtyShotProd = lastWorkshift.Sum(x => x.PartialCounting - x.BlankShot);
                //        lastWorkshiftCadency = Math.Round(lastWorkshiftQtyShotProd / (decimal)lastWorkshiftProductionTime.TotalHours, 2);
                //        lastWorkshiftQtyProd = lastWorkshift.Sum(x => (x.PartialCounting - x.BlankShot) * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                //        if (job.StandardRate > 0)
                //        {
                //            lastWorkshiftOreProdTeorico += Math.Round(lastWorkshiftQtyShotProd / job.StandardRate, 2);
                //        }
                //    }
                //    lastWorkshiftEfficiency = indexService.Efficiency((decimal)lastWorkshiftProductionTime.TotalHours, lastWorkshiftOreProdTeorico);
                //    dataSource.Add(new JobRecordViewModel("Eseguito turni precedenti", lastWorkshiftQtyProd, lastWorkshiftQtyShotProd, lastWorkshiftCadency, lastWorkshiftProductionTime, null, lastWorkshiftEfficiency + "%", "Efficienza"));
                //}

                ////turno corrente
                //decimal currentWorkshiftOreProdTeorico = 0;
                //decimal currentWorkshiftQtyShotProd = 0;
                //decimal currentWorkshiftQtyProd = 0;
                //decimal currentWorkshiftCadency = 0;
                //decimal currentWorkshiftEfficiency = 0;
                //List<Transaction> currentWorkshift = new List<Transaction>();
                ////if (transService.LastTransactionOpenIsInProduction(AssetId))
                ////{
                //if (endLastWorkshift == null)
                //{
                //    //non c'è turno precedente
                //    currentWorkshift = TRep.ReadAll(x => x.MachineId == assetId && x.JobId == job.Id && x.OperatorId == lastTrans.OperatorId).ToList();
                //}
                //else
                //{
                //    currentWorkshift = TRep.ReadAll(x => x.MachineId == assetId && x.JobId == job.Id && x.OperatorId == lastTrans.OperatorId && x.Start > endLastWorkshift.Start).ToList();
                //}
                ////}
                //TimeSpan currentWorkshiftProductionTime = TimeSpan.FromSeconds((double)currentWorkshift.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
                //if (currentWorkshiftProductionTime.TotalSeconds > 0)
                //{
                //    currentWorkshiftQtyShotProd = currentWorkshift.Sum(x => x.PartialCounting - x.BlankShot);
                //    currentWorkshiftCadency = Math.Round(currentWorkshiftQtyShotProd / (decimal)currentWorkshiftProductionTime.TotalHours, 2);
                //    currentWorkshiftQtyProd = currentWorkshift.Sum(x => (x.PartialCounting - x.BlankShot) * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
                //    if (job.StandardRate > 0)
                //    {
                //        currentWorkshiftOreProdTeorico += Math.Round(currentWorkshiftQtyShotProd / job.StandardRate, 2);
                //    }
                //}
                //currentWorkshiftEfficiency = indexService.Efficiency((decimal)currentWorkshiftProductionTime.TotalHours, currentWorkshiftOreProdTeorico);
                //dataSource.Add(new JobRecordViewModel("Eseguito turno corrente", currentWorkshiftQtyProd, currentWorkshiftQtyShotProd, currentWorkshiftCadency, currentWorkshiftProductionTime, null, currentWorkshiftEfficiency + "%", "Efficienza"));

                ////efficiency
                //dataSource.Add(new JobRecord("Eseguito effettivo", qtyProd, qtyShotProd, cadency, null, productionTime, efficiency + "%", "Efficienza"));
                decimal pezziResidui = qtyOrdered - qtyProd + waste;
                //TimeSpan tempoResiduo = TimeSpan.FromHours((double)(cadency > 0 ? ((pezziResidui / (impronte > 0 ? impronte : 1)) / cadency) : 0) + productionTime.TotalHours);
                TimeSpan tempoResiduo = TimeSpan.FromHours((double)(cadency > 0 ? (pezziResidui / cadency) : 0) + productionTime.TotalHours);
                decimal wasteShotResiduo = pezziResidui % (impronte > 0 ? impronte : 1) > 0 ? (int)(pezziResidui / (impronte > 0 ? impronte : 1)) + 1 : pezziResidui / (impronte > 0 ? impronte : 1);
                dataSource.Add(
                    new JobRecordViewModel(
                        "Residuo da produrre (eccedenza) compreso scarti",
                        pezziResidui,
                        wasteShotResiduo,//Math.Round(pezziResidui / (impronte > 0 ? impronte : 1), 2),
                        null,
                        //cadency > 0 ? TimeSpan.FromHours((double)((pezziResidui / (impronte > 0 ? impronte : 1)) / cadency)) : TimeSpan.FromHours((double)(0)),
                        cadency > 0 ? TimeSpan.FromHours((double)(pezziResidui / cadency)) : TimeSpan.FromHours(0),
                        null,
                        "",
                        "Efficienza"));

                if (job.StandardRate > 0)
                {
                    //decimal tempoAssegnato = Math.Round(job.QtyOrdered / (job.StandardRate * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1)), 2);
                    decimal tempoAssegnato = Math.Round(qtyOrdered / job.StandardRate, 2);
                    decimal tempoPrevisto = 0;
                    if (efficiency > 0)
                    {
                        tempoPrevisto = Math.Round(tempoAssegnato / efficiency * 100, 2);
                    }
                    else
                    {
                        tempoPrevisto = tempoAssegnato;
                    }
                    if (tempoPrevisto > 0)
                    {
                        dataSource.Add(new JobRecordViewModel("Tempo eccedenza (recupero) e totale lotto", null, null, null, TimeSpan.FromHours((double)(tempoPrevisto - tempoAssegnato)), tempoResiduo, 100 - Math.Round(tempoAssegnato / tempoPrevisto * 100, 2) + "%", "Efficienza"));
                    }
                }

                //quality
                dataSource.Add(new JobRecordViewModel("Totale quantità prodotta", qtyProd, null, null, null, null, quality + "%", "Qualità"));
                decimal wasteShot = waste % (impronte > 0 ? impronte : 1) > 0 ? (int)(waste / (impronte > 0 ? impronte : 1)) + 1 : waste / (impronte > 0 ? impronte : 1);
                if (job.StandardRate > 0)
                {
                    dataSource.Add(new JobRecordViewModel("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours((double)(wasteShot / job.StandardRate)), null, (100 - quality) + "%", "Qualità"));
                }
                else
                {
                    dataSource.Add(new JobRecordViewModel("Totale non conformità", waste, wasteShot, null, TimeSpan.FromHours(0), null, (100 - quality) + "%", "Qualità"));
                }

                jobGrid.DataSource = dataSource;

                meterAvailability.Pointer.Value = availability;
                txtAvailability.Text = availability.ToString();
                meterEfficiency.Pointer.Value = efficiency;
                txtEfficiency.Text = efficiency.ToString();
                meterQuality.Pointer.Value = quality;
                txtQuality.Text = quality.ToString();
                meterOEE.Pointer.Value = OEE;
                txtOEE.Text = OEE.ToString();

                this.ViewState[IS_OEE_OPEN] = bool.TrueString;
            }
        }

        protected void jobGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridGroupHeaderItem)
            {
                GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;
                DataRowView groupDataRow = (DataRowView)e.Item.DataItem;
                item.DataCell.Text = groupDataRow["GroupType"].ToString();
            }
        }

        protected void btnCloseOEE_Click(object sender, EventArgs e)
        {
            this.ViewState.Remove(IS_OEE_OPEN);
            pnlOEE.Visible = false;
            pnlPrimary.Visible = true;
        }

        private void LoadProgressBar(string assetId, Job job, TimeSpan productionTime, decimal efficiency)
        {
            TransactionRepository TRep = new TransactionRepository();
            //JobRepository jobRep = new JobRepository();

            decimal qtyOrdered;
            decimal qtyProduced = 0;
            decimal qtyResidual;

            //string assetId = Session.SessionMultitenant()[MACHINE_ID].ToString();
            //string jobId = Session.SessionMultitenant()[JOB_ID].ToString();
            //qtyOrdered = jobRep.GetQtyOrdered(jobId);
            JobService jobService = new JobService();
            qtyOrdered = jobService.GetBOM(job.Id).FirstOrDefault().Value.Sum(x => x.Quantity);

            List<Transaction> tList = new List<Transaction>();
            tList = TRep.ReadAll(x => x.MachineId == assetId && x.JobId == job.Id).ToList();

            //TimeSpan machineOn = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));
            //TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
            //TimeSpan stopTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code != ((int)MesEnum.Cause.Production).ToString() && x.Cause?.Code != ((int)MesEnum.Cause.MachineOff).ToString()).Sum(x => x.Duration));

            decimal productionHour = (decimal)productionTime.TotalHours;

            if (productionTime.TotalSeconds > 0)
            {
                qtyProduced = tList.Sum(x => x.PartialCounting * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
            }

            qtyResidual = qtyOrdered - qtyProduced;

            if (qtyOrdered > 0)
            {
                if (job.StandardRate > 0)
                {
                    //decimal tempoAssegnato = Math.Round(job.QtyOrdered / (job.StandardRate * (job.CavityMoldNum > 0 ? job.CavityMoldNum.Value : 1)), 2);
                    decimal tempoAssegnato = Math.Round(qtyOrdered / job.StandardRate, 2);
                    decimal tempoPrevisto = 0;
                    if (efficiency > 0)
                    {
                        tempoPrevisto = Math.Round(tempoAssegnato / efficiency * 100, 2);
                    }
                    else
                    {
                        tempoPrevisto = tempoAssegnato;
                    }

                    if (tempoPrevisto > 0)
                    {
                        barTheoreticalTime.Attributes["aria-valuenow"] = Math.Round(tempoAssegnato / tempoPrevisto * 100, 2).ToString().Replace(",", ".");
                        barTheoreticalTime.Attributes["style"] = string.Format("z-index:1000; font-size: large; font-weight: bold; width:{0}%", Math.Round(tempoAssegnato / tempoPrevisto * 100, 2)).Replace(",", ".");
                        barTheoreticalTime.InnerText = string.Format("{0:n1} h", tempoAssegnato);

                        textEstimatedTime.Attributes["style"] = string.Format("z-index:1000; font-size: large; font-weight: bold;  text-align:right; width:{0}%", 100 - Math.Round(tempoAssegnato / tempoPrevisto * 100, 2)).Replace(",", ".");
                        textEstimatedTime.InnerText = string.Format("{0:n1} h", tempoPrevisto - tempoAssegnato);
                    }
                }

                if (qtyProduced > 0)
                {
                    decimal leftTime = (productionHour * qtyResidual / qtyProduced);
                    decimal tempoPrevisto = productionHour + leftTime;
                    barProducedTime.Attributes["aria-valuenow"] = Math.Round((decimal)productionTime.TotalHours / tempoPrevisto * 100, 2).ToString().Replace(",", ".");
                    barProducedTime.Attributes["style"] = string.Format("font-size: large; font-weight: bold; text-align:center; float:left; width:{0}%", Math.Round((decimal)productionTime.TotalHours / tempoPrevisto * 100, 2)).Replace(",", ".");
                    barProducedTime.InnerText = string.Format("{0} h", Math.Round(productionTime.TotalHours, 1));

                    //decimal leftTime = tempoPrevisto - (decimal)productionTime.TotalHours;
                    barLeftTime.Attributes["aria-valuenow"] = Math.Round(leftTime / tempoPrevisto * 100, 2).ToString().Replace(",", ".");
                    barLeftTime.Attributes["style"] = string.Format("background-color:#f5f5f5 !important; font-size: large; font-weight: bold; color: black; text-align:right; float:left; width:{0}%", Math.Round(leftTime / tempoPrevisto * 100, 2)).Replace(",", ".");
                    barLeftTime.InnerText = string.Format("{0:n1} h", leftTime);
                }
            }
        }
        #endregion

        #region PartProgram
        protected void btnClosePartProgram_Click(object sender, EventArgs e)
        {
            pnlPartProgram.Visible = false;
            pnlPrimary.Visible = true;
        }
        #endregion

        //protected void Export(string assetId = null)//(string methodExport, string assetId = null)
        //{
        //    if (string.IsNullOrEmpty(assetId))
        //    {
        //        if (gridTransaction.SelectedItems.Count > 0)
        //        {
        //            List<Transaction> tList = new List<Transaction>();
        //            TransactionRepository transRepos = new TransactionRepository();
        //            foreach (GridDataItem item in gridTransaction.SelectedItems)
        //            {
        //                //tList.Add(transRepos.FindByID(Convert.ToInt32(item.GetDataKeyValue("Id"))));
        //                string jobId = item.GetDataKeyValue("JobId").ToString();
        //                string operatorId = item.GetDataKeyValue("OperatorId") == null ? null : item.GetDataKeyValue("OperatorId").ToString();
        //                string machineId = item.GetDataKeyValue("MachineId").ToString();
        //                DateTime start = Convert.ToDateTime(item.GetDataKeyValue("Start"));
        //                List<Transaction> l = transRepos.ReadAll(x => x.MachineId == machineId && (x.PartialCounting > 0 || x.QtyProductionWaste > 0) && !x.Exported && x.Verified && !string.IsNullOrEmpty(x.JobId) && !x.Open).ToList();
        //                tList.AddRange(l.Where(x => x.JobId == jobId && x.OperatorId == operatorId && x.Start.Date == start.Date));
        //            }
        //            ppService.CreateProductionPaymentPerDay(tList, true);
        //        }
        //    }
        //    else
        //    {
        //        List<Transaction> tList = ppService.TransactionListPerMachine(assetId);
        //        ppService.CreateProductionPaymentPerDay(tList, true);
        //    }

        //    gridMachine.Rebind();
        //    pnlTransactions.Visible = false;
        //    hiddenMachineId.Value = null;

        //    ProductionPaymentRepository repos = new ProductionPaymentRepository();
        //    OperatorRepository opRepos = new OperatorRepository();
        //    List<MES.Models.ProductionPayment> list = ppService.GetAllVerifiedNotSent();
        //    //List<MES.Models.ProductionPayment> exportList = ppService.AggregateToExport();

        //    string connectionString = ConfigurationManager.ConnectionStrings["FluentisConnection"].ConnectionString;
        //    //string tableName = connectionString.Split(';').Where(x => x.StartsWith("initial catalog", StringComparison.OrdinalIgnoreCase)).First().Split('=')[1];
        //    using (SqlConnection conn = new SqlConnection(connectionString))
        //    {
        //        conn.Open();

        //        SqlTransaction trans = conn.BeginTransaction();
        //        string sql = "INSERT INTO dbo.OP_SAT_MES_Segnalazioni (OPSMS_OPFS_Id, OPSMS_CICL_Cdl, OPSMS_QtaPezziConformi, OPSMS_QtaPezziScarti, OPSMS_OrdineChiuso, OPSMS_DataRegistrazione, OPSMS_TempoMacchina, OPSMS_TempoUomo, OPSMS_CIMC_Macchina, OPSMS_Operatore)" +
        //            "VALUES (@param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10)";//, getdate())";

        //        SqlCommand cmd = new SqlCommand(sql, conn, trans);

        //        cmd.CommandType = CommandType.Text;
        //        cmd.Connection = conn;
        //        cmd.Parameters.AddWithValue("@param1", DbType.Int32);
        //        cmd.Parameters.AddWithValue("@param2", DbType.String);
        //        cmd.Parameters.AddWithValue("@param3", DbType.Decimal);
        //        cmd.Parameters.AddWithValue("@param4", DbType.Decimal);
        //        cmd.Parameters.AddWithValue("@param5", DbType.Boolean);
        //        cmd.Parameters.AddWithValue("@param6", DbType.DateTime);
        //        cmd.Parameters.AddWithValue("@param7", DbType.Int32);
        //        cmd.Parameters.AddWithValue("@param8", DbType.Int32);
        //        cmd.Parameters.AddWithValue("@param9", DbType.String);
        //        cmd.Parameters.AddWithValue("@param10", DbType.String);

        //        try
        //        {
        //            foreach (var item in list)//exportList)
        //            {
        //                cmd.Parameters[0].Value = item.JobId;
        //                cmd.Parameters[1].Value = item.MachineCode;
        //                cmd.Parameters[2].Value = item.QtyOK; //item.QtyProduced;
        //                cmd.Parameters[3].Value = item.QtyProductionWaste;
        //                cmd.Parameters[4].Value = item.IsOrderClose;
        //                cmd.Parameters[5].Value = item.Start;
        //                cmd.Parameters[6].Value = (int)item.Duration;
        //                cmd.Parameters[7].Value = (int)item.Duration;
        //                cmd.Parameters[8].Value = item.MachineCode;
        //                var op = opRepos.GetBadge(item.OperatorId);
        //                if (string.IsNullOrEmpty(op))
        //                {
        //                    op = string.Empty;
        //                }
        //                cmd.Parameters[9].Value = op;

        //                cmd.ExecuteNonQuery();
        //            }
        //            trans.Commit();

        //            foreach (var item in list)
        //            {
        //                item.Verified = true;
        //                item.Sent = DateTime.Now;
        //                repos.Update(item);
        //            }
        //            repos.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {
        //            trans.Rollback();
        //        }
        //        finally
        //        {
        //            //foreach (var item in list)
        //            //{
        //            //    item.Verified = true;
        //            //    item.Sent = DateTime.Now;
        //            //    repos.Update(item);
        //            //}
        //            //repos.SaveChanges();
        //            conn.Close();
        //        }
        //    }
        //}

        protected void Timer1_Tick(object sender, EventArgs e)
        {
        }
    }
}