﻿using AssetManagement.Repositories;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Multitenants;
using MES.Core;
using MES.Helper;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;

namespace MainCloud.HMI.ControlPlan
{
    public partial class ControlPlanExecutionList : BasePage
    {
        private object jobId, jobDescription, orderId, orderCode, articleId, articleCode, articleDescription, operatorCode, operatorUserName, cpId, cpDescription;
        private TransactionService transService = new TransactionService();
        private List<ControlPlanDetail> cpd;

        public string ApplicationId
        {
            get
            {
                try
                {
                    return (string)Session.SessionMultitenant()[MES.Core.MesConstants.APPLICATION_ID];
                }
                catch (Exception ex) { }
                return null;
                //return "407e1c36-cbbe-4cbc-9ff2-9202e2f79cce";//null;            
            }
        }

        protected string operatorId
        {
            get
            {
                return Request.QueryString["operator"];
            }
        }

        protected string assetId
        {
            get
            {
                return Request.QueryString["asset"];
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            LoadTitle();
        }

        private void LoadTitle()
        {
            try
            {
                string zoneId = Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString();
                ZoneRepository zoneRep = new ZoneRepository();
                AssetRepository assetRep = new AssetRepository();
                TransactionService TService = new TransactionService();
                Page.Title = string.Format("{0} - {1} - Controllo Qualità - Operatore: {2}", zoneRep.GetDescription(zoneId), assetRep.GetDescription(assetId), operatorId);
            }
            catch (Exception ex) { }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (assetId != null)
            {
                loadData();

                lblOpCode.Text = (string)operatorCode;
                lblOpUserName.Text = (string)operatorUserName;
                lblArticleCode.Text = (string)articleCode;
                lblArticleDescription.Text = (string)articleDescription;
                lblOrder.Text = (string)orderCode;
                lblJob.Text = (string)jobDescription;
                lblCpDescription.Text = (string)cpDescription;

                if (lblOpCode.Text == "") lblOpCode.Text = "---";
                if (lblOpUserName.Text == "") lblOpUserName.Text = "---";
                if (lblArticleCode.Text == "") lblArticleCode.Text = "---";
                if (lblArticleDescription.Text == "") lblArticleDescription.Text = "---";
                if (lblOrder.Text == "") lblOrder.Text = "---";
                if (lblJob.Text == "") lblJob.Text = "---";
                if (lblCpDescription.Text == "") lblCpDescription.Text = "---";
            }
        }

        protected void loadData()
        {
            Transaction lastTrans = transService.GetLastTransactionOpen(assetId);

            // Get Operator
            OperatorRepository opRep = new OperatorRepository();
            Operator op = opRep.FindByID(operatorId);
            if (op != null)
            {
                operatorCode = op.Code;
                operatorUserName = op.UserName;
            }

            if (lastTrans?.Job != null)
            {
                // Get Job
                jobId = lastTrans?.Job?.Id;

                // Get Job Description
                jobDescription = lastTrans?.Job?.Description;

                // Get Order
                orderId = lastTrans?.Job?.Order?.Id;
                orderCode = lastTrans?.Job?.Order?.OrderCode;

                if (lastTrans?.Job?.Order?.Article != null)
                {
                    // Get Article Id
                    articleId = lastTrans?.Job?.Order?.Article?.Id;

                    // Get Article Code
                    articleCode = lastTrans?.Job?.Order?.Article?.Code;

                    // Get Article Description
                    articleDescription = lastTrans?.Job?.Order?.Article?.Description;

                    if (lastTrans?.Job?.ControlPlan != null)
                    {
                        // Get Control Plan Id
                        cpId = lastTrans?.Job?.ControlPlan?.Id;

                        // Get Control Plan Description
                        cpDescription = lastTrans?.Job?.ControlPlan?.Description;

                        // Get Control Plan Definitions
                        ControlPlanDetailRepository cpdRep = new ControlPlanDetailRepository();
                        List<ControlPlanDetail> cpdList = cpdRep.GetAllByControlPlanId(Convert.ToInt32(cpId));
                        cpd = new List<ControlPlanDetail>();
                        int numControlli = (int)lastTrans?.Job?.ControlPlan?.ControlsCountNum;
                        for (int x = 0; x < numControlli; x++)
                        {
                            foreach (ControlPlanDetail cpdItem in cpdList)
                            {
                                ControlPlanDetail cpdI = new ControlPlanDetail();
                                cpdI.Id = cpdItem.Id;
                                cpdI.IdControllo = x + 1;
                                cpdI.Operator = cpdItem.Operator;
                                cpdI.Tolerance1Sign = cpdItem.Tolerance1Sign;
                                cpdI.Tolerance1Value = cpdItem.Tolerance1Value;
                                cpdI.Tolerance2Sign = cpdItem.Tolerance2Sign;
                                cpdI.Tolerance2Value = cpdItem.Tolerance2Value;
                                cpdI.ToolCode = cpdItem.ToolCode;
                                cpdI.Udm = cpdItem.Udm;
                                cpdI.ApplicationId = cpdItem.ApplicationId;
                                cpdI.BaseValue = cpdItem.BaseValue;
                                cpdI.ControlPlanId = cpdItem.ControlPlanId;
                                cpdI.ControlType = cpdItem.ControlType;
                                cpdI.Description = cpdItem.Description;
                                cpdI.DetailNotes = cpdItem.DetailNotes;

                                cpd.Add(cpdI);
                            }
                        }
                        cpdTable.MasterTableView.GetColumn("IdControllo").Visible = (numControlli > 1);
                    }
                }
            }
        }

        protected void cpdTable_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            (sender as RadGrid).DataSource = cpd;
        }


        protected void btnSaveData_Click(object sender, EventArgs e)
        {
            dlgConfirmDialog.OpenDialog();
        }

        protected void goBack()
        {
            Response.Redirect(@"..\Taglio.aspx");
        }

        protected void btnCloseData_Click(object sender, EventArgs e)
        {
            goBack();
        }

        protected void saveData()
        {
            string sep = "|&|";
            string udms = Request.Form[udmList.UniqueID],
                   ops = Request.Form[operatorList.UniqueID],
                   baseValues = Request.Form[baseValueList.UniqueID],
                   tol1Signs = Request.Form[tol1SignList.UniqueID],
                   tol1Values = Request.Form[tol1ValueList.UniqueID],
                   tol2Signs = Request.Form[tol2SignList.UniqueID],
                   tol2Values = Request.Form[tol2ValueList.UniqueID],
                   toolCodes = Request.Form[toolCodeList.UniqueID],
                   idcs = Request.Form[idcontrolloList.UniqueID],
                   controlTypes = Request.Form[controlTypeList.UniqueID],
                   measuredValues = Request.Form[valuesList.UniqueID],
                   results = Request.Form[resultsList.UniqueID];
            decimal qtyProduced = transService.GetQtyProducedPerJob((string)jobId);

            TransactionRepository TRep = new TransactionRepository();
            JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();
            List<Transaction> tList = new List<Transaction>();
            tList = TRep.ReadAll(x => x.MachineId == assetId && x.JobId == (string)jobId).ToList();
            TimeSpan productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));

            if (measuredValues.Contains(sep))
            {
                List<string> udmsArr = new List<string>(udms.Split(new[] { sep }, StringSplitOptions.None)),
                             opsArr = new List<string>(ops.Split(new[] { sep }, StringSplitOptions.None)),
                             bvArr = new List<string>(baseValues.Split(new[] { sep }, StringSplitOptions.None)),
                             t1sArr = new List<string>(tol1Signs.Split(new[] { sep }, StringSplitOptions.None)),
                             t1vArr = new List<string>(tol1Values.Split(new[] { sep }, StringSplitOptions.None)),
                             t2sArr = new List<string>(tol2Signs.Split(new[] { sep }, StringSplitOptions.None)),
                             t2vArr = new List<string>(tol2Values.Split(new[] { sep }, StringSplitOptions.None)),
                             tcArr = new List<string>(toolCodes.Split(new[] { sep }, StringSplitOptions.None)),
                             idcArr = new List<string>(idcs.Split(new[] { sep }, StringSplitOptions.None)),
                             ctArr = new List<string>(controlTypes.Split(new[] { sep }, StringSplitOptions.None)),
                             mvArr = new List<string>(measuredValues.Split(new[] { sep }, StringSplitOptions.None)),
                             resArr = new List<string>(results.Split(new[] { sep }, StringSplitOptions.None));

                //if (mvArr.Contains(""))
                //{
                //    dlgErrorDialog.OpenDialog();
                //}
                //else
                //{
                ControlPlanDetailExecutedRepository cpdeRep = new ControlPlanDetailExecutedRepository();
                int planID = 0; // Identificativo piano di controllo
                for (int i = 0; i < mvArr.Count; i++)
                {
                    if (mvArr[i] != "")
                    {
                        ControlPlanDetailExecuted cpde = new ControlPlanDetailExecuted();
                        cpde.JobId = (string)jobId;
                        cpde.ControlPlanDetail = cpd[i];
                        cpde.Description = cpd[i].Description;
                        cpde.ArticleId = (int)articleId;
                        cpde.AssetId = assetId;
                        cpde.OperatorId = (string)operatorId;
                        cpde.Udm = udmsArr[i];
                        cpde.BaseValue = Convert.ToDecimal(bvArr[i]);
                        cpde.Tolerance1Value = Convert.ToDecimal(t1vArr[i]);
                        cpde.Tolerance1Sign = t1sArr[i];
                        cpde.Tolerance2Value = Convert.ToDecimal(t2vArr[i]);
                        cpde.Tolerance2Sign = t2sArr[i];
                        cpde.ToolCode = tcArr[i];
                        cpde.IdControllo = (idcArr[i] != "") ? Convert.ToInt32(idcArr[i]) : (int?)null;
                        cpde.MeasuredValue = (ctArr[i] == "Dimensionale") ? Convert.ToDecimal(mvArr[i]) : 0;
                        cpde.BooleanValue = (ctArr[i] == "Visivo") ? (mvArr[i] == "Ok" ? true : false) : true;
                        cpde.ControlType = ctArr[i];
                        cpde.ValueOperator = opsArr[i];
                        cpde.ApplicationId = ApplicationId;
                        cpde.ExecutionDate = DateTime.Now;
                        cpde.QtyProduced = qtyProduced;
                        cpde.ProductionTime = productionTime.Ticks;
                        cpde.ResultValue = (resArr[i] == "Ok") ? true : false;

                        cpdeRep.Insert(cpde);
                        cpdeRep.SaveChanges();

                        // Aggiungo il Client Id
                        ControlPlanDetailExecuted cpdeN = cpdeRep.FindByID(cpde.Id);
                        cpdeN.ClientId = cpde.Id;
                        if (i == 0) planID = cpde.Id;
                        cpdeN.PlanId = planID;
                        cpdeRep.Update(cpdeN);
                        cpdeRep.SaveChanges();
                    }
                }
                goBack();
                //}
            }
            else
            {
                dlgErrorDialog.OpenDialog();
            }
        }

        protected void dlgConfirm_Si_Click(object sender, EventArgs e)
        {
            saveData();
            dlgConfirmDialog.CloseDialog();
        }

        protected void dlgConfirm_No_Click(object sender, EventArgs e)
        {
            dlgConfirmDialog.CloseDialog();
        }

        protected void dlgErrorOk_Click(object sender, EventArgs e)
        {
            dlgErrorDialog.CloseDialog();
        }
    }
}