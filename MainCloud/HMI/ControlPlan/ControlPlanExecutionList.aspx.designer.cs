﻿//------------------------------------------------------------------------------
// <generato automaticamente>
//     Codice generato da uno strumento.
//
//     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
//     il codice viene rigenerato. 
// </generato automaticamente>
//------------------------------------------------------------------------------

namespace MainCloud.HMI.ControlPlan {
    
    
    public partial class ControlPlanExecutionList {
        
        /// <summary>
        /// Controllo dlgConfirmDialog.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MainCloudFramework.Web.PopUpDialog dlgConfirmDialog;
        
        /// <summary>
        /// Controllo dlgConfirmSi.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::Telerik.Web.UI.RadButton dlgConfirmSi;
        
        /// <summary>
        /// Controllo dlgConfirmNo.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::Telerik.Web.UI.RadButton dlgConfirmNo;
        
        /// <summary>
        /// Controllo dlgErrorDialog.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::MainCloudFramework.Web.PopUpDialog dlgErrorDialog;
        
        /// <summary>
        /// Controllo dlgErrorOk.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::Telerik.Web.UI.RadButton dlgErrorOk;
        
        /// <summary>
        /// Controllo lblOpCode.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblOpCode;
        
        /// <summary>
        /// Controllo lblOpUserName.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblOpUserName;
        
        /// <summary>
        /// Controllo lblArticleCode.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblArticleCode;
        
        /// <summary>
        /// Controllo lblArticleDescription.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblArticleDescription;
        
        /// <summary>
        /// Controllo lblOrder.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblOrder;
        
        /// <summary>
        /// Controllo lblJob.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblJob;
        
        /// <summary>
        /// Controllo lblCpDescription.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblCpDescription;
        
        /// <summary>
        /// Controllo cpdTable.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::Telerik.Web.UI.RadGrid cpdTable;
        
        /// <summary>
        /// Controllo btnSaveData.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::Telerik.Web.UI.RadButton btnSaveData;
        
        /// <summary>
        /// Controllo btnCloseData.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::Telerik.Web.UI.RadButton btnCloseData;
        
        /// <summary>
        /// Controllo udmList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField udmList;
        
        /// <summary>
        /// Controllo operatorList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField operatorList;
        
        /// <summary>
        /// Controllo baseValueList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField baseValueList;
        
        /// <summary>
        /// Controllo tol1SignList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField tol1SignList;
        
        /// <summary>
        /// Controllo tol1ValueList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField tol1ValueList;
        
        /// <summary>
        /// Controllo tol2SignList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField tol2SignList;
        
        /// <summary>
        /// Controllo tol2ValueList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField tol2ValueList;
        
        /// <summary>
        /// Controllo toolCodeList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField toolCodeList;
        
        /// <summary>
        /// Controllo controlTypeList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField controlTypeList;
        
        /// <summary>
        /// Controllo idcontrolloList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField idcontrolloList;
        
        /// <summary>
        /// Controllo valuesList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField valuesList;
        
        /// <summary>
        /// Controllo resultsList.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField resultsList;
        
        /// <summary>
        /// Controllo selectedInputBoxIndex.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField selectedInputBoxIndex;
    }
}
