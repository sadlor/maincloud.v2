﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/HMI/MainDashboardHMI.Master" CodeBehind="ControlPlanExecutionList.aspx.cs" Inherits="MainCloud.HMI.ControlPlan.ControlPlanExecutionList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="..\Style\Style.css?111" rel="stylesheet" type="text/css">
    <%--<script type="text/javascript" src="Script.js"></script>--%>
    <link href="ControlPlanExecutionListStyle.css" rel="stylesheet" />
    <link href="..\Style\jquery.numpad.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <script>
        function removeModal() {
            //$("body").removeClass("modal-open");
            //$(".modal-backdrop.in").remove();
            __doPostBack('<%=dlgConfirmSi.ClientID %>', "refresh");
        }
    </script>

<%--<asp:UpdatePanel runat="server">
    <ContentTemplate>--%>
    <mcf:PopUpDialog ID="dlgConfirmDialog" runat="server" Title="Conferma">
        <Body>
            <asp:Label Text="Confermare i controlli inseriti?" runat="server" /><br />
            <br />
            <telerik:RadButton runat="server" ID="dlgConfirmSi" ClientIDMode="Static" Text="Si" OnClientClicking="removeModal" OnClick="dlgConfirm_Si_Click"></telerik:RadButton>
            <telerik:RadButton runat="server" ID="dlgConfirmNo" ClientIDMode="Static" Text="No" OnClientClicking="removeModal" OnClick="dlgConfirm_No_Click"></telerik:RadButton>
        </Body>
    </mcf:PopUpDialog>

    <mcf:PopUpDialog ID="dlgErrorDialog" runat="server" Title="Errore">
        <Body>
            <asp:Label Text="Non sono stati compilati tutti i campi del piano di controllo." runat="server" /><br />
            <br />
            <telerik:RadButton runat="server" ID="dlgErrorOk" ClientIDMode="Static" Text="Ok" OnClientClicking="removeModal" OnClick="dlgErrorOk_Click"></telerik:RadButton>
        </Body>
    </mcf:PopUpDialog>

    <div style="width: 100%;">
        <div class="headerDiv">
            <div style="width: 6%;">
                <asp:Label runat="server" Text="Cod." />
                <asp:Label class="cpeBkTitolo " ID="lblOpCode" Width="100%" runat="server" Text="---" />
            </div>
            <div style="width: 25%;">
                <asp:Label runat="server" Text="Operatore" />
                <asp:Label class="cpeBkTitolo " ID="lblOpUserName" Width="100%" runat="server" Text="---" />
            </div>
            <div style="width: 20%;">
                <asp:Label runat="server" Text="Cod. Articolo" />
                <asp:Label class="cpeBkTitolo " ID="lblArticleCode" Width="100%" runat="server" Text="---" />
            </div>
            <div style="width: 49%;">
                <asp:Label runat="server" Text="Descr. Articolo" />
                <asp:Label class="cpeBkTitolo " ID="lblArticleDescription" Width="100%" runat="server" Text="---" />
            </div>
        </div>
        <div class="headerDiv">
            <div style="width: 10%;">
                <asp:Label runat="server" Text="Commessa" />
                <asp:Label class="cpeBkTitolo " ID="lblOrder" Width="100%" runat="server" Text="---" />
            </div>
            <div style="width: 20%;">
                <asp:Label runat="server" Text="Fase" />
                <asp:Label class="cpeBkTitolo " ID="lblJob" Width="100%" runat="server" Text="---" />
            </div>
            <div style="width: 20%;">
                <asp:Label runat="server" Text="Descr. Piano di Controllo" />
                <asp:Label class="cpeBkTitolo " ID="lblCpDescription" Width="100%" runat="server" Text="---" />
            </div>
        </div>
    </div>
    <div id="tableContainer" style="width: 100%; margin-top: 20px;">
        <telerik:RadGrid ID="cpdTable" ClientIDMode="Static" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="false" AllowMultiRowSelection="false" OnNeedDataSource="cpdTable_NeedDataSource" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" NoMasterRecordsText="No records to display." Width="100%">
                <Columns>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Descrizione" ItemStyle-Width="280px" MaxLength="40" ItemStyle-Wrap="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="ControlType" HeaderText="Tipolog." ItemStyle-Width="40px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("ControlTypeShort") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="Operator" HeaderText="Op." ItemStyle-Width="30px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("OperatorSymbol") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="Udm" HeaderText="UDM" ItemStyle-Width="65px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("Udm") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="Tolerance1Sign" HeaderText="Seg.1" ItemStyle-Width="35px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="Tol1Sign" CssClass="control-label" Text='<%# Eval("Tolerance1Sign") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="Tolerance1Value" HeaderText="Toll.1" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="Tol1" CssClass="control-label" Text='<%# Eval("Tolerance1Value") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="BaseValue" HeaderText="Valore" ItemStyle-Width="90px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="BaseValue" CssClass="control-label" Text='<%# Eval("BaseValue") %>' Visible='<%# Eval("isControlDimensional")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="Tolerance2Sign" HeaderText="Seg.2" ItemStyle-Width="35px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="Tol2Sign" CssClass="control-label" Text='<%# Eval("Tolerance2Sign") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="Tolerance2Value" HeaderText="Toll.2" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="Tol2" CssClass="control-label" Text='<%# Eval("Tolerance2Value") %>' Visible='<%# Eval("isToleranceVisible")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="ToolCode" HeaderText="Cod. Strumento" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DetailNotes" HeaderText="Note" MaxLength="2000" ItemStyle-Wrap="true" ItemStyle-Width="250px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Num. Controllo" UniqueName="IdControllo" ItemStyle-Width="60px" ItemStyle-Wrap="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="control-label" Text='<%# Eval("IdControllo") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn DataField="MeasuredValue" HeaderText="Valore Misurato" ItemStyle-Wrap="true" ItemStyle-Width="90px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="measuredValueLbl" ClientIDMode="AutoID" data-numpadindex="0" CssClass="control-label inputBox" Text=""
                                data-udm='<%# Eval("Udm") %>'
                                data-type='<%# Eval("ControlType") %>'
                                data-operator='<%# Eval("Operator") %>'
                                data-tol1sign='<%# Eval("Tolerance1Sign") %>'
                                data-tol1value='<%# Eval("Tolerance1Value") %>'
                                data-tol2sign='<%# Eval("Tolerance2Sign") %>'
                                data-tol2value='<%# Eval("Tolerance2Value") %>'
                                data-toolcode='<%# Eval("ToolCode") %>'
                                data-basevalue='<%# Eval("BaseValue") %>'
                                data-idcontrollo='<%# Eval("IdControllo") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Esito" ItemStyle-Wrap="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="resultLbl" ClientIDMode="AutoID" CssClass="control-label" Text=""></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="True" EnableColumnClientFreeze="true" FrozenColumnsCount="1" UseStaticHeaders="false" />
            </ClientSettings>
        </telerik:RadGrid>

    </div>
    <br />
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cpeAlDx nop" style="margin-bottom: 10px!important">
        <div id="panelNumpadContainer0" class="col-md-6 nop text-left"></div>
        <div id="panelOkNoPadContainer" class="col-md-6 nop text-left"">
            <table>
                <tr>
                    <td>
                        <button id="padButtonOk" onclick="return false;">Ok</button></td>
                </tr>
                <tr>
                    <td>
                        <button id="padButtonNo" onclick="return false;">No</button></td>
                </tr>
            </table>
        </div>
        <div class="col-md-6 nop text-right" style="height: 215px; position: relative;">
            <telerik:RadButton RenderMode="Lightweight" Width="100px" Height="36px" ID="btnSaveData" Style="position: absolute; bottom: 0; right: 106px; color: #000" CssClass="mb-3" OnClick="btnSaveData_Click" runat="server" Primary="true" Text="Conferma" />
            <telerik:RadButton RenderMode="Lightweight" Width="100px" Height="36px" ID="btnCloseData" Style="position: absolute; bottom: 0; right: 0; color: #000" CssClass="mb-3" OnClick="btnCloseData_Click" runat="server" Primary="false" Text="Annulla" />
        </div>
    </div>
    <%--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cpeAlDx nop">--%>
        <%--<div class="col-md-6 nop text-left">
            <telerik:RadButton RenderMode="Lightweight" Width="210px" Height="36px" ID="btnControlliStat" CssClass="mb-3" runat="server" Primary="true" Text="Controlli statistici di processo" />
            <telerik:RadButton RenderMode="Lightweight" Width="210px" Height="36px" ID="btnEsitiLotti" CssClass="mb-3" runat="server" Primary="false" Text="Esiti valutazione lotti" />
        </div>--%>
        <%--<div class="col-md-12 nop text-right">
            <telerik:RadButton RenderMode="Lightweight" Width="100px" Height="36px" ID="btnSaveData2" CssClass="mb-3" OnClick="btnSaveData_Click" runat="server" Primary="true" Text="Conferma" />
            <telerik:RadButton RenderMode="Lightweight" Width="100px" Height="36px" ID="btnCloseData2" CssClass="mb-3" OnClick="btnCloseData_Click" runat="server" Primary="false" Text="Annulla" />
        </div>--%>
    <%--</div>--%>
    <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:HiddenField ClientIDMode="Static" ID="udmList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="operatorList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="baseValueList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="tol1SignList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="tol1ValueList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="tol2SignList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="tol2ValueList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="toolCodeList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="controlTypeList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="idcontrolloList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="valuesList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="resultsList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="selectedInputBoxIndex" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        selectedInputBoxIndex = -1;
        countInputBox = 0;
        arrayInputBox = new Array();
        arrayNMPD = new Array();
        initFlag = true;

        function scrollTo(parent, child) {
            try {
                var diff = ($(child).offset().top + $(parent).scrollTop()) - $(parent).offset().top;
                diff = (diff - $(parent).parent().height()) + $(child).parent().height();
                if (diff < 0) diff = 0;

                if (!isNaN(diff)) {
                    $(parent).css("position", "relative");
                    $(parent).animate({ scrollTop: diff }, 80);
                }
            } catch (ex) { }
        }

        function changeSelectedInputBox(indx) {
            if (selectedInputBoxIndex > -1)
                findResult($(arrayInputBox[selectedInputBoxIndex]), selectedInputBoxIndex);

            $(arrayInputBox[selectedInputBoxIndex]).parent().css("background-color", (selectedInputBoxIndex % 2 == 0) ? "transparent" : "#f9f9f9");
            $(arrayInputBox[indx]).parent().css("background-color", "LightBlue");

            if ($(arrayInputBox[indx]).attr("data-type") == "Dimensionale") {
                $("#panelNumpadContainer"+$(arrayInputBox[indx]).attr("data-numpadindex")).css("display", "inherit");
                $("#panelOkNoPadContainer").css("display", "none");
            }
            else {
                $("#panelNumpadContainer"+$(arrayInputBox[indx]).attr("data-numpadindex")).css("display", "none");
                $("#panelOkNoPadContainer").css("display", "inherit");
            }

            selectedInputBoxIndex = indx;
            $(arrayInputBox[indx]).trigger("click");

            scrollTo($("#cpdTable_GridData"), $(arrayInputBox[indx]).parent());

            var sep = "|&|";
            if (!initFlag) {
                $("#selectedInputBoxIndex").val(selectedInputBoxIndex);

                var vals = "", cpIds = "", udms = "", bvals = "", tol1signs = "", tol1vals = "", tol2signs = "", tol2vals = "", toolcodes = "", ctypes = "", ops = "", results = "", idc = "";
                $(arrayInputBox).each(function (key, value) {
                    vals += $(value).html() + sep;
                    udms += $(value).attr("data-udm") + sep;
                    ops += $(value).attr("data-operator") + sep;
                    bvals += $(value).attr("data-basevalue") + sep;
                    tol1signs += $(value).attr("data-tol1sign") + sep;
                    tol1vals += $(value).attr("data-tol1value") + sep;
                    tol2signs += $(value).attr("data-tol2sign") + sep;
                    tol2vals += $(value).attr("data-tol2value") + sep;
                    toolcodes += $(value).attr("data-toolcode") + sep;
                    idc += $(value).attr("data-idcontrollo") + sep;
                    ctypes += $(value).attr("data-type") + sep;
                    results += $(value).parent().parent().find("td").eq(12).children().html() + sep;
                });
                if (vals.length > 0) {
                    $("#valuesList").val(vals.substring(0, vals.length - sep.length));
                    $("#udmList").val(udms.substring(0, udms.length - sep.length));
                    $("#operatorList").val(ops.substring(0, ops.length - sep.length));
                    $("#baseValueList").val(bvals.substring(0, bvals.length - sep.length));
                    $("#tol1SignList").val(tol1signs.substring(0, tol1signs.length - sep.length));
                    $("#tol1ValueList").val(tol1vals.substring(0, tol1vals.length - sep.length));
                    $("#tol2SignList").val(tol2signs.substring(0, tol2signs.length - sep.length));
                    $("#tol2ValueList").val(tol2vals.substring(0, tol2vals.length - sep.length));
                    $("#toolCodeList").val(toolcodes.substring(0, toolcodes.length - sep.length));
                    $("#idcontrolloList").val(idc.substring(0, idc.length - sep.length));
                    $("#controlTypeList").val(ctypes.substring(0, ctypes.length - sep.length));
                    $("#resultsList").val(results.substring(0, results.length - sep.length));
                }
            } else {
                var valsArr = String($("#valuesList").val()).split(sep);

                $(arrayInputBox).each(function (key, value) {
                    $(value).html(valsArr[key]);
                    findResult($(value), key);
                });

                if (countInputBox > 0)
                    $(arrayInputBox[0]).trigger("click", true);

                initFlag = false;

                var selectedIndx = $("#selectedInputBoxIndex").val();
                if (selectedIndx != "" && selectedIndx > -1) {
                    changeSelectedInputBox(parseInt(selectedIndx));
                }
            }
        }

        function incrementInputBoxIndex() {
            var newIndex = selectedInputBoxIndex + 1;
            if (newIndex > countInputBox - 1) newIndex = 0;
            changeSelectedInputBox(newIndex);
        }

        function findResult(inputBox, indx) {
            var controlType = inputBox.attr("data-type"),
                value = inputBox.text(),
                result = "";

            if (controlType == "Dimensionale" && value != "") {
                var operator = inputBox.attr("data-operator"),
                    basevalue = parseFloat(String(inputBox.attr("data-basevalue")).replace(",", "."));
                value = parseFloat(String(value).replace(",", "."));

                if (operator == "Uguale a") {
                    var tol1sign = inputBox.attr("data-tol1sign"),
                        tol1value = parseFloat(String(inputBox.attr("data-tol1value")).replace(",", ".")),
                        tol2sign = inputBox.attr("data-tol2sign"),
                        tol2value = parseFloat(String(inputBox.attr("data-tol2value")).replace(",", ".")),
                        val1 = (tol1sign == "-") ? basevalue - tol1value : basevalue + tol1value,
                        val2 = (tol2sign == "-") ? basevalue - tol2value : basevalue + tol2value,
                        vmin = (val1 < val2) ? val1 : val2,
                        vmax = (val1 < val2) ? val2 : val1;

                    result = (value >= vmin && value <= vmax) ? "Ok" : "No";
                }
                else {
                    switch (operator) {
                        case "Maggiore di":
                            result = (value > basevalue) ? "Ok" : "No";
                            break;
                        case "Minore di":
                            result = (value < basevalue) ? "Ok" : "No";
                            break;
                        case "Maggiore Uguale di":
                            result = (value >= basevalue) ? "Ok" : "No";
                            break;
                        case "Minore Uguale di":
                            result = (value <= basevalue) ? "Ok" : "No";
                            break;
                    }
                }
            }
            else {
                result = value;
            }

            var bg = (indx % 2 == 0) ? "transparent" : "#f9f9f9";
            if (result == "Ok") {
                bg = "PaleGreen";
            }
            else if (result == "No") {
                bg = "LightCoral";
            }

            var currentRow = inputBox.parent().parent(),
                tdCount = $(currentRow).find("td").length,
                resultTD = $(currentRow).find("td").eq(tdCount-1),
                resultSpan = $(resultTD).find("span");

            $(resultSpan).text(result);
            $(resultTD).css("background-color", bg);
        }
    </script>
    <script src="..\Script\jquery.numpad.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $("#panelNumpadContainer0").css("display", "none");
            $("#panelOkNoPadContainer").css("display", "none");

            $("#cpdTable_GridData").attr("style", "overflow: auto;width: 100%;max-height: 260px; height: auto;position: relative;");

            $('#cpdTable .inputBox').each(function () {
                arrayInputBox.push($(this));
                $(this).attr("data-index", countInputBox);

                if ($(this).attr("data-type") == "Dimensionale") {
                    $(this).numpad({numpadIndex: $(this).attr("data-numpadindex")});
                }

                $($(this).parent()).click(function () {
                    var indx = parseInt($(this).children().attr("data-index"));
                    if (selectedInputBoxIndex != indx) {
                        changeSelectedInputBox(indx);
                    }
                })

                if ($(this).hasClass("nofocus")) {
                    $(this).blur(function () {
                        $("#panelNumpadContainer"+$(this).attr("data-numpadindex")).css("display", "none");
                    });
                }

                countInputBox++;
            });

            $("#padButtonOk").click(function () {
                $(arrayInputBox[selectedInputBoxIndex]).text("Ok");
                incrementInputBoxIndex();
            });

            $("#padButtonNo").click(function () {
                $(arrayInputBox[selectedInputBoxIndex]).text("No");
                incrementInputBoxIndex();
            });

            // Seleziona il primo label della tabella
            if (countInputBox > 0)
                $(arrayInputBox[0]).trigger("click", true);
        });
    </script>
</asp:Content>
