﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace MainCloud.HMI.PrintLabel
{
    public partial class PrintLabel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtBARCODE.Focus();
            }
            else
            {
                var r = Request["__EVENTARGUMENT"];
                addRowPanel.Visible = false;
                printLbl.Visible = false;
                if (Request["__EVENTARGUMENT"] == "createTable")
                {
                    createTable();
                }
                else if (Request["__EVENTARGUMENT"] == "printLabel")
                {
                    createTable(true);
                    print();
                }
            }
        }

        private string fixVal(string str)
        {
            // Elimina ",00" e ",0" quando inutili | "100,00" -> "100"; "100,0" -> "100"
            // Aggiunge i "." per separare le migliaia, i milioni, ecc.. "1000" -> "1.000"; "10000,00" -> "10.000"; "10.000,2" -> "10.000,2"

            if (str.Contains(","))
            {
                if (str.Length > 3 && str.Substring(str.Length - 3, 3) == ",00")
                {
                    str = str.Replace(",00", "");
                }
                if (str.Length > 2 && str.Substring(str.Length - 2, 2) == ",0")
                {
                    str = str.Replace(",0", "");
                }
            }

            if (!str.Contains(".") && str.Length > 3)
            {
                string nstr = "";
                int i = str.Contains(",") ? str.IndexOf(",") - 1 : str.Length - 1, j = 0;
                for (; i >= 0; i--, j++)
                {
                    if (j == 3)
                    {
                        nstr += "." + str.Substring(i, 1);
                        j = 0;
                    }
                    else
                    {
                        nstr += str.Substring(i, 1);
                    }
                }

                char[] charArray = nstr.ToCharArray();
                Array.Reverse(charArray);
                nstr = new string(charArray);
                if (str.Contains(","))
                    nstr += str.Substring(str.IndexOf(","), str.Length - str.IndexOf(","));

                return nstr;
            }

            return str;
        }

        protected string getZPL(Dictionary<string, string> data)
        {
            string path = Server.MapPath("~/HMI/PrintLabel/output.zpl");
            string ZPL = File.ReadAllText(path);

            ZPL = ZPL.Replace("_CODICEMATERIALE_", data["_CODICEMATERIALE_"]);
            ZPL = ZPL.Replace("_L_", data["_L_"]);
            ZPL = ZPL.Replace("_S_", data["_S_"]);
            ZPL = ZPL.Replace("_LOTTO_", data["_LOTTO_"]);
            ZPL = ZPL.Replace("%OBARCODE%O", data["_LOTTO_"]);
            ZPL = ZPL.Replace("_PESO_", data["_PESO_"]);

            return ZPL;
        }

        protected void print()
        {
            /*azienda = "SpecialAcciai S.p.A.",*/
            string codiceMateriale = "C110-023-2-0180", //TODO: prendi dal DB
                   spessore = "0,23", //TODO: prendi dal CodiceMateriale: XXX-023-X-LARGHEZZA
                   barcode = Request.Form[barcodeVal.UniqueID], //TODO: aggiungi codice sequenza
                   larghezza = fixVal(Request.Form[larghezzaVal.UniqueID]),
                   peso = fixVal(Request.Form[pesoVal.UniqueID]);

            // Printer IP Address and communication port
            string ipAddress = "127.0.0.1";//"192.168.1.99";
            int port = 9100;

            Dictionary<string, string> labelData = new Dictionary<string, string>();
            labelData["_CODICEMATERIALE_"] = codiceMateriale;
            labelData["_L_"] = larghezza;
            labelData["_S_"] = spessore;
            labelData["_LOTTO_"] = barcode;
            labelData["_PESO_"] = peso;

            // ZPL Command(s)
            string ZPLstr = getZPL(labelData);

            try
            {
                // Open connection
                System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient();
                client.Connect(ipAddress, port);

                // Write ZPL String to connection
                System.IO.StreamWriter writer =
                new System.IO.StreamWriter(client.GetStream());
                writer.Write(ZPLstr);
                writer.Flush();

                // Close Connection
                writer.Close();
                client.Close();
            }
            catch (Exception ex)
            {
                // Catch Exception
                errorMsg.Text = ex.Message;
                dlgErrorDialog.OpenDialog();
            }
        }

        protected void dlgErrorOk_Click(object sender, EventArgs e)
        {
            dlgErrorDialog.CloseDialog();
        }

        protected void createTable(bool recreate = false)
        {
            int num = 10;
            List<string> aArr = new List<string>(), pArr = new List<string>();
            if (recreate)
            {
                if (Request.Form[labelsCount.UniqueID] != "")
                    num = Convert.ToInt32(Request.Form[labelsCount.UniqueID]);

                string altezze = Request.Form[larghezzaArr.UniqueID],
                       pesi = Request.Form[pesoArr.UniqueID];

                if (altezze.Length > 0)
                    aArr = new List<string>(altezze.Split('|'));
                if (pesi.Length > 0)
                    pArr = new List<string>(pesi.Split('|'));
            }

            if (txtBARCODE.Text.Trim().Length > 0 || recreate)
            {
                tabLabels.Rows.Clear();

                for (int i = 0; i < num; i++)
                {
                    TableRow tr = new TableRow();
                    tr.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    tr.ID = "row" + i;

                    TableCell tc = new TableCell();
                    tc.CssClass = "pr-10";
                    tc.Width = 350;

                    // BARCODE
                    Label lbl = new Label();
                    lbl.CssClass = "control-label";
                    lbl.Text = "BARCODE";
                    lbl.AssociatedControlID = "barCodeImg" + i;

                    RadBarcode rBarcode = new RadBarcode();
                    rBarcode.Text = (recreate) ? Request.Form[barcodeVal.UniqueID] : txtBARCODE.Text.Replace("/", "-");
                    rBarcode.ShowText = true;
                    rBarcode.ShowChecksum = false;
                    rBarcode.LineWidth = 2;
                    rBarcode.ShortLinesLengthPercentage = 75;
                    rBarcode.Height = 50;
                    rBarcode.Width = new Unit(100, UnitType.Percentage);
                    rBarcode.ID = "barCodeImg" + i;
                    rBarcode.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    rBarcode.Type = Telerik.Web.UI.BarcodeType.Code39Extended;

                    tc.Controls.Add(lbl);
                    Literal lt = new Literal();
                    lt.Text = "<br/>";
                    tc.Controls.Add(lt);
                    tc.Controls.Add(rBarcode);
                    tr.Cells.Add(tc);

                    // larghezza
                    tc = new TableCell();
                    tc.CssClass = "pr-10";
                    tc.Width = 95;
                    tc.Style.Add("min-width", "90px");

                    lbl = new Label();
                    lbl.CssClass = "control-label";
                    lbl.Text = "Largh.";
                    lbl.AssociatedControlID = "txtLarghezza" + i;

                    RadNumericTextBox txtNumeric = new RadNumericTextBox();
                    txtNumeric.ID = "txtLarghezza" + i;
                    txtNumeric.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    txtNumeric.Width = new Unit(100, UnitType.Percentage);
                    txtNumeric.Type = NumericType.Number;
                    txtNumeric.Text = (recreate && aArr[i].Length > 0) ? Convert.ToDouble(aArr[i]).ToString() : "";
                    txtNumeric.CssClass = "inputBox";
                    txtNumeric.Attributes.Add("data-numpadindex", "0");

                    tc.Controls.Add(lbl);
                    lt = new Literal();
                    lt.Text = "<br/>";
                    tc.Controls.Add(lt);
                    tc.Controls.Add(txtNumeric);
                    tr.Cells.Add(tc);

                    // Peso
                    tc = new TableCell();
                    tc.CssClass = "pr-10";
                    tc.Width = 95;
                    tc.Style.Add("min-width", "90px");

                    lbl = new Label();
                    lbl.CssClass = "control-label";
                    lbl.Text = "Peso (KG)";
                    lbl.AssociatedControlID = "txtPeso" + i;

                    txtNumeric = new RadNumericTextBox();
                    txtNumeric.ID = "txtPeso" + i;
                    txtNumeric.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    txtNumeric.Width = new Unit(100, UnitType.Percentage);
                    txtNumeric.Type = NumericType.Number;
                    txtNumeric.Text = (recreate && pArr[i].Length > 0) ? Convert.ToDouble(pArr[i]).ToString() : "";
                    txtNumeric.CssClass = "inputBox";
                    txtNumeric.Attributes.Add("data-numpadindex", "0");

                    tc.Controls.Add(lbl);
                    lt = new Literal();
                    lt.Text = "<br/>";
                    tc.Controls.Add(lt);
                    tc.Controls.Add(txtNumeric);
                    tr.Cells.Add(tc);

                    // Stampa
                    tc = new TableCell();

                    lbl = new Label();
                    lbl.CssClass = "control-label";
                    lbl.Text = "";
                    lbl.AssociatedControlID = "btnStampa" + i;

                    RadButton btn = new RadButton();
                    btn.ID = "btnStampa" + i;
                    btn.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    btn.Text = "Stampa";
                    btn.AutoPostBack = false;
                    btn.Height = 32;
                    btn.Style.Add("padding-top", "0px !important");
                    btn.Style.Add("padding-bottom", "0px !important");
                    btn.Style.Add("position", "relative");
                    btn.Style.Add("top", "2px");
                    btn.Attributes.Add("OnClick", "printLabel(" + i + ", '" + rBarcode.Text + "')");

                    tc.Controls.Add(lbl);
                    lt = new Literal();
                    lt.Text = "<br/>";
                    tc.Controls.Add(lt);
                    tc.Controls.Add(btn);
                    tr.Cells.Add(tc);

                    tabLabels.Rows.Add(tr);
                }

                labelsCount.Value = num.ToString();
                addRowPanel.Visible = true;
                printLbl.Visible = true;

                clearFlag.Value = (recreate) ? "false" : "true";
            }
        }
    }
}