﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/DashboardOperator/MainDashboardOperator.Master" CodeBehind="PrintLabel.aspx.cs" Inherits="MainCloud.HMI.PrintLabel.PrintLabel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="..\Style\Style.css?<%#DateTime.Now %>" rel="stylesheet" type="text/css">
    <link href="..\Style\jquery.numpad.css" rel="stylesheet" type="text/css">
    <link href="PrintLabelStyle.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <script>
        function printLabel(i, barcode)
        {
            saveValues();
            $("#larghezzaVal").val($("#txtLarghezza" + i).val());
            $("#pesoVal").val($("#txtPeso" + i).val());
            $("#barcodeVal").val(barcode);
            __doPostBack('<%=btnConfirm.ClientID %>', "printLabel");
        }

        function saveValues()
        {
            var larghezze = "", pesi = "", sep = "|";
            $("#tabLabels > tbody  > tr").each(function () {
                var tr = $(this),
                    larghezza = $(tr.find("td").eq(1).find("input")).val(),
                    peso = $(tr.find("td").eq(2).find("input")).val();
                console.log(larghezza);

                larghezze += larghezza + sep;
                pesi += peso + sep;
            });

            if (larghezze.length > 0) {
                $("#larghezzaArr").val(larghezze.substring(0, larghezze.length - sep.length));
            }
            if (pesi.length > 0) {
                $("#pesoArr").val(pesi.substring(0, pesi.length - sep.length));
            }
        }

        function removeModal() {
            __doPostBack('<%=btnConfirm.ClientID %>', "printLabel");
        }
    </script>

    <mcf:PopUpDialog ID="dlgErrorDialog" runat="server" Title="Errore">
        <Body>
            <asp:Label ID="errorMsg" Text="Errore durante la stampa." runat="server" /><br />
            <br />
            <telerik:RadButton runat="server" ID="dlgErrorOk" ClientIDMode="Static" Text="Ok" OnClientClicking="removeModal" OnClick="dlgErrorOk_Click"></telerik:RadButton>
        </Body>
    </mcf:PopUpDialog>

    <asp:UpdateProgress runat="server" ID="progressPnl">
        <ProgressTemplate>
            <div class="loading">Loading&#8230;</div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    
<%--    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Always">
        <ContentTemplate>--%>
            <asp:Panel runat="server" ID="pnlPrimary">
                <div class="row nop">
                    <div class="col-md-4 mb-25">
                        <asp:Label runat="server" Text="Scansione BARCODE" /><br /><br />
                        <table style="width: 100%;">
                            <tr>
                                <td class="pr-10">
                                    <telerik:RadTextBox runat="server" ClientIDMode="Static" ID="txtBARCODE" Width="100%"></telerik:RadTextBox>
                                </td>
                                <td style="width: 100px">
                                    <asp:Button runat="server" CssClass="inputBtn" ClientIDMode="Static" ID="btnConfirm" Text="Conferma" Width="100%" Height="34"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-8 mb-25" style="display: inline-table;">
                        <div style="display: inline-block; margin-right: 10px; max-width: calc(100% - 390px)">
                            <asp:Label ClientIDMode="Static" ID="printLbl" runat="server" Text="Stampa etichette" Visible="false"/><br /><br />
                            <asp:Table ClientIDMode="Static" ID="tabLabels" runat="server" Width="100%"></asp:Table><br />
                            <asp:Panel ClientIDMode="Static" ID="addRowPanel" runat="server" Visible="false">
                                <button id="btnAddRow" class="inputBtn" onclick="return false;" style="width: 100%; height: 34px; text-align: center;">Aggiungi Riga</button>
                            </asp:Panel>
                        </div>
                        <div style="width: 380px; display: inline-block; vertical-align: top;">
                            <div id="panelNumpadContainer0" style="position: fixed;"></div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:HiddenField ClientIDMode="Static" ID="clearFlag" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="labelsCount" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="larghezzaArr" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="pesoArr" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="larghezzaVal" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="pesoVal" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="barcodeVal" runat="server" />

            <script>
                // NUMPAD
                selectedInputBoxIndex = -1;
                countInputBox = 0;
                arrayInputBox = new Array();
                initFlag = true;

                function changeSelectedInputBox(indx)
                {
                    $(arrayInputBox[selectedInputBoxIndex]).removeClass("riFocused");
                    $(arrayInputBox[indx]).addClass("riFocused");

                    console.dir($(arrayInputBox[indx]));

                    selectedInputBoxIndex = indx;
                    $(arrayInputBox[indx]).trigger("click");

                    if (initFlag) {
                        if (countInputBox > 0)
                            $(arrayInputBox[0]).trigger("click", true);
                        initFlag = false;
                    }
                }

                function incrementInputBoxIndex()
                {
                    var newIndex = selectedInputBoxIndex + 1;
                    if (newIndex > countInputBox - 1) newIndex = 0;
                    changeSelectedInputBox(newIndex);
                }
            </script>
            <script src="..\Script\jquery.numpad.js" type="text/javascript"></script>
            <script>  
                $(document).ready(function () {
                    $("#btnConfirm").click(function () {
                        __doPostBack('<%=btnConfirm.ClientID %>', "createTable");
                    });

                    $("#btnAddRow").click(function () {
                        var rowCount = $("#labelsCount").val() - 1,
                            newCount = rowCount + 1;
                        var newRow = $("#row" + rowCount).clone();
                        $(newRow).find("#txtPeso" + rowCount).val("").attr("id", "txtPeso" + newCount).removeAttr("name");
                        $(newRow).find("#txtPeso" + rowCount + "_wrapper").val("").attr("id", "txtPeso" + newCount + "_wrapper").removeAttr("value", "").removeAttr("name");
                        $(newRow).find("#txtPeso" + rowCount + "_ClientState").val("").attr("id", "txtPeso" + newCount + "_ClientState").removeAttr("value", "").removeAttr("name");
                        $(newRow).find("#txtLarghezza" + rowCount).val("").attr("id", "txtLarghezza" + newCount).removeAttr("name");
                        $(newRow).find("#txtLarghezza" + rowCount + "_wrapper").val("").attr("id", "txtLarghezza" + newCount + "_wrapper").removeAttr("value", "").removeAttr("name");
                        $(newRow).find("#txtLarghezza" + rowCount + "_ClientState").val("").attr("id", "txtLarghezza" + newCount + "_ClientState").removeAttr("value", "").removeAttr("name");
                        $(newRow).find("#barCodeImg" + rowCount).attr("id", "barCodeImg" + newCount).removeAttr("name");
                        $(newRow).find("#btnStampa" + rowCount).attr("id", "btnStampa" + newCount).removeAttr("name");
                        $(newRow).find("#btnStampa" + rowCount + "_input").val("").attr("id", "btnStampa" + newCount + "_input").attr("value", "Stampa").removeAttr("name");
                        $(newRow).find("#btnStampa" + rowCount + "_ClientState").val("").attr("id", "btnStampa" + newCount + "_ClientState").removeAttr("value", "").removeAttr("name");
                        $(newRow).attr("id", "row" + newCount);
                        $(newRow).appendTo($("#tabLabels > tbody"));

                        // NUMPAD
                        arrayInputBox.push($("#txtLarghezza" + newCount));
                        $("#txtLarghezza" + newCount).attr("data-index", countInputBox);
                        $("#txtLarghezza" + newCount).attr("data-numpadindex", 0);

                        $("#txtLarghezza" + newCount).click(function () {
                            var indx = parseInt($(this).attr("data-index"));
                            if (selectedInputBoxIndex != indx) {
                                changeSelectedInputBox(indx);
                            }
                        })

                        $("#txtLarghezza" + newCount).numpad({numpadIndex: $("#txtLarghezza" + newCount).attr("data-numpadindex")});

                        countInputBox += 1;

                        arrayInputBox.push($("#txtPeso" + newCount));
                        $("#txtPeso" + newCount).attr("data-index", countInputBox);
                        $("#txtPeso" + newCount).attr("data-numpadindex", 0);

                        $("#txtPeso" + newCount).click(function () {
                            var indx = parseInt($(this).attr("data-index"));
                            if (selectedInputBoxIndex != indx) {
                                changeSelectedInputBox(indx);
                            }
                        })

                        $("#txtPeso" + newCount).numpad({numpadIndex: $("#txtPeso" + newCount).attr("data-numpadindex")});

                        countInputBox += 1;
                        // end of NUMPAD

                        $("#labelsCount").val(newCount + 1);
                    });

                    // NUMPAD
                    $('#tabLabels .inputBox').each(function () {
                        arrayInputBox.push($(this));
                        $(this).attr("data-index", countInputBox);

                        if ($("#clearFlag").val() == "true") {
                            $(this).val("");
                        }

                        $(this).numpad({numpadIndex: $(this).attr("data-numpadindex")});

                        $(this).click(function () {
                            var indx = parseInt($(this).attr("data-index"));
                            if (selectedInputBoxIndex != indx) {
                                changeSelectedInputBox(indx);
                            }
                        })

                        countInputBox++;
                    });

                    // Seleziona il primo input della tabella
                    if (countInputBox > 0)
                        $(arrayInputBox[0]).trigger("click", true);
                });
            </script>
<%--        </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>