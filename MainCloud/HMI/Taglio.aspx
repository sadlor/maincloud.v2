﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HMI/MainDashboardHMI.Master" AutoEventWireup="true" CodeBehind="Taglio.aspx.cs" Inherits="MainCloud.HMI.Taglio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Style\Style.css?<%#DateTime.Now %>" rel="stylesheet" type="text/css">
    <link href="Style\ProgressBarStyle.css?<%#DateTime.Now %>" rel="stylesheet" type="text/css">
    <link href="Style\jquery.numpad.css" rel="stylesheet" type="text/css">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdateProgress runat="server" ID="progressPnl">
        <ProgressTemplate>
            <div class="loading">Loading&#8230;</div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlSelectMachine$btnConfirmMachine" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlSelectMachine$btnCancelMachine" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlSelectOperator$btnConfirmConnect" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlSelectOperator$btnCancelConnect" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlSelectOrder$btnConfirmSelectOrder" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlSelectOrder$btnCancelSelectOrder" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlBOM$btnCloseBOM" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlPrelievo$btnConfirmPrelievo" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlPrelievo$btnCancelPrelievo" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlWaste$btnCancelWaste" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlWaste$btnConfirmWaste" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlFermi$btnConfirmCausaleFermo" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlFermi$btnCancelCausaleFermo" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlArticle$btnCloseArticleImage" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlChart$btnViewOEE" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlOEE$btnCloseOEE" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlPartProgram$btnClosePartProgram" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlMold$btnConfirmMold" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="pnlMold$btnCancelMold" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlPrimary">
                <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="60000"></asp:Timer>
                <asp:HiddenField runat="server" ID="hfMachineId" EnableViewState="true" />
                <asp:HiddenField runat="server" ID="hfOperatorId" />
                <asp:HiddenField runat="server" ID="hfJobId" />
                <asp:HiddenField runat="server" ID="hfMoldId" />
                <div class="row">
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 noPaddingRight" style="width:70%;">
                        <div class="row">
                            <%--<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">--%>
                            <%--<div class="col-xs-custom-1 col-sm-custom-1 col-md-custom-1 col-lg-custom-1">--%>
                            <div class="col-xs-custom-2-1 col-sm-custom-2-1 col-md-custom-2-1 col-lg-custom-2-1">
                                <asp:Label Text="Macchina" runat="server" AssociatedControlID="btnMachine" />
                            </div>
                            <%--<div class="col-xs-custom-3-1 col-sm-custom-3-1 col-md-custom-3-1 col-lg-custom-3-1 noPaddingRight">--%>
                            <div class="col-xs-custom-4-1 col-sm-custom-4-1 col-md-custom-4-1 col-lg-custom-4-1 noPaddingRight">
                                <telerik:RadButton runat="server" ID="btnMachine" Enabled="true" BackColor="White" ForeColor="Black" OnClick="btnMachine_Click" Width="201px" CssClass="btnSelect"> 
                                </telerik:RadButton>
                            </div>
                            <%--<div class="col-xs-custom-3 col-sm-custom-3 col-md-custom-3 col-lg-custom-3 text-center">--%>
                            <%--<div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">--%>
                            <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                <telerik:RadButton runat="server" ID="btnStateProd" Text="Prod" Width="58" AutoPostBack="false" Enabled="false" EnableViewState="false" CssClass="btnView" />
                            </div>
                            <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                <telerik:RadButton runat="server" ID="btnStateAlarm" Text="All" Width="58" AutoPostBack="false" Enabled="false" EnableViewState="false" CssClass="btnView" />
                            </div>
                            <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                <telerik:RadButton runat="server" ID="btnStateStop" Text="Stop" Width="58" AutoPostBack="false" Enabled="false" EnableViewState="false" CssClass="btnView" />
                            </div>
                            <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                <telerik:RadButton runat="server" ID="btnStateOff" Text="Off" Width="58" AutoPostBack="false" Enabled="false" EnableViewState="false" CssClass="btnView" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-custom-2-1 col-sm-custom-2-1 col-md-custom-2-1 col-lg-custom-2-1">
                                <asp:Label Text="Operatore" runat="server" AssociatedControlID="btnOperator" />
                            </div>
                            <%--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--%>
                            <div class="col-xs-custom-4-1 col-sm-custom-4-1 col-md-custom-4-1 col-lg-custom-4-1 noPaddingRight">
                                <telerik:RadButton runat="server" ID="btnOperator" Enabled="false" BackColor="White" ForeColor="Black" OnClick="btnOperator_Click" Width="201px"
                                    CssClass="btnSelect" DisabledButtonCssClass="btnActivityDisabled">
                                </telerik:RadButton>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 noPaddingRight">
                                <asp:Label Text="Ord:" runat="server" AssociatedControlID="txtOrderQtyOrdered" style="line-height:2" />
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                                <asp:Label Text="Prod:" runat="server" AssociatedControlID="txtOrderQtyProduced" style="line-height:2" />
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 noPaddingLeft">
                                <asp:Label Text="Res:" runat="server" AssociatedControlID="txtOrderQtyLeft" style="line-height:2" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-custom-2-1 col-sm-custom-2-1 col-md-custom-2-1 col-lg-custom-2-1">
                                <asp:Label Text="Commessa" runat="server" AssociatedControlID="btnOrder" />
                            </div>
                            <%--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--%>
                            <div class="col-xs-custom-4-1 col-sm-custom-4-1 col-md-custom-4-1 col-lg-custom-4-1 noPaddingRight">
                                <telerik:RadButton runat="server" ID="btnOrder" Enabled="false" BackColor="White" ForeColor="Black" OnClientLoad="ResetClosedLoaded" OnClick="btnOrder_Click" Width="201px"
                                    CssClass="btnSelect" DisabledButtonCssClass="btnActivityDisabled">
                                </telerik:RadButton>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 noPaddingRight">
                                <telerik:RadButton runat="server" ID="txtOrderQtyOrdered" Enabled="false" BackColor="White" ForeColor="Black" Font-Bold="true" EnableViewState="false" />
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 textNoPadding">
                                <telerik:RadButton runat="server" ID="txtOrderQtyProduced" Enabled="false" BackColor="White" ForeColor="Black" Font-Bold="true" EnableViewState="false" />
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 noPaddingLeft">
                                <telerik:RadButton runat="server" ID="txtOrderQtyLeft" Enabled="false" BackColor="White" ForeColor="Black" Font-Bold="true" EnableViewState="false" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-custom-2-1 col-sm-custom-2-1 col-md-custom-2-1 col-lg-custom-2-1">
                                <asp:Label Text="Articolo" runat="server" AssociatedControlID="txtArticleCode" />
                            </div>
                            <div class="col-xs-custom-4-1 col-sm-custom-4-1 col-md-custom-4-1 col-lg-custom-4-1 noPaddingRight">
                                <telerik:RadButton runat="server" ID="txtArticleCode" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" Width="201px" EnableViewState="false" CssClass="btnView" />
                            </div>
                            <div class="col-xs-custom-6 col-sm-custom-6 col-md-custom-6 col-lg-custom-6" style="width:50%;">
                                <telerik:RadButton runat="server" ID="txtArticleDescription" Width="100%" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" EnableViewState="false" CssClass="btnView" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-custom-2-1 col-sm-custom-2-1 col-md-custom-2-1 col-lg-custom-2-1">
                                <asp:Label Text="Stampo" runat="server" AssociatedControlID="btnMold" />
                            </div>
                            <div class="col-xs-custom-4-1 col-sm-custom-4-1 col-md-custom-4-1 col-lg-custom-4-1 noPaddingRight">
                                <telerik:RadButton runat="server" ID="btnMold" Enabled="false" BackColor="White" ForeColor="Black" OnClick="btnMold_Click" Width="201px"
                                    CssClass="btnSelect" DisabledButtonCssClass="btnActivityDisabled">
                                </telerik:RadButton>
                            </div>
                            <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2 noPaddingRight" style="width:20%;">
                                <telerik:RadButton runat="server" ID="txtCavityNum" Width="100%" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" EnableViewState="false" CssClass="btnView" />
                            </div>
                            <div class="col-xs-custom-4 col-sm-custom-4 col-md-custom-4 col-lg-custom-4 noPaddingLeft" style="width:30%;">
                                <telerik:RadButton runat="server" ID="txtNStampateOra" Width="100%" Enabled="false" ReadOnly="true" BackColor="White" ForeColor="Black" EnableViewState="false" CssClass="btnView" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <telerik:RadButton runat="server" ID="btnConfirmTransaction" Text="Conferma modifiche" OnClick="btnConfirmTransaction_Click" Visible="false" BackColor="#f80f41" ForeColor="White">
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 noPaddingLeft" style="width:30%;">
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" EnableViewState="false" ChildrenAsTriggers="true">
                            <Triggers>
                                <%--<asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />--%>
                            </Triggers>
                            <ContentTemplate>
                                <asp:Panel runat="server" ID="pnlChart" onclick="ViewOEE()" style="z-index:1000;" EnableViewState="false">
                                    <asp:Button runat="server" ID="btnViewOEE" OnClick="btnViewOEE_Click" style="display:none;" EnableViewState="false" />
                                    <telerik:RadHtmlChart runat="server" ID="chartOEE" Height="225px" EnableViewState="false">
                                        <PlotArea>
                                            <Series>
                                                <telerik:ColumnSeries DataFieldY="IndexValue" ColorField="IndexColor" Stacked="false" Gap="1.5" Spacing="0.4">
                                                    <LabelsAppearance DataFormatString="{0}" Position="OutsideEnd">
                                                        <TextStyle Bold="true" Margin="-5" />
                                                    </LabelsAppearance>
                                                    <TooltipsAppearance DataFormatString="{0}" Color="White"></TooltipsAppearance>
                                                </telerik:ColumnSeries>
                                            </Series>
                                            <Appearance>
                                                <FillStyle BackgroundColor="Transparent"></FillStyle>
                                            </Appearance>
                                            <XAxis AxisCrossingValue="0" Color="black" MajorTickType="Outside" MinorTickType="Outside"
                                                Reversed="false">
                                                <Items>
                                                    <telerik:AxisItem LabelText="D"></telerik:AxisItem>
                                                    <telerik:AxisItem LabelText="E"></telerik:AxisItem>
                                                    <telerik:AxisItem LabelText="Q"></telerik:AxisItem>
                                                    <telerik:AxisItem LabelText="OEE"></telerik:AxisItem>
                                                </Items>
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="0" Skip="0" Step="1"></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="">
                                                </TitleAppearance>
                                            </XAxis>
                                            <YAxis AxisCrossingValue="0" Color="black" MajorTickSize="1" MajorTickType="Outside" MinValue="0" MaxValue="100"
                                                MinorTickType="None" Reversed="false">
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="0" Skip="0" Step="1"></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="%">
                                                </TitleAppearance>
                                            </YAxis>
                                        </PlotArea>
                                        <Appearance>
                                            <FillStyle BackgroundColor="Transparent"></FillStyle>
                                        </Appearance>
                                        <Legend>
                                            <Appearance Visible="false" BackgroundColor="Transparent" Position="Bottom">
                                            </Appearance>
                                        </Legend>
                                    </telerik:RadHtmlChart>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <br />
                <div class="row" runat="server">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <b>Target</b>
                    </div>
                    <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                        <div class="progress progressWithoutMargin">
                            <div runat="server" id="barEstimatedTime" class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="100"
                                aria-valuemin="0" aria-valuemax="100" style="width:100%">
                                <div runat="server" id="barTheoreticalTime" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100"
                                    aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                </div>
                                <div runat="server" class="progress-bar progress-bar-warning" id="textEstimatedTime" style="width:0%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <b>Progr</b>
                    </div>
                    <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                        <div class="progress progressWithoutMargin" style="text-align:right">
                            <div runat="server" id="barProducedTime" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax="100" style="text-align:center; float:left; width:0%;"></div>
                            <div runat="server" id="barLeftTime" class="progress-bar" role="progressbar" aria-valuenow="100"
                                    aria-valuemin="0" aria-valuemax="100" style="background-color:#f5f5f5 !important; text-align:center; width:100%; border:solid; float:left;"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="top:10px;">
                        <b>Maint</b>
                    </div>
                    <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                        <div class="vefs-milestone-wrapper">
                            <div class="milestone-container">
                                 <div class="label-container" style="margin-bottom:7px;" runat="server" id="maintenanceLabelTimeContainer">
                                    <%--<div runat="server" id="labelTimeWarning1">
                                        <div class="label colored"></div>
                                    </div>
                                    <div runat="server" id="labelTimeWarning2">
                                        <div class="label"></div>
                                    </div>
                                    <div runat="server" id="labelTimeAlarm" class="milestones milestone__100">
                                    </div>--%>
                                </div>
                                <div class="chart-container">
                                    <div class="line-container progress">
                                        <div class="line"></div>
                                        <div runat="server" id="progressiveBar" class="line left" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                        <%--<div runat="server" id="progressiveBar" class="progress-bar progress-bar-striped active line left fontBar"
                                           role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>--%>
                                    </div>
                                    <div class="dot-container" runat="server" id="maintenanceDotContainer">
                                        <%--<div runat="server" id="dotContainerWarning1" class="milestones milestone__50">
                                            <div runat="server" id="dotWarning1" class="dot completed colored"></div>
                                        </div>
                                        <div runat="server" id="dotContainerWarning2" class="milestones milestone__80">
                                            <div runat="server" id="dotWarning2" class="dot"></div>
                                        </div>
                                        <div class="milestones milestone__100">
                                            <div class="dot colored"></div>
                                        </div>--%>
                                    </div>
                                </div>
                                <div class="label-container" style="margin-bottom:7px;" runat="server" id="maintenanceLabelContainer">
                                    <%--<div runat="server" id="labelWarning1" class="milestones milestone__50">
                                        <div class="label colored"></div>
                                    </div>
                                    <div runat="server" id="labelWarning2" class="milestones milestone__80">
                                        <div class="label"></div>
                                    </div>
                                    <div runat="server" id="labelAlarm" class="milestones milestone__100">
                                        <div class="label"></div>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <telerik:RadButton runat="server" ID="btnArticleImage" Text="Disegno" OnClick="btnArticleImage_Click" Enabled="false"
                            CssClass="btnActivity btnFooter" DisabledButtonCssClass="btnActivityDisabled" EnableViewState="false">
                        </telerik:RadButton>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <telerik:RadButton runat="server" ID="btnSetupPlan" Text="Piani setup" Enabled="false"
                            CssClass="btnActivity btnFooter" DisabledButtonCssClass="btnActivityDisabled" EnableViewState="false">
                        </telerik:RadButton>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <telerik:RadButton runat="server" ID="btnPartProgram" Text="Part program" OnClick="btnPartProgram_Click" Enabled="false"
                            CssClass="btnActivity btnFooter" DisabledButtonCssClass="btnActivityDisabled" EnableViewState="false">
                        </telerik:RadButton>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <telerik:RadButton runat="server" ID="btnViewMaintPlan" Text="Piano manut." OnClick="btnViewMaintPlan_Click" Enabled="false"
                            CssClass="btnActivity btnFooter" DisabledButtonCssClass="btnActivityDisabled" EnableViewState="false">
                        </telerik:RadButton>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <telerik:RadButton runat="server" ID="btnBOM" Text="Distinta base" OnClick="btnBOM_Click" Enabled="false"
                            CssClass="btnActivity btnFooter" DisabledButtonCssClass="btnActivityDisabled" EnableViewState="false">
                        </telerik:RadButton>
                    </div>
                </div>
                <div class="row" style="padding-top:1px;">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <telerik:RadButton runat="server" ID="btnFermi" Text="Fermi" OnClick="btnFermi_Click" Enabled="false"
                            CssClass="btnActivity btnFooter" DisabledButtonCssClass="btnActivityDisabled" EnableViewState="false">
                        </telerik:RadButton>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <telerik:RadButton runat="server" ID="btnWaste" Text="Scarti" OnClick="btnWaste_Click" Enabled="false"
                            CssClass="btnActivity btnFooter" DisabledButtonCssClass="btnActivityDisabled" EnableViewState="false">
                        </telerik:RadButton>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <telerik:RadButton runat="server" ID="btnQualità" Text="Qualità" OnClick="btnQualità_Click" Enabled="false"
                            CssClass="btnActivity btnFooter" DisabledButtonCssClass="btnActivityDisabled" EnableViewState="false">
                        </telerik:RadButton>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <telerik:RadButton runat="server" ID="btnMaintenance" Text="Manutenzione" OnClick="btnMaintenance_Click" Enabled="false"
                            CssClass="btnActivity btnFooter" DisabledButtonCssClass="btnActivityDisabled" EnableViewState="false">
                        </telerik:RadButton>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <telerik:RadButton runat="server" ID="btnPrelievo" Text="Prelievo" OnClick="btnPrelievo_Click" Enabled="false"
                            CssClass="btnActivity btnFooter" DisabledButtonCssClass="btnActivityDisabled" EnableViewState="false">
                        </telerik:RadButton>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlPrimary$btnMachine" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlSelectMachine" Visible="false">
                <div class="row">
                    <div class="col-xs-7 col-sm-7 col-md-8 col-lg-8">
                        <telerik:RadGrid runat="server" ID="gridSelectMachine" AutoGenerateColumns="false" OnNeedDataSource="gridSelectMachine_NeedDataSource"
                            AllowFilteringByColumn="true" RenderMode="Lightweight">
                            <MasterTableView FilterExpression="" Caption="Macchine" DataKeyNames="Id,ZoneId" NoMasterRecordsText="Non ci sono macchine">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Code" HeaderText="<%$ Resources:Mes,Code %>" UniqueName="Code"
                                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="false" FilterDelay="1000" ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>" AllowFiltering="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Zone.Description" HeaderText="Zona" AllowFiltering="false">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <ClientEvents OnRowSelected="RowSelected" />
                                <Scrolling AllowScroll="true" ScrollHeight="500px" />
                                <Selecting AllowRowSelect="True"></Selecting>
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-4 col-lg-4">
                        <asp:Panel Width="330px" runat="server" Visible="true">
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="7" OnClientClick="MachineFilter(this);return false;" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="8" OnClientClick="MachineFilter(this);return false;" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="9" OnClientClick="MachineFilter(this);return false;" />

                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="4" OnClientClick="MachineFilter(this);return false;" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="5" OnClientClick="MachineFilter(this);return false;" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="6" OnClientClick="MachineFilter(this);return false;" />

                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="1" OnClientClick="MachineFilter(this);return false;" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="2" OnClientClick="MachineFilter(this);return false;" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="3" OnClientClick="MachineFilter(this);return false;" />
                            <asp:Button AutoPostBack="false" Width="194px" Height="60px" runat="server" Text="0" OnClientClick="MachineFilter(this);return false;" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Primary="false" Text="Canc" OnClientClick="MachineFilter(this);return false;" />
                        </asp:Panel>
                        <br /><br /><br />
                        <div class="row alDx" style="width:305px;">
                            <telerik:RadButton runat="server" ID="btnConfirmMachine" CssClass="btnActivity" DisabledButtonCssClass="btnActivityDisabled" OnClick="btnConfirmMachine_Click">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton runat="server" ID="btnCancelMachine" CssClass="btnActivity" OnClick="btnCancelMachine_Click">
                                <ContentTemplate>
                                    <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlPrimary$btnOperator" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlSelectOperator" Visible="false">
                <%--<asp:HiddenField runat="server" ID="ModifyMachineTransaction" />--%>
                <div class="row" style="margin-left:0px !important; margin-right:0px !important;">
                    <div class="col-xs-7 col-sm-7 col-md-8 col-lg-8">
                        <telerik:RadTextBox runat="server" ID="txtBadge" AutoPostBack="true" OnTextChanged="txtBadge_TextChanged"
                            Label="Badge:" LabelWidth="89px" LabelCssClass="labelComboBox" ValidationGroup="OperatorValidation"
                            Width="290px" Height="34px" EmptyMessage="Inserisci" CssClass="textNoPaddingLeft">
                            <ClientEvents OnFocus="Clear" OnValueChanged="PreventPostback" />
                        </telerik:RadTextBox>
                        <br />
                        <telerik:RadTextBox runat="server" ID="txtRegistrationId" ReadOnly="true" ValidationGroup="OperatorValidation"
                            Label="Matricola:" LabelWidth="89px" Width="290px" Height="34px" />
                        <telerik:RadTextBox runat="server" ID="txtUserName" ReadOnly="true" ValidationGroup="OperatorValidation" Width="201px"
                            Label="Nome:" LabelWidth="51px" Height="34px" />
                        <asp:RequiredFieldValidator ID="OperatorNameRequiredFieldValidator" runat="server" ControlToValidate="txtUserName"
                            EnableClientScript="true" ValidationGroup="OperatorValidation" ForeColor="red" />
                        <br />
                        <telerik:RadButton runat="server" ID="btnAddOperator" AutoPostBack="false" OnClientClicked="AddOperator" ValidationGroup="OperatorValidation" Text="Aggiungi">
                            <Icon PrimaryIconCssClass="rbAdd" />
                        </telerik:RadButton>
                        <br />
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-4 col-lg-4">
                        <asp:Panel Width="330px" runat="server" Visible="true" EnableViewState="false" HorizontalAlign="Right" CssClass="pull-right">
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="7" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="8" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="9" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="4" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="5" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="6" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="1" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="2" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Text="3" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="194px" Height="60px" runat="server" Text="0" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="95px" Height="60px" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyRegistrationId(this);return false;" EnableViewState="false" />
                        </asp:Panel>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                        <telerik:RadGrid runat="server" ID="gridOperator" RenderMode="Lightweight" AutoGenerateColumns="false" OnItemCommand="gridOperator_ItemCommand"
                            OnNeedDataSource="gridOperator_NeedDataSource" OnItemDataBound="gridOperator_ItemDataBound"
                            LocalizationPath="~/App_GlobalResources/">
                            <ClientSettings EnablePostBackOnRowClick="false">
                                <Selecting AllowRowSelect="false" UseClientSelectColumnOnly="false" />
                            </ClientSettings>
                            <MasterTableView DataKeyNames="OperatorId" Caption="Operatori" NoMasterRecordsText="Non ci sono operatori in macchina">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="OperatorId" HeaderText="Nome">
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="RoleId" HeaderText="Ruolo" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridDateTimeColumn DataField="DateStart" HeaderText="DataOra ingresso" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridTemplateColumn>
                                        <ItemStyle CssClass="noPadding" />
                                        <ItemTemplate>
                                            <telerik:RadButton runat="server" EnableViewState="false" CommandName="Delete" BorderWidth="0" Width="100%"
                                                OnClientClicking="ConfirmDeleteOperator">
                                                <ContentTemplate>
                                                    <center><i class="fa fa-sign-out" aria-hidden="true"></i> Fine</center>
                                                </ContentTemplate>
                                            </telerik:RadButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                </div>
                <br />
                <div class="row alDx" style="margin-left:0px !important; margin-right:0px !important;">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pull-right">
                    <telerik:RadButton runat="server" ID="btnConfirmConnect" OnClick="btnConfirmConnect_Click" ValidationGroup="OperatorValidation">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton runat="server" ID="btnCancelConnect" CssClass="btnActivity" OnClick="btnCancelConnect_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlPrimary$btnOrder" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlSelectOrder" Visible="false">
                <telerik:RadGrid runat="server" ID="gridOrderClosed" RenderMode="Lightweight" AutoGenerateColumns="false" AllowMultiRowEdit="true"
                    OnNeedDataSource="gridOrderClosed_NeedDataSource" Visible="true">
                    <ClientSettings>
                        <ClientEvents OnDataBound="OrderClosedLoad()" />
                    </ClientSettings>
                    <MasterTableView Caption="Commessa in corso" DataKeyNames="JobId, IsOrderClosed" FilterExpression="" NoMasterRecordsText="Non ci sono commesse in lavorazione">
                        <%--EditMode="Batch" <BatchEditingSettings EditType="Cell" />--%>
                        <Columns>
                            <telerik:GridBoundColumn DataField="OrderCode" HeaderText="Commessa" ReadOnly="true">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn DataField="OrdineCode" HeaderText="Ordine" ReadOnly="true">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ArticleCode" HeaderText="Articolo" ReadOnly="true">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ArticleDescr" HeaderText="Articolo" ReadOnly="true">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="IsOrderClosed" HeaderText="Commessa chiusa" UniqueName="Closed">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridCheckBoxColumn DataField="IsOrderSuspended" HeaderText="Commessa sospesa" UniqueName="Suspended">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridCheckBoxColumn>
                        </Columns>
                    </MasterTableView>
                    <ValidationSettings ValidationGroup="" EnableValidation="true" />
                    <ClientSettings>
                        <ClientEvents OnBatchEditCellValueChanged="BatchEditCellValueChanged" />
                    </ClientSettings>
                </telerik:RadGrid>
                <telerik:RadButton runat="server" ID="btnConfirmOrderClosed" CssClass="btnActivity" Visible="false">
                    <ContentTemplate>
                        <center><i class="fa fa-search" aria-hidden="true"></i> Commesse<br />macchina</center>
                    </ContentTemplate>
                </telerik:RadButton>
               
                <asp:Panel runat="server" ID="pnlViewOrder">
                    <div class="row">
                        <div id="pnlViewOrderBtn1" class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="visibility: hidden">
                            <telerik:RadButton runat="server" ID="btnJobAsset" OnClick="btnJobAsset_Click" CssClass="btnActivity">
                                <ContentTemplate>
                                    <center><i class="fa fa-search" aria-hidden="true"></i> Commesse<br />macchina</center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                        <div id="pnlViewOrderBtn2" class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="visibility: hidden">
                            <telerik:RadButton runat="server" ID="btnJobGroup" OnClick="btnJobGroup_Click" CssClass="btnActivity">
                                <ContentTemplate>
                                    <center><i class="fa fa-search" aria-hidden="true"></i> Commesse<br />gruppo macchine</center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                        <div id="pnlViewOrderBtn3" class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="visibility: hidden">
                            <telerik:RadButton ID="btnSearchOrder" runat="server" CssClass="btnActivity" OnClick="btnSearchOrder_Click" style="float:left;" EnableViewState="false" Visible="true">
                                <ContentTemplate>
                                    <center><i class="fa fa-search" aria-hidden="true"></i> Altre commesse</center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <asp:Panel runat="server" ID="pnlSearchOrder" Visible="false">
                                <telerik:RadTextBox ID="txtSearchOrder" runat="server" EmptyMessage="Inserisci codice" data-numpadindex="0" CssClass="inputBox nodecimal noFocus" OnTextChanged="txtSearchOrder_TextChanged" AutoPostBack="true">
                                    <ClientEvents OnLoad="OnInputBoxLoad" />
                                </telerik:RadTextBox>
                                <!-- Classe Input: sempre "inputBox" (opzionalmente puoi aggiungere: "nodecimal" per non avere decimali, e "nofocus" per far sparire la tastiera quando togli il focus e quando premi "ok" -->
                                <div id="panelNumpadContainer0"></div> <!-- div in cui viene caricata la tastiera, la sequenza finale dell'ID deve coincidere con "data-index" dell'input" -->
                                <asp:Panel ID="pnlTastieraCommessa" Width="300px" runat="server" Visible="false" style="float:left;">
                                    <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="7" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                                    <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="8" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                                    <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="9" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />

                                    <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="4" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                                    <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="5" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                                    <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="6" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />

                                    <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="1" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                                    <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="2" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                                    <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Text="3" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                                    <asp:Button AutoPostBack="false" Width="174px" Height="50px" runat="server" Text="0" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                                    <asp:Button AutoPostBack="false" Width="85px" Height="50px" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyOrderCode(this);return false;" EnableViewState="false" />
                                </asp:Panel>
                            </asp:Panel>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align:right; display:inline-block; float:right;">
                            <telerik:RadButton ID="btnConfirmSelectOrder" runat="server" CssClass="btnActivity" OnClick="btnConfirmSelectOrder_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnConfirmClosedOrder" runat="server" CssClass="btnActivity" AutoPostBack="false" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnCancelSelectOrder" runat="server" CssClass="btnActivity" OnClick="btnCancelSelectOrder_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                    </div>
                    <br />
                    <telerik:RadGrid runat="server" ID="gridSelectOrder" RenderMode="Lightweight" AutoGenerateColumns="false"
                        OnItemDataBound="gridSelectOrder_ItemDataBound">
                        <MasterTableView DataKeyNames="Id" Caption="" FilterExpression="" NoMasterRecordsText="Non ci sono commesse per questa macchina">
                            <Columns>
                                <telerik:GridBoundColumn DataField="Code" HeaderText="Fase">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Description" HeaderText="Descrizione">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CustomerOrder" HeaderText="Commessa">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Order" HeaderText="Ordine">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ArticleCode" HeaderText="Articolo">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ArticleDescr" HeaderText="Articolo">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="StartDate" HeaderText="Inizio previsto" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="Fine prevista" PickerType="DatePicker" DataFormatString="{0: dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridDateTimeColumn>
                                <%--<telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Description %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Order.CustomerOrder.OrderCode" HeaderText="<%$ Resources:Mes,Order %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Order.OrderCode" HeaderText="Ordine">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Order.Article.Code" HeaderText="<%$ Resources:Mes,Article %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Order.Article.Description" HeaderText="<%$ Resources:Mes,Article %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>--%>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlPrimary$btnBOM" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlBOM" Visible="false">
                <telerik:RadTreeView runat="server" ID="treeBOM">
                </telerik:RadTreeView>
                <div style="text-align:right; display:inline-block; float:right;">
                    <telerik:RadButton ID="btnCloseBOM" runat="server" CssClass="btnActivity" OnClick="btnCloseBOM_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlPrimary$btnPrelievo" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlPrelievo" Visible="false" GroupingText="Prelievi magazzino">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:ListView ID="lvOldBatch" runat="server" ItemPlaceholderID="itemPlaceholder">
                            <LayoutTemplate>
                                <div class="row">
                                    <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                    </div>
                                    <div class="col-xs-custom-3 col-sm-custom-3 col-md-custom-3 col-lg-custom-3">
                                        <asp:Label Text="Lotto" runat="server" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <asp:Label Text="Articolo" runat="server" />
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <asp:Label Text="Spessore" runat="server" />
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <asp:Label Text="Larghezza" runat="server" />
                                    </div>
                                    <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                        <asp:Label Text="Peso" runat="server" />
                                    </div>
                                </div>
                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div class="row" style="padding-bottom:3px;">
                                    <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                    </div>
                                    <%--<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">--%>
                                    <div class="col-xs-custom-3 col-sm-custom-3 col-md-custom-3 col-lg-custom-3">
                                        <telerik:RadTextBox ID="txtOldBatch" runat="server" ReadOnly="true" Text='<%# Eval("ArticleBatch.Code") %>' />
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <telerik:RadTextBox ID="txtOldBatchArticle" runat="server" ReadOnly="true" Text='<%# Eval("ArticleBatch.Article.Code") %>' />
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <telerik:RadTextBox ID="txtOldBatchWidth" runat="server" ReadOnly="true" Text='<%# Eval("ArticleBatch.ExtraData[" + MES.Core.MesEnum.ExtraData.Width.ToString() + "]") %>' style="text-align:right;" Width="100%" />
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <telerik:RadTextBox ID="txtOldBatchHeight" runat="server" ReadOnly="true" Text='<%# Eval("ArticleBatch.ExtraData[" + MES.Core.MesEnum.ExtraData.Height.ToString() + "]") %>' style="text-align:right;" Width="100%" />
                                    </div>
                                    <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                        <telerik:RadTextBox ID="txtOldBatchWeight" runat="server" ReadOnly="true" Text='<%# Eval("Quantity") %>' style="text-align:right;" Width="100%" />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:ListView ID="lvDynamicBatch" runat="server" ItemPlaceholderID="itemPlaceholder">
                            <LayoutTemplate>
                                <div class="row">
                                    <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                    </div>
                                    <div class="col-xs-custom-3 col-sm-custom-3 col-md-custom-3 col-lg-custom-3">
                                        <asp:Label Text="Lotto" runat="server" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <asp:Label Text="Articolo" runat="server" />
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <asp:Label Text="Spessore" runat="server" />
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <asp:Label Text="Larghezza" runat="server" />
                                    </div>
                                    <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                        <asp:Label Text="Peso" runat="server" />
                                    </div>
                                </div>
                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div class="row" style="padding-bottom:3px;">
                                    <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                        <asp:Button Text="Annulla" runat="server" ID="btnCancelBatch" CssClass="btn btn-danger" OnClick="btnCancelBatch_Click" Visible="false" />
                                    </div>
                                    <%--<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">--%>
                                    <div class="col-xs-custom-3 col-sm-custom-3 col-md-custom-3 col-lg-custom-3">
                                        <telerik:RadTextBox ID="txtBatch" runat="server" OnTextChanged="txtBatch_TextChanged" AutoPostBack="true" />
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <telerik:RadTextBox ID="txtBatchArticle" runat="server" ReadOnly="true" EmptyMessage="Articolo" />
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <telerik:RadTextBox ID="txtBatchWidth" runat="server" ReadOnly="true" EmptyMessage="Spessore" style="text-align:right;" Width="100%" />
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <telerik:RadTextBox ID="txtBatchHeight" runat="server" ReadOnly="true" EmptyMessage="Larghezza" style="text-align:right;" Width="100%" />
                                    </div>
                                    <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                        <telerik:RadTextBox ID="txtBatchWeight" runat="server" ReadOnly="true" EmptyMessage="Peso" style="text-align:right;" Width="100%" />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <telerik:RadButton runat="server" ID="btnAddBatch" OnClick="btnAddBatch_Click" Width="80px">
                            <Icon PrimaryIconCssClass="rbAdd" />
                        </telerik:RadButton>
                    </div>
                </div>
                <br />
                <div class="row">
                    <%--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">--%>
                        <asp:Panel runat="server" ID="pnlBatchExport" Visible="false">
                            <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                <asp:Label runat="server" Text="Reso a magazzino:" />
                            </div>
                            <div class="col-xs-custom-3 col-sm-custom-3 col-md-custom-3 col-lg-custom-3">
                                <telerik:RadTextBox ID="txtBatchToExport" runat="server" ReadOnly="true" />
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
                            <div class="col-xs-custom-2 col-sm-custom-2 col-md-custom-2 col-lg-custom-2">
                                <telerik:RadNumericTextBox runat="server" ClientIDMode="Static" ID="numTxtBatchRemaining" data-numpadindex="1" data-showoninit="false" CssClass="inputBox nodecimal nofocus" MinValue="0" Width="100%" ValidationGroup="BatchValidation">
                                    <NumberFormat DecimalDigits="3" />
                                    <EnabledStyle HorizontalAlign="Right" />
                                    <ClientEvents OnLoad="OnNumTxtBatchRemainingLoad" />
                                </telerik:RadNumericTextBox>
                                <div id="panelNumpadContainer1"></div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="numTxtBatchRemaining"
                                    EnableClientScript="true" ValidationGroup="BatchValidation" ForeColor="red" />
                            </div>
                            <telerik:RadButton Text="Stampa" ID="btnPrintLabel" runat="server" OnClick="btnPrintLabel_Click" ValidationGroup="BatchValidation">
                                <Icon PrimaryIconCssClass="rbPrint" />
                            </telerik:RadButton>
                        </asp:Panel>
                    <%--</div>--%>
                </div>
                <br />
                <div class="row" style="text-align:right; display:inline-block; float:right;">
                    <telerik:RadButton ID="btnConfirmPrelievo" runat="server" CssClass="btnActivity" OnClick="btnConfirmPrelievo_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnCancelPrelievo" runat="server" CssClass="btnActivity" OnClick="btnCancelPrelievo_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlPrimary$btnWaste" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlWaste" Visible="false" GroupingText="Scarti">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:HiddenField runat="server" ID="hiddenOpenFermi" />
                        <asp:HiddenField runat="server" ID="hiddenTransactionToWatch" />
                        <%--<asp:HiddenField runat="server" ID="hiddenOperatorId" />--%>
                        <asp:HiddenField runat="server" ID="hiddenCompileManualQty" />
                        <telerik:RadTextBox runat="server" ID="txtQtyProducedTot" ReadOnly="true" Label="Qta colpi totale:" LabelWidth="113px" Width="191px" style="text-align:right;" />
                        &nbsp; &nbsp; &nbsp;
                        <telerik:RadTextBox runat="server" ID="txtQtyShotProducedOperator" ReadOnly="true" Label="Qta colpi turno:" LabelWidth="111px" Width="189px" style="text-align:right;">
                            <ClientEvents OnFocus="Focus" />
                        </telerik:RadTextBox>
                        &nbsp; &nbsp; &nbsp;
                        <telerik:RadTextBox runat="server" ID="txtQtyProducedOperator" ReadOnly="true" Label="Qta pezzi turno:" LabelWidth="112px" Width="190px" style="text-align:right;" />
                        &nbsp; &nbsp; &nbsp;
                        <telerik:RadTextBox runat="server" ID="txtQtyWasteTransaction" ReadOnly="true" Label="Qta pezzi scarto:" LabelWidth="118px" Width="196px" style="text-align:right;" />
                        <br /><br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                        <asp:Panel runat="server" ID="pnlWasteTable">
                        <telerik:RadGrid runat="server" ID="gridScarti" RenderMode="Lightweight" AutoGenerateColumns="false">
                            <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId, Num" CommandItemDisplay="Top">
                                <CommandItemTemplate>
                                    <telerik:RadButton runat="server" ID="btnAddBlankShot" Text="Colpi a vuoto" Font-Bold="true" OnClick="btnAddBlankShot_Click">
                                        <Icon PrimaryIconCssClass="rbAdd" />
                                    </telerik:RadButton>
                                </CommandItemTemplate>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridNumericColumn DataField="Num" HeaderText="Dichiarazioni precedenti" DataFormatString="{0:n0}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="OperatorWaste" HeaderText="Operatore" DataFormatString="{0:n0}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="Quality" HeaderText="Qualità" DataFormatString="{0:n0}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                        </telerik:RadGrid>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlBlankShot" Visible="false">
                            <telerik:RadGrid runat="server" ID="gridBlankShot" RenderMode="Lightweight" AutoGenerateColumns="false">
                            <MasterTableView FilterExpression="" Caption="" DataKeyNames="CauseId, Num">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:Mes,Cause %>" ReadOnly="true">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridNumericColumn DataField="Num" HeaderText="Dichiarazioni precedenti" DataFormatString="{0:n0}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="OperatorWaste" HeaderText="Operatore" DataFormatString="{0:n0}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                        </telerik:RadGrid>
                            <%--<asp:TextBox runat="server" ID="txtBlankShotWithMP" />--%>
                            <br />
                            <telerik:RadButton runat="server" ID="btnBackToWaste" Text="Mostra scarti" Font-Bold="true" OnClick="btnBackToWaste_Click">
                                <Icon PrimaryIconCssClass="rbPrevious" />
                            </telerik:RadButton>
                        </asp:Panel>
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <br /><br />
                        <asp:Panel Width="330px" ID="pnlTastieraNum" runat="server" Visible="true">
                            <telerik:RadTextBox runat="server" ID="txtInsValue" ReadOnly="true" RenderMode="Lightweight" Width="278px" Visible="true">
                                <ReadOnlyStyle BackColor="White" ForeColor="Black" />
                            </telerik:RadTextBox>
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn7" runat="server" Text="7" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn8" runat="server" Text="8" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn9" runat="server" Text="9" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn4" runat="server" Text="4" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn5" runat="server" Text="5" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn6" runat="server" Text="6" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />

                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn1" runat="server" Text="1" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn2" runat="server" Text="2" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btn3" runat="server" Text="3" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="184px" Height="60px" ID="btn0" runat="server" Text="0" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btnVir" runat="server" Text="," OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button Width="184px" Height="60px" ID="btnNumPadOk" runat="server" Primary="false" Text="Ok" OnClick="btnNumPadOk_Click" />
                            <asp:Button AutoPostBack="false" Width="90px" Height="60px" ID="btnCanc" runat="server" Primary="false" Text="Canc" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                            <asp:Button AutoPostBack="false" Width="278px" Height="60px" ID="btnRecuperoScarti" runat="server" Primary="false" Text="Recupero" OnClientClick="ModifyNumber(this);return false;" EnableViewState="false" />
                        </asp:Panel>
                        <br /><br /><br />
                        <div class="row" style="text-align:right; display:inline-block; float:right;">
                            <telerik:RadButton ID="btnConfirmWaste" runat="server" CssClass="btnActivity" OnClick="btnConfirmWaste_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnCancelWaste" runat="server" CssClass="btnActivity" OnClick="btnCancelWaste_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlPrimary$btnFermi" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlFermi" Visible="false">
                <div class="row">
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <telerik:RadGrid runat="server" ID="gridFermi" RenderMode="Lightweight" AutoGenerateColumns="false" AllowMultiRowSelection="true"
                            AllowPaging="True" PageSize="10" AllowCustomPaging="false"
                            OnNeedDataSource="gridFermi_NeedDataSource" OnItemDataBound="gridFermi_ItemDataBound">
                            <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="false" FirstPageText="" LastPageText="" PrevPageText="" NextPageText="" PageSizeControlType="None" />
                            <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id">
                                <Columns>
                                    <telerik:GridClientSelectColumn>
                                        <HeaderStyle Width="30px" />
                                    </telerik:GridClientSelectColumn>
                                    <telerik:GridDateTimeColumn DataField="Start" HeaderText="<%$ Resources:Mes,Start %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                        <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridDateTimeColumn DataField="End" HeaderText="<%$ Resources:Mes,End %>" DataFormatString="{0:dd/MM HH:mm:ss}">
                                        <HeaderStyle HorizontalAlign="Center" Width="85px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridTemplateColumn DataField="Duration" HeaderText="Durata<br />[m]">
                                        <HeaderStyle HorizontalAlign="Center" Width="61px" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" Text='<%# Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem,"Duration")) / 60, 0) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:Mes,Status %>">
                                        <HeaderStyle HorizontalAlign="Center" Width="61px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Operator.UserName" HeaderText="<%$ Resources:Mes,Operator %>">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridDropDownColumn DataField="CauseId" HeaderText="<%$ Resources:Mes,Cause %>" UniqueName="ddlCause"
                                        ListTextField="ExternalCode" ListValueField="Id" DataSourceID="edsCause" DropDownControlType="RadComboBox"
                                        ConvertEmptyStringToNull="true" EnableEmptyListItem="true" EmptyListItemText="" EmptyListItemValue="">
                                        <HeaderStyle HorizontalAlign="Center" Width="61px" />
                                    </telerik:GridDropDownColumn>
                                    <telerik:GridBoundColumn DataField="Job.Order.Article.Code" HeaderText="<%$ Resources:Mes,Article %>">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" EnableVirtualScrollPaging="false" UseStaticHeaders="True" SaveScrollPosition="True" /><%--ScrollHeight="430px" />--%>
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <telerik:RadTreeView RenderMode="Lightweight" runat="Server" ID="causeTreeView" OnNodeClick="causeTreeView_NodeClick" Height="350px">
                        </telerik:RadTreeView>
                        <br /><br />
                        <div style="text-align:right">
                            <telerik:RadButton ID="btnConfirmCausaleFermo" runat="server" OnClick="btnConfirmCausaleFermo_Click" EnableViewState="false"
                                CssClass="btnActivity" DisabledButtonCssClass="btnActivityDisabled">
                                <ContentTemplate>
                                    <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnCancelCausaleFermo" runat="server" CssClass="btnActivity" OnClick="btnCancelCausaleFermo_Click" EnableViewState="false">
                                <ContentTemplate>
                                    <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlPrimary$btnArticleImage" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlArticle" Visible="false">
                <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <asp:Label ID="lblArticleImageCode" runat="server" Font-Bold="true" EnableViewState="false" />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <asp:Image ID="imgArticle" runat="server" EnableViewState="false" />
                </div>
            </div>
            <div class="row alDx">
                <telerik:RadButton ID="btnCloseArticleImage" runat="server" CssClass="btnActivity" OnClick="btnCloseArticleImage_Click" EnableViewState="false">
                    <ContentTemplate>
                        <center><i class="fa fa-times" aria-hidden="true"></i> Chiudi </center>
                    </ContentTemplate>
                </telerik:RadButton>
            </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlPrimary$btnViewOEE" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlOEE" Visible="false">
                <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="jobGrid" AutoGenerateColumns="false" GridLines="none" Skin="Silk"
                    OnNeedDataSource="jobGrid_NeedDataSource" OnItemDataBound="jobGrid_ItemDataBound"
                    LocalizationPath="~/App_GlobalResources/" Culture="en-US" EnableViewState="false">
                    <MasterTableView FilterExpression="" Caption="">
                        <ColumnGroups>
                            <telerik:GridColumnGroup Name="Tempo" HeaderText="Tempo">
                                <HeaderStyle HorizontalAlign="Center" />
                            </telerik:GridColumnGroup>
                        </ColumnGroups>
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldName="GroupType"></telerik:GridGroupByField>
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldName="GroupType" SortOrder="None"></telerik:GridGroupByField>
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Label" HeaderText="">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Pezzi" HeaderText="Pezzi" DataFormatString="{0:f0}">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Colpi" HeaderText="Cicli" DataFormatString="{0:f0}">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Teorico" HeaderText="Cicli / h">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Eseguito" HeaderText="Tempo<br/>Effettivo">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Tempo" HeaderText="Tempo<br/>Totale">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Efficienza" HeaderText="Indice %">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                <ItemStyle HorizontalAlign="Right" />
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                </telerik:RadGrid>
                <br />
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="Disponibilità" runat="server" EnableViewState="false" />
                            <telerik:RadRadialGauge runat="server" ID="meterAvailability" EnableViewState="false">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtAvailability" style="text-align:center" EnableViewState="false" />
                        </center>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="Efficienza" runat="server" EnableViewState="false" />
                            <telerik:RadRadialGauge runat="server" ID="meterEfficiency" EnableViewState="false">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtEfficiency" style="text-align:center" EnableViewState="false" />
                        </center>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="Qualità" runat="server" EnableViewState="false" />
                            <telerik:RadRadialGauge runat="server" ID="meterQuality" EnableViewState="false">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtQuality" style="text-align:center" EnableViewState="false" />
                        </center>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <center>
                            <asp:Label Font-Bold="true" Text="OEE" runat="server" EnableViewState="false" />
                            <telerik:RadRadialGauge runat="server" ID="meterOEE" EnableViewState="false">
                                <Pointer Value="0">
                                    <Cap Size="0.1" />
                                </Pointer>
                                <Scale Min="0" Max="100" MajorUnit="20">
                                    <Labels Format="{0}" />
                                    <Ranges>
                                        <telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                        <telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                        <telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                        <telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />
                                    </Ranges>
                                </Scale>
                            </telerik:RadRadialGauge>
                            <telerik:RadTextBox runat="server" ReadOnlyStyle-BackColor="White" ForeColor="Black" Font-Bold="true" ReadOnly="true" ID="txtOEE" style="text-align:center" EnableViewState="false" />
                        </center>
                    </div>
                </div>
                <br />
                <div class="row alDx">
                    <telerik:RadButton ID="btnCloseOEE" runat="server" CssClass="btnActivity" OnClick="btnCloseOEE_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> Chiudi </center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlPrimary$btnPartProgram" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlPartProgram" Visible="false">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:Label ID="lblPartProgram" runat="server" Font-Bold="true" EnableViewState="false" Text="PartProgram" AssociatedControlID="txtPartProgram" />
                        <asp:TextBox runat="server" ID="txtPartProgram" ReadOnly="true" />
                    </div>
                </div>
                <div class="row alDx">
                    <telerik:RadButton ID="btnClosePartProgram" runat="server" CssClass="btnActivity" OnClick="btnClosePartProgram_Click" EnableViewState="false">
                        <ContentTemplate>
                            <center><i class="fa fa-times" aria-hidden="true"></i> Chiudi </center>
                        </ContentTemplate>
                    </telerik:RadButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="pnlPrimary$btnMold" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlMold" Visible="false">
                <div class="row">
                    <telerik:RadGrid runat="server" RenderMode="Lightweight" ID="gridSelectMold" AutoGenerateColumns="false"
                        OnNeedDataSource="gridSelectMold_NeedDataSource">
                        <MasterTableView Caption="Stampi" DataKeyNames="Id" FilterExpression="" NoMasterRecordsText="Non ci sono stampi per questo articolo">
                            <Columns>
                                <telerik:GridBoundColumn DataField="Name" HeaderText="Stampo">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CavityMoldNum" HeaderText="Impronte">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
                <div class="row">
                    <div style="text-align:right; display:inline-block; float:right;">
                        <telerik:RadButton ID="btnConfirmMold" runat="server" CssClass="btnActivity" OnClick="btnConfirmMold_Click" EnableViewState="false">
                            <ContentTemplate>
                                <center><i class="fa fa-check" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Confirm") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnCancelMold" runat="server" CssClass="btnActivity" OnClick="btnCancelMold_Click" EnableViewState="false">
                            <ContentTemplate>
                                <center><i class="fa fa-times" aria-hidden="true"></i> <%: GetGlobalResourceObject("Mes","Cancel") %></center>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        // Commessa in corso
        var orderClosedLoaded = false;
        var closedCheck = undefined, suspendedCheck = undefined;

        function ResetClosedLoaded() {
            orderClosedLoaded = false;
        }

        function HideOrderClosed() {
            $("#ctl00_MainContent_btnConfirmClosedOrder").css("display", "none");
            $("#ctl00_MainContent_btnConfirmSelectOrder").css("visibility", "inherit");
            $("#pnlViewOrderBtn1").css("visibility", "inherit");
            $("#pnlViewOrderBtn2").css("visibility", "inherit");
            $("#pnlViewOrderBtn3").css("visibility", "inherit");
            $("#ctl00_MainContent_gridOrderClosed").css("display", "none");
            $("#ctl00_MainContent_gridSelectOrder").css("visibility", "inherit");
        }

        function OrderClosedLoad() {
            if (!orderClosedLoaded && $("#ctl00_MainContent_gridOrderClosed .rgNoRecords").html() == undefined) {
                orderClosedLoaded = true;

                $("#ctl00_MainContent_gridOrderClosed").css("margin-bottom", "20px");

                $("#ctl00_MainContent_btnConfirmSelectOrder").css("visibility", "hidden");
                $("#ctl00_MainContent_gridSelectOrder").css("visibility", "hidden");

                $("#ctl00_MainContent_gridOrderClosed input[type=checkbox]").each(function (key, e) {
                    var row = $(e).parent().parent().parent();
                    var td = row.children().eq(0);

                    e.setAttribute("commessa", td.html());

                    td = row.children().eq(1);
                    e.setAttribute("articolo", td.html())

                    e.removeAttribute("disabled");

                    if (key == 0) {
                        closedCheck = e;

                        $(e).change(function () {
                            if (this.checked) {
                                $(suspendedCheck).prop('checked', false);
                            }
                        });
                    }
                    else {
                        suspendedCheck = e;

                        $(e).change(function () {
                            if (this.checked) {
                                $(closedCheck).prop('checked', false);
                            }
                        })
                    }
                });

                $("#ctl00_MainContent_btnConfirmClosedOrder").click(function () {
                    var closed = $(closedCheck).prop("checked"),
                        suspended = $(suspendedCheck).prop("checked");

                    if (closed || suspended) {
                        var CLOSED_STATUS = 5,
                            SUSPENDED_STATUS = 0;

                        HideOrderClosed();

                        __doPostBack("<%= btnConfirmClosedOrder.ClientID %>", (closed) ? CLOSED_STATUS : SUSPENDED_STATUS);
                        return false;
                    }
                });
            } else {
                HideOrderClosed();
            }
        }
    </script>

    <script type="text/javascript">
       /* function pageLoad() {
            var timer = $find('Timer1.ClientID');
            var panel = document.getElementById("pnlPrimary.ClientID");
            if (panel) {
            }
            else {
                timer.set_enabled(false);
                timer._stopTimer();
            }
        } */

        function ViewOEE() {
            if ($find('<%= btnOrder.ClientID %>').get_text() != '')
            {
                document.getElementById('<%= btnViewOEE.ClientID %>').click();
            }
        }

        function BatchEditCellValueChanged(sender, args) {
            var batchEditingManager = sender.get_batchEditingManager();
            batchEditingManager.saveAllChanges();
        }

        function ConfirmDeleteOperator(sender, args) {
            args.set_cancel(!window.confirm("Confermi fine lavoro?"));
        }

        function ModifyRegistrationId(sender) {
            var textBox = $find('<%= txtBadge.ClientID %>');
            var txtValue = textBox.get_value();
            var char = sender.value;//sender.get_text();
            if (char == "Canc") {
                if (txtValue.length > 0) {
                    textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                }
            }
            else {
                if (char == ',' || char == '.') {
                    if (txtValue.includes(',') || txtValue.includes('.')) { }
                    else {
                        if (txtValue.length == 0) {
                            textBox.set_value('0' + char);
                        }
                        else {
                            textBox.set_value(txtValue + char);
                        }
                    }
                }
                else {
                    textBox.set_value(txtValue + char);
                }
            }
        }

        function ModifyNumber(sender) {
            var btnOK = document.getElementById('<%= btnNumPadOk.ClientID %>');
            var textBox = $find("<%= txtInsValue.ClientID %>");
            var txtValue = textBox.get_value();
            var char = sender.value;//sender.get_text();
            if (char == "Recupero") {
                if (txtValue.includes('-')) { }
                else {
                    textBox.set_value('-' + txtValue);
                }
            }
            else {
                if (char == "Canc") {
                    if (txtValue.length > 0) {
                        textBox.set_value(txtValue.substring(0, txtValue.length - 1));
                    }
                }
                else {
                    if (char == ',' || char == '.') {
                        if (txtValue.includes(',') || txtValue.includes('.')) { }
                        else {
                            if (txtValue.length == 0) {
                                textBox.set_value('0' + char);
                            }
                            else {
                                textBox.set_value(txtValue + char);
                            }
                        }
                    }
                    else {
                        textBox.set_value(txtValue + char);
                    }
                }
            }
            btnOK.classList.add("btnOkToClick");
            //btnOK.className += " btnOkToClick";
            //return false;
        }

        function Focus(sender, eventArgs) {
            document.getElementById("<%= hiddenCompileManualQty.ClientID %>").value = "True";
        }

        function Clear(sender, eventArgs) {
            sender.set_value("");
        }

        function AddOperator()
        {
            $find("<%= txtBadge.ClientID %>").set_value("");
            $find("<%= txtRegistrationId.ClientID %>").set_value("");
            $find("<%= txtUserName.ClientID %>").set_value("");
        }

        function EnablePrintLabel(sender, eventArgs)
        {
            $find("<%= btnPrintLabel.ClientID %>").set_enabled(true);
        }

        function PreventPostback(sender, eventArgs) {
            if (eventArgs.get_newValue().length < 3)
                eventArgs.set_cancel(true);
        }

        function MachineFilter(sender) {
            var grid = $find("<%=gridSelectMachine.ClientID %>");
            grid.get_masterTableView()._filterExpressions.clear();

            var txtValue = $("#ctl00_MainContent_gridSelectMachine_ctl00_ctl02_ctl01_FilterTextBox_Code").val();
            var char = sender.value;
            if (char == "Canc") {
                if (txtValue.length > 0) {
                    var filterValue = txtValue.substring(0, txtValue.length - 1);
                    grid.get_masterTableView().filter("Code", filterValue, Telerik.Web.UI.GridFilterFunction.Contains, true);
                }
            }
            else {
                var filterValue = txtValue + char;
                grid.get_masterTableView().filter("Code", filterValue, Telerik.Web.UI.GridFilterFunction.Contains, true);
            }
        }

        function RowSelected(sender, eventArgs) {
            var btn = $find("<%= btnConfirmMachine.ClientID %>");
            btn.set_enabled(true);
        }
    </script>

    <script>
        // NUMPAD
        selectedInputBoxIndex = -1;
        countInputBox = 0;
        arrayInputBox = new Array();
        initFlag = true;

        function changeSelectedInputBox(indx)
        {
            $(arrayInputBox[selectedInputBoxIndex]).removeClass("riFocused");
            $(arrayInputBox[indx]).addClass("riFocused");

            selectedInputBoxIndex = indx;
            $(arrayInputBox[indx]).trigger("click");

            if (initFlag) {
                if (countInputBox > 0)
                    $(arrayInputBox[0]).trigger("click", true);
                initFlag = false;
            }
        }

        function incrementInputBoxIndex()
        {
            //var newIndex = selectedInputBoxIndex + 1;
            //if (newIndex > countInputBox - 1) newIndex = 0;
            //changeSelectedInputBox(newIndex);
            $(arrayInputBox[selectedInputBoxIndex]).trigger("blur");
            __doPostBack($find(<%= txtSearchOrder.ClientID%>), '');
        }
    </script>
    <!-- DEV'ESSERE CARICATO DOPO LE FUNZIONI SOVRASTANTI -->
    <script src="Script\jquery.numpad.js" type="text/javascript"></script>
    <script>  
        // Evento per NUMPAD dell'input numTxtBatchRemaining
        function OnNumTxtBatchRemainingLoad() {
            $("#panelNumpadContainer1").css("position", "absolute");
            $("#panelNumpadContainer1").css("z-index", "100");
            $("#panelNumpadContainer1").css("right", 15);
            OnInputBoxLoad(false);
        }

        function OnInputBoxLoad(showOnInit = true) {
            // NUMPAD
            // #tab -> .inputBox
            //$('#tab .inputBox').each(function () {

            $('.inputBox').each(function () {
                arrayInputBox.push($(this));
                $(this).attr("data-index", countInputBox);

                $(this).numpad({numpadIndex: $(this).attr("data-numpadindex")});

                $(this).click(function () {
                    var indx = parseInt($(this).attr("data-index"));
                    if (selectedInputBoxIndex != indx) {
                        changeSelectedInputBox(indx);
                    }
                })

                countInputBox++;
            });

            // Seleziona il primo input
            if (countInputBox > 0 && showOnInit)
                $(arrayInputBox[0]).trigger("click", true);
        }
    </script>

    <ef:EntityDataSource ID="edsCause" runat="server" ContextTypeName="MES.Models.MesDbContext" EntitySetName="Causes"
        OrderBy="it.Description" AutoGenerateWhereClause="true" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>