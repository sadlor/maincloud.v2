﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.HMI.Models
{
    public class OrderCloseViewModel
    {
        public string JobId { get; set; }
        public string OrderCode { get; set; }
        public string OrdineCode { get; set; }
        public string ArticleCode { get; set; }
        public string ArticleDescr { get; set; }
        public bool IsOrderClosed { get; set; }
        public bool IsOrderSuspended { get; set; }

        public OrderCloseViewModel(string jobId, string ordineCode, string orderCode, string articleCode, string articleDescr, bool isOrderClosed, bool isOrderSuspended)
        {
            JobId = jobId;
            OrdineCode = ordineCode;
            OrderCode = orderCode;
            ArticleCode = articleCode;
            ArticleDescr = articleDescr;
            IsOrderClosed = isOrderClosed;
            IsOrderSuspended = isOrderSuspended;
        }
    }
}