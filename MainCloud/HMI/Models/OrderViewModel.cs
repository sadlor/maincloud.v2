﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.HMI.Models
{
    public class OrderViewModel
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Order { get; set; }
        public string CustomerOrder { get; set; }
        public string ArticleCode { get; set; }
        public string ArticleDescr { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}