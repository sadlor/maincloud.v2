﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.HMI.Models
{
    [Serializable]
    public class StopViewModel
    {
        public int TransactionId { get; set; }
        public string CauseId { get; set; }
        public string Description { get; set; }
        public decimal Duration { get; set; }

        public StopViewModel(int tId, string causeId)
        {
            TransactionId = tId;
            CauseId = causeId;
        }

        public StopViewModel(string causeId, string description, decimal duration)
        {
            CauseId = causeId;
            Description = description;
            Duration = duration;
        }
    }
}