﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MainCloud.HMI.Models
{
    public class GenericDbContext : DbContext
    {
        public GenericDbContext() : base("GenericConnection")
        {
        }
    }
}