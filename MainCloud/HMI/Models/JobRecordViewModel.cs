﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.HMI.Models
{
    public class JobRecordViewModel
    {
        public string Label { get; set; }
        public decimal? Pezzi { get; set; }
        public decimal? Colpi { get; set; }
        public decimal? Teorico { get; set; }
        public string Eseguito { get; set; }
        public string Tempo { get; set; }
        public string Efficienza { get; set; }
        public string GroupType { get; set; }

        public JobRecordViewModel(string label, decimal? pezzi, decimal? colpi, decimal? teorico, TimeSpan? eseguito, TimeSpan? tempo, string efficienza, string groupType)
        {
            Label = label;
            Pezzi = pezzi;
            Colpi = colpi;
            Teorico = teorico;
            if (eseguito != null)
            {
                Eseguito = string.Format("{0}:{1:d2}:{2:d2}", (int)eseguito?.TotalHours, Math.Abs(eseguito.Value.Minutes), Math.Abs(eseguito.Value.Seconds));
            }
            if (tempo != null)
            {
                Tempo = string.Format("{0}:{1:d2}:{2:d2}", (int)tempo?.TotalHours, tempo?.Minutes, tempo?.Seconds);
            }
            Efficienza = efficienza;
            GroupType = groupType;
        }
    }
}