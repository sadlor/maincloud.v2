﻿using MES.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.HMI.Models
{
    public class ChartIndex
    {
        private ConfigurationService configService = new ConfigurationService();

        private decimal _indexValue;
        public decimal IndexValue
        {
            get { return _indexValue; }
            set { _indexValue = value; }
        }

        private string _indexColor;
        public string IndexColor
        {
            get { return _indexColor; }
            set { _indexColor = value; }
        }

        public ChartIndex(decimal indexValue, ParamContext index)
        {
            _indexValue = indexValue;
            _indexColor = configService.GetColorByParameter(index, indexValue).Name;
        }
    }
}