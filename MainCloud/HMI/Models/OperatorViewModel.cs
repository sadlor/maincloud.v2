﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.HMI.Models
{
    [Serializable]
    public class OperatorViewModel
    {
        public string OperatorId { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public bool Open { get; set; }

        public OperatorViewModel()
        {}

        public OperatorViewModel(string operatorId, DateTime start, DateTime? end, bool open)
        {
            OperatorId = operatorId;
            DateStart = start;
            DateEnd = end;
            Open = open;
        }
    }
}