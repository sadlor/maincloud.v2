﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainCloud.HMI.Models
{
    [Serializable]
    public class WasteViewModel
    {
        public string CauseId { get; set; }
        public string Description { get; set; } // code - description
                                                /// <summary>
                                                /// Totale scarti dichiarati in precedenza
                                                /// </summary>
        public decimal Num { get; set; }
        /// <summary>
        /// Scarti dichiaarti dall'operatore corrente
        /// </summary>
        public decimal OperatorWaste { get; set; }
        /// <summary>
        /// Scarti dichiarati da un controllo qualità
        /// </summary>
        public decimal Quality { get; set; }

        public WasteViewModel(string id, string description)
        {
            CauseId = id;
            Description = description;
        }

        public WasteViewModel(string id, string description, decimal num, decimal operatorWaste, decimal quality = 0)
        {
            CauseId = id;
            Description = description;
            Num = num;
            OperatorWaste = operatorWaste;
            Quality = quality;
        }
    }
}