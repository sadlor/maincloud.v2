﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/HMI/MainDashboardHMI.Master" CodeBehind="MaintenancePlanExecutionList.aspx.cs" Inherits="MainCloud.HMI.MaintenancePlan.MaintenancePlanExecutionList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="..\Style\Style.css?111" rel="stylesheet" type="text/css">
    <link href="MaintenancePlanExecutionListStyle.css" rel="stylesheet" />
    <link href="..\Style\jquery.numpad.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <script>
        function removeModal() {
            __doPostBack('<%=dlgConfirmSi.ClientID %>', "refresh");
        }
    </script>

<%--<asp:UpdatePanel runat="server">
    <ContentTemplate>--%>
    <mcf:PopUpDialog ID="dlgConfirmDialog" runat="server" Title="Conferma">
        <Body>
            <asp:Label Text="Confermare i controlli inseriti?" runat="server" /><br />
            <br />
            <telerik:RadButton runat="server" ID="dlgConfirmSi" ClientIDMode="Static" Text="Si" OnClientClicking="removeModal" OnClick="dlgConfirm_Si_Click"></telerik:RadButton>
            <telerik:RadButton runat="server" ID="dlgConfirmNo" ClientIDMode="Static" Text="No" OnClientClicking="removeModal" OnClick="dlgConfirm_No_Click"></telerik:RadButton>
        </Body>
    </mcf:PopUpDialog>

    <mcf:PopUpDialog ID="dlgErrorDialog" runat="server" Title="Errore">
        <Body>
            <asp:Label Text="Non sono stati compilati tutti i campi del piano di manutenzione." runat="server" /><br />
            <br />
            <telerik:RadButton runat="server" ID="dlgErrorOk" ClientIDMode="Static" Text="Ok" OnClientClicking="removeModal" OnClick="dlgErrorOk_Click"></telerik:RadButton>
        </Body>
    </mcf:PopUpDialog>

    <div style="width: 100%;">
        <div class="headerDiv">
            <div style="width: 6%;">
                <asp:Label runat="server" Text="Cod." />
                <asp:Label class="mpeBkTitolo " ID="lblOpCode" Width="100%" runat="server" Text="---" />
            </div>
            <div style="width: 25%;">
                <asp:Label ID="lblExecutor" runat="server" Text="Operatore" />
                <asp:Label class="mpeBkTitolo " ID="lblOpUserName" Width="100%" runat="server" Text="---" />
            </div>
            <div style="width: 20%;">
                <asp:Label ID="lblObjectType" runat="server" Text="Cod. Asset" />
                <asp:Label class="mpeBkTitolo " ID="lblObjectId" Width="100%" runat="server" Text="---" />
            </div>
            <div style="width: 49%;">
                <asp:Label runat="server" Text="Descr. Piano di Manutenzione" />
                <asp:Label class="mpeBkTitolo " ID="lblmpDescription" Width="100%" runat="server" Text="---" />
            </div>
        </div>
    </div>
    <div id="tableContainer" style="width: 100%; margin-top: 20px;">
        <telerik:RadGrid ID="mpdTable" ClientIDMode="Static" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false" AllowPaging="false" AllowMultiRowSelection="false" OnNeedDataSource="mpdTable_NeedDataSource" LocalizationPath="~/App_GlobalResources/" Culture="en-US">
            <MasterTableView FilterExpression="" Caption="" DataKeyNames="Id" NoMasterRecordsText="No records to display." Width="100%">
                <Columns>
                    <telerik:GridBoundColumn DataField="Description" HeaderText="Descrizione" ItemStyle-Width="280px" MaxLength="40" ItemStyle-Wrap="true">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ItemCode" HeaderText="Cod. Particolare" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ToolCode" HeaderText="Cod. Strumento" ItemStyle-Width="80px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DetailNotes" HeaderText="Note" MaxLength="2000" ItemStyle-Wrap="true" ItemStyle-Width="250px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="MeasuredValue" HeaderText="Completamento" ItemStyle-Wrap="true" ItemStyle-Width="70px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="measuredValueLbl" ClientIDMode="AutoID" data-numpadindex="0" CssClass="control-label inputBox" Text=""
                                data-type='visivo'
                                data-toolcode='<%# Eval("ToolCode") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="" ItemStyle-Wrap="true" ItemStyle-Width="30px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label runat="server" ID="resultLbl" ClientIDMode="AutoID" CssClass="control-label" Text=""></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="True" EnableColumnClientFreeze="true" FrozenColumnsCount="1" UseStaticHeaders="false" />
            </ClientSettings>
        </telerik:RadGrid>
    </div>
    <br />
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mpeAlDx nop" style="margin-bottom: 10px!important">
        <div id="panelOkNoPadContainer" class="col-md-6 nop text-left"">
            <table>
                <tr>
                    <td><button id="padButtonOk" onclick="return false;">Effettuato</button></td>
                    <td><button id="padButtonNo" onclick="return false;">Non effettuabile</button></td>
                </tr>
                <tr>
                    <td><button id="padButtonStop" onclick="return false;">Sospeso</button></td>
                    <td><button id="padButtonSkip" onclick="return false;">Saltato</button></td>
                </tr>
            </table>
        </div>
        <div class="col-md-6 nop text-right" style="height: 215px; position: relative;">
            <telerik:RadButton RenderMode="Lightweight" Width="100px" Height="36px" ID="btnSaveData" Style="position: absolute; bottom: 0; right: 106px; color: #000" CssClass="mb-3" OnClick="btnSaveData_Click" runat="server" Primary="true" Text="Conferma" />
            <telerik:RadButton RenderMode="Lightweight" Width="100px" Height="36px" ID="btnCloseData" Style="position: absolute; bottom: 0; right: 0; color: #000" CssClass="mb-3" OnClick="btnCloseData_Click" runat="server" Primary="false" Text="Annulla" />
        </div>
    </div>

    <div id="maintenanceAddForm" class="rgEditForm RadGrid_Bootstrap rgEditPopup" style="width: 400px; position: fixed; top: 10%; z-index: 2500; left: 445px; display: none;" tabindex="0">
        <div id="maintenanceAddFormHeader" class="rgHeader" style="cursor:move;">
            <div style="float:left; position: relative; top: 8px; left: 10px;">
				Intervento aggiuntivo
			</div>
            <div style="clear:both;display:none;"></div>
        </div>
        <div style="float:right; position: relative; top: -30px; right: 10px;">
			<a id="extraMaintenanceFormClose" style="text-decoration:none;"><img title="Close" src="/WebResource.axd?d=EJ5TU3KBIxMV9V41DLVyrUcYA-mJ8UD2LJUl3X0AJxtOEDL4kT5rrd23HYeUGoYpBzaYEH0f-1u44PHV4xgn8pSRHkP5sEewuht-vWFf6FtPk7Y7tN3cHkWD2i56woRcJ2lA8EYP2JeW8Zv_bi0wow2&amp;amp;t=636722546483418768" alt="Close" style="border:0;cursor:pointer;float:left;"></a>
		</div>
        <div class="rgEditFormContainer" style="min-height: 277px; width: 100%;">
            <table>
				<thead>
					<tr style="display:none;">
						<th scope="col"></th>
					</tr>
				</thead><tbody>
					<tr>
						<td><label for="extraMaintenanceDescription">Descrizione:</label></td>
                        <td>
                            <div class="col-md-12">
                                <span class="RadInput RadInput_Bootstrap RadInputMultiline RadInputMultiline_Bootstrap" style="width:100%;">
                                    <textarea id="extraMaintenanceDescription" rows="2" cols="20" class="riTextBox riEnabled" maxlength="40"></textarea>
                                </span>
                            </div>
                        </td>
					</tr>
                    <tr>
						<td><label for="extraMaintenanceItemCode">Cod. Particolare:</label></td>
                        <td>
                            <div class="col-md-12">
                                <span class="RadInput RadInput_Bootstrap" style="width:100%;">
                                    <input id="extraMaintenanceItemCode" size="20" maxlength="12" class="riTextBox riEnabled" type="text" value="">
                                </span>
                            </div>
                        </td>
					</tr>
                    <tr>
						<td><label for="extraMaintenanceToolCode">Cod. Strumento:</label></td>
                        <td>
                            <div class="col-md-12">
                                <span class="RadInput RadInput_Bootstrap" style="width:100%;">
                                    <input id="extraMaintenanceToolCode" size="20" maxlength="12" class="riTextBox riEnabled" type="text" value="">
                                </span>
                            </div>
                        </td>
					</tr>
                    <tr>
						<td><label for="extraMaintenanceNotes">Note:</label></td>
                        <td>
                            <div class="col-md-12">
                                <span class="RadInput RadInput_Bootstrap RadInputMultiline RadInputMultiline_Bootstrap" style="width:100%;">
                                    <textarea id="extraMaintenanceNotes" rows="2" cols="20" class="riTextBox riEnabled" maxlength="2000"></textarea>
                                </span>
                            </div>
                        </td>
					</tr>
				</tbody>
			</table>
            <br>
            <button id="addExtraMaintenance" type="button" value="Add" title="Add" class="rgActionButton rgUpdate"><span class="rgIcon rgUpdateIcon"></span></button>&nbsp;<button id="extraMaintenanceFormCancel" type="button" value="Cancel" title="Cancel" class="rgActionButton rgCancel"><span class="rgIcon rgCancelIcon"></span></button>
        </div>
    </div>

    <div id="modalbg" class="GridModal_Bootstrap" style="width: 100%; height: 200%; left: 0px; top: 0px; position: fixed; background-color: threedshadow; z-index: 2490; opacity: 0.5; display:none;"></div>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:HiddenField ClientIDMode="Static" ID="valuesList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="selectedInputBoxIndex" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="extraDescriptionList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="extraItemCodeList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="extraToolCodeList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="extraNotesList" runat="server" />
            <asp:HiddenField ClientIDMode="Static" ID="extraCount" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        selectedInputBoxIndex = -1;
        countInputBox = 0;
        arrayInputBox = new Array();
        arrayNMPD = new Array();
        initFlag = true;

        function scrollTo(parent, child) {
            try {
                var diff = ($(child).offset().top + $(parent).scrollTop()) - $(parent).offset().top;
                diff = (diff - $(parent).parent().height()) + $(child).parent().height();
                if (diff < 0) diff = 0;

                if (!isNaN(diff)) {
                    $(parent).css("position", "relative");
                    $(parent).animate({ scrollTop: diff }, 80);
                }
            } catch (ex) { }
        }

        function changeSelectedInputBox(indx) {
            if (selectedInputBoxIndex > -1)
                findResult($(arrayInputBox[selectedInputBoxIndex]), selectedInputBoxIndex);

            $(arrayInputBox[selectedInputBoxIndex]).parent().css("background-color", (selectedInputBoxIndex % 2 == 0) ? "transparent" : "#f9f9f9");
            $(arrayInputBox[indx]).parent().css("background-color", "LightBlue");

            if ($(arrayInputBox[indx]).attr("data-type") == "Dimensionale") {
                $("#panelNumpadContainer"+$(arrayInputBox[indx]).attr("data-numpadindex")).css("display", "inherit");
                $("#panelOkNoPadContainer").css("display", "none");
            }
            else {
                $("#panelNumpadContainer"+$(arrayInputBox[indx]).attr("data-numpadindex")).css("display", "none");
                $("#panelOkNoPadContainer").css("display", "inherit");
            }

            selectedInputBoxIndex = indx;
            $(arrayInputBox[indx]).trigger("click");

            scrollTo($("#mpdTable_GridData"), $(arrayInputBox[indx]).parent());

            var sep = "|&|";
            if (!initFlag) {
                $("#selectedInputBoxIndex").val(selectedInputBoxIndex);

                var vals = "";
                $(arrayInputBox).each(function (key, value) {
                    vals += $(value).html().replace("<span></span>", "") + sep;
                });
                if (vals.length > 0) {
                    $("#valuesList").val(vals.substring(0, vals.length - sep.length));
                }
            } else {
                if ($("#extraDescriptionList").val().length > 0) {
                    // Load additional activities
                    var extraDArr = String($("#extraDescriptionList").val()).split(sep),
                        extraICArr = String($("#extraItemCodeList").val()).split(sep),
                        extraTCArr = String($("#extraToolCodeList").val()).split(sep),
                        extraNArr = String($("#extraNotesList").val()).split(sep),
                        extraCount = parseInt($("#extraCount").val());

                    for (var i = 0; i < extraCount; i++) {
                        if (extraDArr[i] == undefined) extraDArr[i] = "";
                        if (extraICArr[i] == undefined) extraICArr[i] = "";
                        if (extraTCArr[i] == undefined) extraTCArr[i] = "";
                        if (extraNArr[i] == undefined) extraNArr[i] = "";
                        addNewActivity(extraDArr[i], extraICArr[i], extraTCArr[i], extraNArr[i]);
                    }
                }

                var valsArr = String($("#valuesList").val()).split(sep);

                $(arrayInputBox).each(function (key, value) {
                    $(value).html(valsArr[key]);
                    findResult($(value), key);
                });

                if (countInputBox > 0)
                    $(arrayInputBox[0]).trigger("click", true);

                initFlag = false;

                var selectedIndx = $("#selectedInputBoxIndex").val();
                if (selectedIndx != "" && selectedIndx > -1) {
                    changeSelectedInputBox(parseInt(selectedIndx));
                }
            }
        }

        function incrementInputBoxIndex() {
            var newIndex = selectedInputBoxIndex + 1;
            if (newIndex > countInputBox - 1) newIndex = 0;
            changeSelectedInputBox(newIndex);
        }

        function findResult(inputBox, indx) {
            var controlType = inputBox.attr("data-type"),
                value = inputBox.text(),
                result = "";

            var bg = (indx % 2 == 0) ? "transparent" : "#f9f9f9";

            if (value == "Effettuato") {
                result = "✓";
                bg = "PaleGreen";
            }
            else if (value == "Non effettuabile")
            {
                result = "✕";
                bg = "LightCoral";
            }
            else if (value == "Sospeso")
            {
                result = "―";
                bg = "#FBFB98";
            }
            else if (value == "Saltato")
            {
                result = "￬";
            }

            var currentRow = inputBox.parent().parent(),
                tdCount = $(currentRow).find("td").length,
                resultTD = $(currentRow).find("td").eq(tdCount-1),
                resultSpan = $(resultTD).find("span");

            $(resultSpan).text(result);
            $(resultTD).css("background-color", bg);
        }
    </script>
    <script src="..\Script\jquery.numpad.js" type="text/javascript"></script>
    <script>
        function showAddNewActivity()
        {
            var lc = $("#mpdTable_GridData tr").last();

            if (lc != undefined) {
                var c = (lc.attr("class") == "rgAltRow") ? "rgRow" : "rgAltRow";
                var i = "";
                if (lc.attr("id") != undefined) {
                    i = parseInt(lc.attr("id").replace('__', '')) + 1;
                }

                var html = '<tr class="' + c + '" id="__' + i + '">';
                html += '<td align="center" colspan="6">';
                html += '<button class="btn-c" onclick="return false;">Crea intervento aggiuntivo</button>';
                html += '</td>';
                html += '</tr>';

                lc.after(html);

                lc = $("#mpdTable_GridData tr").last();

                var btn = lc.find(".btn-c");
                btn.click(function () {
                    showNewActivityForm();
                });
            }
        }

        function showNewActivityForm()
        {
            $("#modalbg").css("display", "inherit");
            $("#maintenanceAddForm").css("display", "inherit");
        }

        function closeActivityForm()
        {
            $("#maintenanceAddForm").css("display", "none");
            $("#modalbg").css("display", "none");

            $("#extraMaintenanceDescription").val("");
            $("#extraMaintenanceItemCode").val("");
            $("#extraMaintenanceToolCode").val("");
            $("#extraMaintenanceNotes").val("");
        }

        function addNewActivity(description, itemCode, toolCode, notes)
        {
            var lc = $("#mpdTable_GridData tr").last();

            if (lc != undefined) {
                var slc = $("#mpdTable_GridData tr").last().prev();

                if (slc != undefined) {
                    var c = (slc.attr("class") == "rgAltRow") ? "rgRow" : "rgAltRow";
                    var i = parseInt(slc.attr("id").replace('__', '')) + 1;
            
                    var html = '<tr class="' + c + '" id="__' + i + '">';
                    html += '<td align="left" style="width:280px;">'+description+'</td>';
                    html += '<td align="center" style="width:80px;">'+itemCode+'</td>';
                    html += '<td align="center" style="width:80px;">'+toolCode+'</td>';
                    html += '<td align="left" style="width:250px;">'+notes+'</td>';
                    html += '<td align="center" style="width: 70px;">';
                    html += '<span class="control-label inputBox" data-numpadindex="0" data-type="visivo" data-toolcode='+toolCode+'><span>';
                    html += '</td>';
                    html += '<td align="center" style="width: 30px;">';
                    html += '<span class="control-label"><span>';
                    html += '</td>';
                    html += '</tr>';

                    slc.after(html);

                    $("#mpdTable_GridData tr").last().attr("class", (c == "rgAltRow") ? "rgRow" : "rgAltRow");

                    // Events
                    slc = $("#mpdTable_GridData tr").last().prev();
                    var nspan = slc.find(".inputBox");
                    arrayInputBox.push(nspan);
                    nspan.attr("data-index", countInputBox);

                    $(nspan.parent()).click(function () {
                        var indx = parseInt($(this).children().attr("data-index"));
                        if (selectedInputBoxIndex != indx) {
                            changeSelectedInputBox(indx);
                        }
                    });
                    countInputBox++;

                    if (!initFlag) {
                        // Save on server

                        var edl = $("#extraDescriptionList"),
                            eicl = $("#extraItemCodeList"),
                            etcl = $("#extraToolCodeList"),
                            enl = $("#extraNotesList"),
                            ec = $("#extraCount"),
                            sep = "|&|";

                        edl.val(edl.val() + (edl.val().length > 0 ? sep + description : description));
                        eicl.val(eicl.val() + (eicl.val().length > 0 ? sep + itemCode : itemCode));
                        etcl.val(etcl.val() + (etcl.val().length > 0 ? sep + toolCode : toolCode));
                        enl.val(enl.val() + (enl.val().length > 0 ? sep + notes : notes));

                        ec.val(parseInt(ec.val()) + 1);
                    }
                }
            }
        }

        function addListeners() {
            document.getElementById('maintenanceAddFormHeader').addEventListener('mousedown', mouseDown, false);
            window.addEventListener('mouseup', mouseUp, false);
        }

        function mouseUp()
        {
            window.removeEventListener('mousemove', divMove, true);
        }

        function mouseDown(e){
            window.addEventListener('mousemove', divMove, true);
        }

        function divMove(e) {
            var div = document.getElementById('maintenanceAddForm'),
                Y = e.clientY - 60,
                X = e.clientX - 30;
            div.style.position = 'absolute';
            div.style.top = Y + 'px';
            div.style.left = X + 'px';
        }

        $(document).ready(function () {
            var ec = $("#extraCount");
            if (ec.val().length == 0) ec.val(0);

            // Add Activity Form
            $("body form").append($("#modalbg"));
            addListeners();
            $("#extraMaintenanceFormClose").click(function () {
                closeActivityForm();
            })
            $("#extraMaintenanceFormCancel").click(function () {
                closeActivityForm();
            })
            $("#addExtraMaintenance").click(function () {
                var eMD = $("#extraMaintenanceDescription").val(),
                    eMIC = $("#extraMaintenanceItemCode").val(),
                    eMTC = $("#extraMaintenanceToolCode").val(),
                    eMN = $("#extraMaintenanceNotes").val();

                if (eMD.length > 0) {
                    addNewActivity(eMD, eMIC, eMTC, eMN);
                    closeActivityForm();
                }
            })
            showAddNewActivity();
            //

            $("#panelOkNoPadContainer").css("display", "none");

            $("#mpdTable_GridData").attr("style", "overflow: auto;width: 100%;max-height: 260px; height: auto;position: relative;");

            $('#mpdTable .inputBox').each(function () {
                arrayInputBox.push($(this));
                $(this).attr("data-index", countInputBox);

                $($(this).parent()).click(function () {
                    var indx = parseInt($(this).children().attr("data-index"));
                    if (selectedInputBoxIndex != indx) {
                        changeSelectedInputBox(indx);
                    }
                })
                countInputBox++;
            });

            $("#padButtonOk").click(function () {
                $(arrayInputBox[selectedInputBoxIndex]).text("Effettuato");
                incrementInputBoxIndex();
            });

            $("#padButtonNo").click(function () {
                $(arrayInputBox[selectedInputBoxIndex]).text("Non effettuabile");
                incrementInputBoxIndex();
            });

            $("#padButtonStop").click(function () {
                $(arrayInputBox[selectedInputBoxIndex]).text("Sospeso");
                incrementInputBoxIndex();
            });

            $("#padButtonSkip").click(function () {
                $(arrayInputBox[selectedInputBoxIndex]).text("Saltato");
                incrementInputBoxIndex();
            });

            // Seleziona il primo label della tabella
            if (countInputBox > 0)
                $(arrayInputBox[0]).trigger("click", true);
        });
    </script>
</asp:Content>
