﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Multitenants;
using MES.Core;
using MES.Helper;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;

namespace MainCloud.HMI.MaintenancePlan
{
    public partial class MaintenancePlanExecutionList : BasePage
    {
        private object jobId, objectType, objectId, operatorCode, operatorUserName, mpId, mpDescription, mpoId;
        private TransactionService transService = new TransactionService();
        private List<MaintenancePlanDetail> mpd;

        public string ApplicationId
        {
            get
            {
                try
                {
                    return (string)Session.SessionMultitenant()[MES.Core.MesConstants.APPLICATION_ID];
                }
                catch (Exception) { }
                return null;
            }
        }

        protected string operatorId
        {
            get
            {
                return Request.QueryString["operator"];
            }
        }

        protected string supplierId
        {
            get
            {
                return Request.QueryString["supplier"];
            }
        }

        protected string assetId
        {
            get
            {
                return Request.QueryString["asset"];
            }
        }

        protected string moldId
        {
            get
            {
                return Request.QueryString["mold"];
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            LoadTitle();
        }

        private void LoadTitle()
        {
            try
            {
                string zoneId = Session.SessionMultitenant()[MesConstants.DEPARTMENT_ZONE].ToString();
                ZoneRepository zoneRep = new ZoneRepository();
                AssetRepository assetRep = new AssetRepository();
                TransactionService TService = new TransactionService();
                Page.Title = string.Format("{0} - {1} - Intervento Manutenzione - Operatore: {2}", zoneRep.GetDescription(zoneId), assetRep.GetDescription(assetId), operatorId);
            }
            catch (Exception) { }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (assetId != null || moldId != null)
            {
                loadData();

                lblOpCode.Text = (string)operatorCode;
                if (operatorId != null)
                {
                    lblExecutor.Text = "Operatore";
                    lblOpUserName.Text = (string)operatorUserName;
                }
                else if (supplierId != null)
                {
                    lblExecutor.Text = "Fornitore";
                    lblOpUserName.Text = supplierId;
                }
                lblObjectType.Text = "Cod. " + (string)objectType;
                lblObjectId.Text = (string)objectId;
                lblmpDescription.Text = (string)mpDescription;

                if (lblOpCode.Text == "") lblOpCode.Text = "---";
                if (lblOpUserName.Text == "") lblOpUserName.Text = "---";
                if (lblObjectId.Text == "") lblObjectId.Text = "---";
                if (lblmpDescription.Text == "") lblmpDescription.Text = "---";
            }
        }

        protected void loadData()
        {
            if (assetId != null)
            {
                Transaction lastTrans = transService.GetLastTransactionOpen(assetId);
                if (!string.IsNullOrEmpty(lastTrans?.JobId))
                {
                    // Get Job
                    jobId = lastTrans?.JobId;
                }
            }
            else
            {
                if (moldId != null)
                {
                    TransactionMoldService transMoldService = new TransactionMoldService();
                    TransactionMold lastTrans = transMoldService.GetLastTransactionOpen(assetId);
                    if (!string.IsNullOrEmpty(lastTrans?.JobId))
                    {
                        // Get Job
                        jobId = lastTrans?.JobId;
                    }
                }
            }

            // Get Operator or Supplier
            if (operatorId != null)
            {
                OperatorRepository opRep = new OperatorRepository();
                Operator op = opRep.FindByID(operatorId);
                if (op != null)
                {
                    operatorCode = op.Code;
                    operatorUserName = op.UserName;
                }
            }

            // Get Maintenance Plan
            MaintenanceRepository mRep = new MaintenanceRepository();
            Maintenance m = (assetId != null) ? mRep.FindByAssetId((string)assetId) : mRep.FindByMoldId((string)moldId); // Asset or Mold

            if (m != null && m.MaintenancePlanId != null)
            {
                objectType = m.ObjectType;
                objectId = m.ObjectId;

                MaintenancePlanOperatorRepository mpoRep = new MaintenancePlanOperatorRepository();
                MaintenancePlanOperator mpo = mpoRep.GetByOperatorMaintenanceId(operatorId, m.Id);
                if (mpo != null) mpoId = mpo.Id;

                MaintenancePlanRepository mpRep = new MaintenancePlanRepository();
                MES.Models.MaintenancePlan mp = mpRep.FindByID(m.MaintenancePlanId);

                if (mp != null)
                {
                    // Get Maintenance Plan Id
                    mpId = mp.Id;

                    // Get Maintenance Plan Description
                    mpDescription = mp.Description;

                    // Get Maintenance Plan Definitions
                    MaintenancePlanDetailRepository mpdRep = new MaintenancePlanDetailRepository();
                    mpd = mpdRep.GetAllByMaintenancePlanId(Convert.ToInt32(mpId));
                }
            }
        }

        protected void mpdTable_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            (sender as RadGrid).DataSource = mpd;
        }


        protected void btnSaveData_Click(object sender, EventArgs e)
        {
            dlgConfirmDialog.OpenDialog();
        }

        protected void goBack()
        {
            Response.Redirect(@"..\Taglio.aspx");
        }

        protected void btnCloseData_Click(object sender, EventArgs e)
        {
            goBack();
        }

        protected void saveData()
        {
            string sep = "|&|",
                   measuredValues = Request.Form[valuesList.UniqueID],
                   extraDescriptions = Request.Form[extraDescriptionList.UniqueID],
                   extraItemCodes = Request.Form[extraItemCodeList.UniqueID],
                   extraToolCodes = Request.Form[extraToolCodeList.UniqueID],
                   extraNotes = Request.Form[extraNotesList.UniqueID];
            int extraValuesCount = Convert.ToInt32(Request.Form[extraCount.UniqueID]);

            decimal qtyProduced = 0;
            TimeSpan productionTime = new TimeSpan();
            if (jobId != null)
            {
                qtyProduced = transService.GetQtyProducedPerJob((string)jobId);

                TransactionRepository TRep = new TransactionRepository();
                JobProductionWasteRepository jpwRep = new JobProductionWasteRepository();
                List<Transaction> tList = new List<Transaction>();
                tList = TRep.ReadAll(x => x.MachineId == assetId && x.JobId == (string)jobId).ToList();

                productionTime = TimeSpan.FromSeconds((double)tList.Where(x => x.Cause?.Code == ((int)MesEnum.Cause.Production).ToString()).Sum(x => x.Duration));
            }

            //if (measuredValues.Contains(sep))
            //{
                List<string> mvArr = new List<string>(measuredValues.Split(new[] { sep }, StringSplitOptions.None)),
                             eDArr = new List<string>(extraDescriptions.Split(new[] { sep }, StringSplitOptions.None)),
                             eICArr = new List<string>(extraItemCodes.Split(new[] { sep }, StringSplitOptions.None)),
                             eTCArr = new List<string>(extraToolCodes.Split(new[] { sep }, StringSplitOptions.None)),
                             eNArr = new List<string>(extraNotes.Split(new[] { sep }, StringSplitOptions.None));

                // Add missing items if the values were null
                for (int c = eICArr.Count; c < extraValuesCount; c++) { eICArr.Add(""); }
                for (int c = eTCArr.Count; c < extraValuesCount; c++) { eTCArr.Add(""); }
                for (int c = eNArr.Count; c < extraValuesCount; c++) { eNArr.Add(""); }

                if (mvArr.Contains(""))
                {
                    dlgErrorDialog.OpenDialog();
                }
                else
                {
                    MaintenancePlanDetailExecutedRepository mpdeRep = new MaintenancePlanDetailExecutedRepository();
                    int planID = 0; // Identificativo piano di manutenzione
                    int mPlanId = mpd[0].MaintenancePlanId;
                    for (int i = 0; i < mvArr.Count; i++)
                    {
                        MaintenancePlanDetailExecuted mpde = new MaintenancePlanDetailExecuted();
                        mpde.MaintenancePlanId = mPlanId;
                        mpde.ObjectId = (string)objectId;
                        mpde.ObjectType = (string)objectType;

                        if (i < mvArr.Count - extraValuesCount)
                        {
                            mpde.MaintenancePlanDetailId = mpd[i].Id;
                            mpde.Description = mpd[i].Description;
                            mpde.ItemCode = mpd[i].ItemCode;
                            mpde.ToolCode = mpd[i].ToolCode;
                            mpde.DetailNotes = mpd[i].DetailNotes;
                            mpde.extraMaintenance = false;
                        }
                        else
                        {
                            int newi = i - (mvArr.Count - extraValuesCount);
                            mpde.Description = eDArr[newi];
                            mpde.ItemCode = eICArr[newi];
                            mpde.ToolCode = eTCArr[newi];
                            mpde.DetailNotes = eNArr[newi];
                            mpde.extraMaintenance = true;
                        }

                        mpde.Result = mvArr[i];
                        mpde.ApplicationId = ApplicationId;
                        mpde.ExecutionDate = DateTime.Now;
                        mpde.QtyProduced = qtyProduced;
                        mpde.ProductionTime = productionTime.Ticks;
                        mpde.MaintenancePlanOperatorId = (int?)mpoId;

                        mpdeRep.Insert(mpde);
                        mpdeRep.SaveChanges();

                        // Aggiungo il Client Id
                        MaintenancePlanDetailExecuted mpdeN = mpdeRep.FindByID(mpde.Id);
                        mpdeN.ClientId = mpde.Id;
                        if (i == 0) planID = mpde.Id;
                        mpdeN.PlanId = planID;
                        mpdeRep.Update(mpdeN);
                        mpdeRep.SaveChanges();
                    }

                    // Aggiorno MaintenancePlanOperator ExecutedDate
                    if (mpoId != null)
                    {
                        MaintenancePlanOperatorRepository mpoRep = new MaintenancePlanOperatorRepository();
                        MaintenancePlanOperator mpo = mpoRep.FindByID(mpoId);
                        mpo.ExecutedDate = DateTime.Now;
                        mpo.Executed = true;
                        mpoRep.Update(mpo);
                        mpoRep.SaveChanges();
                    }

                    goBack();
                }
            //}
            //else
            //{
            //    dlgErrorDialog.OpenDialog();
            //}
        }

        protected void dlgConfirm_Si_Click(object sender, EventArgs e)
        {
            saveData();
            dlgConfirmDialog.CloseDialog();
        }

        protected void dlgConfirm_No_Click(object sender, EventArgs e)
        {
            dlgConfirmDialog.CloseDialog();
        }

        protected void dlgErrorOk_Click(object sender, EventArgs e)
        {
            dlgErrorDialog.CloseDialog();
        }
    }
}