/*cerco partprogram per articolo*/
 if EXISTS(
	 select *
	 from PartPrograms
	 where ArticleId = @ArticleId and ISNULL(PartProgramCode, '') <> '')
 		 begin
			 select top 1 *
			 from PartPrograms
			 where ArticleId = @ArticleId and ISNULL(PartProgramCode, '') <> ''
		 end
 else
/*cerco partprogram per classe articolo*/
 if EXISTS(
	select *
	from PartPrograms
	where ArticleType = @ArticleType and ISNULL(PartProgramCode, '') <> '')
		begin
			select top 1 *
			from PartPrograms
			where ArticleType = @ArticleType and ISNULL(ArticleType, '') <> '' and ISNULL(PartProgramCode, '') <> ''
		end
else
/*cerco partprogram per macchina*/
if EXISTS(
	select *
	from PartPrograms
	where AssetId = @AssetId and ISNULL(PartProgramCode, '') <> '')
		begin
			select top 1 *
			from PartPrograms
			where AssetId = @AssetId and ISNULL(PartProgramCode, '') <> ''
		end
else
/*cerco partprogram per assetgroup*/
select top 1 *
from PartPrograms
where AssetGroupId = @AssetGroupId and ISNULL(PartProgramCode, '') <> ''