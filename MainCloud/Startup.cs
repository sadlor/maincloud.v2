﻿using AssetManagement.Models;
using EnergyModule.Models;
using EverynetModule.Models;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using Microsoft.Owin;
using MoldID.Models;
using Owin;
using SenecaModule.Models;
using SOLModule.Models;

[assembly: OwinStartupAttribute(typeof(MainCloudFramework.Startup))]
namespace MainCloudFramework
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);

            //inizializzazione plugin
            AssetManagementDbContext.Create().Database.Initialize(true);
            //EnergyDbContext.Create().Database.Initialize(true);
            //WeldDbContext.Create().Database.Initialize(true);
            MesDbContext.Create().Database.Initialize(true);
            //EverynetDbContext.Create().Database.Initialize(true);
            //MoldDbContext.Create().Database.Initialize(true);

            //SenecaDbContext.Create().Database.Initialize(true);       
            
            //ApplicationSettingsHelper.StartAllSyncronizationBackgroundService();
        }
    }
}