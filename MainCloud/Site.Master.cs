﻿using MainCloudFramework.Models;
using MainCloudFramework.Web;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloudFramework
{
    public partial class SiteMaster : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataBind();
            }
        }
    }

}