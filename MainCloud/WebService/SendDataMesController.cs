﻿using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.RequestDBContext;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace MainCloud.WebService
{
    public class SendDataMesController : ApiController
    {
        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        [HttpPost]
        [AcceptVerbs("POST")]
        public bool Post([FromBody]Dictionary<string, object> value)
        {
            if (!value.ContainsKey(ApplicationSettingsKey.ApplicationId.ToString()) || value[ApplicationSettingsKey.ApplicationId.ToString()] == null)
            {
                //return null;
                return false;
            }

            string applicationId = value[ApplicationSettingsKey.ApplicationId.ToString()] as string;
            string authInfo = "main_api_rest_user" + ":" + "main_api_rest_password";

            ApplicationRepository appService = new ApplicationRepository();
            var applicationInstalled = appService.FindByID(applicationId);

            authInfo = ApplicationSettingsHelper.GetConfiguration(ApplicationSettingsKey.BasicAuthUser, applicationId)
                + ":"
                + ApplicationSettingsHelper.GetConfiguration(ApplicationSettingsKey.BasicAuthPassword, applicationId);

            string authorization = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            if (Request.Headers.Authorization.Parameter != authorization)
            {
                //return null;
                return false;
            }

            IEnumerable<BaseTransactionLog> dataSource = JsonConvert.DeserializeObject<IEnumerable<BaseTransactionLog>>(value["TransactionLog"].ToString());
            MES.HMI.API.TransactionAPI tAPI = new MES.HMI.API.TransactionAPI();
            tAPI.UpdateTransactionFanuc(dataSource);
            return true;
        }

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}