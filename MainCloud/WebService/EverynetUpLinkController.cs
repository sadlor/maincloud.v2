﻿using EverynetModule.Models;
using EverynetModule.Repositories;
using MES.HMI.API;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MainCloud.WebService
{
    public class EverynetUpLinkController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("EverynetLogFile");

        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        public HttpStatusCode Post([FromBody]object value)
        {
            try
            {
                log.Info("Messaggio API: " + value.ToString());
                UpLinkMessage receivedData = JsonConvert.DeserializeObject<UpLinkMessage>(value.ToString());
                log.Debug("Messaggio deserializzato con successo");
                TMoldMessage message = new TMoldMessage();
                try
                {
                    message.Application = receivedData.meta.application;
                    message.Device = receivedData.meta.device;
                    message.Device_addr = receivedData.meta.device_addr;
                    message.Gateway = receivedData.meta.gateway;
                    message.Network = receivedData.meta.network;
                    message.Packet_id = receivedData.meta.packet_id;
                    message.Time = GetMessageDate(receivedData.meta.time);
                    message.Packet_hash = receivedData.meta.packet_hash;
                    message.Packet_id = receivedData.meta.packet_id;
                    message.Rx_time = GetMessageDate(receivedData.@params.rx_time);

                    //deserializzo payload da messaggio
                    try
                    {
                        byte[] payload = Convert.FromBase64String(receivedData.@params.payload);
                        TMoldPayload deserializedPayload = UnPack(BitConverter.ToString(payload));
                        log.Debug("Messaggio decriptato con successo");
                        log.Debug("Messaggio decriptato - " + JsonConvert.SerializeObject(deserializedPayload));

                        message.Counter = deserializedPayload.Counter;
                        message.Temperature = deserializedPayload.Temperature;
                        message.Battery = deserializedPayload.Battery;
                        message.Reset = deserializedPayload.Reset;
                        message.ClickButton = deserializedPayload.ClickButton;
                        message.LowBattery = deserializedPayload.LowBattery;
                    }
                    catch (Exception ex)
                    {
                        log.Error("Errore decriptazione - " + ex.Message, ex);
                        return HttpStatusCode.InternalServerError;
                    }

                    try
                    {
                        TMoldMessageRepository repo = new TMoldMessageRepository();
                        repo.Insert(message);
                        repo.SaveChanges();

                        try
                        {
                            TransactionAPI tAPI = new TransactionAPI();
                            tAPI.UpdateTransactionsTMold(message);
                        }
                        catch (Exception ex)
                        {
                            log.Error("Errore in save transaction - " + ex.Message);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Errore save db - " + ex.Message);
                        return HttpStatusCode.InternalServerError;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message, ex);
                    return HttpStatusCode.InternalServerError;
                }

                return HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Deserializzazione fallita - " + ex.Message);
                return HttpStatusCode.InternalServerError;
            }
        }

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}

        private DateTime GetMessageDate(double millis)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(millis).ToLocalTime();
            return dtDateTime;
        }

        public TMoldPayload UnPack(string hexString)
        {
            try
            {
                TMoldPayload payload = new TMoldPayload();
                string[] hex = hexString.Split('-');
                string value = "";
                for (int i = 0; i < 4; i++)
                {
                    value += hex[i];
                }
                payload.Counter = int.Parse(value, System.Globalization.NumberStyles.HexNumber);
                value = "";
                for (int i = 4; i < 6; i++)
                {
                    value += hex[i];
                }
                payload.Temperature = (int.Parse(value, System.Globalization.NumberStyles.HexNumber) / 100) - (decimal)273.15;
                value = "";
                for (int i = 6; i < 8; i++)
                {
                    value += hex[i];
                }
                payload.Battery = int.Parse(value, System.Globalization.NumberStyles.HexNumber);
                value = "";
                for (int i = 8; i <= 9; i++)
                {
                    value += hex[i];
                }
                string binaryFlag = Convert.ToString(Convert.ToInt32(value, 16), 2).PadLeft(16, '0');
                payload.Reset = binaryFlag[13] == '1';
                payload.Event = binaryFlag[13] == '1' ? EverynetModule.Core.Event.Reset.ToString() : null;
                payload.ClickButton = binaryFlag[14] == '1';
                payload.Event = binaryFlag[14] == '1' ? EverynetModule.Core.Event.ClickButton.ToString() : null;
                payload.LowBattery = binaryFlag[15] == '1';

                return payload;
            }
            catch (Exception ex)
            {
                log.Error("Errore in unpack - " + ex.Message);
            }
            return null;
        }
    }
}