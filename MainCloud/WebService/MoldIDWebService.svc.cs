﻿using MoldID.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace MainCloud.WebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MoldIDWebService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MoldIDWebService.svc or MoldIDWebService.svc.cs at the Solution Explorer and start debugging.
    [ServiceContract]
    public class MoldIDWebService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("RollingLogFile");

        [OperationContract]
        [WebInvoke(Method = "POST",
              ResponseFormat = WebMessageFormat.Json,
              RequestFormat = WebMessageFormat.Json,
              BodyStyle = WebMessageBodyStyle.WrappedRequest,
              UriTemplate = "SendData")]
        public HttpStatusCode SendToCloud()
        {
            try
            {
                try
                {
                    MoldID.Device.XmlPayload.XmlPayload payload = null;
                    System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(MoldID.Device.XmlPayload.XmlPayload));
                    //using (TextReader reader = new StringReader(OperationContext.Current.RequestContext.RequestMessage.ToString().Replace("&lt;", "<").Replace("&gt;", ">").Replace("<root type=\"string\">", "").Replace("</root></root>", "</root>"))) x debug
                    using (TextReader reader = new StringReader(OperationContext.Current.RequestContext.RequestMessage.ToString()))
                    {
                        payload = (MoldID.Device.XmlPayload.XmlPayload)serializer.Deserialize(reader);
                    }
                    MoldData item = new MoldData();
                    item.CurrentShots = payload.RfidPorts[0].Item.CurrentShots;
                    item.CycleEnd = payload.Header.CycleEnd;
                    item.CycleStart = payload.Header.CycleStart;
                    item.ErrorState = payload.Header.ErrorState;
                    item.FormName = payload.RfidPorts[0].Item.FormName;
                    item.LastChange = payload.RfidPorts[0].Item.LastChange;
                    item.LastDeviceName = payload.RfidPorts[0].Item.LastDeviceName;
                    item.LastMaintenance = payload.RfidPorts[0].Item.LastMaintenance;
                    item.MaxCycleTime = payload.RfidPorts[0].Item.MaxCycleTime;
                    item.Maximum = payload.RfidPorts[0].Item.Maximum;
                    item.MinCycleTime = payload.RfidPorts[0].Item.MinCycleTime;
                    item.NextMaintenance = payload.RfidPorts[0].Item.NextMaintenance;
                    item.Port = payload.RfidPorts[0].Item.Port;
                    item.SmartlightStatus = payload.RfidPorts[0].Item.SmartlightStatus;
                    item.Status = payload.RfidPorts[0].Item.Status;
                    item.StorageLocation = payload.RfidPorts[0].Item.StorageLocation;
                    item.TagId = payload.RfidPorts[0].Item.TagId;
                    item.TotalCycleTime = payload.RfidPorts[0].Item.TotalCycleTime;
                    item.TotalShots = payload.RfidPorts[0].Item.TotalShots;
                    item.Warning1 = payload.RfidPorts[0].Item.Warning1;
                    item.Warning2 = payload.RfidPorts[0].Item.Warning2;
                    try
                    {
                        using (MoldDbContext db = new MoldDbContext())
                        {
                            db.MoldDatas.Add(item);
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error in save to db - error: " + ex.Message);
                    }

                    //log.Info("xmlPayload: " + JsonConvert.SerializeObject(payload)); x debug
                }
                catch (Exception ex)
                {
                    log.Error("Impossibile convertire in xmlPayload - error: " + ex.Message);
                }
                string body = JsonConvert.SerializeObject(OperationContext.Current.RequestContext.RequestMessage.ToString());
                //var json = OperationContext.Current.RequestContext.RequestMessage.GetBody<MoldID.Device.XmlPayload.XmlPayload>();
                log.Info("Body message: " + body);
                File.WriteAllText(@"C:\jsonData\" + DateTime.Now.ToFileTime() + ".json", body);
                return HttpStatusCode.OK;
            }
            catch (Exception)
            {
                log.Error("Error in save on file");
                return HttpStatusCode.BadRequest;
            }
        }
    }
}