﻿using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MainCloud.WebService.TMold
{
    [Route("api/TMold/Transaction")]
    public class TransactionController : ApiController
    {
        public IEnumerable<object> Get()
        {
            TransactionMoldRepository repos = new TransactionMoldRepository();
            IQueryable<TransactionMold> returnList = null;
            IEnumerable<TransactionMold> dataSource = null;

            var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();

            string moldId = allUrlKeyValues.LastOrDefault(x => x.Key == "moldId").Value;
            DateTime? startDate = null;//Convert.ToDateTime(allUrlKeyValues.LastOrDefault(x => x.Key == "startDate").Value);
            DateTime? endDate = null;// Convert.ToDateTime(allUrlKeyValues.LastOrDefault(x => x.Key == "endDate").Value);

            if (!string.IsNullOrEmpty(moldId))
            {
                returnList = repos.ReadAll(x => x.MoldId == moldId).OrderBy(x => x.Start);
            }

            if (returnList != null && returnList.Count() > 0)
            {
                if (startDate.HasValue)
                {
                    if (endDate.HasValue)
                    {
                        dataSource = returnList.Where(x => x.Start >= startDate && x.Start <= endDate);
                    }
                    else
                    {
                        dataSource = returnList.Where(x => x.Start >= startDate);
                    }
                }
                else
                {
                    dataSource = returnList;
                }
            }
            return dataSource.Select(x => new { x.Start, x.End, x.Duration, x.PartialCounting, x.ProgressiveCounter }).ToArray();
        }

        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}