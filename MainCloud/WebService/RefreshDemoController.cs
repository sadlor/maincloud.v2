﻿using MES.Models;
using MES.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace MainCloud.WebService
{
    public class RefreshDemoController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("RollingLogFile");

        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        [HttpPost]
        [AcceptVerbs("POST")]
        public bool Post([FromBody]Dictionary<string, object> value)
        {
            ///Ricevo i dati (articoli, commesse, ordini, jobs, transazioni) ogni ora
            try
            {
                var articleList = JsonConvert.DeserializeObject<List<Article>>(value[ObjectType.Article.ToString()].ToString());
                var coList = JsonConvert.DeserializeObject<List<CustomerOrder>>(value[ObjectType.CustomerOrder.ToString()].ToString());
                var orderList = JsonConvert.DeserializeObject<List<Order>>(value[ObjectType.Order.ToString()].ToString());
                var jobList = JsonConvert.DeserializeObject<List<Job>>(value[ObjectType.Job.ToString()].ToString());
                var tList = JsonConvert.DeserializeObject<List<Transaction>>(value[ObjectType.Transaction.ToString()].ToString());
                var tOperList = JsonConvert.DeserializeObject<List<TransactionOperator>>(value[ObjectType.TransactionOperator.ToString()].ToString());

                try
                {
                    ///Aggiungo gli articoli
                    ArticleRepository artRepos = new ArticleRepository();
                    string query;
                    Type type = typeof(Article); //GetType();
                    PropertyInfo[] transProp = type.GetProperties(
                            BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Where(p => !p.GetGetMethod().IsVirtual).ToArray();
                    articleList.ForEach(x =>
                    {
                        string props = string.Empty,
                               values = string.Empty;
                        foreach (PropertyInfo prop in transProp)
                        {
                            props += string.Format("[{0}],", prop.Name);
                            if (prop.GetValue(x) == null)
                            {
                                values += string.Format("null,");
                            }
                            else
                            {
                                if (prop.PropertyType.Name == "String")
                                {
                                    values += string.Format("'{0}',", prop.GetValue(x));
                                }
                                else
                                {
                                    if (prop.PropertyType.FullName.Contains("System.DateTime"))
                                    {
                                        values += string.Format("'{0}',", Convert.ToDateTime(prop.GetValue(x)).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                    }
                                    else
                                    {
                                        if (prop.PropertyType.Name == "Boolean")
                                        {
                                            values += string.Format("{0},", Convert.ToBoolean(prop.GetValue(x)) ? 1 : 0);
                                        }
                                        else
                                        {
                                            if (prop.PropertyType.Name == "Decimal")
                                            {
                                                values += string.Format("{0},", prop.GetValue(x).ToString().Replace(",", "."));
                                            }
                                            else
                                            {
                                                values += string.Format("{0},", prop.GetValue(x));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        query = string.Format("SET IDENTITY_INSERT Articles ON INSERT INTO[dbo].[Articles] ({0}) VALUES ({1}) SET IDENTITY_INSERT Articles OFF", props.Remove(props.Length - 1), values.Remove(values.Length - 1));
                        artRepos.ExecuteSqlCommand(query);
                    });
                    //artRepos.InsertAll(articleList, true);
                }
                catch (Exception ex)
                {
                    log.Error("Errore in articoli - " + ex.Message + ex.InnerException + ex.InnerException?.InnerException);
                    return false;
                }

                try
                {
                    ///Aggiungo le commesse
                    CustomerOrderRepository coRepos = new CustomerOrderRepository();
                    string query;
                    Type type = typeof(CustomerOrder); //GetType();
                    PropertyInfo[] transProp = type.GetProperties(
                            BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Where(p => !p.GetGetMethod().IsVirtual).ToArray();
                    coList.ForEach(x =>
                    {
                        string props = string.Empty,
                               values = string.Empty;
                        foreach (PropertyInfo prop in transProp)
                        {
                            props += string.Format("[{0}],", prop.Name);
                            if (prop.GetValue(x) == null)
                            {
                                values += string.Format("null,");
                            }
                            else
                            {
                                if (prop.PropertyType.Name == "String")
                                {
                                    values += string.Format("'{0}',", prop.GetValue(x));
                                }
                                else
                                {
                                    if (prop.PropertyType.FullName.Contains("System.DateTime"))
                                    {
                                        values += string.Format("'{0}',", Convert.ToDateTime(prop.GetValue(x)).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                    }
                                    else
                                    {
                                        if (prop.PropertyType.Name == "Boolean")
                                        {
                                            values += string.Format("{0},", Convert.ToBoolean(prop.GetValue(x)) ? 1 : 0);
                                        }
                                        else
                                        {
                                            if (prop.PropertyType.Name == "Decimal")
                                            {
                                                values += string.Format("{0},", prop.GetValue(x).ToString().Replace(",", "."));
                                            }
                                            else
                                            {
                                                values += string.Format("{0},", prop.GetValue(x));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        query = string.Format("SET IDENTITY_INSERT CustomerOrders ON INSERT INTO[dbo].[CustomerOrders] ({0}) VALUES ({1}) SET IDENTITY_INSERT CustomerOrders OFF", props.Remove(props.Length - 1), values.Remove(values.Length - 1));
                        coRepos.ExecuteSqlCommand(query);
                    });
                    //coRepos.InsertAll(coList, true);
                }
                catch (Exception ex)
                {
                    log.Error("Errore in customerOrder - " + ex.Message + ex.InnerException + ex.InnerException?.InnerException);
                    return false;
                }

                try
                {
                    ///Aggiungo gli ordini
                    OrderRepository orderRepos = new OrderRepository();
                    string query;
                    Type type = typeof(Order); //GetType();
                    PropertyInfo[] transProp = type.GetProperties(
                            BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Where(p => !p.GetGetMethod().IsVirtual).ToArray();
                    orderList.ForEach(x =>
                    {
                        string props = string.Empty,
                               values = string.Empty;
                        foreach (PropertyInfo prop in transProp)
                        {
                            props += string.Format("[{0}],", prop.Name);
                            if (prop.GetValue(x) == null)
                            {
                                values += string.Format("null,");
                            }
                            else
                            {
                                if (prop.PropertyType.Name == "String")
                                {
                                    values += string.Format("'{0}',", prop.GetValue(x));
                                }
                                else
                                {
                                    if (prop.PropertyType.FullName.Contains("System.DateTime"))
                                    {
                                        values += string.Format("'{0}',", Convert.ToDateTime(prop.GetValue(x)).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                    }
                                    else
                                    {
                                        if (prop.PropertyType.Name == "Boolean")
                                        {
                                            values += string.Format("{0},", Convert.ToBoolean(prop.GetValue(x)) ? 1 : 0);
                                        }
                                        else
                                        {
                                            if (prop.PropertyType.Name == "Decimal")
                                            {
                                                values += string.Format("{0},", prop.GetValue(x).ToString().Replace(",", "."));
                                            }
                                            else
                                            {
                                                values += string.Format("{0},", prop.GetValue(x));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        query = string.Format("SET IDENTITY_INSERT Orders ON INSERT INTO[dbo].[Orders] ({0}) VALUES ({1}) SET IDENTITY_INSERT Orders OFF", props.Remove(props.Length - 1), values.Remove(values.Length - 1));
                        orderRepos.ExecuteSqlCommand(query);
                    });
                    //orderRepos.InsertAll(orderList, true);
                }
                catch (Exception ex)
                {
                    log.Error("Errore in order - " + ex.Message + ex.InnerException + ex.InnerException?.InnerException);
                    return false;
                }

                try
                {
                    ///Aggiungo i job
                    JobRepository jobRepos = new JobRepository();
                    jobRepos.InsertAll(jobList);
                    jobRepos.SaveChanges();
                }
                catch (Exception ex)
                {
                    log.Error("Errore in job - " + ex.Message + ex.InnerException + ex.InnerException?.InnerException);
                    return false;
                }

                try
                {
                    ///Cancello tutte le transazioni aggiunte dall'ultimo aggiornamento (tutte + l'ultima ricevuta -> macchina/dateStart)
                    if (tList.Count > 0)
                    {
                        var first = tList.GroupBy(x => x.MachineId, (key, g) => new { MachineId = key, Start = g.Min(x => x.Start) }).ToList();
                        TransactionRepository tRepos = new TransactionRepository();
                        string whereClause = "WHERE ";
                        first.ForEach(x =>
                        {
                            whereClause += "(MachineId = '" + x.MachineId + "' and Start >= '" + x.Start.ToString("yyyy-MM-dd HH:mm:ss.fff") + "') or";
                        });
                        string query = "DELETE FROM dbo.Transactions " + whereClause.Remove(whereClause.Count() - 2);
                        tRepos.ExecuteSqlCommand(query);
                        //query = "INSERT INTO [dbo].[Transactions]" +
                        //    "([Id],[CauseId],[IdArticle],[JobId],[MachineId],[OperatorId],[EquipmentId],[MoldId],[Status],[Start],[End],[Duration]" +
                        //    ",[CounterStart],[CounterEnd],[ProgressiveCounter],[PartialCounting],[BlankShot],[CavityMoldNum],[StandardRate]" +
                        //    ",[QtyProduced],[QtyResidual],[QtyProductionWaste],[QtyProductionWasteReusable],[QtyRawMaterial],[QtyProductionWasteRawMaterial],[QtyOK]" +
                        //    ",[PartialCountingUM],[QtyProducedUM],[ArticleBatchId],[Open],[LastUpdate],[Verified],[Exported],[IsOrderClosed],[ClientId],[ApplicationId]) VALUES(";

                        Type type = typeof(Transaction); //GetType();
                        PropertyInfo[] transProp = type.GetProperties(
                                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Where(p => !p.GetGetMethod().IsVirtual).ToArray();
                        tList.ForEach(x =>
                        {
                            string props = string.Empty,
                                   values = string.Empty;
                            foreach (PropertyInfo prop in transProp)
                            {
                                props += string.Format("[{0}],", prop.Name);
                                if (prop.Name == "MoldId")
                                {
                                    try
                                    {
                                        switch (x.MachineId)
                                        {
                                            case "a19340ba-1fb2-4e7a-b120-f9a0cafbf68f": //macchina 75
                                                values += string.Format("'{0}',", "E0080134530170AC"); //stampo 1
                                                break;
                                            case "b0a64bb2-6fb8-488d-8afd-1d3cd7cad62b": //macchina 76
                                                values += string.Format("'{0}',", "E00801345301A6AA"); //stampo 2
                                                break;
                                            case "d8b53fd5-2560-42ca-b3dc-b7e595968d0c": //macchina 77
                                                values += string.Format("'{0}',", "E008013453017AAC"); //stampo 3
                                                break;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        log.Error("Errore in transazioni(property) - " + ex.Message + ex.InnerException + ex.InnerException?.InnerException);
                                    }
                                }
                                else
                                {
                                    if (prop.GetValue(x) == null)
                                    {
                                        values += string.Format("null,");
                                    }
                                    else
                                    {
                                        if (prop.PropertyType.Name == "String")
                                        {
                                            values += string.Format("'{0}',", prop.GetValue(x));
                                        }
                                        else
                                        {
                                            if (prop.PropertyType.FullName.Contains("System.DateTime"))
                                            {
                                                values += string.Format("'{0}',", Convert.ToDateTime(prop.GetValue(x)).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                            }
                                            else
                                            {
                                                if (prop.PropertyType.Name == "Boolean")
                                                {
                                                    values += string.Format("{0},", Convert.ToBoolean(prop.GetValue(x)) ? 1 : 0);
                                                }
                                                else
                                                {
                                                    if (prop.PropertyType.Name == "Decimal")
                                                    {
                                                        values += string.Format("{0},", prop.GetValue(x).ToString().Replace(",", "."));
                                                    }
                                                    else
                                                    {
                                                        values += string.Format("{0},", prop.GetValue(x));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            query = string.Format("SET IDENTITY_INSERT Transactions ON INSERT INTO[dbo].[Transactions] ({0}) VALUES ({1}) SET IDENTITY_INSERT Transactions OFF", props.Remove(props.Length - 1), values.Remove(values.Length - 1));
                            tRepos.ExecuteSqlCommand(query);
                        });

                        string queryMold = "DELETE FROM dbo.TransactionMolds " + whereClause.Remove(whereClause.Count() - 2) +
                        " SET IDENTITY_INSERT TransactionMolds ON " +
                        "INSERT INTO [dbo].[TransactionMolds]([Id],[CauseId],[MoldId],[MachineId],[JobId],[OperatorId],[Status],[Start],[End],[Duration],[ProgressiveCounter],[PartialCounting],[BlankShot],[CavityMoldNum],[CounterStart],[CounterEnd],[QtyProduced],[Open],[LastUpdate],[Verified],[ApplicationId],[Exported]) " +
                        "SELECT [Id],[CauseId],[MoldId],[MachineId],[JobId],[OperatorId],[Status],[Start],[End],[Duration],[ProgressiveCounter],[PartialCounting],[BlankShot],[CavityMoldNum],[CounterStart],[CounterEnd],[QtyProduced],[Open],[LastUpdate],0,[ApplicationId],0 " +
                        "FROM [MES].[dbo].[Transactions] " +
                        whereClause.Remove(whereClause.Count() - 2) +
                        " SET IDENTITY_INSERT TransactionMolds OFF";
                        try
                        {
                            tRepos.ExecuteSqlCommand(queryMold);
                        }
                        catch (Exception ex)
                        {
                            log.Error("Errore in save transactionmold - " + ex.Message + ex.InnerException);
                        }
                        try
                        {
                            tRepos.ExecuteSqlCommand(
                                 "UPDATE AssetManagement.dbo.Molds " +
                                 "SET CavityMoldNum = Machine.CavityMoldNum, ProductionCadency = Machine.StandardRate " +
                                 "FROM ( " +
                                 "SELECT top(1) CavityMoldNum, StandardRate " +
                                 "FROM MES.dbo.Transactions " +
                                 "WHERE MachineId = 'a19340ba-1fb2-4e7a-b120-f9a0cafbf68f' and [Open] = 1 " +
	                             "order by [Start] desc) Machine " +
                                 "WHERE Id = 'E0080134530170AC'"); //Stampo 1
                            tRepos.ExecuteSqlCommand(
                                 "UPDATE AssetManagement.dbo.Molds " +
                                 "SET CavityMoldNum = Machine.CavityMoldNum, ProductionCadency = Machine.StandardRate " +
                                 "FROM ( " +
                                 "SELECT top(1) CavityMoldNum, StandardRate " +
                                 "FROM MES.dbo.Transactions " +
                                 "WHERE MachineId = 'b0a64bb2-6fb8-488d-8afd-1d3cd7cad62b' and [Open] = 1 " +
                                 "order by [Start] desc) Machine " +
                                 "WHERE Id = 'E00801345301A6AA'"); //Stampo 2
                            tRepos.ExecuteSqlCommand(
                                 "UPDATE AssetManagement.dbo.Molds " +
                                 "SET CavityMoldNum = Machine.CavityMoldNum, ProductionCadency = Machine.StandardRate " +
                                 "FROM ( " +
                                 "SELECT top(1) CavityMoldNum, StandardRate " +
                                 "FROM MES.dbo.Transactions " +
                                 "WHERE MachineId = 'd8b53fd5-2560-42ca-b3dc-b7e595968d0c' and [Open] = 1 " +
                                 "order by [Start] desc) Machine " +
                                 "WHERE Id = 'E008013453017AAC'"); //Stampo 3
                        }
                        catch (Exception ex)
                        {
                            log.Error("Errore in update mold - " + ex.Message + ex.InnerException);
                        }

                        //tRepos.InsertAll(tList, true);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Errore in transazioni - " + ex.Message + ex.InnerException + ex.InnerException?.InnerException);
                }

                try
                {
                    ///Cancello tutte le transazioni aggiunte dall'ultimo aggiornamento (tutte + l'ultima ricevuta -> macchina/dateStart)
                    if (tOperList.Count > 0)
                    {
                        var first = tOperList.GroupBy(x => x.MachineId, (key, g) => new { MachineId = key, Start = g.Min(x => x.DateStart) }).ToList();
                        TransactionOperatorRepository tOperRepos = new TransactionOperatorRepository();
                        string whereClause = "WHERE ";
                        first.ForEach(x =>
                        {
                            whereClause += "(MachineId = '" + x.MachineId + "' and DateStart >= '" + x.Start.ToString("yyyy-MM-dd HH:mm:ss.fff") + "') or";
                        });
                        string query = "DELETE FROM dbo.TransactionOperators " + whereClause.Remove(whereClause.Count() - 2);
                        tOperRepos.ExecuteSqlCommand(query);

                        Type type = typeof(TransactionOperator); //GetType();
                        PropertyInfo[] transProp = type.GetProperties(
                                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Where(p => !p.GetGetMethod().IsVirtual).ToArray();
                        tOperList.ForEach(x =>
                        {
                            string props = string.Empty,
                                   values = string.Empty;
                            foreach (PropertyInfo prop in transProp)
                            {
                                props += string.Format("[{0}],", prop.Name);
                                if (prop.GetValue(x) == null)
                                {
                                    values += string.Format("null,");
                                }
                                else
                                {
                                    if (prop.PropertyType.Name == "String")
                                    {
                                        values += string.Format("'{0}',", prop.GetValue(x));
                                    }
                                    else
                                    {
                                        if (prop.PropertyType.FullName.Contains("System.DateTime"))
                                        {
                                            values += string.Format("'{0}',", Convert.ToDateTime(prop.GetValue(x)).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                        }
                                        else
                                        {
                                            if (prop.PropertyType.Name == "Boolean")
                                            {
                                                values += string.Format("{0},", Convert.ToBoolean(prop.GetValue(x)) ? 1 : 0);
                                            }
                                            else
                                            {
                                                if (prop.PropertyType.Name == "Decimal")
                                                {
                                                    values += string.Format("{0},", prop.GetValue(x).ToString().Replace(",", "."));
                                                }
                                                else
                                                {
                                                    values += string.Format("{0},", prop.GetValue(x));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            query = string.Format("SET IDENTITY_INSERT TransactionOperators ON INSERT INTO[dbo].[TransactionOperators] ({0}) VALUES ({1}) SET IDENTITY_INSERT TransactionOperators OFF", props.Remove(props.Length - 1), values.Remove(values.Length - 1));
                            tOperRepos.ExecuteSqlCommand(query);
                        });
                        //tOperRepos.InsertAll(tOperList, true);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Errore in transazioni operatori - " + ex.Message + ex.InnerException + ex.InnerException?.InnerException);
                }

                return true;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.InnerException + ex.InnerException?.InnerException);
                return false;
            }
        }

        private enum ObjectType
        {
            Article,
            CustomerOrder,
            Order,
            Job,
            Transaction,
            TransactionOperator
        }

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}