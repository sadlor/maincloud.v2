﻿using MES.Models;
using MES.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MainCloud.WebService
{
    public class TMoldController : ApiController
    {
        // GET api/<controller>
        //public IHttpActionResult Get(string moldId)
        //{
        //    var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();

        //    //string moldId = allUrlKeyValues.LastOrDefault(x => x.Key == "moldId").Value;
        //    DateTime? startDate = Convert.ToDateTime(allUrlKeyValues.LastOrDefault(x => x.Key == "startDate").Value);
        //    DateTime? endDate = Convert.ToDateTime(allUrlKeyValues.LastOrDefault(x => x.Key == "endDate").Value);
        //    List<TransactionMold> dataSource = new List<TransactionMold>();

        //    if (!string.IsNullOrEmpty(moldId))
        //    {
        //        TransactionMoldRepository repos = new TransactionMoldRepository();
        //        IQueryable<TransactionMold> returnList = null;

        //        returnList = repos.ReadAll(x => x.MoldId == moldId);

        //        DateTime? end = endDate.Value;//.AddDays(1);

        //        if (returnList != null && returnList.Count() > 0)
        //        {
        //            if (startDate.HasValue)
        //            {
        //                if (end.HasValue)
        //                {
        //                    //dataSource = returnList.Where(x => x.Start >= DateStart && x.Start <= end && !x.Open).OrderByDescending(x => x.Start).ToList();
        //                    dataSource = returnList.Where(x => x.Start >= startDate && x.Start <= end).OrderByDescending(x => x.Start).ToList();
        //                }
        //                else
        //                {
        //                    //dataSource = returnList.Where(x => x.Start >= DateStart && !x.Open).OrderByDescending(x => x.Start).ToList();
        //                    dataSource = returnList.Where(x => x.Start >= startDate).OrderByDescending(x => x.Start).ToList();
        //                }
        //            }
        //            else
        //            {
        //                dataSource = returnList.ToList();
        //            }
        //        }
        //        else
        //        {
        //            if (startDate.HasValue)
        //            {
        //                if (end.HasValue)
        //                {
        //                    //dataSource = repos.ReadAll(x => x.Start >= DateStart && x.Start <= end && !x.Open).OrderByDescending(x => x.Start).ToList();
        //                    dataSource = repos.ReadAll(x => x.Start >= startDate && x.Start <= end).OrderByDescending(x => x.Start).ToList();
        //                }
        //                else
        //                {
        //                    //dataSource = repos.ReadAll(x => x.Start >= DateStart && !x.Open).OrderByDescending(x => x.Start).ToList();
        //                    dataSource = repos.ReadAll(x => x.Start >= startDate).OrderByDescending(x => x.Start).ToList();
        //                }
        //            }
        //        }
        //    }
        //    return Ok(dataSource.Select(x => new { x.MoldId, x.MachineId, x.Start, x.End, x.CounterStart, x.CounterEnd, x.PartialCounting, x.ProgressiveCounter }));
        //}

        public IHttpActionResult Get()
        {
            var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();

            string moldId = allUrlKeyValues.LastOrDefault(x => x.Key == "moldId").Value;
            DateTime? startDate = Convert.ToDateTime(allUrlKeyValues.LastOrDefault(x => x.Key == "startDate").Value);
            DateTime? endDate = Convert.ToDateTime(allUrlKeyValues.LastOrDefault(x => x.Key == "endDate").Value);
            List<TransactionMold> dataSource = new List<TransactionMold>();

            if (!string.IsNullOrEmpty(moldId))
            {
                TransactionMoldRepository repos = new TransactionMoldRepository();
                IQueryable<TransactionMold> returnList = null;

                returnList = repos.ReadAll(x => x.MoldId == moldId);

                DateTime? end = endDate.Value;//.AddDays(1);

                if (returnList != null && returnList.Count() > 0)
                {
                    if (startDate.HasValue)
                    {
                        if (end.HasValue)
                        {
                            //dataSource = returnList.Where(x => x.Start >= DateStart && x.Start <= end && !x.Open).OrderByDescending(x => x.Start).ToList();
                            dataSource = returnList.Where(x => x.Start >= startDate && x.Start <= end).OrderByDescending(x => x.Start).ToList();
                        }
                        else
                        {
                            //dataSource = returnList.Where(x => x.Start >= DateStart && !x.Open).OrderByDescending(x => x.Start).ToList();
                            dataSource = returnList.Where(x => x.Start >= startDate).OrderByDescending(x => x.Start).ToList();
                        }
                    }
                    else
                    {
                        dataSource = returnList.ToList();
                    }
                }
                else
                {
                    if (startDate.HasValue)
                    {
                        if (end.HasValue)
                        {
                            //dataSource = repos.ReadAll(x => x.Start >= DateStart && x.Start <= end && !x.Open).OrderByDescending(x => x.Start).ToList();
                            dataSource = repos.ReadAll(x => x.Start >= startDate && x.Start <= end).OrderByDescending(x => x.Start).ToList();
                        }
                        else
                        {
                            //dataSource = repos.ReadAll(x => x.Start >= DateStart && !x.Open).OrderByDescending(x => x.Start).ToList();
                            dataSource = repos.ReadAll(x => x.Start >= startDate).OrderByDescending(x => x.Start).ToList();
                        }
                    }
                }
            }
            return Ok(new { results = dataSource });
            //return JsonConvert.SerializeObject(dataSource);//Ok(dataSource.Select(x => new { x.MoldId, x.MachineId, x.Start, x.End, x.CounterStart, x.CounterEnd, x.PartialCounting, x.ProgressiveCounter }));
        }

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}