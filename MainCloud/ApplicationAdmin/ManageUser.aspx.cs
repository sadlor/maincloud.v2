﻿using MainCloudFramework.Controls;
using MainCloudFramework.Models;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MainCloudFramework;
using MainCloudFramework.Services;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Validation;
using MainCloudFramework.Web;

namespace MainCloud.Admin
{
    public partial class ManageUser : BaseAdminPage
    {
        protected ApplicationUser AppUser { get; set; }
        private ApplicationService applicationService = new ApplicationService();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string userId = Request["Id"] as string;
                AppUser = applicationService.FindUserById(userId);

                if (AppUser != null)
                {
                    if (!IsPostBack)
                    {
                        txtUserName.Text = AppUser.UserName;
                        txtEmail.Text = AppUser.Email;
                        List<ApplicationRole> appRoles;

                        if (!IsHostUser)
                        {
                            ApplicationRole appRoleOperator = applicationService.RolesInCurrentApplication().Where(r => r.Name == MCFConstants.ROLE_OPERATOR).First();
                            if (AppUser.Roles.Any(x => x.RoleId == appRoleOperator.Id)) {
                                appRoles = new List<ApplicationRole>() { appRoleOperator }; // Resetta la lista ruoli
                            }
                            else {
                                appRoles = applicationService.RolesInCurrentApplication(new List<string>() { MCFConstants.ROLE_OPERATOR });
                            }
                        }
                        else
                        {
                            appRoles = applicationService.RolesInCurrentApplication();
                        }

                        if (appRoles != null && appRoles.Count > 0)
                        {
                            lstRoles.DataSource = appRoles;
                            lstRoles.DataBind();

                            var userRole = applicationService.FindUserRoleCurrentApp(AppUser);
                            lstRoles.SelectedValue = userRole != null ? userRole.Id : null;

                            lstRoles.Visible = true;
                        }
                        else
                        {
                            lstRoles.Visible = false;
                        }
                    }
                }
                else
                {
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }
            }
            catch (DbEntityValidationException exdb)
            {
                AlertMessage.ShowError(GetValidationMessageException(exdb));
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }
    
        protected void btnSaveUser_Click(object sender, EventArgs e)
        {
            try
            {
                AppUser.UserName = txtUserName.Text;
                AppUser.Email = txtEmail.Text;

                applicationService.ChangeRoleAndUpdateUser(lstRoles.SelectedValue, AppUser);

                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            catch (DbEntityValidationException exdb)
            {
                AlertMessage.ShowError(GetValidationMessageException(exdb));
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }
    }
}