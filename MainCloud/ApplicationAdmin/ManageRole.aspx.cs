﻿using MainCloudFramework;
using MainCloudFramework.Models;
using MainCloudFramework.Services;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.ApplicationAdmin
{
    public partial class ManageRole : BaseAdminPage
    {
        protected ApplicationRole AppRole { get; set; }
        private ApplicationService applicationService = new ApplicationService();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string userId = Request["Id"] as string;
                AppRole = applicationService.FindRoleByIdInCurrentApplication(userId);

                if (!IsHostUser && AppRole.Name == MCFConstants.ROLE_OPERATOR)
                {
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }

                if (AppRole != null)
                {
                    if (!IsPostBack)
                    {
                        Title += " " + AppRole.Name;

                        chkAllowAddArea.Checked = AppRole.AllowAddArea;
                        chkAllowEditArea.Checked = AppRole.AllowEditArea;
                        chkAllowDeleteArea.Checked = AppRole.AllowDeleteArea;

                        chkAllowAddWidget.Checked = AppRole.AllowAddWidget;
                        chkAllowEditWidget.Checked = AppRole.AllowEditWidget;
                        chkAllowDeleteWidget.Checked = AppRole.AllowDeleteWidget;

                        chkAllowPublicArea.Checked = AppRole.AllowPublicArea;

                        //chkWidgetAdvanced.Checked = AppRole.ShowWidgetAdvanced;
                        //chkWidgetAggregator.Checked = AppRole.ShowWidgetAggregator;
                    }
                }
                else
                {
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                AppRole.AllowAddArea = chkAllowAddArea.Checked;
                AppRole.AllowEditArea = chkAllowEditArea.Checked;
                AppRole.AllowDeleteArea = chkAllowDeleteArea.Checked;

                AppRole.AllowAddWidget = chkAllowAddWidget.Checked;
                AppRole.AllowEditWidget = chkAllowEditWidget.Checked;
                AppRole.AllowDeleteWidget = chkAllowDeleteWidget.Checked;

                AppRole.AllowPublicArea = chkAllowPublicArea.Checked;

                //AppRole.ShowWidgetAdvanced = chkWidgetAdvanced.Checked;
                //AppRole.ShowWidgetAggregator = chkWidgetAggregator.Checked;

                applicationService.SaveRole(AppRole);
                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            catch (DbEntityValidationException exdb)
            {
                AlertMessage.ShowError(GetValidationMessageException(exdb));
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }
    }
}