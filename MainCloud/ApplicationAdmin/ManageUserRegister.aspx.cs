﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using MainCloudFramework.Models;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using MainCloudFramework.Web;
using MainCloudFramework.Core.Multitenants.Identity;
using MainCloudFramework;
using System.Data.Entity.Validation;
using MainCloudFramework.Services;

namespace MainCloud.Admin
{
    public partial class ManageUserRegister : BaseAdminPage
    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            try
            {
                ApplicationService applicationService = new ApplicationService();

                var user = new ApplicationUser()
                {
                    UserName = Email.Text,
                    Email = Email.Text,
                };

                IdentityResult result = applicationService.CreateUserCurrentApplication(user, Password.Text);

                if (result.Succeeded)
                {
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }
                else
                {
                    ErrorMessage.Text = result.Errors.FirstOrDefault();
                    return;
                }
            }
            catch (DbEntityValidationException exdb)
            {
                string msg = "";
                foreach (var e1 in exdb.EntityValidationErrors)
                {
                    foreach (var error in e1.ValidationErrors)
                    {
                        msg = string.Join(",", error.ErrorMessage);
                    }
                }
                AlertMessage.ShowError(msg);
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }

    }
}