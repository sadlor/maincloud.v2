﻿<%@ Page Title="Manage Roles" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true" CodeBehind="ManageRoles.aspx.cs" Inherits="MainCloud.Admin.ManageRoles" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <mcf:Alert runat="server" id="AlertMessage" />

    <h3><%: Title %></h3>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-inline">
                <div class="form-group">
                    <asp:TextBox ID="txtNewRoleName" runat="server"  />
                </div>
                <asp:Button runat="server" ID="BtnAdd" class="btn btn-default" Text="Add Role" OnClick="BtnAdd_Click" />
            </div>
        </div>
    </div>

    <asp:GridView ID="DgvRoles" runat="server" DataKeyNames="ID"
        CssClass="table table-hover table-striped" GridLines="None"
        OnRowDeleting="DgvRoles_RowDeleting"
        OnRowEditing="DgvRoles_RowEditing"
        AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
            <asp:BoundField DataField="Name" HeaderText="Name" />

            <asp:CheckBoxField DataField="AllowAddArea" HeaderText="Add<br/>Area" />
            <asp:CheckBoxField DataField="AllowEditArea" HeaderText="Edit<br/>Area" />
            <asp:CheckBoxField DataField="AllowDeleteArea" HeaderText="Delete<br/>Area" />
            <asp:CheckBoxField DataField="AllowAddWidget" HeaderText="Add<br/>Widget" />
            <asp:CheckBoxField DataField="AllowEditWidget" HeaderText="Edit<br/>Widget" />
            <asp:CheckBoxField DataField="AllowDeleteWidget" HeaderText="Delete<br/>Widget" />
            <asp:CheckBoxField DataField="AllowPublicArea" HeaderText="Public<br/>Area" />

<%--            <asp:CheckBoxField DataField="ShowWidgetAdvanced" HeaderText="Show<br/>Widget<br/>Advanced" />
            <asp:CheckBoxField DataField="ShowWidgetAggregator" HeaderText="Show<br/>Widget<br/>Aggregator" />--%>

            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <%--<asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select" class="btn btn-primary btn-xs"><i class="fa fa-info-circle"></i> Select</asp:LinkButton>--%>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" class="btn btn-primary btn-xs" Visible='<%# ("Host Administrators".Contains((string)Eval("Name"))) ? false : true %>'><i class="fa fa-pencil"></i> Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" class="btn btn-danger btn-xs" Visible='<%# ("Host Administrators Guests".Contains((string)Eval("Name"))) ? false : true %>'><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="cursor-pointer" />
    </asp:GridView>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
