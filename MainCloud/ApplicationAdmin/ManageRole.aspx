﻿<%@ Page Title="Manage Role" Language="C#" MasterPageFile="~/ApplicationAdmin/Site.Master" AutoEventWireup="true" CodeBehind="ManageRole.aspx.cs" Inherits="MainCloud.ApplicationAdmin.ManageRole" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <mcf:Alert runat="server" id="AlertMessage" />

    <h3><%: Title %></h3>
    <hr />

    <div class="form-horizontal">
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="chkAllowAddArea" CssClass="col-md-2 control-label">Allow Add Area</asp:Label>
            <div class="col-md-10">
                <asp:CheckBox runat="server" ID="chkAllowAddArea"/>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="chkAllowEditArea" CssClass="col-md-2 control-label">Allow Edit Area</asp:Label>
            <div class="col-md-10">
                <asp:CheckBox runat="server" ID="chkAllowEditArea"/>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="chkAllowDeleteArea" CssClass="col-md-2 control-label">Allow Delete Area</asp:Label>
            <div class="col-md-10">
                <asp:CheckBox runat="server" ID="chkAllowDeleteArea"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="chkAllowAddWidget" CssClass="col-md-2 control-label">Allow Add Widget</asp:Label>
            <div class="col-md-10">
                <asp:CheckBox runat="server" ID="chkAllowAddWidget"/>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="chkAllowEditWidget" CssClass="col-md-2 control-label">Allow Edit Widget</asp:Label>
            <div class="col-md-10">
                <asp:CheckBox runat="server" ID="chkAllowEditWidget"/>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="chkAllowDeleteWidget" CssClass="col-md-2 control-label">Allow Delete Widget</asp:Label>
            <div class="col-md-10">
                <asp:CheckBox runat="server" ID="chkAllowDeleteWidget"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="chkAllowPublicArea" CssClass="col-md-2 control-label">Allow Public Area</asp:Label>
            <div class="col-md-10">
                <asp:CheckBox runat="server" ID="chkAllowPublicArea"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="chkWidgetAdvanced" CssClass="col-md-2 control-label">Show Widget Advanced</asp:Label>
            <div class="col-md-10">
                <asp:CheckBox runat="server" ID="chkWidgetAdvanced"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="chkWidgetAggregator" CssClass="col-md-2 control-label">Show Widget Aggregator</asp:Label>
            <div class="col-md-10">
                <asp:CheckBox runat="server" ID="chkWidgetAggregator"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="btnSaveUser" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="btn btn-primary" />
                <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" CssClass="btn btn-default" CausesValidation="false" />
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
