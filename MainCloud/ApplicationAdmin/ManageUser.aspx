﻿<%@ Page Title="Manage User" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true" CodeBehind="ManageUser.aspx.cs" Inherits="MainCloud.Admin.ManageUser" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <mcf:Alert runat="server" id="AlertMessage" />

    <h3><%: Title %></h3>

    <div class="form-horizontal">
        <h4>Edit an account</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtUserName" CssClass="col-md-2 control-label">User name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtUserName" CssClass="form-control"/>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtUserName"
                    CssClass="text-danger" ErrorMessage="The user name field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtEmail" CssClass="col-md-2 control-label">Email</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" TextMode="Email"/>
               <%-- <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail"
                    CssClass="text-danger" ErrorMessage="The email field is required." />--%>
            </div>
        </div>

        <script type="text/javascript">
            function ValidateCheckBoxList(sender, args) {
                var checkBoxList = document.getElementById("<%=lstRoles.ClientID %>");
                var checkboxes = checkBoxList.getElementsByTagName("input");
                var isValid = false;
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].checked) {
                        isValid = true;
                        break;
                    }
                }
                args.IsValid = isValid;
            }
        </script>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                    ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" runat="server" />
                <asp:RadioButtonList ID="lstRoles" runat="server" DataTextField="Name" DataValueField="id"></asp:RadioButtonList>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="btnSaveUser" runat="server" OnClick="btnSaveUser_Click" Text="Save" CssClass="btn btn-primary" />
                <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" CssClass="btn btn-default" CausesValidation="false" />
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
