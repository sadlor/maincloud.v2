﻿using MainCloudFramework.Core.Multitenants.Identity;
using MainCloudFramework.Models;
using MainCloudFramework.Services;
using MainCloudFramework.Web;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

namespace MainCloud.Admin
{
    public partial class ManageRoles : BaseAdminPage
    {
        private ApplicationService applicationService = new ApplicationService();

        private void GridViewDataBind()
        {
            if (!IsHostUser)
            {
                List<string> excludeRoles = new List<string>(new string[] { MCFConstants.ROLE_OPERATOR } );
                DgvRoles.DataSource = applicationService.RolesInCurrentApplication(excludeRoles);
            }
            else
            {
                DgvRoles.DataSource = applicationService.RolesInCurrentApplication();
            }
            DgvRoles.DataBind();

            DgvRoles.UseAccessibleHeader = true;
            if (DgvRoles.HeaderRow != null)
            {
                DgvRoles.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridViewDataBind();
        }

        protected void DgvRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var usersContext = new ApplicationDbContext();
            string id = e.Keys["ID"] as string;
            var role = usersContext.Roles.FirstOrDefault(u => u.Id == id);
            if (role != null)
            {
                usersContext.Roles.Remove(role);
                //usersContext.Entry(user).State = EntityState.Deleted;
                usersContext.SaveChanges();
            }
            GridViewDataBind();
        }

        protected void DgvRoles_RowEditing(object sender, GridViewEditEventArgs e)
        {
            string id = DgvRoles.DataKeys[e.NewEditIndex].Value as string;
            string fromUrl = string.Format("~/{0}/{1}", MultiTenantsHelper.AdministrationUrl, "ManageRole".TrimStart('/'));
            string returnUrl = string.Format("~/{0}/{1}", MultiTenantsHelper.AdministrationUrl, "ManageRoles".TrimStart('/'));
            Response.Redirect(string.Format("{0}?Id={1}&ReturnUrl={2}", fromUrl, id, returnUrl));
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string appId = MultiTenantsHelper.ApplicationId;
                IdentityResult roleresult;

                // TODO: Verificare se serve
                if (string.IsNullOrEmpty(appId))
                {
                    var roleManager = new MFRoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new ApplicationDbContext()));
                    var newRole = new ApplicationRole(txtNewRoleName.Text);
                    roleresult = roleManager.Create(newRole);
                }
                else
                {
                    var roleManager = new MFRoleManager<ApplicationRole>(new ApplicationRoleStore<ApplicationRole>(appId, new ApplicationDbContext()));
                    var newRole = new ApplicationRole(txtNewRoleName.Text, txtNewRoleName.Text);
                    //newRole.ApplicationId = MultiTenantsHelper.ApplicationId;
                    roleresult = roleManager.Create(newRole);
                }

                if (roleresult.Succeeded)
                {
                    txtNewRoleName.Text = "";
                }
                else
                {
                    AlertMessage.ShowError(string.Join(",", roleresult.Errors));
                }
                GridViewDataBind();
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }
    }
}