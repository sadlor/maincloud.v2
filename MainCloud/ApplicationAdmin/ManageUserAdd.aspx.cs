﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using MainCloudFramework.Models;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using MainCloudFramework.Web;
using MainCloudFramework.Core.Multitenants.Identity;
using MainCloudFramework;
using MainCloudFramework.Services;
using MainCloudFramework.Repositories;
using System.Web.UI.WebControls;

namespace MainCloud.Admin
{
    public partial class ManageUserAdd : BaseAdminPage
    {
        protected ApplicationUser UserFound { get; set; }

        protected void AddUser_Click(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as Button;
                var selectedUser = btn.CommandArgument;
                if (!string.IsNullOrEmpty(selectedUser))
                {
                    ApplicationService appService = new ApplicationService();
                    appService.AddExistingUserById(selectedUser);
                }
                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            try
            {
                var textToSearch = (txtUserByName.Text ?? "").Trim();
                if (!string.IsNullOrEmpty(textToSearch))
                {
                    var appId = MultiTenantsHelper.ApplicationId;
                    UserRepository userService = new UserRepository();
                    UserFound = userService.FindUserByName(textToSearch, appId);
                    if (UserFound != null)
                    {
                        btnAdd.CommandArgument = UserFound.Id;
                        btnAdd.Enabled = true;
                    }
                    DataBind();
                }
            }
            catch (Exception ex)
            {
                AlertMessage.ShowError(string.Join(",", ex.Message));
            }
        }
    }
}