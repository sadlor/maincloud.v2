﻿using MainCloudFramework;
using MainCloudFramework.Web.BasePages;
using MainCloudFramework.Web.Helpers;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace MainCloud.Admin
{
    public partial class SiteMaster : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataBind(); // IMPORTANTE NON RIMUOVERE SERVE PER I BINDING DI SICUREZZA HOST <%# IsHostUser %>
            if (!string.IsNullOrEmpty(TenantName))
            {
                Page.Title += " [Application " + TenantName + "]";
            }
        }

        protected void lnkBtnBackHost_Click(object sender, EventArgs e)
        {
            var returnUrl = "";

            if (Request.QueryString["ReturnUrl"] != null)
            {
                returnUrl = Request.QueryString["ReturnUrl"];
            }
            else if (Request.AppRelativeCurrentExecutionFilePath == "~/Host/ManageApplications")
            {
                returnUrl = "~/ListApplications";
            }
            else if (IsHostUser)
            {
                returnUrl = "~/Host/ManageApplications";
            }
            else
            {
                returnUrl = MultiTenantsHelper.ApplicationUrl;
            }

            IdentityHelper.RedirectToReturnUrl(returnUrl, Response);
        }

        protected void hostMenu_Load(object sender, EventArgs e)
        {
            (sender as HtmlGenericControl).Visible = IsHostUser;
        }
    }

}