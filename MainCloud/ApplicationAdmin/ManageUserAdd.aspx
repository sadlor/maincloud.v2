﻿<%@ Page Title="Register" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true" CodeBehind="ManageUserAdd.aspx.cs" Inherits="MainCloud.Admin.ManageUserAdd" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="mcf" TagName="Alert" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <mcf:Alert runat="server" id="AlertMessage" />

    <h2><%: Title %>.</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div class="form-horizontal">
        <h4>Add existing user</h4>
        <hr />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtUserByName" CssClass="col-md-2 control-label">User name</asp:Label>
            <div class="col-md-5">
                <div class="input-group">
                    <asp:TextBox ID="txtUserByName" runat="server" CssClass="form-control" />
                    <span class="input-group-btn">
                        <asp:Button runat="server" ID="Search" OnClick="Search_Click" Text="Search" CssClass="btn btn-default" />
                    </span>
                </div>
            </div>
        </div>
        <div class="container">

                <div class="row" runat="server" Visible="<%# UserFound != null ? true : false %>">
                    <div class="col-md-10 col-md-offset-2"><h4>Risultati:</h4></div>
                    <div class="col-md-10 col-md-offset-2">
                        <dl class="dl-horizontal">
                          <dt>User name:</dt>
                          <dd><%# UserFound == null ? "" : UserFound.UserName %></dd>
                        </dl>
                    </div>
                </div>

        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="btnAdd" runat="server" OnClick="AddUser_Click" Text="Add" CssClass="btn btn-default" CommandName="add" CommandArgument="" Enabled="false" />
                <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" CssClass="btn btn-cancel" CausesValidation="false" />
            </div>
        </div>
    </div>
</asp:Content>