﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using MainCloudFramework.Models;
using MainCloudFramework.Web.Helpers;

namespace MainCloudFramework.Account
{
    public partial class Login : Page
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger("MCFLogger");

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            //ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    // Validate the user password
                    var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                    var loggedinUser = manager.Find(Email.Text, Password.Text);
                    if (loggedinUser != null)
                    {
                        // Now user have entered correct username and password.
                        // Time to change the security stamp
                        //                    manager.UpdateSecurityStamp(loggedinUser.Id);
                    }

                    // This doen't count login failures towards account lockout
                    // To enable password failures to trigger lockout, change to shouldLockout: true
                    // do sign-in AFTER we have done the update of the security stamp, so the new stamp goes into the cookie
                    var result = signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);

                    //log.Info("LogIn result" + result.ToString());
                    switch (result)
                    {
                        case SignInStatus.Success:

                            IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);

                            break;
                        case SignInStatus.LockedOut:
                            Response.Redirect("/Account/Lockout");
                            break;
                        case SignInStatus.RequiresVerification:
                            Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}",
                                                            Request.QueryString["ReturnUrl"],
                                                            RememberMe.Checked),
                                              true);
                            break;
                        case SignInStatus.Failure:
                        default:
                            FailureText.Text = "Invalid login attempt";
                            ErrorMessage.Visible = true;
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    //log.Info("errore login" + ex.Message);
                }
            }
        }
    }
}