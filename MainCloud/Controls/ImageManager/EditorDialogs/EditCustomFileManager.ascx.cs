﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloud.Dashboard.Modules.ImageManager.EditorDialogs
{
    public partial class EditCustomFileManager : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Telerik.Web.UI.RadFileExplorer rfe = (Telerik.Web.UI.RadFileExplorer)this.FindRadControl(this.Page);
            if (rfe != null)
            {
                rfe.FileList.Grid.MasterTableView.FilterExpression = "";   //.EnableLinqExpressions = false;
                rfe.Configuration.EnableAsyncUpload = true;
            }
        }

        //protected void Page_PreRender(object sender, EventArgs e)
        //{
        //    Telerik.Web.UI.RadFileExplorer rfe = (Telerik.Web.UI.RadFileExplorer)this.FindRadControl(this.Page);
        //    if (rfe != null)
        //    {
        //        FixLabelsAndToolTips(rfe);
        //    }
        //}

        private Control FindRadControl(Control parent)
        {
            foreach (Control c in parent.Controls)
            {
                if (c is Telerik.Web.UI.RadFileExplorer) return c;
                if (c.Controls.Count > 0)
                {
                    Control sub = FindRadControl(c);
                    if (sub != null) return sub;
                }
            }
            return null;
        }

        private void FixLabelsAndToolTips(Telerik.Web.UI.RadFileExplorer rfe)
        {
            //Toolbar
            foreach (Telerik.Web.UI.RadToolBarItem item in rfe.ToolBar.Items)
            {
                item.Text = item.Text.Replace("Common_", "");
                item.ToolTip = item.ToolTip.Replace("Common_", "");
            }
            //Grid
            foreach (Telerik.Web.UI.GridColumn column in rfe.FileList.Grid.MasterTableView.Columns)
            {
                column.HeaderText = column.HeaderText.Replace("Common_", "");
                column.HeaderTooltip = column.HeaderTooltip.Replace("Common_", "");
            }
            rfe.WindowManager.Title = rfe.WindowManager.Title.Replace("Common_", "");
        }
    }
}