﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageManager.ascx.cs" Inherits="MainCloudFramework.Controls.ImageManager" %>

<script type="text/javascript">
    function ImageManagerFunction(sender, args) {
        if (!args) {
            //alert('No file was selected!');
            return false;
        }

        var selectedItem = args.get_value();


        var img = $get('<%= ImageSelected.ClientID %>');
        if ($telerik.isIE) {
            img.value = selectedItem.outerHTML;  //this is the selected IMG tag element
        }
        else {
            var path = args.value[0].src;//args.value.getAttribute("src", 2);
            img.src = path;
        }
        var imgPath = $get('<%= ImageSelectedPath.ClientID %>');
        imgPath.value = args.value[0].src;
    }

    function DocumentManagerFunction(sender, args) {
        if (!args) {
            alert('No file was selected!');
            return false;
        }

        var selectedItem = args.get_value();
        var path = args.value.src;

        var txt = $get('<%= ImageSelected.ClientID %>');
        if ($telerik.isIE) {
            txt.value = selectedItem.outerHTML;
        }
        else {
            var path = args.value.pathname;
            txt.value = "<a href='" + path + "'>" + path + "</a>";
        }
    }

    function OpenDocManager() {
        var args = new Telerik.Web.UI.EditorCommandEventArgs("DocumentManager", null, document.createElement("a"));
        args.CssClasses = [];

        $find('<%= DialogOpener.ClientID %>').open('DocumentManager', args);
    }
</script> 
                 
<%--<asp:TextBox runat="server" ID="TextBox1"></asp:TextBox>--%>
<%--<asp:UpdatePanel runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger EventName="ImageManagerFunction" />
    </Triggers>
    <ContentTemplate>--%>

<asp:HiddenField runat="server" ID="ImageSelectedPath" />
<asp:Image runat="server" ID="ImageSelected" Width="30%" />
<telerik:RadDialogOpener Language="it-IT" runat="server" id="DialogOpener" HandlerUrl="~/Telerik.Web.UI.DialogHandler.axd"></telerik:RadDialogOpener>
<button type="button" class="btn btn-secondary" onclick="$find('<%= DialogOpener.ClientID %>').open('ImageManager', {CssClasses: []});return false;">Select</button>
<%--<button onclick="OpenDocManager();return false;">Open Document Manager</button>--%>

    <%--</ContentTemplate>
</asp:UpdatePanel>--%>