﻿using MainCloudFramework.UI.Modules;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Editor.DialogControls;

namespace MainCloudFramework.Controls
{
    public partial class ImageManager : UserControl
    {
        [Bindable(true), Category("Appearance")]
        public string Path
        {
            get
            {
                object o = ViewState["PathImageManager"];
                if (o == null)
                    return "~/Uploads/" + MultiTenantsHelper.ApplicationId;   // return the default value
                else
                    return (string)o;
            }
            set
            {
                ViewState["PathImageManager"] = "~/Uploads/" + MultiTenantsHelper.ApplicationId + value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Directory.Exists(Server.MapPath("~/Uploads/" + MultiTenantsHelper.ApplicationId)))
            {
                Directory.CreateDirectory(Server.MapPath("~/Uploads/" + MultiTenantsHelper.ApplicationId + "/Widgets/Shared"));
            }
            FileManagerDialogParameters imageManagerParameters = new FileManagerDialogParameters();
            imageManagerParameters.ViewPaths = new string[] { Path };
            imageManagerParameters.UploadPaths = new string[] { Path };
            imageManagerParameters.DeletePaths = new string[] { Path };
            imageManagerParameters.MaxUploadFileSize = 5000000;
            // If you don't set the following property, the default value will be used
            imageManagerParameters.SearchPatterns = new string[] { "*.jpg", "*.gif", "*.png", "*.jpe", "*.jpeg", "*.bmp" };

            DialogDefinition imageManager = new DialogDefinition(typeof(ImageManagerDialog), imageManagerParameters);
            imageManager.ClientCallbackFunction = "ImageManagerFunction";
            imageManager.Width = Unit.Pixel(694);
            imageManager.Height = Unit.Pixel(440);
            //If you need to customize the dialog then register the external dialog files
            imageManager.Parameters["ExternalDialogsPath"] = "~/Controls/ImageManager/EditorDialogs/";

            DialogOpener.DialogDefinitions.Add("ImageManager", imageManager);


            //FileManagerDialogParameters documentManagerParameters = new FileManagerDialogParameters();
            //documentManagerParameters.ViewPaths = new string[] { "~/Documents" };
            //documentManagerParameters.UploadPaths = new string[] { "~/Documents" };
            //documentManagerParameters.DeletePaths = new string[] { "~/Documents" };
            //documentManagerParameters.MaxUploadFileSize = 5000000;

            //DialogDefinition documentManager = new DialogDefinition(typeof(DocumentManagerDialog), documentManagerParameters);
            //documentManager.ClientCallbackFunction = "DocumentManagerFunction";
            //documentManager.Width = Unit.Pixel(694);
            //documentManager.Height = Unit.Pixel(440);
            //documentManager.Parameters["ExternalDialogsPath"] = "~/Dashboard/Modules/ImageManager/EditorDialogs/";

            //DialogOpener1.DialogDefinitions.Add("DocumentManager", documentManager);

            FileManagerDialogParameters imageEditorParameters = new FileManagerDialogParameters();
            imageEditorParameters.ViewPaths = new string[] { "~/Uploads/" + MultiTenantsHelper.ApplicationId };
            imageEditorParameters.UploadPaths = new string[] { "~/Uploads/" + MultiTenantsHelper.ApplicationId };
            imageEditorParameters.DeletePaths = new string[] { "~/Uploads/" + MultiTenantsHelper.ApplicationId };
            imageEditorParameters.MaxUploadFileSize = 5000000;

            DialogDefinition imageEditor = new DialogDefinition(typeof(ImageEditorDialog), imageEditorParameters);
            imageEditor.Width = Unit.Pixel(832);
            imageEditor.Height = Unit.Pixel(520);
            imageEditor.Parameters["ExternalDialogsPath"] = "~/Controls/ImageManager/EditorDialogs/";

            DialogOpener.DialogDefinitions.Add("ImageEditor", imageEditor);
        }

        public void SetImageManagerPaths(string[] viewPaths, string[] uploadPaths, string[] deletePaths)
        {
            //(DialogOpener.DialogDefinitions["ImageManager"].Parameters as FileManagerDialogParameters).ViewPaths = viewPaths;
            //(DialogOpener.DialogDefinitions["ImageManager"].Parameters as FileManagerDialogParameters).UploadPaths = uploadPaths;
            //(DialogOpener.DialogDefinitions["ImageManager"].Parameters as FileManagerDialogParameters).DeletePaths = deletePaths;

            FileManagerDialogParameters imageManagerParameters = new FileManagerDialogParameters();
            imageManagerParameters.ViewPaths = viewPaths;
            imageManagerParameters.UploadPaths = uploadPaths;
            imageManagerParameters.DeletePaths = deletePaths;
            imageManagerParameters.MaxUploadFileSize = 5000000;

            DialogDefinition imageManager = new DialogDefinition(typeof(ImageManagerDialog), imageManagerParameters);
            imageManager.ClientCallbackFunction = "ImageManagerFunction";
            imageManager.Width = Unit.Pixel(694);
            imageManager.Height = Unit.Pixel(440);
            imageManager.Parameters["ExternalDialogsPath"] = "~/Controls/ImageManager/EditorDialogs/";

            DialogOpener.DialogDefinitions.Add("ImageManager", imageManager);

            FileManagerDialogParameters imageEditorParameters = new FileManagerDialogParameters();
            imageEditorParameters.ViewPaths = viewPaths;
            imageEditorParameters.UploadPaths = uploadPaths;
            imageEditorParameters.DeletePaths = deletePaths;
            imageEditorParameters.MaxUploadFileSize = 5000000;

            DialogDefinition imageEditor = new DialogDefinition(typeof(ImageEditorDialog), imageEditorParameters);
            imageEditor.Width = Unit.Pixel(832);
            imageEditor.Height = Unit.Pixel(520);
            imageEditor.Parameters["ExternalDialogsPath"] = "~/Controls/ImageManager/EditorDialogs/";
            
            DialogOpener.DialogDefinitions.Add("ImageEditor", imageEditor);
        }
    }
}