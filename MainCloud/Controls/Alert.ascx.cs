﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloudFramework.Controls
{
    public partial class Alert : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AlertDiv.Visible = false;
        }

        private void ShowMessage(string message, string cssclass)
        {
            AlertDiv.Attributes.Add("class", "alert " + cssclass);
            AlertDiv.InnerText = message;
            AlertDiv.Visible = true;
        }

        public void ShowSuccess(string message)
        {
            ShowMessage(message, "alert-success");
        }

        public void ShowError(string message)
        {
            ShowMessage(message, "alert-danger");
        }

        public void ShowInfo(string message)
        {
            ShowMessage(message, "alert-info");
        }

        public void ShowWarning(string message)
        {
            ShowMessage(message, "alert-warning");
        }
    }
}