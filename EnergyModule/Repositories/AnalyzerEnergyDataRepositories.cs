﻿using EnergyModule.Models;
using MainCloudFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Repositories
{
    public class AnalyzerEnergyDataRepository : BaseRepository<AnalyzerEnergyData, EnergyDbContext>
    {
        public void Update(IList<AnalyzerEnergyData> energydatas)
        {
            foreach (AnalyzerEnergyData e in energydatas)
            {
                Update(e);
            }
        }

        public void Update(AnalyzerEnergyData energydata)
        {
            try
            {
                var dbED = (from e in DBContext.AnalyzerEnergyDatas
                            where e.AnalyzerId == energydata.AnalyzerId &&
                                  e.TimeStamp.Year == energydata.TimeStamp.Year &&
                                  e.TimeStamp.Month == energydata.TimeStamp.Month &&
                                  e.TimeStamp.Day == energydata.TimeStamp.Day &&
                                  e.TimeStamp.Hour == energydata.TimeStamp.Hour &&
                                  e.TimeStamp.Minute == energydata.TimeStamp.Minute
                            select e).First();
                if (dbED == null)
                {
                    DBContext.AnalyzerEnergyDatas.Add(energydata);
                }
                else
                {
                    //dbED = energydata;
                    //if (string.IsNullOrEmpty(dbED.Data))
                    //{
                    //    dbED.Data = energydata.Data;
                    //}
                    dbED.Data = energydata.Data; //verificare
                }
            }
            catch (Exception ex)
            {
                //la sequenza non contiene elementi
                DBContext.AnalyzerEnergyDatas.Add(energydata);
            }
            DBContext.SaveChanges();
        }

        public void Delete(AnalyzerEnergyData entity)
        {
            DBContext.AnalyzerEnergyDatas.Remove(DBContext.AnalyzerEnergyDatas.Where(x => x.TimeStamp == entity.TimeStamp && x.AnalyzerId == entity.AnalyzerId).SingleOrDefault());  //Remove(entity);
            DBContext.SaveChanges();
        }

        public List<AnalyzerEnergyData> Load(string analyzerId, DateTime start, DateTime end)
        {
            end = end.AddDays(1);

            IEnumerable<AnalyzerEnergyData> search = from s in DBContext.AnalyzerEnergyDatas
                                                     where s.AnalyzerId == analyzerId && s.TimeStamp >= start && s.TimeStamp < end
                                                     orderby s.TimeStamp
                                                     select s;
            return search.ToList();
        }

        //public List<AnalyzerEnergyData> FindByApplicationId(string applicationId, DateTime start, DateTime end)
        //{
        //    var search = from analyzerEnergyDatas in DBContext.AnalyzerEnergyDatas//.Include(i => i.Analyzer.Department.Plant)
        //                 where analyzerEnergyDatas.Analyzer.ApplicationId == applicationId && analyzerEnergyDatas.TimeStamp >= start.Date && analyzerEnergyDatas.TimeStamp <= end.Date
        //                 orderby analyzerEnergyDatas.TimeStamp
        //                 select analyzerEnergyDatas;
        //    return search.ToList();
        //}
    }
}