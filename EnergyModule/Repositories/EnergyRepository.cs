﻿using EnergyModule.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Repositories
{
    public class EnergyRepository
    {
        public void Update(IList<AnalyzerEnergyData> energydatas)
        {
            foreach (AnalyzerEnergyData e in energydatas)
            {
                Update(e);
            }
        }

        public void Update(AnalyzerEnergyData energydata)
        {
            using (EnergyDbContext db = new EnergyDbContext())
            {
                try
                {
                    var dbED = (from e in db.AnalyzerEnergyDatas
                                where e.AnalyzerId == energydata.AnalyzerId &&
                                      e.TimeStamp.Year == energydata.TimeStamp.Year &&
                                      e.TimeStamp.Month == energydata.TimeStamp.Month &&
                                      e.TimeStamp.Day == energydata.TimeStamp.Day &&
                                      e.TimeStamp.Hour == energydata.TimeStamp.Hour &&
                                      e.TimeStamp.Minute == energydata.TimeStamp.Minute
                                select e).First();
                    if (dbED == null)
                    {
                        db.AnalyzerEnergyDatas.Add(energydata);
                    }
                    else
                    {
                        //dbED = energydata;
                        //if (string.IsNullOrEmpty(dbED.Data))
                        //{
                        //    dbED.Data = energydata.Data;
                        //}
                        dbED.Data = energydata.Data; //verificare
                    }
                }
                catch (Exception ex)
                {
                    //la sequenza non contiene elementi
                    db.AnalyzerEnergyDatas.Add(energydata);
                }
                db.SaveChanges();
            }
        }

        //public void Save(string key, EnergyRecord entity) //key = "{Asset_Id:},{TimeStamp:}"
        //{
        //    using (EnergyDbContext db = new EnergyDbContext())
        //    {
        //        AnalyzerEnergyData e = new AnalyzerEnergyData();

        //        e.AssetId = key.Split(',')[0].Replace("{", "").Replace("}","").Split(';')[1];

        //        string date = key.Split(',')[1].Replace("{", "").Replace("}", "").Split(';')[1];
        //        if (date.Contains("/") || date.Contains("-"))
        //        {
        //            e.TimeStamp = Convert.ToDateTime(key.Split(',')[1].Replace("{", "").Replace("}", "").Split(';')[1]);
        //        }
        //        else
        //        {
        //            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        //            e.TimeStamp = start.AddMilliseconds(Convert.ToInt64(date)).ToLocalTime();
        //        }

        //        e.Data = SerializeData(entity);

        //        var update = from u in db.AnalyzerEnergyDatas.ToList()
        //                     where u.AssetId == e.AssetId && u.TimeStamp == e.TimeStamp
        //                     select u;
        //        if (update.Count() == 0)
        //        {
        //            //add
        //            db.AnalyzerEnergyDatas.Add(e);
        //            db.SaveChanges();
        //        }
        //        else
        //        {
        //            //update
        //            update.ElementAt(0).Data = e.Data;
        //            db.SaveChanges();
        //        }
        //    }
        //}

        public void Delete(AnalyzerEnergyData entity)
        {
            using (EnergyDbContext db = new EnergyDbContext())
            {
                db.AnalyzerEnergyDatas.Remove(db.AnalyzerEnergyDatas.Where(x => x.TimeStamp == entity.TimeStamp && x.AnalyzerId == entity.AnalyzerId).SingleOrDefault());  //Remove(entity);
                db.SaveChanges();
            }
        }

        public List<AnalyzerEnergyData> Load(string assetId, DateTime start)
        {
            using (EnergyDbContext db = new EnergyDbContext())
            {
                //sistemare i filtri

                IEnumerable<AnalyzerEnergyData> search = from s in db.AnalyzerEnergyDatas
                                                             //where s.AssetId == assetId && s.TimeStamp.Year >= start.Year && 
                                                             //s.TimeStamp.Month >= start.Month && s.TimeStamp.Day >= start.Day
                                                         where s.AnalyzerId == assetId && s.TimeStamp >= start
                                                         orderby s.TimeStamp
                                                         select s;
                return search.ToList();
            }
        }

        public List<AnalyzerEnergyData> Load(string assetId, DateTime start, DateTime end)
        {
            end = end.AddDays(1);
            using (EnergyDbContext db = new EnergyDbContext())
            {
                //sistemare i filtri

                IEnumerable<AnalyzerEnergyData> search = from s in db.AnalyzerEnergyDatas
                                                             //where s.AssetId == assetId && s.TimeStamp.Year >= start.Year && s.TimeStamp.Month >= start.Month &&
                                                             //s.TimeStamp.Day >= start.Day && s.TimeStamp.Year <= end.Year && s.TimeStamp.Month <= end.Month
                                                             //&& s.TimeStamp.Day <= end.Day
                                                         where s.AnalyzerId == assetId && s.TimeStamp >= start && s.TimeStamp < end
                                                         orderby s.TimeStamp
                                                         select s;
                return search.ToList();
            }
        }

        public static string SerializeData(object data)
        {
            return JsonConvert.SerializeObject(data);
        }

        //public List<AnalyzerEnergyData> FindByApplicationId(string applicationId, DateTime start, DateTime end)
        //{
        //    using (EnergyDbContext db = new EnergyDbContext())
        //    {
        //        var search = from analyzerEnergyDatas in db.AnalyzerEnergyDatas//.Include(i => i.Analyzer.Department.Plant)
        //                     where analyzerEnergyDatas.Analyzer.ApplicationId == applicationId && analyzerEnergyDatas.TimeStamp >= start.Date && analyzerEnergyDatas.TimeStamp <= end.Date
        //                     orderby analyzerEnergyDatas.TimeStamp
        //                     select analyzerEnergyDatas;
        //        return search.ToList();
        //    }
        //}
    }
}