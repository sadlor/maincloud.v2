﻿using EnergyModule.Models;
using MainCloudFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Repositories
{
    public class AlarmReadDataRepository : BaseRepository<AlarmReadData, EnergyDbContext>
    {
        public void Update(AlarmReadData alarm)
        {
            try
            {
                var dbAlarm = (from x in DBContext.AlarmReadDatas
                               where x.Id == alarm.Id
                               select x).First();
                dbAlarm = alarm;
            }
            catch (NullReferenceException ex)
            {
                DBContext.AlarmReadDatas.Add(alarm);
            }
            DBContext.SaveChanges();
        }
    }
}