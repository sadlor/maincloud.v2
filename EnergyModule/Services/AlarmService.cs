﻿using AssetManagement.Services;
using EnergyModule.Core;
using EnergyModule.Gateway.FTP.Model;
using EnergyModule.Models;
using EnergyModule.Repositories;
using MainCloudFramework.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Services
{
    public class AlarmService : BaseService<EnergyDbContext>
    {
        private AlarmReadDataRepository alarmRepository = new AlarmReadDataRepository();

        /// <summary>
        /// Verifica che la colonna quality della riga passata sia uguale a 0
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool IsRowInAlarm(EnergyCSVRecord row, string applicationId)
        {
            if (row.Quality == 0)
            {
                return false;
            }
            else
            {
                AnalyzerService analyzerService = new AnalyzerService();
                EnergyDataService energyDataService = new EnergyDataService();
                List<AlarmReadData> alarmList = GetAlarm(applicationId, AlarmEnum.AlarmContext.Param, analyzerService.GetAnalyzerIdBySerialCode(row.DeviceId, applicationId));
                if (alarmList.Count > 0)
                {
                    try
                    {
                        AlarmReadData alarm = alarmList.Where(x =>
                            JsonConvert.DeserializeObject<Dictionary<AlarmEnum.AlarmDetails, object>>(x.Details).Contains(new KeyValuePair<AlarmEnum.AlarmDetails, object>(AlarmEnum.AlarmDetails.ParamNameNotReceived, row.DeviceName)) &&
                            x.IsActive).First();
                        var details = JsonConvert.DeserializeObject<Dictionary<AlarmEnum.AlarmDetails, object>>(alarm.Details);
                        details[AlarmEnum.AlarmDetails.ParamNotReceivedNumber] = (int)details[AlarmEnum.AlarmDetails.ParamNotReceivedNumber] + 1;
                        if ((int)details[AlarmEnum.AlarmDetails.ParamNotReceivedNumber] == (int)alarm.Alarm.Threshold)
                        {
                            //l'allarme viene inviato, quindi lo chiudo
                            alarm.IsActive = false;
                            alarmRepository.Update(alarm);
                            return true;
                        }
                        else
                        {
                            alarmRepository.Update(alarm);
                            return false;
                        }
                    }
                    catch (NullReferenceException ex)
                    {
                        //creare l'allarme
                        AlarmReadData alarm = new AlarmReadData(applicationId);
                        alarm.AssociatedId = analyzerService.GetAnalyzerIdBySerialCode(row.DeviceId, applicationId);
                        alarm.AlarmId = GetAlarmId(applicationId, AlarmEnum.AlarmContext.Param);
                        Dictionary<AlarmEnum.AlarmDetails, object> details = new Dictionary<AlarmEnum.AlarmDetails, object>();
                        details.Add(AlarmEnum.AlarmDetails.ParamNameNotReceived, row.DeviceName);
                        details.Add(AlarmEnum.AlarmDetails.ParamNotReceivedNumber, 1);
                        alarm.Details = JsonConvert.SerializeObject(details);
                        if ((int)details[AlarmEnum.AlarmDetails.ParamNotReceivedNumber] == (int)alarm.Alarm.Threshold) //credo non vada
                        {
                            alarm.IsActive = false;
                            alarmRepository.Update(alarm);
                            return true;
                        }
                        else
                        {
                            alarmRepository.Update(alarm);
                            return false;
                        }
                    }
                }
                else
                {
                    //creare l'allarme
                    AlarmReadData alarm = new AlarmReadData(applicationId);
                    alarm.AssociatedId = analyzerService.GetAnalyzerIdBySerialCode(row.DeviceId, applicationId);
                    alarm.AlarmId = GetAlarmId(applicationId, AlarmEnum.AlarmContext.Param);
                    Dictionary<AlarmEnum.AlarmDetails, object> details = new Dictionary<AlarmEnum.AlarmDetails, object>();
                    details.Add(AlarmEnum.AlarmDetails.ParamNameNotReceived, row.DeviceName);
                    details.Add(AlarmEnum.AlarmDetails.ParamNotReceivedNumber, 1);
                    alarm.Details = JsonConvert.SerializeObject(details);
                    if ((int)details[AlarmEnum.AlarmDetails.ParamNotReceivedNumber] == (int)alarm.Alarm.Threshold) //credo non vada
                    {
                        alarm.IsActive = false;
                        alarmRepository.Update(alarm);
                        return true;
                    }
                    else
                    {
                        alarmRepository.Update(alarm);
                        return false;
                    }
                }
            }
        }

        public string GetAlarmId(string applicationId, AlarmEnum.AlarmContext alarmContext)
        {
            string alarmId = (from a in DBContext.Alarms
                              where a.ApplicationId == applicationId && a.Context == alarmContext.ToString()
                              select a.Id).FirstOrDefault();
            return alarmId;
        }

        public List<AlarmReadData> GetAlarm(string applicationId, AlarmEnum.AlarmContext alarmContext, string associatedId)
        {
            string alarmId = GetAlarmId(applicationId, alarmContext);

            var alarm = (from a in DBContext.AlarmReadDatas
                         where a.ApplicationId == applicationId && a.AlarmId == alarmId && a.AssociatedId == associatedId
                         select a).ToList();

            return alarm;
        }
    }
}