﻿using EnergyModule.Models;
using MainCloudFramework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Services
{
    public class CostService : BaseService<EnergyDbContext>
    {
        public List<Cost> GetRatesList(string applicationId, DateTime start, DateTime end)
        {
            List<Cost> tariffe = new List<Cost>();
            if (DBContext.Costs.Where(x => x.ApplicationId == applicationId).Count() > 0)
            {
                DateTime maxDate;
                try
                {
                    maxDate = DBContext.Costs.Where(x => x.ApplicationId == applicationId && x.Date <= start).Max(x => x.Date);
                }
                catch (InvalidOperationException ex)
                {
                    maxDate = DBContext.Costs.Where(x => x.ApplicationId == applicationId && x.Date >= start).Min(x => x.Date);
                }
                tariffe = DBContext.Costs.Where(x => x.ApplicationId == applicationId && x.Date >= maxDate && x.Date < end).OrderBy(x => x.Date).ToList();
            }
            return tariffe;
        }
    }
}