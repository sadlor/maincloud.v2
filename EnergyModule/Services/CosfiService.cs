﻿using EnergyModule.Core;
using EnergyModule.Models;
using MainCloudFramework.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Services
{
    public class CosfiService : BaseService<EnergyDbContext>
    {
        public SortedDictionary<string, decimal> GetAverageDaily(string analyzerId, DateTime start, DateTime end)
        {
            SortedDictionary<string, decimal> returnList = new SortedDictionary<string, decimal>();

            string paramName = EnergyEnum.ParamToRead.TotalPowerFactor.ToString();

            var energyDataList = (from x in DBContext.AnalyzerEnergyDatas
                                  where x.AnalyzerId == analyzerId && x.TimeStamp >= start && x.TimeStamp <= end
                                  select x).ToList();

            SortedDictionary<DateTime, EnergyRecord> erList = new SortedDictionary<DateTime, EnergyRecord>();
            foreach (var item in energyDataList)
            {
                try
                {
                    EnergyRecord e = JsonConvert.DeserializeObject<EnergyRecord>(item.Data);
                    erList[item.TimeStamp] = e;
                }
                catch (Exception ex)
                { }
            }

            if (erList.Count > 0)
            {
                start = erList.First().Key;

                while (start.Date <= erList.Last().Key.Date)
                {
                    var dayList = erList.Where(x => x.Key.Date == start.Date);

                    try
                    {
                        if (dayList.Count() == 0)
                        {
                            throw new NullReferenceException();
                        }

                        returnList.Add(
                            new DateTime(start.Year, start.Month, start.Day).ToString("dd/MM/yyyy"),
                            Math.Round(
                                dayList.Where(x => x.Value.Count > 0 && x.Value.Exist(paramName)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(paramName).Value,
                                CultureInfo.InvariantCulture)), 2));
                    }
                    catch (InvalidOperationException ex)
                    {
                        returnList.Add(new DateTime(start.Year, start.Month, start.Day).ToString("dd/MM/yyyy"), (decimal)0);
                    }
                    catch (NullReferenceException ex) //non ci sono dati per quel giorno
                    {
                        returnList.Add(new DateTime(start.Year, start.Month, start.Day).ToString("dd/MM/yyyy"), (decimal)0);
                    }

                    start = start.AddDays(1);
                }
            }
            return returnList;
        }
    }
}