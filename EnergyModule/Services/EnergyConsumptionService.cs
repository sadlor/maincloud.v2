﻿using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule.Core;
using EnergyModule.Models;
using EnergyModule.Repositories;
using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Services
{
    public class EnergyConsumptionService : BaseService<EnergyDbContext>
    {
        public List<EnergyConsumption> GetDailyList(string analyzerId, DateTime start, DateTime end)
        {
            List<DailyConsumption> list = (from x in DBContext.DailyConsumptions
                                           where x.ApplicationId == MultiTenantsHelper.ApplicationId &&
                                                 x.AnalyzerId == analyzerId &&
                                                 x.Date >= start && x.Date <= end
                                           orderby x.Date
                                           select x).ToList();
            return list.ToList<EnergyConsumption>();
        }

        public List<EnergyConsumption> GetMonthlyList(string analyzerId, DateTime start, DateTime end)
        {
            List<MonthlyConsumption> list = (from x in DBContext.MonthlyConsumptions
                                             where x.ApplicationId == MultiTenantsHelper.ApplicationId &&
                                                   x.AnalyzerId == analyzerId &&
                                                   x.Date >= start && x.Date <= end
                                             orderby x.Date
                                             select x).ToList();

            List<EnergyConsumption> returnList = list.ToList<EnergyConsumption>();

            if (end.Month >= DateTime.Today.Month || end > DateTime.Today)
            {
                //se è selezionato anche il mese in corso, prendo i dati dalla tabella del consumo giornaliero e li sommo
                List<EnergyConsumption> dayList = GetDailyList(analyzerId, new DateTime(DateTime.Today.Year, DateTime.Today.Month, 01), DateTime.Today.Date);
                EnergyConsumption item = new EnergyConsumption(analyzerId, MultiTenantsHelper.ApplicationId);
                item.Date = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 01);
                item.kWh = dayList.Sum(x => x.kWh);
                item.kWhF1 = dayList.Sum(x => x.kWhF1);
                item.kWhF2 = dayList.Sum(x => x.kWhF2);
                item.kWhF3 = dayList.Sum(x => x.kWhF3);
                item.CostF1 = dayList.Sum(x => x.CostF1);
                item.CostF2 = dayList.Sum(x => x.CostF2);
                item.CostF3 = dayList.Sum(x => x.CostF3);
                returnList.Add(item);
            }
            return returnList;
        }

        public List<EnergyConsumption> GetYearlyList(string analyzerId, DateTime start, DateTime end)
        {
            List<MonthlyConsumption> list = (from x in DBContext.MonthlyConsumptions
                                             where x.ApplicationId == MultiTenantsHelper.ApplicationId &&
                                                   x.AnalyzerId == analyzerId &&
                                                   x.Date >= start && x.Date <= end
                                             orderby x.Date
                                             select x).ToList();

            List<EnergyConsumption> returnValue = new List<EnergyConsumption>();
            foreach (int year in list.Select(x => x.Date.Year).Distinct())
            {
                EnergyConsumption item = new EnergyConsumption(analyzerId, MultiTenantsHelper.ApplicationId);
                item.Date = new DateTime(year, 01, 01);
                item.kWh = list.Where(x => x.Date.Year == year).Sum(x => x.kWh);
                item.kWhF1 = list.Where(x => x.Date.Year == year).Sum(x => x.kWhF1);
                item.kWhF2 = list.Where(x => x.Date.Year == year).Sum(x => x.kWhF2);
                item.kWhF3 = list.Where(x => x.Date.Year == year).Sum(x => x.kWhF3);
                item.CostF1 = list.Where(x => x.Date.Year == year).Sum(x => x.CostF1);
                item.CostF2 = list.Where(x => x.Date.Year == year).Sum(x => x.CostF2);
                item.CostF3 = list.Where(x => x.Date.Year == year).Sum(x => x.CostF3);
                returnValue.Add(item);
            }
            return returnValue;
        }

        public List<EnergyConsumption> GetEnergyDataList(string analyzerId, DateTime start, DateTime end, EnergyEnum.TimeScale timeScale)
        {
            switch (timeScale)
            {
                case EnergyEnum.TimeScale.Year:
                    List<EnergyConsumption> yearlyList = GetYearlyList(analyzerId, start, end);
                    return yearlyList;
                case EnergyEnum.TimeScale.Month:
                    List<EnergyConsumption> monthlyList = GetMonthlyList(analyzerId, start, end);
                    return monthlyList;
                case EnergyEnum.TimeScale.Day:
                case EnergyEnum.TimeScale.Hour:
                    List<EnergyConsumption> dailyList = GetDailyList(analyzerId, start, end);
                    return dailyList;
                    //case EnergyEnum.TimeScale.Hour:
                    //    EnergyDataService EDS = new EnergyDataService();
                    //    var energyDataList = EDS.GetAnalyzerEnergyDataList(analyzer, start, end);
                    //    SortedDictionary<DateTime, EnergyRecord> erList = new SortedDictionary<DateTime, EnergyRecord>();
                    //    foreach (var item in energyDataList)
                    //    {
                    //        try
                    //        {
                    //            EnergyRecord e = JsonConvert.DeserializeObject<EnergyRecord>(item.Data);
                    //            erList[item.TimeStamp] = e;
                    //        }
                    //        catch (Exception ex)
                    //        {}
                    //    }
                    //    HistoryDataService historyDataService = new HistoryDataService();
                    //    SortedDictionary<DateTime, EnergyData> history = historyDataService.GetHourlyData(erList, EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString());
                    //    foreach (var item in history)
                    //    {
                    //        EnergyConsumption
                    //        item.Key
                    //    }
                    //    break;
            }
            return new List<EnergyConsumption>();
        }

        public List<EnergyConsumption> GetList(string analyzerId, DateTime start, DateTime end)
        {
            List<EnergyConsumption> energyList = new List<EnergyConsumption>();
            ConsumptionDailyStorageRepository dailyRepos = new ConsumptionDailyStorageRepository();
            ConsumptionMonthlyStorageRepository monthlyRepos = new ConsumptionMonthlyStorageRepository();
            bool monthly = false; //mi indica se la scala temporale in esame è mensile o giornaliera

            if (start.Day != 01)
            {
                if (end.Month == start.Month && end.Year == start.Year)
                {
                    energyList.AddRange(dailyRepos.ReadAll(x => x.AnalyzerId == analyzerId && x.Date >= start && x.Date <= end).OrderBy(x => x.Date).ToList<EnergyConsumption>());
                    return energyList;
                }
                else
                {
                    DateTime endMonth = new DateTime(start.Year, start.Month, DateTime.DaysInMonth(start.Year, start.Month));
                    energyList.AddRange(dailyRepos.ReadAll(x => x.AnalyzerId == analyzerId && x.Date >= start && x.Date <= endMonth).OrderBy(x => x.Date).ToList<EnergyConsumption>());
                }
            }
            else
            {
                if (end.Month == start.Month && end.Year == start.Year)
                {
                    energyList.AddRange(dailyRepos.ReadAll(x => x.AnalyzerId == analyzerId && x.Date >= start && x.Date <= end).OrderBy(x => x.Date).ToList<EnergyConsumption>());
                    return energyList;
                }
                else
                {
                    DateTime endMonth = new DateTime(start.Year, start.Month, DateTime.DaysInMonth(start.Year, start.Month));
                    energyList.AddRange(monthlyRepos.ReadAll(x => x.AnalyzerId == analyzerId && x.Date >= start && x.Date <= endMonth).OrderBy(x => x.Date).ToList<EnergyConsumption>());
                    monthly = true;
                }
            }

            List<EnergyConsumption> returnList = new List<EnergyConsumption>();
            DateTime startMonth = start.AddMonths(1);
            if (startMonth.Day != 01)
            {
                startMonth = new DateTime(startMonth.Year, startMonth.Month, 01);
            }
            while (startMonth <= end)
            {
                if (end.Month == startMonth.Month && end.Year == startMonth.Year)
                {
                    energyList.AddRange(dailyRepos.ReadAll(x => x.AnalyzerId == analyzerId && x.Date >= startMonth && x.Date <= end).OrderBy(x => x.Date).ToList<EnergyConsumption>());
                    if (monthly)
                    {
                        DateTime startCicle = new DateTime(start.Year, start.Month, 01);
                        while (startCicle <= end)
                        {
                            EnergyConsumption item = new EnergyConsumption(analyzerId, MultiTenantsHelper.ApplicationId);
                            item.Date = new DateTime(startCicle.Year, startCicle.Month, 01);
                            item.kWh = energyList.Where(x => x.Date.Month == startCicle.Month && x.Date.Year == startCicle.Year).Sum(x => x.kWh);
                            item.kWhF1 = energyList.Where(x => x.Date.Month == startCicle.Month && x.Date.Year == startCicle.Year).Sum(x => x.kWhF1);
                            item.kWhF2 = energyList.Where(x => x.Date.Month == startCicle.Month && x.Date.Year == startCicle.Year).Sum(x => x.kWhF2);
                            item.kWhF3 = energyList.Where(x => x.Date.Month == startCicle.Month && x.Date.Year == startCicle.Year).Sum(x => x.kWhF3);
                            item.CostF1 = energyList.Where(x => x.Date.Month == startCicle.Month && x.Date.Year == startCicle.Year).Sum(x => x.CostF1);
                            item.CostF2 = energyList.Where(x => x.Date.Month == startCicle.Month && x.Date.Year == startCicle.Year).Sum(x => x.CostF2);
                            item.CostF3 = energyList.Where(x => x.Date.Month == startCicle.Month && x.Date.Year == startCicle.Year).Sum(x => x.CostF3);
                            returnList.Add(item);
                            startCicle = startCicle.AddMonths(1);
                        }
                        return returnList;
                    }
                    return energyList;
                }
                else
                {
                    DateTime endMonth = new DateTime(startMonth.Year, startMonth.Month, DateTime.DaysInMonth(startMonth.Year, startMonth.Month));
                    energyList.AddRange(monthlyRepos.ReadAll(x => x.AnalyzerId == analyzerId && x.Date >= startMonth && x.Date <= endMonth).OrderBy(x => x.Date).ToList<EnergyConsumption>());
                    monthly = true;
                }

                startMonth = startMonth.AddMonths(1);
            }

            return energyList;
        }

        public EnergyConsumption GetTotalizerCostConsumption(string analyzerId, DateTime start, DateTime end)
        {
            List<EnergyConsumption> list = GetList(analyzerId, start, end);
            if (list.Count > 0)
            {
                EnergyConsumption returnValue = new EnergyConsumption();
                returnValue.kWh = list.Sum(x => x.kWh);
                returnValue.kWhF1 = list.Sum(x => x.kWhF1);
                returnValue.kWhF2 = list.Sum(x => x.kWhF2);
                returnValue.kWhF3 = list.Sum(x => x.kWhF3);
                returnValue.CostF1 = list.Sum(x => x.CostF1);
                returnValue.CostF2 = list.Sum(x => x.CostF2);
                returnValue.CostF3 = list.Sum(x => x.CostF3);
                return returnValue;
            }
            else
            {
                return null;
            }
        }

        public List<MonthlyConsumption> TotalAssetGroupConsumption(string groupId, DateTime start, DateTime end)
        {
            List<MonthlyConsumption> dataSource = new List<MonthlyConsumption>();

            AssetGroupService AGS = new AssetGroupService();
            List<Asset> assetList = AGS.GetAssetsByGroup(groupId);
            foreach (Asset assetItem in assetList)
            {
                ConsumptionStorage.ConsumptionStorage storage = new ConsumptionStorage.ConsumptionStorage();
                MonthlyConsumption tot = storage.GetTotalizerCostConsumption(assetItem.AnalyzerId, start, end);  //valutare se non c'è analizzatore collegato
                dataSource.Add(tot);  //TODO: storage.GetTotalizerCostConsumption non riporta la data
            }
            List<DateTime> dateList = dataSource.Select(x => x.Date).Distinct().OrderBy(x => x.Date).ToList(); //TODO: non va perchè non c'è la data
            List<MonthlyConsumption> totalConsumption = new List<MonthlyConsumption>();
            foreach (DateTime item in dateList)
            {
                MonthlyConsumption tot = new MonthlyConsumption();
                tot.Date = item;
                tot.kWh = dataSource.Where(x => x.Date == item).Sum(x => x.kWh);
                tot.kWhF1 = dataSource.Where(x => x.Date == item).Sum(x => x.kWhF1);
                tot.kWhF2 = dataSource.Where(x => x.Date == item).Sum(x => x.kWhF2);
                tot.kWhF3 = dataSource.Where(x => x.Date == item).Sum(x => x.kWhF3);
                tot.CostF1 = dataSource.Where(x => x.Date == item).Sum(x => x.CostF1);
                tot.CostF2 = dataSource.Where(x => x.Date == item).Sum(x => x.CostF2);
                tot.CostF3 = dataSource.Where(x => x.Date == item).Sum(x => x.CostF3);
                totalConsumption.Add(tot);
            }
            return totalConsumption;
        }
    }
}