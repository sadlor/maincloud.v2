﻿using AssetManagement.Core;
using AssetManagement.Models;
using AssetManagement.Services;
using EnergyModule.Core;
using EnergyModule.Models;
using EnergyModule.Repositories;
using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EnergyModule.Services
{
    public class EnergyDataService : BaseService<EnergyDbContext>
    {
        private AnalyzerEnergyDataRepository energyDataRepository = new AnalyzerEnergyDataRepository();

        public string GetAnalyzerId(string selectedId, string type)
        {
            if (type.ToUpper() == "ASSET")
            {
                return GetAnalyzerId(selectedId, AssetManagementEnum.AnalyzerContext.Asset);
            }
            if (type.ToUpper() == "DEPARTMENT")
            {
                return GetAnalyzerId(selectedId, AssetManagementEnum.AnalyzerContext.Department);
            }
            if (type.ToUpper() == "PLANT")
            {
                return GetAnalyzerId(selectedId, AssetManagementEnum.AnalyzerContext.Plant);
            }
            return null;
        }

        public string GetAnalyzerId(string selectedId, AssetManagementEnum.AnalyzerContext type)
        {
            AnalyzerService service = new AnalyzerService();
            return service.GetAnalyzerIdByContextId(selectedId, type);
        }

        public string GetAnalyzerId(SelectedContext context)
        {
            return GetAnalyzerId(context.Id, context.Type);
        }

        public List<AnalyzerEnergyData> GetAnalyzerEnergyDataList(SelectedContext context, DateTime start, DateTime end)
        {
            string analyzerId = GetAnalyzerId(context);
            List<AnalyzerEnergyData> energyDataList = energyDataRepository.Load(analyzerId, start, end);

            return energyDataList;
        }

        public List<AnalyzerEnergyData> GetAnalyzerEnergyDataList(string analyzerId, DateTime start, DateTime end)
        {
            List<AnalyzerEnergyData> energyDataList = energyDataRepository.Load(analyzerId, start, end);
            return energyDataList;
        }

        public List<AnalyzerEnergyData> GetAnalyzerEnergyDataList(params Expression<Func<AnalyzerEnergyData, Boolean>>[] predicates)
        {
            return energyDataRepository.ReadAll(predicates).OrderBy(x => x.TimeStamp).ToList();
        }

        public EnergyData GetTotalizerEnergyData(string analyzerId, DateTime start, DateTime end, string paramName)
        {
            EnergyData ED = null;
            List<AnalyzerEnergyData> energyDataList = GetAnalyzerEnergyDataList(analyzerId, start, end);
            if (energyDataList.Count > 0)
            {
                SortedDictionary<DateTime, EnergyRecord> erList = new SortedDictionary<DateTime, EnergyRecord>();
                foreach (var item in energyDataList)
                {
                    try
                    {
                        EnergyRecord e = JsonConvert.DeserializeObject<EnergyRecord>(item.Data);
                        erList[item.TimeStamp] = e;
                    }
                    catch (Exception ex)
                    { }
                }

                if (paramName == EnergyEnum.ParamToRead.TotalActivePower.ToString())
                {
                    ED = new EnergyData(
                        paramName,
                        Math.Round(erList.Values.Where(x => x.Count > 0 && x.Exist(paramName)).Average(x => Convert.ToDecimal(x.GetEnergyData(paramName).Value)) / 1000, 1),
                        Math.Round(erList.Values.Where(x => x.Count > 0 && x.Exist(paramName)).Average(x => Convert.ToDecimal(x.GetEnergyData(paramName).Min)) / 1000, 1),
                        Math.Round(erList.Values.Where(x => x.Count > 0 && x.Exist(paramName)).Average(x => Convert.ToDecimal(x.GetEnergyData(paramName).Max)) / 1000, 1)
                        );
                }
                if (paramName == EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString())// || paramName == EnergyEnum.ParamToRead.TotalActiveEnergyProduced.ToString())
                {
                    ED = new EnergyData(paramName, Math.Round((erList.Values.Last(x => x.Count > 0 && x.Exist(paramName) && x.GetData(paramName) > 0).GetData(paramName) - erList.Values.First(x => x.Count > 0 && x.Exist(paramName) && x.GetData(paramName) > 0).GetData(paramName)), 1));
                }
            }
            return ED;
        }

        public SortedDictionary<DateTime, EnergyData> CreateHistorySerie(SelectedContext context, DateTime start, DateTime end, string param, EnergyEnum.TimeScale typeOfTime)
        {
            var energyDataList = GetAnalyzerEnergyDataList(context, start, end);

            SortedDictionary<DateTime, EnergyRecord> erList = new SortedDictionary<DateTime, EnergyRecord>();
            foreach (var item in energyDataList)
            {
                try
                {
                    EnergyRecord e = JsonConvert.DeserializeObject<EnergyRecord>(item.Data);
                    erList[item.TimeStamp] = e;
                }
                catch (Exception ex)
                { }
            }

            HistoryDataService historyDataService = new HistoryDataService();
            switch (typeOfTime)
            {
                case EnergyEnum.TimeScale.Year:
                    return historyDataService.GetYearlyData(erList, param);
                case EnergyEnum.TimeScale.Month:
                    return historyDataService.GetMonthlyData(erList, param);
                case EnergyEnum.TimeScale.Day:
                    return historyDataService.GetDailyData(erList, param);
                case EnergyEnum.TimeScale.Hour:
                    return historyDataService.GetHourlyData(erList, param);
            }

            return new SortedDictionary<DateTime, EnergyData>();
        }

        /// <summary>
        /// Restituisce l'ultimo consumo kWh valido per poter fare la differenza nel calcolo del consumo giornaliero
        /// </summary>
        /// <param name="start">Giorno di partenza di lettura dati</param>
        /// <returns></returns>
        public decimal GetLastValidateActiveEnergyConsumed(DateTime start, string analyzerId, int day = 20)
        {
            DateTime end = start.AddDays(-day);
            var searchingList = (from a in DBContext.AnalyzerEnergyDatas
                                 where a.AnalyzerId == analyzerId &&
                                 a.TimeStamp < start && a.TimeStamp > end &&
                                 a.Data.Contains(EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString())
                                 orderby a.TimeStamp
                                 select a).ToList();
            if (searchingList.Count == 0)
            {
                return 0;
            }
            EnergyRecord er = JsonConvert.DeserializeObject<EnergyRecord>(searchingList.LastOrDefault(
                x => JsonConvert.DeserializeObject<EnergyRecord>(x.Data).GetData(EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString()) > 0
                ).Data);
            if (er != null)
            {
                return er.GetData(EnergyEnum.ParamToRead.TotalActiveEnergyConsumed.ToString());
            }
            else
            {
                return GetLastValidateActiveEnergyConsumed(start.AddDays(-10), analyzerId, day + 10);
            }
        }

        public decimal GetPlantEnablePower(SelectedContext context)
        {
            Plant p = null;
            switch (context.Type)
            {
                case AssetManagementEnum.AnalyzerContext.Asset:
                    AssetService AS = new AssetService();
                    p = AS.GetPlantOwner(context.Id);
                    break;
                case AssetManagementEnum.AnalyzerContext.Department:
                    DepartmentService DS = new DepartmentService();
                    p = DS.GetPlantOwner(context.Id);
                    break;
                case AssetManagementEnum.AnalyzerContext.Plant:
                    PlantService PS = new PlantService();
                    p = PS.GetPlantById(context.Id);
                    break;
            }
            try
            {
                return Convert.ToDecimal(p.EnablePower, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}