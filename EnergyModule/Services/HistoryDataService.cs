﻿using EnergyModule.Core;
using EnergyModule.Models;
using MainCloudFramework.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Services
{
    public class HistoryDataService : BaseService<EnergyDbContext>
    {
        public SortedDictionary<DateTime, EnergyData> GetMonthlyData(SortedDictionary<DateTime, EnergyRecord> erList, string param)
        {
            SortedDictionary<DateTime, EnergyData> returnList = new SortedDictionary<DateTime, EnergyData>();

            if (erList.Count > 0)
            {
                DateTime start = erList.First().Key;

                while (start.Date <= erList.Last().Key.Date)
                {
                    var monthList = erList.Where(x => x.Key.Year == start.Year && x.Key.Month == start.Month);

                    try
                    {
                        if (monthList.Count() == 0)
                        {
                            throw new NullReferenceException();
                        }
                        switch (param)
                        {
                            case "TotalActiveEnergyConsumed":
                            case "TotalReactiveEnergyConsumed":
                                var value = Math.Round(
                                    Convert.ToDecimal(monthList.LastOrDefault(x => x.Value.Count > 0 && x.Value.Exist(param) && x.Value.GetData(param) > 0).Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture) -
                                    Convert.ToDecimal(monthList.FirstOrDefault(x => x.Value.Count > 0 && x.Value.Exist(param) && x.Value.GetData(param) > 0).Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture),
                                    1);
                                returnList.Add(new DateTime(start.Year, start.Month, 01), new EnergyData(param, value));
                                break;
                            case "TotalActivePower":
                            case "TotalReactivePower":
                                returnList.Add(new DateTime(start.Year, start.Month, 01), new EnergyData(param,
                                    Math.Round(monthList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture)) / 1000, 1),
                                    Math.Round(monthList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Max, CultureInfo.InvariantCulture)) / 1000, 1),
                                    Math.Round(monthList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Min, CultureInfo.InvariantCulture)) / 1000, 1)));
                                break;
                            default:
                                returnList.Add(new DateTime(start.Year, start.Month, 01), new EnergyData(param,
                                    Math.Round(monthList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture)), 2),
                                    Math.Round(monthList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Max, CultureInfo.InvariantCulture)), 2),
                                    Math.Round(monthList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Min, CultureInfo.InvariantCulture)), 2)));
                                break;
                        }
                    }
                    catch (NullReferenceException ex)
                    {
                        returnList.Add(new DateTime(start.Year, start.Month, 01), new EnergyData(param, (decimal)0));
                    }

                    start = start.AddMonths(1);
                }
            }
            return returnList;
        }

        public SortedDictionary<DateTime, EnergyData> GetDailyData(SortedDictionary<DateTime, EnergyRecord> erList, string param)
        {
            SortedDictionary<DateTime, EnergyData> returnList = new SortedDictionary<DateTime, EnergyData>();

            if (erList.Count > 0)
            {
                DateTime start = erList.First().Key;

                while (start.Date <= erList.Last().Key.Date)
                {
                    var dayList = erList.Where(x => x.Key.Date == start.Date);

                    try
                    {
                        if (dayList.Count() == 0)
                        {
                            throw new NullReferenceException();
                        }
                        switch (param)
                        {
                            case "TotalActiveEnergyConsumed":
                            case "TotalReactiveEnergyConsumed":
                                var value = Math.Round(
                                    Convert.ToDecimal(dayList.LastOrDefault(x => x.Value.Count > 0 && x.Value.Exist(param) && x.Value.GetData(param) > 0).Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture) -
                                    Convert.ToDecimal(dayList.FirstOrDefault(x => x.Value.Count > 0 && x.Value.Exist(param) && x.Value.GetData(param) > 0).Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture),
                                    1);
                                returnList.Add(new DateTime(start.Year, start.Month, start.Day), new EnergyData(param, value));
                                break;
                            case "TotalActivePower":
                            case "TotalReactivePower":
                                returnList.Add(new DateTime(start.Year, start.Month, start.Day), new EnergyData(param,
                                    Math.Round(dayList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture)) / 1000, 1),
                                    Math.Round(dayList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Max, CultureInfo.InvariantCulture)) / 1000, 1),
                                    Math.Round(dayList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Min, CultureInfo.InvariantCulture)) / 1000, 1)));
                                break;
                            default:
                                returnList.Add(new DateTime(start.Year, start.Month, start.Day), new EnergyData(param,
                                    Math.Round(dayList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture)), 2),
                                    Math.Round(dayList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Max, CultureInfo.InvariantCulture)), 2),
                                    Math.Round(dayList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Min, CultureInfo.InvariantCulture)), 2)));
                                break;
                        }
                    }
                    catch (NullReferenceException ex) //non ci sono dati per quel giorno
                    {
                        returnList.Add(new DateTime(start.Year, start.Month, start.Day), new EnergyData(param, (decimal)0));
                    }

                    start = start.AddDays(1);
                }
            }
            return returnList;
        }

        public SortedDictionary<DateTime, EnergyData> GetYearlyData(SortedDictionary<DateTime, EnergyRecord> erList, string param)
        {
            SortedDictionary<DateTime, EnergyData> returnList = new SortedDictionary<DateTime, EnergyData>();

            if (erList.Count > 0)
            {
                DateTime start = erList.First().Key;

                while (start.Date <= erList.Last().Key.Date)
                {
                    var yearList = erList.Where(x => x.Key.Year == start.Year);

                    try
                    {
                        if (yearList.Count() == 0)
                        {
                            throw new NullReferenceException();
                        }
                        switch (param)
                        {
                            case "TotalActiveEnergyConsumed":
                            case "TotalReactiveEnergyConsumed":
                                var value = Math.Round(
                                    Convert.ToDecimal(yearList.LastOrDefault(x => x.Value.Count > 0 && x.Value.Exist(param) && x.Value.GetData(param) > 0).Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture) -
                                    Convert.ToDecimal(yearList.FirstOrDefault(x => x.Value.Count > 0 && x.Value.Exist(param) && x.Value.GetData(param) > 0).Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture),
                                    1);
                                returnList.Add(new DateTime(start.Year, 01, 01), new EnergyData(param, value));
                                break;
                            case "TotalActivePower":
                            case "TotalReactivePower":
                                returnList.Add(new DateTime(start.Year, 01, 01), new EnergyData(param,
                                    Math.Round(yearList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture)) / 1000, 1),
                                    Math.Round(yearList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Max, CultureInfo.InvariantCulture)) / 1000, 1),
                                    Math.Round(yearList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Min, CultureInfo.InvariantCulture)) / 1000, 1)));
                                break;
                            default:
                                returnList.Add(new DateTime(start.Year, 01, 01), new EnergyData(param,
                                    Math.Round(yearList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture)), 2),
                                    Math.Round(yearList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Max, CultureInfo.InvariantCulture)), 2),
                                    Math.Round(yearList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Min, CultureInfo.InvariantCulture)), 2)));
                                break;
                        }
                    }
                    catch (NullReferenceException ex) //non ci sono dati per quel giorno
                    {
                        returnList.Add(new DateTime(start.Year, 01, 01), new EnergyData(param, (decimal)0));
                    }

                    start = start.AddYears(1);
                }
            }
            return returnList;
        }

        public SortedDictionary<DateTime, EnergyData> GetHourlyData(SortedDictionary<DateTime, EnergyRecord> erList, string param)
        {
            SortedDictionary<DateTime, EnergyData> returnList = new SortedDictionary<DateTime, EnergyData>();

            if (erList.Count > 0)
            {
                DateTime start = erList.First().Key;

                while (start.Date <= erList.Last().Key.Date)
                {
                    var hourList = erList.Where(x => x.Key.Date == start.Date && x.Key.Hour == start.Hour);

                    try
                    {
                        if (hourList.Count() == 0)
                        {
                            throw new NullReferenceException();
                        }
                        switch (param)
                        {
                            case "TotalActiveEnergyConsumed":
                            case "TotalReactiveEnergyConsumed":
                                var value = Math.Round(
                                    Convert.ToDecimal(hourList.LastOrDefault(x => x.Value.Count > 0 && x.Value.Exist(param) && x.Value.GetData(param) > 0).Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture) -
                                    Convert.ToDecimal(hourList.FirstOrDefault(x => x.Value.Count > 0 && x.Value.Exist(param) && x.Value.GetData(param) > 0).Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture),
                                    1);
                                returnList.Add(new DateTime(start.Year, start.Month, start.Day, start.Hour, 0, 0), new EnergyData(param, value));
                                break;
                            case "TotalActivePower":
                            case "TotalReactivePower":
                                returnList.Add(new DateTime(start.Year, start.Month, start.Day, start.Hour, 0, 0), new EnergyData(param,
                                    Math.Round(hourList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture)) / 1000, 1),
                                    Math.Round(hourList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Max, CultureInfo.InvariantCulture)) / 1000, 1),
                                    Math.Round(hourList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Min, CultureInfo.InvariantCulture)) / 1000, 1)));
                                break;
                            default:
                                returnList.Add(new DateTime(start.Year, start.Month, start.Day, start.Hour, 0, 0), new EnergyData(param,
                                    Math.Round(hourList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Value, CultureInfo.InvariantCulture)), 2),
                                    Math.Round(hourList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Max, CultureInfo.InvariantCulture)), 2),
                                    Math.Round(hourList.Where(x => x.Value.Count > 0 && x.Value.Exist(param)).Average(x => Convert.ToDecimal(x.Value.GetEnergyData(param).Min, CultureInfo.InvariantCulture)), 2)));
                                break;
                        }
                    }
                    catch (NullReferenceException ex) //non ci sono dati per quel giorno
                    {
                        returnList.Add(new DateTime(start.Year, start.Month, start.Day, start.Hour, 0, 0), new EnergyData(param, (decimal)0));
                    }

                    start = start.AddHours(1);
                }
            }
            return returnList;
        }
    }
}