﻿using AssetManagement.Core;
using AssetManagement.Models;
using EnergyModule.Core;
using EnergyModule.Models;
using MainCloudFramework.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Services
{
    public class SelectedContextService : BaseService<AssetManagementDbContext>
    {
        public string GetSelectedDescription(SelectedContext context)
        {
            string descr = "";
            switch (context.Type)
            {
                case AssetManagementEnum.AnalyzerContext.Asset:
                    descr = (from a in DBContext.Assets
                             where a.Id == context.Id
                             select a.Description).FirstOrDefault();
                    break;
                case AssetManagementEnum.AnalyzerContext.Plant:
                    descr = (from p in DBContext.Plants
                             where p.Id == context.Id
                             select p.Description).FirstOrDefault();
                    break;
                case AssetManagementEnum.AnalyzerContext.Department:
                    descr = (from d in DBContext.Departments
                             where d.Id == context.Id
                             select d.Description).FirstOrDefault();
                    break;
            }
            return descr;
        }

        public string GetSelectedDescription(string value)
        {
            SelectedContext context = JsonConvert.DeserializeObject<SelectedContext>(value);
            string descr = "";
            switch (context.Type)
            {
                case AssetManagementEnum.AnalyzerContext.Asset:
                    descr = (from a in DBContext.Assets
                             where a.Id == context.Id
                             select a.Description).FirstOrDefault();
                    break;
                case AssetManagementEnum.AnalyzerContext.Plant:
                    descr = (from p in DBContext.Plants
                             where p.Id == context.Id
                             select p.Description).FirstOrDefault();
                    break;
                case AssetManagementEnum.AnalyzerContext.Department:
                    descr = (from d in DBContext.Departments
                             where d.Id == context.Id
                             select d.Description).FirstOrDefault();
                    break;
            }
            return descr;
        }
    }
}