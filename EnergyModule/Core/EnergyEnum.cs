﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Core
{
    //tutti gli enumeratori inserirli qua
    public static class EnergyEnum
    {
        public enum TimeScale
        {
            Hour,
            Day,
            Month,
            Year
        }

        public enum ParamToRead
        {
            TotalActivePower,
            TotalActiveEnergyConsumed,
            TotalReactiveEnergyConsumed,
            TotalActiveEnergyProduced,
            TotalPowerFactor
        }

        public enum EnergyCarrier
        {
            Acqua,
            Gas,
            Energia
        }
    }
}