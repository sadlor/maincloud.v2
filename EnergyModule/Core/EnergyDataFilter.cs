﻿using MainCloudFramework.Models;
using MainCloudFramework.Web.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Core
{
    public class EnergyParameterFilter : List<string>
    {
        //private List<string> Config;

        public EnergyParameterFilter()
        {
            if (ApplicationSettingsHelper.GetConfiguration(ApplicationSettingsKey.EnergyParameterFilter) != null)
            {
                this.AddRange((List<string>)ApplicationSettingsHelper.GetConfiguration(ApplicationSettingsKey.EnergyParameterFilter));
            }
        }

        public EnergyParameterFilter(string applicationId)
        {
            if (ApplicationSettingsHelper.GetConfiguration(ApplicationSettingsKey.EnergyParameterFilter, applicationId) != null)
            {
                this.AddRange(((Newtonsoft.Json.Linq.JArray)ApplicationSettingsHelper.GetConfiguration(ApplicationSettingsKey.EnergyParameterFilter, applicationId)).ToObject<List<string>>());
            }
        }

        public bool Exist(string param)
        {
            return this.Exists(x => x == param);
        }

        //public List<string> GetConfig()
        //{
        //    return Config;
        //}

        public void AddFields(params string[] fields)
        {
            foreach (string field in fields)
            {
                this.Add(field);
            }
        }
    }
}