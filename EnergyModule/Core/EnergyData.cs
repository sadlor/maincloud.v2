﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Core
{
    public interface IEnergyData
    {
        string Code { get; set; }
        object Value { get; set; }
        object Min { get; set; }
        object Max { get; set; }
        string MeasureUnit { get; set; }
    }

    public class EnergyData : IEnergyData
    {
        public string Code { get; set; }
        public object Value { get; set; }
        public object Min { get; set; }
        public object Max { get; set; }
        public string MeasureUnit { get; set; }

        public EnergyData()
        { }

        public EnergyData(string code, object value, string measureUnit = "")
        {
            this.Code = code;
            this.Value = value;
            this.Min = value;
            this.Max = value;
            this.MeasureUnit = measureUnit;
        }

        public EnergyData(string code, object value, object min, object max, string measureUnit = "")
        {
            this.Code = code;
            this.Value = value;
            this.Min = min;
            this.Max = max;
            this.MeasureUnit = measureUnit;
        }
    }
}