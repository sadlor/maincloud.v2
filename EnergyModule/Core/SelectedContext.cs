﻿using AssetManagement.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Core
{
    /// <summary>
    /// Rappresenta la selezione del contesto di lettura dei dati (asset, deparment, plant)
    /// </summary>
    public class SelectedContext
    {
        public string Id { get; set; }
        public AssetManagementEnum.AnalyzerContext Type { get; set; }

        public SelectedContext(string id, AssetManagementEnum.AnalyzerContext type)
        {
            Id = id;
            Type = type;
        }
    }
}