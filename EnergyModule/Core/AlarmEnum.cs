﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Core
{
    public static class AlarmEnum
    {
        public enum AlarmContext
        {
            Gateway,
            Analyzer,
            Param
        }

        public enum AlarmDetails
        {
            LastFileReceived,
            ParamNameNotReceived,
            ParamNotReceivedNumber
        }
    }
}