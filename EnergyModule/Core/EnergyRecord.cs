﻿using EnergyModule.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Core
{
    public class EnergyRecord : List<EnergyData>
    {
        public EnergyRecord()
        {
        }

        /// <summary>
        /// get the min value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="param"></param>
        /// <returns></returns>
        public T GetMin<T>(string param)
        {
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            if (this.Count > 0)
            {
                if (this.Find(x => x.Code == param) != null)
                {
                    return serializer.ConvertToType<T>(this.Find(x => x.Code == param).Min);
                }
                else
                {
                    throw new EnergyDataException("Non esiste il parametro cercato");
                }
            }
            else
            {
                throw new EnergyDataException("Non esistono dati");
            }
        }

        /// <summary>
        /// get the max value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="param"></param>
        /// <returns></returns>
        public T GetMax<T>(string param)
        {
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            if (this.Count > 0)
            {
                if (this.Find(x => x.Code == param) != null)
                {
                    return serializer.ConvertToType<T>(this.Find(x => x.Code == param).Max);
                }
                else
                {
                    throw new EnergyDataException("Non esiste il parametro cercato");
                }
            }
            else
            {
                throw new EnergyDataException("Non esistono dati");
            }
        }

        public T GetData<T>(string param)  //decimal GetData
        {
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            if (this.Count > 0)
            {
                var ci = CultureInfo.InvariantCulture.Clone() as CultureInfo;
                ci.NumberFormat.NumberDecimalSeparator = ".";

                if (this.Find(x => x.Code == param) != null)
                {
                    return serializer.ConvertToType<T>(this.Find(x => x.Code == param).Value);
                }
                else
                {
                    //non esiste il parametro cercato
                    throw new EnergyDataException("Non esiste il parametro cercato");
                    //return serializer.ConvertToType<T>(null);
                }
            }
            else
            {
                //l'oggetto è vuoto
                throw new EnergyDataException("Non esistono dati");
                //return serializer.ConvertToType<T>(null);
            }
        }

        public decimal GetData(string param)
        {
            if (this.Count > 0)
            {
                if (this.Find(x => x.Code == param) != null)
                {
                    if (this.Find(x => x.Code == param).Value.GetType().Name == "String")
                    {
                        if ((string)this.Find(x => x.Code == param).Value == string.Empty)
                        {
                            return 0;
                        }
                        else
                        {
                            return Convert.ToDecimal(this.Find(x => x.Code == param).Value, CultureInfo.InvariantCulture);//decimal.Parse(s, ci);
                        }
                    }
                    else
                    {
                        return Convert.ToDecimal(this.Find(x => x.Code == param).Value, CultureInfo.InvariantCulture);//decimal.Parse(s, ci);
                    }
                }
                else
                {
                    //non esiste il parametro cercato
                    throw new EnergyDataException("Non esiste il parametro cercato");
                    //return serializer.ConvertToType<T>(null);
                }
            }
            else
            {
                //l'oggetto è vuoto
                return 0;
            }
        }

        public decimal GetMax(string param)
        {
            if (this.Count > 0)
            {
                if (this.Find(x => x.Code == param) != null)
                {
                    return Convert.ToDecimal(this.Find(x => x.Code == param).Max, CultureInfo.InvariantCulture);
                }
                else
                {
                    throw new EnergyDataException("Non esiste il parametro cercato");
                }
            }
            else
            {
                throw new EnergyDataException("Non esistono dati");
            }
        }

        public decimal GetMin(string param)
        {
            if (this.Count > 0)
            {
                if (this.Find(x => x.Code == param) != null)
                {
                    return Convert.ToDecimal(this.Find(x => x.Code == param).Min, CultureInfo.InvariantCulture);
                }
                else
                {
                    throw new EnergyDataException("Non esiste il parametro cercato");
                }
            }
            else
            {
                throw new EnergyDataException("Non esistono dati");
            }
        }

        public EnergyData GetEnergyData(string param)
        {
            if (this.Count > 0)
            {
                if (this.Find(x => x.Code == param) != null)
                {
                    return this.Find(x => x.Code == param);
                }
                else
                {
                    throw new EnergyDataException("Non esiste il parametro cercato");
                }
            }
            else
            {
                throw new EnergyDataException("Non esistono dati");
            }
        }

        public bool Exist(string param)
        {
            if (this.Count > 0)
            {
                if (this.Find(x => x.Code == param) != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        private static T ConvertValue<T, U>(U value) where U : IConvertible
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }
    }
}