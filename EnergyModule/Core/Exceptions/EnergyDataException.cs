﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Core.Exceptions
{
    public class EnergyDataException : Exception
    {
        public EnergyDataException()
        {
        }

        public EnergyDataException(string message) : base(message)
        {
        }

        public EnergyDataException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}