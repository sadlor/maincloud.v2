﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Core
{
    public class EMConstants
    {
        public const string SELECTED_ID_CONTEXT = "SelectedIdContext"; //id e tipo di scelta: asset, department, plant

        public const string START_ACTIVEENERGY_COUNTER = "StartActiveEnergyCounter";
        public const string START_REACTIVEENERGY_COUNTER = "StartReactiveEnergyCounter";
    }
}