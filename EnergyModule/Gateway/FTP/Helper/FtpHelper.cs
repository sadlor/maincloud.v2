﻿using AssetManagement.Models;
using CsvHelper;
using EnergyModule.Core;
using EnergyModule.Gateway.FTP.Model;
using EnergyModule.Models;
using FTPModule;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Gateway.FTP.Helper
{
    public class FtpHelper
    {
        public static EnergyRecord EnergyAverage(List<EnergyCSVRecord> rows)
        {
            EnergyRecord list = new EnergyRecord();
            if (rows.Count > 0)
            {
                List<decimal> cosfi = new List<decimal>();
                var paramNames = rows.Select(x => x.DeviceName).Distinct();
                foreach (string paramName in paramNames)
                {
                    decimal min, max, average;
                    switch (paramName)
                    {
                        case "TotalActiveEnergyConsumed":
                        case "TotalReactiveEnergyConsumed":
                            average = rows.Where(x => x.DeviceName == paramName).Select(x => x.Value).Max();
                            list.Add(new EnergyData(paramName, average, average, average));
                            break;
                        case "PowerFactor1":
                        case "PowerFactor2":
                        case "PowerFactor3":
                            cosfi.AddRange(rows.Where(x => x.DeviceName == paramName).Select(x => x.Value).ToList());
                            break;
                        default:
                            average = rows.Where(x => x.DeviceName == paramName).Select(x => x.Value).Average();
                            min = rows.Where(x => x.DeviceName == paramName).Select(x => x.Value).Min();
                            max = rows.Where(x => x.DeviceName == paramName).Select(x => x.Value).Max();
                            list.Add(new EnergyData(paramName, average, min, max));
                            break;
                    }
                }
                if (cosfi.Count > 0)
                {
                    list.Add(new EnergyData("PowerFactor", cosfi.Average(), cosfi.Min(), cosfi.Max()));
                }
            }
            return list;
        }

        public static string GetAnalyzerId(string serialCodeAnalyzer, string applicationId)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                try
                {
                    return db.Analyzers.Where(x => x.SerialCode == serialCodeAnalyzer && x.ApplicationId == applicationId).SingleOrDefault().Id;
                }
                catch (Exception ex)
                {
                    //associare i dati al serial code dell'analizzatore e non all'asset
                    throw new FTPException("Il misuratore con SerialNumber = '" + serialCodeAnalyzer + "' non è registrato!");
                }
            }
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static decimal ValueDetailFromFile(string fileName, string paramName, DateTime time)
        {
            try
            {
                StreamReader sr = new StreamReader(fileName);
                var csv = new CsvReader(sr);
                sr.Close();

                var rows = csv.GetRecords<EnergyCSVRecord>().ToList().OrderBy(x => x.Time);
                return rows.First(x => x.DeviceName == paramName && x.Time == time).Value;
            }
            catch (Exception ex)
            {
                //non esiste il dato al tempo richiesto
                throw new Exception("Non esiste il dettaglio cercato");
            }
        }

        public static decimal LastValueFromFile(string fileName, string paramName)
        {
            StreamReader sr = new StreamReader(fileName);
            var csv = new CsvReader(sr);
            sr.Close();

            var rows = csv.GetRecords<EnergyCSVRecord>().ToList().OrderByDescending(x => x.Time);
            foreach (EnergyCSVRecord row in rows)
            {
                if (row.DeviceName == paramName)
                {
                    return row.Value;  //sono in ordine decrescente, quindi la prima riga interessata è quella corretta
                }
            }
            return 0;
        }

        public static void UpdateLastUpdate(string analyzerId, string applicationId)
        {
            using (AssetManagementDbContext db = new AssetManagementDbContext())
            {
                try
                {
                    var analyzer = db.Analyzers.Where(x => x.ApplicationId == applicationId && x.Id == analyzerId).First();
                    analyzer.LastUpdate = DateTime.Now;
                    db.SaveChanges();
                }
                catch (InvalidOperationException ex)
                {
                    throw new FTPException("Aggiornamento LastUpdate: errore con analyzerId " + analyzerId + " - messaggio: " + ex.Message);
                }
                catch (Exception ex)
                {
                    throw new FTPException("Aggiornamento LastUpdate: Non esiste l'analizzatore con ID " + analyzerId);
                }
            }
        }
    }
}