﻿using EnergyModule.Gateway.FTP.Helper;
using EnergyModule.Gateway.FTP.Model;
using EnergyModule.Models;
using EnergyModule.Repositories;
using FTPModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Gateway.FTP.Parser
{
    public class EnergyParser : AbsWatcherParser<EnergyCSVRecord, EnergyCSVRecordMap>
    {
        private void UpdateDBFromFile(string fileName)
        {
            //da cancellare
            string IdApplication = null;
            string BasePath = null;
            /////

            ConsumptionStorage.ConsumptionStorage storage = new ConsumptionStorage.ConsumptionStorage();

            List<AnalyzerEnergyData> list = new List<AnalyzerEnergyData>();

            //utilizzare SortedDictionary al posto di OrderBy
            //List<CSVStructure> rows = Read(fileName).OrderBy(x => x.DeviceId && x.Time).ToList();
            List<EnergyCSVRecord> allRows = Read(fileName);
            var serialCodes = allRows.Select(x => x.DeviceId).Distinct();
            foreach (string serialCode in serialCodes)
            {
                try
                {
                    //SortedDictionary<DateTime, CSVStructure> orderedRows = new SortedDictionary<DateTime, CSVStructure>(allRows.Where(x => x.DeviceId == serialCode).ToDictionary(x => x.Time, x => x));
                    var orderedRows = allRows.Where(x => x.DeviceId == serialCode).OrderBy(x => x.Time);

                    //Elaboro 15 min per volta
                    for (int hour = 0; hour <= orderedRows.Last().Time.Hour; hour++)
                    {
                        int quarter = 0;
                        while (quarter <= 59)
                        {
                            //List<CSVStructure> rows = orderedRows.Where(x => x.Key.TimeOfDay.Hours == hour && x.Key.TimeOfDay.Minutes >= quarter && x.Key.TimeOfDay.Minutes <= quarter + 15).Select(x => x.Value).ToList();
                            List<EnergyCSVRecord> rows = orderedRows.Where(x => x.Time.TimeOfDay.Hours == hour && x.Time.TimeOfDay.Minutes >= quarter && x.Time.TimeOfDay.Minutes <= quarter + 15).ToList();
                            AnalyzerEnergyData ED = new AnalyzerEnergyData();
                            ED.AnalyzerId = FtpHelper.GetAnalyzerId(serialCode, IdApplication);
                            if (quarter + 15 == 60)
                            {
                                if (hour + 1 == 24)
                                {
                                    if (orderedRows.First().Time.Day + 1 > DateTime.DaysInMonth(orderedRows.First().Time.Year, orderedRows.First().Time.Month))
                                    {
                                        if (orderedRows.First().Time.Month + 1 == 13)
                                        {
                                            ED.TimeStamp = new DateTime(orderedRows.First().Time.Year + 1, 01, 01, 0, 0, 0); //rows.Max(x => x.Time).AddMinutes(-(rows.Max(x => x.Time).Minute % 15) + 15);
                                        }
                                        else
                                        {
                                            ED.TimeStamp = new DateTime(orderedRows.First().Time.Year, orderedRows.First().Time.Month + 1, 01, 0, 0, 0); //rows.Max(x => x.Time).AddMinutes(-(rows.Max(x => x.Time).Minute % 15) + 15);
                                        }
                                    }
                                    else
                                    {
                                        ED.TimeStamp = new DateTime(orderedRows.First().Time.Year, orderedRows.First().Time.Month, orderedRows.First().Time.Day + 1, 0, 0, 0); //rows.Max(x => x.Time).AddMinutes(-(rows.Max(x => x.Time).Minute % 15) + 15);
                                    }
                                }
                                else
                                {
                                    ED.TimeStamp = new DateTime(orderedRows.First().Time.Year, orderedRows.First().Time.Month, orderedRows.First().Time.Day, hour + 1, 0, 0); //rows.Max(x => x.Time).AddMinutes(-(rows.Max(x => x.Time).Minute % 15) + 15);
                                }
                            }
                            else
                            {
                                ED.TimeStamp = new DateTime(orderedRows.First().Time.Year, orderedRows.First().Time.Month, orderedRows.First().Time.Day, hour, quarter + 15, 0); //rows.Max(x => x.Time).AddMinutes(-(rows.Max(x => x.Time).Minute % 15) + 15);
                            }
                            ED.Data = JsonConvert.SerializeObject(FtpHelper.EnergyAverage(rows));
                            list.Add(ED);
                            quarter += 15;
                            if (rows.Count > 0 && rows.Last() == orderedRows.Last())
                            {
                                quarter = 60;
                            }
                        }
                    }

                    //Analyzer LastUpdate
                    FtpHelper.UpdateLastUpdate(FtpHelper.GetAnalyzerId(serialCode, IdApplication), IdApplication);
                }
                catch (FTPException ex)
                {
                    //log.Error("Watcher: " + BasePath + ex.Message);
                    //mandare una mail (o avvisare in qualche modo) con il messaggio ex.Message
                }
            }
            EnergyRepository repos = new EnergyRepository();
            //AnalyzerEnergyDataRepository repos = new AnalyzerEnergyDataRepository();
            repos.Update(list);

            /////update table dailyConsumption
            foreach (string serialCode in serialCodes)
            {
                try
                {
                    storage.DailyStoragePerThread(FtpHelper.GetAnalyzerId(serialCode, IdApplication), allRows[0].Time.Date, allRows[0].Time.Date, IdApplication);
                    if (allRows[0].Time.Date.Month != DateTime.Today.Month) //se non è il mese corrente aggiorno il mese aggregato corrispondente
                    {
                        storage.MonthlyStoragePerThread(
                            FtpHelper.GetAnalyzerId(serialCode, IdApplication),
                            new DateTime(allRows[0].Time.Date.Year, allRows[0].Time.Date.Month, 01),
                            new DateTime(allRows[0].Time.Date.Year, allRows[0].Time.Date.Month, DateTime.DaysInMonth(allRows[0].Time.Date.Year, allRows[0].Time.Date.Month)),
                            IdApplication);
                    }
                }
                catch (FTPException ex)
                {
                    //log.Error("Watcher: " + BasePath + ex.Message);
                }
                catch (Exception ex)
                {
                    //log.Error("Watcher: " + BasePath + ex.Message);
                }
            }
            /////end update
        }
    }
}