﻿using FTPModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Gateway.FTP.Model
{
    public class EnergyCSVRecord
    {
        public string DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string Timestamp { get; set; }
        public decimal Quality { get; set; }
        public decimal Value { get; set; }
        public string Alarm { get; set; }
        public DateTime Time { get; set; }
    }

    public class EnergyCSVRecordMap : CSVStructure<EnergyCSVRecord>
    {
        public EnergyCSVRecordMap()
        {
            Map(m => m.DeviceId).Name("deviceId");
            Map(m => m.DeviceName).Name("deviceName");
            Map(m => m.Timestamp).Name("timestamp");
            Map(m => m.Quality).Name("quality");
            Map(m => m.Value).Name("value");
            Map(m => m.Alarm).Name("alarm");
            Map(m => m.Time).Name("time");
        }
    }
}