﻿using FTPModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using EnergyModule.Gateway.FTP.Helper;
using CsvHelper;
using EnergyModule.Models;
using EnergyModule.Gateway.FTP.Model;
using Newtonsoft.Json;
using EnergyModule.Repositories;
using EnergyModule.Core;
using System.Globalization;
using EnergyModule.Gateway.FTP.Parser;

namespace EnergyModule.Gateway.FTP.Watcher
{
    public class FtpWatcher : WatcherJob
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly log4net.ILog emailLog = log4net.LogManager.GetLogger("EmailLogger");

        public FtpWatcher(string applicationId, string path) : base(applicationId, path)
        {
        }

        public override void OnChanged(object source, FileSystemEventArgs e)
        {
            try
            {
                string[] files = Directory.GetFiles(Path);
                foreach (string f in files)
                {
                    try
                    {
                        //verificare che il file non sia lockato
                        FileInfo file = new FileInfo(f);
                        if (!file.Exists)
                        {
                            throw new FileNotFoundException();
                        }
                        var startTimer = DateTime.Now;
                        var timeout = 60000;
                        while (FtpHelper.IsFileLocked(file))
                        {
                            if ((DateTime.Now - startTimer).TotalMilliseconds > timeout)
                            {
                                throw new Exception("Is file locked timeout");
                            }
                            Thread.Sleep(300);
                        }

                        if (file.Length == 0) //se il file è vuoto
                        {
                            File.Delete(f);
                            throw new FTPException(string.Format("Watcher: {0}. Il file {1} è vuoto", BasePath, file.Name));
                        }
                        //var rows = Read(f);
                        var parser = new EnergyParser();
                        var rows = parser.Read(f);
                        WriteDailyFile(rows);

                        File.Move(f, System.IO.Path.Combine(BasePath, "Elaborati", file.Name));
                        //File.Delete(f);

                        LastFileReceived = DateTime.Now;
                    }
                    catch (FileNotFoundException ex)
                    {
                        //non esiste il file
                    }
                    catch (CsvMissingFieldException ex)
                    {
                        log.Error("Impossible to read file " + e.FullPath + " in watcher " + BasePath, ex);
                    }
                    catch (FTPException ex)
                    {
                        log.Error("Impossibile elaborare il file", ex);
                        emailLog.Error("Impossibile elaborare il file", ex);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Generic error on file - Watcher: " + BasePath, ex);
                        emailLog.Error(string.Format("Watcher: {0} - Generic error on file.", BasePath), ex);
                    }
                }
                try
                {
                    ProcessFile();
                }
                catch (Exception ex)
                {
                    log.Error("Error on save into database in watcher " + BasePath, ex);
                }
            }
            catch (Exception ex)
            {
                log.Error("Generic error - Watcher: " + BasePath, ex);
            }
        }

        public override void OnError(object source, ErrorEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void ProcessFile()
        {
            //ogni quarto d'ora
            if ((LastFileReceived - LastFileProcess).TotalMilliseconds > 15 * 60000)
            {
                //string fileName = System.IO.Path.Combine(Path, "Output", DateTime.Today.Date.ToString("yyyyMMdd"));
                UpdateDBFromFile();

                LastFileProcess = DateTime.Now;
                FileUploadToDBList.Clear(); //azzero la lista dei file da elaborare sul DB
            }
        }

        private void UpdateDBFromFile()
        {
            //legge i files che sono da aggiornare e aggiorno il db
            foreach (string file in FileUploadToDBList)
            {
                UpdateDBFromFile(file);
            }

            //x debug
            //var files = new DirectoryInfo(System.IO.Path.Combine(BasePath, "Output")).GetFiles();//.Where(file => file.LastWriteTime.Date == DateTime.Today.Date);
            //foreach (var file in files)
            //{
            //    UpdateDBFromFile(file.FullName);
            //}
        }

        private void UpdateDBFromFile(string fileName)
        {
            ConsumptionStorage.ConsumptionStorage storage = new ConsumptionStorage.ConsumptionStorage();

            List<AnalyzerEnergyData> list = new List<AnalyzerEnergyData>();

            //utilizzare SortedDictionary al posto di OrderBy
            //List<CSVStructure> rows = Read(fileName).OrderBy(x => x.DeviceId && x.Time).ToList();

            var parser = new EnergyParser();
            List<EnergyCSVRecord> allRows = parser.Read(fileName);
            //List<EnergyCSVRecord> allRows = Read(fileName);
            var serialCodes = allRows.Select(x => x.DeviceId).Distinct();
            foreach (string serialCode in serialCodes)
            {
                try
                {
                    //SortedDictionary<DateTime, CSVStructure> orderedRows = new SortedDictionary<DateTime, CSVStructure>(allRows.Where(x => x.DeviceId == serialCode).ToDictionary(x => x.Time, x => x));
                    var orderedRows = allRows.Where(x => x.DeviceId == serialCode).OrderBy(x => x.Time);

                    //Elaboro 15 min per volta
                    for (int hour = 0; hour <= orderedRows.Last().Time.Hour; hour++)
                    {
                        int quarter = 0;
                        while (quarter <= 59)
                        {
                            //List<CSVStructure> rows = orderedRows.Where(x => x.Key.TimeOfDay.Hours == hour && x.Key.TimeOfDay.Minutes >= quarter && x.Key.TimeOfDay.Minutes <= quarter + 15).Select(x => x.Value).ToList();
                            List<EnergyCSVRecord> rows = orderedRows.Where(x => x.Time.TimeOfDay.Hours == hour && x.Time.TimeOfDay.Minutes >= quarter && x.Time.TimeOfDay.Minutes <= quarter + 15).ToList();
                            AnalyzerEnergyData ED = new AnalyzerEnergyData();
                            ED.AnalyzerId = FtpHelper.GetAnalyzerId(serialCode, IdApplication);
                            if (quarter + 15 == 60)
                            {
                                if (hour + 1 == 24)
                                {
                                    if (orderedRows.First().Time.Day + 1 > DateTime.DaysInMonth(orderedRows.First().Time.Year, orderedRows.First().Time.Month))
                                    {
                                        if (orderedRows.First().Time.Month + 1 == 13)
                                        {
                                            ED.TimeStamp = new DateTime(orderedRows.First().Time.Year + 1, 01, 01, 0, 0, 0); //rows.Max(x => x.Time).AddMinutes(-(rows.Max(x => x.Time).Minute % 15) + 15);
                                        }
                                        else
                                        {
                                            ED.TimeStamp = new DateTime(orderedRows.First().Time.Year, orderedRows.First().Time.Month + 1, 01, 0, 0, 0); //rows.Max(x => x.Time).AddMinutes(-(rows.Max(x => x.Time).Minute % 15) + 15);
                                        }
                                    }
                                    else
                                    {
                                        ED.TimeStamp = new DateTime(orderedRows.First().Time.Year, orderedRows.First().Time.Month, orderedRows.First().Time.Day + 1, 0, 0, 0); //rows.Max(x => x.Time).AddMinutes(-(rows.Max(x => x.Time).Minute % 15) + 15);
                                    }
                                }
                                else
                                {
                                    ED.TimeStamp = new DateTime(orderedRows.First().Time.Year, orderedRows.First().Time.Month, orderedRows.First().Time.Day, hour + 1, 0, 0); //rows.Max(x => x.Time).AddMinutes(-(rows.Max(x => x.Time).Minute % 15) + 15);
                                }
                            }
                            else
                            {
                                ED.TimeStamp = new DateTime(orderedRows.First().Time.Year, orderedRows.First().Time.Month, orderedRows.First().Time.Day, hour, quarter + 15, 0); //rows.Max(x => x.Time).AddMinutes(-(rows.Max(x => x.Time).Minute % 15) + 15);
                            }
                            ED.Data = JsonConvert.SerializeObject(FtpHelper.EnergyAverage(rows));
                            list.Add(ED);
                            quarter += 15;
                            if (rows.Count > 0 && rows.Last() == orderedRows.Last())
                            {
                                quarter = 60;
                            }
                        }
                    }

                    //Analyzer LastUpdate
                    FtpHelper.UpdateLastUpdate(FtpHelper.GetAnalyzerId(serialCode, IdApplication), IdApplication);
                }
                catch (FTPException ex)
                {
                    log.Error("Watcher: " + BasePath + ex.Message);
                    //mandare una mail (o avvisare in qualche modo) con il messaggio ex.Message
                }
            }
            EnergyRepository repos = new EnergyRepository();
            //AnalyzerEnergyDataRepository repos = new AnalyzerEnergyDataRepository();
            repos.Update(list);

            /////update table dailyConsumption
            foreach (string serialCode in serialCodes)
            {
                try
                {
                    storage.DailyStoragePerThread(FtpHelper.GetAnalyzerId(serialCode, IdApplication), allRows[0].Time.Date, allRows[0].Time.Date, IdApplication);
                    if (allRows[0].Time.Date.Month != DateTime.Today.Month) //se non è il mese corrente aggiorno il mese aggregato corrispondente
                    {
                        storage.MonthlyStoragePerThread(
                            FtpHelper.GetAnalyzerId(serialCode, IdApplication),
                            new DateTime(allRows[0].Time.Date.Year, allRows[0].Time.Date.Month, 01),
                            new DateTime(allRows[0].Time.Date.Year, allRows[0].Time.Date.Month, DateTime.DaysInMonth(allRows[0].Time.Date.Year, allRows[0].Time.Date.Month)),
                            IdApplication);
                    }
                }
                catch (FTPException ex)
                {
                    log.Error("Watcher: " + BasePath + ex.Message);
                }
                catch (Exception ex)
                {
                    log.Error("Watcher: " + BasePath + ex.Message);
                }
            }
            /////end update
        }

        private void WriteDailyFile(List<EnergyCSVRecord> rows)
        {
            EnergyParameterFilter filters = new EnergyParameterFilter(IdApplication);

            var dateList = rows.Select(x => x.Time.Date.ToString("yyyyMMdd")).Distinct();

            foreach (string date in dateList)
            {
                //string date = rows[0].Time.Date.ToString("yyyyMMdd");
                string fileName = System.IO.Path.Combine(BasePath, "Output", date + ".csv");  //nomeFile -> date
                bool writeHeader = false;
                if (!File.Exists(fileName))
                {
                    File.WriteAllText(fileName, ""); //File.WriteAllText(fileName, HeaderRow);
                    writeHeader = true;
                }
                using (StreamWriter sr = new StreamWriter(fileName, true))
                {
                    var csv = new CsvWriter(sr);
                    csv.Configuration.Delimiter = ";";
                    csv.Configuration.CultureInfo = CultureInfo.InvariantCulture;
                    csv.Configuration.RegisterClassMap<EnergyCSVRecordMap>();
                    if (writeHeader)
                    {
                        csv.WriteHeader<EnergyCSVRecord>();
                    }

                    var rowsInDate = rows.Where(x => x.Time.Date.ToString("yyyyMMdd") == date);
                    foreach (EnergyCSVRecord row in rowsInDate)
                    {
                        if (filters.Exist(row.DeviceName))
                        {
                            csv.WriteRecord<EnergyCSVRecord>(row);
                        }
                    }
                }

                //salvo i file da elaborare sul DB in una lista
                if (!FileUploadToDBList.Contains(fileName))
                {
                    FileUploadToDBList.Add(fileName);
                }
            }

            //UpdateDBFromFile(); //x debug

            //quando il file che leggo non è di oggi, modifico subito il db
            //if (date != DateTime.Today.Date.ToString("yyyyMMdd"))
            //{
            //    UpdateDBFromFile(fileName);
            //}
        }
    }
}