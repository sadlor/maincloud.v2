﻿using EnergyModule.Core;
using EnergyModule.Models;
using EnergyModule.Repositories;
using EnergyModule.Services;
using MainCloudFramework.Web.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.ConsumptionStorage
{
    /// <summary>
    /// Daily and monthly consumption kWh and cost storage
    /// </summary>
    public class ConsumptionStorage
    {
        private List<TableDataSource> ScheduleDataByDay(string analyzerId, DateTime start, DateTime end, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            EnergyDataService energyDataService = new EnergyDataService();
            //string analyzerId = energyDataService.GetAnalyzerId(SelectedContextId);
            end = end.AddDays(1);
            List<AnalyzerEnergyData> list = energyDataService.GetAnalyzerEnergyDataList(x => x.AnalyzerId == analyzerId && x.TimeStamp >= start && x.TimeStamp <= end);

            List<TableDataSource> dataSource = new List<TableDataSource>();
            if (list.Count > 0)
            {
                string paramName = "TotalActiveEnergyConsumed";

                CostService costService = new CostService();
                List<Cost> tariffe = costService.GetRatesList(applicationId, start, end);

                for (DateTime day = start; day < end; day = day.AddDays(1))
                {
                    List<decimal> F1Items = new List<decimal>();
                    List<decimal> F2Items = new List<decimal>();
                    List<decimal> F3Items = new List<decimal>();

                    List<AnalyzerEnergyData> dailyList = list.Where(x => x.TimeStamp.Date == day.Date || (x.TimeStamp.Date == day.Date.AddDays(1) && x.TimeStamp.Hour == 0 && x.TimeStamp.Minute == 00)).ToList();
                    if (dailyList.Count > 0)
                    {
                        SortedDictionary<DateTime, EnergyRecord> erList = new SortedDictionary<DateTime, EnergyRecord>();
                        foreach (var item in dailyList)
                        {
                            try
                            {
                                EnergyRecord e = JsonConvert.DeserializeObject<EnergyRecord>(item.Data);
                                erList[item.TimeStamp] = e;
                            }
                            catch (Exception ex)
                            { }
                        }

                        TableDataSource dataSourceItem = new TableDataSource();

                        EnergyRecord first = erList.FirstOrDefault(x => x.Value.Count > 0 && x.Value.Exist(paramName) && x.Value.GetData(paramName) > 0).Value;

                        if (first != null)
                        {
                            int i = 0;
                            while (i < erList.Count)
                            {
                                Fascia f = GetFascia(erList.ElementAt(i).Key);
                                List<EnergyRecord> subList = erList.Values.ToList().GetRange(i, erList.Count - i);
                                EnergyRecord firstkWh = subList.FirstOrDefault(x => x.Count > 0 && x.Exist(paramName) && x.GetData(paramName) > 0);
                                KeyValuePair<DateTime, EnergyRecord> item = new KeyValuePair<DateTime, EnergyRecord>();
                                while (i < erList.Count && f == GetFascia(erList.ElementAt(i).Key))
                                {
                                    i++;
                                    if (erList.ElementAt(i - 1).Value.Count > 0 && erList.ElementAt(i - 1).Value.Exist(paramName) && erList.ElementAt(i - 1).Value.GetData(paramName) > 0)
                                    {
                                        item = erList.ElementAt(i - 1);
                                    }
                                }
                                switch (f)
                                {
                                    case Fascia.F1:
                                        try
                                        {
                                            F1Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                        }
                                        catch (Exception ex)
                                        { }
                                        break;
                                    case Fascia.F2:
                                        try
                                        {
                                            F2Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                        }
                                        catch (Exception ex)
                                        { }
                                        break;
                                    case Fascia.F3:
                                        try
                                        {
                                            F3Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                        }
                                        catch (Exception ex)
                                        { }
                                        break;
                                }
                            }
                            dataSourceItem.kWh = erList.Where(x => x.Value.Count > 0 && x.Value.Exist(paramName)).Max(x => x.Value.GetData(paramName)) - first.GetData(paramName);
                        }
                        else
                        {
                            dataSourceItem.kWh = 0;
                        }

                        if (F1Items.Count > 0)
                        {
                            dataSourceItem.kWhF1 = F1Items.Sum();
                            if (tariffe.Count > 0)
                            {
                                dataSourceItem.F1 = Math.Round(F1Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F1, 2);
                            }
                        }
                        else
                        {
                            dataSourceItem.kWhF1 = 0;
                            dataSourceItem.F1 = 0;
                        }
                        if (F2Items.Count > 0)
                        {
                            dataSourceItem.kWhF2 = F2Items.Sum();
                            if (tariffe.Count > 0)
                            {
                                dataSourceItem.F2 = Math.Round(F2Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F2, 2);
                            }
                        }
                        else
                        {
                            dataSourceItem.kWhF2 = 0;
                            dataSourceItem.F2 = 0;
                        }
                        if (F3Items.Count > 0)
                        {
                            dataSourceItem.kWhF3 = F3Items.Sum();
                            if (tariffe.Count > 0)
                            {
                                dataSourceItem.F3 = Math.Round(F3Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F3, 2);
                            }
                        }
                        else
                        {
                            dataSourceItem.kWhF3 = 0;
                            dataSourceItem.F3 = 0;
                        }

                        dataSourceItem.Date = day;
                        dataSource.Add(dataSourceItem);
                    }
                }
            }
            return dataSource;
        }

        public void MonthlyStorage(string analyzerId, DateTime start, DateTime end, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            List<TableDataSource> dataSource = ScheduleDataByDay(analyzerId, start, end, applicationId);

            MonthlyConsumption mc = new MonthlyConsumption(analyzerId, applicationId);
            mc.Date = start;
            mc.CostF1 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.F1);
            mc.CostF2 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.F2);
            mc.CostF3 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.F3);
            mc.kWh = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.kWh);
            mc.kWhF1 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.kWhF1);
            mc.kWhF2 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.kWhF2);
            mc.kWhF3 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.kWhF3);
            ConsumptionMonthlyStorageRepository repository = new ConsumptionMonthlyStorageRepository();
            try
            {
                MonthlyConsumption toUpdate = repository.ReadAll(x => x.AnalyzerId == mc.AnalyzerId && x.Date == mc.Date && x.ApplicationId == mc.ApplicationId).First();
                toUpdate.kWh = mc.kWh;
                toUpdate.kWhF1 = mc.kWhF1;
                toUpdate.kWhF2 = mc.kWhF2;
                toUpdate.kWhF3 = mc.kWhF3;
                toUpdate.CostF1 = mc.CostF1;
                toUpdate.CostF2 = mc.CostF2;
                toUpdate.CostF3 = mc.CostF3;
                repository.Update(toUpdate);
            }
            catch (InvalidOperationException ex)
            {
                repository.Insert(mc);
            }
            repository.SaveChanges();
        }

        public void DailyStorage(string analyzerId, DateTime start, DateTime end)
        {
            string applicationId = MultiTenantsHelper.ApplicationId;

            List<TableDataSource> dataSource = ScheduleDataByDay(analyzerId, start, end, applicationId);

            ConsumptionDailyStorageRepository repository = new ConsumptionDailyStorageRepository();
            foreach (var item in dataSource)
            {
                try
                {
                    DailyConsumption toUpdate = repository.ReadAll(x => x.AnalyzerId == analyzerId && x.Date == item.Date && x.ApplicationId == applicationId).First();
                    toUpdate.kWh = item.kWh;
                    toUpdate.kWhF1 = item.kWhF1;
                    toUpdate.kWhF2 = item.kWhF2;
                    toUpdate.kWhF3 = item.kWhF3;
                    toUpdate.CostF1 = item.F1;
                    toUpdate.CostF2 = item.F2;
                    toUpdate.CostF3 = item.F3;
                    repository.Update(toUpdate);
                }
                catch (InvalidOperationException ex)
                {
                    DailyConsumption toInsert = new DailyConsumption(analyzerId, applicationId);
                    toInsert.Date = item.Date;
                    toInsert.kWh = item.kWh;
                    toInsert.kWhF1 = item.kWhF1;
                    toInsert.kWhF2 = item.kWhF2;
                    toInsert.kWhF3 = item.kWhF3;
                    toInsert.CostF1 = item.F1;
                    toInsert.CostF2 = item.F2;
                    toInsert.CostF3 = item.F3;
                    repository.Insert(toInsert);
                }
            }
            repository.SaveChanges();
        }

        #region per thread
        public void DailyStoragePerThread(string analyzerId, DateTime start, DateTime end, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            List<TableDataSource> dataSource = ScheduleDataByDayPerThread(analyzerId, start, end, applicationId);

            using (EnergyDbContext db = new EnergyDbContext())
            {
                foreach (var item in dataSource)
                {
                    try
                    {
                        DailyConsumption toUpdate = db.DailyConsumptions.Where(x => x.AnalyzerId == analyzerId && x.Date == item.Date && x.ApplicationId == applicationId).First();
                        toUpdate.kWh = item.kWh;
                        toUpdate.kWhF1 = item.kWhF1;
                        toUpdate.kWhF2 = item.kWhF2;
                        toUpdate.kWhF3 = item.kWhF3;
                        toUpdate.CostF1 = item.F1;
                        toUpdate.CostF2 = item.F2;
                        toUpdate.CostF3 = item.F3;
                        db.Entry(toUpdate).State = System.Data.Entity.EntityState.Modified;
                    }
                    catch (InvalidOperationException ex)
                    {
                        DailyConsumption toInsert = new DailyConsumption(analyzerId, applicationId);
                        toInsert.Date = item.Date;
                        toInsert.kWh = item.kWh;
                        toInsert.kWhF1 = item.kWhF1;
                        toInsert.kWhF2 = item.kWhF2;
                        toInsert.kWhF3 = item.kWhF3;
                        toInsert.CostF1 = item.F1;
                        toInsert.CostF2 = item.F2;
                        toInsert.CostF3 = item.F3;
                        db.Set<DailyConsumption>().Add(toInsert);
                    }
                }
                db.SaveChanges();
            }
        }

        public void MonthlyStoragePerThread(string analyzerId, DateTime start, DateTime end, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            //List<TableDataSource> dataSource = ScheduleDataByDayPerThread(analyzerId, start, end, applicationId);
            List<DailyConsumption> dataSource = GetDailyListPerThread(analyzerId, start, end);


            MonthlyConsumption mc = new MonthlyConsumption(analyzerId, applicationId);
            mc.Date = start;
            mc.CostF1 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.CostF1);
            mc.CostF2 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.CostF2);
            mc.CostF3 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.CostF3);
            mc.kWh = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.kWh);
            mc.kWhF1 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.kWhF1);
            mc.kWhF2 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.kWhF2);
            mc.kWhF3 = dataSource.Where(x => x.Date.Month == start.Month && x.Date.Year == start.Year).Sum(x => x.kWhF3);
            using (EnergyDbContext db = new EnergyDbContext())
            {
                try
                {
                    MonthlyConsumption toUpdate = db.MonthlyConsumptions.Where(x => x.AnalyzerId == mc.AnalyzerId && x.Date == mc.Date && x.ApplicationId == mc.ApplicationId).First();
                    toUpdate.kWh = mc.kWh;
                    toUpdate.kWhF1 = mc.kWhF1;
                    toUpdate.kWhF2 = mc.kWhF2;
                    toUpdate.kWhF3 = mc.kWhF3;
                    toUpdate.CostF1 = mc.CostF1;
                    toUpdate.CostF2 = mc.CostF2;
                    toUpdate.CostF3 = mc.CostF3;
                    db.Entry(toUpdate).State = System.Data.Entity.EntityState.Modified;
                }
                catch (InvalidOperationException ex)
                {
                    db.Set<MonthlyConsumption>().Add(mc);
                }
                db.SaveChanges();
            }
        }

        private List<TableDataSource> ScheduleDataByDayPerThread(string analyzerId, DateTime start, DateTime end, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            EnergyDataService energyDataService = new EnergyDataService();
            //string analyzerId = energyDataService.GetAnalyzerId(SelectedContextId);
            end = end.AddDays(1);

            List<AnalyzerEnergyData> list = new List<AnalyzerEnergyData>();
            using (EnergyDbContext db = new EnergyDbContext())
            {
                list = db.AnalyzerEnergyDatas.Where(x => x.AnalyzerId == analyzerId && x.TimeStamp >= start && x.TimeStamp <= end).OrderBy(x => x.TimeStamp).ToList();
            }

            if (list.Count > 0)
            {
                string paramName = "TotalActiveEnergyConsumed";

                CostService costService = new CostService();
                List<Cost> tariffe = new List<Cost>();
                using (EnergyDbContext db = new EnergyDbContext())
                {
                    if (db.Costs.Where(x => x.ApplicationId == applicationId).Count() > 0)
                    {
                        DateTime maxDate;
                        try
                        {
                            maxDate = db.Costs.Where(x => x.ApplicationId == applicationId && x.Date <= start).Max(x => x.Date);
                        }
                        catch (InvalidOperationException ex)
                        {
                            maxDate = db.Costs.Where(x => x.ApplicationId == applicationId && x.Date >= start).Min(x => x.Date);
                        }
                        tariffe = db.Costs.Where(x => x.ApplicationId == applicationId && x.Date >= maxDate && x.Date < end).OrderBy(x => x.Date).ToList();
                    }
                }

                List<TableDataSource> dataSource = new List<TableDataSource>();

                for (DateTime day = start; day < end; day = day.AddDays(1))
                {
                    List<decimal> F1Items = new List<decimal>();
                    List<decimal> F2Items = new List<decimal>();
                    List<decimal> F3Items = new List<decimal>();

                    List<AnalyzerEnergyData> dailyList = list.Where(x => x.TimeStamp.Date == day.Date || (x.TimeStamp.Date == day.Date.AddDays(1) && x.TimeStamp.Hour == 0 && x.TimeStamp.Minute == 00)).ToList();
                    if (dailyList.Count > 0)
                    {
                        SortedDictionary<DateTime, EnergyRecord> erList = new SortedDictionary<DateTime, EnergyRecord>();
                        foreach (var item in dailyList)
                        {
                            try
                            {
                                EnergyRecord e = JsonConvert.DeserializeObject<EnergyRecord>(item.Data);
                                erList[item.TimeStamp] = e;
                            }
                            catch (Exception ex)
                            { }
                        }

                        TableDataSource dataSourceItem = new TableDataSource();

                        EnergyRecord first = erList.FirstOrDefault(x => x.Value.Count > 0 && x.Value.Exist(paramName) && x.Value.GetData(paramName) > 0).Value;

                        if (first != null)
                        {
                            int i = 0;
                            while (i < erList.Count)
                            {
                                Fascia f = GetFascia(erList.ElementAt(i).Key);
                                List<EnergyRecord> subList = erList.Values.ToList().GetRange(i, erList.Count - i);
                                EnergyRecord firstkWh = subList.FirstOrDefault(x => x.Count > 0 && x.Exist(paramName) && x.GetData(paramName) > 0);
                                KeyValuePair<DateTime, EnergyRecord> item = new KeyValuePair<DateTime, EnergyRecord>();
                                while (i < erList.Count && f == GetFascia(erList.ElementAt(i).Key))
                                {
                                    i++;
                                    if (erList.ElementAt(i - 1).Value.Count > 0 && erList.ElementAt(i - 1).Value.Exist(paramName) && erList.ElementAt(i - 1).Value.GetData(paramName) > 0)
                                    {
                                        item = erList.ElementAt(i - 1);
                                    }
                                }
                                switch (f)
                                {
                                    case Fascia.F1:
                                        try
                                        {
                                            F1Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                        }
                                        catch (Exception ex)
                                        { }
                                        break;
                                    case Fascia.F2:
                                        try
                                        {
                                            F2Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                        }
                                        catch (Exception ex)
                                        { }
                                        break;
                                    case Fascia.F3:
                                        try
                                        {
                                            F3Items.Add(item.Value.GetData<decimal>(paramName) - firstkWh.GetData<decimal>(paramName));
                                        }
                                        catch (Exception ex)
                                        { }
                                        break;
                                }
                            }
                            dataSourceItem.kWh = erList.Where(x => x.Value.Count > 0 && x.Value.Exist(paramName)).Max(x => x.Value.GetData(paramName)) - first.GetData(paramName);
                        }
                        else
                        {
                            dataSourceItem.kWh = 0;
                        }

                        if (F1Items.Count > 0)
                        {
                            dataSourceItem.kWhF1 = F1Items.Sum();
                            if (tariffe.Count > 0)
                            {
                                dataSourceItem.F1 = Math.Round(F1Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F1, 2);
                            }
                        }
                        else
                        {
                            dataSourceItem.kWhF1 = 0;
                            dataSourceItem.F1 = 0;
                        }
                        if (F2Items.Count > 0)
                        {
                            dataSourceItem.kWhF2 = F2Items.Sum();
                            if (tariffe.Count > 0)
                            {
                                dataSourceItem.F2 = Math.Round(F2Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F2, 2);
                            }
                        }
                        else
                        {
                            dataSourceItem.kWhF2 = 0;
                            dataSourceItem.F2 = 0;
                        }
                        if (F3Items.Count > 0)
                        {
                            dataSourceItem.kWhF3 = F3Items.Sum();
                            if (tariffe.Count > 0)
                            {
                                dataSourceItem.F3 = Math.Round(F3Items.Sum() * tariffe.Last(x => x.Date.Date <= day.Date).F3, 2);
                            }
                        }
                        else
                        {
                            dataSourceItem.kWhF3 = 0;
                            dataSourceItem.F3 = 0;
                        }

                        dataSourceItem.Date = day;
                        dataSource.Add(dataSourceItem);
                    }
                }
                return dataSource;
            }
            return null;
        }

        //public void CompleteUpdatePastMonths(IEnumerable<string> serialCodeList, string applicationId)
        //{
        //    using (EnergyDbContext db = new EnergyDbContext())
        //    {
        //        foreach (string serialCode in serialCodeList)
        //        {
        //            string analyzerId = Helpers.FtpHelper.GetAnalyzerId(serialCode, applicationId);
        //            try
        //            {
        //                db.MonthlyConsumptions.Where(x => x.AnalyzerId == analyzerId && x.ApplicationId == applicationId).OrderBy(x => x.Date).Last();




        //            }
        //            catch (InvalidOperationException ex)
        //            {
        //                //non ci sono dati per quell'analizzatore
        //            }
        //        }
        //    }
        //}

        public List<DailyConsumption> GetDailyListPerThread(string analyzerId, DateTime start, DateTime end)
        {
            using (EnergyDbContext db = new EnergyDbContext())
            {
                return db.DailyConsumptions.Where(x => x.AnalyzerId == analyzerId && x.Date >= start && x.Date <= end).ToList();
            }
        }
        #endregion

        public List<MonthlyConsumption> GetList(string analyzerId, DateTime start, DateTime end)
        {
            ConsumptionMonthlyStorageRepository repository = new ConsumptionMonthlyStorageRepository();
            return repository.ReadAll(x => x.AnalyzerId == analyzerId && x.Date >= start && x.Date < end).OrderBy(x => x.Date).ToList();
        }

        public MonthlyConsumption GetTotalizerCostConsumption(string analyzerId, DateTime start, DateTime end)
        {
            List<MonthlyConsumption> list = GetList(analyzerId, start, end);
            MonthlyConsumption returnValue = new MonthlyConsumption();
            returnValue.kWh = list.Sum(x => x.kWh);
            returnValue.kWhF1 = list.Sum(x => x.kWhF1);
            returnValue.kWhF2 = list.Sum(x => x.kWhF2);
            returnValue.kWhF3 = list.Sum(x => x.kWhF3);
            returnValue.CostF1 = list.Sum(x => x.CostF1);
            returnValue.CostF2 = list.Sum(x => x.CostF2);
            returnValue.CostF3 = list.Sum(x => x.CostF3);
            return returnValue;
        }

        struct TableDataSource
        {
            public DateTime Date { get; set; }
            public decimal kWh { get; set; }
            public decimal kWhF1 { get; set; }
            public decimal kWhF2 { get; set; }
            public decimal kWhF3 { get; set; }
            public decimal F1 { get; set; }
            public decimal F2 { get; set; }
            public decimal F3 { get; set; }
        }

        private enum Fascia
        {
            F1,
            F2,
            F3
        }

        private Fascia GetFascia(DateTime time)
        {
            if (time.DayOfWeek == DayOfWeek.Sunday)
            {
                return Fascia.F3;
            }
            else
            {
                if (time.DayOfWeek == DayOfWeek.Saturday)
                {
                    if ((time.Hour >= 7 && time.Hour < 23) || (time.Hour == 23 && time.Minute == 00))
                    {
                        return Fascia.F2;
                    }
                    else
                    {
                        return Fascia.F3;
                    }
                }
                else
                {
                    if ((time.Hour >= 8 && time.Hour < 19) || (time.Hour == 19 && time.Minute == 00))
                    {
                        return Fascia.F1;
                    }
                    else
                    {
                        if ((time.Hour >= 7 && time.Hour < 8) || (time.Hour == 8 && time.Minute == 00) || (time.Hour >= 19 && time.Hour < 23) || (time.Hour == 23 && time.Minute == 00))
                        {
                            return Fascia.F2;
                        }
                        else
                        {
                            return Fascia.F3;
                        }
                    }
                }
            }
        }
    }
}