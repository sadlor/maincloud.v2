namespace EnergyModule.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AlarmReadDatas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        AssociatedId = c.String(),
                        ApplicationId = c.String(),
                        AlarmId = c.String(maxLength: 128),
                        IsActive = c.Boolean(nullable: false),
                        Details = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Alarms", t => t.AlarmId)
                .Index(t => t.AlarmId);
            
            CreateTable(
                "dbo.Alarms",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Context = c.String(),
                        ApplicationId = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AnalyzerEnergyDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AnalyzerId = c.String(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        Data = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Costs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Monorario = c.Decimal(nullable: false, precision: 18, scale: 6),
                        F1 = c.Decimal(nullable: false, precision: 18, scale: 6),
                        F2 = c.Decimal(nullable: false, precision: 18, scale: 6),
                        F3 = c.Decimal(nullable: false, precision: 18, scale: 6),
                        Config = c.String(),
                        ApplicationId = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DailyConsumptions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AnalyzerId = c.String(nullable: false),
                        ApplicationId = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        kWh = c.Decimal(nullable: false, precision: 18, scale: 2),
                        kWhF1 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        kWhF2 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        kWhF3 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostF1 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostF2 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostF3 = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Thread = c.String(),
                        Level = c.String(),
                        Logger = c.String(),
                        Message = c.String(),
                        Exception = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MonthlyConsumptions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AnalyzerId = c.String(nullable: false),
                        ApplicationId = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        kWh = c.Decimal(nullable: false, precision: 18, scale: 2),
                        kWhF1 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        kWhF2 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        kWhF3 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostF1 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostF2 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostF3 = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AlarmReadDatas", "AlarmId", "dbo.Alarms");
            DropIndex("dbo.AlarmReadDatas", new[] { "AlarmId" });
            DropTable("dbo.MonthlyConsumptions");
            DropTable("dbo.Logs");
            DropTable("dbo.DailyConsumptions");
            DropTable("dbo.Costs");
            DropTable("dbo.AnalyzerEnergyDatas");
            DropTable("dbo.Alarms");
            DropTable("dbo.AlarmReadDatas");
        }
    }
}
