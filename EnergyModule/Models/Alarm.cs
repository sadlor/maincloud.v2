﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyModule.Models
{
    /// <summary>
    /// Gestione allarmi generici
    /// </summary>
    public class Alarm
    {
        public Alarm()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        /// <summary>
        /// usare il tipo AlarmEnum.AlarmContext
        /// </summary>
        public string Context { get; set; }

        public object Threshold { get; set; }
        public string ApplicationId { get; set; }
        public string Description { get; set; }
    }
}