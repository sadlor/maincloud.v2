﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyModule.Models
{
    public class AlarmEnergyData
    {
        public AlarmEnergyData()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }
        public string AssociatedId { get; set; }
        public decimal Threshold { get; set; }
        public string ApplicationId { get; set; }

        /// <summary>
        /// Associazione: analyzer/gateway
        /// </summary>
        public string Config { get; set; }
    }
}