﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Models
{
    public class EnergyDbContext : DbContext
    {
        public DbSet<AnalyzerEnergyData> AnalyzerEnergyDatas { get; set; }
        public DbSet<Cost> Costs { get; set; }
        public DbSet<Alarm> Alarms { get; set; }
        public DbSet<AlarmReadData> AlarmReadDatas { get; set; }
        public DbSet<DailyConsumption> DailyConsumptions { get; set; }
        public DbSet<MonthlyConsumption> MonthlyConsumptions { get; set; }
        public DbSet<Log> Logs { get; set; }

        public EnergyDbContext()
            : base("MainEnergyConnection")
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new EnergyDbInitializer());
        }

        //public EnergyDbContext(string connectionString) : base(connectionString)
        //{
        //    Database.SetInitializer(new EnergyDbInitializer());
        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cost>().Property(x => x.F1).HasPrecision(18, 6);
            modelBuilder.Entity<Cost>().Property(x => x.F2).HasPrecision(18, 6);
            modelBuilder.Entity<Cost>().Property(x => x.F3).HasPrecision(18, 6);
            modelBuilder.Entity<Cost>().Property(x => x.Monorario).HasPrecision(18, 6);

            base.OnModelCreating(modelBuilder); // I had removed this
                                                /// Rest of on model creating here.
        }

        public static EnergyDbContext Create()
        {
            return new EnergyDbContext();
        }
    }
}