﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyModule.Models
{
    public class AnalyzerEnergyData
    {
        public AnalyzerEnergyData()
        {
            //TimeStamp = DateTime.Now;
        }

        public AnalyzerEnergyData(string analyzerId)
        {
            //TimeStamp = DateTime.Now;
            AnalyzerId = analyzerId;
        }

        [Key]
        [Required]
        [Column(Order = 1)]
        public int Id { get; set; }


        [Required]
        public string AnalyzerId { get; set; }

        [Required]
        public DateTime TimeStamp { get; set; }

        [Required]
        public string Data { get; set; }

        //[ForeignKey("AnalyzerId")]
        //public virtual Analyzer Analyzer { get; set; }
    }
}