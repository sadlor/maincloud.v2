﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyModule.Models
{
    public class Cost
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public int Id { get; set; }

        public DateTime Date { get; set; }
        public decimal Monorario { get; set; }
        public decimal F1 { get; set; }
        public decimal F2 { get; set; }
        public decimal F3 { get; set; }
        public string Config { get; set; }

        [Required]
        public string ApplicationId { get; set; }
    }
}