﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyModule.Models
{
    public class EnergyConsumption
    {
        public EnergyConsumption()
        { }

        public EnergyConsumption(string analyzerId, string applicationId)
        {
            AnalyzerId = analyzerId;
            ApplicationId = applicationId;
        }

        public virtual string AnalyzerId { get; set; }

        public DateTime Date { get; set; }
        public decimal kWh { get; set; }
        public decimal kWhF1 { get; set; }
        public decimal kWhF2 { get; set; }
        public decimal kWhF3 { get; set; }
        //public decimal TotCost { get; set; }
        public decimal CostF1 { get; set; }
        public decimal CostF2 { get; set; }
        public decimal CostF3 { get; set; }

        public virtual string ApplicationId { get; set; }
    }
}