﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace EnergyModule.Models
{
    public class EnergyDbInitializer : CreateDatabaseIfNotExists<EnergyDbContext>
    {
        protected override void Seed(EnergyDbContext context)
        {
            //InitializeDemoData(context, "bc6058d2-4f74-4c62-b023-6ab4132e6f2f", "Demo");
            base.Seed(context);
        }

        //public static void InitializeDemoData(EnergyDbContext db, string applicationId)//, string applicationName)
        //{
        //}
    }
}