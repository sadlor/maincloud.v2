﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyModule.Models
{
    /// <summary>
    /// Log allarmi lettura dati FTP
    /// </summary>
    public class AlarmReadData
    {
        public AlarmReadData()
        {
            Id = Guid.NewGuid().ToString();
            IsActive = true;
        }

        public AlarmReadData(string applicationId)
        {
            Id = Guid.NewGuid().ToString();
            IsActive = true;
            ApplicationId = applicationId;
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }
        public string AssociatedId { get; set; }
        public string ApplicationId { get; set; }
        public string AlarmId { get; set; }

        /// <summary>
        /// nel momento in cui viene creato l'allarme è true, quando l'allarme supera la soglia o è chiuso va a false
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary> 
        /// Dettagli relativi all'allarme (usare il tipo Dictionary<AlarmEnum.AlarmDetails, object>)
        /// </summary>
        public string Details { get; set; }

        [ForeignKey("AlarmId")]
        public virtual Alarm Alarm { get; set; }
    }
}