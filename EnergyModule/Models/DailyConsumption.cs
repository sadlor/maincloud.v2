﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyModule.Models
{
    public class DailyConsumption : EnergyConsumption
    {
        public DailyConsumption()
            : base()
        { }

        public DailyConsumption(string analyzerId, string applicationId)
            : base(analyzerId, applicationId)
        {
            AnalyzerId = analyzerId;
            ApplicationId = applicationId;
        }

        [Key]
        [Required]
        [Column(Order = 1)]
        public long Id { get; set; }

        [Required]
        override public string AnalyzerId { get; set; }

        //public DateTime Date { get; set; }
        //public decimal kWh { get; set; }
        //public decimal kWhF1 { get; set; }
        //public decimal kWhF2 { get; set; }
        //public decimal kWhF3 { get; set; }
        ////public decimal TotCost { get; set; }
        //public decimal CostF1 { get; set; }
        //public decimal CostF2 { get; set; }
        //public decimal CostF3 { get; set; }

        [Required]
        override public string ApplicationId { get; set; }
    }
}