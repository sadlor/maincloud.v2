﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentisModule.Helper
{
    public class Helper
    {
        public static string CreateBatchCode(string materialCode)
        {
            string newBatchCode = string.Empty;
            string connectionString = ConfigurationManager.ConnectionStrings["FluentisConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlTransaction trans = conn.BeginTransaction();
                string sql = "declare @ID_Art int " +
                             "select @ID_ART = MGAA_ID from [dbo].[MG_AnaArt] where MGAA_matricola = '" + materialCode + "' " +
                             "declare @Tipo int " +
                             "select @Tipo = MGAA_MGTC_Id from [dbo].[MG_AnaArt] where MGAA_ID = @ID_ART " +
                             "declare @Start datetime " +
                             "select @Start = GETDATE() " +
                             "declare @CodLotto nvarchar(50) " +
                             "exec [dbo].[MG_CreateCodiceLotto] " +
                               "@IdArt = @ID_ART " +
                             ", @IdVar = 0 " +
                             ", @TipoCode = @Tipo " +
                             ", @TipoLotto = NULL " +
                             ", @DataInizio = @Start " +
                             ", @DataScadenza = NULL " +
                             ", @CodiceLotto = @param output " +
                             ", @FornitoreId = NULL " +
                             ", @BANum = NULL " +
                             ", @FANum = NULL " +
                             ", @LottoForn = NULL " +
                             ", @DVI = NULL;";

                SqlCommand cmd = new SqlCommand(sql, conn, trans);

                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                // set up the parameters
                cmd.Parameters.AddWithValue("@param", DbType.String).Direction = ParameterDirection.Output;
                try
                {
                    cmd.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                }
                finally
                {
                    // read output value from @param
                    newBatchCode = cmd.Parameters["@param"].Value.ToString();
                    conn.Close();
                }
            }
            return newBatchCode;
        }
    }
}