﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceModule.Driver
{
    /// <summary>
    /// Interfaccia per tutti i tipi di dispositivi per la raccolta dati
    /// </summary>
    public interface IDevice
    {
        string Id { get; set; }
        string Type { get; set; }
        string Producer { get; set; }
        string SerialNumber { get; set; }
        string Description { get; set; }
        string NetworkAddress { get; set; }
    }
}