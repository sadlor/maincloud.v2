﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceModule.Driver
{
    public enum TypeOfDevice
    {
        TMold,
        TouchTag,
        Z10DIN
    }

    public enum DeviceConfigParam
    {
        CycleNum,
        Acceleration,
        Timeout,
        Axis,
        Frequency
    }
}