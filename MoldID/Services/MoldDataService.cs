﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MoldID.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoldID.Services
{
    public class MoldDataService : BaseService<MoldDbContext>
    {
        public int GetTotalShots(string moldId)
        {
            try
            {
                return DBContext.MoldDatas.Where(x => x.TagId == moldId).Max(x => x.TotalShots);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int GetCurrentShots(string moldId)
        {
            try
            {
                return DBContext.MoldDatas.Where(x => x.TagId == moldId).Max(x => x.CurrentShots);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public decimal Efficiency(string moldId)
        {
            try
            {
                int currentShots = GetCurrentShots(moldId);
                var duration = GetCurrentShotsDuration(moldId);
                var pezziOra = Math.Round(currentShots / duration.TotalHours, 2);
                int frequency = DBContext.Molds.Where(x => x.Id == moldId && x.ApplicationId == MultiTenantsHelper.ApplicationId).First().Frequency;
                return Math.Round((decimal)pezziOra / frequency, 2);
            }
            catch (Exception ex)
            {
                return (decimal)0.9;
            }
        }

        public int GetSmartLightStatus(string moldId)
        {
            return DBContext.MoldDatas.Where(x => x.TagId == moldId && x.CycleStart >= DateTime.Today).OrderByDescending(x => x.CycleStart).First().SmartlightStatus;
        }

        public void NextMaintenance(string moldId)
        {
            var duration = DBContext.MoldDatas.Where(x => x.TagId == moldId).Max(x => x.CycleEnd) - DBContext.MoldDatas.Where(x => x.TagId == moldId).Min(x => x.CycleStart);
            var totalShots = DBContext.MoldDatas.Where(x => x.TagId == moldId).Max(x => x.CurrentShots);
            var pezziOra = totalShots / duration.TotalHours;
        }

        public TimeSpan GetCurrentShotsDuration(string moldId)
        {
            return DBContext.MoldDatas.Where(x => x.TagId == moldId).Max(x => x.CycleEnd) - DBContext.MoldDatas.Where(x => x.TagId == moldId).Min(x => x.CycleStart);
        }

        public DateTime GetLastMaintenance(string moldId)
        {
            return DBContext.MoldDatas.Where(x => x.TagId == moldId).Max(x => x.LastMaintenance);
        }

        public int GetTotalShotsLastMaintenance(string moldId)
        {
            DateTime lastMaintenance = GetLastMaintenance(moldId);
            return DBContext.MoldDatas.Where(x => x.TagId == moldId && x.LastMaintenance == lastMaintenance).Min(x => x.TotalShots);
        }

        public DateTime GetTotalShotsLastUpdate(string moldId)
        {
            return DBContext.MoldDatas.Where(x => x.TagId == moldId).Max(x => x.CycleStart);
        }

        public int GetDailyCurrentShots(string moldId)
        {
            int? start = DBContext.MoldDatas.Where(x => x.TagId == moldId && x.CycleStart >= DateTime.Today)?.Min(x => (int?)x.TotalShots);
            int? end = DBContext.MoldDatas.Where(x => x.TagId == moldId && x.CycleStart >= DateTime.Today)?.Max(x => (int?)x.TotalShots);
            if (start.HasValue && end.HasValue)
            {
                return end.Value - start.Value;
            }
            else
            {
                return 0;
            }
        }
    }
}