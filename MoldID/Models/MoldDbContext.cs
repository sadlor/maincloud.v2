﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoldID.Models
{
    public class MoldDbContext : DbContext
    {
        public DbSet<Mold> Molds { get; set; }
        public DbSet<MoldData> MoldDatas { get; set; }

        public MoldDbContext()
            : base("MoldConnection")
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new MoldDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // I had removed this
                                                /// Rest of on model creating here.
        }

        public static MoldDbContext Create()
        {
            return new MoldDbContext();
        }
    }
}