﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoldID.Models
{
    public class MoldDbInitializer : CreateDatabaseIfNotExists<MoldDbContext>
    {
        protected override void Seed(MoldDbContext context)
        {
            base.Seed(context);
        }
    }
}