﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoldID.Models
{
    public class Mold
    {
        //public Mold()
        //{
        //    Id = Guid.NewGuid().ToString();
        //}

        [Key]
        [Required]
        [MaxLength(128)]
        public string Id { get; set; }  //TagId

        [Required]
        public string Name { get; set; }

        public string Code { get; set; }
        public string Description { get; set; }
        public string ApplicationId { get; set; }
        public string Brand { get; set; }
        public string IdNumber { get; set; }
        public string Model { get; set; }
        public string Builder { get; set; }
        public DateTime? DtInstallation { get; set; }
        public DateTime? DtEndGuarantee { get; set; }
        public string PathImg { get; set; }
        public string Location { get; set; }
        public string HostName { get; set; }
        public string Contact { get; set; }
        public int Frequency { get; set; }
        public int ProductionCadence { get; set; }
        public string LastMaintenance { get; set; }
        public string NextMaintenance { get; set; }
        public int Warning1 { get; set; }
        public int Warning2 { get; set; }
        public int Maximum { get; set; }
        public int ProgressiveCount { get; set; }
        public int NImpronte { get; set; } //tradurre
    }
}