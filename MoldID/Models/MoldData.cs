﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoldID.Models
{
    public class MoldData
    {
        [Key]
        [Required]
        public int Id { get; set; }

        public int Port { get; set; }
        public int ErrorState { get; set; }

        [Index("MoldDataSearchIndex", 1, IsUnique = true)]
        [MaxLength(128)]
        public string TagId { get; set; }

        public int Status { get; set; }
        public DateTime LastMaintenance { get; set; }
        public int SmartlightStatus { get; set; }
        public int Warning1 { get; set; }
        public int Warning2 { get; set; }
        public int Maximum { get; set; }
        public string FormName { get; set; }
        public string StorageLocation { get; set; }
        public string LastDeviceName { get; set; }
        public int MaxCycleTime { get; set; }
        public int MinCycleTime { get; set; }
        public int CurrentShots { get; set; }
        public int TotalShots { get; set; }
        public int TotalCycleTime { get; set; }
        public DateTime LastChange { get; set; }
        public DateTime NextMaintenance { get; set; }

        [Index("MoldDataSearchIndex", 2, IsUnique = true)]
        public DateTime CycleStart { get; set; }
        public DateTime CycleEnd { get; set; }
    }
}