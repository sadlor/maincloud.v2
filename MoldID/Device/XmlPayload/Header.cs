﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MoldID.Device.XmlPayload
{
    public class Header
    {
        [XmlElement("version")]
        public string Version { get; set; }
        [XmlElement("smartlightErrorState")]
        public int SmartlightErrorState { get; set; }
        [XmlElement("errorState")]
        public int ErrorState { get; set; }
        [XmlElement("location")]
        public string Location { get; set; }
        [XmlElement("smartlightStatus")]
        public int SmartlightStatus { get; set; }
        [XmlElement("hostname")]
        public string Hostname { get; set; }
        [XmlElement("contact")]
        public string Contact { get; set; }
        [XmlElement("cycleStart")]
        public DateTime CycleStart { get; set; }
        [XmlElement("applicationState")]
        public int ApplicationState { get; set; }
        [XmlElement("trigger")]
        public int Trigger { get; set; }
        [XmlElement("cycleEnd")]
        public DateTime CycleEnd { get; set; }
    }
}
