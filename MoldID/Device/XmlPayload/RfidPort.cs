﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MoldID.Device.XmlPayload
{
    public class RfidPort
    {
        [XmlElement("item")]
        public RfidPortItem Item { get; set; }
    }

    public class RfidPortItem
    {
        [XmlElement("port")]
        public int Port { get; set; }
        [XmlElement("errorState")]
        public int ErrorState { get; set; }
        [XmlElement("tagId")]
        public string TagId { get; set; }
        [XmlElement("status")]
        public int Status { get; set; }
        [XmlElement("lastMaintenance")]
        public DateTime LastMaintenance { get; set; }
        [XmlElement("smartlightStatus")]
        public int SmartlightStatus { get; set; }
        [XmlElement("warning1")]
        public int Warning1 { get; set; }
        [XmlElement("warning2")]
        public int Warning2 { get; set; }
        [XmlElement("maximum")]
        public int Maximum { get; set; }
        [XmlElement("formName")]
        public string FormName { get; set; }
        [XmlElement("storageLocation")]
        public string StorageLocation { get; set; }
        [XmlElement("lastDeviceName")]
        public string LastDeviceName { get; set; }
        [XmlElement("maxCycleTime")]
        public int MaxCycleTime { get; set; }
        [XmlElement("minCycleTime")]
        public int MinCycleTime { get; set; }
        [XmlElement("currentShots")]
        public int CurrentShots { get; set; }
        [XmlElement("totalShots")]
        public int TotalShots { get; set; }
        [XmlElement("totalCycleTime")]
        public int TotalCycleTime { get; set; }
        [XmlElement("lastChange")]
        public DateTime LastChange { get; set; }
        [XmlElement("nextMaintenance")]
        public DateTime NextMaintenance { get; set; }
    }
}