﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MoldID.Device.XmlPayload
{
    [XmlRoot("root")]
    public class XmlPayload
    {
        [XmlElement("header")]
        public Header Header { get; set; }

        [XmlElement("rfidPorts")]
        public RfidPort[] RfidPorts { get; set; }
    }
}