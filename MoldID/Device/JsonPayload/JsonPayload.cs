﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoldID.Device.JsonPayload
{
    public class JsonPayload
    {
        public Header header { get; set; }
        public RfidPort[] rfidPorts { get; set; }
    }
}