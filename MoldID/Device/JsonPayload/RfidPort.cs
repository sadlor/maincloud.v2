﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoldID.Device.JsonPayload
{
    public class RfidPort
    {
        public int port { get; set; }
        public int errorState { get; set; }
        public string tagId { get; set; }
        public int status { get; set; }
        public DateTime lastMaintenance { get; set; }
        public int smartlightStatus { get; set; }
        public int warning1 { get; set; }
        public int warning2 { get; set; }
        public int maximum { get; set; }
        public string formName { get; set; }
        public string storageLocation { get; set; }
        public string lastDeviceName { get; set; }
        public int maxCycleTime { get; set; }
        public int minCycleTime { get; set; }
        public int currentShots { get; set; }
        public int totalShots { get; set; }
        public int totalCycleTime { get; set; }
        public DateTime lastChange { get; set; }
        public DateTime nextMaintenance { get; set; }
    }
}