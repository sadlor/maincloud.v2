﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoldID.Device.JsonPayload
{
    public class Header
    {
        public string version { get; set; }
        public int smartlightErrorState { get; set; }
        public int errorState { get; set; }
        public string location { get; set; }
        public int smartlightStatus { get; set; }
        public string hostname { get; set; }
        public string contact { get; set; }
        public DateTime cycleStart { get; set; }
        public int applicationState { get; set; }
        public int trigger { get; set; }
        public DateTime cycleEnd { get; set; }
    }
}