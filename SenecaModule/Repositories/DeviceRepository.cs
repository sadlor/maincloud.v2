﻿using MainCloudFramework.Repositories;
using SenecaModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenecaModule.Repositories
{
    public class DeviceRepository : BaseRepository<Device, SenecaDbContext>
    {
    }
}
