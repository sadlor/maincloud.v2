﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenecaModule.Models
{
    public class Gateway
    {
        public Gateway()
        {
            Id = Guid.NewGuid().ToString();
            DeviceList = new List<Device>();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }
        public string SerialNumber { get; set; }
        public string Model { get; set; }  //device type as Z-KEY, ecc...
        public string Supplier { get; set; }  //fornitore
        public string NetworkAddress { get; set; } //private IP
        public DateTime? InstallationDate { get; set; }
        public string BoxId { get; set; }
        public bool Active { get; set; }

        [ForeignKey("BoxId")]
        public Box Box { get; set; }

        [InverseProperty("Gateway")]
        public virtual List<Device> DeviceList { get; set; }

        public string ApplicationId { get; set; }
    }
}