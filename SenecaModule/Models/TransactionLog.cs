﻿using MES.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenecaModule.Models
{
    public class TransactionLog //: ITransactionLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string GatewayId { get; set; }

        public string Machine { get; set; }

        public bool Status { get; set; }

        public DateTime Date { get; set; }

        public decimal Counter { get; set; }

        /////JSON column
        //public string ExtraData { get; set; }
    }
}