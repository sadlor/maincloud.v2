﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenecaModule.Models
{
    public class ConnectedMachine //Dati essenziali per ogni macchina collegata
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string AssetId { get; set; } //Id asset di riferimento

        [Required]
        public string DeviceId { get; set; }

        [ForeignKey("DeviceId")]
        public Device Device { get; set; }

        public int ValuePin { get; set; }   //Pin contatore
        public int Value { get; set; }      //Contatore
        public int StatusPin { get; set; }  //Pin on-off
        public bool Status { get; set; }    //on-off

        //public string ApplicationId { get; set; }
    }
}