﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenecaModule.Models
{
    public class Box
    {
        public Box()
        {
            Id = Guid.NewGuid().ToString();
            Gateway = new List<Gateway>();
            //DeviceList = new List<Device>();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }
        public string SerialNumber { get; set; }
        public string Supplier { get; set; }
        public DateTime? InstallationDate { get; set; }
        public string PlantId { get; set; }

        //[InverseProperty("Box")]
        //public virtual List<Device> DeviceList { get; set; }

        [InverseProperty("Box")]
        public virtual List<Gateway> Gateway { get; set; }

        public string ApplicationId { get; set; }
    }
}