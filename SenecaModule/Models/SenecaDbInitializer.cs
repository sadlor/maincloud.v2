﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenecaModule.Models
{
    public class SenecaDbInitializer : CreateDatabaseIfNotExists<SenecaDbContext>
    {
        protected override void Seed(SenecaDbContext context)
        {
            base.Seed(context);
        }
    }
}
