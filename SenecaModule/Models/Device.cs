﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenecaModule.Models
{
    public class Device
    {
        public Device()
        {
            Id = Guid.NewGuid().ToString();
            ConnectedMachineList = new List<ConnectedMachine>();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }
        public string SerialNumber { get; set; }
        public string Model { get; set; }  //device type as Z-10D-IN, ecc...
        public string Supplier { get; set; }
        public string NetworkAddress { get; set; }
        public DateTime? InstallationDate { get; set; }
        public int ConnectedNumber { get; set; } //identifica l'ordine di collegamento dei dispositivi per la lettura dei contatori (macchina 1 primi 10 registri, macchina 2 da 10 a 20, ecc...)

        public string BoxId { get; set; }
        public string GatewayId { get; set; }

        [ForeignKey("BoxId")]
        public Box Box { get; set; }

        [ForeignKey("GatewayId")]
        public Gateway Gateway { get; set; }

        [InverseProperty("Device")]
        public virtual List<ConnectedMachine> ConnectedMachineList { get; set; }

        public string ApplicationId { get; set; }
    }
}