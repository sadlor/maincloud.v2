﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenecaModule.Models
{
    public class SenecaDbContext : DbContext
    {
        public DbSet<Box> Boxes { get; set; }
        public DbSet<Gateway> Gateways { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<ConnectedMachine> ConnectedMachines { get; set; }
        public DbSet<TransactionLog> TransactionLogs { get; set; }

        public SenecaDbContext()
            : base("SenecaConnection")
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new SenecaDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // I had removed this
                                                /// Rest of on model creating here.
        }

        public static SenecaDbContext Create()
        {
            return new SenecaDbContext();
        }
    }
}