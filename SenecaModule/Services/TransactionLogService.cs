﻿using MainCloudFramework.Services;
using SenecaModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenecaModule.Services
{
    public class TransactionLogService : BaseService<SenecaDbContext>
    {
        public TransactionLog LastTransaction(string machineId)
        {
            return DBContext.TransactionLogs.Where(x => x.Machine == machineId).OrderByDescending(x => x.Date).First();
        }
    }
}