﻿using MainCloudFramework.Services;
using SenecaModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenecaModule.Services
{
    public class BoxService : BaseService<SenecaDbContext>
    {
        public List<Box> GetBoxListByApplicationId(string applicationId)
        {
            return DBContext.Boxes.Where(x => x.ApplicationId == applicationId).ToList();
        }

        public bool IsMachineConnected(string assetId)
        {
            return DBContext.ConnectedMachines.Where(x => x.AssetId == assetId).Any();
        }
    }
}