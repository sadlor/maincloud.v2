﻿using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Helper
{
    public class GrantOperator
    {
        //public bool IsSupervisorOperator(string userId)
        //{
        //    var appId = OperatorHelper.ApplicationId;

        //    using (MesDbContext db = new MesDbContext())
        //    {
        //        string adminRoleId = (from r in db.MesRoles
        //                              where r.Name == MesConstants.ROLE_SUPERVISOR && r.ApplicationId == appId
        //                              select r.Id).Single();

        //        var IsInRole = (from u in db.Operators
        //                        where u.Id == userId && u.Roles.Where(x => x.RoleId == adminRoleId).Any()
        //                        select u).Any();

        //        return IsInRole;
        //    }
        //}

        //public bool IsInRole(string role)
        //{
        //    try
        //    {
        //        string userId = HttpContext.Current.User.Identity.GetUserId();
        //        var userRole = applicationService.FindUserRoleCurrentApp(userId);
        //        return userRole != null && userRole.Name == role;
        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }
        //}

        //private OperatorRole CurrentApplicationUserRole()
        //{
        //    string currenUserId = HttpContext.Current.User.Identity.GetUserId();
        //    var r = applicationService.FindUserRoleCurrentApp(currenUserId);
        //    return r;
        //}

        private OperatorRoleService operRoleService = new OperatorRoleService();

        public bool AllowProduction(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            else
            {
                var roles = operRoleService.RolesPerOperator(userId);
                return roles.Count > 0 && roles.Where(x => x.AllowProduction).Any();
            }
        }

        public bool AllowSetup(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            else
            {
                var roles = operRoleService.RolesPerOperator(userId);
                return roles.Count > 0 && roles.Where(x => x.AllowSetup).Any();
            }
        }

        public bool AllowMaintenance(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            else
            {
                var roles = operRoleService.RolesPerOperator(userId);
                return roles.Count > 0 && roles.Where(x => x.AllowMaintenance).Any();
            }
        }

        public bool AllowQuality(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            else
            {
                var roles = operRoleService.RolesPerOperator(userId);
                return roles.Count > 0 && roles.Where(x => x.AllowQuality).Any();
            }
        }

        public bool AllowWaste(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            else
            {
                var roles = operRoleService.RolesPerOperator(userId);
                return roles.Count > 0 && roles.Where(x => x.AllowWaste).Any();
            }
        }

        public bool AllowSelectJob(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            else
            {
                var roles = operRoleService.RolesPerOperator(userId);
                return roles.Count > 0 && roles.Where(x => x.AllowSelectJob).Any();
            }
        }

        public bool AllowSelectMold(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            else
            {
                var roles = operRoleService.RolesPerOperator(userId);
                return roles.Count > 0 && roles.Where(x => x.AllowSelectMold).Any();
            }
        }

        public bool AllowSelectMP(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            else
            {
                var roles = operRoleService.RolesPerOperator(userId);
                return roles.Count > 0 && roles.Where(x => x.AllowSelectMP).Any();
            }
        }

        public bool AllowOEE(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            else
            {
                var roles = operRoleService.RolesPerOperator(userId);
                return roles.Count > 0 && roles.Where(x => x.AllowOEE).Any();
            }
        }

        public bool AllowAnalysis(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            else
            {
                var roles = operRoleService.RolesPerOperator(userId);
                return roles.Count > 0 && roles.Where(x => x.AllowAnalysis).Any();
            }
        }

        public bool AllowConfirmStartUpProduction(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            else
            {
                var roles = operRoleService.RolesPerOperator(userId);
                return roles.Count > 0 && roles.Where(x => x.AllowConfirmStartUpProduction).Any();
            }
        }
    }
}