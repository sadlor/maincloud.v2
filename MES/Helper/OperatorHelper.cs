﻿using MainCloudFramework.Repositories;
using MainCloudFramework.Web.Multitenants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MES.Helper
{
    public class OperatorHelper
    {
        const string APPLICATION_ID = "application_tenant_id";

        public static string ApplicationId
        {
            get
            {
                if (HttpContext.Current.Session.SessionMultitenant().ContainsKey(APPLICATION_ID))
                {
                    return HttpContext.Current.Session.SessionMultitenant()[APPLICATION_ID] as string;
                }
                else
                {
                    return null;
                }
                //ApplicationRepository appRep = new ApplicationRepository();
                //string applicationName = HttpContext.Current.Session.SessionMultitenant()[APPLICATION_NAME] as string;
                //return appRep.ReadAll(x => x.Name == applicationName).FirstOrDefault()?.Id;
            }
        }

        //public ApplicationRole GetCurrentApplicationUserRole()
        //{
        //    string userId = HttpContext.Current.User.Identity.GetUserId();
        //    var r = FindUserRoleCurrentApp(userId);
        //    return r;
        //}

        //public ApplicationRole FindUserRoleCurrentApp(string userId)
        //{
        //    var user = userRepository.FindByID(userId);
        //    return FindUserRoleCurrentApp(user);
        //}

        /// <summary>
        /// Ricerca l'utente, identifica l'applicazione tramite application name presente nll'url.
        /// Ricerca il ruolo di un utente e restituisce la relativa entity
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        //public ApplicationRole FindUserRoleCurrentApp(ApplicationUser user)
        //{
        //    var appId = MultiTenantsHelper.ApplicationId;
        //    var appRoles = RolesInApplication(appId);
        //    var userRoles = user.Roles.Where(x => appRoles.Where(y => y.Users.Where(u => u.RoleId == x.RoleId).Any()).Any()).SingleOrDefault();
        //    if (userRoles != null)
        //    {
        //        var role = DBContext.Roles.Find(userRoles.RoleId);
        //        return role;
        //    }
        //    return null;
        //}
    }
}