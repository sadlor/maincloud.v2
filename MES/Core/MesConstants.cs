﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Core
{
    public class MesConstants
    {
        public const string DEPARTMENT_ZONE = "department_zone";
        public const string APPLICATION_ID = "application_tenant_id";
        public const string ROLE_SUPERVISOR = "Capoturno";
        public const string MANUAL_OPERATIONS_ACTIVE = "manual_operations_active";
        public const string TRANSACTION_ID_TO_MODIFY = "transaction_id_to_modify";
        public const string ASSET_TO_MODIFY_TRANSACTIONS = "asset_to_modify_transactions";
        public const string ASSET_TO_VIEW_TRANSACTIONS = "asset_to_view_transactions";
    }
}