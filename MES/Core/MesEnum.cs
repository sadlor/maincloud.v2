﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Core
{
    public static class MesEnum
    {
        //tutti gli enumeratori inserirli qua
        public enum TransactionParam
        {
            Cause,
            CavityMoldNum,
            Job,
            Mold,
            Machine,
            MP,
            Operator,
            Status,
            StandardRate
        }

        public enum Cause
        {
            SetUp = 100,       //Attrezzaggio
            Production = 200,  //Produzione
            Maintenance = 300, //Manutenzione
            MachineOff = 400,  //Macchina spenta
            Stop = 400,        //Fermi
            Waste = 500,       //scarti
            Alarm = 600,       //Allarme
        }

        public enum OperationName
        {
            LogIn,
            Attrezzaggio,
            Manutenzione,
            Scarti,
            Qualità
        }

        public enum OperatorRole
        {
            Attrezzista,
            Capoturno,
            Manutentore,
            Operatore,
            Qualità
        }

        public enum QualityLevel
        {
            Ok,
            Warning,
            Stop
        }

        public enum OrderStatusCode
        {
            Sospesa = 0,
            Non_esaminata = 1,
            Schedulata = 2,
            Lanciata = 3,
            Esecutiva = 4,
            Evasa = 5,
            Storicizzata = 6,
            Annullata = 7
        }

        public enum IndexParam
        {
            Availability,
            Efficiency,
            Quality,
            OEE
        }

        public enum ExtraData
        {
            Height = 1,
            Width = 2
        }
    }
}