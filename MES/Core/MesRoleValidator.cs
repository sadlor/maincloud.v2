﻿using MES.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Core
{
    /// <summary>
    ///     Validates roles before they are saved
    /// </summary>
    /// <typeparam name="TRole"></typeparam>
    public class MesRoleValidator<TRole> : RoleValidator<TRole, string> where TRole : MesRole, IRole<string>
    {
        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="manager"></param>
        public MesRoleValidator(MesRoleManager<TRole, string> manager)
            : base(manager)
        {
        }
    }

    /// <summary>
    ///     Validates roles before they are saved
    /// </summary>
    /// <typeparam name="TRole"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public class MesRoleValidator<TRole, TKey> : IIdentityValidator<TRole>
        where TRole : MesRole, IRole<TKey>
        where TKey : IEquatable<TKey>
    {
        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="manager"></param>
        public MesRoleValidator(MesRoleManager<TRole, TKey> manager)
        {
            if (manager == null)
            {
                throw new ArgumentNullException("manager");
            }
            Manager = manager;
        }

        private MesRoleManager<TRole, TKey> Manager { get; set; }

        /// <summary>
        ///     Validates a role before saving
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> ValidateAsync(TRole item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            var errors = new List<string>();
            await ValidateRoleName(item, errors).WithCurrentCulture();
            if (errors.Count > 0)
            {
                return IdentityResult.Failed(errors.ToArray());
            }
            return IdentityResult.Success;
        }

        private async Task ValidateRoleName(TRole role, List<string> errors)
        {
            if (string.IsNullOrWhiteSpace(role.Name))
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, "{0} cannot be null or empty.", "Name"));
            }
            else
            {
                var owner = await Manager.FindByNameAsync(role.Name).WithCurrentCulture();
                if (owner != null && !EqualityComparer<string>.Default.Equals(owner.Id, role.Id) && EqualityComparer<string>.Default.Equals(owner.ApplicationId, role.ApplicationId))
                {
                    errors.Add(String.Format(CultureInfo.CurrentCulture, "Name {0} is already taken.", role.Name));
                }

            }
        }

    }
}