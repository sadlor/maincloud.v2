﻿using MES.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Core
{
    /// <summary>
    ///     Exposes role related api which will automatically save changes to the RoleStore
    /// </summary>
    /// <typeparam name="TRole"></typeparam>
    public class MesRoleManager<TRole> : MesRoleManager<TRole, string>
        where TRole : MesRole, IRole<string>
    {
        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="store"></param>
        public MesRoleManager(IRoleStore<TRole, string> store)
            : base(store)
        {
        }
    }

    /// <summary>
    ///     Exposes role related api which will automatically save changes to the RoleStore
    /// </summary>
    /// <typeparam name="TRole"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public class MesRoleManager<TRole, TKey> : RoleManager<TRole, TKey>, IDisposable
        where TRole : MesRole, IRole<TKey>
        where TKey : IEquatable<TKey>
    {
        private bool _disposed;
        private IIdentityValidator<TRole> _roleValidator;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="store">The IRoleStore is responsible for commiting changes via the UpdateAsync/CreateAsync methods</param>
        public MesRoleManager(IRoleStore<TRole, TKey> store)
            : base(store)
        {
            RoleValidator = new MesRoleValidator<TRole, TKey>(this);
        }
    }
}