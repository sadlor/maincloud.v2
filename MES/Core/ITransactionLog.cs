﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Core
{
    public interface ITransactionLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        long Id { get; set; }
        string Machine { get; set; }
        string StatusCode { get; set; }
        string StatusDescription { get; set; }
        DateTime Date { get; set; }
        decimal Counter { get; set; }
        bool Status { get; set; }

        string ExtraData { get; set; }
    }

    public class BaseTransactionLog : ITransactionLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Machine { get; set; }
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public DateTime Date { get; set; }
        public decimal Counter { get; set; }
        public bool Status { get; set; }

        public string ExtraData { get; set; }
    }
}