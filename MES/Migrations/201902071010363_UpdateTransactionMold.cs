namespace MES.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTransactionMold : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TransactionMolds", "OperatorId", c => c.String(maxLength: 128));
            AddColumn("dbo.TransactionMolds", "Exported", c => c.Boolean(nullable: false));
            CreateIndex("dbo.TransactionMolds", "OperatorId");
            AddForeignKey("dbo.TransactionMolds", "OperatorId", "dbo.Operators", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TransactionMolds", "OperatorId", "dbo.Operators");
            DropIndex("dbo.TransactionMolds", new[] { "OperatorId" });
            DropColumn("dbo.TransactionMolds", "Exported");
            DropColumn("dbo.TransactionMolds", "OperatorId");
        }
    }
}
