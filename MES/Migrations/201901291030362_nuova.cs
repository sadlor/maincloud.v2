namespace MES.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nuova : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomerOrders", "IdArticle", "dbo.Articles");
            DropForeignKey("dbo.Orders", "IdArticle", "dbo.Articles");
            DropForeignKey("dbo.ArticleBatches", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.ArticleMolds", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.ControlPlanDetailExecuteds", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.JobProductionWastes", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.PartPrograms", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.Transactions", "IdArticle", "dbo.Articles");
            DropForeignKey("dbo.Jobs", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Orders", "CustomerOrderId", "dbo.CustomerOrders");
            DropPrimaryKey("dbo.Articles");
            DropPrimaryKey("dbo.Orders");
            DropPrimaryKey("dbo.CustomerOrders");
            DropPrimaryKey("dbo.Transactions");
            AlterColumn("dbo.Articles", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Orders", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.CustomerOrders", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Transactions", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Articles", "Id");
            AddPrimaryKey("dbo.Orders", "Id");
            AddPrimaryKey("dbo.CustomerOrders", "Id");
            AddPrimaryKey("dbo.Transactions", "Id");
            AddForeignKey("dbo.CustomerOrders", "IdArticle", "dbo.Articles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Orders", "IdArticle", "dbo.Articles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ArticleBatches", "ArticleId", "dbo.Articles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ArticleMolds", "ArticleId", "dbo.Articles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ControlPlanDetailExecuteds", "ArticleId", "dbo.Articles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.JobProductionWastes", "ArticleId", "dbo.Articles", "Id");
            AddForeignKey("dbo.PartPrograms", "ArticleId", "dbo.Articles", "Id");
            AddForeignKey("dbo.Transactions", "IdArticle", "dbo.Articles", "Id");
            AddForeignKey("dbo.Jobs", "OrderId", "dbo.Orders", "Id");
            AddForeignKey("dbo.Orders", "CustomerOrderId", "dbo.CustomerOrders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "CustomerOrderId", "dbo.CustomerOrders");
            DropForeignKey("dbo.Jobs", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Transactions", "IdArticle", "dbo.Articles");
            DropForeignKey("dbo.PartPrograms", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.JobProductionWastes", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.ControlPlanDetailExecuteds", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.ArticleMolds", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.ArticleBatches", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.Orders", "IdArticle", "dbo.Articles");
            DropForeignKey("dbo.CustomerOrders", "IdArticle", "dbo.Articles");
            DropPrimaryKey("dbo.Transactions");
            DropPrimaryKey("dbo.CustomerOrders");
            DropPrimaryKey("dbo.Orders");
            DropPrimaryKey("dbo.Articles");
            AlterColumn("dbo.Transactions", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.CustomerOrders", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Articles", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Transactions", "Id");
            AddPrimaryKey("dbo.CustomerOrders", "Id");
            AddPrimaryKey("dbo.Orders", "Id");
            AddPrimaryKey("dbo.Articles", "Id");
            AddForeignKey("dbo.Orders", "CustomerOrderId", "dbo.CustomerOrders", "Id");
            AddForeignKey("dbo.Jobs", "OrderId", "dbo.Orders", "Id");
            AddForeignKey("dbo.Transactions", "IdArticle", "dbo.Articles", "Id");
            AddForeignKey("dbo.PartPrograms", "ArticleId", "dbo.Articles", "Id");
            AddForeignKey("dbo.JobProductionWastes", "ArticleId", "dbo.Articles", "Id");
            AddForeignKey("dbo.ControlPlanDetailExecuteds", "ArticleId", "dbo.Articles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ArticleMolds", "ArticleId", "dbo.Articles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ArticleBatches", "ArticleId", "dbo.Articles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Orders", "IdArticle", "dbo.Articles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CustomerOrders", "IdArticle", "dbo.Articles", "Id", cascadeDelete: true);
        }
    }
}
