namespace MES.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProgressiveCounter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TransactionMolds", "CounterStart", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.TransactionMolds", "CounterEnd", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Transactions", "CounterStart", c => c.Decimal(nullable: false, precision: 18, scale: 4));
            AddColumn("dbo.Transactions", "CounterEnd", c => c.Decimal(precision: 18, scale: 4));
            AlterColumn("dbo.Transactions", "ProgressiveCounter", c => c.Decimal(nullable: false, precision: 18, scale: 4));

            Sql("UPDATE dbo.TransactionMolds SET CounterStart = ProgressiveCounter, CounterEnd = ProgressiveCounterEnd, ProgressiveCounter = ProgressiveCounterEnd WHERE ISNULL([End], '') <> ''");
            Sql("UPDATE dbo.TransactionMolds SET ProgressiveCounter = CounterStart WHERE ISNULL([End], '') = ''");
            Sql("UPDATE dbo.Transactions SET CounterStart = ProgressiveCounter, CounterEnd = ProgressiveCounterEnd, ProgressiveCounter = ProgressiveCounterEnd WHERE ProgressiveCounterEnd <> null");
            Sql("UPDATE dbo.Transactions SET ProgressiveCounter = CounterStart WHERE ISNULL([End], '') = ''");

            DropColumn("dbo.TransactionMolds", "ProgressiveCounterEnd");
            DropColumn("dbo.Transactions", "ProgressiveCounterEnd");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "ProgressiveCounterEnd", c => c.Decimal(precision: 18, scale: 4));
            AddColumn("dbo.TransactionMolds", "ProgressiveCounterEnd", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Transactions", "ProgressiveCounter", c => c.Decimal(nullable: false, precision: 18, scale: 4));
            DropColumn("dbo.Transactions", "CounterEnd");
            DropColumn("dbo.Transactions", "CounterStart");
            DropColumn("dbo.TransactionMolds", "CounterEnd");
            DropColumn("dbo.TransactionMolds", "CounterStart");
        }
    }
}
