namespace MES.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0001 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.CustomerOrders", "UniqueCustomerOrderIndex");
            AddColumn("dbo.Articles", "ClientId", c => c.Int());
            AddColumn("dbo.Jobs", "ClientId", c => c.Int());
            AddColumn("dbo.ControlPlans", "ClientId", c => c.Int());
            AddColumn("dbo.ControlPlanDetailExecuteds", "ClientId", c => c.Int());
            AddColumn("dbo.ControlPlanDetails", "ClientId", c => c.Int());
            AddColumn("dbo.Transactions", "ClientId", c => c.Int());
            AlterColumn("dbo.Articles", "ApplicationId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Jobs", "ApplicationId", c => c.String(maxLength: 128));
            AlterColumn("dbo.ControlPlans", "ApplicationId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Orders", "ClientId", c => c.Int());
            AlterColumn("dbo.Orders", "ApplicationId", c => c.String(maxLength: 128));
            AlterColumn("dbo.CustomerOrders", "ClientId", c => c.Int());
            AlterColumn("dbo.ControlPlanDetailExecuteds", "ApplicationId", c => c.String(maxLength: 128));
            AlterColumn("dbo.ControlPlanDetails", "ApplicationId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Transactions", "ApplicationId", c => c.String(maxLength: 128));

            Sql("UPDATE dbo.Articles SET ClientId = Id WHERE ClientId IS NULL");
            Sql("UPDATE dbo.Jobs SET ClientId = CAST(Id as int) WHERE ClientId IS NULL and ISNUMERIC(Id) = 1");
            Sql("UPDATE dbo.Orders SET ClientId = Id WHERE ClientId IS NULL");
            Sql("UPDATE dbo.CustomerOrders SET ClientId = Id WHERE ClientId IS NULL");
            Sql("UPDATE dbo.Transactions SET ClientId = Id WHERE ClientId IS NULL");
            Sql("UPDATE dbo.ControlPlans SET ClientId = Id WHERE ClientId IS NULL");
            Sql("UPDATE dbo.ControlPlanDetails SET ClientId = Id WHERE ClientId IS NULL");
            Sql("UPDATE dbo.ControlPlanDetailExecuteds SET ClientId = Id WHERE ClientId IS NULL");

            AlterColumn("dbo.Jobs", "ClientId", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "ClientId", c => c.Int(nullable: false));

            //CreateIndex("dbo.Articles", new[] { "ClientId", "ApplicationId" }, unique: true, name: "UniqueClientApplicationIndex");
            //CreateIndex("dbo.Jobs", new[] { "ClientId", "ApplicationId" }, unique: true, name: "UniqueClientApplicationIndex");
            //CreateIndex("dbo.ControlPlans", new[] { "ClientId", "ApplicationId" }, unique: true, name: "UniqueClientApplicationIndex");
            //CreateIndex("dbo.Orders", new[] { "ClientId", "ApplicationId" }, unique: true, name: "UniqueClientApplicationIndex");
            //CreateIndex("dbo.CustomerOrders", new[] { "ClientId", "ApplicationId" }, unique: true, name: "UniqueClientApplicationIndex");
            //CreateIndex("dbo.ControlPlanDetailExecuteds", new[] { "ClientId", "ApplicationId" }, unique: true, name: "UniqueClientApplicationIndex");
            //CreateIndex("dbo.ControlPlanDetails", new[] { "ClientId", "ApplicationId" }, unique: true, name: "UniqueClientApplicationIndex");
            //CreateIndex("dbo.Transactions", new[] { "ClientId", "ApplicationId" }, unique: true, name: "UniqueClientApplicationIndex");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Transactions", "UniqueClientApplicationIndex");
            DropIndex("dbo.ControlPlanDetails", "UniqueClientApplicationIndex");
            DropIndex("dbo.ControlPlanDetailExecuteds", "UniqueClientApplicationIndex");
            DropIndex("dbo.CustomerOrders", "UniqueClientApplicationIndex");
            DropIndex("dbo.Orders", "UniqueClientApplicationIndex");
            DropIndex("dbo.ControlPlans", "UniqueClientApplicationIndex");
            DropIndex("dbo.Jobs", "UniqueClientApplicationIndex");
            DropIndex("dbo.Articles", "UniqueClientApplicationIndex");
            AlterColumn("dbo.Transactions", "ApplicationId", c => c.String());
            AlterColumn("dbo.ControlPlanDetails", "ApplicationId", c => c.String());
            AlterColumn("dbo.ControlPlanDetailExecuteds", "ApplicationId", c => c.String());
            AlterColumn("dbo.CustomerOrders", "ClientId", c => c.String());
            AlterColumn("dbo.Orders", "ApplicationId", c => c.String());
            AlterColumn("dbo.Orders", "ClientId", c => c.String());
            AlterColumn("dbo.ControlPlans", "ApplicationId", c => c.String());
            AlterColumn("dbo.Jobs", "ApplicationId", c => c.String());
            AlterColumn("dbo.Articles", "ApplicationId", c => c.String());
            DropColumn("dbo.Transactions", "ClientId");
            DropColumn("dbo.ControlPlanDetails", "ClientId");
            DropColumn("dbo.ControlPlanDetailExecuteds", "ClientId");
            DropColumn("dbo.ControlPlans", "ClientId");
            DropColumn("dbo.Jobs", "ClientId");
            DropColumn("dbo.Articles", "ClientId");
            CreateIndex("dbo.CustomerOrders", new[] { "OrderCode", "ApplicationId" }, unique: true, name: "UniqueCustomerOrderIndex");
        }
    }
}
