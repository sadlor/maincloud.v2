namespace MES.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMoldInWaste : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.JobProductionWastes", "MoldId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.JobProductionWastes", "MoldId");
        }
    }
}
