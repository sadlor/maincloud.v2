
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[V_SelectJobOrders]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[V_SelectJobOrders]
AS
SELECT dbo.Jobs.Id, dbo.Jobs.Code, dbo.Jobs.Description, dbo.Orders.OrderCode AS "Order", dbo.Orders.StatusCode as OrderStatusCode, dbo.CustomerOrders.OrderCode AS CustomerOrder,
	   dbo.Articles.Code AS ArticleCode, dbo.Articles.Description AS ArticleDescr, dbo.Jobs.StartDate, dbo.Jobs.EndDate,
	   dbo.Jobs.ApplicationId, dbo.Jobs.AssetId
FROM dbo.Jobs
INNER JOIN dbo.Orders ON dbo.Jobs.OrderId = dbo.Orders.Id
INNER JOIN dbo.CustomerOrders ON dbo.Orders.CustomerOrderId = dbo.CustomerOrders.Id
INNER JOIN dbo.Articles ON dbo.Orders.IdArticle = dbo.Articles.Id
WHERE dbo.Orders.StatusCode = 4 or dbo.Orders.StatusCode = 0'