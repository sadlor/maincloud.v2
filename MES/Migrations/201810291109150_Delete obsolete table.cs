namespace MES.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Deleteobsoletetable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BOMs", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.BOMs", "ComponentId", "dbo.Articles");
            DropForeignKey("dbo.JobRawMaterials", "RawMaterialBatchId", "dbo.RawMaterialBatches");
            DropForeignKey("dbo.JobRawMaterials", "ArticleBatchId", "dbo.ArticleBatches");
            DropForeignKey("dbo.JobRawMaterials", "JobId", "dbo.Jobs");
            DropForeignKey("dbo.RawMaterials", "JobId", "dbo.Jobs");
            DropForeignKey("dbo.RawMaterials", "MeasureUnitId", "dbo.MeasureUnits");
            DropForeignKey("dbo.RawMaterials", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.RawMaterialBatches", "RawMaterialId", "dbo.RawMaterials");
            DropForeignKey("dbo.Transactions", "RawMaterialBatchId", "dbo.RawMaterialBatches");
            DropIndex("dbo.BOMs", new[] { "ArticleId" });
            DropIndex("dbo.BOMs", new[] { "ComponentId" });
            DropIndex("dbo.JobRawMaterials", new[] { "JobId" });
            DropIndex("dbo.JobRawMaterials", new[] { "RawMaterialBatchId" });
            DropIndex("dbo.JobRawMaterials", new[] { "ArticleBatchId" });
            DropIndex("dbo.RawMaterialBatches", new[] { "RawMaterialId" });
            DropIndex("dbo.RawMaterials", new[] { "MeasureUnitId" });
            DropIndex("dbo.RawMaterials", new[] { "JobId" });
            DropIndex("dbo.RawMaterials", new[] { "OrderId" });
            DropIndex("dbo.Transactions", new[] { "RawMaterialBatchId" });
            RenameColumn(table: "dbo.Transactions", name: "RawMaterialBatchId", newName: "ArticleBatchId");
            AddForeignKey("dbo.Transactions", "ArticleBatchId", "dbo.ArticleBatches", "Id", cascadeDelete: true);
            CreateIndex("dbo.Transactions", "ArticleBatchId");
            //RenameIndex(table: "dbo.Transactions", name: "IX_RawMaterialBatchId", newName: "IX_ArticleBatchId");
            CreateTable(
                "dbo.JobBatches",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    MachineId = c.String(nullable: false),
                    JobId = c.String(nullable: false, maxLength: 128),
                    ArticleBatchId = c.Int(nullable: false),
                    OperatorId = c.String(maxLength: 128),
                    Start = c.DateTime(nullable: false),
                    End = c.DateTime(),
                    Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                    ApplicationId = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArticleBatches", t => t.ArticleBatchId, cascadeDelete: true)
                .ForeignKey("dbo.Jobs", t => t.JobId, cascadeDelete: true)
                .ForeignKey("dbo.Operators", t => t.OperatorId)
                .Index(t => t.JobId)
                .Index(t => t.ArticleBatchId)
                .Index(t => t.OperatorId);

            DropTable("dbo.BOMs");
            DropTable("dbo.JobRawMaterials");
            DropTable("dbo.JobSeries");
            DropTable("dbo.RawMaterialBatches");
            DropTable("dbo.RawMaterials");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.RawMaterials",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MeasureUnitId = c.String(maxLength: 128),
                        ArticleClass = c.String(),
                        ArticleClassDescription = c.String(),
                        JobId = c.String(maxLength: 128),
                        OrderId = c.Int(nullable: false),
                        ApplicationId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RawMaterialBatches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        RawMaterialId = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ApplicationId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.JobSeries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JobId = c.String(nullable: false),
                        OperatorId = c.String(),
                        MachineId = c.String(),
                        Start = c.DateTime(nullable: false),
                        Stop = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.JobRawMaterials",
                c => new
                    {
                        JobId = c.String(nullable: false, maxLength: 128),
                        Id = c.Int(nullable: false, identity: true),
                        ArticleBatchId = c.Int(nullable: false),
                        ArticleBatchCode = c.String(),
                        QtyRawMaterial = c.Decimal(nullable: false, precision: 18, scale: 2),
                        QtyProductionWasteRawMaterial = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BOMs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArticleId = c.Int(nullable: false),
                        ComponentId = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MeasureUnit = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.JobBatches", "OperatorId", "dbo.Operators");
            DropForeignKey("dbo.JobBatches", "JobId", "dbo.Jobs");
            DropForeignKey("dbo.JobBatches", "ArticleBatchId", "dbo.ArticleBatches");
            DropIndex("dbo.JobBatches", new[] { "OperatorId" });
            DropIndex("dbo.JobBatches", new[] { "ArticleBatchId" });
            DropIndex("dbo.JobBatches", new[] { "JobId" });
            DropTable("dbo.JobBatches");
            RenameIndex(table: "dbo.Transactions", name: "IX_ArticleBatchId", newName: "IX_RawMaterialBatchId");
            RenameColumn(table: "dbo.Transactions", name: "ArticleBatchId", newName: "RawMaterialBatchId");
            CreateIndex("dbo.RawMaterials", "OrderId");
            CreateIndex("dbo.RawMaterials", "JobId");
            CreateIndex("dbo.RawMaterials", "MeasureUnitId");
            CreateIndex("dbo.RawMaterialBatches", "RawMaterialId");
            CreateIndex("dbo.JobRawMaterials", "ArticleBatchId");
            CreateIndex("dbo.JobRawMaterials", "JobId");
            CreateIndex("dbo.BOMs", "ComponentId");
            CreateIndex("dbo.BOMs", "ArticleId");
            AddForeignKey("dbo.RawMaterialBatches", "RawMaterialId", "dbo.RawMaterials", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RawMaterials", "OrderId", "dbo.Orders", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RawMaterials", "MeasureUnitId", "dbo.MeasureUnits", "Id");
            AddForeignKey("dbo.RawMaterials", "JobId", "dbo.Jobs", "Id");
            AddForeignKey("dbo.JobRawMaterials", "JobId", "dbo.Jobs", "Id", cascadeDelete: true);
            AddForeignKey("dbo.JobRawMaterials", "ArticleBatchId", "dbo.ArticleBatches", "Id", cascadeDelete: true);
            AddForeignKey("dbo.BOMs", "ComponentId", "dbo.Articles", "Id");
            AddForeignKey("dbo.BOMs", "ArticleId", "dbo.Articles", "Id");
        }
    }
}
