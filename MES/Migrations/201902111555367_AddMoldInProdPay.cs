namespace MES.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMoldInProdPay : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductionPayments", "MoldId", c => c.String());
            AddColumn("dbo.ProductionPayments", "MoldName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductionPayments", "MoldName");
            DropColumn("dbo.ProductionPayments", "MoldId");
        }
    }
}
