namespace MES.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeIndex : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Orders", "UniqueClientApplicationIndex");
            DropIndex("dbo.CustomerOrders", "UniqueClientApplicationIndex");
        }
        
        public override void Down()
        {
            CreateIndex("dbo.CustomerOrders", new[] { "ClientId", "ApplicationId" }, unique: true, name: "UniqueClientApplicationIndex");
            CreateIndex("dbo.Orders", new[] { "ClientId", "ApplicationId" }, unique: true, name: "UniqueClientApplicationIndex");
        }
    }
}
