namespace MES.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MAINTENANCE : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MaintenancePlanDetailExecuteds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlanId = c.Int(),
                        MaintenancePlanDetailId = c.Int(),
                        extraMaintenance = c.Boolean(nullable: false),
                        MaintenancePlanId = c.Int(),
                        MaintenancePlanOperatorId = c.Int(),
                        ObjectId = c.String(),
                        ObjectType = c.String(),
                        ItemCode = c.String(maxLength: 40),
                        Description = c.String(maxLength: 40),
                        ToolCode = c.String(maxLength: 20),
                        DetailNotes = c.String(maxLength: 2000),
                        Result = c.String(maxLength: 20),
                        ExecutionDate = c.DateTime(nullable: false),
                        QtyProduced = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProductionTime = c.Long(nullable: false),
                        ClientId = c.Int(),
                        ApplicationId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MaintenancePlanOperators", t => t.MaintenancePlanOperatorId)
                .ForeignKey("dbo.MaintenancePlans", t => t.MaintenancePlanId)
                .ForeignKey("dbo.MaintenancePlanDetails", t => t.MaintenancePlanDetailId)
                .Index(t => t.MaintenancePlanDetailId)
                .Index(t => t.MaintenancePlanId)
                .Index(t => t.MaintenancePlanOperatorId);
            
            CreateTable(
                "dbo.MaintenancePlanOperators",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Supplier = c.String(maxLength: 50),
                        SupplierFlag = c.Boolean(nullable: false),
                        OperatorId = c.String(maxLength: 128),
                        MaintenanceId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Executed = c.Boolean(nullable: false),
                        ExecutedDate = c.DateTime(nullable: false),
                        ClientId = c.Int(),
                        ApplicationId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Operators", t => t.OperatorId)
                .Index(t => t.OperatorId);
            
            CreateTable(
                "dbo.MaintenancePlans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 60),
                        CreationDate = c.DateTime(nullable: false),
                        ValidationDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        ExecutionSupplier = c.String(maxLength: 50),
                        ExecutionSupplierFlag = c.Boolean(nullable: false),
                        ExecutionOperator = c.String(maxLength: 50),
                        ApprovalUser = c.String(maxLength: 50),
                        PlanNotes = c.String(maxLength: 2000),
                        QtyFrequency = c.Decimal(precision: 18, scale: 2),
                        QtyFrequencyFlag = c.Boolean(nullable: false),
                        TimeFrequency = c.Long(nullable: false),
                        TimeExpected = c.Long(nullable: false),
                        ClientId = c.Int(),
                        ApplicationId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MaintenancePlanDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaintenancePlanId = c.Int(nullable: false),
                        ItemCode = c.String(maxLength: 40),
                        Description = c.String(maxLength: 40),
                        DetailNotes = c.String(maxLength: 2000),
                        ToolCode = c.String(maxLength: 20),
                        ClientId = c.Int(),
                        ApplicationId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MaintenancePlans", t => t.MaintenancePlanId, cascadeDelete: true)
                .Index(t => t.MaintenancePlanId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MaintenancePlanDetailExecuteds", "MaintenancePlanDetailId", "dbo.MaintenancePlanDetails");
            DropForeignKey("dbo.MaintenancePlanDetails", "MaintenancePlanId", "dbo.MaintenancePlans");
            DropForeignKey("dbo.MaintenancePlanDetailExecuteds", "MaintenancePlanId", "dbo.MaintenancePlans");
            DropForeignKey("dbo.MaintenancePlanDetailExecuteds", "MaintenancePlanOperatorId", "dbo.MaintenancePlanOperators");
            DropForeignKey("dbo.MaintenancePlanOperators", "OperatorId", "dbo.Operators");
            DropIndex("dbo.MaintenancePlanDetails", new[] { "MaintenancePlanId" });
            DropIndex("dbo.MaintenancePlanOperators", new[] { "OperatorId" });
            DropIndex("dbo.MaintenancePlanDetailExecuteds", new[] { "MaintenancePlanOperatorId" });
            DropIndex("dbo.MaintenancePlanDetailExecuteds", new[] { "MaintenancePlanId" });
            DropIndex("dbo.MaintenancePlanDetailExecuteds", new[] { "MaintenancePlanDetailId" });
            DropTable("dbo.MaintenancePlanDetails");
            DropTable("dbo.MaintenancePlans");
            DropTable("dbo.MaintenancePlanOperators");
            DropTable("dbo.MaintenancePlanDetailExecuteds");
        }
    }
}
