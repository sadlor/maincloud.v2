namespace MES.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PartProgram : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PartPrograms", "AssetGroupId", c => c.String());
            AddColumn("dbo.PartPrograms", "AssetId", c => c.String());
            AddColumn("dbo.PartPrograms", "ArticleType", c => c.String());
            DropColumn("dbo.PartPrograms", "MachineId");
            DropColumn("dbo.PartPrograms", "CycleLength");

            DropForeignKey("dbo.PartPrograms", "ArticleId", "dbo.Articles");
            DropIndex("dbo.PartPrograms", new[] { "ArticleId" });
            AlterColumn("dbo.PartPrograms", "ArticleId", c => c.Int(nullable: true));
            CreateIndex("dbo.PartPrograms", "ArticleId");
            AddForeignKey("dbo.PartPrograms", "ArticleId", "dbo.Articles", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PartPrograms", "CycleLength", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PartPrograms", "MachineId", c => c.String());
            DropColumn("dbo.PartPrograms", "ArticleType");
            DropColumn("dbo.PartPrograms", "AssetId");
            DropColumn("dbo.PartPrograms", "AssetGroupId");

            DropForeignKey("dbo.PartPrograms", "ArticleId", "dbo.Articles");
            DropIndex("dbo.PartPrograms", new[] { "ArticleId" });
            AlterColumn("dbo.PartPrograms", "ArticleId", c => c.Int(nullable: false));
            CreateIndex("dbo.PartPrograms", "ArticleId");
            AddForeignKey("dbo.PartPrograms", "ArticleId", "dbo.Articles", "Id", cascadeDelete: true);
        }
    }
}
