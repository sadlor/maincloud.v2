﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class JobService : BaseService<MesDbContext>
    {
        public Job GetJobByAsset(string assetId)
        {
            try
            {
                return DBContext.Jobs.Where(x => x.AssetId == assetId).OrderByDescending(x => x.StartDate).First();
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        //public List<SelectOrder> GetProductionOrderByAsset(string assetId, string applicationId = null)
        //{
        //    if (string.IsNullOrEmpty(applicationId))
        //    {
        //        applicationId = MultiTenantsHelper.ApplicationId;
        //    }
        //    if (string.IsNullOrEmpty(applicationId))
        //    {
        //        applicationId = Helper.OperatorHelper.ApplicationId;
        //    }

        //    try
        //    {
        //        string statusCode = ((int)MesEnum.OrderStatusCode.Esecutiva).ToString(),
        //               suspendedStatusCode = ((int)MesEnum.OrderStatusCode.Sospesa).ToString();

        //        return (from j in DBContext.Jobs
        //                     join o in DBContext.Orders on j.OrderId equals o.Id
        //                     join co in DBContext.CustomerOrders on o.CustomerOrderId equals co.Id
        //                     join a in DBContext.Articles on o.IdArticle equals a.Id
        //                     where j.AssetId == assetId && j.ApplicationId == applicationId && (o.StatusCode == statusCode || o.StatusCode == suspendedStatusCode)
        //                     orderby j.StartDate
        //                     select new SelectOrder
        //                     {
        //                         Id = j.Id,
        //                         Code = j.Code,
        //                         Description = j.Description,
        //                         Order = o.OrderCode,
        //                         CustomerOrder = co.OrderCode,
        //                         ArticleCode = a.Code,
        //                         ArticleDescr = a.Description,
        //                         StartDate = j.StartDate,
        //                         EndDate = j.EndDate
        //                     }).ToList();

        //        //return DBContext.Jobs.Include("Order.CustomerOrder").Include("Order.Article").Where(x => x.AssetId == assetId && x.ApplicationId == applicationId && (x.Order.StatusCode == statusCode || x.Order.StatusCode == suspendedStatusCode)).OrderBy(x => x.StartDate).ThenBy(x => x.Order.StatusCode).ToList();
        //    }
        //    catch (InvalidOperationException)
        //    {
        //        return null;
        //    }
        //}

        public List<Models.Views.JobOrderView> GetProductionOrderByAsset(string assetId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = Helper.OperatorHelper.ApplicationId;
            }

            try
            {
                //string statusCode = ((int)MesEnum.OrderStatusCode.Esecutiva).ToString(),
                //       suspendedStatusCode = ((int)MesEnum.OrderStatusCode.Sospesa).ToString();

                return DBContext.SelectJobOrders.Where(x => x.AssetId == assetId && x.ApplicationId == applicationId).OrderBy(x => x.StartDate).ThenBy(x => x.OrderStatusCode).ToList();
                //return DBContext.Jobs.Include("Order.CustomerOrder").Include("Order.Article").Where(x => x.AssetId == assetId && x.ApplicationId == applicationId && (x.Order.StatusCode == statusCode || x.Order.StatusCode == suspendedStatusCode)).OrderBy(x => x.StartDate).ThenBy(x => x.Order.StatusCode).ToList();
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public List<Job> GetProductionJobListByAsset(string assetId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = Helper.OperatorHelper.ApplicationId;
            }

            try
            {
                string statusCode = ((int)MesEnum.OrderStatusCode.Esecutiva).ToString(),
                       suspendedStatusCode = ((int)MesEnum.OrderStatusCode.Sospesa).ToString();
                return DBContext.Jobs.Include("Order.CustomerOrder").Include("Order.Article").Where(x => x.AssetId == assetId && x.ApplicationId == applicationId && (x.Order.StatusCode == statusCode || x.Order.StatusCode == suspendedStatusCode)).OrderBy(x => x.StartDate).ThenBy(x => x.Order.StatusCode).ToList();
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public List<SelectOrder> GetProductionOrderByAssetGroup(string groupId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = Helper.OperatorHelper.ApplicationId;
            }

            try
            {
                string statusCode = ((int)MesEnum.OrderStatusCode.Esecutiva).ToString(),
                       suspendedStatusCode = ((int)MesEnum.OrderStatusCode.Sospesa).ToString();

                //Get list of machine with same group
                AssetManagement.Services.AssetGroupService service = new AssetManagement.Services.AssetGroupService();
                var idList = service.GetAssetsByGroup(groupId, applicationId).Select(x => x.Id).ToList();

                return (from j in DBContext.Jobs
                        join o in DBContext.Orders on j.OrderId equals o.Id
                        join co in DBContext.CustomerOrders on o.CustomerOrderId equals co.Id
                        join a in DBContext.Articles on o.IdArticle equals a.Id
                        where idList.Contains(j.AssetId) && j.ApplicationId == applicationId && (o.StatusCode == statusCode || o.StatusCode == suspendedStatusCode)
                        orderby j.StartDate, o.StatusCode
                        select new SelectOrder
                        {
                            Id = j.Id,
                            Code = j.Code,
                            Description = j.Description,
                            Order = o.OrderCode,
                            CustomerOrder = co.OrderCode,
                            ArticleCode = a.Code,
                            ArticleDescr = a.Description,
                            StartDate = j.StartDate,
                            EndDate = j.EndDate
                        }).ToList();                
               
                //return DBContext.Jobs.Include("Order.CustomerOrder").Include("Order.Article").Where(x => idList.Contains(x.AssetId) && x.ApplicationId == applicationId && (x.Order.StatusCode == statusCode || x.Order.StatusCode == suspendedStatusCode)).OrderBy(x => x.StartDate).ThenBy(x => x.Order.StatusCode).ToList();
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public List<Job> GetProductionJobListByAssetGroup(string groupId, string applicationId = null)
        {
            if (!string.IsNullOrEmpty(groupId))
            {
                if (string.IsNullOrEmpty(applicationId))
                {
                    applicationId = MultiTenantsHelper.ApplicationId;
                }
                if (string.IsNullOrEmpty(applicationId))
                {
                    applicationId = Helper.OperatorHelper.ApplicationId;
                }

                //Get list of machine with same group
                AssetManagement.Services.AssetGroupService service = new AssetManagement.Services.AssetGroupService();
                var idList = service.GetAssetsByGroup(groupId, applicationId).Select(x => x.Id).ToList();
                string statusCode = ((int)MesEnum.OrderStatusCode.Esecutiva).ToString(),
                       suspendedStatusCode = ((int)MesEnum.OrderStatusCode.Sospesa).ToString();
                return DBContext.Jobs.Include("Order.CustomerOrder").Include("Order.Article").Where(x => idList.Contains(x.AssetId) && x.ApplicationId == applicationId && (x.Order.StatusCode == statusCode || x.Order.StatusCode == suspendedStatusCode)).OrderBy(x => x.StartDate).ThenBy(x => x.Order.StatusCode).ToList();
            }
            else
            {
                return null;
            }
        }

        public List<Job> GetJobListByAsset(string assetId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = Helper.OperatorHelper.ApplicationId;
            }

            try
            {
                return DBContext.Jobs.Include("Order.CustomerOrder").Include("Order.Article").Where(x => x.AssetId == assetId && x.ApplicationId == applicationId).OrderBy(x => x.StartDate).ToList();
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public List<SelectOrder> GetProductionOrderCodeFiltered(string orderCode, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = Helper.OperatorHelper.ApplicationId;
            }

            try
            { 
                return (from j in DBContext.Jobs
                        join o in DBContext.Orders on j.OrderId equals o.Id
                        join co in DBContext.CustomerOrders on o.CustomerOrderId equals co.Id
                        join a in DBContext.Articles on o.IdArticle equals a.Id
                        where j.ApplicationId == applicationId && co.OrderCode.Contains(orderCode)
                        orderby j.StartDate
                        select new SelectOrder
                        {
                            Id = j.Id,
                            Code = j.Code,
                            Description = j.Description,
                            Order = o.OrderCode,
                            CustomerOrder = co.OrderCode,
                            ArticleCode = a.Code,
                            ArticleDescr = a.Description,
                            StartDate = j.StartDate,
                            EndDate = j.EndDate
                        }).ToList();
                //return DBContext.Jobs.Include("Order.CustomerOrder").Include("Order.Article").Where(x => x.Order.CustomerOrder.OrderCode.Contains(orderCode) && x.ApplicationId == applicationId).OrderBy(x => x.StartDate).ToList();
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        /// <summary>
        /// Get list of job where customerOrder's OrderCode CONTAINS orderCode
        /// </summary>
        /// <param name="orderCode">string used to filter</param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        public List<Job> GetJobListOrderCodeFiltered(string orderCode, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = Helper.OperatorHelper.ApplicationId;
            }

            try
            {
                return DBContext.Jobs.Include("Order.CustomerOrder").Include("Order.Article").Where(x => x.Order.CustomerOrder.OrderCode.Contains(orderCode) && x.ApplicationId == applicationId).OrderBy(x => x.StartDate).ToList();
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public string GetJobDescriptionById(string jobId)
        {
            try
            {
                return DBContext.Jobs.Where(x => x.Id == jobId).First().Description;
            }
            catch (InvalidOperationException)
            {
                return string.Empty;
            }
        }

        public string GetJobCodeById(string jobId)
        {
            try
            {
                return DBContext.Jobs.Where(x => x.Id == jobId).First().Code;
            }
            catch (InvalidOperationException)
            {
                return string.Empty;
            }
        }

        public Article GetArticleByJobId(string jobId)
        {
            try
            {
                return DBContext.Jobs.Include("Order.Article").Where(x => x.Id == jobId).Select(x => x.Order.Article).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string GetArticleDescriptionByJobId(string jobId)
        {
            try
            {
                return DBContext.Jobs.Include("Order.Article").Where(x => x.Id == jobId).First().Order.Article.Description;
            }
            catch (InvalidOperationException)
            {
                return string.Empty;
            }
        }

        public string GetArticleCodeByJobId(string jobId)
        {
            try
            {
                return DBContext.Jobs.Where(x => x.Id == jobId).First().Order.Article.Code;
            }
            catch (InvalidOperationException)
            {
                return string.Empty;
            }
        }

        public List<Job> GetJobListByCustomerOrder(int orderId)
        {
            return DBContext.Jobs.Where(x => x.Order.CustomerOrder.Id == orderId).ToList();
        }

        public Dictionary<Article, List<Article>> GetBOM(string jobId)
        {
            if (string.IsNullOrEmpty(jobId))
            {
                return null;
            }
            //var query = DBContext.Jobs.Include("Order").Where(x => x.Id == jobId).Select(x => x.Order.IdArticle);
            //return DBContext.BOMs.Where(x => x.ArticleId == query.FirstOrDefault()).ToList();
            ////la distinta base va salvata, e ogni volta che cambia un componente va creata una nuova
            Article a = DBContext.Jobs.Include("Order.Article").Where(x => x.Id == jobId).Select(x => x.Order.Article).FirstOrDefault();
            var list = DBContext.Articles.Where(x => x.JobId == jobId).ToList();
            return new Dictionary<Article, List<Article>>() { { a, list } };
        }

        public void OpenJobBatch(string assetId, string jobId, int batchId, string operatorId, decimal quantity = 0, bool open = true)
        {
            JobBatch transaction = new JobBatch();
            transaction.ArticleBatchId = batchId;
            transaction.JobId = jobId;
            transaction.MachineId = assetId;
            transaction.OperatorId = operatorId;
            transaction.Start = DateTime.Now;
            if (!open)
            {
                transaction.End = DateTime.Now;
            }
            transaction.Quantity = quantity;

            DBContext.JobBatches.Add(transaction);
            DBContext.SaveChanges();
        }

        public void CloseJobBatch(string machineId, string jobId, int batchId)
        {
            //JobBatch transaction = DBContext.JobBatches.Where(x => x.MachineId == machineId && x.JobId == jobId && x.ArticleBatchId == batchId).FirstOrDefault();
            JobBatch transaction = DBContext.JobBatches.Where(x => x.Id == batchId).FirstOrDefault();
            if (transaction != null)
            {
                transaction.End = DateTime.Now;

                DBContext.Entry(transaction).State = System.Data.Entity.EntityState.Modified;
                DBContext.SaveChanges();
            }
        }

        public void CloseAndOpenBatch(string machineId, string jobId, int batchId, string operatorId, int articleId, decimal quantity = 0, bool open = true)
        {
            List<int> list = DBContext.JobBatches.Where(x => x.MachineId == machineId && x.JobId == jobId && x.End == null && x.ArticleBatch.ArticleId == articleId).Select(x => x.Id).ToList();
            foreach (int item in list)
            {
                CloseJobBatch(machineId, jobId, item);
            }
            OpenJobBatch(machineId, jobId, batchId, operatorId, quantity, open);
        }

        public decimal GetProducedByJob(string jobId)
        {
            try
            {
                return DBContext.Transactions.Where(x => x.JobId == jobId).Sum(x => (x.PartialCounting - x.BlankShot) * x.CavityMoldNum);
            }
            catch (ArgumentNullException)
            {
                return 0;
            }
        }
    }
}