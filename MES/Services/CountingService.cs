﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class CountingService : BaseService<MesDbContext>
    {
        const string CAUSALE_PROD = "Produzione";
        private CountingDataRepository countingRepository = new CountingDataRepository();

        public void CreateDemo()
        {
            CreateDemo(new DateTime(2017, 02, 28), new DateTime(2017, 05, 31), "05198807-13eb-42a5-a8a5-5a3642623709");
        }

        public void CreateDemo(DateTime start, DateTime end, string jobId, string macchinaId = null)
        {
            //1 turno 06:00 - 14:00
            //2 turno 14:00 - 22:00

            string analyzerId = null;  //"34ffcea2-baa2-4858-9a30-b64966a8a0a4";
            //string macchinaId = null; //"1022c77b-5c53-47ef-b46b-27e9d7d4ead5";
            string operatorId = null; //"dc635694-d3ae-4d1c-9480-c6c9e4694072";
            //string jobId = "05198807-13eb-42a5-a8a5-5a3642623709";

            int progressivo = 0;
            try
            {
                progressivo = (int)DBContext.CountingDatas.Where(x => x.JobId == jobId).Max(x => x.ProgressiveCounting);
            }
            catch (Exception)
            {}

            List<CountingData> DataSource = new List<CountingData>();
            CountingData inizio = null;
            CauseService causeService = new CauseService();

            //for (DateTime dayStart = new DateTime(2017, 01, 30); dayStart < DateTime.Today.AddMonths(1); dayStart = dayStart.AddDays(1))
            for (DateTime dayStart = start; dayStart < end.AddDays(1); dayStart = dayStart.AddDays(1))
            {
                //string jobId = dayStart.ToString("yyyyMMdd");//"20161215";

                List<CountingData> dataList = new List<CountingData>();
                Random r = new Random();
                DateTime tempo = dayStart;
                int lavoro = 0;
                int pausa = 0;
                int datoRandom = 0;
                while (tempo.Day == dayStart.Day)
                {
                    if ((tempo.Hour >= 6 && tempo.Hour <= 14) || (tempo.Hour >= 14 && tempo.Hour < 22))
                    {
                        datoRandom = r.Next(25, 50);
                        lavoro = r.Next(4, 9);
                        pausa = r.Next(1, 3);
                        for (int i = 0; i < lavoro; i++)
                        {
                            if (tempo.Hour < 22)
                            {
                                int tRand = r.Next(1, 15);
                                CountingData cd = new CountingData();
                                cd.AssetId = macchinaId;
                                cd.CountingAnalyzerId = analyzerId;
                                //cd.Cause = new Cause() { Description = CountingEnum.Causale.Produzione.ToString() };
                                cd.Cause = causeService.GetCauseByDescription(CAUSALE_PROD);
                                cd.OperatorId = operatorId;
                                cd.JobId = jobId;
                                cd.Start = tempo;
                                cd.End = tempo.AddMinutes(tRand);
                                cd.ProgressiveCounting = progressivo;
                                cd.PartialCounting = datoRandom / lavoro;
                                dataList.Add(cd);
                                tempo = tempo.AddMinutes(tRand);
                                progressivo += datoRandom / lavoro;
                            }
                            else
                            {
                                break;
                            }
                        }
                        for (int i = 0; i < pausa; i++)
                        {
                            if (tempo.Hour < 22)
                            {
                                int tRand = r.Next(1, 15);
                                CountingData cd = new CountingData();
                                cd.AssetId = macchinaId;
                                cd.CountingAnalyzerId = analyzerId;
                                cd.OperatorId = operatorId;
                                cd.JobId = jobId;
                                //cd.Cause = new Cause();
                                cd.Start = tempo;
                                cd.End = tempo.AddMinutes(tRand);
                                cd.ProgressiveCounting = progressivo;
                                cd.PartialCounting = 0;
                                dataList.Add(cd);
                                tempo = tempo.AddMinutes(tRand);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        //produzione = 0
                        CountingData cd = new CountingData();
                        cd.AssetId = macchinaId;
                        cd.CountingAnalyzerId = analyzerId;
                        cd.OperatorId = operatorId;
                        cd.JobId = jobId;
                        //cd.Cause = new Cause();
                        cd.Start = tempo;
                        cd.ProgressiveCounting = progressivo;
                        cd.PartialCounting = 0;
                        dataList.Add(cd);
                        tempo = tempo.AddMinutes(15);
                    }
                }

                CountingData fine = new CountingData();
                for (int i = 0; i < dataList.Count - 1; i++)
                {
                    if (dataList[i].Cause != dataList[i + 1].Cause)  //cambio Evento
                    {
                        if (inizio == null)
                        {
                            inizio = new CountingData();
                            inizio = dataList[i + 1];
                        }
                        else
                        {
                            fine = dataList[i + 1];
                            inizio.End = fine.Start;
                            inizio.Duration = (int)(inizio.End.Value - inizio.Start).TotalMinutes;
                            inizio.PartialCounting = fine.ProgressiveCounting - inizio.ProgressiveCounting;
                            inizio.AverageCounting = Math.Round(inizio.PartialCounting / (decimal)TimeSpan.FromMinutes(inizio.Duration).TotalHours, 3);
                            DataSource.Add(inizio);
                            inizio = fine;
                        }
                    }
                }
            }
            countingRepository.InsertAll(DataSource);
        }

        public List<Cause> CauseList()
        {
            return DBContext.Causes.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Description).ToList();
        }

        public List<Job> JobList()
        {
            return DBContext.Jobs.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Code).ToList();
        }

        public List<Operator> OperatorList()
        {
            return DBContext.Operators.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.UserName).ToList();
        }

        public void ChangeRecordOperatorJob(CountingData recordStart)
        {
            //nel momento in cui setto manualmente un job o un operatore, tutti i record successivi,
            //con uguale operatore (o senza) e senza job, vanno settati con lo stesso job/operatore

            List<CountingData> listRecordToChange = new List<CountingData>();

            //!(x.OperatorId == null || x.OperatorId.Equals("")
            try
            {
                CountingData recordStop = countingRepository.ReadAll(x => x.Start >= recordStart.End &&
                                                                     x.AssetId == recordStart.AssetId &&
                                                                     ((x.OperatorId != recordStart.OperatorId && !string.IsNullOrEmpty(x.OperatorId)) ||
                                                                     (x.JobId != recordStart.JobId && !string.IsNullOrEmpty(x.JobId)))).First();
                listRecordToChange = countingRepository.ReadAll(x => x.Start >= recordStart.End && x.Start < recordStop.Start &&
                                                                x.AssetId == recordStart.AssetId &&
                                                                (string.IsNullOrEmpty(x.OperatorId) || x.OperatorId == recordStart.OperatorId) &&
                                                                (string.IsNullOrEmpty(x.JobId) || x.JobId == recordStart.JobId)).ToList();
            }
            catch (InvalidOperationException ex)
            {
                listRecordToChange = countingRepository.ReadAll(x => x.Start >= recordStart.End &&
                                                                x.AssetId == recordStart.AssetId &&
                                                                (string.IsNullOrEmpty(x.OperatorId) || x.OperatorId == recordStart.OperatorId) &&
                                                                (string.IsNullOrEmpty(x.JobId) || x.JobId == recordStart.JobId)).ToList();
            }

            foreach (CountingData item in listRecordToChange)
            {
                if (!string.IsNullOrEmpty(recordStart.OperatorId))
                {
                    item.OperatorId = recordStart.OperatorId;
                }
                if (!string.IsNullOrEmpty(recordStart.JobId))
                {
                    item.JobId = recordStart.JobId;
                }
                countingRepository.Update(item);
            }

            countingRepository.SaveChanges();
        }

        public TimeSpan TotalDuration(DateTime start, DateTime end, string countingAnalyzerId)
        {
            end = end.AddDays(1);
            return TimeSpan.FromMinutes(DBContext.CountingDatas.Where(x => x.Start >= start && x.End.Value <= end && x.CountingAnalyzerId == countingAnalyzerId).Sum(x => x.Duration));
        }

        public TimeSpan TotalProductionTime(DateTime start, DateTime end, string countingAnalyzerId)
        {
            end = end.AddDays(1);
            return TimeSpan.FromMinutes(DBContext.CountingDatas.Where(x => x.Start >= start && x.End.Value <= end && x.Cause.Description == CAUSALE_PROD && x.CountingAnalyzerId == countingAnalyzerId).Sum(x => x.Duration));
        }

        public TimeSpan TotalStopTime(DateTime start, DateTime end, string countingAnalyzerId)
        {
            end = end.AddDays(1);
            return TimeSpan.FromMinutes(DBContext.CountingDatas.Where(x => x.Start >= start && x.End.Value <= end && x.Cause.Description != CAUSALE_PROD && x.CountingAnalyzerId == countingAnalyzerId).Sum(x => x.Duration));
        }

        public decimal PartialCountingInPeriod(DateTime start, DateTime end, string countingAnalyzerId)
        {
            end = end.AddDays(1);
            return DBContext.CountingDatas.Where(x => x.Start >= start && x.End.Value <= end && x.CountingAnalyzerId == countingAnalyzerId).Sum(x => x.PartialCounting);
        }
    }
}