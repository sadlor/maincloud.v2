﻿using AssetManagement.Models;
using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class ArticleMoldService : BaseService<MesDbContext>
    {
        public List<ArticleMold> GetMoldListByJobId(string jobId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            try
            {
                Job j = DBContext.Jobs.Include("Order.Article").Where(x => x.Id == jobId && x.ApplicationId == applicationId).First();
                return DBContext.ArticleMolds.Where(x => x.ArticleId == j.Order.Article.Id).ToList();
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public Mold GetMoldById(string moldId)
        {
            AssetManagement.Repositories.MoldRepository mRep = new AssetManagement.Repositories.MoldRepository();
            return mRep.FindByID(moldId);
        }

        public List<Mold> GetMoldListByArticleId(int articleId)
        {
            var list = DBContext.ArticleMolds.Where(x => x.ArticleId == articleId).ToList();
            AssetManagement.Repositories.MoldRepository mRep = new AssetManagement.Repositories.MoldRepository();
            List<Mold> returnList = new List<Mold>();
            foreach (var item in list)
            {
                returnList.Add(mRep.FindByID(item.MoldId));
            }
            return returnList;
        }

        public List<ArticleMold> GetArticleMoldList()
        {
            return DBContext.ArticleMolds.Where(x => x.Article.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
        }

        public void AddNew(ArticleMold entity)
        {
            DBContext.ArticleMolds.Add(entity);
            DBContext.SaveChanges();
            // Load author name
            DBContext.Entry(entity).Reference(x => x.Article).Load();
        }

        public void DeleteAssociation(ArticleMold entity)
        {
            DBContext.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
            DBContext.SaveChanges();
        }

        public ArticleMold FindArticleMold(string moldId, int articleId)
        {
            return DBContext.ArticleMolds.Where(x => x.MoldId == moldId && x.ArticleId == articleId).FirstOrDefault();
        }
    }
}