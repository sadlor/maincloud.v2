﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Configuration;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class PartProgramService : BaseService<MesDbContext>
    {
        public List<PartProgram> GetAllArticleWithoutPP()
        {
            return DBContext.PartPrograms.Where(x => string.IsNullOrEmpty(x.PartProgramCode)).ToList();
        }

        public List<PartProgram> GetAll()
        {
            return DBContext.PartPrograms.ToList();
        }

        public PartProgram GetPartProgram(int articleId)
        {
            return DBContext.PartPrograms.Where(x => x.ArticleId == articleId).FirstOrDefault();            
        }

        public PartProgram GetPartProgram(string query, int articleId, string assetId = null, string assetGroupId = null, string articleType = null)
        {
            if (articleType == null)
            {
                articleType = "";
            }
            if (assetGroupId == null)
            {
                assetGroupId = "";
            }
            return DBContext.Database.SqlQuery<PartProgram>(
                query, 
                new SqlParameter("@ArticleId", articleId),
                new SqlParameter("@ArticleType", articleType),
                new SqlParameter("@AssetId", assetId),
                new SqlParameter("@AssetGroupId", assetGroupId)).FirstOrDefault();
        }

        /// <summary>
        /// Aggiunge assetGroup, asset, classe articolo e articolo senza partprogram
        /// </summary>
        public void AddRecordToTable(string query)
        {
            DBContext.Database.ExecuteSqlCommand(query, new SqlParameter("@ApplicationId", MultiTenantsHelper.ApplicationId));
        }

        public void CopyPartProgram(string assetId, string ppCode)
        {
            if (string.IsNullOrEmpty(ppCode))
            {
                return;
            }

            AssetManagement.Repositories.AssetRepository repo = new AssetManagement.Repositories.AssetRepository();
            string machineDir = repo.GetPartProgramPath(assetId);//@"c:\archives\2008";
            machineDir = Path.Combine(machineDir, ppCode);

            ConfigurationService config = new ConfigurationService();
            string sourceDir = config.GetByParameter(ParamContext.PartProgram).Where(x => x.ParamName == ConfigParam.DestinationPath.ToString()).FirstOrDefault().Value;
            sourceDir = Path.Combine(sourceDir, repo.GetCode(assetId));
            Directory.CreateDirectory(sourceDir);

            try
            {
                string[] ppList = Directory.GetFiles(machineDir, ppCode); //+ ".*"

                // Copy file.
                foreach (string f in ppList)
                {
                    // Remove path from the file name.
                    string fName = f.Substring(machineDir.Length + 1);

                    // Use the Path.Combine method to safely append the file name to the path.
                    // Will overwrite if the destination file already exists.
                    File.Copy(Path.Combine(machineDir, fName), Path.Combine(sourceDir, fName), true);

                    // Will not overwrite if the destination file already exists.
                    //try
                    //{
                    //    File.Copy(Path.Combine(sourceDir, fName), Path.Combine(machineDir, fName));
                    //}
                    //// Catch exception if the file was already copied.
                    //catch (IOException copyError)
                    //{
                    //    Console.WriteLine(copyError.Message);
                    //}
                }
            }
            catch (DirectoryNotFoundException)
            {
            }
        }
    }
}