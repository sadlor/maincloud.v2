﻿using MainCloudFramework.Services;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class IndexService : BaseService<MesDbContext>
    {
        public decimal OEE(decimal availability, decimal efficiency, decimal quality)
        {
            return Math.Round((availability * efficiency * quality) * 100, 2);
        }

        public decimal Availability(TimeSpan productionTime, TimeSpan totTimeOn)
        {
            return Availability((decimal)productionTime.TotalSeconds, (decimal)totTimeOn.TotalSeconds);
        }

        public decimal Availability(decimal productionTime, decimal totTimeOn)
        {
            if (totTimeOn > 0)
            {
                return Math.Round((productionTime / totTimeOn) * 100, 2);
            }
            else
            {
                return 0;
            }
        }

        public decimal Quality(decimal qtyProd, decimal qtyWaste)
        {
            if (qtyProd > 0)
            {
                return Math.Round(((qtyProd - qtyWaste) / qtyProd) * 100, 2);
            }
            else
            {
                return 0;
            }
        }

        public decimal Efficiency(TimeSpan productionTime, TimeSpan theoreticalTime)
        {
            return Efficiency((decimal)productionTime.TotalSeconds, (decimal)theoreticalTime.TotalSeconds);
        }

        public decimal Efficiency(decimal productionTime, decimal theoreticalTime)
        {
            if (productionTime > 0)
            {
                return Math.Round((theoreticalTime / productionTime) * 100, 2);
            }
            else
            {
                return 0;
            }
        }

        public decimal Efficiency(decimal nStampateOra, decimal qtyProd, TimeSpan productionTime)
        {
            if (nStampateOra > 0)
            {
                decimal oreProdTeorico = Math.Round(qtyProd / nStampateOra, 0);
                return Efficiency((decimal)productionTime.TotalHours, oreProdTeorico);
            }
            else
            {
                return 0;
            }
        }
    }
}