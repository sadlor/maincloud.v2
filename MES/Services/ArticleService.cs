﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class ArticleService : BaseService<MesDbContext>
    {
        public string GetArticleDescriptionById(int articleId)
        {
            try
            {
                return DBContext.Articles.Where(x => x.Id == articleId).First().Description;
            }
            catch (InvalidOperationException)
            {
                return string.Empty;
            }
        }
    }
}