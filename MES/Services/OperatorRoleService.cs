﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Helper;
using MES.Models;
using MES.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class OperatorRoleService : BaseService<MesDbContext>
    {
        private OperatorRepository userRepository = new OperatorRepository();
        
        public List<MesRole> RolesPerOperator(string operId)
        {
            string applicationId = MultiTenantsHelper.ApplicationId;
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = OperatorHelper.ApplicationId;
            }

            List<MesRole> returnList = new List<MesRole>();
            if (!string.IsNullOrEmpty(operId))
            {
                Operator op = DBContext.Operators.Where(x => x.Id == operId && x.ApplicationId == applicationId).FirstOrDefault();
                if (op.Roles == null)
                {
                    return returnList;
                }
                foreach (var role in op?.Roles)
                {
                    returnList.Add(FindRoleById(role.RoleId));
                }
            }
            return returnList;
        }

        public MesRole FindRoleByNameInApplication(string name)
        {
            string applicationId = MultiTenantsHelper.ApplicationId;
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = OperatorHelper.ApplicationId;
            }

            return DBContext.MesRoles.Where(x => x.ApplicationId == applicationId && x.Name == name).FirstOrDefault();
        }

        public MesRole FindRoleById(string roleId)
        {
            return DBContext.MesRoles.Where(x => x.Id == roleId).First();
        }

        public List<Operator> OperatorsInCurrentApplication()
        {
            var applicationId = MultiTenantsHelper.ApplicationId;
            return OperatorsInApplication(applicationId);
        }

        public List<Operator> OperatorsInApplication(string applicationId)
        {
            var res = (from r in DBContext.Operators
                       where r.ApplicationId == applicationId
                       orderby r.UserName
                       select r).ToList();
            return res;
        }

        public List<MesRole> RolesInCurrentApplication()
        {
            var applicationId = MultiTenantsHelper.ApplicationId;
            return RolesInApplication(applicationId);
        }

        public List<MesRole> RolesInCurrentApplication(List<string> excludeRoles)
        {
            var roles = RolesInCurrentApplication().Where(x => !excludeRoles.Contains(x.Name)).ToList();
            return roles;
        }

        public List<MesRole> RolesInApplication(string applicationId)
        {
            var res = (from r in DBContext.MesRoles
                       where r.ApplicationId == applicationId
                       select r).ToList();
            return res;
        }

        /// <summary>
        /// Ricerca l'utente per id, identifica l'applicazione tramite application name presente nll'url.
        /// Ricerca il ruolo di un utente e restituisce la relativa entity
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public MesRole FindUserRoleCurrentApp(string userId)
        {
            var user = userRepository.FindByID(userId);
            return FindUserRoleCurrentApp(user);
        }

        /// <summary>
        /// Ricerca l'utente, identifica l'applicazione tramite application name presente nll'url.
        /// Ricerca il ruolo di un utente e restituisce la relativa entity
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public MesRole FindUserRoleCurrentApp(Operator user)
        {
            var appId = MultiTenantsHelper.ApplicationId;
            if (string.IsNullOrEmpty(appId))
            {
                appId = OperatorHelper.ApplicationId;
            }

            var appRoles = RolesInApplication(appId);
            var userRoles = user.Roles.Where(x => appRoles.Where(y => y.Users.Where(u => u.RoleId == x.RoleId).Any()).Any()).SingleOrDefault();
            if (userRoles != null)
            {
                var role = DBContext.MesRoles.Find(userRoles.RoleId);
                return role;
            }
            return null;
        }

        public void ChangeRoleAndUpdateUser(string newRoleId, Operator user)
        {
            if (!string.IsNullOrEmpty(newRoleId))
            {
                var currentUserRole = FindUserRoleCurrentApp(user);

                if (currentUserRole != null)
                {
                    user.Roles.Remove(user.Roles.Where(x => x.RoleId == currentUserRole.Id).SingleOrDefault());
                }

                user.Roles.Add(new MesOperatorRole()
                {
                    RoleId = newRoleId,
                    UserId = user.Id
                });
            }
            DBContext.SaveChanges();
        }

        public IdentityResult CreateUserCurrentApplication(Operator user)
        {
            return CreateUser(user, MultiTenantsHelper.ApplicationId);
        }

        public IdentityResult CreateUser(Operator user, string applicationId)
        {
            //IdentityResult result = null;
            //var role = roleRepository.FindGuestRole(applicationId);
            //if (role == null)
            //{
            //    role = GetRoleGuest(applicationId);
            //    roleRepository.Insert(role);
            //}

            //var userStore = new ApplicationUserStore<ApplicationUser>(DBContext) { ApplicationId = applicationId };
            //var userManager = new UserManager<ApplicationUser, string>(userStore);
            //result = userManager.Create(user, password);

            //if (result.Succeeded)
            //{
            //    user.Roles.Add(new IdentityUserRole()
            //    {
            //        RoleId = role.Id,
            //        UserId = user.Id
            //    });

            //    result = userManager.Update(user);
            //}
            //return result;

            user.ApplicationId = applicationId;
            userRepository.Insert(user);
            try
            {
                userRepository.SaveChanges();
                return IdentityResult.Success;
                //result = IdentityResult.Success;
            }
            catch (DbEntityValidationException exdb)
            {
                string msg = "";
                foreach (var e1 in exdb.EntityValidationErrors)
                {
                    foreach (var error in e1.ValidationErrors)
                    {
                        msg = string.Join(",", error.ErrorMessage);
                    }
                }
                return new IdentityResult(msg);
                //result = new IdentityResult(ex.Message);
            }
            catch (Exception ex)
            {
                return new IdentityResult(ex.Message);
            }
        }
    }
}