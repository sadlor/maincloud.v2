﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class CauseService : BaseService<MesDbContext>
    {
        public Cause GetCauseByDescription(string descr)
        {
            try
            {
                return DBContext.Causes.Where(x => x.Description == descr && x.ApplicationId == MultiTenantsHelper.ApplicationId).First();
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            catch (ArgumentNullException)
            {
                return null;
            }
        }

        //public List<Cause> GetCausaleFermoList()
        //{
        //    //return DBContext.Causes.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && x.Type == "Fermi").ToList(); TODO: sistemare
        //    return null;
        //}

        public Cause GetCauseByCode(string code, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            try
            {
                return DBContext.Causes.Where(x => x.Code == code && x.ApplicationId == applicationId).First();
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            catch (ArgumentNullException)
            {
                return null;
            }
        }

        public string GetCauseDescriptionById(string id, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            try
            {
                return DBContext.Causes.Where(x => x.Id == id && x.ApplicationId == applicationId).First().Description;
            }
            catch (InvalidOperationException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Ritorna la lista di id di tutte le causali appartenenti ad un gruppo
        /// </summary>
        /// <param name="causeGroup">Nome del gruppo causali</param>
        /// <returns></returns>
        public List<string> GetListCauseIdByGroupName(string causeGroup, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            var group = DBContext.CauseTypes.Where(x => x.Description == causeGroup && x.ApplicationId == applicationId).FirstOrDefault();
            if (group != null)
            {
                return DBContext.Causes.Where(x => x.CauseTypeId == group.Id && x.ApplicationId == applicationId).Select(x => x.Id).ToList();
            }
            else
            {
                return null;
            }
        }

        public List<string> GetListCauseIdByGroupName(string[] causeGroups, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            List<string> returnList = new List<string>();
            foreach (string causeGroup in causeGroups)
            {
                var group = DBContext.CauseTypes.Where(x => x.Description == causeGroup && x.ApplicationId == applicationId).FirstOrDefault();
                if (group != null)
                {
                    returnList.AddRange(DBContext.Causes.Where(x => x.CauseTypeId == group.Id && x.ApplicationId == applicationId).Select(x => x.Id).ToList());
                }
                else
                {
                    return null;
                }
            }

            return returnList;
        }

        public List<Cause> GetListCauseByGroupName(string[] causeGroups, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            List<Cause> returnList = new List<Cause>();
            foreach (string causeGroup in causeGroups)
            {
                var group = DBContext.CauseTypes.Where(x => x.Description == causeGroup && x.ApplicationId == applicationId).FirstOrDefault();
                if (group != null)
                {
                    returnList.AddRange(DBContext.Causes.Where(x => x.CauseTypeId == group.Id && x.ApplicationId == applicationId).ToList());
                }
                else
                {
                    return null;
                }
            }

            return returnList;
        }
    }
}