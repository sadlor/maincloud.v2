﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class ProductionPaymentService : BaseService<MesDbContext>
    {
        ProductionPaymentRepository repos = new ProductionPaymentRepository();

        public struct MachineProductionRecord
        {
            public string MachineId { get; set; }
            public string MachineCode { get; set; }
            public decimal QtyTot { get; set; }
            public decimal QtyExport { get; set; }
            public decimal QtyToVerify { get; set; }

            public MachineProductionRecord(string machineId, string machineCode, decimal qtyTot, decimal qtyExport, decimal qtyToVerify)
            {
                MachineId = machineId;
                MachineCode = machineCode;
                QtyTot = qtyTot;
                QtyExport = qtyExport;
                QtyToVerify = qtyToVerify;
            }
        }

        /// <summary>
        /// Return machine list with production payment not verified
        /// </summary>
        /// <returns></returns>
        public List<MachineProductionRecord> MachineListWithProduction(string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = Helper.OperatorHelper.ApplicationId;
            }
            //where !Transaction.Verified

            AssetManagement.Repositories.AssetRepository AR = new AssetManagement.Repositories.AssetRepository();
            List<string> allMachineIdList = AR.ReadAll(x => x.ApplicationId == applicationId).Select(x => x.Id).ToList();

            var transList = DBContext.Transactions.Where(x => allMachineIdList.Contains(x.MachineId) && (x.PartialCounting > 0 || x.QtyProductionWaste > 0) && !x.Exported);
            List<string> machineIDList = transList.Where(x => !string.IsNullOrEmpty(x.MachineId)).Select(x => x.MachineId).Distinct().ToList();
            List<MachineProductionRecord> returnList = new List<MachineProductionRecord>();
            foreach (string id in machineIDList)
            {
                returnList.Add(new MachineProductionRecord(
                    id,
                    AR.GetCode(id),
                    transList.Where(x => x.MachineId == id).Select(x => x.PartialCounting * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1)).DefaultIfEmpty(0).Sum(),
                    transList.Where(x => x.MachineId == id && !string.IsNullOrEmpty(x.JobId) && x.Verified).Select(x => x.PartialCounting * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1)).DefaultIfEmpty(0).Sum(),
                    transList.Where(x => x.MachineId == id && !x.Verified).Select(x => x.PartialCounting * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1)).DefaultIfEmpty(0).Sum()));
            }
            return returnList.OrderBy(x => x.MachineCode.Length).ThenBy(x => x.MachineCode).ToList();
        }

        public List<Transaction> TransactionListPerMachine(string machineId, bool verified = true)
        {
            if (verified)
            {
                return DBContext.Transactions.Where(x => x.MachineId == machineId && (x.PartialCounting > 0 || x.QtyProductionWaste > 0) && !x.Exported && x.Verified && !string.IsNullOrEmpty(x.JobId) && !x.Open).ToList();
            }
            else
            {
                return DBContext.Transactions.Where(x => x.MachineId == machineId && (x.PartialCounting > 0 || x.QtyProductionWaste > 0) && !x.Exported && !string.IsNullOrEmpty(x.JobId) && !x.Open).ToList();
            }
        }

        public List<ProductionPayment> CreateViewToExport(string machineId)
        {
            List<ProductionPayment> returnList = new List<ProductionPayment>();
            List<Transaction> list = DBContext.Transactions.Where(x => x.MachineId == machineId && (x.PartialCounting > 0 || x.QtyProductionWaste > 0) && !x.Exported && x.Verified && !string.IsNullOrEmpty(x.JobId) && !x.Open).ToList();
            AssetManagement.Repositories.AssetRepository AR = new AssetManagement.Repositories.AssetRepository();
            List<Job> jobList = list.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct().ToList();
            List<Operator> operList = list.Select(x => x.Operator).Distinct().ToList();
            //List<string> operList = list.Select(x => x.OperatorId).Distinct().ToList();
            foreach (Job job in jobList)
            {
                if (job != null)
                {
                    var transJobList = list.Where(x => x.JobId == job.Id);
                    DateTime minDate = transJobList.Min(x => x.Start);
                    DateTime maxDate = transJobList.Max(x => x.Start);
                    for (DateTime day = minDate.Date; day.Date <= maxDate.Date; day = day.AddDays(1))
                    {
                        //foreach (string opId in operList)
                        foreach (Operator op in operList)
                        {
                            if (transJobList.Where(x => x.OperatorId == op?.Id && x.Start.Date == day).Any())
                            {
                                var tList = transJobList.Where(x => x.OperatorId == op?.Id && x.Start.Date == day);
                                ProductionPayment pp = new ProductionPayment();
                                pp.MachineId = list.Select(x => x.MachineId).First();
                                pp.MachineCode = AR.FindByID(pp.MachineId)?.Code;
                                pp.JobId = job.Id;
                                pp.Job = job;
                                pp.OrderCode = job.Order.CustomerOrder.OrderCode;
                                pp.OperatorId = op?.Id;
                                pp.Operator = op;
                                pp.Start = tList.Min(x => x.Start);
                                pp.End = tList.Max(x => x.Start);
                                pp.Duration = Math.Round(tList.Sum(x => x.Duration) / 60, 0);
                                pp.QtyShotProduced = tList.Sum(x => x.PartialCounting);
                                pp.QtyProduced = tList.Sum(x => x.QtyProduced);
                                pp.QtyProductionWaste = tList.Sum(x => x.QtyProductionWaste);
                                pp.QtyOK = pp.QtyProduced - pp.QtyProductionWaste; //tList.Sum(x => x.QtyOK);
                                pp.Verified = false;
                                pp.IsOrderClose = tList.Where(x => x.IsOrderClosed == true).Any();
                                returnList.Add(pp);
                            }
                        }
                    }
                }
            }
            return returnList;
        }

        public void CreateProductionPaymentPerDay(List<Transaction> list, bool verified = false)
        {
            AssetManagement.Repositories.AssetRepository AR = new AssetManagement.Repositories.AssetRepository();
            List<Job> jobList = list.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct().ToList();
            List<string> operList = list.Select(x => x.OperatorId).Distinct().ToList();
            foreach (Job job in jobList)
            {
                if (job != null)
                {
                    var transJobList = list.Where(x => x.JobId == job.Id);
                    DateTime minDate = transJobList.Min(x => x.Start);
                    DateTime maxDate = transJobList.Max(x => x.Start);
                    for (DateTime day = minDate.Date; day.Date <= maxDate.Date; day = day.AddDays(1))
                    {
                        foreach (string opId in operList)
                        {
                            if (transJobList.Where(x => x.OperatorId == opId && x.Start.Date == day).Any())
                            {
                                var tList = transJobList.Where(x => x.OperatorId == opId && x.Start.Date == day);
                                ProductionPayment pp = new ProductionPayment();
                                pp.MachineId = list.Select(x => x.MachineId).First();
                                pp.MachineCode = AR.FindByID(pp.MachineId)?.Code;
                                pp.JobId = job.Id;
                                pp.OrderCode = job.Order.CustomerOrder.OrderCode;
                                pp.OperatorId = opId;
                                pp.Start = tList.Min(x => x.Start);
                                pp.End = tList.Max(x => x.Start);
                                pp.Duration = Math.Round(tList.Sum(x => x.Duration) / 60, 0);
                                pp.QtyShotProduced = tList.Sum(x => x.PartialCounting - x.BlankShot);
                                pp.QtyProduced = tList.Sum(x => x.QtyProduced);
                                pp.QtyProductionWaste = tList.Sum(x => x.QtyProductionWaste);
                                pp.QtyOK = pp.QtyProduced - pp.QtyProductionWaste; //tList.Sum(x => x.QtyOK);
                                pp.Verified = verified;
                                pp.IsOrderClose = tList.Where(x => x.IsOrderClosed == true).Any();
                                repos.Insert(pp);
                            }
                        }
                    }
                }
            }
            repos.SaveChanges();

            ConfirmTransaction(list);
        }

        public void CreateProductionPayment(List<Transaction> list, bool verified = false)
        {
            AssetManagement.Repositories.AssetRepository AR = new AssetManagement.Repositories.AssetRepository();
            foreach (Transaction t in list)
            {
                ProductionPayment pp = new ProductionPayment();
                pp.MachineId = t.MachineId;
                pp.MachineCode = AR.GetCode(pp.MachineId);
                pp.JobId = t.JobId;
                pp.OrderCode = t.Job?.Order?.CustomerOrder?.OrderCode;
                pp.OperatorId = t.OperatorId;
                pp.Start = t.Start;
                pp.End = t.End;
                pp.Duration = t.Duration;
                pp.QtyShotProduced = t.PartialCounting - t.BlankShot;
                pp.QtyProduced = t.QtyProduced;
                pp.QtyProductionWaste = t.QtyProductionWaste;
                pp.QtyOK = pp.QtyProduced - pp.QtyProductionWaste; //t.QtyOK;
                pp.Verified = verified;
                repos.Insert(pp);
            }

            ConfirmTransaction(list);
        }

        public void VerifyWaste()
        {

        }

        public void ConfirmTransaction(List<Transaction> list)
        {
            TransactionRepository r = new TransactionRepository();
            foreach (var item in list)
            {
                item.Exported = true;
                r.Update(item);
            }
            r.SaveChanges();
        }

        /// <summary>
        /// Restituisce tutte le transazioni non esportate
        /// </summary>
        /// <returns></returns>
        public List<ProductionPayment> GetAllNotSent()
        {
            return repos.ReadAll(x => x.Sent == null).OrderBy(x => x.Start).ToList();
        }

        /// <summary>
        /// Restituisce tutte le transazioni verificate non esportate
        /// </summary>
        /// <returns></returns>
        public List<ProductionPayment> GetAllVerifiedNotSent()
        {
            return repos.ReadAll(x => x.Sent == null && x.Verified).OrderBy(x => x.Start).ToList();
        }

        public List<ProductionPayment> AggregateToExport()
        {
            return AggregateToExport(GetAllNotSent());
        }

        public List<ProductionPayment> AggregateToExport(List<ProductionPayment> list)
        {
            List<ProductionPayment> exportList = new List<ProductionPayment>();
            List<string> jobList = list.Select(x => x.JobId).Distinct().ToList();
            DateTime minDate = list.Min(x => x.Start);
            DateTime maxDate = list.Max(x => x.Start);
            for (DateTime day = minDate.Date; day.Date <= maxDate.Date; day = day.AddDays(1))
            {
                foreach (string jobId in jobList)
                {
                    if (list.Where(x => x.JobId == jobId && x.Start.Date == day).Any())
                    {
                        var tList = list.Where(x => x.JobId == jobId && x.Start.Date == day.Date);
                        ProductionPayment pp = new ProductionPayment();
                        pp.MachineId = tList.Select(x => x.MachineId).First();
                        pp.MachineCode = tList.Select(x => x.MachineCode).First();
                        pp.JobId = jobId;
                        pp.OrderCode = tList.Select(x => x.OrderCode).First();
                        pp.OperatorId = null;
                        pp.Start = tList.Max(x => x.Start);
                        //pp.End = tList.Max(x => x.Start);
                        pp.QtyOperatorShotProduced = tList.Sum(x => x.QtyOperatorShotProduced);
                        pp.QtyOperatorProduced = tList.Sum(x => x.QtyOperatorProduced);
                        pp.QtyProductionWaste = tList.Sum(x => x.QtyProductionWaste);
                        pp.QtyOK = tList.Sum(x => x.QtyOK);
                        exportList.Add(pp);
                    }
                }
            }
            return exportList;
        }

        public void CloseOrder(string jobId)
        {
            //setto l'ordine chiuso sull'ultima transazione relativa al job
            TransactionRepository transRepos = new TransactionRepository();
            //Transaction lastTransJob = transRepos.ReadAll(x => x.JobId == jobId && x.PartialCounting > 0).OrderByDescending(x => x.Start).FirstOrDefault();
            Transaction lastTransJob = transRepos.ReadAll(x => x.JobId == jobId).OrderByDescending(x => x.Start).FirstOrDefault();
            if (lastTransJob != null)
            {
                lastTransJob.IsOrderClosed = true;
                transRepos.Update(lastTransJob);
                transRepos.SaveChanges();

                TransactionService service = new TransactionService();
                service.CloseAndOpenTransaction(lastTransJob);
            }

            //cambio lo stato dell'ordine di produzione
            JobRepository jobRepos = new JobRepository();
            var orderId = jobRepos.GetOrderId(jobId);
            OrderRepository orderRepos = new OrderRepository();
            Order o = orderRepos.FindByID(orderId);
            o.StatusCode = ((int)MesEnum.OrderStatusCode.Evasa).ToString();
            o.StatusDescription = MesEnum.OrderStatusCode.Evasa.ToString();
            orderRepos.Update(o);
            orderRepos.SaveChanges();
        }

        public void SuspendOrder(string jobId)
        {
            //cambio lo stato dell'ordine di produzione
            JobRepository jobRepos = new JobRepository();
            var orderId = jobRepos.GetOrderId(jobId);
            OrderRepository orderRepos = new OrderRepository();
            Order o = orderRepos.FindByID(orderId);
            o.StatusCode = ((int)MesEnum.OrderStatusCode.Sospesa).ToString();
            o.StatusDescription = MesEnum.OrderStatusCode.Sospesa.ToString();
            orderRepos.Update(o);
            orderRepos.SaveChanges();

            TransactionRepository transRepos = new TransactionRepository();
            Transaction lastTransJob = transRepos.ReadAll(x => x.JobId == jobId).OrderByDescending(x => x.Start).FirstOrDefault();
            if (lastTransJob != null)
            {
                TransactionService service = new TransactionService();
                service.CloseAndOpenTransaction(lastTransJob);
            }
        }

        /// <summary>
        /// Available only if order is "Evaso"
        /// </summary>
        /// <param name="jobId"></param>
        public void CreateProductionPaymentPerJob(string jobId)
        {
            TransactionRepository tRepos = new TransactionRepository();
            Transaction t = tRepos.ReadAll(x => x.JobId == jobId && x.IsOrderClosed.Value).FirstOrDefault();
            if (t != null)
            {
                JobRepository jobRepos = new JobRepository();
                Job job = jobRepos.FindByID(jobId);
                AssetManagement.Repositories.AssetRepository AR = new AssetManagement.Repositories.AssetRepository();
                List<Transaction> jobTList = tRepos.ReadAll(x => x.JobId == jobId).ToList();
                ProductionPayment pp = new ProductionPayment();
                pp.MachineId = t.MachineId;
                pp.MachineCode = AR.GetCode(pp.MachineId);
                pp.JobId = jobId;
                pp.OrderCode = t.Job?.Order?.CustomerOrder?.OrderCode;
                pp.OperatorId = t.OperatorId;
                pp.Start = jobTList.Select(x => x.Start).Min();
                //pp.End = jobTList.Select(x => x.End).Max();
                pp.End = DateTime.Now;
                pp.Duration = (decimal)(pp.End.Value - pp.Start).TotalSeconds;
                pp.QtyShotProduced = job.Order.QtyOrdered;
                pp.QtyProduced = job.Order.QtyOrdered;
                pp.QtyProductionWaste = 0;
                pp.QtyOK = pp.QtyProduced - pp.QtyProductionWaste; //t.QtyOK;
                pp.IsOrderClose = true;
                pp.Verified = true;
                repos.Insert(pp);
                repos.SaveChanges();
            }
        }

        public List<ProductionPayment> ProductionPaymentPerOperator(string operatorId, DateTime start, DateTime end)
        {
            List<ProductionPayment> returnList = new List<ProductionPayment>();
            List<Transaction> list = DBContext.Transactions.Where(x => x.OperatorId == operatorId && x.Start >= start && x.Start <= end && (x.PartialCounting > 0 || x.QtyProductionWaste > 0) && !string.IsNullOrEmpty(x.JobId) && !x.Open).OrderBy(x => x.Start).ToList();
            AssetManagement.Repositories.AssetRepository AR = new AssetManagement.Repositories.AssetRepository();
            OperatorRepository opRepo = new OperatorRepository();
            Operator op = opRepo.FindByID(operatorId);
            List<Job> jobList = list.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct().ToList();
            List<string> assetIdList = list.Select(x => x.MachineId).Distinct().ToList();
            foreach (Job job in jobList)
            {
                if (job != null)
                {
                    var transJobList = list.Where(x => x.JobId == job.Id);
                    DateTime minDate = transJobList.Min(x => x.Start);
                    DateTime maxDate = transJobList.Max(x => x.Start);
                    for (DateTime day = minDate.Date; day.Date <= maxDate.Date; day = day.AddDays(1))
                    {
                        //foreach (string opId in operList)
                        foreach (string assetId in assetIdList)
                        {
                            var tList = transJobList.Where(x => x.MachineId == assetId && x.Start.Date == day);
                            if (tList.Any())
                            {
                                //var tList = transJobList.Where(x => x.OperatorId == op?.Id && x.Start.Date == day);
                                ProductionPayment pp = new ProductionPayment();
                                pp.MachineId = list.Select(x => x.MachineId).First();
                                pp.MachineCode = AR.FindByID(pp.MachineId)?.Code;
                                pp.JobId = job.Id;
                                pp.Job = job;
                                pp.OrderCode = job.Order.CustomerOrder.OrderCode;
                                pp.OperatorId = operatorId;
                                pp.Operator = op;
                                pp.Start = tList.Min(x => x.Start);
                                pp.End = tList.Max(x => x.End);
                                pp.Duration = Math.Round(tList.Sum(x => x.Duration) / 60, 0);
                                pp.QtyShotProduced = tList.Sum(x => x.PartialCounting);
                                pp.QtyProduced = tList.Sum(x => x.QtyProduced);
                                pp.QtyProductionWaste = tList.Sum(x => x.QtyProductionWaste);
                                pp.QtyOK = pp.QtyProduced - pp.QtyProductionWaste; //tList.Sum(x => x.QtyOK);
                                pp.Verified = false;
                                pp.IsOrderClose = tList.Where(x => x.IsOrderClosed == true).Any();
                                returnList.Add(pp);
                            }
                        }
                    }
                }
            }
            return returnList;
        }

        public List<ProductionPayment> ProductionPaymentPerAsset(string assetId, DateTime start, DateTime end)
        {
            List<ProductionPayment> returnList = new List<ProductionPayment>();
            List<Transaction> list = DBContext.Transactions.Where(x => x.MachineId == assetId && x.Start >= start && x.Start <= end && (x.PartialCounting > 0 || x.QtyProductionWaste > 0) && !string.IsNullOrEmpty(x.JobId) && !x.Open).OrderBy(x => x.Start).ToList();
            AssetManagement.Repositories.AssetRepository AR = new AssetManagement.Repositories.AssetRepository();
            List<Job> jobList = list.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct().ToList();
            List<Operator> operList = list.Select(x => x.Operator).Distinct().ToList();
            //List<string> operList = list.Select(x => x.OperatorId).Distinct().ToList();
            foreach (Job job in jobList)
            {
                if (job != null)
                {
                    var transJobList = list.Where(x => x.JobId == job.Id);
                    DateTime minDate = transJobList.Min(x => x.Start);
                    DateTime maxDate = transJobList.Max(x => x.Start);
                    for (DateTime day = minDate.Date; day.Date <= maxDate.Date; day = day.AddDays(1))
                    {
                        //foreach (string opId in operList)
                        foreach (Operator op in operList)
                        {
                            if (transJobList.Where(x => x.OperatorId == op?.Id && x.Start.Date == day).Any())
                            {
                                var tList = transJobList.Where(x => x.OperatorId == op?.Id && x.Start.Date == day);
                                ProductionPayment pp = new ProductionPayment();
                                pp.MachineId = assetId;//list.Select(x => x.MachineId).First();
                                pp.MachineCode = AR.FindByID(pp.MachineId)?.Code;
                                pp.JobId = job.Id;
                                pp.Job = job;
                                pp.OrderCode = job.Order.CustomerOrder.OrderCode;
                                pp.OperatorId = op?.Id;
                                pp.Operator = op;
                                pp.Start = tList.Min(x => x.Start);
                                pp.End = tList.Max(x => x.End);
                                pp.Duration = Math.Round(tList.Sum(x => x.Duration) / 60, 0);
                                pp.QtyShotProduced = tList.Sum(x => x.PartialCounting);
                                pp.QtyProduced = tList.Sum(x => x.QtyProduced);
                                pp.QtyProductionWaste = tList.Sum(x => x.QtyProductionWaste);
                                pp.QtyOK = pp.QtyProduced - pp.QtyProductionWaste; //tList.Sum(x => x.QtyOK);
                                pp.Verified = false;
                                pp.IsOrderClose = tList.Where(x => x.IsOrderClosed == true).Any();
                                returnList.Add(pp);
                            }
                        }
                    }
                }
            }
            return returnList;
        }

        public List<ProductionPayment> ProductionPaymentPerAssetOperator(string assetId, string operatorId, DateTime start, DateTime end)
        {
            List<ProductionPayment> returnList = new List<ProductionPayment>();
            List<Transaction> list = DBContext.Transactions.Where(x => x.MachineId == assetId && x.OperatorId == operatorId && x.Start >= start && x.Start <= end && (x.PartialCounting > 0 || x.QtyProductionWaste > 0) && !string.IsNullOrEmpty(x.JobId) && !x.Open).OrderBy(x => x.Start).ToList();
            AssetManagement.Repositories.AssetRepository AR = new AssetManagement.Repositories.AssetRepository();
            string machineCode = AR.FindByID(assetId)?.Code;
            OperatorRepository opRepo = new OperatorRepository();
            Operator op = opRepo.FindByID(operatorId);
            List<Job> jobList = list.Where(x => !string.IsNullOrEmpty(x.JobId)).Select(x => x.Job).Distinct().ToList();
            foreach (Job job in jobList)
            {
                if (job != null)
                {
                    var transJobList = list.Where(x => x.JobId == job.Id);
                    DateTime minDate = transJobList.Min(x => x.Start);
                    DateTime maxDate = transJobList.Max(x => x.Start);
                    for (DateTime day = minDate.Date; day.Date <= maxDate.Date; day = day.AddDays(1))
                    {
                        if (transJobList.Where(x => x.OperatorId == op?.Id && x.Start.Date == day).Any())
                        {
                            var tList = transJobList.Where(x => x.OperatorId == op?.Id && x.Start.Date == day);
                            ProductionPayment pp = new ProductionPayment();
                            pp.MachineId = assetId;//list.Select(x => x.MachineId).First();
                            pp.MachineCode = machineCode;
                            pp.JobId = job.Id;
                            pp.Job = job;
                            pp.OrderCode = job.Order.CustomerOrder.OrderCode;
                            pp.OperatorId = operatorId;
                            pp.Operator = op;
                            pp.Start = tList.Min(x => x.Start);
                            pp.End = tList.Max(x => x.End);
                            pp.Duration = Math.Round(tList.Sum(x => x.Duration) / 60, 0);
                            pp.QtyShotProduced = tList.Sum(x => x.PartialCounting);
                            pp.QtyProduced = tList.Sum(x => x.QtyProduced);
                            pp.QtyProductionWaste = tList.Sum(x => x.QtyProductionWaste);
                            pp.QtyOK = pp.QtyProduced - pp.QtyProductionWaste; //tList.Sum(x => x.QtyOK);
                            pp.Verified = false;
                            pp.IsOrderClose = tList.Where(x => x.IsOrderClosed == true).Any();
                            returnList.Add(pp);
                        }
                    }
                }
            }
            return returnList;
        }
    }
}