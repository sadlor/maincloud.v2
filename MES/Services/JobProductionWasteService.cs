﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class JobProductionWasteService : BaseService<MesDbContext>
    {
        public List<JobProductionWaste> GetWasteListByJobId(string jobId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            return DBContext.JobProductionWaste.Where(x => x.JobId == jobId).ToList();
        }

        public List<JobProductionWaste> GetWasteListPerWorkshift(string jobId, string operatorId, DateTime start, DateTime? end, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            if (end.HasValue)
            {
                return DBContext.JobProductionWaste.Where(x => x.JobId == jobId && x.OperatorId == operatorId && x.Date >= start && x.Date <= end).ToList();
            }
            else
            {
                return DBContext.JobProductionWaste.Where(x => x.JobId == jobId && x.OperatorId == operatorId && x.Date >= start).ToList();
            }
        }

        public List<JobProductionWaste> GetWasteListPerWorkshift(string jobId, DateTime start, DateTime? end, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            if (end.HasValue)
            {
                return DBContext.JobProductionWaste.Where(x => x.JobId == jobId && x.Date >= start && x.Date <= end).ToList();
            }
            else
            {
                return DBContext.JobProductionWaste.Where(x => x.JobId == jobId && x.Date >= start).ToList();
            }
        }

        public List<JobProductionWaste> GetWasteListPerTransaction(int transactionId, string jobId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            return DBContext.JobProductionWaste.Where(x => x.TransactionId == transactionId && x.JobId == jobId).ToList();
        }

        public List<JobProductionWaste> GetWasteListPerTransaction(List<Transaction> transactionList, string jobId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            List<int> transIdList = transactionList.Select(x => x.Id).Distinct().ToList();
            return DBContext.JobProductionWaste.Where(x => transIdList.Contains(x.TransactionId) && x.JobId == jobId).ToList();
        }

        public List<JobProductionWaste> GetWasteListPerTransaction(List<Transaction> transactionList, string jobId, string operatorId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            List<int> transIdList = transactionList.Select(x => x.Id).Distinct().ToList();
            return DBContext.JobProductionWaste.Where(x => transIdList.Contains(x.TransactionId) && x.JobId == jobId && x.OperatorId == operatorId).ToList();
        }
    }
}