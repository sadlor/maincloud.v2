﻿using MainCloudFramework.Services;
using MES.Core;
using MES.Helper;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class TransactionMoldService : BaseService<MesDbContext>
    {
        public decimal LastCounterMold(string moldId)
        {
            try
            {
                //return DBContext.TransactionMolds.Where(x => x.MoldId == moldId && x.Open).Select(x => x.CounterStart).DefaultIfEmpty(0).Max();
                return DBContext.TransactionMolds.Where(x => x.MoldId == moldId && x.Open).Select(x => x.ProgressiveCounter + x.PartialCounting).DefaultIfEmpty(0).First();
            }
            catch (ArgumentNullException)
            {
                return 0;
            }
        }

        public string GetLastJob(string moldId)
        {
            return DBContext.TransactionMolds.Where(x => x.MoldId == moldId && !string.IsNullOrEmpty(x.JobId)).OrderByDescending(x => x.Start).Select(x => x.JobId).FirstOrDefault();
        }

        public TransactionMold GetLastTransactionOpen(string moldId)
        {
            try
            {
                return DBContext.TransactionMolds
                    .Include("Job.Order.CustomerOrder")
                    .Include("Job.Order.Article")
                    //.Include("Operator")
                    .Include("Cause")
                    .Where(x => x.Open && x.MoldId == moldId).OrderByDescending(x => x.Start).FirstOrDefault();
            }
            catch (InvalidOperationException)
            {
                //non ci sono transazioni aperte
                return null;
            }
        }

        public bool LastTransactionOpenIsInProduction(string moldId)
        {
            CauseService cService = new CauseService();
            string causeId = cService.GetCauseByCode(((int)MES.Core.MesEnum.Cause.Production).ToString(), OperatorHelper.ApplicationId)?.Id;
            TransactionMold t = GetLastTransactionOpen(moldId);
            if (t != null)
            {
                if (t.CauseId == causeId)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void OpenTransaction(TransactionMold t)
        {
            t.Open = true;
            t.Start = DateTime.Now;
            t.End = null;
            t.Duration = 0;
            t.LastUpdate = DateTime.Now;
            t.Verified = false;
            DBContext.TransactionMolds.Add(t);
            DBContext.SaveChanges();
        }

        public void OpenTransaction(string moldId)
        {
            TransactionMold t = new TransactionMold();
            t.MoldId = moldId;

            OpenTransaction(t);
        }

        public void CloseTransaction(string moldId)
        {
            TransactionMold t = DBContext.TransactionMolds.Where(x => x.Open && x.MoldId == moldId).OrderByDescending(x => x.Start).First();
            t.End = DateTime.Now;
            t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
            t.Open = false;
            t.LastUpdate = DateTime.Now;
            if (t.PartialCounting > 0)
            {
                t.QtyProduced = (t.PartialCounting - t.BlankShot) * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
            }

            DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
            DBContext.SaveChanges();
        }

        public void CloseAndOpenTransaction(TransactionMold t)
        {
            t.End = DateTime.Now;
            t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
            t.Open = false;
            t.LastUpdate = DateTime.Now;
            if (t.PartialCounting > 0)
            {
                t.QtyProduced = (t.PartialCounting - t.BlankShot) * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
            }

            DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
            DBContext.SaveChanges();

            t.CounterStart = t.CounterEnd.Value;
            t.CounterEnd = t.CounterStart;
            t.PartialCounting = 0;
            t.QtyProduced = 0;
            t.CauseId = null;

            OpenTransaction(t);
        }

        public void CloseAndOpenTransaction(string moldId)
        {
            TransactionMold t;
            try
            {
                t = DBContext.TransactionMolds.Where(x => x.Open && x.MoldId == moldId).OrderByDescending(x => x.Start).First();
                t.End = DateTime.Now;
                t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
                t.Open = false;
                t.LastUpdate = DateTime.Now;
                if (t.PartialCounting > 0)
                {
                    t.QtyProduced = (t.PartialCounting - t.BlankShot) * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
                }

                DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
                DBContext.SaveChanges();
            }
            catch (InvalidOperationException)
            {
                //non c'è transazione aperta
                t = new TransactionMold();
                t.MoldId = moldId;
            }

            t.CounterStart = t.CounterEnd.Value;
            t.CounterEnd = t.CounterStart;
            t.PartialCounting = 0;
            t.QtyProduced = 0;
            t.CauseId = null;

            OpenTransaction(t);
        }

        public void CloseAndOpenTransaction(string moldId, params KeyValuePair<MesEnum.TransactionParam, object>[] parameters)
        {
            TransactionMold t;
            try
            {
                t = DBContext.TransactionMolds.Where(x => x.Open && x.MoldId == moldId).OrderByDescending(x => x.Start).First();
                t.End = DateTime.Now;
                t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
                t.Open = false;
                t.LastUpdate = DateTime.Now;
                if (t.PartialCounting > 0)
                {
                    t.QtyProduced = (t.PartialCounting - t.BlankShot) * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
                }

                //DBContext.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
                DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
                DBContext.SaveChanges();
            }
            catch (InvalidOperationException)
            {
                //non c'è transazione aperta
                t = new TransactionMold();
                t.MoldId = moldId;
            }

            t.CounterStart = t.CounterEnd.HasValue ? t.CounterEnd.Value : 0;
            t.CounterEnd = t.CounterStart;
            t.PartialCounting = 0;
            t.QtyProduced = 0;

            foreach (var param in parameters)
            {
                switch (param.Key)
                {
                    case MesEnum.TransactionParam.Cause:
                        CauseService cService = new CauseService();
                        if (param.Value == null)
                        {
                            t.CauseId = null;
                        }
                        else
                        {
                            if (param.Value is string)
                            {
                                t.CauseId = param.Value.ToString();
                            }
                            else
                            {
                                t.CauseId = cService.GetCauseByCode(((int)param.Value).ToString(), OperatorHelper.ApplicationId)?.Id;
                            }
                        }
                        break;
                    case MesEnum.TransactionParam.Job:
                        if (!string.IsNullOrEmpty((string)param.Value))
                        {
                            t.JobId = (string)param.Value;
                        }
                        else
                        {
                            t.JobId = null;
                        }
                        break;
                    case MesEnum.TransactionParam.Machine:
                        if (!string.IsNullOrEmpty((string)param.Value))
                        {
                            t.MachineId = (string)param.Value;
                        }
                        else
                        {
                            t.MachineId = null;
                        }
                        break;
                    case MesEnum.TransactionParam.CavityMoldNum:
                        if (Type.GetTypeCode(param.Value.GetType()) == TypeCode.String)
                        {
                            if (!string.IsNullOrEmpty((string)param.Value))
                            {
                                t.CavityMoldNum = Convert.ToInt32(param.Value);
                            }
                        }
                        else
                        {
                            t.CavityMoldNum = Convert.ToInt32(param.Value);
                        }
                        break;
                }
            }
            OpenTransaction(t);
        }
    }
}