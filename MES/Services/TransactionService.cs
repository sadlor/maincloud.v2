﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Core;
using MES.Helper;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class TransactionService : BaseService<MesDbContext>
    {
        //private static void UpdateForType(Type type, Transaction source, Transaction destination)
        //{
        //    PropertyInfo[] myObjectFields = type.GetProperties(
        //        BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

        //    foreach (PropertyInfo fi in myObjectFields)
        //    {
        //        if (fi.Name != "Id")
        //        {
        //            fi.SetValue(destination, fi.GetValue(source));
        //        }
        //    }
        //}

        public void OpenTransaction(Transaction t)
        {
            //Transaction newT = new Transaction();

            //Type type = t.GetType();
            //while (type != null)
            //{
            //    UpdateForType(type, t, newT);
            //    type = type.BaseType;
            //}

            t.Open = true;
            t.Start = DateTime.Now;
            t.End = null;
            t.Duration = 0;
            t.LastUpdate = DateTime.Now;
            t.Verified = false;
            t.Exported = false;
            t.IsOrderClosed = null;
            DBContext.Transactions.Add(t);
            DBContext.SaveChanges();
        }

        public void OpenTransaction(string assetId)
        {
            Transaction t = new Transaction();
            t.MachineId = assetId;

            OpenTransaction(t);
        }

        public void OpenTransaction(string assetId, string operatorId)
        {
            Transaction t = new Transaction();
            t.MachineId = assetId;
            t.OperatorId = operatorId;

            OpenTransaction(t);
        }

        public void CloseTransaction(string assetId)
        {
            Transaction t = DBContext.Transactions.Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).First();
            t.End = DateTime.Now;
            t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
            t.Open = false;
            t.LastUpdate = DateTime.Now;
            if (t.PartialCounting > 0 && t.CavityMoldNum > 0)
            {
                t.QtyProduced = (t.PartialCounting - t.BlankShot) * t.CavityMoldNum;
            }

            DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
            DBContext.SaveChanges();
        }

        public void CloseAndOpenTransaction(Transaction t)
        {
            t.End = DateTime.Now;
            t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
            t.Open = false;
            t.LastUpdate = DateTime.Now;
            if (t.PartialCounting > 0)
            {
                t.QtyProduced = (t.PartialCounting - t.BlankShot) * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
            }

            DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
            DBContext.SaveChanges();

            t.CounterStart = t.CounterEnd.Value;
            t.CounterEnd = t.CounterStart;
            t.PartialCounting = 0;
            t.QtyProduced = 0;
            t.QtyProductionWaste = 0;

            OpenTransaction(t);
        }

        public void CloseAndOpenTransaction(string assetId)
        {
            Transaction t = DBContext.Transactions.Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).First();
            t.End = DateTime.Now;
            t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
            t.Open = false;
            t.LastUpdate = DateTime.Now;
            //if (t.PartialCounting > 0 && t.CavityMoldNum > 0)
            //{
            //    t.QtyProduced = (t.PartialCounting - t.BlankShot) * t.CavityMoldNum;
            //}
            if (t.PartialCounting > 0)
            {
                t.QtyProduced = (t.PartialCounting - t.BlankShot) * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
            }

            DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
            DBContext.SaveChanges();

            t.CounterStart = t.CounterEnd.Value;
            t.CounterEnd = t.CounterStart;
            t.PartialCounting = 0;
            t.QtyProduced = 0;
            t.QtyProductionWaste = 0;

            OpenTransaction(t);
        }

        public void CloseAndOpenTransaction(string assetId, params KeyValuePair<MesEnum.TransactionParam, object>[] parameters)
        {
            Transaction t;
            try
            {
                t = DBContext.Transactions.Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).First();
                t.End = DateTime.Now;
                t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
                t.Open = false;
                t.LastUpdate = DateTime.Now;
                if (t.PartialCounting > 0 && t.CavityMoldNum > 0)
                {
                    t.QtyProduced = (t.PartialCounting - t.BlankShot) * t.CavityMoldNum;
                }

                //DBContext.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
                DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
                //if (!string.IsNullOrEmpty(t.OperatorId))
                //{
                //    DBContext.Operators.Attach(t.Operator);
                //    DBContext.Entry(t.Operator).State = System.Data.Entity.EntityState.Detached;
                //}
                //DBContext.Configuration.AutoDetectChangesEnabled = false;
                DBContext.SaveChanges();
            }
            catch (InvalidOperationException)
            {
                //non c'è transazione aperta
                t = new Transaction();
                t.MachineId = assetId;
            }

            t.CounterStart = t.CounterEnd.HasValue ? t.CounterEnd.Value : 0;
            t.CounterEnd = t.CounterStart;
            t.PartialCounting = 0;
            t.QtyProduced = 0;
            t.QtyProductionWaste = 0;

            if (parameters != null)
            {
                foreach (var param in parameters)
                {
                    switch (param.Key)
                    {
                        case MesEnum.TransactionParam.Cause:
                            CauseService cService = new CauseService();
                            if (param.Value == null)
                            {
                                t.CauseId = null;
                            }
                            else
                            {
                                if (param.Value is string)
                                {
                                    t.CauseId = param.Value.ToString();
                                }
                                else
                                {
                                    t.CauseId = cService.GetCauseByCode(((int)param.Value).ToString(), OperatorHelper.ApplicationId)?.Id;
                                }
                            }
                            break;
                        case MesEnum.TransactionParam.Job:
                            if (!string.IsNullOrEmpty((string)param.Value))
                            {
                                t.JobId = (string)param.Value;
                            }
                            else
                            {
                                t.JobId = null;
                            }
                            break;
                        case MesEnum.TransactionParam.Mold:
                            if (!string.IsNullOrEmpty((string)param.Value))
                            {
                                t.MoldId = (string)param.Value;
                            }
                            else
                            {
                                t.MoldId = null;
                            }
                            break;
                        case MesEnum.TransactionParam.Operator:
                            if (!string.IsNullOrEmpty((string)param.Value))
                            {
                                t.OperatorId = (string)param.Value;
                            }
                            else
                            {
                                t.OperatorId = null;
                            }
                            break;
                        case MesEnum.TransactionParam.Status:
                            break;
                        case MesEnum.TransactionParam.StandardRate:
                            if (Type.GetTypeCode(param.Value.GetType()) == TypeCode.String)
                            {
                                if (!string.IsNullOrEmpty((string)param.Value))
                                {
                                    t.StandardRate = Convert.ToDecimal(param.Value);
                                }
                            }
                            else
                            {
                                t.StandardRate = Convert.ToDecimal(param.Value);
                            }
                            break;
                        case MesEnum.TransactionParam.CavityMoldNum:
                            if (Type.GetTypeCode(param.Value.GetType()) == TypeCode.String)
                            {
                                if (!string.IsNullOrEmpty((string)param.Value))
                                {
                                    t.CavityMoldNum = Convert.ToDecimal(param.Value);
                                }
                            }
                            else
                            {
                                t.CavityMoldNum = Convert.ToDecimal(param.Value);
                            }
                            break;
                    }
                }
            }
            OpenTransaction(t);
        }

        public void CloseAndOpenTransaction(Transaction t, params KeyValuePair<MesEnum.TransactionParam, object>[] parameters)
        {
            t.End = DateTime.Now;
            t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
            t.Open = false;
            t.LastUpdate = DateTime.Now;
            if (t.PartialCounting > 0 && t.CavityMoldNum > 0)
            {
                t.QtyProduced = (t.PartialCounting - t.BlankShot) * t.CavityMoldNum;
            }

            //DBContext.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
            DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
            //if (!string.IsNullOrEmpty(t.OperatorId))
            //{
            //    DBContext.Operators.Attach(t.Operator);
            //    DBContext.Entry(t.Operator).State = System.Data.Entity.EntityState.Detached;
            //}
            //DBContext.Configuration.AutoDetectChangesEnabled = false;
            DBContext.SaveChanges();

            t.CounterStart = t.CounterEnd.HasValue ? t.CounterEnd.Value : 0;
            t.CounterEnd = t.CounterStart;
            t.PartialCounting = 0;
            t.QtyProduced = 0;
            t.QtyProductionWaste = 0;

            if (parameters != null)
            {
                foreach (var param in parameters)
                {
                    switch (param.Key)
                    {
                        case MesEnum.TransactionParam.Cause:
                            CauseService cService = new CauseService();
                            if (param.Value == null)
                            {
                                t.CauseId = null;
                            }
                            else
                            {
                                if (param.Value is string)
                                {
                                    t.CauseId = param.Value.ToString();
                                }
                                else
                                {
                                    t.CauseId = cService.GetCauseByCode(((int)param.Value).ToString(), OperatorHelper.ApplicationId)?.Id;
                                }
                            }
                            break;
                        case MesEnum.TransactionParam.Job:
                            if (!string.IsNullOrEmpty((string)param.Value))
                            {
                                t.JobId = (string)param.Value;
                            }
                            else
                            {
                                t.JobId = null;
                            }
                            break;
                        case MesEnum.TransactionParam.Mold:
                            if (!string.IsNullOrEmpty((string)param.Value))
                            {
                                t.MoldId = (string)param.Value;
                            }
                            else
                            {
                                t.MoldId = null;
                            }
                            break;
                        case MesEnum.TransactionParam.Operator:
                            if (!string.IsNullOrEmpty((string)param.Value))
                            {
                                t.OperatorId = (string)param.Value;
                            }
                            else
                            {
                                t.OperatorId = null;
                            }
                            break;
                        case MesEnum.TransactionParam.Status:
                            break;
                        case MesEnum.TransactionParam.StandardRate:
                            if (Type.GetTypeCode(param.Value.GetType()) == TypeCode.String)
                            {
                                if (!string.IsNullOrEmpty((string)param.Value))
                                {
                                    t.StandardRate = Convert.ToDecimal(param.Value);
                                }
                            }
                            else
                            {
                                t.StandardRate = Convert.ToDecimal(param.Value);
                            }
                            break;
                        case MesEnum.TransactionParam.CavityMoldNum:
                            if (Type.GetTypeCode(param.Value.GetType()) == TypeCode.String)
                            {
                                if (!string.IsNullOrEmpty((string)param.Value))
                                {
                                    t.CavityMoldNum = Convert.ToDecimal(param.Value);
                                }
                            }
                            else
                            {
                                t.CavityMoldNum = Convert.ToDecimal(param.Value);
                            }
                            break;
                    }
                }
            }
            OpenTransaction(t);
        }

        public bool IsTransactionOpen(string assetId)
        {
            return DBContext.Transactions.Where(x => x.Open && x.MachineId == assetId).Any();
            //try
            //{
            //    Transaction t = DBContext.Transactions.Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).First();
            //    return true;
            //}
            //catch (ArgumentNullException)
            //{
            //    return false;
            //}
            //catch (InvalidOperationException)
            //{
            //    return false;
            //}
        }

        public bool IsOperatorOnMachine(string assetId)
        {
            Transaction t = DBContext.Transactions.Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).FirstOrDefault();
            if (t != null)
            {
                if (string.IsNullOrEmpty(t.OperatorId))
                {
                    //non c'è operatore associato
                    return false;
                }
                else
                {
                    //c'è operatore associato alla macchina
                    return true;
                }
            }
            else
            {
                //non c'è transazione aperta
                return false;
            }
        }

        public Color GetCauseColor(string assetId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            Transaction t = DBContext.Transactions.Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).FirstOrDefault();
            if (t != null)
            {
                if (!string.IsNullOrEmpty(t.CauseId))
                {
                    CauseService causeService = new CauseService();
                    List<string> setup = causeService.GetListCauseIdByGroupName("Attrezzaggio", applicationId);
                    List<string> prod = causeService.GetListCauseIdByGroupName("Produzione", applicationId);
                    List<string> maintenance = causeService.GetListCauseIdByGroupName("Manutenzione", applicationId);

                    //verifico qual è la causale
                    if (setup.Contains(t.CauseId))
                    {
                        return Color.FromName("#f7f76a");
                    }
                    if (prod.Contains(t.CauseId))
                    {
                        return Color.LightGreen; //.FromName("#1ac71a");
                    }
                    if (maintenance.Contains(t.CauseId))
                    {
                        return Color.LightBlue; //.FromName("#7fd3e6");
                    }
                    if (t.CauseId == causeService.GetCauseByCode(((int)MesEnum.Cause.MachineOff).ToString(), applicationId).Id)
                    {
                        return Color.FromName(""); //Color.Red;
                    }
                    return Color.FromName("");
                }
                else
                {
                    //non c'è causale, quindi è un fermo
                    return Color.FromName("#f75555"); //Color.Red;
                }
            }
            else
            {
                //non c'è transazione aperta
                return Color.FromName("");
            }
        }

        public Color GetCauseColorPerTransaction(int transId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            if (!string.IsNullOrEmpty(applicationId))
            {
                Transaction t = DBContext.Transactions.Find(transId);
                if (t != null)
                {
                    if (!string.IsNullOrEmpty(t.CauseId))
                    {
                        CauseService causeService = new CauseService();
                        List<string> setup = causeService.GetListCauseIdByGroupName("Attrezzaggio", applicationId);
                        List<string> prod = causeService.GetListCauseIdByGroupName("Produzione", applicationId);
                        List<string> maintenance = causeService.GetListCauseIdByGroupName("Manutenzione", applicationId);
                        List<string> stop = causeService.GetListCauseIdByGroupName("Fermi", applicationId);
                        List<string> alarm = causeService.GetListCauseIdByGroupName("Allarmi", applicationId);

                        //verifico qual è la causale
                        if (setup.Contains(t.CauseId))
                        {
                            return Color.FromName("#f7f76a");
                        }
                        if (prod.Contains(t.CauseId))
                        {
                            return Color.LightGreen; //.FromName("#1ac71a");
                        }
                        if (maintenance.Contains(t.CauseId))
                        {
                            return Color.LightBlue; //.FromName("#7fd3e6");
                        }
                        if (stop.Contains(t.CauseId))
                        {
                            return Color.FromName("#f75555"); //Color.Red;
                        }
                        if (alarm.Contains(t.CauseId))
                        {
                            return Color.FromName("#F80F41"); //Color.Red;
                        }
                        if (t.CauseId == causeService.GetCauseByCode(((int)MesEnum.Cause.MachineOff).ToString(), applicationId).Id)
                        {
                            return Color.FromName(""); //Color.Red;
                        }
                        return Color.FromName("");
                    }
                    else
                    {
                        //non c'è causale, quindi è un fermo
                        return Color.FromName("#f75555"); //Color.Red;
                    }
                }
                else
                {
                    //non c'è transazione aperta
                    return Color.FromName("");
                }
            }
            else
            {
                return Color.FromName("");
            }
        }

        public MesEnum.Cause GetCurrentCause(string assetId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            Transaction t = DBContext.Transactions.Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).FirstOrDefault();
            if (t != null)
            {
                if (!string.IsNullOrEmpty(t.CauseId))
                {
                    CauseService causeService = new CauseService();
                    List<string> setup = causeService.GetListCauseIdByGroupName("Attrezzaggio", applicationId);
                    List<string> prod = causeService.GetListCauseIdByGroupName("Produzione", applicationId);
                    List<string> maintenance = causeService.GetListCauseIdByGroupName("Manutenzione", applicationId);

                    //verifico qual è la causale
                    if (setup.Contains(t.CauseId))
                    {
                        return MesEnum.Cause.SetUp;
                    }
                    if (prod.Contains(t.CauseId))
                    {
                        return MesEnum.Cause.Production;
                    }
                    if (maintenance.Contains(t.CauseId))
                    {
                        return MesEnum.Cause.Maintenance;
                    }
                    if (t.CauseId == causeService.GetCauseByCode(((int)MesEnum.Cause.MachineOff).ToString(), applicationId).Id)
                    {
                        return MesEnum.Cause.MachineOff;
                    }
                    return MesEnum.Cause.Stop;
                }
                else
                {
                    //non c'è causale, quindi è un fermo
                    return MesEnum.Cause.Stop;
                }
            }
            else
            {
                //non c'è transazione aperta
                return MesEnum.Cause.Stop;
            }
        }

        public Transaction GetLastTransactionOpen(string assetId)
        {
            try
            {
                return DBContext.Transactions
                    .Include("Job.Order.CustomerOrder")
                    .Include("Job.Order.Article")
                    .Include("Operator")
                    .Include("Cause")
                    .Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).First();
            }
            catch (InvalidOperationException)
            {
                //non ci sono transazioni aperte
                return null;
            }
        }

        public Job GetLastJob(string assetId)
        {
            try
            {
                return DBContext.Transactions
                    .Include("Job.Order.CustomerOrder")
                    .Include("Job.Order.Article")
                    .Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).Select(x => x.Job).FirstOrDefault();
            }
            catch (InvalidOperationException)
            {
                //non ci sono transazioni aperte
                return null;
            }
        }

        /// <summary>
        /// Return last OrderCode worked on machine
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetLastCustomerOrderCode(string assetId)
        {
            return DBContext.Transactions.Include("Job.Order.CustomerOrder").Where(x => x.MachineId == assetId && x.Open).OrderByDescending(x => x.Start).Select(x => x.Job.Order.CustomerOrder.OrderCode).FirstOrDefault();
        }

        public string GetLastJobId(string assetId)
        {
            return DBContext.Transactions.Where(x => x.MachineId == assetId && !string.IsNullOrEmpty(x.JobId)).OrderByDescending(x => x.Start).Select(x => x.JobId).FirstOrDefault();
        }

        public Transaction GetLastTransactionPerOperator(string assetId, string operatorId)
        {
            try
            {
                return DBContext.Transactions.Where(x => x.MachineId == assetId && x.OperatorId == operatorId).OrderByDescending(x => x.Start).First();
            }
            catch (InvalidOperationException)
            {
                //non ci sono transazioni aperte
                return null;
            }
        }

        public Transaction GetLastTransactionPerOperator(string assetId, string operatorId, string jobId)
        {
            try
            {
                return DBContext.Transactions.Where(x => x.MachineId == assetId && x.JobId == jobId && x.OperatorId == operatorId).OrderByDescending(x => x.Start).First();
            }
            catch (InvalidOperationException)
            {
                //non ci sono transazioni aperte
                return null;
            }
        }

        public Transaction GetLastTransactionPerOperatorInProd(string assetId, string operatorId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            CauseService causeService = new CauseService();
            List<string> prod = causeService.GetListCauseIdByGroupName("Produzione", applicationId);

            try
            {
                return DBContext.Transactions.Where(x => x.MachineId == assetId && x.OperatorId == operatorId && prod.Contains(x.CauseId)).OrderByDescending(x => x.Start).First();
            }
            catch (InvalidOperationException)
            {
                //non ci sono transazioni aperte
                return null;
            }
        }

        /// <summary>
        /// Ultima transazione per operatore nel turno dato
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="operatorId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public Transaction GetLastTransactionPerOperatorWorkshift(string assetId, string operatorId, DateTime start, DateTime? end)
        {
            try
            {
                if (end.HasValue)
                {
                    return DBContext.Transactions.Where(x => x.MachineId == assetId && x.OperatorId == operatorId && x.Start >= start && x.Start <= end.Value).OrderByDescending(x => x.Start).First();
                }
                else
                {
                    return DBContext.Transactions.Where(x => x.MachineId == assetId && x.OperatorId == operatorId && x.Start >= start).OrderByDescending(x => x.Start).First();
                }
            }
            catch (InvalidOperationException)
            {
                //non ci sono transazioni aperte
                return null;
            }
        }

        public Operator GetLastOperatorOnMachine(string assetId)
        {
            try
            {
                Transaction t = DBContext.Transactions.Where(x => x.MachineId == assetId && x.OperatorId != null).OrderByDescending(x => x.Start).FirstOrDefault();
                if (t != null)
                {
                    return t.Operator;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Operator GetLastOperatorOnMachinePerJob(string assetId, string jobId)
        {
            try
            {
                Transaction t = DBContext.Transactions.Where(x => x.MachineId == assetId && x.JobId == jobId && x.OperatorId != null).OrderByDescending(x => x.Start).FirstOrDefault();
                if (t != null)
                {
                    return t.Operator;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Transaction> TransactionToJustifyList(string assetId)
        {
            return DBContext.Transactions.Where(x => x.MachineId == assetId && string.IsNullOrEmpty(x.CauseId) && !x.Open).OrderBy(x => x.Start).ToList();
        }

        public void UpdateLastTransactionOpen(string assetId, params KeyValuePair<MesEnum.TransactionParam, object>[] parameters)
        {
            Transaction t = GetLastTransactionOpen(assetId);
            foreach (var param in parameters)
            {
                switch (param.Key)
                {
                    case MesEnum.TransactionParam.Cause:
                        CauseService cService = new CauseService();
                        t.CauseId = cService.GetCauseByCode(((int)param.Value).ToString(), OperatorHelper.ApplicationId)?.Id;
                        break;
                    case MesEnum.TransactionParam.Job:
                        if (!string.IsNullOrEmpty((string)param.Value))
                        {
                            t.JobId = (string)param.Value;
                        }
                        else
                        {
                            t.JobId = null;
                        }
                        break;
                    case MesEnum.TransactionParam.Mold:
                        if (!string.IsNullOrEmpty((string)param.Value))
                        {
                            t.MoldId = (string)param.Value;
                        }
                        else
                        {
                            t.MoldId = null;
                        }
                        break;
                    case MesEnum.TransactionParam.Operator:
                        if (!string.IsNullOrEmpty((string)param.Value))
                        {
                            t.OperatorId = (string)param.Value;
                        }
                        else
                        {
                            t.OperatorId = null;
                        }
                        break;
                }
            }
            t.LastUpdate = DateTime.Now;

            DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
            DBContext.SaveChanges();
        }

        public bool LastTransactionOpenIsInProduction(string assetId)
        {
            CauseService cService = new CauseService();
            string causeId = cService.GetCauseByCode(((int)MES.Core.MesEnum.Cause.Production).ToString(), OperatorHelper.ApplicationId)?.Id;
            Transaction t = GetLastTransactionOpen(assetId);
            if (t.CauseId == causeId)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Transaction> GetTransactionListByCurrentJob(string assetId)
        {
            Transaction lastT = GetLastTransactionOpen(assetId);
            string jobId = lastT.JobId;
            if (!string.IsNullOrEmpty(jobId))
            {
                return DBContext.Transactions.Where(x => x.MachineId == assetId && x.JobId == jobId).OrderBy(x => x.Start).ToList();
            }
            return null;
        }

        public List<Transaction> GetTransactionListByJobId(string assetId, string jobId)
        {
            if (!string.IsNullOrEmpty(jobId))
            {
                return DBContext.Transactions.Include("Job.Order.CustomerOrder").Include("Job.Order.Article").Include("Cause").Include("Job").Where(x => x.MachineId == assetId && x.JobId == jobId).OrderBy(x => x.Start).ToList();
            }
            return null;
        }

        /// <summary>
        /// Aggiorna il jobId per tutte le transazioni esistenti dall'ultima accensione della macchina
        /// (da usare solo se arriva una causale produzione in automatico)
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="jobId"></param>
        public void UpdateJobIdInProdTransaction(string assetId, string jobId)
        {
            //ultima transazione in cui la macchina era in off
            Transaction lastTOff = DBContext.Transactions.Where(x => x.MachineId == assetId && !x.Status).OrderByDescending(x => x.Start).First();

            //lista delle transazioni senza job da quando la macchina è tornata in on
            List<Transaction> list = DBContext.Transactions.Where(x => x.MachineId == assetId && x.Start >= lastTOff.Start && string.IsNullOrEmpty(x.JobId) && x.Status).ToList();
            //TODO: considerare di aggiornare solo le transazioni che hanno causale produzione e non tutte
            foreach (Transaction t in list)
            {
                t.JobId = jobId;
            }
            TransactionRepository rep = new TransactionRepository();
            rep.UpdateAll(list);
            rep.SaveChanges();
        }

        public List<CustomerOrder> SearchCustomerOrderPerMachine(string assetId)
        {
            IQueryable<Transaction> machineList = DBContext.Transactions.Where(x => x.MachineId == assetId);
            List<CustomerOrder> orderList = machineList.Where(x => x.Job.Order.CustomerOrder != null).Select(x => x.Job.Order.CustomerOrder).Distinct().ToList();
            return orderList;
        }

        public decimal GetQtyProducedPerJob(Job job)
        {
            return GetQtyProducedPerJob(job.Id);
        }

        public decimal GetQtyProducedPerJob(string jobId)
        {
            if (!string.IsNullOrEmpty(jobId))
            {
                return DBContext.Transactions.Where(x => x.JobId == jobId).Sum(x => (x.PartialCounting - x.BlankShot) * (x.CavityMoldNum > 0 ? x.CavityMoldNum : 1));
            }
            return 0;
        }

        public Transaction GetLastProdTransactionPerJob(string assetId, string jobId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            CauseService causeService = new CauseService();
            List<string> prod = causeService.GetListCauseIdByGroupName("Produzione", applicationId);
            return DBContext.Transactions.Where(x => x.MachineId == assetId && x.JobId == jobId && prod.Contains(x.CauseId) && x.PartialCounting > 0).OrderByDescending(x => x.Start).First();
        }

        public Transaction GetLastProdTransactionPerOperator(string operatorId, string jobId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            CauseService causeService = new CauseService();
            List<string> prod = causeService.GetListCauseIdByGroupName("Produzione", applicationId);
            return DBContext.Transactions.Where(x => x.JobId == jobId && x.OperatorId == operatorId && prod.Contains(x.CauseId) && x.PartialCounting > 0).OrderByDescending(x => x.Start).FirstOrDefault();
        }

        #region Quality
        public List<Transaction> GetProdTransactionForQuality(string assetId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            CauseService causeService = new CauseService();
            List<string> prod = causeService.GetListCauseIdByGroupName("Produzione", applicationId);
            DateTime date = DateTime.Today.AddDays(-1);
            List<Transaction> returnList = DBContext.Transactions.Where(x => x.MachineId == assetId && prod.Contains(x.CauseId) && (x.PartialCounting > 0 || x.QtyProductionWaste > 0 && !x.Exported) && x.Start >= date).ToList();
            Transaction last = GetLastProdTransaction(assetId, applicationId);
            if (!returnList.Contains(last))
            {
                returnList.Add(last);
            }
            return returnList;
        }

        public Transaction GetLastProdTransaction(string assetId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }

            CauseService causeService = new CauseService();
            List<string> prod = causeService.GetListCauseIdByGroupName("Produzione", applicationId);
            return DBContext.Transactions.Where(x => x.MachineId == assetId && prod.Contains(x.CauseId) && !x.Exported).OrderByDescending(x => x.Start).First();
        }
        #endregion

        #region Operator on transaction
        public void AddOperator(string assetId, List<Operator> operators)
        {}

        public void AddOperator(string assetId, Operator op)
        {}

        public void RemoveOperator(string assetId, List<Operator> operators)
        {}

        public void RemoveOperator(string assetId, Operator op)
        {}
        #endregion

        public void AddBlankShot(int transactionId, decimal shots)
        {
            Transaction t = DBContext.Transactions.Where(x => x.Id == transactionId).FirstOrDefault();
            if (t != null)
            {
                if (t.PartialCounting > 0 && shots < t.PartialCounting)
                {
                    t.BlankShot = shots;
                    DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
                    DBContext.SaveChanges();
                }
            }
        }

        public void AddBlankShot(Transaction t, decimal shots)
        {
            if (t.PartialCounting > 0 && shots < t.PartialCounting)
            {
                t.BlankShot = shots;
                DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
                DBContext.SaveChanges();
            }
        }

        public decimal GetBlankShotByJobId(string jobId)
        {
            //TODO: valutare se filtrare la ricerca anche per causale di produzione
            return DBContext.Transactions.Where(x => x.JobId == jobId).Select(x => x.BlankShot).DefaultIfEmpty(0).Sum();
        }

        public Transaction GetNextTransaction(int tId)
        {
            var query = DBContext.Transactions.Where(x => x.Id == tId);
            var transaction = DBContext.Transactions.Where(x => x.MachineId == query.Select(y => y.MachineId).FirstOrDefault() && x.Start >= query.Select(y => y.End).FirstOrDefault()).OrderBy(x => x.Start).FirstOrDefault();
            return transaction;
        }

        public List<Transaction> GetAllTransactionOpen(IEnumerable<string> assetIdList)
        {
            return DBContext.Transactions.Where(x => assetIdList.Contains(x.MachineId) && x.Open).OrderByDescending(x => x.Start).ToList();
        }
    }
}