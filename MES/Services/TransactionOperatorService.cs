﻿using MainCloudFramework.Services;
using MES.Core;
using MES.Models;
using MES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class TransactionOperatorService : BaseService<MesDbContext>
    {
        TransactionOperatorRepository repos = new TransactionOperatorRepository();

        public void OpenTransaction(string machineId, string operatorId, string roleId, string operationName, string jobId, int? transId)
        {
            TransactionOperator newTrans = new TransactionOperator();
            newTrans.DateStart = DateTime.Now;
            newTrans.MachineId = machineId;
            newTrans.OperatorId = operatorId;
            newTrans.RoleId = roleId;
            newTrans.OperationName = operationName;
            newTrans.JobId = jobId;
            newTrans.QualityTransactionId = transId;
            newTrans.Open = true;

            repos.Insert(newTrans);
            repos.SaveChanges();
        }

        public void OpenTransaction(string machineId, Operator operatorId, string operationName)
        {
            OpenTransaction(machineId, operatorId.Id, operatorId.Roles.Count > 0 ? operatorId.Roles.First().RoleId : null, operationName, null, null);
        }

        public void OpenTransaction(string machineId, string operatorId, string operationName)
        {
            OperatorRepository opRep = new OperatorRepository();
            Operator op = opRep.FindByID(operatorId);
            OpenTransaction(machineId, operatorId, op.Roles.Count > 0 ? op.Roles.First().RoleId : null, operationName, null, null);
        }

        public void OpenTransaction(string machineId, string operatorId, MesEnum.OperationName operationName)
        {
            OperatorRepository opRep = new OperatorRepository();
            Operator op = opRep.FindByID(operatorId);
            OpenTransaction(machineId, operatorId, op.Roles.Count > 0 ? op.Roles.First().RoleId : null, operationName.ToString(), null, null);
        }

        public void OpenTransaction(string machineId, string operatorId, MesEnum.OperationName operationName, string jobId)
        {
            OperatorRepository opRep = new OperatorRepository();
            Operator op = opRep.FindByID(operatorId);
            OpenTransaction(machineId, operatorId, op.Roles.Count > 0 ? op.Roles.First().RoleId : null, operationName.ToString(), jobId, null);
        }

        public void OpenTransaction(string machineId, string operatorId, MesEnum.OperationName operationName, string jobId, int? transId)
        {
            OperatorRepository opRep = new OperatorRepository();
            Operator op = opRep.FindByID(operatorId);
            OpenTransaction(machineId, operatorId, op.Roles.Count > 0 ? op.Roles.First().RoleId : null, operationName.ToString(), jobId, transId);
        }

        public void OpenTransaction(string machineId, Operator operatorId, MesEnum.OperationName operationName)
        {
            OpenTransaction(machineId, operatorId.Id, operatorId.Roles.Count > 0 ? operatorId.Roles.First().RoleId : null, operationName.ToString(), null, null);
        }

        public void CloseTransaction(string machineId, string operatorId, params MesEnum.OperationName[] operationName)
        {
            foreach (var oper in operationName)
            {
                CloseTransaction(machineId, operatorId, oper.ToString());
            }
        }

        public void CloseTransaction(string machineId, string operatorId, string operationName)
        {
            TransactionOperator t = DBContext.TransactionOperators.Where(x => x.MachineId == machineId && x.OperatorId == operatorId && x.OperationName == operationName && x.Open).FirstOrDefault();
            if (t != null)
            {
                //DBContext.Database.ExecuteSqlCommand("UPDATE [dbo].[TransactionOperators] SET [DateEnd] = @p1, [Open] = 'False' WHERE [Id] = @p0", t.Id, DateTime.Now);
                t.Open = false;
                t.DateEnd = DateTime.Now;
                DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
                DBContext.SaveChanges();
            }
        }

        public void CloseTransaction(int transactionId)
        {
            TransactionOperator t = DBContext.TransactionOperators.Where(x => x.Id == transactionId).First();
            t.Open = false;
            t.DateEnd = DateTime.Now;
            DBContext.Entry(t).State = System.Data.Entity.EntityState.Modified;
            DBContext.SaveChanges();
        }

        public List<TransactionOperator> OperatorsOnMachine(string assetId)
        {
            return repos.ReadAll(x => x.MachineId == assetId && x.OperationName == MesEnum.OperationName.LogIn.ToString() && x.Open).ToList();
        }

        public Operator FirstOperatorOnMachine(string assetId)
        {
            return repos.ReadAll(x => x.MachineId == assetId && x.OperationName == MesEnum.OperationName.LogIn.ToString() && x.Open).OrderBy(x => x.DateStart).Select(x => x.Operator).FirstOrDefault();
        }

        public bool IsOperatorOnMachine(string assetId, string opId)
        {
            return repos.ReadAll(x => x.MachineId == assetId && x.OperatorId == opId && x.OperationName == MesEnum.OperationName.LogIn.ToString() && x.Open).Any();
        }

        public bool TransactionOpenPerOperator(string assetId, string opId)
        {
            return repos.ReadAll(x => x.MachineId == assetId && x.OperatorId == opId && x.Open).Any();
        }

        public List<TransactionOperator> PreviousWorkshift(string assetId)
        {
            DateTime yesterday = DateTime.Today.AddDays(-1);
            return repos.ReadAll(x => x.MachineId == assetId && x.DateStart >= yesterday).ToList();
        }
    }
}