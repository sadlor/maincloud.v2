﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class OperatorService : BaseService<MesDbContext>
    {
        public bool ValidateOperatorAuthentication(string registrationId, string pin, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            try
            {
                DBContext.Operators.Where(x => x.ApplicationId == applicationId && x.RegistrationId.Equals(registrationId, StringComparison.InvariantCultureIgnoreCase) && x.PIN == pin).First();
                return true;
            }
            catch (InvalidOperationException)
            {
                return false;
            }
        }

        public Operator GetOperatorByRegisterId(string registrationId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            try
            {
                Operator op = DBContext.Operators.Where(x => x.ApplicationId == applicationId && x.Code.Equals(registrationId, StringComparison.InvariantCultureIgnoreCase)).First();
                return op;
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public string GetOperatorNameById(string operatorId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            try
            {
                return DBContext.Operators.Where(x => x.ApplicationId == applicationId && x.Id == operatorId).Select(x => x.UserName).First();
            }
            catch(InvalidOperationException)
            {
                return string.Empty;
            }
        }
    }
}