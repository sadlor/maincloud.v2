﻿using MainCloudFramework.Services;
using MES.Core;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Services
{
    public class QualityLevelService : BaseService<MesDbContext>
    {
        public Color GetColor(string assetId, string jobId)
        {
            QualityLevel q = DBContext.QualityLevels.Where(x => x.MachineId == assetId && x.JobId == jobId).OrderByDescending(x => x.Date).FirstOrDefault();

            if (q != null)
            {
                if (q.Level == MesEnum.QualityLevel.Ok.ToString())
                {
                    return Color.LightGreen;
                }
                if (q.Level == MesEnum.QualityLevel.Warning.ToString())
                {
                    return Color.Yellow;//Color.FromName("#f7f76a");
                }
                if (q.Level == MesEnum.QualityLevel.Stop.ToString())
                {
                    return Color.Red;
                }
            }
            
            return Color.FromName("");
        }

        public QualityLevel GetQualityLevel(string assetId, string jobId)
        {
            return DBContext.QualityLevels.Where(x => x.MachineId == assetId && x.JobId == jobId).OrderByDescending(x => x.Date).FirstOrDefault();
        }
    }
}