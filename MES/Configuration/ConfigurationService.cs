﻿using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using MES.Helper;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Configuration
{
    public class ConfigurationService : BaseService<MesDbContext>
    {
        public List<ModuleConfiguration> GetAllParameter()
        {
            return DBContext.ModuleConfigurations.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && x.Module == "MES").ToList();
        }

        public void InsertDefault()
        {
            // Se non è presente nessun dato inizializza i dati nel DB
            if (!DBContext.ModuleConfigurations.Any())
            {
                DBContext.ModuleConfigurations.AddRange(new List<ModuleConfiguration>() {
                    new ModuleConfiguration() { ParamContext = ParamContext.Availability.ToString(), ParamName = ConfigParam.Warning1.ToString(), Value = "80", Module = "MES", ApplicationId = MultiTenantsHelper.ApplicationId },
                    new ModuleConfiguration() { ParamContext = ParamContext.Availability.ToString(), ParamName = ConfigParam.Warning2.ToString(), Value = "70", Module = "MES", ApplicationId = MultiTenantsHelper.ApplicationId },
                    new ModuleConfiguration() { ParamContext = ParamContext.Efficiency.ToString(), ParamName = ConfigParam.Warning1.ToString(), Value = "85", Module = "MES", ApplicationId = MultiTenantsHelper.ApplicationId },
                    new ModuleConfiguration() { ParamContext = ParamContext.Efficiency.ToString(), ParamName = ConfigParam.Warning2.ToString(), Value = "75", Module = "MES", ApplicationId = MultiTenantsHelper.ApplicationId },
                    new ModuleConfiguration() { ParamContext = ParamContext.Quality.ToString(), ParamName = ConfigParam.Warning1.ToString(), Value = "93", Module = "MES", ApplicationId = MultiTenantsHelper.ApplicationId },
                    new ModuleConfiguration() { ParamContext = ParamContext.Quality.ToString(), ParamName = ConfigParam.Warning2.ToString(), Value = "85", Module = "MES", ApplicationId = MultiTenantsHelper.ApplicationId },
                    new ModuleConfiguration() { ParamContext = ParamContext.OEE.ToString(), ParamName = ConfigParam.Warning1.ToString(), Value = "70", Module = "MES", ApplicationId = MultiTenantsHelper.ApplicationId },
                    new ModuleConfiguration() { ParamContext = ParamContext.OEE.ToString(), ParamName = ConfigParam.Warning2.ToString(), Value = "60", Module = "MES", ApplicationId = MultiTenantsHelper.ApplicationId },
                    new ModuleConfiguration() { ParamContext = ParamContext.StandardRate.ToString(), ParamName = ConfigParam.StandardRateFromMachine.ToString(), Value = bool.FalseString, Module = "MES", ApplicationId = MultiTenantsHelper.ApplicationId }});
                DBContext.SaveChanges();
            }
        }

        public void ModifyParameter(ModuleConfiguration entity)
        {
            DBContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            DBContext.SaveChanges();
        }

        public ModuleConfiguration GetByID(int id)
        {
            return DBContext.ModuleConfigurations.Find(id);
        }

        public Color GetColorByParameter(ParamContext param, decimal value)
        {
            return GetColorByParameter(param.ToString(), value);
        }

        public Color GetColorByParameter(string paramContext, decimal value)
        {
            List<ModuleConfiguration> itemList = GetByParameter(paramContext);
            if (itemList.Count > 0)
            {
                if (value <= itemList.Min(x => Convert.ToDecimal(x.Value)))
                {
                    return Color.Red;
                }
                else
                {
                    if (value >= itemList.Max(x => Convert.ToDecimal(x.Value)))
                    {
                        return Color.LightGreen;
                    }
                    else
                    {
                        return Color.Yellow;
                    }
                }
            }
            return Color.Transparent;
        }

        public List<ModuleConfiguration> GetByParameter(ParamContext param)
        {
            return GetByParameter(param.ToString());
        }

        public List<ModuleConfiguration> GetByParameter(string param)
        {
            string applicationId = MultiTenantsHelper.ApplicationId;
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = OperatorHelper.ApplicationId;
            }
            return DBContext.ModuleConfigurations.Where(x => x.ApplicationId == applicationId && x.Module == "MES" && x.ParamContext == param).ToList();
        }
    }
}