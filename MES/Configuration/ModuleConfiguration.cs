﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Configuration
{
    public class ModuleConfiguration
    {
        public ModuleConfiguration()
        {
        }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        [Required]
        public string ParamContext { get; set; }

        [Required]
        public string ParamName { get; set; }

        //internal string _Value { get; set; }
        //[NotMapped]
        //public object Value
        //{
        //    get
        //    {
        //        return Convert.ChangeType(_Value, TypeCode.Object);
        //    }
        //    set
        //    {
        //        _Value = value.ToString();
        //    }
        //}
        [Required]
        public string Value { get; set; }

        public string MeasureUnit { get; set; }
        public string Module { get; set; }
        public string ApplicationId { get; set; }
    }
}