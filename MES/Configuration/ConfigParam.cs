﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Configuration
{
    public enum ConfigParam
    {
        Warning1,
        Warning2,
        StandardRateFromMachine,
        DestinationPath
    }

    public enum ParamContext
    {
        Availability,
        Efficiency,
        Quality,
        OEE,
        StandardRate,
        PartProgram
    }
}