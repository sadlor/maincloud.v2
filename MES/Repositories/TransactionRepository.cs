﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class TransactionRepository : BaseRepository<Transaction, MesDbContext>
    {
        public void UpdateAll(List<Transaction> list)
        {
            foreach(Transaction t in list)
            {
                Update(t);
            }
        }

        public new void Update(Transaction entity)
        {
            //if (entity.PartialCounting > 0)
            //{
            //    entity.QtyProduced = entity.PartialCounting * (entity.CavityMoldNum > 0 ? entity.CavityMoldNum : 1);
            //}
            entity.LastUpdate = DateTime.Now;
            DBContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public string GetJobId(int transId)
        {
            return DBContext.Transactions.Where(x => x.Id == transId).Select(x => x.JobId).FirstOrDefault();
        }

        /// <summary>
        /// Reads all objects in which all of the predicates return true.
        /// </summary>
        public override IQueryable<Transaction> ReadAll(params Expression<Func<Transaction, Boolean>>[] predicates)
        {
            IQueryable<Transaction> results = DBContext.Transactions.Include("Job.Order.CustomerOrder").Include("Job.Order.Article");
            //IQueryable <Transaction> results = DBContext.Set<Transaction>();
            if (predicates == null || predicates.Length <= 0) { return results; }

            foreach (var predicate in predicates)
            {
                results = results.Where(predicate);
            }

            return results;
        }

        public void ExecuteSqlCommand(string query)
        {
            DBContext.Database.ExecuteSqlCommand(query);
        }

        /// <summary>
        /// se IdentityInsert è a true, non bisogna usare SaveChanges()
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="identityInsert"></param>
        public void InsertAll(IEnumerable<Transaction> entities, bool identityInsert = false)
        {
            //if (identityInsert)
            //{
            //    DBContext.Database.Connection.Open();
            //    //DBContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Transactions ON");
            //    DBContext.Transactions.AddRange(entities);
            //    DBContext.SaveChanges();
            //    //DBContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Transactions OFF");
            //    DBContext.Database.Connection.Close();
            //}
            //else
            //{
            //    DBContext.Transactions.AddRange(entities);
            //}
        }
    }
}