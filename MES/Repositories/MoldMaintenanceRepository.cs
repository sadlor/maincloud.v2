﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class MoldMaintenanceRepository : BaseRepository<MoldMaintenance, MesDbContext>
    {
        public List<MoldMaintenance> GetHistory(string moldId, decimal progressiveCounter = 0)
        {
            return DBContext.MoldMaintenances.Where(x => x.MoldId == moldId && x.ProgressiveCounter >= progressiveCounter).OrderBy(x => x.Date).ToList();
        }
    }
}