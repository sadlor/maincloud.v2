﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class TransactionMoldRepository : BaseRepository<TransactionMold, MesDbContext>
    {
        public void UpdateAll(List<TransactionMold> list)
        {
            foreach (TransactionMold t in list)
            {
                Update(t);
            }
        }

        public new void Update(TransactionMold entity)
        {
            //if (entity.PartialCounting > 0)
            //{
            //    entity.QtyProduced = entity.PartialCounting * (entity.CavityMoldNum > 0 ? entity.CavityMoldNum : 1);
            //}
            entity.LastUpdate = DateTime.Now;
            DBContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }
    }
}