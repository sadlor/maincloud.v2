﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class ControlPlanDetailExecutedRepository : BaseRepository<ControlPlanDetailExecuted, MesDbContext>
    {
        public ControlPlanDetailExecuted GetLatestByJobId(string JobId)
        {
            try
            {
                return DBContext.ControlPlanDetailExecuteds.Where(x => x.JobId == JobId).OrderByDescending(x => x.ExecutionDate).First();
            }
            catch (Exception ex) { }
            return null;
        }

        public List<ControlPlanDetail> GetControlPlanDetailsByJobAndOperator(string JobId, string OperatorId)
        {
            try
            {
                List<ControlPlanDetailExecuted> cpdeList = (OperatorId != null) ? DBContext.ControlPlanDetailExecuteds.Where(x => x.JobId == JobId && x.OperatorId == OperatorId).ToList() : DBContext.ControlPlanDetailExecuteds.Where(x => x.JobId == JobId).ToList();
                List<ControlPlanDetail> cpdList = new List<ControlPlanDetail>();
                List<int> ids = new List<int>();

                foreach (ControlPlanDetailExecuted cpde in cpdeList)
                {
                    ControlPlanDetail cpd = new ControlPlanDetail();
                    cpd.Id = cpde.ControlPlanDetailId;
                    cpd.Description = cpde.Description;
                    cpd.ControlType = cpde.ControlType;

                    if (!ids.Contains(cpd.Id))
                    {
                        cpdList.Add(cpd);
                        ids.Add(cpd.Id);
                    }
                }
                return cpdList;
            }
            catch (Exception ex) { }
            return null;
        }

        public List<ControlPlanDetailExecuted> GetByCpdJobAndOperator(int cpdId, string JobId, string OperatorId)
        {
            try
            {
                return (OperatorId != null) ? DBContext.ControlPlanDetailExecuteds.Where(x => x.JobId == JobId && x.OperatorId == OperatorId && x.ControlPlanDetailId == cpdId).OrderByDescending(x => x.ExecutionDate).ToList()
                       : DBContext.ControlPlanDetailExecuteds.Where(x => x.JobId == JobId && x.ControlPlanDetailId == cpdId).OrderByDescending(x => x.ExecutionDate).ToList();
            }
            catch (Exception ex) { }
            return null;
        }
    }
}
