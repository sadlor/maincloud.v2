﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class ControlPlanRepository : BaseRepository<ControlPlan, MesDbContext>
    {

        public ControlPlan GetControlPlanById(int ControlPlanId)
        {
            return DBContext.ControlPlans.Where(x => x.Id == ControlPlanId).First();
        }
    }
}
