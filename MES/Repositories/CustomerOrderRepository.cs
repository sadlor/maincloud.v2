﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class CustomerOrderRepository : BaseRepository<CustomerOrder, MesDbContext>
    {
        /// <summary>
        /// se IdentityInsert è a true, non bisogna usare SaveChanges()
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="identityInsert"></param>
        public void InsertAll(IEnumerable<CustomerOrder> entities, bool identityInsert = false)
        {
            //if (identityInsert)
            //{
            //    DBContext.Database.Connection.Open();
            //    DBContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT CustomerOrders ON");
            //    DBContext.CustomerOrders.AddRange(entities);
            //    DBContext.SaveChanges();
            //    DBContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT CustomerOrders OFF");
            //    DBContext.Database.Connection.Close();
            //}
            //else
            //{
            //    DBContext.CustomerOrders.AddRange(entities);
            //}
        }

        public void ExecuteSqlCommand(string query)
        {
            DBContext.Database.ExecuteSqlCommand(query);
        }
    }
}