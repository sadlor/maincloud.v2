﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class JobBatchRepository : BaseRepository<JobBatch, MesDbContext>
    {
        /// <summary>
        /// Return all batches by jobId
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="previous">If true, return only batches confirmed</param>
        /// <returns></returns>
        public List<JobBatch> GetListByJob(string jobId, bool previous = false)
        {
            if (previous)
            {
                return DBContext.JobBatches.Include("ArticleBatch.Article").Include("Job").Where(x => x.JobId == jobId && x.Verified).ToList();
            }
            else
            {
                return DBContext.JobBatches.Include("ArticleBatch.Article").Include("Job").Where(x => x.JobId == jobId).ToList();
            }
        }

        public void DeleteBatches(string machineId)
        {
            var batches = ReadAll(x => x.MachineId == machineId && !x.Verified && x.Exported == null);
            foreach (var batch in batches)
            {
                DBContext.Entry(batch).State = System.Data.Entity.EntityState.Deleted;
            }
            DBContext.SaveChanges();
        }

        public void DeleteLastBatch(string machineId)
        {
            var batch = ReadAll(x => x.MachineId == machineId && !x.Verified && x.Exported == null).OrderByDescending(x => x.Start).FirstOrDefault();
            if (batch != null)
            {
                DBContext.Entry(batch).State = System.Data.Entity.EntityState.Deleted;
                DBContext.SaveChanges();
            }
        }

        public List<JobBatch> GetAllNotVerified(string machineId)
        {
            return ReadAll(x => x.MachineId == machineId && !x.Verified).ToList();
        }
    }
}