﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class MaintenancePlanOperatorRepository : BaseRepository<MaintenancePlanOperator, MesDbContext>
    {

        public MaintenancePlanOperator GetByOperatorMaintenanceId(string OperatorId, int MaintenanceId)
        {
            try
            {
                return DBContext.MaintenancePlanOperators.Where(x => x.OperatorId == OperatorId && x.MaintenanceId == MaintenanceId && x.Executed == false).First();
            }
            catch (Exception) { }
            return null;
        }
    }
}
