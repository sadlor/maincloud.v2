﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class OrderRepository : BaseRepository<Order, MesDbContext>
    {
        public List<Order> GetAllOrders()
        {
            return DBContext.Orders.Include("CustomerOrder").Include("Article").Where(x => x.ApplicationId == MainCloudFramework.Web.Helpers.MultiTenantsHelper.ApplicationId).ToList();
        }

        /// <summary>
        /// se IdentityInsert è a true, non bisogna usare SaveChanges()
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="identityInsert"></param>
        public void InsertAll(IEnumerable<Order> entities, bool identityInsert = false)
        {
            //if (identityInsert)
            //{
            //    DBContext.Database.Connection.Open();
            //    DBContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Orders ON");
            //    DBContext.Orders.AddRange(entities);
            //    DBContext.SaveChanges();
            //    DBContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Orders OFF");
            //    DBContext.Database.Connection.Close();
            //}
            //else
            //{
            //    DBContext.Orders.AddRange(entities);
            //}
        }

        public void ExecuteSqlCommand(string query)
        {
            DBContext.Database.ExecuteSqlCommand(query);
        }
    }
}