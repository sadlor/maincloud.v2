﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class JobProductionWasteRepository : BaseRepository<JobProductionWaste, MesDbContext>
    {
        //public new void Insert(JobProductionWaste entity)
        //{
        //    JobProductionWaste row = DBContext.JobProductionWaste.Where(x => x.AssetId == entity.AssetId &&
        //                                                                     x.CauseId == entity.CauseId &&
        //                                                                     x.JobId == entity.JobId &&
        //                                                                     x.OperatorId == entity.OperatorId).FirstOrDefault();  //TODO: chiedere se scarti fatti da operatori diversi sullo stesso job vanno salvati diversamente e nelle analisi bisognerà fare la somma
        //    if (row != null)
        //    {
        //        //esiste già quindi aggiorno
        //        row.QtyProductionWaste += entity.QtyProductionWaste;
        //        row.Date = entity.Date;
        //        base.Update(row);
        //    }
        //    else
        //    {
        //        //non esiste quindi inserisco nuovo
        //        base.Insert(entity);
        //    }
        //}

        public void UpdateAll(List<JobProductionWaste> list)
        {
            foreach (JobProductionWaste item in list)
            {
                Update(item);
            }
        }
    }
}