﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class TransactionOperatorRepository : BaseRepository<TransactionOperator, MesDbContext>
    {
        public void UpdateAll(List<TransactionOperator> list)
        {
            foreach (TransactionOperator t in list)
            {
                Update(t);
            }
        }

        public void ExecuteSqlCommand(string query)
        {
            DBContext.Database.ExecuteSqlCommand(query);
        }

        /// <summary>
        /// se IdentityInsert è a true, non bisogna usare SaveChanges()
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="identityInsert"></param>
        public void InsertAll(IEnumerable<TransactionOperator> entities, bool identityInsert = false)
        {
            //if (identityInsert)
            //{
            //    DBContext.Database.Connection.Open();
            //    //DBContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT TransactionOperators ON");
            //    DBContext.TransactionOperators.AddRange(entities);
            //    DBContext.SaveChanges("ok");
            //    //DBContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT TransactionOperators OFF");
            //    DBContext.Database.Connection.Close();
            //}
            //else
            //{
            //    DBContext.TransactionOperators.AddRange(entities);
            //}
        }
    }
}