﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class ControlPlanDetailRepository : BaseRepository<ControlPlanDetail, MesDbContext>
    {
        public List<ControlPlanDetail> GetAllByControlPlanId(int ControlPlanId)
        {
            return DBContext.ControlPlanDetails.Where(x => x.ControlPlanId == ControlPlanId).ToList();
        }

        public ControlPlanDetail GetControlPlanDetailById(int ControlPlanDetailId)
        {
            return DBContext.ControlPlanDetails.Where(x => x.Id == ControlPlanDetailId).First();
        }
    }
}
