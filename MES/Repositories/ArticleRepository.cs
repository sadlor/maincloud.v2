﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class ArticleRepository : BaseRepository<Article, MesDbContext>
    {
        public new void Delete(Article entity)
        {
            //foreach (var art in DBContext.ArticleMolds.Where(x => x.ArticleId == entity.Id))
            //{
            //    DBContext.ArticleMolds.Remove(art);
            //}
            DBContext.ArticleMolds.RemoveRange(DBContext.ArticleMolds.Where(x => x.ArticleId == entity.Id));
            DBContext.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
            //DBContext.SaveChanges();
        }

        /// <summary>
        /// se IdentityInsert è a true, non bisogna usare SaveChanges()
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="identityInsert"></param>
        public void InsertAll(IEnumerable<Article> entities, bool identityInsert = false)
        {
            //if (identityInsert)
            //{
            //    DBContext.Database.Connection.Open();
            //    DBContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Articles ON");
            //    DBContext.Articles.AddRange(entities);
            //    DBContext.SaveChanges();
            //    DBContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Articles OFF");
            //    DBContext.Database.Connection.Close();
            //}
            //else
            //{
            //    DBContext.Articles.AddRange(entities);
            //}
        }

        public void ExecuteSqlCommand(string query)
        {
            DBContext.Database.ExecuteSqlCommand(query);
        }
    }
}