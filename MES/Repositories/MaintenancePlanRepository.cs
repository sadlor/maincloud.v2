﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class MaintenancePlanRepository : BaseRepository<MaintenancePlan, MesDbContext>
    {

        public MaintenancePlan GetMaintenancePlanById(int MaintenancePlanId)
        {
            try
            {
                return DBContext.MaintenancePlans.Where(x => x.Id == MaintenancePlanId).First();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
