﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class JobRepository : BaseRepository<Job, MesDbContext>
    {
        public new void Delete(Job entity)
        {
            DBContext.JobProductionWaste.RemoveRange(DBContext.JobProductionWaste.Where(x => x.JobId == entity.Id));
            DBContext.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
        }

        public decimal GetStandardRate(string jobId)
        {
            return DBContext.Jobs.Where(x => x.Id == jobId).Select(x => x.StandardRate).FirstOrDefault();
        }

        public decimal GetCycleTime(string jobId)
        {
            return DBContext.Jobs.Where(x => x.Id == jobId).Select(x => x.CycleTime).FirstOrDefault();
        }

        public decimal GetCavityMoldNum(string jobId)
        {
            var i = DBContext.Jobs.Where(x => x.Id == jobId).Select(x => x.CavityMoldNum).FirstOrDefault();
            return i ?? 1;
        }

        public decimal GetQtyOrdered(string jobId)
        {
            return DBContext.Jobs.Include("Order").Where(x => x.Id == jobId).Select(x => x.Order.QtyOrdered).FirstOrDefault();
        }

        public int? GetOrderId(string jobId)
        {
            return DBContext.Jobs.Where(x => x.Id == jobId).Select(x => x.OrderId).FirstOrDefault();
        }

        public string GetCustomerOrderCode(string jobId)
        {
            return DBContext.Jobs.Include("Order").Include("Order.CustomerOrder").Where(x => x.Id == jobId).Select(x => x.Order.CustomerOrder.OrderCode).FirstOrDefault();
        }

        public Order GetOrder(string jobId)
        {
            return DBContext.Jobs.Include("Order.CustomerOrder").Include("Order.Article").Where(x => x.Id == jobId).Select(x => x.Order).FirstOrDefault();
        }

        public List<Job> GetAllJobs()
        {
            return DBContext.Jobs.Include("Order").Include("Order.Article").Where(x => x.ApplicationId == MainCloudFramework.Web.Helpers.MultiTenantsHelper.ApplicationId).ToList();
        }

        public List<Job> GetAllByControlPlanId(int ControlPlanId)
        {
            return DBContext.Jobs.Where(x => x.ControlPlanId == ControlPlanId).ToList();
        }

        public string GetMoldId(string jobId)
        {
            return DBContext.Jobs.Where(x => x.Id == jobId).Select(x => x.MoldId).FirstOrDefault();
        }

        public void InsertAll(IEnumerable<Job> entities)
        {
            DBContext.Jobs.AddRange(entities);
        }
    }
}