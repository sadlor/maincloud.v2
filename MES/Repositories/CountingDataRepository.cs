﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class CountingDataRepository : BaseRepository<CountingData, MesDbContext>
    {
        public void InsertAll(IEnumerable<CountingData> list)
        {
            foreach (CountingData item in list)
            {
                try
                {
                    CountingData dbCD = (from c in DBContext.CountingDatas
                                         where c.Start == item.Start &&
                                               c.AssetId == item.AssetId &&
                                               c.JobId == item.JobId &&
                                               c.OperatorId == item.OperatorId
                                         select c).First();
                    if (dbCD == null)
                    {
                        DBContext.CountingDatas.Add(item);
                    }
                    else
                    {
                        dbCD = item;
                    }
                }
                catch (Exception ex)
                {
                    //la sequenza non contiene elementi
                    DBContext.CountingDatas.Add(item);
                }
            }
            try
            {
                DBContext.SaveChanges();
            }
            catch (Exception ex)
            {
            }
        }
    }
}