﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class MaintenancePlanDetailRepository : BaseRepository<MaintenancePlanDetail, MesDbContext>
    {
        public List<MaintenancePlanDetail> GetAllByMaintenancePlanId(int MaintenancePlanId)
        {
            try
            {
                return DBContext.MaintenancePlanDetails.Where(x => x.MaintenancePlanId == MaintenancePlanId).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public MaintenancePlanDetail GetMaintenancePlanDetailById(int MaintenancePlanDetailId)
        {
            try
            {
                return DBContext.MaintenancePlanDetails.Where(x => x.Id == MaintenancePlanDetailId).First();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
