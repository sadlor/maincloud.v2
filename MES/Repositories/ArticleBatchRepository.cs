﻿using MainCloudFramework.Repositories;
using MES.Helper;
using MES.HMI.ViewModel;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MES.Core.MesEnum;

namespace MES.Repositories
{
    public class ArticleBatchRepository : BaseRepository<ArticleBatch, MesDbContext>
    {
        public ArticleBatch AddOrUpdate(BatchViewModel batch, string jobId, string applicationId = null)
        {
            applicationId = OperatorHelper.ApplicationId;
            ArticleBatch b = DBContext.ArticleBatches.Include("Article").Where(x => x.Code == batch.Code && x.Article.Code == batch.ArticleCode).FirstOrDefault();  //&& x.Article.JobId == jobId
            if (b == null)
            {
                //Add
                Article a = DBContext.Articles.Where(x => x.Code == batch.ArticleCode && x.JobId == jobId).FirstOrDefault();
                if (a == null)
                {
                    a = new Article() { ApplicationId = applicationId, Code = batch.ArticleCode, Description = batch.ArticleDescription };
                    DBContext.Articles.Add(a);
                    DBContext.SaveChanges();
                    //a.ClientId = a.Id; TODO: verificare clientId articolo
                }
                ArticleBatch newBatch = new ArticleBatch();
                newBatch.ArticleId = a.Id;
                newBatch.Code = batch.Code;
                newBatch.Quantity = batch.Weight ?? 0;
                newBatch.ExtraData = new Dictionary<string, object>() { { ExtraData.Height.ToString(), batch.Height }, { ExtraData.Width.ToString(), batch.Width } };
                newBatch.ApplicationId = applicationId;
                Insert(newBatch);
                DBContext.SaveChanges();
                return newBatch;
            }
            else
            {
                //Update
                //b.ExtraData = new Dictionary<string, object>() { { ExtraData.Height.ToString(), batch.Height }, { ExtraData.Width.ToString(), batch.Width } };
                //DBContext.Entry(b).State = System.Data.Entity.EntityState.Modified;
                //DBContext.SaveChanges();
                return b;
            }
        }
    }
}