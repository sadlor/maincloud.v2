﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class CauseTypeRepository : BaseRepository<CauseType, MesDbContext>
    {
        public new void Delete(CauseType entity)
        {
            DBContext.Causes.RemoveRange(DBContext.Causes.Where(x => x.CauseTypeId == entity.Id));
            DBContext.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
        }
    }
}