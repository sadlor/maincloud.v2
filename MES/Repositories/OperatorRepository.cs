﻿using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class OperatorRepository : BaseRepository<Operator, MesDbContext>
    {
        public new void Delete(Operator user)
        {
            user.Roles.Clear();
            base.Delete(user);
        }

        public string GetUserName(string operatorId)
        {
            return DBContext.Operators.Where(x => x.Id == operatorId).Select(x => x.UserName).FirstOrDefault();
        }

        public string GetBadge(string operatorId)
        {
            return DBContext.Operators.Where(x => x.Id == operatorId).Select(x => x.Badge).FirstOrDefault();
        }

        //public new void Delete(Operator user)
        //{
        //    DBContext.Database.ExecuteSqlCommand("DELETE FROM Operators WHERE Id=@p0", user.Id);
        //}
    }
}