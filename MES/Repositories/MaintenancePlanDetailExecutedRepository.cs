﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Repositories;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Repositories
{
    public class MaintenancePlanDetailExecutedRepository : BaseRepository<MaintenancePlanDetailExecuted, MesDbContext>
    {

        public IQueryable<MaintenancePlanDetailExecuted> GetAllByDateAndMoldAndOperatorQuery(DateTime startDate, DateTime endDate, string operatorId, string moldId)
        {
            try
            {
                if (startDate == DateTime.MinValue || endDate == DateTime.MaxValue)
                {
                    return DBContext.MaintenancePlanDetailExecuteds.Where(x => x.MaintenanceOperatorPlan.OperatorId == operatorId && x.ObjectType == "Mold" && x.ObjectId == moldId);
                }
                else
                {
                    return DBContext.MaintenancePlanDetailExecuteds.Where(x => x.MaintenanceOperatorPlan.OperatorId == operatorId && x.ObjectType == "Mold" && x.ObjectId == moldId && x.ExecutionDate >= startDate && x.ExecutionDate <= endDate);
                }
            }
            catch (Exception) { }
            return null;
        }

        public IQueryable<MaintenancePlanDetailExecuted> GetAllByDateAndMoldQuery(DateTime startDate, DateTime endDate, string moldId)
        {
            try
            {
                return DBContext.MaintenancePlanDetailExecuteds.Where(x => x.ObjectType == "Mold" && x.ObjectId == moldId && x.ExecutionDate >= startDate && x.ExecutionDate <= endDate);
            }
            catch (Exception) { }
            return null;
        }

        public DateTime GetExecutionDateByPlanId(int? planId)
        {
            try
            {
                MaintenancePlanDetailExecuted mpde = DBContext.MaintenancePlanDetailExecuteds.Where(x => x.PlanId == planId).First();
                if (mpde != null)
                    return mpde.ExecutionDate;
            }
            catch (Exception) { }
            return new DateTime();
        }

        public string GetOperatorUserNameByPlanId(int? planId)
        {
            try
            {
                MaintenancePlanDetailExecuted mpde = DBContext.MaintenancePlanDetailExecuteds.Where(x => x.PlanId == planId).First();
                if (mpde != null)
                    return mpde.MaintenanceOperatorPlan?.Operator.UserName;
            }
            catch (Exception) { }
            return "";
        }

        public List<MaintenancePlanDetailExecuted> GetAllByPlanId(int? planId)
        {
            try
            {
                return DBContext.MaintenancePlanDetailExecuteds.Where(x => x.PlanId == planId).ToList();
            }
            catch (Exception) { }
            return null;
        }

        public List<MaintenancePlanDetailExecuted> GetAllByDateAndMoldAndOperator(DateTime startDate, DateTime endDate, string operatorId, string moldId)
        {
            try
            {
                return DBContext.MaintenancePlanDetailExecuteds.Where(x => x.MaintenanceOperatorPlan.OperatorId == operatorId && x.ObjectType == "Mold" && x.ObjectId == moldId && x.ExecutionDate >= startDate && x.ExecutionDate <= endDate).ToList();
            }
            catch (Exception) { }
            return null;
        }

        public List<MaintenancePlanDetailExecuted> GetAllByDateAndMold(DateTime startDate, DateTime endDate, string moldId)
        {
            try
            {
                return DBContext.MaintenancePlanDetailExecuteds.Where(x => x.ObjectType == "Mold" && x.ObjectId == moldId && x.ExecutionDate >= startDate && x.ExecutionDate <= endDate).ToList();
            }
            catch (Exception) { }
            return null;
        }

        public MaintenancePlanDetailExecuted GetLatestMaintenanceByMoldId(string moldId)
        {
            try
            {
                return DBContext.MaintenancePlanDetailExecuteds.Where(x => x.ObjectType == "Mold" && x.ObjectId == moldId).OrderByDescending(x => x.ExecutionDate).First();
            }
            catch (Exception) { }
            return null;
        }

        public bool isNextMaintenanceRequiredByMoldId(string moldId, decimal qtyProduced, TimeSpan productionTime)
        {
            MaintenanceRepository mRep = new MaintenanceRepository();
            Maintenance m = mRep.FindByMoldId(moldId);
            if (m == null) return false;

            int? planId = m.MaintenancePlanId;
            MaintenancePlanRepository mpRep = new MaintenancePlanRepository();
            MaintenancePlan mp = mpRep.FindByID(planId);
            if (mp == null) return false;

            MaintenancePlanDetailExecuted mpde = GetLatestMaintenanceByMoldId(moldId);
            if (mpde == null) return true;

            if (mp.QtyFrequencyFlag)
            {
                // Confronto la quantità prodotta attualmente con quella al momento della manutenzione
                return (qtyProduced >= mpde.QtyProduced + mp.QtyFrequency);
            }
            else
            {
                // Confronto il tempo di produzione attuale con quello al momento della manutenzione
                return (productionTime.Ticks >= mpde.ProductionTime + mp.TimeFrequency);
            }
        }

        public TimeSpan getNextEstimatedMaintenanceByMoldId(string moldId, decimal qtyProduced, decimal cadency, TimeSpan productionTime)
        {
            MaintenanceRepository mRep = new MaintenanceRepository();
            Maintenance m = mRep.FindByMoldId(moldId);
            if (m == null) return new TimeSpan();

            int? planId = m.MaintenancePlanId;
            MaintenancePlanRepository mpRep = new MaintenancePlanRepository();
            MaintenancePlan mp = mpRep.FindByID(planId);
            if (mp == null) return new TimeSpan();

            MaintenancePlanDetailExecuted mpde = GetLatestMaintenanceByMoldId(moldId);
            if (mpde == null) return new TimeSpan();

            if (mp.QtyFrequencyFlag)
            {
                // Calcolo pezzi da produrre prima della prossima manutenzione e il tempo previsto
                decimal ticksCadency = cadency / TimeSpan.TicksPerHour; // da cadenza per ora a cadenza per tick
                if (ticksCadency <= 0) return new TimeSpan();

                decimal diff = qtyProduced - (mpde.QtyProduced + (decimal)mp.QtyFrequency);
                long ticksRequired = (long)(diff / ticksCadency); // numero di tick necessari al raggiungimento della quantità da produrre prima della manutenzione necessaria

                return new TimeSpan(productionTime.Ticks + ticksRequired);
            }
            else
            {
                // Calcolo tempo di produzione prima della prossima manutenzione
                long diff = productionTime.Ticks - (mpde.ProductionTime + mp.TimeFrequency);

                return new TimeSpan(productionTime.Ticks + diff);
            }
        }
    }
}
