﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MES.HMI.ViewModel
{
    [Serializable]
    public class BatchViewModel
    {
        public BatchViewModel()
        {
        }

        public BatchViewModel(string code, string articleCode, decimal width, decimal height, decimal weight)
        {
            Code = code;
            ArticleCode = articleCode;
            Width = width;
            Height = height;
            Weight = weight;
        }

        public BatchViewModel(string code, string articleCode, string articleDescription, decimal width, decimal height, decimal weight)
        {
            Code = code;
            ArticleCode = articleCode;
            ArticleDescription = articleDescription;
            Width = width;
            Height = height;
            Weight = weight;
        }

        public string Code { get; set; }
        public string ArticleCode { get; set; }
        public string ArticleDescription { get; set; }
        public decimal? Width { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
    }
}