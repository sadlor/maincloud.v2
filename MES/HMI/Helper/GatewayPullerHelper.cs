﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.HMI.Helper
{
    public static class GatewayPullerHelper
    {
        public static string GetApplicationId(string assetId, bool mold = false)
        {
            using (AssetManagement.Models.AssetManagementDbContext db = new AssetManagement.Models.AssetManagementDbContext())
            {
                if (mold)
                {
                    return db.Molds.Where(x => x.Id == assetId).First().ApplicationId;
                }
                else
                {
                    return db.Assets.Where(x => x.Id == assetId).First().ApplicationId;
                }
            }
        }
    }
}