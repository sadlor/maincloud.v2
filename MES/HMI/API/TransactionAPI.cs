﻿using EverynetModule.Models;
using MES.Core;
using MES.HMI.Helper;
using MES.Models;
using MES.Repositories;
using MES.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.HMI.API
{
    public class TransactionAPI
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("RollingLogFile");

        public void UpdateTransactionSeneca(List<string> assetIdList, IEnumerable<ITransactionLog> transLogList)
        {
            foreach (string assetId in assetIdList.Distinct())
            {
                UpdateTransactionSeneca(assetId, transLogList);
            }
        }

        public void UpdateTransactionSeneca(string assetId, IEnumerable<ITransactionLog> transLogList)
        {
            using (MesDbContext db = new MesDbContext())
            {
                string applicationId = GatewayPullerHelper.GetApplicationId(assetId);
                var productionCode = ((int)MesEnum.Cause.Production).ToString();
                var machineOffCode = ((int)MesEnum.Cause.MachineOff).ToString();
                var setupCode = ((int)MesEnum.Cause.SetUp).ToString();
                string productionCause = db.Causes.Where(x => x.Code == productionCode && x.ApplicationId == applicationId).First().Id;
                string machineOffCause = db.Causes.Where(x => x.Code == machineOffCode && x.ApplicationId == applicationId).First().Id;
                string setupCause = db.Causes.Where(x => x.Code == setupCode && x.ApplicationId == applicationId).First().Id;
                Transaction t;
                try
                {
                    t = db.Transactions.Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).First();
                    var tLog = transLogList.Where(x => x.Machine == assetId).First();
                    if (t.Status == tLog.Status)
                    {
                        //se lo stato è uguale verifico che il contatore non sia cambiato;
                        //se il contatore non cambia per un tempo >= di timeout allora chiudo transazione e ne apro una nuova senza causale;
                        //viceversa se il contatore riprende ad incrementare dopo un periodo di inattività >= timeout
                        //allora chiudo e apro una nuova transazione con la causale di produzione
                        if (tLog.Counter > t.CounterEnd)
                        {
                            //il contatore è aumentato, quindi è in produzione
                            if (string.IsNullOrEmpty(t.CauseId))
                            {
                                //la transazione non ha causale, quindi chiudo e apro una nuova con la causale produzione
                                //verifica prima il timeout, se non è stato superato, aggiorno la transazione in corso con causale produzione
                                if (tLog.Date - t.Start <= Timeout(t, tLog, db))
                                {
                                    t.Duration = (decimal)(tLog.Date - t.Start).TotalSeconds;
                                    if (t.CounterEnd > 60000 && tLog.Counter < 2000)
                                    {
                                        t.CounterEnd = 65536 + tLog.Counter;
                                    }
                                    else
                                    {
                                        t.CounterEnd = tLog.Counter;
                                    }
                                    //t.ProgressiveCounterEnd = tLog.Counter;
                                    t.PartialCounting = t.CounterEnd.Value - t.CounterStart;
                                    t.CauseId = productionCause;
                                    t.LastUpdate = tLog.Date;
                                    db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    t.End = t.LastUpdate;
                                    t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
                                    t.Open = false;
                                    //t.ProgressiveCounterEnd = tLog.Counter;
                                    t.PartialCounting = t.CounterEnd.Value - t.CounterStart;
                                    t.QtyProduced = t.PartialCounting * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
                                    t.LastUpdate = tLog.Date;
                                    db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();

                                    Transaction newT = new Transaction();
                                    newT = t;
                                    newT.Verified = false;
                                    newT.Exported = false;
                                    newT.Start = newT.End.Value;
                                    newT.End = null;
                                    newT.Duration = 0;
                                    newT.Open = true;
                                    newT.Status = tLog.Status;
                                    newT.LastUpdate = tLog.Date;
                                    if (newT.CounterEnd.Value >= 65536)
                                    {
                                        newT.CounterStart = tLog.Counter;
                                    }
                                    else
                                    {
                                        newT.CounterStart = newT.CounterEnd.Value;
                                    }
                                    //newT.ProgressiveCounter = newT.ProgressiveCounterEnd.Value;
                                    newT.CounterEnd = tLog.Counter;
                                    newT.PartialCounting = newT.CounterEnd.Value - newT.CounterStart;
                                    newT.QtyProduced = 0;
                                    newT.CauseId = productionCause; //causale produzione
                                    db.Transactions.Add(newT);
                                }
                            }
                            else
                            {
                                ////aggiorno il contatore
                                ////verifico che la causale in corso sia attrezzaggio o manutenzione, in questo caso la macchina non può andare in produzione
                                if (GetListCauseIdByGroupName("Attrezzaggio", db, applicationId).Contains(t.CauseId) ||
                                    GetListCauseIdByGroupName("Manutenzione", db, applicationId).Contains(t.CauseId))
                                {
                                    t.LastUpdate = tLog.Date;
                                    db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    if (t.CauseId != productionCause)
                                    {
                                        t.CauseId = productionCause;
                                    }
                                    t.Duration = (decimal)(tLog.Date - t.Start).TotalSeconds;
                                    if (t.CounterEnd > 60000 && tLog.Counter < 2000)
                                    {
                                        t.CounterEnd = 65536 + tLog.Counter;
                                    }
                                    else
                                    {
                                        t.CounterEnd = tLog.Counter;
                                    }
                                    //t.ProgressiveCounterEnd = tLog.Counter;
                                    t.PartialCounting = t.CounterEnd.Value - t.CounterStart;
                                    t.LastUpdate = tLog.Date;
                                    db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            if (!t.Status)
                            {
                                t.LastUpdate = tLog.Date;   //macchina spenta - aggiorno solo lastUpdate
                                db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                            }
                            else
                            {
                                //il contatore non è aumentato, verifico quindi il timeout (solo se è in produzione)
                                if (!string.IsNullOrEmpty(t.CauseId) && t.CauseId == productionCause)
                                {
                                    if (!string.IsNullOrEmpty(t.JobId))
                                    {
                                        //decimal numCicli = db.Jobs.Where(x => x.Id == t.JobId).First().TempoAssegnatoUnitario;
                                        //int numShotTimeout = 2;
                                        //TimeSpan totTimeout = new TimeSpan();
                                        //using (AssetManagement.Models.AssetManagementDbContext assetDB = new AssetManagement.Models.AssetManagementDbContext())
                                        //{
                                        //    try
                                        //    {
                                        //        numShotTimeout = assetDB.Assets.Where(x => x.Id == t.MachineId).First().NumShotTimeout;
                                        //        TimeSpan timeout = TimeSpan.FromSeconds((double)assetDB.Assets.Where(x => x.Id == t.MachineId).First().Timeout);
                                        //        if (numShotTimeout == 0)
                                        //        {
                                        //            numShotTimeout = 2;
                                        //        }
                                        //        if (timeout.TotalSeconds == 0)
                                        //        {
                                        //            timeout = TimeSpan.FromMinutes(2);
                                        //        }
                                        //        totTimeout = TimeSpan.FromSeconds((double)numCicli * numShotTimeout);
                                        //        log.Info("Macchina = " + tLog.Machine + " Timeout = " + timeout);
                                        //        totTimeout = totTimeout.Add(timeout);
                                        //        log.Info("Macchina = " + tLog.Machine + " totale timeout = " + totTimeout);
                                        //    }
                                        //    catch (ArgumentNullException ex)
                                        //    {
                                        //        log.Error(ex.Message);
                                        //    }
                                        //    catch (InvalidOperationException ex)
                                        //    {
                                        //        log.Error(ex.Message);
                                        //    }
                                        //}
                                        //if (totTimeout.TotalSeconds == 0)
                                        //{
                                        //    totTimeout = TimeSpan.FromSeconds((double)numCicli * numShotTimeout).Add(TimeSpan.FromMinutes(2));
                                        //    log.Info("Macchina = " + tLog.Machine + " timeout finale = " + totTimeout);
                                        //}
                                        if (tLog.Date - t.LastUpdate > Timeout(t, tLog, db))
                                        {
                                            log.Info("Superato il timeout=" + Timeout(t, tLog, db));
                                            //la causale è produzione e ha superato il timeout, quindi tolgo la causale
                                            t.End = t.LastUpdate;
                                            t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
                                            t.Open = false;
                                            if (t.CounterEnd > 60000 && tLog.Counter < 2000)
                                            {
                                                t.CounterEnd = 65536 + tLog.Counter;
                                            }
                                            else
                                            {
                                                t.CounterEnd = tLog.Counter;
                                            }
                                            //t.ProgressiveCounterEnd = tLog.Counter;
                                            t.PartialCounting = t.CounterEnd.Value - t.CounterStart;
                                            t.QtyProduced = t.PartialCounting * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
                                            t.LastUpdate = tLog.Date;
                                            db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                                            db.SaveChanges();

                                            Transaction newT = new Transaction();
                                            newT = t;
                                            newT.Verified = false;
                                            newT.Exported = false;
                                            newT.Start = newT.End.Value;
                                            newT.End = null;
                                            newT.Duration = (decimal)(tLog.Date - newT.Start).TotalSeconds;
                                            newT.Open = true;
                                            newT.Status = tLog.Status;
                                            newT.LastUpdate = tLog.Date;
                                            if (newT.CounterEnd.Value >= 65536)
                                            {
                                                newT.CounterStart = tLog.Counter;
                                            }
                                            else
                                            {
                                                newT.CounterStart = newT.CounterEnd.Value;
                                            }
                                            //newT.ProgressiveCounter = newT.ProgressiveCounterEnd.Value;
                                            newT.CounterEnd = tLog.Counter;
                                            newT.PartialCounting = newT.CounterEnd.Value - newT.CounterStart;
                                            newT.QtyProduced = 0;
                                            newT.CauseId = null;
                                            db.Transactions.Add(newT);
                                        }
                                    }
                                    else
                                    {
                                        //non ho un timeout, quindi aggiorno solo il lastupdate
                                        t.LastUpdate = tLog.Date;
                                        db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                                    }
                                }
                                else
                                {
                                    t.LastUpdate = tLog.Date;
                                    db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                                }
                            }
                        }
                    }
                    else
                    {
                        //è cambiato lo stato
                        //chiudo la transazione corrente e ne apro una nuova
                        //se la macchina passa da on a off, aggiungere la causale macchina spenta
                        //se la macchina passa da off a on, e il contatore è aumentato, va messa anche la causale produzione

                        if (!tLog.Status) //se la macchina è spenta
                        {
                            t.End = t.LastUpdate;//t.End = tLog.Date;
                            t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
                            t.Open = false;
                            if (t.CounterEnd > 60000 && tLog.Counter < 2000)
                            {
                                t.CounterEnd = 65536 + tLog.Counter;
                            }
                            else
                            {
                                t.CounterEnd = tLog.Counter;
                            }
                            //t.ProgressiveCounterEnd = tLog.Counter;
                            t.PartialCounting = (t.CounterEnd - t.CounterStart).Value;
                            t.QtyProduced = t.PartialCounting * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
                            t.LastUpdate = tLog.Date;
                            db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();

                            Transaction newT = new Transaction();
                            newT = t;
                            newT.Verified = false;
                            newT.Exported = false;
                            newT.Start = newT.End.Value;//tLog.Date;
                            newT.End = null;
                            newT.Duration = 0;
                            newT.Open = true;
                            newT.Status = tLog.Status;
                            newT.LastUpdate = tLog.Date;
                            if (newT.CounterEnd.Value >= 65536)
                            {
                                newT.CounterStart = tLog.Counter;
                            }
                            else
                            {
                                newT.CounterStart = newT.CounterEnd.Value;
                            }
                            //newT.ProgressiveCounter = tLog.Counter;
                            newT.CounterEnd = tLog.Counter;
                            newT.PartialCounting = 0;
                            newT.QtyProduced = 0;
                            newT.CauseId = machineOffCause; //causale macchina spenta
                            db.Transactions.Add(newT);
                        }
                        else
                        {
                            //macchina in on
                            t.End = t.LastUpdate;//t.End = tLog.Date;
                            t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
                            t.Open = false;
                            //t.ProgressiveCounterEnd = tLog.Counter;
                            t.PartialCounting = (t.CounterEnd - t.CounterStart).Value;
                            t.QtyProduced = t.PartialCounting * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
                            t.LastUpdate = tLog.Date;
                            db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();

                            Transaction newT = new Transaction();
                            newT = t;
                            newT.Verified = false;
                            newT.Exported = false;
                            newT.Start = newT.End.Value;//tLog.Date;
                            newT.End = null;
                            newT.Duration = 0;
                            newT.Open = true;
                            newT.Status = tLog.Status;
                            newT.LastUpdate = tLog.Date;
                            if (newT.CounterEnd.Value >= 65536)
                            {
                                newT.CounterStart = tLog.Counter;
                            }
                            else
                            {
                                newT.CounterStart = newT.CounterEnd.Value;
                            }
                            //newT.ProgressiveCounter = newT.ProgressiveCounterEnd.Value;
                            newT.CounterEnd = tLog.Counter;
                            newT.PartialCounting = (newT.CounterEnd - newT.CounterStart).Value;
                            newT.QtyProduced = 0;
                            //verifico che sia già in produzione
                            if (tLog.Counter > newT.CounterEnd)
                            {
                                newT.CauseId = productionCause; //causale produzione
                            }
                            else
                            {
                                newT.CauseId = null;
                            }
                            db.Transactions.Add(newT);
                        }

                        //db.SaveChanges();
                    }
                    //db.SaveChanges();
                }
                catch (InvalidOperationException)
                {
                    //non c'è transazione aperta
                    t = new Transaction();
                    var tLog = transLogList.Where(x => x.Machine == assetId).First();
                    t.MachineId = assetId;
                    t.Status = tLog.Status;
                    t.Start = tLog.Date;
                    t.LastUpdate = tLog.Date;
                    t.Duration = 0;
                    t.CounterStart = tLog.Counter;
                    t.CounterEnd = tLog.Counter;
                    t.PartialCounting = 0;
                    if (!transLogList.Where(x => x.Machine == assetId).First().Status)
                    {
                        t.CauseId = machineOffCause;
                    }
                    t.Open = true;

                    db.Transactions.Add(t);
                    //db.SaveChanges();
                }
                try
                {
                    int n = db.SaveChanges();
                    log.Info("Salvati nel DB Transaction numero oggetti = " + n + " in data ora: " + DateTime.Now);
                }
                catch (Exception ex)
                {
                    log.Error("Error in save transaction - " + ex.ToString());
                }
            }
        }

        public void UpdateTransactionFlexy(List<string> assetIdList, IEnumerable<ITransactionLog> transLogList)
        {
            foreach (string assetId in assetIdList)
            {
                UpdateTransactionFlexy(assetId, transLogList);
            }
        }

        public void UpdateTransactionFlexy(string assetId, IEnumerable<ITransactionLog> transLogList)
        {
            using (MesDbContext db = new MesDbContext())
            {
                string applicationId = GatewayPullerHelper.GetApplicationId(assetId);
                Transaction t;
                try
                {
                    t = db.Transactions.Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).First();
                    var tLog = transLogList.Where(x => x.Machine == assetId).First();
                    if (t.Status == tLog.Status)
                    {
                        //se lo stato è uguale verifico che il contatore non sia cambiato;
                        if (tLog.Counter > t.CounterEnd)
                        {
                            t.Duration = (decimal)(tLog.Date - t.Start).TotalSeconds;
                            t.CounterEnd = tLog.Counter;
                            t.PartialCounting = t.CounterEnd.Value - t.CounterStart;
                            //t.CauseId = productionCause;
                            t.LastUpdate = tLog.Date;
                            db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            t.Duration = (decimal)(tLog.Date - t.Start).TotalSeconds;
                            t.CounterEnd = tLog.Counter;
                            t.PartialCounting = t.CounterEnd.Value - t.CounterStart;
                            t.LastUpdate = tLog.Date;
                            db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        //è cambiato lo stato, chiudo la transazione corrente e ne apro una nuova
                        t.End = t.LastUpdate;//t.End = tLog.Date;
                        t.Duration = (decimal)(t.End - t.Start).Value.TotalSeconds;
                        t.Open = false;
                        t.CounterEnd = tLog.Counter;
                        t.PartialCounting = (t.CounterEnd - t.CounterStart).Value;
                        //t.QtyProduced = t.PartialCounting * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
                        t.LastUpdate = tLog.Date;
                        db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        Transaction newT = new Transaction();
                        newT = t;
                        newT.Verified = false;
                        newT.Exported = false;
                        newT.Start = newT.End.Value;//tLog.Date;
                        newT.End = null;
                        newT.Duration = 0;
                        newT.Open = true;
                        newT.Status = tLog.Status;
                        newT.LastUpdate = tLog.Date;
                        newT.CounterStart = newT.CounterEnd.Value;
                        newT.CounterEnd = tLog.Counter;
                        newT.PartialCounting = (newT.CounterEnd - newT.CounterStart).Value;
                        newT.QtyProduced = 0;
                        //newT.CauseId = machineOffCause; //causale macchina spenta
                        db.Transactions.Add(newT);
                        //db.SaveChanges();
                    }
                    //db.SaveChanges();
                }
                catch (InvalidOperationException)
                {
                    //non c'è transazione aperta
                    t = new Transaction();
                    var tLog = transLogList.Where(x => x.Machine == assetId).First();
                    t.MachineId = assetId;
                    t.Status = true;//la macchina è sempre accesa   tLog.Status;
                    t.Start = tLog.Date;
                    t.LastUpdate = tLog.Date;
                    t.Duration = 0;
                    t.CounterStart = tLog.Counter;
                    t.CounterEnd = tLog.Counter;
                    t.PartialCounting = 0;
                    //if (!transLogList.Where(x => x.Machine == assetId).First().Status)
                    //{
                    //    t.CauseId = machineOffCause;
                    //}
                    t.Open = true;

                    db.Transactions.Add(t);
                    //db.SaveChanges();
                }
            }
        }

        public void UpdateTransactionFanuc(IEnumerable<BaseTransactionLog> dataSource)
        {
            List<string> machineIdList = dataSource.Select(x => x.Machine).Distinct().ToList();
            foreach (string assetId in machineIdList)
            {
                List<BaseTransactionLog> tList = dataSource.Where(x => x.Machine == assetId).OrderBy(x => x.Date).ToList();
                using (MesDbContext db = new MesDbContext())
                {
                    string applicationId = GatewayPullerHelper.GetApplicationId(assetId);
                    var productionCode = ((int)MesEnum.Cause.Production).ToString();
                    var alarmCode = ((int)MesEnum.Cause.Alarm).ToString();
                    var machineOffCode = ((int)MesEnum.Cause.MachineOff).ToString();
                    string productionCause = db.Causes.Where(x => x.Code == productionCode && x.ApplicationId == applicationId).First().Id;
                    string alarmCause = db.Causes.Where(x => x.Code == alarmCode && x.ApplicationId == applicationId).First().Id;
                    string machineOffCause = db.Causes.Where(x => x.Code == machineOffCode && x.ApplicationId == applicationId).First().Id;

                    Transaction tLast = db.Transactions.Include("Cause").Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).FirstOrDefault();
                    tLast = db.Transactions.Include("Cause").Where(x => x.Open && x.MachineId == assetId).OrderByDescending(x => x.Start).FirstOrDefault();
                    if (tLast == null)
                    {
                        tLast = new Transaction();
                        tLast.MachineId = assetId;
                        tLast.Status = false;//tLog.Status;
                        tLast.Start = DateTime.Now;// tLog.Date;
                        tLast.LastUpdate = DateTime.Now;// tLog.Date;
                        tLast.Duration = 0;
                        tLast.CounterStart = 0;//tLog.Counter;
                        tLast.CounterEnd = 0;//tLog.Counter;
                        tLast.PartialCounting = 0;
                        tLast.CauseId = null;//tLog.First().StatusCode;
                        tLast.Open = true;

                        db.Transactions.Add(tLast);
                        db.SaveChanges();
                    }
                    foreach (BaseTransactionLog tLog in tList)
                    {

                        //var tLog = transLogList.Where(x => x.Machine == assetId).First();
                        switch (tLog.StatusCode)
                        {
                            case "2":
                                //Production
                                tLog.StatusCode = productionCause;
                                break;
                            case "5":
                                //Alarm
                                tLog.StatusCode = alarmCause;
                                break;
                            case "0":
                                tLog.StatusCode = machineOffCause;
                                break;
                            default:
                                tLog.StatusCode = null;
                                break;
                        }

                        //if (tLast.Status == tLog.Status)
                        if (tLast.Cause?.Id == tLog.StatusCode)
                        {
                            //se lo stato è uguale verifico che il contatore non sia cambiato;
                            if (tLog.Counter > tLast.CounterEnd)
                            {
                                tLast.Duration = (decimal)(tLog.Date - tLast.Start).TotalSeconds;
                                tLast.CounterEnd = tLog.Counter;
                                tLast.PartialCounting = tLast.CounterEnd.Value - tLast.CounterStart;
                                tLast.CauseId = tLog.StatusCode;
                                tLast.LastUpdate = tLog.Date;
                                db.Entry(tLast).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                //il contatore non è aumentato, verifico quindi il timeout (solo se è in produzione)
                                if (!string.IsNullOrEmpty(tLast.CauseId) && tLast.CauseId == productionCause)
                                {
                                    if (!string.IsNullOrEmpty(tLast.JobId))
                                    {
                                        if (tLog.Date - tLast.LastUpdate > Timeout(tLast, tLog, db))
                                        {
                                            log.Info("Superato il timeout=" + Timeout(tLast, tLog, db));
                                            //la causale è produzione e ha superato il timeout, quindi tolgo la causale
                                            tLast.End = tLast.LastUpdate;
                                            tLast.Duration = (decimal)(tLast.End - tLast.Start).Value.TotalSeconds;
                                            tLast.Open = false;
                                            //if (tLast.ProgressiveCounterEnd > 60000 && tLog.Counter < 2000)
                                            //{
                                            //    tLast.ProgressiveCounterEnd = 65536 + tLog.Counter;
                                            //}
                                            //else
                                            //{
                                            tLast.CounterEnd = tLog.Counter;
                                            //}
                                            //t.ProgressiveCounterEnd = tLog.Counter;
                                            tLast.PartialCounting = tLast.CounterEnd.Value - tLast.CounterStart;
                                            tLast.QtyProduced = tLast.PartialCounting * (tLast.CavityMoldNum > 0 ? tLast.CavityMoldNum : 1);
                                            tLast.LastUpdate = tLog.Date;
                                            db.Entry(tLast).State = System.Data.Entity.EntityState.Modified;
                                            db.SaveChanges();

                                            Transaction newT = new Transaction();
                                            newT = tLast;
                                            newT.Verified = false;
                                            newT.Exported = false;
                                            newT.Start = newT.End.Value;
                                            newT.End = null;
                                            newT.Duration = (decimal)(tLog.Date - newT.Start).TotalSeconds;
                                            newT.Open = true;
                                            newT.Status = tLog.Status;
                                            newT.LastUpdate = tLog.Date;
                                            newT.CounterStart = newT.CounterEnd.Value;
                                            newT.CounterEnd = tLog.Counter;
                                            newT.PartialCounting = newT.CounterEnd.Value - newT.CounterStart;
                                            newT.QtyProduced = 0;
                                            newT.CauseId = null;
                                            db.Transactions.Add(newT);
                                        }
                                    }
                                    else
                                    {
                                        //non ho un timeout, quindi aggiorno solo il lastupdate
                                        tLast.LastUpdate = tLog.Date;
                                        db.Entry(tLast).State = System.Data.Entity.EntityState.Modified;
                                    }
                                }
                                else
                                {
                                    tLast.LastUpdate = tLog.Date;
                                    db.Entry(tLast).State = System.Data.Entity.EntityState.Modified;
                                }

                                //tLast.Duration = (decimal)(tLog.Date - tLast.Start).TotalSeconds;
                                //    tLast.ProgressiveCounterEnd = tLog.Counter;
                                //    tLast.PartialCounting = tLast.ProgressiveCounterEnd.Value - tLast.ProgressiveCounter;
                                //    tLast.LastUpdate = tLog.Date;
                                //    db.Entry(tLast).State = System.Data.Entity.EntityState.Modified;
                                //    db.SaveChanges();
                            }
                        }
                        else
                        {
                            //è cambiato lo stato, verifico che lo stato precedente fosse prodzuione e quello attuale sia un fermo
                            if (tLast.CauseId == productionCause && tLog.StatusCode == null)
                            {
                                //in questo caso aspetto un timeout prima di accettare un fermo
                                if (tLog.Date - tLast.LastUpdate > Timeout(tLast, tLog, db))
                                {
                                    //è cambiato lo stato, chiudo la transazione corrente e ne apro una nuova
                                    tLast.End = tLast.LastUpdate;//t.End = tLog.Date;
                                    tLast.Duration = (decimal)(tLast.End - tLast.Start).Value.TotalSeconds;
                                    tLast.Open = false;
                                    tLast.CounterEnd = tLog.Counter;
                                    tLast.PartialCounting = (tLast.CounterEnd - tLast.CounterStart).Value;
                                    //t.QtyProduced = t.PartialCounting * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
                                    tLast.LastUpdate = tLog.Date;
                                    db.Entry(tLast).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();

                                    Transaction newT = new Transaction();
                                    newT = tLast;
                                    newT.Verified = false;
                                    newT.Exported = false;
                                    newT.Start = newT.End.Value;//tLog.Date;
                                    newT.End = null;
                                    newT.Duration = 0;
                                    newT.Open = true;
                                    newT.IsOrderClosed = false;
                                    newT.Status = tLog.Status;
                                    newT.LastUpdate = tLog.Date;
                                    newT.CounterStart = newT.CounterEnd.Value;
                                    newT.CounterEnd = tLog.Counter;
                                    newT.PartialCounting = (newT.CounterEnd - newT.CounterStart).Value;
                                    newT.QtyProduced = 0;
                                    newT.CauseId = tLog.StatusCode;//machineOffCause; //causale macchina spenta
                                    db.Transactions.Add(newT);
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                //è cambiato lo stato, chiudo la transazione corrente e ne apro una nuova
                                tLast.End = tLast.LastUpdate;//t.End = tLog.Date;
                                tLast.Duration = (decimal)(tLast.End - tLast.Start).Value.TotalSeconds;
                                tLast.Open = false;
                                tLast.CounterEnd = tLog.Counter;
                                tLast.PartialCounting = (tLast.CounterEnd - tLast.CounterStart).Value;
                                //t.QtyProduced = t.PartialCounting * (t.CavityMoldNum > 0 ? t.CavityMoldNum : 1);
                                tLast.LastUpdate = tLog.Date;
                                db.Entry(tLast).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();

                                Transaction newT = new Transaction();
                                newT = tLast;
                                newT.Verified = false;
                                newT.Exported = false;
                                newT.Start = newT.End.Value;//tLog.Date;
                                newT.End = null;
                                newT.Duration = 0;
                                newT.Open = true;
                                newT.IsOrderClosed = false;
                                newT.Status = tLog.Status;
                                newT.LastUpdate = tLog.Date;
                                newT.CounterStart = newT.CounterEnd.Value;
                                newT.CounterEnd = tLog.Counter;
                                newT.PartialCounting = (newT.CounterEnd - newT.CounterStart).Value;
                                newT.QtyProduced = 0;
                                newT.CauseId = tLog.StatusCode;//machineOffCause; //causale macchina spenta
                                db.Transactions.Add(newT);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
        }

        public void UpdateTransactionsTMold(TMoldMessage message)
        {
            try
            {
                EverynetModule.Repositories.DeviceRepository devRepo = new EverynetModule.Repositories.DeviceRepository();
                string moldId = devRepo.GetMoldId(message.Device);
                AssetManagement.Repositories.MoldRepository moldRepos = new AssetManagement.Repositories.MoldRepository();
                int moldStop = moldRepos.GetStopGone(moldId) + moldRepos.GetStopReturn(moldId) + 1;
                //string moldId = moldRepos.GetMoldByDevice(message.Device);

                if (!string.IsNullOrEmpty(moldId))
                {
                    MES.Repositories.TransactionMoldRepository tRepos = new MES.Repositories.TransactionMoldRepository();
                    MES.Services.TransactionMoldService tService = new MES.Services.TransactionMoldService();
                    MES.Models.TransactionMold lastT = tService.GetLastTransactionOpen(moldId);
                    if (lastT != null)
                    {
                        ////controllo l'ultima transazione: se è in produzione aumento il contatore, o verifico timeout
                        string applicationId = GatewayPullerHelper.GetApplicationId(moldId, true);
                        string productionCause;
                        //var machineOffCode = ((int)MesEnum.Cause.MachineOff).ToString();
                        using (var db = new MesDbContext())
                        {
                            var productionCode = ((int)MesEnum.Cause.Production).ToString();
                            productionCause = db.Causes.Where(x => x.Code == productionCode && x.ApplicationId == applicationId).First().Id;
                        }

                        if (lastT.CounterEnd != message.Counter)
                        {
                            ////il contatore è cambiato
                            bool insert = false;
                            if (lastT.CauseId != productionCause)
                            {
                                ////non era in produzione, cambio quindi la causale
                                lastT.Open = false;
                                lastT.End = message.Time;
                                lastT.Duration = (decimal)(lastT.End.Value - lastT.Start).TotalSeconds;
                                lastT.LastUpdate = DateTime.Now;
                                tRepos.Update(lastT);
                                tRepos.SaveChanges();

                                ////apro nuova transazione
                                insert = true;
                                lastT.CauseId = productionCause;
                                lastT.CounterStart = lastT.CounterEnd.Value;
                                //lastT.CounterEnd = message.Counter;
                                lastT.PartialCounting = lastT.CounterEnd.Value - lastT.CounterStart;
                                lastT.Open = true;
                                lastT.Start = lastT.End.Value;
                                lastT.End = null;
                                lastT.Duration = 0;
                                lastT.Verified = false;
                            }

                            if (message.Counter < lastT.CounterEnd)
                            {
                                //il contatore del sensore si è azzerato, devo quindi tenere il progressivo
                                lastT.CounterEnd = message.Counter;
                                lastT.PartialCounting = message.Counter / moldStop;
                                lastT.ProgressiveCounter += lastT.PartialCounting;
                            }
                            else
                            {
                                lastT.CounterEnd = message.Counter;
                                lastT.PartialCounting = Math.Round((lastT.CounterEnd.Value - lastT.CounterStart) / moldStop, 0);
                                if (lastT.PartialCounting < 0)
                                {
                                    //il contatore si era azzerato, tengo buono solo il progressivo finale
                                    lastT.PartialCounting = Math.Round(lastT.CounterEnd.Value / moldStop, 0);
                                }
                            }

                            lastT.LastUpdate = DateTime.Now;
                            if (insert)
                            {
                                tRepos.Insert(lastT);
                            }
                            else
                            {
                                tRepos.Update(lastT);
                            }
                            tRepos.SaveChanges();
                        }
                        else
                        {
                            //il contatore non è cambiato, verificare il timeout per decidere se togliere la causale di produzione
                            if (lastT.CauseId == productionCause && message.Time - lastT.LastUpdate > Timeout(lastT))
                            {
                                //timeout superato -> tolgo causale di produzione
                                lastT.Open = false;
                                lastT.End = message.Time;
                                lastT.Duration = (decimal)(lastT.End.Value - lastT.Start).TotalSeconds;
                                lastT.PartialCounting = Math.Round((lastT.CounterEnd.Value - lastT.CounterStart) / moldStop, 0);
                                lastT.ProgressiveCounter += lastT.PartialCounting;
                                //lastT.QtyProduced = (lastT.PartialCounting - lastT.BlankShot) * lastT.CavityMoldNum;
                                lastT.LastUpdate = DateTime.Now;
                                tRepos.Update(lastT);
                                tRepos.SaveChanges();

                                ////apro nuova transazione
                                lastT.CauseId = null;
                                lastT.CounterStart = lastT.CounterEnd.Value;
                                lastT.PartialCounting = lastT.CounterEnd.Value - lastT.CounterStart;
                                lastT.Open = true;
                                lastT.Start = lastT.End.Value;
                                lastT.End = null;
                                lastT.Duration = 0;
                                lastT.Verified = false;
                                lastT.LastUpdate = DateTime.Now;
                                tRepos.Insert(lastT);
                                tRepos.SaveChanges();
                            }
                            else
                            {
                                if (lastT.CauseId != productionCause)
                                {
                                    lastT.LastUpdate = DateTime.Now;
                                    tRepos.Update(lastT);
                                    tRepos.SaveChanges();
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(lastT.MachineId))
                        {
                            UpdateMachineTMold(lastT.MachineId, message);
                        }
                    }
                    else
                    {
                        //non esistono transazioni, quindi creo nuova
                        lastT = new MES.Models.TransactionMold();
                        lastT.MoldId = moldId;
                        lastT.Start = message.Time;
                        lastT.Open = true;
                        lastT.LastUpdate = DateTime.Now;
                        lastT.CounterStart = message.Counter;
                        lastT.CounterEnd = message.Counter;
                        lastT.PartialCounting = lastT.CounterEnd.Value - lastT.CounterStart;
                        lastT.ProgressiveCounter = 0;
                        tRepos.Insert(lastT);
                        tRepos.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }
        }

        public void UpdateMachineTMold(string assetId, TMoldMessage message)
        {
            try
            {
                TransactionRepository tRepos = new TransactionRepository();
                TransactionService tService = new TransactionService();
                Transaction lastT = tService.GetLastTransactionOpen(assetId);
                AssetManagement.Repositories.MoldRepository moldRepos = new AssetManagement.Repositories.MoldRepository();
                int moldStop = moldRepos.GetStopGone(lastT?.MoldId) + moldRepos.GetStopReturn(lastT?.MoldId) + 1;
                if (lastT != null)
                {
                    ////controllo l'ultima transazione: se è in produzione aumento il contatore, o verifico timeout
                    string applicationId = GatewayPullerHelper.GetApplicationId(assetId);
                    string productionCause;
                    //var machineOffCode = ((int)MesEnum.Cause.MachineOff).ToString();
                    using (var db = new MesDbContext())
                    {
                        var productionCode = ((int)MesEnum.Cause.Production).ToString();
                        productionCause = db.Causes.Where(x => x.Code == productionCode && x.ApplicationId == applicationId).First().Id;
                    }

                    if (lastT.CounterEnd != message.Counter)
                    {
                        ////il contatore è cambiato
                        bool insert = false;
                        if (lastT.CauseId != productionCause)
                        {
                            ////non era in produzione, cambio quindi la causale
                            lastT.Open = false;
                            lastT.End = message.Time;
                            lastT.Duration = (decimal)(lastT.End.Value - lastT.Start).TotalSeconds;
                            lastT.LastUpdate = DateTime.Now;
                            tRepos.Update(lastT);
                            tRepos.SaveChanges();

                            ////apro nuova transazione
                            insert = true;
                            lastT.CauseId = productionCause;
                            lastT.CounterStart = lastT.CounterEnd.Value;
                            //lastT.CounterEnd = message.Counter;
                            lastT.PartialCounting = lastT.CounterEnd.Value - lastT.CounterStart;
                            lastT.Open = true;
                            lastT.Start = lastT.End.Value;
                            lastT.End = null;
                            lastT.Duration = 0;
                            lastT.Verified = false;
                            lastT.Exported = false;
                            lastT.IsOrderClosed = false;
                        }

                        if (message.Counter < lastT.CounterEnd)
                        {
                            //il contatore del sensore si è azzerato, devo quindi tenere il progressivo
                            lastT.CounterEnd = message.Counter;
                            lastT.PartialCounting = message.Counter / moldStop;
                            lastT.ProgressiveCounter += lastT.PartialCounting;
                        }
                        else
                        {
                            lastT.CounterEnd = message.Counter;
                            lastT.PartialCounting = Math.Round((lastT.CounterEnd.Value - lastT.CounterStart) / moldStop, 0);
                            if (lastT.PartialCounting < 0)
                            {
                                //il contatore si era azzerato, tengo buono solo il progressivo finale
                                lastT.PartialCounting = Math.Round(lastT.CounterEnd.Value / moldStop, 0);
                            }
                        }

                        lastT.LastUpdate = DateTime.Now;
                        if (insert)
                        {
                            tRepos.Insert(lastT);
                        }
                        else
                        {
                            tRepos.Update(lastT);
                        }
                        tRepos.SaveChanges();
                    }
                    else
                    {
                        //il contatore non è cambiato, verificare il timeout per decidere se togliere la causale di produzione
                        if (lastT.CauseId == productionCause && message.Time - lastT.LastUpdate > Timeout(lastT))
                        {
                            //timeout superato -> tolgo causale di produzione
                            lastT.Open = false;
                            lastT.End = message.Time;
                            lastT.Duration = (decimal)(lastT.End.Value - lastT.Start).TotalSeconds;
                            lastT.PartialCounting = Math.Round((lastT.CounterEnd.Value - lastT.CounterStart) / moldStop, 0);
                            lastT.ProgressiveCounter += lastT.PartialCounting;
                            //lastT.QtyProduced = (lastT.PartialCounting - lastT.BlankShot) * lastT.CavityMoldNum;
                            lastT.LastUpdate = DateTime.Now;
                            tRepos.Update(lastT);
                            tRepos.SaveChanges();

                            ////apro nuova transazione
                            lastT.CauseId = null;
                            lastT.CounterStart = lastT.CounterEnd.Value;
                            lastT.PartialCounting = lastT.CounterEnd.Value - lastT.CounterStart;
                            lastT.Open = true;
                            lastT.Start = lastT.End.Value;
                            lastT.End = null;
                            lastT.Duration = 0;
                            lastT.Verified = false;
                            lastT.Exported = false;
                            lastT.IsOrderClosed = false;
                            lastT.LastUpdate = DateTime.Now;
                            tRepos.Insert(lastT);
                            tRepos.SaveChanges();
                        }
                        else
                        {
                            if (lastT.CauseId != productionCause)
                            {
                                lastT.LastUpdate = DateTime.Now;
                                tRepos.Update(lastT);
                                tRepos.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    //non c'è transazione aperta
                    lastT = new Transaction();
                    lastT.MachineId = assetId;
                    lastT.Start = message.Time;
                    lastT.Open = true;
                    lastT.Verified = false;
                    lastT.Exported = false;
                    lastT.IsOrderClosed = false;
                    lastT.LastUpdate = DateTime.Now;
                    lastT.Duration = 0;
                    lastT.CounterStart = message.Counter;
                    lastT.CounterEnd = message.Counter;
                    lastT.PartialCounting = lastT.CounterEnd.Value - lastT.CounterStart;
                    tRepos.Insert(lastT);
                    tRepos.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }
        }

        public void UpdateMachineTMold(TMoldMessage message)
        {
            EverynetModule.Repositories.DeviceRepository devRepo = new EverynetModule.Repositories.DeviceRepository();
            string assetId = devRepo.GetMoldId(message.Device); //TODO: cambiare funzione per ottenere asset e non mold
            //AssetManagement.Repositories.AssetRepository moldRepos = new AssetManagement.Repositories.AssetRepository();
            //int moldStop = moldRepos.GetStopGone(moldId) + moldRepos.GetStopReturn(moldId) + 1;
            UpdateMachineTMold(assetId, message);
        }

        private TimeSpan Timeout(Transaction t, ITransactionLog tLog, MesDbContext db)
        {
            TimeSpan totTimeout = new TimeSpan();
            int numShotTimeout = 2;
            using (AssetManagement.Models.AssetManagementDbContext assetDB = new AssetManagement.Models.AssetManagementDbContext())
            {
                numShotTimeout = assetDB.Assets.Where(x => x.Id == t.MachineId).First().NumShotTimeout;
                TimeSpan timeout = TimeSpan.FromSeconds((double)assetDB.Assets.Where(x => x.Id == t.MachineId).First().Timeout);
                if (numShotTimeout == 0)
                {
                    numShotTimeout = 2;
                }
                if (timeout.TotalSeconds == 0)
                {
                    timeout = TimeSpan.FromMinutes(2);
                }
                totTimeout = timeout;
            }

            if (!string.IsNullOrEmpty(t.JobId))
            {
                decimal numCicli = db.Jobs.Where(x => x.Id == t.JobId).First().CycleTime;
                if (numCicli == 0)
                {
                    try
                    {
                        numCicli = 3600 / db.Jobs.Where(x => x.Id == t.JobId).First().StandardRate;
                    }
                    catch (Exception)
                    {
                        numCicli = 1;
                    }
                }
                //int numShotTimeout = 2;
                //TimeSpan totTimeout = new TimeSpan();
                totTimeout = totTimeout.Add(TimeSpan.FromSeconds((double)numCicli * numShotTimeout));
                //using (AssetManagement.Models.AssetManagementDbContext assetDB = new AssetManagement.Models.AssetManagementDbContext())
                //{
                //    try
                //    {
                //        numShotTimeout = assetDB.Assets.Where(x => x.Id == t.MachineId).First().NumShotTimeout;
                //        TimeSpan timeout = TimeSpan.FromSeconds((double)assetDB.Assets.Where(x => x.Id == t.MachineId).First().Timeout);
                //        if (numShotTimeout == 0)
                //        {
                //            numShotTimeout = 2;
                //        }
                //        if (timeout.TotalSeconds == 0)
                //        {
                //            timeout = TimeSpan.FromMinutes(2);
                //        }
                //        totTimeout = totTimeout.Add(TimeSpan.FromSeconds((double)numCicli * numShotTimeout));
                //        //log.Info("Macchina = " + tLog.Machine + " Timeout = " + timeout);
                //        //totTimeout = totTimeout.Add(timeout);
                //        //log.Info("Macchina = " + tLog.Machine + " totale timeout = " + totTimeout);
                //    }
                //    catch (ArgumentNullException ex)
                //    {
                //        log.Error(ex.Message);
                //    }
                //    catch (InvalidOperationException ex)
                //    {
                //        log.Error(ex.Message);
                //    }
                //}
                if (totTimeout.TotalSeconds == 0)
                {
                    totTimeout = TimeSpan.FromSeconds((double)numCicli * numShotTimeout).Add(TimeSpan.FromMinutes(2));
                    log.Info("Macchina = " + tLog.Machine + " timeout finale = " + totTimeout);
                }
                return totTimeout;
            }
            //else
            //{
            //    return TimeSpan.FromMinutes(2);
            //}
            return totTimeout;
        }

        public List<string> GetListCauseIdByGroupName(string causeGroup, MesDbContext db, string applicationId)
        {
            var group = db.CauseTypes.Where(x => x.Description == causeGroup && x.ApplicationId == applicationId).FirstOrDefault();
            if (group != null)
            {
                return db.Causes.Where(x => x.CauseTypeId == group.Id && x.ApplicationId == applicationId).Select(x => x.Id).ToList();
            }
            else
            {
                return null;
            }
        }

        private TimeSpan Timeout(TransactionMold t)
        {
            try
            {
                TimeSpan totTimeout = new TimeSpan();
                int numShotTimeout = 2;
                TimeSpan timeout = TimeSpan.FromMinutes(2);
                using (AssetManagement.Models.AssetManagementDbContext assetDB = new AssetManagement.Models.AssetManagementDbContext())
                {
                    var result = assetDB.Assets.Where(x => x.Id == t.MachineId).Select(x => new { x.NumShotTimeout, x.Timeout }).FirstOrDefault();
                    if (result != null)
                    {
                        numShotTimeout = result.NumShotTimeout;
                        timeout = TimeSpan.FromSeconds((double)result.Timeout);
                    }
                    //TimeSpan timeout = TimeSpan.FromSeconds((double)assetDB.Assets.Where(x => x.Id == t.MachineId).First().Timeout);
                    if (numShotTimeout == 0)
                    {
                        numShotTimeout = 2;
                    }
                    if (timeout.TotalSeconds == 0)
                    {
                        timeout = TimeSpan.FromMinutes(2);
                    }
                    totTimeout = timeout;
                }

                if (!string.IsNullOrEmpty(t.JobId))
                {
                    JobRepository repos = new JobRepository();
                    decimal numCicli = repos.GetCycleTime(t.JobId); //db.Jobs.Where(x => x.Id == t.JobId).First().CycleTime;
                    if (numCicli == 0)
                    {
                        try
                        {
                            numCicli = 3600 / repos.GetStandardRate(t.JobId); //db.Jobs.Where(x => x.Id == t.JobId).First().StandardRate;
                        }
                        catch (Exception)
                        {
                            numCicli = 1;
                        }
                    }

                    totTimeout = totTimeout.Add(TimeSpan.FromSeconds((double)numCicli * numShotTimeout));

                    if (totTimeout.TotalSeconds == 0)
                    {
                        totTimeout = TimeSpan.FromSeconds((double)numCicli * numShotTimeout).Add(TimeSpan.FromMinutes(2));
                    }
                    return totTimeout;
                }
                //else
                //{
                //    return TimeSpan.FromMinutes(2);
                //}
                return totTimeout;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return TimeSpan.FromMinutes(2);
            }
        }

        private TimeSpan Timeout(Transaction t)
        {
            try
            {
                TimeSpan totTimeout = new TimeSpan();
                int numShotTimeout = 2;
                TimeSpan timeout = TimeSpan.FromMinutes(2);
                using (AssetManagement.Models.AssetManagementDbContext assetDB = new AssetManagement.Models.AssetManagementDbContext())
                {
                    var result = assetDB.Assets.Where(x => x.Id == t.MachineId).Select(x => new { x.NumShotTimeout, x.Timeout }).FirstOrDefault();
                    if (result != null)
                    {
                        numShotTimeout = result.NumShotTimeout;
                        timeout = TimeSpan.FromSeconds((double)result.Timeout);
                    }
                    //TimeSpan timeout = TimeSpan.FromSeconds((double)assetDB.Assets.Where(x => x.Id == t.MachineId).First().Timeout);
                    if (numShotTimeout == 0)
                    {
                        numShotTimeout = 2;
                    }
                    if (timeout.TotalSeconds == 0)
                    {
                        timeout = TimeSpan.FromMinutes(2);
                    }
                    totTimeout = timeout;
                }

                if (!string.IsNullOrEmpty(t.JobId))
                {
                    JobRepository repos = new JobRepository();
                    decimal numCicli = repos.GetCycleTime(t.JobId); //db.Jobs.Where(x => x.Id == t.JobId).First().CycleTime;
                    if (numCicli == 0)
                    {
                        try
                        {
                            numCicli = 3600 / repos.GetStandardRate(t.JobId); //db.Jobs.Where(x => x.Id == t.JobId).First().StandardRate;
                        }
                        catch (Exception)
                        {
                            numCicli = 1;
                        }
                    }

                    totTimeout = totTimeout.Add(TimeSpan.FromSeconds((double)numCicli * numShotTimeout));

                    if (totTimeout.TotalSeconds == 0)
                    {
                        totTimeout = TimeSpan.FromSeconds((double)numCicli * numShotTimeout).Add(TimeSpan.FromMinutes(2));
                    }
                    return totTimeout;
                }
                //else
                //{
                //    return TimeSpan.FromMinutes(2);
                //}
                return totTimeout;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return TimeSpan.FromMinutes(2);
            }
        }
    }
}