﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class JobProductionWaste
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        [Required]
        public string JobId { get; set; }
        
        [Required]
        public string AssetId { get; set; }

        public string MoldId { get; set; }

        [Required]
        public string OperatorId { get; set; }

        public int? ArticleId { get; set; }

        [Required]
        public string CauseId { get; set; }

        /// <summary>
        /// Colpi prodotti totale
        /// </summary>
        public decimal QtyShotProduced { get; set; }
        /// <summary>
        /// Pezzi prodotti totale
        /// </summary>
        public decimal QtyProduced { get; set; }

        /// <summary>
        /// Quantità scarti
        /// </summary>
        [Required]
        public decimal QtyProductionWaste { get; set; }

        /// <summary>
        /// Pezzi prodotti buoni (QtyProduced - QtyProducedWaste)
        /// </summary>
        public decimal QtyOk { get; set; }

        public DateTime Date { get; set; }

        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Article Article { get; set; }

        [ForeignKey("OperatorId")]
        public virtual Operator Operator { get; set; }

        [ForeignKey("CauseId")]
        public virtual Cause Cause { get; set; }

        /// <summary>
        /// Transazione di riferimento
        /// </summary>
        public int TransactionId { get; set; }
        
        /// <summary>
        /// True se è un controllo qualità
        /// </summary>
        public bool Quality { get; set; }
    }
}