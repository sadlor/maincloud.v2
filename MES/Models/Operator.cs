﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class Operator
    {
        public Operator()
        {
            Id = Guid.NewGuid().ToString();
        }

        public Operator(string userName) : this()
        {
            UserName = userName;
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        /// <summary>
        /// Matricola dipendente
        /// </summary>
        [Required]
        [MaxLength(10)]
        //[Index("UniqueOperatorIndex", 1, IsUnique = true)]
        public string Code { get; set; }

        /// <summary>
        /// Human Resource
        /// </summary>
        [MaxLength(10)]
        public string HR { get; set; }

        [MaxLength(10)]
        public string RegistrationId { get; set; } //Matricola (solo numerica)   TODO: sostituire dappertutto con Code

        [Required]
        public string UserName { get; set; }
        public string PIN { get; set; }   //solo numerico

        /// <summary>
        /// Per il login operatore, solo numerico
        /// </summary>
        [MinLength(3), MaxLength(10)]
        //[Index("SearchOperatorIndex", 1, IsUnique = true)]
        public string Badge { get; set; }

        public string Qualification { get; set; }

        /// <summary>
        /// Indica quali operatori sono attivi e quali sono invece obsoleti
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        ///     Navigation property for users in the role
        /// </summary>
        public virtual ICollection<MesOperatorRole> Roles { get; } = new List<MesOperatorRole>();

        [MaxLength(128)]
        //[Index("SearchOperatorIndex", 2, IsUnique = true)]
        public string ApplicationId { get; set; }
    }
}