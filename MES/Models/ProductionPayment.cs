﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class ProductionPayment
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        public string MachineId { get; set; }
        public string MachineCode { get; set; }              //codice macchina
        public string MoldId { get; set; }
        public string MoldName { get; set; }                 //nome stampo
        public string JobId { get; set; }
        public string OrderCode { get; set; }                //codice commessa
        public string OperatorId { get; set; }
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }

        public decimal QtyOrdered { get; set; }              //quantità ordinata
        public decimal QtyShotProduced { get; set; }         //colpi prodotti totale per la commessa
        public decimal QtyProduced { get; set; }             //pezzi prodotti totale per la commessa
        public decimal QtyOperatorShotProduced { get; set; } //colpi prodotti totale nel turno operatore
        public decimal QtyOperatorProduced { get; set; }     //pezzi prodotti totale nel turno operatore
        public decimal ResidualQty { get; set; }             //quantità residua
        public decimal QtyProductionWaste { get; set; }      //quantità scarti operatore
        public decimal QtyOK { get; set; }                   //quantità pezzi OK
        public decimal Duration { get; set; }                //Duata transazione
        
        public bool IsOrderClose { get; set; }               //commessa chiusa
        public bool Verified { get; set; }                   //convalida della transazione
        public DateTime? Sent { get; set; }                   //dataora di invio al gestionale

        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        [ForeignKey("OperatorId")]
        public virtual Operator Operator { get; set; }
    }
}