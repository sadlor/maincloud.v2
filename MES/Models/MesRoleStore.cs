﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class MesRoleStore<TRole> : RoleStore<TRole>
        where TRole : MesRole, new()
    {
        //
        // Summary:
        //     Constructor
        public MesRoleStore(string applicationId) : base()
        {
            ApplicationId = applicationId;
        }

        //
        // Summary:
        //     Constructor
        //
        // Parameters:
        //   context:
        public MesRoleStore(string applicationId, DbContext context) : base(context)
        {
            ApplicationId = applicationId;
        }

        public string ApplicationId { get; set; }

        public override Task CreateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            role.ApplicationId = ApplicationId;
            return base.CreateAsync(role);
        }

        public override Task UpdateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            role.ApplicationId = ApplicationId;
            return base.UpdateAsync(role);
        }
    }
}