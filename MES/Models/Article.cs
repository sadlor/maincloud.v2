﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    [JsonObject(IsReference = true)]
    public class Article   //Articolo
    {
        public Article()
        {
            Order = new List<Order>();
        }

        [Key]
        [Required]
        //[MaxLength(128)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        //public string RawMaterialCode { get; set; }
        //public string RawMaterialDescription { get; set; }
        /// <summary>
        /// Id materiale del cliente
        /// </summary>
        public int? ClientRawMaterialId { get; set; }
        public decimal Quantity { get; set; }
        /// <summary>
        /// Unit of measure
        /// </summary>
        public string UM { get; set; }
        public string MeasureUnitId { get; set; }
        public string ArticleClass { get; set; }
        public string ArticleClassDescription { get; set; }
        public string TechnicalNotes { get; set; }
        public string QualityNotes { get; set; }
        //public decimal? SpecificWeight { get; set; }
        //public decimal? PesoLordo { get; set; }
        //public decimal? PesoNetto { get; set; } //tradurre
        public string PathImg { get; set; }

        //public decimal Timeout { get; set; } //secondi

        [ForeignKey("MeasureUnitId")]
        public virtual MeasureUnit MeasureUnit { get; set; }

        public string JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        public int? OrderId { get; set; }

        [InverseProperty("Article")]
        public virtual List<Order> Order { get; set; }

        //public virtual List<BOM> BOM { get; set; }
        //public virtual List<BOM> Component { get; set; }

        //[Index("UniqueClientApplicationIndex", 1, IsUnique = true)]
        public int? ClientId { get; set; }

        //[Index("UniqueClientApplicationIndex", 2, IsUnique = true)]
        [MaxLength(128)]
        public string ApplicationId { get; set; }

        //[InverseProperty("Article")]
        //public virtual List<ArticleBatch> ArticleBatch { get; set; }

        //[InverseProperty("Article")]
        //public virtual List<JobRawMaterial> JobRawMaterial { get; set; }
    }
}