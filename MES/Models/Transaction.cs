﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class Transaction
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        public string CauseId { get; set; }
        [ForeignKey("CauseId")]
        public virtual Cause Cause { get; set; }

        public int? IdArticle { get; set; }
        [ForeignKey("IdArticle")]
        public virtual Article Article { get; set; }

        public string JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        public string MachineId { get; set; }
        public string OperatorId { get; set; }
        [ForeignKey("OperatorId")]
        public virtual Operator Operator { get; set; }

        public string EquipmentId { get; set; }

        /// <summary>
        /// Id Stampo
        /// </summary>
        public string MoldId { get; set; }

        public bool Status { get; set; }

        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
        public decimal Duration { get; set; }

        public decimal CounterStart { get; set; }
        public decimal? CounterEnd { get; set; }
        
        /// <summary>
        /// Progressivo generale
        /// </summary>
        public decimal ProgressiveCounter { get; set; }
        /// <summary>
        /// Colpi contati
        /// </summary>
        public decimal PartialCounting { get; set; }
        /// <summary>
        /// Colpi a vuoto (da sottrarre al PartialCounting)
        /// </summary>
        public decimal BlankShot { get; set; }

        /// <summary>
        /// NImpronte stampo
        /// </summary>
        public decimal CavityMoldNum { get; set; }
        /// <summary>
        /// Numero di cicli ora
        /// </summary>
        public decimal StandardRate { get; set; }

        /// <summary>
        /// Pezzi prodotti (PartialCounting * CavityMoldNum)
        /// </summary>
        public decimal QtyProduced { get; set; }
        /// <summary>
        /// Quantità residua
        /// </summary>
        public decimal QtyResidual { get; set; }
        /// <summary>
        /// Quantità scarti
        /// </summary>
        public decimal QtyProductionWaste { get; set; }
        /// <summary>
        /// Quantità scarti recuperabile
        /// </summary>
        public decimal QtyProductionWasteReusable { get; set; }
        /// <summary>
        /// Quantità materia prima
        /// </summary>
        public decimal QtyRawMaterial { get; set; }
        /// <summary>
        /// Quantità scarti materia prima
        /// </summary>
        public decimal QtyProductionWasteRawMaterial { get; set; }
        /// <summary>
        /// Quantità pezzi OK
        /// </summary>
        public decimal QtyOK { get; set; }

        /// <summary>
        /// Unità di misura della quantità in ingresso
        /// </summary>
        public string PartialCountingUM { get; set; }
        /// <summary>
        /// Unità di misura della quantità prodotta
        /// </summary>
        public string QtyProducedUM { get; set; }

        public int? ArticleBatchId { get; set; }
        [ForeignKey("ArticleBatchId")]
        public virtual ArticleBatch Batch { get; set; }

        /// <summary>
        /// True se la transazione è aperta
        /// </summary>
        public bool Open { get; set; }

        public DateTime LastUpdate { get; set; } //da usare per verificare tempo di timeout di una macchina

        /// <summary>
        /// Indica se la transazione è stata verificata, quindi se è esportabile
        /// </summary>
        public bool Verified { get; set; }

        /// <summary>
        /// True se la transazione è stata esportata
        /// </summary>
        public bool Exported { get; set; }

        /// <summary>
        /// True se la commessa è stata chiusa in quella transazione, altrimenti null
        /// </summary>
        public bool? IsOrderClosed { get; set; }

        //[Index("UniqueClientApplicationIndex", 1, IsUnique = true)]
        public int? ClientId { get; set; }

        //[Index("UniqueClientApplicationIndex", 2, IsUnique = true)]
        [MaxLength(128)]
        public string ApplicationId { get; set; }
    }
}