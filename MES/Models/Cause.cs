﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    [JsonObject(IsReference = true)]
    public class Cause
    {
        public Cause()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        public string Code { get; set; }

        public string ExternalCode { get; set; } //Campo per un codice dell'azienda se vuole usarlo il cliente

        [Required]
        public string Description { get; set; }
        public string CauseTypeId { get; set; }
        public string ApplicationId { get; set; }

        [ForeignKey("CauseTypeId")]
        public virtual CauseType CauseType { get; set; }
    }
}