﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class MaintenancePlanOperator // Assegnazione piano di manutenzione ad un operatore
    {
        public MaintenancePlanOperator() { }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        [MaxLength(50)]
        public string Supplier { get; set; }        // Fornitore (Manutenzione Esterna)
        public bool SupplierFlag { get; set; }      // Quando true = Manutenzione Esterna, quando false = Manuntezione Interna

        public string OperatorId { get; set; }
        [ForeignKey("OperatorId")]
        public virtual Operator Operator { get; set; }

        public int MaintenanceId { get; set; }  // Id associazione Asset/Mold <-> Piano di manutenzione e controllo

        public DateTime Date { get; set; } // Data prevista

        public bool Executed { get; set; }

        public DateTime ExecutedDate { get; set; }

        //[Index("UniqueClientApplicationIndex", 1, IsUnique = true)]
        public int? ClientId { get; set; }

        //[Index("UniqueClientApplicationIndex", 2, IsUnique = true)]
        [MaxLength(128)]
        public string ApplicationId { get; set; }
    }
}