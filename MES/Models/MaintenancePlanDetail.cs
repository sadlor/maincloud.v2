﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MES.Models
{
    public class MaintenancePlanDetail
    {
        public MaintenancePlanDetail() { }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        public int MaintenancePlanId { get; set; }
        [ForeignKey("MaintenancePlanId")]
        public virtual MaintenancePlan MaintenancePlan { get; set; }

        [MaxLength(40)]
        public string ItemCode { get; set; } // Codice particolare

        [MaxLength(40)]
        public string Description { get; set; }

        [MaxLength(2000)]
        public string DetailNotes { get; set; }

        [MaxLength(20)]
        public string ToolCode { get; set; }

        //[Index("UniqueClientApplicationIndex", 1, IsUnique = true)]
        public int? ClientId { get; set; }

        //[Index("UniqueClientApplicationIndex", 2, IsUnique = true)]
        [MaxLength(128)]
        public string ApplicationId { get; set; }
    }
}
