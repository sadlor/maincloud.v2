﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    [JsonObject(IsReference = true)]
    public class CauseType //TODO: capire se utile e implementare o cancellare
    {
        public CauseType()
        {
            Id = Guid.NewGuid().ToString();
            Cause = new List<Models.Cause>();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        public string Description { get; set; }

        public string ApplicationId { get; set; }

        [InverseProperty("CauseType")]
        public virtual List<Cause> Cause { get; set; }
    }
}