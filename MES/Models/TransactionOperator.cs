﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class TransactionOperator
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        public string MachineId { get; set; }
        public string JobId { get; set; }
        public string OperatorId { get; set; }
        public string RoleId { get; set; }
        public string CauseId { get; set; }
        public string OperationName { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public string Notes { get; set; }
        public bool Open { get; set; }

        public int? QualityTransactionId { get; set; } //transaction id to watch for quality

        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        [ForeignKey("CauseId")]
        public virtual Cause Cause { get; set; }

        [ForeignKey("OperatorId")]
        public virtual Operator Operator { get; set; }

        [ForeignKey("RoleId")]
        public virtual MesRole Role { get; set; }
    }
}