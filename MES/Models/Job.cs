﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    [JsonObject(IsReference = true)]
    public class Job  //Ordine di produzione
    {
        public Job()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        //public string JobId { get; set; }
        public string Code { get; set; }

        public string Description { get; set; }
        public string DescriptionExtended { get; set; }

        public string MachineGroupId { get; set; }
        //public int? ArticleId { get; set; }
        public int? OrderId { get; set; }
        public string AssetId { get; set; }    //MachineId
        public string OperatorId { get; set; }
        public string EquipmentId { get; set; } //Attrezzatura
        public string MoldId { get; set; } //Stampo

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? SetupTime { get; set; }
        public TimeSpan? TransferTime { get; set; }  //DurataTrasferimento
        
        /// <summary>
        /// Tempo singolo ciclo
        /// </summary>
        public decimal CycleTime { get; set; }
        /// <summary>
        /// Numero cicli ora
        /// </summary>
        public decimal StandardRate { get; set; }
        public decimal? CavityMoldNum { get; set; } //NImpronte stampo

        public decimal QtyOrdered { get; set; }

        public string DisegnoFase { get; set; }
        public string SetupPlan { get; set; }
        public string SafetyNotes { get; set; }
        public string EnvironmentNotes { get; set; }

        public int? ControlPlanId { get; set; }
        [ForeignKey("ControlPlanId")]
        public virtual ControlPlan ControlPlan { get; set; }

        //[ForeignKey("ArticleId")]
        //public virtual Article Article { get; set; }

        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }

        [ForeignKey("EquipmentId")]
        public virtual Equipment Equipment { get; set; }

        //[Index("UniqueClientApplicationIndex", 1, IsUnique = true)]
        //[MaxLength(50)]
        public int ClientId { get; set; }

        //[Index("UniqueClientApplicationIndex", 2, IsUnique = true)]
        [MaxLength(128)]
        public string ApplicationId { get; set; }
    }
}