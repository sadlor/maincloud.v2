﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MES.Models
{
    public class MaintenancePlanDetailExecuted
    {
        public MaintenancePlanDetailExecuted() { }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        public int? PlanId { get; set; } // Permette di raggruppare attività compilate nello stesso piano di manutenzione (equivale all'ID della prima attività registrata)

        public int? MaintenancePlanDetailId { get; set; }
        [ForeignKey("MaintenancePlanDetailId")]
        public virtual MaintenancePlanDetail MaintenancePlanDetail { get; set; }

        public bool extraMaintenance { get; set; } // Flag intervento aggiuntivo

        public string extraMaintenanceString
        {
            get
            {
                return (extraMaintenance) ? "Si" : "No";
            }
        }

        public int? MaintenancePlanId { get; set; }
        [ForeignKey("MaintenancePlanId")]
        public virtual MaintenancePlan MaintenancePlan { get; set; }

        public int? MaintenancePlanOperatorId { get; set; }
        [ForeignKey("MaintenancePlanOperatorId")]
        public virtual MaintenancePlanOperator MaintenanceOperatorPlan { get; set; }

        public string ObjectId { get; set; } // AssetId, MoldId

        public string ObjectType { get; set; } // "Asset" (AssetType), "Mold" (MoldType)

        [NotMapped]
        public string MoldType { get { return "Mold"; } }
        [NotMapped]
        public string AssetType { get { return "Asset"; } }

        [MaxLength(40)]
        public string ItemCode { get; set; } // Codice particolare

        [MaxLength(40)]
        public string Description { get; set; }

        [MaxLength(20)]
        public string ToolCode { get; set; }

        [MaxLength(2000)]
        public string DetailNotes { get; set; }

        [MaxLength(20)]
        public string Result { get; set; }  // Esito: Effettuato, Non effettuabile, Sospeso, Saltato

        public string ResultValueString
        {
            get
            {
                switch (Result.ToLower())
                {
                    case "effettuato":
                        return "✓";
                    case "non effettuabile":
                        return "✕";
                    case "sospeso":
                        return "―";
                    case "saltato":
                        return "￬";
                    default:
                        return "";
                }
            }
        }

        public string ResultValueColor
        {
            get
            {
                switch (Result.ToLower())
                {
                    case "effettuato":
                        return "PaleGreen";
                    case "non effettuabile":
                        return "LightCoral";
                    case "sospeso":
                        return "#FBFB98";
                    case "saltato":
                        return "Transparent";
                    default:
                        return "Transparent";
                }
            }
        }

        public DateTime ExecutionDate { get; set; } // Data di esecuzione del piano di manutenzione

        public decimal QtyProduced { get; set; } // Quantità prodotta fino ad allora

        public long ProductionTime { get; set; } // Tempo di produzione fino ad allora

        public string ProductionTimeString
        {
            get
            {
                TimeSpan pt = new TimeSpan(ProductionTime);
                return ((pt.Hours < 10) ? "0" + pt.Hours.ToString() : pt.Hours.ToString()) + ":" +
                       ((pt.Minutes < 10) ? "0" + pt.Minutes.ToString() : pt.Minutes.ToString()) + ":" +
                       ((pt.Seconds < 10) ? "0" + pt.Seconds.ToString() : pt.Seconds.ToString());
            }
        }

        //[Index("UniqueClientApplicationIndex", 1, IsUnique = true)]
        public int? ClientId { get; set; }

        //[Index("UniqueClientApplicationIndex", 2, IsUnique = true)]
        [MaxLength(128)]
        public string ApplicationId { get; set; }
    }
}
