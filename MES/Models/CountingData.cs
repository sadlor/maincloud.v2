﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class CountingData
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public long Id { get; set; }

        [Index("IX_CountingIndex", 1, IsUnique = true)]
        [Index("IX_UniqueCountingData", 1, IsUnique = true)]
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
        public int Duration { get; set; }

        [Index("IX_CountingIndex", 2, IsUnique = true)]
        [Index("IX_UniqueCountingData", 2, IsUnique = true)]
        [MaxLength(128)]
        public string AssetId { get; set; }              //macchina

        [Index("IX_CountingIndex", 3, IsUnique = true)]
        [MaxLength(128)]
        public string OperatorId { get; set; }

        [Index("IX_CountingIndex", 4, IsUnique = true)]
        [MaxLength(128)]
        public string JobId { get; set; }

        [Index("IX_UniqueCountingData", 3, IsUnique = true)]
        [MaxLength(128)]
        public string CountingAnalyzerId { get; set; }

        public decimal ProgressiveCounting { get; set; }
        public decimal? ProgressiveCountingEnd { get; set; }
        public decimal PartialCounting { get; set; }
        public decimal AverageCounting { get; set; }
        public decimal? ProductionComply { get; set; } //Produzione conforme
        public decimal? ProductionWaste { get; set; }
        public string MeasureUnit { get; set; }
        public string CauseId { get; set; }

        [ForeignKey("OperatorId")]
        public virtual Operator Operator { get; set; }

        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        [ForeignKey("CauseId")]
        public virtual Cause Cause { get; set; }
    }
}