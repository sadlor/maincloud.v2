﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    [JsonObject(IsReference = true)]
    public class Order   //Ordine di produzione
    {
        public Order()
        {
            Job = new List<Job>();
        }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        public int IdArticle { get; set; }
        public string OrderCode { get; set; }
        public string Description { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime PlannedStartDate { get; set; } //data inizio prevista
        public DateTime PlannedEndDate { get; set; }   //data fine prevista
        public decimal QtyOrdered { get; set; }          //quantità ordinata
        public decimal QtyProduced { get; set; }         //quantità prodotta
        public decimal ResidualQty { get; set; }         //quantità residua
        public decimal QtyProductionWaste { get; set; }  //quantità scarti
        public decimal QtyOK { get; set; }               //quantità pezzi OK
        public bool FlagFromManagement { get; set; } //flag da gestionale
        public int? CustomerOrderId { get; set; } //Id Commessa
        public string StatusCode { get; set; }  //Codice dello stato
        public string StatusDescription { get; set; }  //Descrizione dello stato

        [ForeignKey("CustomerOrderId")]
        public virtual CustomerOrder CustomerOrder { get; set; }

        [ForeignKey("IdArticle")]
        public virtual Article Article { get; set; }

        [InverseProperty("Order")]
        public virtual List<Job> Job { get; set; }

        //[Index("UniqueClientApplicationIndex", 1, IsUnique = true)]
        public int ClientId { get; set; }

        //[Index("UniqueClientApplicationIndex", 2, IsUnique = true)]
        [MaxLength(128)]
        public string ApplicationId { get; set; }
    }
}