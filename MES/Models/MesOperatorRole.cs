﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    /// <summary>
    ///     EntityType that represents a user belonging to a role
    /// </summary>
    public class MesOperatorRole
    {
        /// <summary>
        ///     UserId for the user that is in the role
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///     RoleId for the role
        /// </summary>
        public string RoleId { get; set; }
    }
}