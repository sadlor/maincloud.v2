﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class MoldMaintenance
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        public string MoldId { get; set; }
        public DateTime Date { get; set; }
        public decimal ProgressiveCounter { get; set; }
        public decimal Duration { get; set; }

        public string CauseId { get; set; }
        [ForeignKey("CauseId")]
        public virtual Cause Cause { get; set; }

        public string OperatorId { get; set; }
        [ForeignKey("OperatorId")]
        public virtual Operator Operator { get; set; }

        public string ApplicationId { get; set; }
    }
}