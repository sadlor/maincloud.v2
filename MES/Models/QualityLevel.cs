﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    /// <summary>
    /// Livello qualità, registra i benestare alla produzione per ogni commessa
    /// </summary>
    public class QualityLevel
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        [Required]
        public string MachineId { get; set; }

        [Required]
        public string OperatorId { get; set; }

        [Required]
        public string JobId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public string Level { get; set; }

        public string Note { get; set; }

        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        [ForeignKey("OperatorId")]
        public virtual Operator Operator { get; set; }
    }
}