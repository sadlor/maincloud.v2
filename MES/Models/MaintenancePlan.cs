﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MES.Models
{
    public class MaintenancePlan  //Piani di manutenzione
    {
        public MaintenancePlan() { }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        [MaxLength(60)]
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ValidationDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public string CreationDateText
        {
            get
            {
                return (CreationDate.Day < 10 ? "0" + CreationDate.Day.ToString() : CreationDate.Day.ToString()) + "/" +
                       (CreationDate.Month < 10 ? "0" + CreationDate.Month.ToString() : CreationDate.Month.ToString()) + "/" + CreationDate.Year;
            }
        }
        public string ValidationDateText
        {
            get
            {
                return (ValidationDate.Day < 10 ? "0" + ValidationDate.Day.ToString() : ValidationDate.Day.ToString()) + "/" +
                     (ValidationDate.Month < 10 ? "0" + ValidationDate.Month.ToString() : ValidationDate.Month.ToString()) + "/" + ValidationDate.Year;
            }
        }
        public string UpdateDateText
        {
            get
            {
                return (UpdateDate.Day < 10 ? "0" + UpdateDate.Day.ToString() : UpdateDate.Day.ToString()) + "/" +
                         (UpdateDate.Month < 10 ? "0" + UpdateDate.Month.ToString() : UpdateDate.Month.ToString()) + "/" + UpdateDate.Year;
            }
        }

        [MaxLength(50)]
        public string ExecutionSupplier { get; set; }        // Fornitore (Manutenzione Esterna)
        public bool ExecutionSupplierFlag { get; set; }      // Quando true = Manutenzione Esterna, quando false = Manuntezione Interna
        public bool ExecutionOperatorFlag { get { return !ExecutionSupplierFlag; } }
        [MaxLength(50)]
        public string ExecutionOperator { get; set; }   // Operatore (Manutenzione Interna)
        public string Executor { get { return (ExecutionOperatorFlag) ? ExecutionOperator : ExecutionSupplier; } }  // Esecutore manutenzione: Fornitore oppure Operatore

        public string Type { get { return (ExecutionOperatorFlag) ? "Interna" : "Esterna"; } }


        [MaxLength(50)]
        public string ApprovalUser { get; set; }
        [MaxLength(2000)]
        public string PlanNotes { get; set; }

        public decimal? QtyFrequency { get; set; }

        public bool QtyFrequencyFlag { get; set; }
        public bool QtyFrequencyNotFlag { get { return !QtyFrequencyFlag; } }

        public long TimeFrequency { get; set; }

        [NotMapped]
        public TimeSpan tf
        {
            get
            {
                return new TimeSpan(TimeFrequency);
            }
        }

        public bool TimeFrequencyFlag { get { return !QtyFrequencyFlag; } }
        public bool TimeFrequencyNotFlag { get { return QtyFrequencyFlag; } }

        public string TimeFrequencyDate
        {
            get
            {
                return TimeFrequencyHours + ":" + TimeFrequencyMinutes + ":" + TimeFrequencySeconds;
            }
        }

        public string TimeFrequencyHours
        {
            get
            {
                return ((Math.Floor(tf.TotalHours) < 10) ? "0" + Math.Floor(tf.TotalHours).ToString() : Math.Floor(tf.TotalHours).ToString());
            }
        }

        public string TimeFrequencyMinutes
        {
            get
            {
                return ((tf.Minutes < 10) ? "0" + tf.Minutes.ToString() : tf.Minutes.ToString());
            }
        }

        public string TimeFrequencySeconds
        {
            get
            {
                return ((tf.Seconds < 10) ? "0" + tf.Seconds.ToString() : tf.Seconds.ToString());
            }
        }

        public long TimeExpected { get; set; } // Tempo previsto (ticks)

        [NotMapped]
        public TimeSpan ts
        {
            get
            {
                return new TimeSpan(TimeExpected);
            }
        }

        public string TimeExpectedString
        {
            get
            {
                return TimeExpectedHours + ":" + TimeExpectedMinutes + ":" + TimeExpectedSeconds;
            }
        }

        public string TimeExpectedHours
        {
            get
            {
                return ((Math.Floor(ts.TotalHours) < 10) ? "0" + Math.Floor(ts.TotalHours).ToString() : Math.Floor(ts.TotalHours).ToString());
            }
        }

        public string TimeExpectedMinutes
        {
            get
            {
                return ((ts.Minutes < 10) ? "0" + ts.Minutes.ToString() : ts.Minutes.ToString());
            }
        }

        public string TimeExpectedSeconds
        {
            get
            {
                return ((ts.Seconds < 10) ? "0" + ts.Seconds.ToString() : ts.Seconds.ToString());
            }
        }

        //[Index("UniqueClientApplicationIndex", 1, IsUnique = true)]
        public int? ClientId { get; set; }

        //[Index("UniqueClientApplicationIndex", 2, IsUnique = true)]
        [MaxLength(128)]
        public string ApplicationId { get; set; }
    }
}