﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class ProductionBatch
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public int Id { get; set; }
        public string Code { get; set; }
        public decimal Quantity { get; set; }

        //public int RilievoId { get; set; }
        //[ForeignKey("RilievoId")]
        //public virtual Rilievo Rilievo { get; set; }

        public string ApplicationId { get; set; }
    }
}