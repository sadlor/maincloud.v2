﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class JobBatch
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        [Required]
        public string MachineId { get; set; }
        [Required]
        public string JobId { get; set; }
        [Required]
        public int ArticleBatchId { get; set; }
        public string OperatorId { get; set; }

        [Required]
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }

        public decimal Quantity { get; set; }
        //public decimal? QtyProductionWaste { get; set; } //quantità scarti materia prima

        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        [ForeignKey("ArticleBatchId")]
        public virtual ArticleBatch ArticleBatch { get; set; }

        [ForeignKey("OperatorId")]
        public virtual Operator Operator { get; set; }

        public string ApplicationId { get; set; }

        /// <summary>
        /// True se il lotto è stato confermato
        /// </summary>
        public bool Verified { get; set; }
        /// <summary>
        /// Data ora di esportazione al gestionale
        /// </summary>
        public DateTime? Exported { get; set; }

        public JobBatch()
        {
            Verified = false;
            Exported = null;
        }
    }
}