﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MES.Models
{
    public class ControlPlanDetailExecuted
    {
        public ControlPlanDetailExecuted() { }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        public int? IdControllo { get; set; }

        public int? PlanId { get; set; } // Permette di raggruppare attività compilate nello stesso piano di controllo (equivale all'ID della prima attività registrata)

        [MaxLength(128)]
        public string JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        public int ControlPlanDetailId { get; set; }
        [ForeignKey("ControlPlanDetailId")]
        public virtual ControlPlanDetail ControlPlanDetail { get; set; }

        [MaxLength(40)]
        public string Description { get; set; }

        public int ArticleId { get; set; }
        [ForeignKey("ArticleId")]
        public virtual Article Article { get; set; }

        public string AssetId { get; set; }    // MachineId

        public string OperatorId { get; set; }
        [ForeignKey("OperatorId")]
        public virtual Operator Operator { get; set; }

        public string OperatorUserName
        {
            get
            {
                return (Operator != null) ? Operator.UserName : "";
            }
        }

        public string ValueOperator { get; set; } // Operatore: Uguale a, Minore di, Maggiore di, Minore Uguale di, Maggiore Uguale di

        public string ValueOperatorSymbol
        {
            get
            {
                switch (ValueOperator)
                {
                    case "Maggiore di":
                        return ">";
                    case "Minore di":
                        return "<";
                    case "Maggiore Uguale di":
                        return ">=";
                    case "Minore Uguale di":
                        return "<=";
                    default:
                        return "=";
                }
            }
        }

        public bool isValueOperatorEqual { get { return (ValueOperator == "Uguale a"); } }

        [MaxLength(7)]
        public string Udm { get; set; }

        public decimal BaseValue { get; set; }

        public decimal Tolerance1Value { get; set; }

        [MaxLength(1)]
        public string Tolerance1Sign { get; set; }

        public decimal Tolerance2Value { get; set; }

        [MaxLength(1)]
        public string Tolerance2Sign { get; set; }

        public bool isControlDimensional { get { return (ControlType == "Dimensionale"); } }

        public bool isToleranceVisible { get { return isValueOperatorEqual && isControlDimensional; } }

        [MaxLength(12)]
        public string ToolCode { get; set; }

        [MaxLength(20)]
        public string ControlType { get; set; }

        public string ControlTypeShort
        {
            get
            {
                if (ControlType == "Dimensionale")
                {
                    return "Dim.";
                }
                else
                {
                    return "Vis.";
                }
            }
        }

        public decimal MeasuredValue { get; set; } // Valore misurato (Dimensionale)

        public bool BooleanValue { get; set; } // Valore booleano (OK, NO) (Visivo)

        public string StringValue
        {
            get
            {
                return (isControlDimensional) ? MeasuredValue.ToString() : ((BooleanValue) ? "Ok" : "No");
            }
        }

        public bool ResultValue { get; set; } // Esito

        public string ResultValueString
        {
            get
            {
                return (ResultValue) ? "Ok" : "No";
            }
        }

        public DateTime ExecutionDate { get; set; } // Data di esecuzione del piano di controllo

        public decimal QtyProduced { get; set; } // Quantità prodotta fino ad allora

        public long ProductionTime { get; set; } // Tempo di produzione fino ad allora

        public string ProductionTimeString
        {
            get
            {
                TimeSpan pt = new TimeSpan(ProductionTime);
                return ((pt.Hours < 10) ? "0" + pt.Hours.ToString() : pt.Hours.ToString()) + ":" +
                       ((pt.Minutes < 10) ? "0" + pt.Minutes.ToString() : pt.Minutes.ToString()) + ":" +
                       ((pt.Seconds < 10) ? "0" + pt.Seconds.ToString() : pt.Seconds.ToString());
            }
        }

        //[Index("UniqueClientApplicationIndex", 1, IsUnique = true)]
        public int? ClientId { get; set; }

        //[Index("UniqueClientApplicationIndex", 2, IsUnique = true)]
        [MaxLength(128)]
        public string ApplicationId { get; set; }
    }
}
