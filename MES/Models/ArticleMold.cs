﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class ArticleMold
    {
        [Key, Required, Column(Order = 0), MaxLength(128)]
        public string MoldId { get; set; }

        [Key, Required, Column(Order = 1)]//, MaxLength(128)]
        public int ArticleId { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Article Article { get; set; }
    }
}