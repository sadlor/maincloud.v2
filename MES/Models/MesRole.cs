﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    /// <summary>
    ///     Represents a Role entity
    /// </summary>
    public class MesRole : IdentityRole
    {
        /// <summary>
        ///     Constructor
        /// </summary>
        public MesRole()
        {
            Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="roleName"></param>
        public MesRole(string roleName) : this()
        {
            Name = roleName;
        }

        public MesRole(string roleName, string description) : this(roleName)
        {
            Description = description;
        }

        /// <summary>
        ///     Navigation property for users in the role
        /// </summary>
        public new virtual ICollection<MesOperatorRole> Users { get; } = new List<MesOperatorRole>();

        /// <summary>
        ///     Role id
        /// </summary>
        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public new string Id { get; set; }

        /// <summary>
        ///     Role name
        /// </summary>
        [MaxLength(128)]
        [Index("MesRoleMultiTenantNameIndex", IsUnique = true, Order = 1)]
        public new string Name { get; set; }

        public string Description { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("MesRoleMultiTenantNameIndex", IsUnique = true, Order = 2)]
        public string ApplicationId { get; set; }

        public bool AllowSelectJob { get; set; }
        public bool AllowSelectMold { get; set; }
        public bool AllowSelectMP { get; set; }

        public bool AllowSetup { get; set; }       //permetti attrezzaggio
        public bool AllowMaintenance { get; set; } //permetti manutenzione
        public bool AllowQuality { get; set; }     //permetti qualità
        public bool AllowProduction { get; set; }
        public bool AllowWaste { get; set; }       //permetti scarti
        public bool AllowConfirmStartUpProduction { get; set; }  //benestare alla produzione (per capoturno)

        public bool AllowAnalysis { get; set; }
        public bool AllowOEE { get; set; }
    }
}