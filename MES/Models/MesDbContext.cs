﻿using MES.Configuration;
using MES.Core;
using MES.Models.Views;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class MesDbContext : DbContext
    {
        public DbSet<CountingData> CountingDatas { get; set; }
        
        #region Users & roles
        public DbSet<Operator> Operators { get; set; }
        public DbSet<MesRole> MesRoles { get; set; }
        public DbSet<MesOperatorRole> MesOperatorRoles { get; set; }
        #endregion

        public DbSet<Cause> Causes { get; set; }
        public DbSet<CauseType> CauseTypes { get; set; }

        #region Articles & orders
        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleBatch> ArticleBatches { get; set; }
        public DbSet<ArticleMold> ArticleMolds { get; set; }
        public DbSet<CustomerOrder> CustomerOrders { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<JobBatch> JobBatches { get; set; }
        #endregion

        public DbSet<ControlPlan> ControlPlans { get; set; }
        public DbSet<ControlPlanDetail> ControlPlanDetails { get; set; }
        public DbSet<ControlPlanDetailExecuted> ControlPlanDetailExecuteds { get; set; }
        public DbSet<MaintenancePlan> MaintenancePlans { get; set; }
        public DbSet<MaintenancePlanDetail> MaintenancePlanDetails { get; set; }
        public DbSet<MaintenancePlanDetailExecuted> MaintenancePlanDetailExecuteds { get; set; }
        public DbSet<MaintenancePlanOperator> MaintenancePlanOperators { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        //public DbSet<RawMaterial> RawMaterials { get; set; }
        //public DbSet<RawMaterialBatch> RawMaterialBatches { get; set; }
        public DbSet<ProductionBatch> ProductionBatches { get; set; }
        public DbSet<MeasureUnit> MeasureUnits { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionOperator> TransactionOperators { get; set; }
        public DbSet<JobProductionWaste> JobProductionWaste { get; set; }
        public DbSet<QualityLevel> QualityLevels { get; set; }
        public DbSet<ProductionPayment> ProductionPayments { get; set; }
        public DbSet<ModuleConfiguration> ModuleConfigurations { get; set; }
        public DbSet<TransactionMold> TransactionMolds { get; set; }
        public DbSet<MoldMaintenance> MoldMaintenances { get; set; }

        public DbSet<PartProgram> PartPrograms { get; set; }

        #region Views
        public DbSet<JobOrderView> SelectJobOrders { get; set; }
        #endregion

        //private static EFLogCommandInterceptor commandInterceptor;

        public MesDbContext()
            : base("MESConnection")
        {
            //commandInterceptor = new EFLogCommandInterceptor();
            //DbInterception.Add(commandInterceptor);

            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new MesDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder); // I had removed this
                                                /// Rest of on model creating here.

            //modelBuilder.Entity<JobRawMaterial>()
            //    .HasRequired(x => x.Article)
            //    .WithMany(rm => rm.JobRawMaterial)
            //    .HasForeignKey(x => x.ArticleId)
            //    .WillCascadeOnDelete(false);

            //// Needed to ensure subclasses share the same table
            //var user = modelBuilder.Entity<Operator>()
            //    .ToTable("Operators");
            ////user.HasKey(u => u.Id);
            //user.HasMany(u => u.Roles).WithRequired().HasForeignKey(ur => ur.UserId);
            ////user.Property(u => u.Code) //la matricola deve essere unica
            ////    .IsRequired()
            ////    .HasMaxLength(256)
            ////    .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("UserNameIndex") { IsUnique = true }));

            //modelBuilder.Entity<IdentityUserRole>()
            //    .HasKey(r => new { r.UserId, r.RoleId })
            //    .ToTable("MesOperatorRoles");
            
            //var role = modelBuilder.Entity<MesRole>()
            //    .ToTable("MesRoles");
            //role.Property(r => r.Name)
            //    .IsRequired()
            //    .HasMaxLength(256)
            //    .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("OperatorRoleMultiTenantNameIndex") { IsUnique = true, Order = 1 }));
            //role.HasMany(r => r.Users).WithRequired().HasForeignKey(ur => ur.RoleId);

            #region Users & roles
            var user = modelBuilder.Entity<Operator>().ToTable("Operators");
            user.HasMany(u => u.Roles).WithRequired().HasForeignKey(ur => ur.UserId);
            //user_new.Property(u => u.UserName)
            //        .IsRequired()
            //        .HasMaxLength(256)
            //        .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("UserNameIndex") { IsUnique = true }));

            var role = modelBuilder.Entity<MesRole>().ToTable("MesRoles");
            role.HasMany(r => r.Users).WithRequired().HasForeignKey(ur => ur.RoleId);

            modelBuilder.Entity<MesOperatorRole>()
                .HasKey(r => new { r.UserId, r.RoleId })
                .ToTable("MesOperatorRoles");
            #endregion

            //modelBuilder.Entity<BOM>()
            //        .HasRequired(b => b.Article)
            //        .WithMany(a => a.BOM)
            //        .HasForeignKey(b => b.ArticleId)
            //        .WillCascadeOnDelete(false);
            //modelBuilder.Entity<BOM>()
            //        .HasRequired(b => b.Component)
            //        .WithMany(a => a.Component)
            //        .HasForeignKey(b => b.ComponentId)
            //        .WillCascadeOnDelete(false);

            modelBuilder.Entity<ArticleBatch>().Property(x => x._ExtraData).HasColumnName("ExtraData");

            modelBuilder.Entity<Transaction>().Property(x => x.PartialCounting).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(x => x.CounterStart).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(x => x.CounterEnd).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(x => x.ProgressiveCounter).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(x => x.QtyProduced).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(x => x.QtyOK).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(x => x.QtyProductionWaste).HasPrecision(18, 4);
            modelBuilder.Entity<Transaction>().Property(x => x.CavityMoldNum).HasPrecision(10, 4);
            modelBuilder.Entity<Job>().Property(x => x.CavityMoldNum).HasPrecision(10, 4);

            //modelBuilder.Entity<Transaction>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            //modelBuilder.Entity<TransactionOperator>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            //modelBuilder.Entity<Article>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            //modelBuilder.Entity<CustomerOrder>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            //modelBuilder.Entity<Order>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }

        //public int SaveChanges(string identityTable = null)
        //{
        //    if (identityTable != null)
        //    {
        //        commandInterceptor.SQLInjectText =
        //          string.Format("SET IDENTITY_INSERT {0} ON", identityTable);
        //    }
        //    return base.SaveChanges();
        //}

        public static MesDbContext Create()
        {
            return new MesDbContext();
        }
    }
}