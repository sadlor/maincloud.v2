﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    /// <summary>
    /// Bill of materials = distinta base
    /// </summary>
    public class BOM
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public int ComponentId { get; set; }
        public decimal Quantity { get; set; }
        public string MeasureUnit { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Article Article { get; set; }

        [ForeignKey("ComponentId")]
        public virtual Article Component { get; set; }
    }
}