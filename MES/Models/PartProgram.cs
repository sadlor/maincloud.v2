﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class PartProgram
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string AssetGroupId { get; set; }
        //[Required]
        public string AssetId { get; set; }
        //public string MachineCode { get; set; }
        public string ArticleType { get; set; }
        public int? ArticleId { get; set; }
        //public string JobId { get; set; }
        public string PartProgramCode { get; set; }
        //public decimal CycleLength { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Article Article { get; set; }
    }
}