﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class ArticleBatch
    {
        [Key]
        [Required]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }
        public string Code { get; set; }
        public int ArticleId { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Article Article { get; set; }

        public decimal Quantity { get; set; }

        internal string _ExtraData { get; set; }
        [NotMapped]
        public Dictionary<string, object> ExtraData
        {
            get { return _ExtraData == null ? null : JsonConvert.DeserializeObject<Dictionary<string, object>>(_ExtraData); }
            set { _ExtraData = JsonConvert.SerializeObject(value); }
        }

        public string ApplicationId { get; set; }
    }
}
