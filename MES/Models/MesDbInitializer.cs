﻿using MainCloudFramework.Web.Helpers;
using MES.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    public class MesDbInitializer : CreateDatabaseIfNotExists<MesDbContext>
    {
        protected override void Seed(MesDbContext context)
        {
            InitializeDefaultData(context);
            base.Seed(context);
        }

        protected void InitializeDefaultData(MesDbContext db)
        {
            //ConfigurationService config = new ConfigurationService();
            //config.InsertDefault();
        }
    }
}