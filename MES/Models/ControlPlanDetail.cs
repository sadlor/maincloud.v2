﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MES.Models
{
    public class ControlPlanDetail
    {
        public ControlPlanDetail() { }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        [NotMapped]
        public int? IdControllo { get; set; }

        public int ControlPlanId { get; set; }
        [ForeignKey("ControlPlanId")]
        public virtual ControlPlan ControlPlan { get; set; }

        [MaxLength(40)]
        public string Description { get; set; }

        [MaxLength(2000)]
        public string DetailNotes { get; set; }

        public string Operator { get; set; }

        public string OperatorSymbol
        {
            get
            {
                switch (Operator)
                {
                    case "Maggiore di":
                        return ">";
                    case "Minore di":
                        return "<";
                    case "Maggiore Uguale di":
                        return ">=";
                    case "Minore Uguale di":
                        return "<=";
                    default:
                        return "=";
                }
            }
        }

        public bool isOperatorEqual { get { return (Operator == "Uguale a"); } }

        [MaxLength(7)]
        public string Udm { get; set; }

        public decimal BaseValue { get; set; }

        public decimal Tolerance1Value { get; set; }

        [MaxLength(1)]
        public string Tolerance1Sign { get; set; }

        public decimal Tolerance2Value { get; set; }

        [MaxLength(1)]
        public string Tolerance2Sign { get; set; }

        [MaxLength(12)]
        public string ToolCode { get; set; }

        [MaxLength(20)]
        public string ControlType { get; set; }

        public string ControlTypeShort
        {
            get
            {
                if (ControlType == "Dimensionale")
                {
                    return "Dim.";
                }
                else
                {
                    return "Vis.";
                }
            }
        }

        public bool isControlDimensional { get { return (ControlType == "Dimensionale"); } }

        public bool isToleranceVisible { get { return isOperatorEqual && isControlDimensional; } }

        public string Tol1
        {
            get
            {
                switch (Tolerance1Sign)
                {
                    case "+":
                        return "+" + Tolerance1Value.ToString("0.00");
                    case "-":
                        return "-" + Tolerance1Value.ToString("0.00");
                }
                return "0";
            }
        }

        public string Tol2
        {
            get
            {
                switch (Tolerance2Sign)
                {
                    case "+":
                        return "+" + Tolerance2Value.ToString("0.00");
                    case "-":
                        return "-" + Tolerance2Value.ToString("0.00");
                }
                return "0";
            }
        }

        public decimal va1
        {
            get
            {
                switch (Tolerance1Sign)
                {
                    case "+":
                        return BaseValue + Tolerance1Value;
                    case "-":
                        return BaseValue - Tolerance1Value;
                }
                return 0;
            }
        }

        public decimal va2
        {
            get
            {
                switch (Tolerance2Sign)
                {
                    case "+":
                        return BaseValue + Tolerance2Value;
                    case "-":
                        return BaseValue - Tolerance2Value;
                }
                return 0;
            }
        }

        //[Index("UniqueClientApplicationIndex", 1, IsUnique = true)]
        public int? ClientId { get; set; }

        //[Index("UniqueClientApplicationIndex", 2, IsUnique = true)]
        [MaxLength(128)]
        public string ApplicationId { get; set; }
    }
}
