﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Models
{
    /// <summary>
    /// Transazioni stampi
    /// </summary>
    public class TransactionMold
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        [Required]
        public string MoldId { get; set; }
        public string MachineId { get; set; }

        public string JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        public string OperatorId { get; set; }
        [ForeignKey("OperatorId")]
        public virtual Operator Operator { get; set; }

        public string CauseId { get; set; }
        [ForeignKey("CauseId")]
        public virtual Cause Cause { get; set; }

        public string Status { get; set; }

        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
        public decimal Duration { get; set; }

        public decimal CounterStart { get; set; }
        public decimal? CounterEnd { get; set; }
        
        /// <summary>
        /// Progressivo generale
        /// </summary>
        public decimal ProgressiveCounter { get; set; }
        /// <summary>
        /// Colpi contati
        /// </summary>
        public decimal PartialCounting { get; set; }

        /// <summary>
        /// Numero impronte stampo
        /// </summary>
        public int CavityMoldNum { get; set; }

        /// <summary>
        /// Pezzi fatti = PartialCounting * CavityMoldNum
        /// </summary>
        public decimal QtyProduced { get; set; }

        /// <summary>
        /// Colpi a vuoto
        /// </summary>
        public decimal BlankShot { get; set; }

        public bool Open { get; set; }
        public DateTime LastUpdate { get; set; } //da usare per verificare tempo di timeout di una macchina
        public bool Verified { get; set; }
        public bool Exported { get; set; }
        public string ApplicationId { get; set; }
    }
}