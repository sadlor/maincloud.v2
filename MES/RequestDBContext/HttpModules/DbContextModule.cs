﻿using MainCloudFramework.RequestDBContext.HttpModules;
using MES.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MES.RequestDBContext.HttpModules
{
    public class DBContextModule : BaseDBContextModule
    {
        // Lista dei db context da rendere disponibili
        override protected Type[] DbContextTypesList
        {
            get
            {
                return new Type[]
                {
                    typeof(MesDbContext)
                };
            }
        }
    }
}