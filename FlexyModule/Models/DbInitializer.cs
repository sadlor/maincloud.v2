﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FlexyModule.Models
{
    public class DbInitializer : CreateDatabaseIfNotExists<FlexyDbContext>
    {
        protected override void Seed(FlexyDbContext context)
        {
            base.Seed(context);
        }
    }
}