﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlexyModule.Models
{
    public class FlexyConfiguration
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        /// <summary>
        /// Flexy IP
        /// </summary>
        public string NetworkAddress { get; set; }
        public string TopicName { get; set; }
        public string AssetId { get; set; }
        public string ApplicationId { get; set; }
    }
}