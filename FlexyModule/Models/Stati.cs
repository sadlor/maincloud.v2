﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FlexyModule.Models
{
    public class Stati
    {

         
        [System.ComponentModel.DataAnnotations.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime inizio { get; set; }

        public string stato { get; set; }

        public double km { get; set; }

        public double kmTot { get; set; }

        public double spessore { get; set; }

        public double larghezza { get; set; }

        public string descrizione { get; set; }

        public double durata { get; set; }

        public double deltaKm { get; set; }

        //public string deltaKmTot { get; set; }
    }
}
