﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FlexyModule.Models
{
    public class FlexyDbContext : DbContext
    {
        public DbSet<TransactionLog> TransactionLogs { get; set; }
        public DbSet<FlexyConfiguration> FlexyConfigurations { get; set; }

        public DbSet<Stati> stato { get; set; }
        public DbSet<Dettagli> dettaglio { get; set; }

        public FlexyDbContext() : base("FlexyConnection")
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new DbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TransactionLog>().Property(x => x.Counter).HasPrecision(18, 4);
        }
    }
}