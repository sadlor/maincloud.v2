﻿using FlexyModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlexyModule
{
    public static class FlexyHelper
    {
        public static string GetApplicationId(string networkAddress)
        {
            using (FlexyDbContext db = new FlexyDbContext())
            {
                return db.FlexyConfigurations.Where(x => x.NetworkAddress == networkAddress).Select(x => x.ApplicationId).FirstOrDefault();
            }
        }
    }
}