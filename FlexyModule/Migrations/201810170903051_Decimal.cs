namespace FlexyModule.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Decimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TransactionLogs", "Counter", c => c.Decimal(nullable: false, precision: 18, scale: 4));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TransactionLogs", "Counter", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
