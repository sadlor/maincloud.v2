namespace FlexyModule.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TransactionLog : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Dettaglis",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            inizio = c.DateTime(nullable: false),
            //            stato = c.String(),
            //            km = c.Double(nullable: false),
            //            kmTot = c.Double(nullable: false),
            //            spessore = c.Double(nullable: false),
            //            larghezza = c.Double(nullable: false),
            //            descrizione = c.String(),
            //            durata = c.Double(nullable: false),
            //            deltaKm = c.Double(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.Statis",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            inizio = c.DateTime(nullable: false),
            //            stato = c.String(),
            //            km = c.Double(nullable: false),
            //            kmTot = c.Double(nullable: false),
            //            spessore = c.Double(nullable: false),
            //            larghezza = c.Double(nullable: false),
            //            descrizione = c.String(),
            //            durata = c.Double(nullable: false),
            //            deltaKm = c.Double(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TransactionLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Machine = c.String(),
                        Status = c.Boolean(nullable: false),
                        CauseCode = c.String(),
                        CauseDescription = c.String(),
                        Date = c.DateTime(nullable: false),
                        Counter = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExtraData = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TransactionLogs");
            DropTable("dbo.Statis");
            DropTable("dbo.Dettaglis");
        }
    }
}
