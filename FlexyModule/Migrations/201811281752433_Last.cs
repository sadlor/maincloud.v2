namespace FlexyModule.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Last : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TransactionLogs", "StatusCode", c => c.String());
            AddColumn("dbo.TransactionLogs", "StatusDescription", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TransactionLogs", "StatusDescription");
            DropColumn("dbo.TransactionLogs", "StatusCode");
        }
    }
}
