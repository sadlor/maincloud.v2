namespace FlexyModule.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FlexyConfiguration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FlexyConfigurations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NetworkAddress = c.String(),
                        TopicName = c.String(),
                        AssetId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FlexyConfigurations");
        }
    }
}
