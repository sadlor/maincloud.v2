namespace FlexyModule.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FlexyConfiguration1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FlexyConfigurations", "ApplicationId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FlexyConfigurations", "ApplicationId");
        }
    }
}
