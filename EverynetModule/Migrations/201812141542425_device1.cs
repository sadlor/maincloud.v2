namespace EverynetModule.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class device1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Devices", "Config", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Devices", "Config");
        }
    }
}
