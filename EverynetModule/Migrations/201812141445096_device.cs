namespace EverynetModule.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class device : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Producer = c.String(),
                        SerialNumber = c.String(),
                        Type = c.String(),
                        Description = c.String(),
                        NetworkAddress = c.String(),
                        ApplicationId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Devices");
        }
    }
}
