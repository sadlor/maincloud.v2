namespace EverynetModule.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class molddevice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Devices", "MoldId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Devices", "MoldId");
        }
    }
}
