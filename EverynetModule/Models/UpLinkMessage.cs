﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverynetModule.Models
{
    public class UpLinkMessage
    {
        public Params @params { get; set; }
        public Meta meta { get; set; }
        public string type { get; set; }

        public class Params
        {
            public string payload { get; set; }
            public int port { get; set; }
            public bool duplicate { get; set; }
            public int counter_up { get; set; }
            public double rx_time { get; set; }
            public string encrypted_payload { get; set; }
        }

        public class Meta
        {
            public string network { get; set; }
            public string packet_hash { get; set; }
            public string application { get; set; }
            public string device_addr { get; set; }
            public double time { get; set; }
            public string device { get; set; }
            public string packet_id { get; set; }
            public string gateway { get; set; }
        }
    }
}