﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverynetModule.Models
{
    public class TMoldMessage
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Network { get; set; }
        public string Packet_hash { get; set; }
        public string Application { get; set; }
        public string Device_addr { get; set; }
        public DateTime Time { get; set; }
        public DateTime Rx_time { get; set; }
        public string Device { get; set; }
        public string Packet_id { get; set; }
        public string Gateway { get; set; }

        //public TMoldPayload Payload { get; set; }
        #region Payload structure
        public int Counter { get; set; }
        public decimal Temperature { get; set; }
        public decimal Battery { get; set; }
        public string Event { get; set; }
        public bool Reset { get; set; }
        public bool ClickButton { get; set; }
        public bool LowBattery { get; set; }
        #endregion
    }
}