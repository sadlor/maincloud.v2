﻿using DeviceModule.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverynetModule.Models
{
    public class Device : IDevice
    {
        public string Id { get; set; }

        public string Producer { get; set; }

        public string SerialNumber { get; set; }

        public string Type { get; set; }

        public string Description { get; set; }

        public string NetworkAddress { get; set; }

        public string ApplicationId { get; set; }

        internal string _Config { get; set; }
        [NotMapped]
        public Dictionary<string, object> Config
        {
            get { return _Config == null ? null : JsonConvert.DeserializeObject<Dictionary<string, object>>(_Config); }
            set { _Config = JsonConvert.SerializeObject(value); }
        }

        public string MoldId { get; set; }
    }
}