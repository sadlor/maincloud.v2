﻿using System.Data.Entity;

namespace EverynetModule.Models
{
    public class EverynetDbInitializer : CreateDatabaseIfNotExists<EverynetDbContext>
    {
        protected override void Seed(EverynetDbContext context)
        {
            base.Seed(context);
        }
    }
}