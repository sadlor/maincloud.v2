﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EverynetModule.Models
{
    [JsonObject(IsReference = true)]
    public class TouchTagMessage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int Id { get; set; }

        public string ConfigVersion { get; set; }
        public int Temperature { get; set; }
        public string FwVersion { get; set; }
        public int TriggerCode { get; set; }
        public int TriggerCounter { get; set; }
        public int Pitch { get; set; }
        public int Roll { get; set; }
        public string Event { get; set; }
        public string Status { get; set; }

        //Meta data of message
        public string Network { get; set; }
        public string Packet_hash { get; set; }
        public string Application { get; set; }
        public string Device_addr { get; set; }
        public DateTime Time { get; set; }
        public string Device { get; set; }
        public string Packet_id { get; set; }
        public string Gateway { get; set; }
        public bool History { get; set; }

        //Modulation data
        public int Bandwidth { get; set; }
        public int Spreading { get; set; }
        public string Coderate { get; set; }

        //GPS data
        public double Lat { get; set; }
        public double Lng { get; set; }
        public double Alt { get; set; }

        //Hardware data
        public int Chain { get; set; }
        public long Tmst { get; set; }
        public double Snr { get; set; }
        public int Rssi { get; set; }
        public int Channel { get; set; }

        //Radio data
        public double Delay { get; set; }
        public int Datarate { get; set; }
        public double Freq { get; set; }
        public int Size { get; set; }

        //Header data
        public int Type { get; set; }
        public int Version { get; set; }

        //Params data
        public double Rx_time { get; set; }
        public int Port { get; set; }
        public bool Duplicate { get; set; }
        public int Counter_up { get; set; }
        public string Encrypted_payload { get; set; }
    }
}