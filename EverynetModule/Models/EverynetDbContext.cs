﻿using System.Data.Entity;

namespace EverynetModule.Models
{
    public class EverynetDbContext : DbContext
    {
        public DbSet<TMoldMessage> TMoldMessages { get; set; }
        public DbSet<TouchTagMessage> TouchTagMessages { get; set; }
        public DbSet<Device> Devices { get; set; }

        public EverynetDbContext() : base("EverynetConnection")
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new EverynetDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TMoldMessage>().Property(x => x.Temperature).HasPrecision(5, 2);
            modelBuilder.Entity<TMoldMessage>().Property(x => x.Battery).HasPrecision(7, 3);

            modelBuilder.Entity<Device>().Property(x => x._Config).HasColumnName("Config");
        }

        public static EverynetDbContext Create()
        {
            return new EverynetDbContext();
        }
    }
}