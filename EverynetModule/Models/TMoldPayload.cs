﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverynetModule.Models
{
    public class TMoldPayload
    {
        public int Counter { get; set; }
        /// <summary>
        /// °C
        /// </summary>
        public decimal Temperature { get; set; }
        /// <summary>
        /// mV
        /// </summary>
        public decimal Battery { get; set; }

        public string Event { get; set; }
        /// <summary>
        /// Bit 14
        /// </summary>
        public bool Reset { get; set; }
        /// <summary>
        /// Bit 15
        /// </summary>
        public bool ClickButton { get; set; }
        /// <summary>
        /// Bit 16
        /// </summary>
        public bool LowBattery { get; set; }
    }
}