﻿using System.Web;

namespace EverynetModule.RequestDBContext.HttpModules
{
    public class PreApplicationStartCode
    {
        public static void Start()
        {
            HttpApplication.RegisterModule(typeof(DBContextModule));
        }
    }
}