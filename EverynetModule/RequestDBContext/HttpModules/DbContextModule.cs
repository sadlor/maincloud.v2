﻿using EverynetModule.Models;
using MainCloudFramework.RequestDBContext.HttpModules;
using System;

namespace EverynetModule.RequestDBContext.HttpModules
{
    public class DBContextModule : BaseDBContextModule
    {
        // Lista dei db context da rendere disponibili
        override protected Type[] DbContextTypesList
        {
            get
            {
                return new Type[]
                {
                    typeof(EverynetDbContext)
                };
            }
        }
    }
}