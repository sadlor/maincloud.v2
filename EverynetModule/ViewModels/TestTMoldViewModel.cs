﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverynetModule.ViewModels
{
    public class TestTMoldViewModel
    {
        public string Device { get; set; }
        public DateTime Time { get; set; }
        public string Gateway { get; set; }
        public int Counter { get; set; }
        public decimal Temperature { get; set; }
        public decimal Battery { get; set; }
        public bool ClickButton { get; set; }
        public bool LowBattery { get; set; }
    }
}