﻿using EverynetModule.Models;
using EverynetModule.ViewModels;
using MainCloudFramework.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace EverynetModule.Repositories
{
    public class TMoldMessageRepository : BaseRepository<TMoldMessage, EverynetDbContext>
    {
        public List<TMoldMessage> GetLastCounter()
        {
            string query = "WITH TT AS (SELECT *, ROW_NUMBER() OVER(PARTITION BY Device ORDER BY Time DESC) AS N " +
                           "FROM TMoldMessages) " +
                           "SELECT * FROM TT WHERE N = 1";
            return DBContext.Database.SqlQuery<TMoldMessage>(query).ToList();
        }

        public List<TestTMoldViewModel> GetMessages(string deviceId)
        {
            return DBContext.TMoldMessages.Where(x => x.Device == deviceId).OrderByDescending(x => x.Time).Select(x =>
            new TestTMoldViewModel()
            {
                Device = x.Device,
                Time = x.Time,
                Gateway = x.Gateway,
                Counter = x.Counter,
                Temperature = x.Temperature,
                Battery = x.Battery,
                ClickButton = x.ClickButton,
                LowBattery = x.LowBattery
            }).Take(100).ToList();
        }
    }
}