﻿using EverynetModule.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverynetModule.Repositories
{
    public class DeviceRepository : BaseRepository<Device, EverynetDbContext>
    {
        public string GetDeviceSN(string deviceId)
        {
            return DBContext.Devices.Where(x => x.Id == deviceId).Select(x => x.SerialNumber).FirstOrDefault();
        }

        public List<Device> GetAll()
        {
            return DBContext.Devices.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).ToList();
        }

        public string GetMoldId(string deviceId)
        {
            return DBContext.Devices.Where(x => x.Id == deviceId).Select(x => x.MoldId).FirstOrDefault();
        }

        public string GetConfig(string deviceId)
        {
            return DBContext.Devices.Where(x => x.Id == deviceId).Select(x => x._Config).FirstOrDefault();
        }
    }
}