﻿using AssetManagement.Models;
using MainCloudFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Repositories
{
    public class MoldRepository : BaseRepository<Mold, AssetManagementDbContext>
    {
        public DateTime? GetLastMaintenance(string moldId)
        {
            return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.LastMaintenanceDate).FirstOrDefault();
        }

        public int GetLastMaintenanceShot(string moldId)
        {
            return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.LastMaintenanceProgressiveCount).FirstOrDefault();
        }

        public int GetWarning1(string moldId)
        {
            return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.Warning1).FirstOrDefault();
        }

        public int GetWarning2(string moldId)
        {
            return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.Warning2).FirstOrDefault();
        }

        public int GetAlarm(string moldId)
        {
            return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.Maximum).FirstOrDefault();
        }

        public string GetName(string moldId)
        {
            return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.Name).FirstOrDefault();
        }

        public string GetImagePath(string moldId)
        {
            return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.PathImg).FirstOrDefault();
        }

        public List<string> GetMoldIdList()
        {
            return DBContext.Molds.Select(x => x.Id).ToList();
        }

        public string GetDeviceId(string moldId)
        {
            return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.DeviceId).FirstOrDefault();
        }

        public string GetMoldByDevice(string deviceId)
        {
            return DBContext.Molds.Where(x => x.DeviceId == deviceId).Select(x => x.Id).FirstOrDefault();
        }

        public int GetCavityNum(string moldId)
        {
            if (!string.IsNullOrEmpty(moldId))
            {
                return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.CavityMoldNum).FirstOrDefault();
            }
            return 1;
        }

        public int GetStopReturn(string moldId)
        {
            return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.StopReturn).FirstOrDefault();
        }

        public int GetStopGone(string moldId)
        {
            return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.StopGone).FirstOrDefault();
        }

        public decimal GetStandardRate(string moldId)
        {
            return DBContext.Molds.Where(x => x.Id == moldId).Select(x => x.ProductionCadency).FirstOrDefault();
        }
    }
}