﻿using AssetManagement.Models;
using MainCloudFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Repositories
{
    public class AssetRepository : BaseRepository<Asset, AssetManagementDbContext>
    {
        public void UpdatePositionAndSize(string id, string posiziontAndSize)
        {
            var item = (from x in DBContext.Assets
                        where x.Id == id
                        select x).First();
            if (item != null)
            {
                item.PositionAndSize = posiziontAndSize;
                DBContext.SaveChanges();
            }
        }

        public string GetDescription(string assetId)
        {
            return DBContext.Assets.Where(x => x.Id == assetId).Select(x => x.Description).FirstOrDefault();
        }

        public string GetCode(string assetId)
        {
            return DBContext.Assets.Where(x => x.Id == assetId).Select(x => x.Code).FirstOrDefault();
        }

        public decimal GetStandardRate(string assetId)
        {
            return DBContext.Assets.Where(x => x.Id == assetId).Select(x => x.StandardRate).FirstOrDefault();
        }

        public string GetAssetGroupId(string assetId)
        {
            return DBContext.Assets.Where(x => x.Id == assetId).Select(x => x.AssetGroupId).FirstOrDefault();
        }

        public string GetPartProgramPath(string assetId)
        {
            return DBContext.Assets.Where(x => x.Id == assetId).Select(x => x.PartProgramPath).FirstOrDefault();
        }
    }
}