﻿using AssetManagement.Models;
using MainCloudFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Repositories
{
    public class AnalyzerRepository : BaseRepository<Analyzer, AssetManagementDbContext>
    {
        public void UpdatePositionAndSize(string id, string posiziontAndSize)
        {
            var item = (from x in DBContext.Analyzers
                        where x.Id == id
                        select x).First();
            if (item != null)
            {
                item.PositionAndSize = posiziontAndSize;
                DBContext.SaveChanges();
            }
        }
    }
}