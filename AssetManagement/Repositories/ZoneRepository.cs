﻿using AssetManagement.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Repositories
{
    public class ZoneRepository : BaseRepository<Zone, AssetManagementDbContext>
    {
        public void InsertNewZone()
        {
            string lastZone = DBContext.Zones.OrderBy(x => x.Code).Max(x => x.Code);
            Zone newZone = new Zone();
            newZone.Code = (Convert.ToInt32(lastZone) + 1).ToString();
            newZone.Description = "Zone" + newZone.Code;
            newZone.ApplicationId = MultiTenantsHelper.ApplicationId;
            base.Insert(newZone);
        }

        public string GetDescription(string zoneId)
        {
            return DBContext.Zones.Where(x => x.Id == zoneId).Select(x => x.Description).FirstOrDefault();
        }
    }
}