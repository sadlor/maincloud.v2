﻿using AssetManagement.Models;
using MainCloudFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AssetManagement.Repositories
{
    public class MaintenanceRepository : BaseRepository<Maintenance, AssetManagementDbContext>
    {
        const string MoldType = "Mold";
        const string AssetType = "Asset";

        public Maintenance FindByMoldId(string moldId)
        {
            try
            {
                return DBContext.Maintenances.Where(x => x.ObjectId == moldId && x.ObjectType == MoldType).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Maintenance FindByAssetId(string assetId)
        {
            try
            {
                return DBContext.Maintenances.Where(x => x.ObjectId == assetId && x.ObjectType == AssetType).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Maintenance> GetAllByMaintenancePlanId(int MaintenancePlanId)
        {
            try
            {
                return DBContext.Maintenances.Where(x => x.MaintenancePlanId == MaintenancePlanId).ToList();
            }
            catch (Exception)
            {
                return new List<Maintenance>();
            }
        }

        public List<Maintenance> GetAllByControlPlanId(int ControlPlanId)
        {
            try
            {
                return DBContext.Maintenances.Where(x => x.ControlPlanId == ControlPlanId).ToList();
            }
            catch (Exception)
            {
                return new List<Maintenance>();
            }
        }
    }
}