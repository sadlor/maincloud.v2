﻿using AssetManagement.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetManagement.Models
{
    public class Maintenance
    {
        public Maintenance() { }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        public string ObjectId { get; set; } // AssetId, MoldId

        public string ObjectType { get; set; } // "Asset" (AssetType), "Mold" (MoldType)

        [NotMapped]
        public string MoldType { get { return "Mold"; } }
        [NotMapped]
        public string AssetType { get { return "Asset"; } }

        public int? ControlPlanId { get; set; } // Piano di controllo
        public int? MaintenancePlanId { get; set; } // Piano di manutenzione

        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }

        //[Index("UniqueClientApplicationIndex", 1, IsUnique = true)]
        public int? ClientId { get; set; }

        //[Index("UniqueClientApplicationIndex", 2, IsUnique = true)]
        [MaxLength(128)]
        public string ApplicationId { get; set; }
    }
}