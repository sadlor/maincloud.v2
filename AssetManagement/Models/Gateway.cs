﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetManagement.Models
{
    public class Gateway
    {
        public Gateway()
        {
            Id = Guid.NewGuid().ToString();
            Analyzer = new List<Analyzer>();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        public string SerialCode { get; set; }

        [Required]
        public string Description { get; set; }

        [InverseProperty("Gateway")]
        public virtual List<Analyzer> Analyzer { get; set; }

        public string ApplicationId { get; set; }
        public string PathImg { get; set; }
        // Map
        public string PositionAndSize { get; set; }

        public string NetworkAddress { get; set; }
        public string DNSName { get; set; }
    }
}