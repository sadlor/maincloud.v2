﻿using AssetManagement.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Models
{
    public class Enermix : BaseAnalyzer
    {
        public string GatewayId { get; set; }

        [ForeignKey("GatewayId")]
        public virtual Gateway Gateway { get; set; }

        public string NetworkAddress { get; set; }

        // Map
        public string PositionAndSize { get; set; }
    }
}