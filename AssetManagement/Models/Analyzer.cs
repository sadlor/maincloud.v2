﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetManagement.Models
{
    public class Analyzer
    {
        public Analyzer()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        public string SerialCode { get; set; }

        [Required]
        public string Description { get; set; }

        public string GatewayId { get; set; }

        [ForeignKey("GatewayId")]
        public virtual Gateway Gateway { get; set; }

        public string ApplicationId { get; set; }
        public string PathImg { get; set; }
        // Map
        public string PositionAndSize { get; set; }

        //public string EnergyCarrier { get; set; }
        //public bool Autoprod { get; set; }
        public DateTime? LastUpdate { get; set; }

        public string NetworkAddress { get; set; }
    }
}