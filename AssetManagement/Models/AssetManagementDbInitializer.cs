﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Models
{
    public class AssetManagementDbInitializer : CreateDatabaseIfNotExists<AssetManagementDbContext>
    {
        protected override void Seed(AssetManagementDbContext context)
        {
            //InitializeDemoData(context, "bc6058d2-4f74-4c62-b023-6ab4132e6f2f", "Demo");
            base.Seed(context);
        }

        //public static void InitializeDemoData(AssetManagementDbContext db, string applicationId)//, string applicationName)
        //{
        //}
    }
}