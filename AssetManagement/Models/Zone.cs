﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Models
{
    public class Zone
    {
        public Zone()
        {
            Id = Guid.NewGuid().ToString();
            Asset = new List<Models.Asset>();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        public string Id { get; set; }

        public string Code { get; set; }
        public string Description { get; set; }
        public string DepartmentId { get; set; }

        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }

        [InverseProperty("Zone")]
        public virtual List<Asset> Asset { get; set; }

        public string ApplicationId { get; set; }
    }
}