﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Models
{
    public class AssetManagementDbContext : DbContext
    {
        public DbSet<Analyzer> Analyzers { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<AssetCategory> AssetCategories { get; set; }
        public DbSet<AssetGroup> AssetGroups { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Plant> Plants { get; set; }
        public DbSet<Gateway> Gateways { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<Mold> Molds { get; set; }
        public DbSet<Maintenance> Maintenances { get; set; }

        public AssetManagementDbContext()
            : base("AssetManagementConnection")
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new AssetManagementDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // I had removed this
                                                /// Rest of on model creating here.
        }

        public static AssetManagementDbContext Create()
        {
            return new AssetManagementDbContext();
        }
    }
}