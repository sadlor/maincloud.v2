﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetManagement.Models
{
    public class Department
    {
        public Department()
        {
            Id = Guid.NewGuid().ToString();
            Asset = new List<Asset>();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        public string Code { get; set; }
        public string Description { get; set; }
        public string PlantId { get; set; }
        public string PathImg { get; set; }
        public string AnalyzerId { get; set; }
        public string ApplicationId { get; set; }

        [ForeignKey("PlantId")]
        public virtual Plant Plant { get; set; }

        [ForeignKey("AnalyzerId")]
        public virtual Analyzer Analyzer { get; set; }

        [InverseProperty("Department")]
        public virtual List<Asset> Asset { get; set; }
    }
}