﻿using AssetManagement.Core;
using AssetManagement.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Models
{
    /// <summary>
    /// Classe di uno stampo
    /// </summary>
    public class Mold : BaseAsset
    {
        public Mold() : base()
        { }

        [Required]
        public string Name { get; set; }

        public string Location { get; set; }  //Ubicazione
        public string HostName { get; set; }
        public string Contact { get; set; }

        /// <summary>
        /// Tempo stampaggio -> tempo unitario
        /// </summary>
        public decimal WorkFrequency { get; set; }

        /// <summary>
        /// Cadenza di produzione (Colpi ora)
        /// </summary>
        public decimal ProductionCadency { get; set; }
        public DateTime? LastMaintenanceDate { get; set; }
        public int LastMaintenanceProgressiveCount { get; set; }
        //public decimal LastMaintenanceProgressiveHour { get; set; }
        public DateTime? NextMaintenanceDate { get; set; }
        public int NextMaintenanceShots { get; set; }
        public int Warning1 { get; set; }
        public int Warning2 { get; set; }
        public int Maximum { get; set; }
        public int ProgressiveCount { get; set; }

        public string MaintenancePlan
        {
            get
            {
                MaintenanceRepository mRep = new MaintenanceRepository();
                string mPlan = mRep.FindByMoldId(Id)?.MaintenancePlanId.ToString();
                return (mPlan != null) ? mPlan : "";
            }
        }

        public string ControlPlan
        {
            get
            {
                MaintenanceRepository mRep = new MaintenanceRepository();
                string cPlan = mRep.FindByMoldId(Id)?.ControlPlanId.ToString();
                return (cPlan != null) ? cPlan : "";
            }
        }

        public string AssetId { get; set; }

        /// <summary>
        /// Numero impronte
        /// </summary>
        public int CavityMoldNum { get; set; }
        public string Status { get; set; }  //es.: Produzione, in manutenzione, ecc...

        [ForeignKey("AssetCategoryId")]
        public virtual AssetCategory AssetCategory { get; set; }

        [ForeignKey("AssetGroupId")]
        public virtual AssetGroup AssetGroup { get; set; }

        /// <summary>
        /// Id sensore
        /// </summary>
        public string DeviceId { get; set; }


        /// <summary>
        /// Macchina preferenziale
        /// </summary>
        public string DefaultAssetId { get; set; }
        /// <summary>
        /// Gruppo macchine preferenziale
        /// </summary>
        public string DefaultAssetGroupId { get; set; }
        /// <summary>
        /// Numero massimo di materie prime con cui può lavorare lo stampo
        /// </summary>
        public int MaxRawMaterialNum { get; set; }
        /// <summary>
        /// Numero fermate in andata
        /// </summary>
        public int StopGone { get; set; } //TODO:rename
        /// <summary>
        /// Numero fermate in ritorno
        /// </summary>
        public int StopReturn { get; set; } //TODO:rename

        [ForeignKey("DefaultAssetId")]
        public virtual Asset DefaultAsset { get; set; }

        [ForeignKey("DefaultAssetGroupId")]
        public virtual AssetGroup DefaultAssetGroup { get; set; }
    }
}