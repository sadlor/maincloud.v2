﻿using AssetManagement.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetManagement.Models
{
    public class Asset : BaseAsset
    {
        public string AnalyzerId { get; set; }
        public DateTime? DtInstallationAnalyzer { get; set; } //i dati vanno letti a partire dalla data di associazione macchina-analizzatore

        public string ZoneId { get; set; }

        /// <summary>
        /// Numero di cicli da contare a vuoto per macchina, prima di far scattare un timeout di fermo produzione
        /// </summary>
        public int NumShotTimeout { get; set; }

        /// <summary>
        /// In Secondi (default: 2 min)
        /// </summary>
        public decimal Timeout { get; set; }

        /// <summary>
        /// Tempo singolo ciclo
        /// </summary>
        public decimal CycleTime { get; set; }
        /// <summary>
        /// Numero di cicli in un ora
        /// </summary>
        public decimal StandardRate { get; set; }

        public string PartProgramPath { get; set; }

        [ForeignKey("ZoneId")]
        public virtual Zone Zone { get; set; }

        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }

        [ForeignKey("AnalyzerId")]
        public virtual Analyzer Analyzer { get; set; }

        [ForeignKey("AssetCategoryId")]
        public virtual AssetCategory AssetCategory { get; set; }

        [ForeignKey("AssetGroupId")]
        public virtual AssetGroup AssetGroup { get; set; }
    }
}