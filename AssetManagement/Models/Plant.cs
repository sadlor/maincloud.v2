﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetManagement.Models
{
    public class Plant
    {
        public Plant()
        {
            Id = Guid.NewGuid().ToString();
            Department = new List<Department>();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        public string Code { get; set; }
        public string Description { get; set; }
        public string AnalyzerId { get; set; }
        public string ApplicationId { get; set; }
        public string PathImg { get; set; }

        public string POD { get; set; }
        public string Voltage { get; set; }
        public string EnablePower { get; set; }

        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        [ForeignKey("AnalyzerId")]
        public virtual Analyzer Analyzer { get; set; }

        [InverseProperty("Plant")]
        public virtual List<Department> Department { get; set; }
    }
}