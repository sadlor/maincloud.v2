﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetManagement.Models
{
    public class AssetGroup
    {
        public AssetGroup()
        {
            Id = Guid.NewGuid().ToString();
        }

        public AssetGroup(string applicationId)
        {
            Id = Guid.NewGuid().ToString();
            ApplicationId = applicationId;
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public string Description { get; set; }

        public string ApplicationId { get; set; }
    }
}