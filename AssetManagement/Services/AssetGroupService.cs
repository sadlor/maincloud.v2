﻿using AssetManagement.Models;
using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Services
{
    public class AssetGroupService : BaseService<AssetManagementDbContext>
    {
        public List<AssetGroup> GroupList()
        {
            List<AssetGroup> groups = (from g in DBContext.AssetGroups
                                       where g.ApplicationId == MultiTenantsHelper.ApplicationId
                                       orderby g.Description
                                       select g).ToList();
            return groups;
        }

        public List<Asset> GetAssetsByGroup(string groupId, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            List<Asset> list = (from a in DBContext.Assets
                                where a.ApplicationId == applicationId && a.AssetGroupId == groupId
                                orderby a.Description
                                select a).ToList();
            return list;
        }

        public string GetDescriptionById(string groupId)
        {
            return DBContext.AssetGroups.Where(x => x.Id == groupId).First().Description;
        }
    }
}