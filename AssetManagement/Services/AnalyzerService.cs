﻿using AssetManagement.Core;
using AssetManagement.Models;
using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Services
{
    public class AnalyzerService : BaseService<AssetManagementDbContext>
    {
        public DateTime? GetLastUpdate(string analyzerId)
        {
            try
            {
                return DBContext.Analyzers.Where(x => x.Id == analyzerId && x.ApplicationId == MultiTenantsHelper.ApplicationId).First().LastUpdate;
            }
            catch (InvalidOperationException ex)
            {
                //non esiste analyzerId
                return null;
            }
        }

        public string GetAnalyzerIdBySerialCode(string serialCode, string applicationId)
        {
            var analyzerId = (from a in DBContext.Analyzers
                              where a.ApplicationId == applicationId && a.SerialCode == serialCode
                              select a.Id).FirstOrDefault();
            return analyzerId;
        }

        public string GetAnalyzerIdByContextId(string contextId, AssetManagementEnum.AnalyzerContext context)
        {
            string analyzerId = null;
            switch (context)
            {
                case AssetManagementEnum.AnalyzerContext.Asset:
                    analyzerId = (from a in DBContext.Assets
                                  where a.Id == contextId //&& a.ApplicationId == MultiTenantsHelper.ApplicationId
                                  select a.AnalyzerId).FirstOrDefault();
                    break;
                case AssetManagementEnum.AnalyzerContext.Department:
                    analyzerId = (from a in DBContext.Departments
                                  where a.Id == contextId //&& a.ApplicationId == MultiTenantsHelper.ApplicationId
                                  select a.AnalyzerId).FirstOrDefault();
                    break;
                case AssetManagementEnum.AnalyzerContext.Plant:
                    analyzerId = (from a in DBContext.Plants
                                  where a.Id == contextId //&& a.ApplicationId == MultiTenantsHelper.ApplicationId
                                  select a.AnalyzerId).FirstOrDefault();
                    break;
            }
            return analyzerId;
        }
    }
}