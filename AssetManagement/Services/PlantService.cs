﻿using AssetManagement.Models;
using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Services
{
    public class PlantService : BaseService<AssetManagementDbContext>
    {
        public List<Plant> PlantList()
        {
            List<Plant> plants = (from p in DBContext.Plants
                                  where p.ApplicationId == MultiTenantsHelper.ApplicationId
                                  orderby p.Description
                                  select p).ToList();
            return plants;
        }

        public string GetDescriptionById(string plantId)
        {
            return DBContext.Plants.Where(x => x.Id == plantId).First().Description;
        }

        public Plant GetPlantById(string id)
        {
            Plant result = (from p in DBContext.Plants
                            where p.ApplicationId == MultiTenantsHelper.ApplicationId && p.Id == id
                            select p).FirstOrDefault();
            return result;
        }

        public decimal GetPlantEnablePower(string id)
        {
            return Convert.ToDecimal(GetPlantById(id).EnablePower, CultureInfo.InvariantCulture);
        }

        public Plant GetPlantByAnalyzerId(string analyzerId)
        {
            Plant result = (from a in DBContext.Plants
                            where a.ApplicationId == MultiTenantsHelper.ApplicationId && a.AnalyzerId == analyzerId
                            select a).FirstOrDefault();
            return result;
        }

        public List<Asset> GetAssetListByPlant(string plantId)
        {
            List<Asset> assetList = (from a in DBContext.Assets
                                     where a.ApplicationId == MultiTenantsHelper.ApplicationId && a.Department.PlantId == plantId
                                     orderby a.Description
                                     select a).ToList();
            return assetList;
        }

        /// <summary>
        /// Restituisce la lista dei consumi mensili del sito, se il sito non ha analizzatore associato, restituisce come valori le somme dei valori degli asset sottostanti
        /// </summary>
        /// <param name="plant"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        //public List<MonthlyConsumption> TotalMonthlyConsumptionPlant(Plant plant, DateTime start, DateTime end)
        //{
        //    if (string.IsNullOrEmpty(plant.AnalyzerId))
        //    {
        //    }
        //    return null;
        //}
    }
}