﻿using AssetManagement.Models;
using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Services
{
    public class DepartmentService : BaseService<AssetManagementDbContext>
    {
        public Department GetDepartmentById(string id)
        {
            Department result = (from d in DBContext.Departments
                                 where d.ApplicationId == MultiTenantsHelper.ApplicationId && d.Id == id
                                 select d).FirstOrDefault();
            return result;
        }

        public Plant GetPlantOwnerByDepartmentAnalyzerId(string analyzerId)
        {
            return GetDepartmentByAnalyzerId(analyzerId).Plant;
        }

        public Department GetDepartmentByAnalyzerId(string analyzerId)
        {
            Department result = (from a in DBContext.Departments
                                 where a.ApplicationId == MultiTenantsHelper.ApplicationId && a.AnalyzerId == analyzerId
                                 select a).FirstOrDefault();
            return result;
        }

        public Plant GetPlantOwner(string departmentId)
        {
            return GetDepartmentById(departmentId).Plant;
        }

        public List<Department> DepartmentList()
        {
            List<Department> departments = (from d in DBContext.Departments
                                            where d.ApplicationId == MultiTenantsHelper.ApplicationId
                                            orderby d.Description
                                            select d).ToList();
            return departments;
        }
    }
}