﻿using AssetManagement.Models;
using AssetManagement.Repositories;
using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Services
{
    public class AssetService : BaseService<AssetManagementDbContext>
    {
        public Asset GetAssedById(string id, string applicationId = null)
        {
            if (string.IsNullOrEmpty(applicationId))
            {
                applicationId = MultiTenantsHelper.ApplicationId;
            }
            Asset result = (from a in DBContext.Assets
                            where a.ApplicationId == applicationId && a.Id == id
                            select a).FirstOrDefault();
            return result;
        }

        public void UpdateAsset(Asset asset)
        {
            if (asset != null)
            {
                DBContext.Entry(asset).State = System.Data.Entity.EntityState.Modified;
            }
            DBContext.SaveChanges();
        }

        public List<Asset> GetAssetsByDepartment(string departmentId)
        {
            List<Asset> list = (from a in DBContext.Assets
                                where a.ApplicationId == MultiTenantsHelper.ApplicationId && a.DepartmentId == departmentId
                                orderby a.Description
                                select a).ToList();
            return list;
        }

        public Plant GetPlantOwner(string assetId)
        {
            return GetAssedById(assetId).Department.Plant;
        }

        public Plant GetPlantOwnerByAssetAnalyzerId(string analyzerId)
        {
            return GetAssetByAnalyzerId(analyzerId).Department.Plant;
        }

        public Asset GetAssetByAnalyzerId(string analyzerId)
        {
            Asset result = (from a in DBContext.Assets
                            where a.ApplicationId == MultiTenantsHelper.ApplicationId && a.AnalyzerId == analyzerId
                            select a).FirstOrDefault();
            return result;
        }

        public List<Asset> GetAssetList()
        {
            //return DBContext.Assets.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Description).ToList();
            return DBContext.Assets.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Code.Length).ThenBy(x => x.Code).ToList();
        }

        public IEnumerable<Asset> GetAssetEntity()
        {
            return DBContext.Assets.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Code.Length).ThenBy(x => x.Code);
        }

        public List<Asset> GetAssetListByZoneId(string zoneId)
        {
            return DBContext.Assets.Where(x => x.ZoneId == zoneId).OrderBy(x => x.Description).ToList();
            //return DBContext.Assets.Where(x => x.ZoneId == zoneId && x.ApplicationId == MultiTenantsHelper.ApplicationId).OrderBy(x => x.Description).ToList();
        }

        public string GetAssetIdByName(string assetName)
        {
            try
            {
                return DBContext.Assets.Where(x => x.ApplicationId == MultiTenantsHelper.ApplicationId && x.Description == assetName).FirstOrDefault().Id;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string GetAssetGroupDescr(string assetId)
        {
            AssetRepository repos = new AssetRepository();
            return repos.FindByID(assetId).AssetGroup?.Description;
        }
    }
}