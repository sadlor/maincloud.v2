namespace AssetManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class foreignkeydefaultasset : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Molds", "DefaultAssetId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Molds", "DefaultAssetGroupId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Molds", "DefaultAssetId");
            CreateIndex("dbo.Molds", "DefaultAssetGroupId");
            AddForeignKey("dbo.Molds", "DefaultAssetId", "dbo.AssetCategories", "Id");
            AddForeignKey("dbo.Molds", "DefaultAssetGroupId", "dbo.AssetGroups", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Molds", "DefaultAssetGroupId", "dbo.AssetGroups");
            DropForeignKey("dbo.Molds", "DefaultAssetId", "dbo.AssetCategories");
            DropIndex("dbo.Molds", new[] { "DefaultAssetGroupId" });
            DropIndex("dbo.Molds", new[] { "DefaultAssetId" });
            AlterColumn("dbo.Molds", "DefaultAssetGroupId", c => c.String());
            AlterColumn("dbo.Molds", "DefaultAssetId", c => c.String());
        }
    }
}
