namespace AssetManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MAINTENANCE : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Maintenances",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ObjectId = c.String(),
                    ObjectType = c.String(),
                    ControlPlanId = c.Int(),
                    MaintenancePlanId = c.Int(),
                    CreationDate = c.DateTime(nullable: false),
                    UpdateDate = c.DateTime(nullable: false),
                    ClientId = c.Int(),
                    ApplicationId = c.String(maxLength: 128),
                })
                .PrimaryKey(t => t.Id);

        }
        
        public override void Down()
        {
            DropTable("dbo.Maintenances");
        }
    }
}
