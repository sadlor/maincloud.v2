namespace AssetManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newmoldprop : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Molds", "DefaultAssetId", c => c.String());
            AddColumn("dbo.Molds", "DefaultAssetGroupId", c => c.String());
            AddColumn("dbo.Molds", "MaxRawMaterialNum", c => c.Int(nullable: false));
            AddColumn("dbo.Molds", "StopGone", c => c.Int(nullable: false));
            AddColumn("dbo.Molds", "StopReturn", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Molds", "StopReturn");
            DropColumn("dbo.Molds", "StopGone");
            DropColumn("dbo.Molds", "MaxRawMaterialNum");
            DropColumn("dbo.Molds", "DefaultAssetGroupId");
            DropColumn("dbo.Molds", "DefaultAssetId");
        }
    }
}
