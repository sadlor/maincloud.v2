namespace AssetManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AssetPartProgramPath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Assets", "PartProgramPath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Assets", "PartProgramPath");
        }
    }
}
