﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Core
{
    public static class AssetManagementEnum
    {
        //tutti gli enumeratori inserirli qua
        public enum AnalyzerContext
        {
            Asset,
            Department,
            Plant
        }
    }

    public enum MoldStatus
    {
        Produzione,
        Manutenzione,
        Disponibile
    }

    public enum AssetType
    {
        Slitter
    }
}