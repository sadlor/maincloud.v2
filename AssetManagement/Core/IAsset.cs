﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Core
{
    interface IAsset
    {
        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        string Id { get; set; }

        [Required]
        string Code { get; set; }

        string Description { get; set; }
        string DescrExtended { get; set; }
        string IdNumber { get; set; }
        string Brand { get; set; }
        string Model { get; set; }
        string Builder { get; set; }
        DateTime DtInstallation { get; set; }
        DateTime? DtEndGuarantee { get; set; }
        string PathImg { get; set; }

        // Map
        string PositionAndSize { get; set; }

        string AssetCategoryId { get; set; }
        string AssetGroupId { get; set; }

        string ApplicationId { get; set; }
    }
}