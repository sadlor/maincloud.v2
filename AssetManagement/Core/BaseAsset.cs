﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Core
{
    public abstract class BaseAsset : IAsset
    {
        public BaseAsset()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public string Description { get; set; }

        public string DescrExtended { get; set; }
        public string IdNumber { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Builder { get; set; }
        public DateTime DtInstallation { get; set; }
        public DateTime? DtEndGuarantee { get; set; }
        public string PathImg { get; set; }
        public string DepartmentId { get; set; }
        public string ApplicationId { get; set; }
        public string AssetCategoryId { get; set; }
        public string AssetGroupId { get; set; }
        // Map
        public string PositionAndSize { get; set; }
        //public string ZoneId { get; set; }
        //public int NumShotTimeout { get; set; } //numero di cicli da contare a vuoto per macchina, prima di far scattare un timeout di fermo produzione
        //public decimal Timeout { get; set; } //secondi   default: 2 min

        //[ForeignKey("ZoneId")]
        //public virtual Zone Zone { get; set; }

        //[ForeignKey("DepartmentId")]
        //public virtual Department Department { get; set; }

        ////[ForeignKey("AnalyzerId")]
        ////public virtual Analyzer Analyzer { get; set; }

        //[ForeignKey("AssetCategoryId")]
        //public virtual AssetCategory AssetCategory { get; set; }

        //[ForeignKey("AssetGroupId")]
        //public virtual AssetGroup AssetGroup { get; set; }
    }
}