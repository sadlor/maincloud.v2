﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Driver
{
    public abstract class BaseGateway
    {
        public BaseGateway()
        {
            Id = Guid.NewGuid().ToString();
            //Analyzer = new List<Analyzer>();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        public string SerialCode { get; set; }

        [Required]
        public string Description { get; set; }

        public string ApplicationId { get; set; }
        public string PathImg { get; set; }

        //[InverseProperty("Gateway")]
        //public virtual List<Analyzer> Analyzer { get; set; }
    }
}