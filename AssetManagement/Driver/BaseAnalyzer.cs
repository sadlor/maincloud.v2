﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagement.Driver
{
    public abstract class BaseAnalyzer : IAnalyzer
    {
        public BaseAnalyzer()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        public string SerialCode { get; set; }

        [Required]
        public string Description { get; set; }
                
        public string ApplicationId { get; set; }
        public DateTime? LastUpdate { get; set; }
        public string PathImg { get; set; }
    }
}