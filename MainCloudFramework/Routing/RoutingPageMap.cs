﻿using System;
using System.Web.Routing;
using MainCloudFramework.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using System.Web;
using System.Web.Security;
using System.Net;
using System.Web.Compilation;
using System.Web.UI;
using System.Linq;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web;
using MainCloudFramework.Services;

namespace MainCloudFramework.Rooting
{
    /// <summary>
    /// Routing Page Map inizializza la tabella di routing per le pagine
    /// </summary>
    public class RoutingPageMap
    {
        public RoutingPageMap()
        {
        }

        public static void RegisterAreaRoutes(RouteCollection routes)
        {
            // Implemets multitenant routing
            using (ApplicationDbContext db = new ApplicationDbContext())
            {

            }

            //using (ApplicationDbContext db = new ApplicationDbContext())
            //{
            //    foreach (WidgetArea page in db.WidgetAreaSet)
            //    {
            //        //String pageRoutId;
            //        //if (page.WidgetAreaParent != null)
            //        //{
            //        //    // SOLO UN LIVELLO DI PARENTELA
            //        //    pageRoutId = String.Format("Area/{0}/{1}", page.WidgetAreaParent.RoutePath, page.RoutePath);
            //        //}
            //        //else
            //        //{
            //        //    pageRoutId = String.Format("Area/{0}", page.RoutePath);
            //        //}
            //        //routes.MapPageRoute(pageRoutId, pageRoutId, "~/Dashboard/DefaultPage.aspx", true);
            //    }
            //}


            RouteValueDictionary defaultTenant = new RouteValueDictionary();
            defaultTenant.Add("tenant", MCFConstants.ROLE_HOST);

            routes.MapPageRoute("Default", "", "~/Default.aspx", true);

            routes.MapPageRoute("Login", "Login", "~/Account/Login.aspx", true);

            // START HOST RULES
            routes.MapPageRoute("HostManageSetting", MultiTenantsHelper.MULTI_TENANT_ROUTE_HOST_BASE_URL + "/ManageSetting", "~/Host/ManageSetting.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageUser", MultiTenantsHelper.MULTI_TENANT_ROUTE_HOST_BASE_URL + "/ManageUser", "~/Host/ManageUser.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageUsers", MultiTenantsHelper.MULTI_TENANT_ROUTE_HOST_BASE_URL + "/ManageUsers", "~/Host/ManageUsers.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageRegisterUser", MultiTenantsHelper.MULTI_TENANT_ROUTE_HOST_BASE_URL + "/ManageUserRegister", "~/Host/ManageUserRegister.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageAddUser", MultiTenantsHelper.MULTI_TENANT_ROUTE_HOST_BASE_URL + "/ManageUserAdd", "~/Host/ManageUserAdd.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageUserPassword", MultiTenantsHelper.MULTI_TENANT_ROUTE_HOST_BASE_URL + "/ManageUserPassword", "~/Host/ManagePassword.aspx", true, defaultTenant);
            //routes.MapPageRoute("HostManageRoles", MultiTenantsHelper.MULTI_TENANT_ROUTE_HOST_BASE_URL + "/ManageRoles", "~/Host/ManageRoles.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageWidgets", MultiTenantsHelper.MULTI_TENANT_ROUTE_HOST_BASE_URL + "/ManageWidgets", "~/Host/ManageWidgets.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageLogbooks", MultiTenantsHelper.MULTI_TENANT_ROUTE_HOST_BASE_URL + "/ManageLogbooks", "~/Host/ManageLogbooks.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageAlarms", MultiTenantsHelper.MULTI_TENANT_ROUTE_HOST_BASE_URL + "/ManageAlarms", "~/Host/ManageAlarms.aspx", true, defaultTenant);


            routes.MapPageRoute("HostManageApplications2", "ManageApplications", "~/Host/ManageApplications.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageSetting2", "Host/ManageSetting", "~/Host/ManageSetting.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageUser2", "Host/ManageUser", "~/Host/ManageUser.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageUsers2", "Host/ManageUsers", "~/Host/ManageUsers.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageRegisterUser2", "Host/ManageUserRegister", "~/Host/ManageUserRegister.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageAddUser2", "Host/ManageUserAdd", "~/Host/ManageUserAdd.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageUserPassword2", "Host/ManageUserPassword", "~/Host/ManagePassword.aspx", true, defaultTenant);
            //routes.MapPageRoute("HostManageRoles2", "Host/ManageRoles", "~/Host/ManageRoles.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageWidgets2", "Host/ManageWidgets", "~/Host/ManageWidgets.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageLogbooks2", "Host/ManageLogbooks", "~/Host/ManageLogbooks.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageAlarms2", "Host/ManageAlarms", "~/Host/ManageAlarms.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageCustomers", "Host/ManageCustomers", "~/Host/ManageCustomers.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageAgents", "Host/ManageAgents", "~/Host/ManageAgents.aspx", true, defaultTenant);
            routes.MapPageRoute("HostManageResellers", "Host/ManageResellers", "~/Host/ManageResellers.aspx", true, defaultTenant);

            //routes.MapPageRoute("ManageTranslation", "Host/ManageTranslation", "~/LocalizationAdmin/index.aspx", true, defaultTenant);

            // END HOST RULES

            // START ADMIN RULES
            routes.MapPageRoute("AdminManageUser", MultiTenantsHelper.MULTI_TENANT_ROUTE_ADMIN_BASE_URL + "/ManageUser", "~/ApplicationAdmin/ManageUser.aspx", true, defaultTenant);
            routes.MapPageRoute("AdminManageUsers", MultiTenantsHelper.MULTI_TENANT_ROUTE_ADMIN_BASE_URL + "/ManageUsers", "~/ApplicationAdmin/ManageUsers.aspx", true, defaultTenant);
            routes.MapPageRoute("AdminManageRegisterUser", MultiTenantsHelper.MULTI_TENANT_ROUTE_ADMIN_BASE_URL + "/ManageUserRegister", "~/ApplicationAdmin/ManageUserRegister.aspx", true, defaultTenant);
            routes.MapPageRoute("AdminManageAddUser", MultiTenantsHelper.MULTI_TENANT_ROUTE_ADMIN_BASE_URL + "/ManageUserAdd", "~/ApplicationAdmin/ManageUserAdd.aspx", true, defaultTenant);
            routes.MapPageRoute("AdminManageUserPassword", MultiTenantsHelper.MULTI_TENANT_ROUTE_ADMIN_BASE_URL + "/ManageUserPassword", "~/ApplicationAdmin/ManagePassword.aspx", true, defaultTenant);
            routes.MapPageRoute("AdminManageRoles", MultiTenantsHelper.MULTI_TENANT_ROUTE_ADMIN_BASE_URL + "/ManageRoles", "~/ApplicationAdmin/ManageRoles.aspx", true, defaultTenant);
            routes.MapPageRoute("AdminManageRole", MultiTenantsHelper.MULTI_TENANT_ROUTE_ADMIN_BASE_URL + "/ManageRole", "~/ApplicationAdmin/ManageRole.aspx", true, defaultTenant);
            // END ADMIN RULES

            routes.Add("ListApplications", new Route("ListApplications", new ClientRoute("~/Tenants/ListApplications.aspx", true)));

            routes.Add("Area", new Route(MultiTenantsHelper.MULTI_TENANT_ROUTE_BASE_URL + "/Area/{*name}", new ClientRoute("~/Dashboard/DefaultPage.aspx", true)));
            routes.Add("EditWidget", new Route(MultiTenantsHelper.MULTI_TENANT_ROUTE_BASE_URL + "/Edit/{*name}", new ClientRoute("~/Dashboard/PageEdit.aspx", true)));
            routes.Add("DashboardAreas", new Route(MultiTenantsHelper.MULTI_TENANT_ROUTE_BASE_URL + "/Dashboard/Areas", new ClientRoute("~/Dashboard/DashboardAreas.aspx", true)));
            routes.Add("DefaultTenant", new Route(MultiTenantsHelper.MULTI_TENANT_ROUTE_BASE_URL, new ClientRoute("~/Dashboard/DashboardAreas.aspx", true)));
            //??? Duplicata??? routes.Add("AppDefault", new Route(MultiTenantsHelper.MULTI_TENANT_ROUTE_BASE_URL + "/Area/{*name}", new ClientRoute("~/Dashboard/DefaultPage.aspx", true)));

            //-- NON VA!! routes.MapPageRoute("WebServiceAjax", MultiTenantsHelper.MULTI_TENANT_ROUTE_BASE_URL + "/Modules/RealPower", "~/Dashboard/Modules/RealPower/EnergyMeter.svc/GetValue", true);

            routes.RouteExistingFiles = true;
        }
    }

    //public class RedirectRouterHandler : IRouteHandler
    //{
    //    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    //    {
    //        string userId = requestContext.HttpContext.User.Identity.GetUserId();
    //        string redirect = "Login";

    //        if (!(string.IsNullOrEmpty(userId) || string.IsNullOrWhiteSpace(userId)))
    //        {
    //            try
    //            {
    //                string applicationTenantName = (string)requestContext.RouteData.Values["tenant"];

    //                using (ApplicationDbContext db = new ApplicationDbContext())
    //                {
    //                    var isInApplicationTenant = (from apps in db.AspNetUserApplications
    //                                                 where apps.UserId == userId && apps.AspNetApplication.Name == applicationTenantName
    //                                                 select apps).Any();
    //                    if (isInApplicationTenant)
    //                    {
    //                        return null;
    //                    }
    //                    else
    //                    {
    //                        var applicationsCount = from apps in db.AspNetUserApplications
    //                                                where apps.UserId == userId
    //                                                select apps;
    //                        //if (applications.Count() == 1)
    //                        //{
    //                        //    redirect = "{tenant}/Dashboard/Areas".Replace("{tenant}", applications.First().AspNetApplication.Name);
    //                        //}
    //                        //else if (applications.Count() > 0)
    //                        if (applicationsCount.Count() > 0)
    //                        {
    //                            redirect = "ListApplications";
    //                        }
    //                    }
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                redirect = "About";
    //            }
    //        }

    //        requestContext.HttpContext.Response.Redirect(redirect);
    //        requestContext.HttpContext.Response.End();
    //        return null;
    //    }
                            //}

    public class ClientRoute : PageRouteHandler
    {
        private ApplicationService applicationService = new ApplicationService();

        //public bool IsReusable
        //{
        //    get { return true; }
        //}

        public bool checkMultiTenantsApplicationAccess { get; set; }

        public ClientRoute(string virtualPath)
            : this(virtualPath, true, true)
        {
        }

        public ClientRoute(string virtualPath, bool checkPhysicalUrlAccess)
            : this(virtualPath, checkPhysicalUrlAccess, true)
        {
        }

        private ClientRoute(string virtualPath, bool checkMultiTenantsApplicationAccess, bool checkPhysicalUrlAccess)
            : base(virtualPath, checkPhysicalUrlAccess)
        {
            this.checkMultiTenantsApplicationAccess = checkMultiTenantsApplicationAccess;
        }

        public override IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            string userId = requestContext.HttpContext.User.Identity.GetUserId();

            string applicationTenantName = (string)requestContext.RouteData.Values["tenant"];
            HttpContext.Current.Items.Add("tenant", applicationTenantName);

            // If not logged in redirect to login page
            if (string.IsNullOrEmpty(userId) || string.IsNullOrWhiteSpace(userId))
            {
                requestContext.HttpContext.Response.Redirect("/Login");
                requestContext.HttpContext.Response.End();
                return null;
            }

            bool isHost = requestContext.HttpContext.User.IsInRole(MCFConstants.ROLE_HOST);
            string url = ((Route)requestContext.RouteData.Route).Url;

            if (!isHost)
            {
                if (url.StartsWith(MultiTenantsHelper.MULTI_TENANT_ROUTE_BASE_URL, StringComparison.InvariantCultureIgnoreCase))
                {
                    // Verifica se l'utenza ha diritto ad accedere all'applicazione specificata nel campo tenant dell'url
                    bool isInApplicationTenant = applicationService.IsUserInApplication(userId, applicationTenantName);
                    if (!isInApplicationTenant)
                    {
                        requestContext.HttpContext.Response.Redirect("/Default");
                        requestContext.HttpContext.Response.End();
                        return null;
                    }
                }
            }
    
            //if (shouldValidate && !UrlAuthorizationModule.CheckUrlAccessForPrincipal(virtualPath, requestContext.HttpContext.User, requestContext.HttpContext.Request.HttpMethod))
            //{
            //    requestContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            //    requestContext.HttpContext.Response.End();
            //    return null;
            //}

            //string virtualPath = GetSubstitutedVirtualPath(requestContext);
            //HttpContext.Current.RewritePath(virtualPath);

            //Page page = base.GetHttpHandler(requestContext) as Page;
           //return page;
            return base.GetHttpHandler(requestContext);
        }

    }
}