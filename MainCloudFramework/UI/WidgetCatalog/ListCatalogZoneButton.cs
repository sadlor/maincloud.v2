using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.UI.WidgetCatalog
{
	public class ListCatalogZoneButton : Button
	{
		WebZone _owner = null;
		string _eventArgument = null;

		public override bool UseSubmitBehavior
		{
			get
			{
				return false;
			}
			set
			{
				throw new InvalidOperationException();
			}
		}

        public ListCatalogZoneButton(WebZone owner, string eventArgument)
		{
			_owner = owner;
			_eventArgument = eventArgument;
		}

		protected override PostBackOptions GetPostBackOptions()
		{
			PostBackOptions options = null;
			if (_owner != null && !String.IsNullOrEmpty(_eventArgument))
			{
				options = new PostBackOptions(_owner, _eventArgument);
				options.ClientSubmit = true;
			}
			else
			{
				options = base.GetPostBackOptions();
			}
			return options;
		}
	}
}
