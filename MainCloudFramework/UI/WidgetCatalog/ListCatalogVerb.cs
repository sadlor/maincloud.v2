using System;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.UI.WidgetCatalog
{
	public abstract class ListCatalogVerb : WebPartVerb
	{
		string _eventArgument = null;

		public string EventArgument
		{
			get { return _eventArgument; }
		}

		public ListCatalogVerb(string id) 
			: base(id, new WebPartEventHandler(OnDoNothing))
		{
			_eventArgument = id;
		}

		static void OnDoNothing(object sender, WebPartEventArgs e)
		{
		}
	}

	public class ModuleCatalogAddVerb : ListCatalogVerb
	{
		string DefaultText
		{
			get { return "Add"; }
		}

		string DefaultDescription
		{
			get { return "Adds the selected module to the selected zone."; }
		}

		public override string Text
		{
			get
			{
				string text = (String)ViewState["Text"];
				if (text != null)
				{
					return text;
				}
				return DefaultText;
			}
			set
			{
				ViewState["Text"] = value;
			}
		}

		public override string Description
		{
			get
			{
				string description = (String)ViewState["Description"];
				if (description != null)
				{
					return description;
				}
				return DefaultDescription;
			}
			set
			{
				ViewState["Description"] = value;
			}
		}

		public ModuleCatalogAddVerb()
			: base("Add")
		{
		}

	}

	public class ModuleCatalogCloseVerb : ListCatalogVerb
	{
		string DefaultText
		{
			get { return "Close"; }
		}

		string DefaultDescription
		{
			get { return "Closes the module catalog zone."; }
		}

		public override string Text
		{
			get
			{
				string text = (String)ViewState["Text"];
				if (text != null)
				{
					return text;
				}
				return DefaultText;
			}
			set
			{
				ViewState["Text"] = value;
			}
		}

		public override string Description
		{
			get
			{
				string description = (String)ViewState["Description"];
				if (description != null)
				{
					return description;
				}
				return DefaultDescription;
			}
			set
			{
				ViewState["Description"] = value;
			}
		}

		public ModuleCatalogCloseVerb()
			: base("Close")
		{
		}

	}
}
