﻿using System.Linq;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using MainCloudFramework.UI.Containers;
using MainCloudFramework.Models;
using MainCloudFramework.Web.Helpers;

namespace MainCloudFramework.UI.WidgetCatalog
{
    /// <summary>
    /// Summary description for WidgetsCatalog
    /// </summary>
    public class ListWidgetsCatalog : CatalogPart
    {
        /// <summary>
        /// Overrides the Title to display Modules Catalog Part by default
        /// </summary>
        public override string Title
        {
            get
            {
                string title = base.Title;
                return string.IsNullOrEmpty(title) ? "Widgets disponibili" : title;
            }
            set
            {
                base.Title = value;
            }
        }

        /// <summary>
        /// Creates a new instance of the class
        /// </summary>
        public ListWidgetsCatalog()
        {

        }

        /// <summary>
        /// Returns the WebPartDescriptions
        /// </summary>
        public override WebPartDescriptionCollection GetAvailableWebPartDescriptions()
        {
            return GetAvailableWebPartDescriptions(null);
        }

        public WebPartDescriptionCollection GetAvailableWebPartDescriptions(string category)
        {
            if (this.DesignMode)
            {
                return new WebPartDescriptionCollection(new object[] {
                    new WebPartDescription("1", "Widget 1", null, null),
                    new WebPartDescription("2", "Widget 2", null, null),
                        new WebPartDescription("3", "Widget 3", null, null)});
            }

            List<WebPartDescription> list = new List<WebPartDescription>();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                List<Widget> widgets = null;
                if (string.IsNullOrEmpty(category))
                {
                    widgets = db.WidgetSet.ToList();
                }
                else
                {
                    widgets = db.WidgetSet.Where(x => x.Category == category).ToList();
                }

                foreach (Widget module in widgets)
                {
                    //if (ApplicationSettingsHelper.IsModuleEnable(module.Category))
                    //{
                        list.Add(new WidgetWebPartDescription(module.WidgetId, module.Name, module.Description, module.ImageUrl, module.Icon, module.Category));
                    //}
                }
            }
            return new WebPartDescriptionCollection(list);
        }

        /// <summary>
        /// Returns a new instance of the WebPart specified by the description
        /// </summary>
        public override WebPart GetWebPart(WebPartDescription description)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                if (description != null)
                {
                    var widgetData = db.WidgetSet.Where(m => m.WidgetId == description.ID).SingleOrDefault();
                    if (widgetData != null)
                    {
                        WidgetWebPart wp = new WidgetWebPart(widgetData);
                        return wp;
                    }
                }
            }
            return null;
        }

    
    }
}
