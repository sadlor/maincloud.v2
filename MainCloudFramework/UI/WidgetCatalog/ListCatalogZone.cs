using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using System.Collections.Generic;

namespace MainCloudFramework.UI.WidgetCatalog
{
	public class ListCatalogZone : CatalogZone
	{
		string _selectedPart = null;
		string _selectedZone = null;
        string _selectedCategory = null; //TODO: ripristinare null
        //string _selectedCategory = null;
        ListCatalogVerb _addVerb = null;
		ListCatalogVerb _closeVerb = null;

        //
        // Summary:
        //     Ottiene o imposta l'oggetto System.Web.UI.ITemplate che consente di definire
        //     la modalit� di visualizzazione della sezione pi� di pagina del controllo System.Web.UI.WebControls.Repeater.
        //
        // Returns:
        //     Oggetto System.Web.UI.ITemplate che consente di definire la modalit� di visualizzazione
        //     della sezione pi� di pagina del controllo System.Web.UI.WebControls.Repeater.Il
        //     valore predefinito � null.
        [Browsable(false)]
        [DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(RepeaterItem))]
        public virtual ITemplate WidgetsCatalogFooterTemplate { get; set; }
        
        //
        // Summary:
        //     Ottiene o imposta l'oggetto System.Web.UI.ITemplate che consente di definire
        //     la modalit� di visualizzazione della sezione intestazione del controllo System.Web.UI.WebControls.Repeater.
        //
        // Returns:
        //     Oggetto System.Web.UI.ITemplate che consente di definire la modalit� di visualizzazione
        //     della sezione intestazione del controllo System.Web.UI.WebControls.Repeater.Il
        //     valore predefinito � null.
        [Browsable(false)]
        [DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(RepeaterItem))]
        public virtual ITemplate WidgetsCatalogHeaderTemplate { get; set; }

        //
        // Summary:
        //     Ottiene o imposta l'oggetto System.Web.UI.ITemplate che consente di definire
        //     la modalit� di visualizzazione degli elementi nel controllo System.Web.UI.WebControls.Repeater.
        //
        // Returns:
        //     Oggetto System.Web.UI.ITemplate che consente di definire le modalit� di visualizzazione
        //     di elementi nel controllo System.Web.UI.WebControls.Repeater.Il valore predefinito
        //     � null.
        [Browsable(false)]
        [DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(RepeaterItem))]
        public virtual ITemplate WidgetsCatalogItemTemplate { get; set; }

        string ZoneSelectorControlID
		{
			get { return string.Concat(UniqueID, IdSeparator, "zones"); }
		}

		public string ModuleSelectorControlID
		{
			get { return string.Concat(UniqueID, IdSeparator, "widgets"); }
		}

        public string ModuleCategorySelectorControlID
        {
            get { return string.Concat(UniqueID, IdSeparator, "widgets_category"); }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }

        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            //writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
            //writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
            //writer.AddAttribute(HtmlTextWriterAttribute.Border, "0");

            // On Mac IE, if height is not set, render height:1px, so the table sizes to contents.
            // Otherwise, Mac IE may give the table an arbitrary height (equal to the width of its contents). 
            //if (!DesignMode &&
            //    Page != null &&
            //    Page.Request.Browser.Type == "IE5" &&
            //    Page.Request.Browser.Platform == "MacPPC" &&
            //    (!ControlStyleCreated || ControlStyle.Height == Unit.Empty))
            //{
            //    writer.AddStyleAttribute(HtmlTextWriterStyle.Height, "1px");
            //}

            // Render <table> 
            base.RenderBeginTag(writer);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (HasHeader)
            {
                //writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                //TitleStyle headerStyle = HeaderStyle;
                //if (!headerStyle.IsEmpty)
                //{
                //    headerStyle.AddAttributesToRender(writer, this);
                //}
                //writer.RenderBeginTag(HtmlTextWriterTag.Td);
                RenderHeader(writer);
                //writer.RenderEndTag();  // Td 
                //writer.RenderEndTag();  // Tr
            }

            //writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            //// We want the body to fill the height of the zone, and squish the header and footer
            //// to the size of their contents
            //// Mac IE needs height=100% set on <td> instead of <tr>
            //writer.AddStyleAttribute(HtmlTextWriterStyle.Height, "100%");

            //writer.RenderBeginTag(HtmlTextWriterTag.Td);
            RenderBody(writer);
            //writer.RenderEndTag();  // Td
            //writer.RenderEndTag();  // Tr 

            if (HasFooter)
            {
                //writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                //TitleStyle footerStyle = FooterStyle;
                //if (!footerStyle.IsEmpty)
                //{
                //    footerStyle.AddAttributesToRender(writer, this);
                //}
                //writer.RenderBeginTag(HtmlTextWriterTag.Td);
                RenderFooter(writer);
                //writer.RenderEndTag();  // Td
                //writer.RenderEndTag();  // Tr
            }
        }

        public override WebPartVerb AddVerb
		{
			get 
			{
				if (_addVerb == null)
				{
					_addVerb = new ModuleCatalogAddVerb();
					if (IsTrackingViewState)
					{
						((IStateManager)_addVerb).TrackViewState();
					}
				}
				return _addVerb;
			}
		}

		public override WebPartVerb CloseVerb
		{
			get
			{
				if (_closeVerb == null)
				{
					_closeVerb = new ModuleCatalogCloseVerb();
					if (IsTrackingViewState)
					{
						((IStateManager)_closeVerb).TrackViewState();
					}
				}
				return _closeVerb;
			}
		}
        
        CatalogPart SelectedCatalogPart
        {
            get
            {
                CatalogPart part = null;
                CatalogPartCollection catalogParts = CatalogParts;
                if ((catalogParts != null) && (catalogParts.Count > 0))
                {
                    if (string.IsNullOrEmpty(SelectedCatalogPartID))
                    {
                        part = catalogParts[0];
                    }
                    else
                    {
                        part = catalogParts[SelectedCatalogPartID];
                    }
                }
                return part;
            }
        }

        protected override CatalogPartChrome CreateCatalogPartChrome()
		{
			return new ListCatalogPartChrome(this, WidgetsCatalogHeaderTemplate, WidgetsCatalogItemTemplate, WidgetsCatalogFooterTemplate);
		}

		protected override void RenderHeader(HtmlTextWriter writer)
		{
            writer.AddStyleAttribute(HtmlTextWriterStyle.Margin, "4px");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            CatalogPartCollection catalogParts = CatalogParts;
            // solo in caso sia un solo catalog
            if (catalogParts.Count == 1)
            {
                HashSet<string> cat = new HashSet<string>();
                foreach (WidgetWebPartDescription catalogPart in catalogParts[0].GetAvailableWebPartDescriptions())
                {
                    if (!string.IsNullOrEmpty(catalogPart.Category))
                    {
                        cat.Add(catalogPart.Category);
                    }
                }

                if (cat.Count > 0)
                {
                    DropDownList category = new DropDownList();
                    category.Items.Add(new ListItem("Tutti", ""));

                    foreach (string c in cat)
                    {
                        ListItem listItem = new ListItem(c, c);
                        if (string.Equals(c, _selectedCategory, StringComparison.InvariantCultureIgnoreCase))
                        {
                            listItem.Selected = true;
                        }
                        category.Items.Add(listItem);
                    }

                    LabelStyle.AddAttributesToRender(writer, this);
                    writer.AddAttribute(HtmlTextWriterAttribute.For, category.ClientID);
                    writer.RenderBeginTag(HtmlTextWriterTag.Label);
                    writer.Write("Categorie:&nbsp;");
                    writer.RenderEndTag();

                    category.ID = ModuleCategorySelectorControlID;
                    category.Width = new Unit(125, UnitType.Pixel);
                    category.ApplyStyle(base.EditUIStyle);
                    category.AutoPostBack = true;
                    category.Page = Page;
                    category.RenderControl(writer);
                    writer.Write("&nbsp;&nbsp;");
                }
            }

            DropDownList list = new DropDownList();
            list.ID = ZoneSelectorControlID;
            list.Width = new Unit(125, UnitType.Pixel);
            //list.CssClass = "form-control";
            WebPartZoneCollection zones = WebPartManager.Zones;
            if (zones != null)
            {
                foreach (WebPartZoneBase zone in zones)
                {
                    if (zone.AllowLayoutChange)
                    {
                        ListItem listItem = new ListItem(
                            zone.DisplayTitle, zone.ID);
                        if (String.Equals(zone.ID, _selectedZone,
                            StringComparison.InvariantCultureIgnoreCase))
                        {
                            listItem.Selected = true;
                        }
                        list.Items.Add(listItem);
                    }
                }
            }
            LabelStyle.AddAttributesToRender(writer, this);
            writer.AddAttribute(HtmlTextWriterAttribute.For, list.ClientID);
            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            writer.Write(this.SelectTargetZoneText);
            writer.RenderEndTag();
            writer.Write("&nbsp;");
            list.ApplyStyle(base.EditUIStyle);
            list.RenderControl(writer);
            writer.Write("&nbsp;");
            RenderVerbs(writer);

            writer.RenderEndTag();
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            base.AddAttributesToRender(writer);
        }

        protected override void RenderBody(HtmlTextWriter writer)
        {
            //RenderBodyTableBeginTag(writer);
            if (DesignMode)
            {
                //RenderDesignerRegionBeginTag(writer, Orientation.Vertical);
            }

            CatalogPartCollection catalogParts = CatalogParts;
            if (catalogParts != null && catalogParts.Count > 0)
            {
                bool firstCell = true;
                // Only render links if there is more than 1 catalog part (VSWhidbey 77672)
                if (catalogParts.Count > 1)
                {
                    //writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    //writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    firstCell = false;
                    RenderCatalogPartLinks(writer);
                    //writer.RenderEndTag();  // Td 
                    //writer.RenderEndTag();  // Tr
                }

                CatalogPartChrome chrome = CatalogPartChrome;
                if (DesignMode)
                {
                    foreach (CatalogPart catalogPart in catalogParts)
                    {
                        RenderCatalogPart(writer, catalogPart, chrome, ref firstCell);
                    }
                }
                else
                {
                    CatalogPart selectedCatalogPart = SelectedCatalogPart;
                    if (selectedCatalogPart != null)
                    {
                        RenderCatalogPart(writer, selectedCatalogPart, chrome, ref firstCell);
                    }
                }

                //writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                //// Mozilla renders padding on an empty TD without this attribute
                //writer.AddStyleAttribute(HtmlTextWriterStyle.Padding, "0");

                //// Add an extra row with height of 100%, to [....] up any extra space
                //// if the height of the zone is larger than its contents
                //// Mac IE needs height=100% set on <td> instead of <tr> 
                //writer.AddStyleAttribute(HtmlTextWriterStyle.Height, "100%");

                //writer.RenderBeginTag(HtmlTextWriterTag.Td);
                //writer.RenderEndTag(); // Td
                //writer.RenderEndTag(); // Tr 
            }
            else
            {
                //RenderEmptyZoneText(writer);
            }

            if (DesignMode)
            {
                //RenderDesignerRegionEndTag(writer);
            }
            //RenderBodyTableEndTag(writer);
        }

        private void RenderCatalogPart(HtmlTextWriter writer, CatalogPart catalogPart, CatalogPartChrome chrome, ref bool firstCell)
        {
            //writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            //if (!firstCell)
            //{
            //    writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingTop, "0");
            //}
            //writer.RenderBeginTag(HtmlTextWriterTag.Td);

            firstCell = false;
            ListCatalogPartChrome widgetChrome = chrome as ListCatalogPartChrome;
            if (widgetChrome != null)
            {
                widgetChrome.RenderCatalogPart(writer, catalogPart, _selectedCategory);
            }
            else
            {
                chrome.RenderCatalogPart(writer, catalogPart);
            }

            //writer.RenderEndTag();  // Td
            //writer.RenderEndTag();  // Tr 
        }

        protected override void RenderFooter(HtmlTextWriter writer)
        {

        }

		protected override void RenderVerbs(HtmlTextWriter writer)
		{
			int moduleCount = 0;
			bool moduleEnabled = false;
			WebPartDescriptionCollection modules = SelectedCatalogPart.GetAvailableWebPartDescriptions();
			moduleCount = (modules != null) ? modules.Count : 0;
			if (moduleCount == 0 || WebPartManager.Zones.Count == 0)
			{
				moduleEnabled = this.AddVerb.Enabled;
				this.AddVerb.Enabled = false;
			}
			try
			{
				RenderVerb(writer, AddVerb);
				writer.Write("&nbsp;");
				RenderVerb(writer, CloseVerb);
			}
			finally
			{
				if (moduleCount == 0 || WebPartManager.Zones.Count == 0)
				{
					this.AddVerb.Enabled = moduleEnabled;
				}
			}
		}

		protected override void RenderVerb(HtmlTextWriter writer, WebPartVerb verb)
		{
			string eventArgument = ((ListCatalogVerb)verb).EventArgument;

			ListCatalogZoneButton button = new ListCatalogZoneButton(this, eventArgument);
			button.Text = verb.Text;
			button.ApplyStyle(base.VerbStyle);
			button.ToolTip = verb.Description;
			button.Enabled = verb.Enabled;
			button.Page = this.Page;
			button.RenderControl(writer);
		}

        protected override void RaisePostBackEvent(string eventArgument)
        {
	        if (eventArgument == ((ListCatalogVerb)AddVerb).EventArgument && 
                AddVerb.Visible && AddVerb.Enabled)
	        {
		        WebPartZoneBase zone = base.WebPartManager.Zones[_selectedZone];
		        WebPartDescriptionCollection descriptions = SelectedCatalogPart.GetAvailableWebPartDescriptions();
                var description = descriptions[_selectedPart];
                if (description != null)
                {
                    WebPart webPart = SelectedCatalogPart.GetWebPart(description);
                    if (webPart != null)
                    {
                        WebPartManager.AddWebPart(webPart, zone, 0);
                    }
                }
	        }
	        base.RaisePostBackEvent(eventArgument);
        }

        protected override bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
	        _selectedPart = postCollection[ModuleSelectorControlID];
	        _selectedZone = postCollection[ZoneSelectorControlID];
            _selectedCategory = postCollection[ModuleCategorySelectorControlID];
            return false;
        }

		protected override void TrackViewState()
		{
			base.TrackViewState();
			if (_addVerb != null)
			{
				((IStateManager)_addVerb).TrackViewState();
			}
			if (_closeVerb != null)
			{
				((IStateManager)_closeVerb).TrackViewState();
			}
		}

		protected override void LoadViewState(object savedState)
		{
			Triplet triplet = (Triplet)savedState;
			base.LoadViewState(triplet.First);
			if (triplet.Second != null)
			{
				((IStateManager)AddVerb).LoadViewState(triplet.Second);
			}
			if (triplet.Third != null)
			{
				((IStateManager)CloseVerb).LoadViewState(triplet.Third);
			}
		}

		protected override object SaveViewState()
		{
			return new Triplet(base.SaveViewState(),
				_addVerb != null ? ((IStateManager)_addVerb).SaveViewState() : null,
				_closeVerb != null ? ((IStateManager)_closeVerb).SaveViewState() : null);
		}

        //[Browsable(false)]
        //[DefaultValue("")]
        //[PersistenceMode(PersistenceMode.InnerProperty)]
        //[TemplateContainer(typeof(WidgetCatalog))]
        //[TemplateInstance(TemplateInstance.Single)]
        //public override ITemplate ZoneTemplate
        //{
        //    get
        //    {
        //        return base.ZoneTemplate;
        //    }
        //    set
        //    {
        //        base.ZoneTemplate = value;
        //    }
        //}


    }
}
