﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.UI.WidgetCatalog
{
    public class WidgetWebPartDescription : WebPartDescription
    {
        //
        // Summary:
        //     Initializes a new instance of the class when a System.Web.UI.WebControls.WebParts.WebPart
        //     control instance is available.
        //
        // Parameters:
        //   part:
        //     A System.Web.UI.WebControls.WebParts.WebPart control whose information is contained
        //     in a System.Web.UI.WebControls.WebParts.WebPartDescription.
        //
        // Exceptions:
        //   T:System.ArgumentException:
        //     The System.Web.UI.Control.ID property of part is null or an empty string ("").
        public WidgetWebPartDescription(WebPart part) : base (part)
        {

        }
        //
        // Summary:
        //     Initializes a new instance of the class by using several strings that contain
        //     description information for a System.Web.UI.WebControls.WebParts.WebPart control.
        //
        // Parameters:
        //   id:
        //     The value to assign to the System.Web.UI.WebControls.WebParts.WebPartDescription.ID.
        //
        //   title:
        //     The value to assign to the System.Web.UI.WebControls.WebParts.WebPartDescription.Title.
        //
        //   description:
        //     The value to assign to the System.Web.UI.WebControls.WebParts.WebPartDescription.Description.
        //
        //   imageUrl:
        //     The value to assign to the System.Web.UI.WebControls.WebParts.WebPartDescription.CatalogIconImageUrl.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     id or title is null or an empty string ("").
        public WidgetWebPartDescription(string id, string title, string description, string imageUrl, string iconClass, string category) : base (id, title, description, imageUrl)
        {
            Category = category;
            IconClass = iconClass;
        }

        public string Category
        {
            get;
            set;
        }

        public string IconClass
        {
            get;
            set;
        }
    }
}
