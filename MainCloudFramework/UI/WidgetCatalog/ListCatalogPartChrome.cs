using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.UI.WidgetCatalog
{
    public class MyTemplate : ITemplate
    {
        private ListItemType _type = ListItemType.Item;

        public MyTemplate(ListItemType type)
        {
            _type = type;
        }

        public void InstantiateIn(Control container)
        {

            switch (_type)
            {
                case ListItemType.Header:
                    container.Controls.Add(new LiteralControl("<ul>"));
                    break;

                case ListItemType.Footer:
                    container.Controls.Add(new LiteralControl("</ul>"));
                    break;

                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    var c = new HtmlGenericControl("li");
                    c.ID = "L";
                    // .... add needed attributes etc.
                    container.Controls.Add(c);
                    // manage data binding
                    container.DataBinding += (o, e) =>
                    {
                        c.InnerText = "" + DataBinder.Eval(container, "DataItem.Title");
                    };
                    break;
            }
        }
    }


    public class ListCatalogPartChrome : CatalogPartChrome
    {
        private ITemplate WidgetsCatalogHeaderTemplate;
        private ITemplate WidgetsCatalogItemTemplate;
        private ITemplate WidgetsCatalogFooterTemplate;

        public ListCatalogPartChrome(ListCatalogZone zone, ITemplate widgetsCatalogHeaderTemplate, ITemplate widgetsCatalogItemTemplate, ITemplate widgetsCatalogFooterTemplate)
            : base(zone)
        {
            WidgetsCatalogHeaderTemplate = widgetsCatalogHeaderTemplate;
            WidgetsCatalogItemTemplate = widgetsCatalogItemTemplate;
            WidgetsCatalogFooterTemplate = widgetsCatalogFooterTemplate;
        }

        public void RenderCatalogPart(HtmlTextWriter writer, CatalogPart catalogPart, string category)
        {
            if (catalogPart == null)
            {
                throw new ArgumentNullException("catalogPart");
            }

            WebPartDescriptionCollection partDescriptions = null;
            ListWidgetsCatalog widgetCatalogPart = catalogPart as ListWidgetsCatalog;
            if (widgetCatalogPart != null)
            {
                partDescriptions = widgetCatalogPart.GetAvailableWebPartDescriptions(category);
            }
            else
            {
                partDescriptions = catalogPart.GetAvailableWebPartDescriptions();
            }

            //DropDownList ddllist = new DropDownList();
            //ddllist.Width = new Unit(85, UnitType.Percentage);
            //ddllist.ID = ((ListCatalogZone)Zone).ModuleSelectorControlID;
            //foreach (WebPartDescription description in partDescriptions)
            //{
            //    ddllist.Items.Add(new ListItem(description.Title, description.ID));
            //}
            //writer.Write("Moduli:&nbsp;");
            //ddllist.RenderControl(writer);


            HiddenField hf = new HiddenField();
            hf.ID = ((ListCatalogZone)Zone).ModuleSelectorControlID;
            hf.RenderControl(writer);

            Repeater list = new Repeater();
            list.ID = "widget_" + ((ListCatalogZone)Zone).ModuleSelectorControlID;
            list.DataSource = partDescriptions;

            list.HeaderTemplate = WidgetsCatalogHeaderTemplate != null ? WidgetsCatalogHeaderTemplate : new MyTemplate(ListItemType.Header);
            list.ItemTemplate = WidgetsCatalogItemTemplate != null ? WidgetsCatalogItemTemplate : new MyTemplate(ListItemType.Item);
            list.FooterTemplate = WidgetsCatalogFooterTemplate != null ? WidgetsCatalogFooterTemplate : new MyTemplate(ListItemType.Footer);
            list.DataBind();

            //writer.Write("Moduli:&nbsp;");
            list.RenderControl(writer);
        }

        public override void RenderCatalogPart(HtmlTextWriter writer, CatalogPart catalogPart)
        {
            RenderCatalogPart(writer, catalogPart, null);
        }
    }
}
