namespace MainCloudFramework.UI.Modules
{
	using System;
	using System.ComponentModel;
	using System.Web.UI;
	using System.Web.UI.WebControls.WebParts;
	using System.Collections.Specialized;
    using System.Web.UI.WebControls;

	public class DropDownCatalogZone : CatalogZone
	{
		string _selectedPart = null;
		string _selectedZone = null;
		DropDownCatalogVerb _addVerb = null;
		DropDownCatalogVerb _closeVerb = null;

		string ZoneSelectorControlID
		{
			get { return String.Concat(UniqueID, IdSeparator, "zones"); }
		}

		internal string ModuleSelectorControlID
		{
			get { return String.Concat(UniqueID, IdSeparator, "modules"); }
		}

		public override WebPartVerb AddVerb
		{
			get 
			{
				if (_addVerb == null)
				{
					_addVerb = new ModuleCatalogAddVerb();
					if (IsTrackingViewState)
					{
						((IStateManager)_addVerb).TrackViewState();
					}
				}
				return _addVerb;
			}
		}

		public override WebPartVerb CloseVerb
		{
			get
			{
				if (_closeVerb == null)
				{
					_closeVerb = new ModuleCatalogCloseVerb();
					if (IsTrackingViewState)
					{
						((IStateManager)_closeVerb).TrackViewState();
					}
				}
				return _closeVerb;
			}
		}
        
        CatalogPart SelectedCatalogPart
        {
            get
            {
                CatalogPart part = null;
                CatalogPartCollection catalogParts = CatalogParts;
                if ((catalogParts != null) && (catalogParts.Count > 0))
                {
                    if (String.IsNullOrEmpty(SelectedCatalogPartID))
                    {
                        part = catalogParts[0];
                    }
                    else
                    {
                        part = catalogParts[SelectedCatalogPartID];
                    }
                }
                return part;
            }
        } 

		protected override CatalogPartChrome CreateCatalogPartChrome()
		{
			return new DropDownCatalogPartChrome(this);
		}

		protected override void RenderHeader(HtmlTextWriter writer)
		{
			
		}

        protected override void RenderFooter(HtmlTextWriter writer)
        {
            writer.AddStyleAttribute(HtmlTextWriterStyle.Margin, "4px");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            DropDownList list = new DropDownList();
            list.ID = ZoneSelectorControlID;
            list.Width = new Unit(125, UnitType.Pixel);
            WebPartZoneCollection zones = WebPartManager.Zones;
            if (zones != null)
            {
                foreach (WebPartZoneBase zone in zones)
                {
                    if (zone.AllowLayoutChange)
                    {
                        ListItem listItem = new ListItem(
                            zone.DisplayTitle, zone.ID);
                        if (String.Equals(zone.ID, _selectedZone, 
                            StringComparison.InvariantCultureIgnoreCase))
                        {
                            listItem.Selected = true;
                        }
                        list.Items.Add(listItem);
                    }
                }
            }
            LabelStyle.AddAttributesToRender(writer, this);
            writer.AddAttribute(HtmlTextWriterAttribute.For, list.ClientID);
            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            writer.Write(this.SelectTargetZoneText);
            writer.RenderEndTag();
            writer.Write("&nbsp;");
            list.ApplyStyle(base.EditUIStyle);
            list.RenderControl(writer);
            writer.Write("&nbsp;");
            RenderVerbs(writer);
            writer.RenderEndTag();
        }

		protected override void RenderVerbs(HtmlTextWriter writer)
		{
			int moduleCount = 0;
			bool moduleEnabled = false;
			WebPartDescriptionCollection modules = 
                SelectedCatalogPart.GetAvailableWebPartDescriptions();
			moduleCount = (modules != null) ? modules.Count : 0;
			if (moduleCount == 0 || WebPartManager.Zones.Count == 0)
			{
				moduleEnabled = this.AddVerb.Enabled;
				this.AddVerb.Enabled = false;
			}
			try
			{
				RenderVerb(writer, AddVerb);
				writer.Write("&nbsp;");
				RenderVerb(writer, CloseVerb);
			}
			finally
			{
				if (moduleCount == 0 || WebPartManager.Zones.Count == 0)
				{
					this.AddVerb.Enabled = moduleEnabled;
				}
			}
		}

		protected override void RenderVerb(HtmlTextWriter writer, WebPartVerb verb)
		{
			string eventArgument = ((DropDownCatalogVerb)verb).EventArgument;

			DropDownCatalogZoneButton button = new DropDownCatalogZoneButton(this, eventArgument);
			button.Text = verb.Text;
			button.ApplyStyle(base.VerbStyle);
			button.ToolTip = verb.Description;
			button.Enabled = verb.Enabled;
			button.Page = this.Page;
			button.RenderControl(writer);
		}

        protected override void RaisePostBackEvent(string eventArgument)
        {
	        if (eventArgument == ((DropDownCatalogVerb)AddVerb).EventArgument && 
                AddVerb.Visible && AddVerb.Enabled)
	        {
		        WebPartZoneBase zone = base.WebPartManager.Zones[_selectedZone];
		        WebPartDescriptionCollection descriptions = 
                    SelectedCatalogPart.GetAvailableWebPartDescriptions();
                WebPart webPart = SelectedCatalogPart.GetWebPart(
                    descriptions[_selectedPart]);
		        WebPartManager.AddWebPart(webPart, zone, 0);
	        }
	        base.RaisePostBackEvent(eventArgument);
        }

        protected override bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
	        _selectedPart = postCollection[ModuleSelectorControlID];
	        _selectedZone = postCollection[ZoneSelectorControlID];
	        return false;
        }

		protected override void TrackViewState()
		{
			base.TrackViewState();
			if (_addVerb != null)
			{
				((IStateManager)_addVerb).TrackViewState();
			}
			if (_closeVerb != null)
			{
				((IStateManager)_closeVerb).TrackViewState();
			}
		}

		protected override void LoadViewState(object savedState)
		{
			Triplet triplet = (Triplet)savedState;
			base.LoadViewState(triplet.First);
			if (triplet.Second != null)
			{
				((IStateManager)AddVerb).LoadViewState(triplet.Second);
			}
			if (triplet.Third != null)
			{
				((IStateManager)CloseVerb).LoadViewState(triplet.Third);
			}
		}

		protected override object SaveViewState()
		{
			return new Triplet(base.SaveViewState(),
				_addVerb != null ? ((IStateManager)_addVerb).SaveViewState() : null,
				_closeVerb != null ? ((IStateManager)_closeVerb).SaveViewState() : null);
		}

        //[Browsable(false)]
        //[DefaultValue("")]
        //[PersistenceMode(PersistenceMode.InnerProperty)]
        //[TemplateContainer(typeof(WidgetCatalog))]
        //[TemplateInstance(TemplateInstance.Single)]
        //public override ITemplate ZoneTemplate
        //{
        //    get
        //    {
        //        return base.ZoneTemplate;
        //    }
        //    set
        //    {
        //        base.ZoneTemplate = value;
        //    }
        //}
	}
}
