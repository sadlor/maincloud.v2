﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using MainCloudFramework.UI.Containers;
using MainCloudFramework.Models;

namespace MainCloudFramework.UI.Modules
{
    /// <summary>
    /// Summary description for WidgetsCatalog
    /// </summary>
    public class WidgetsCatalog : CatalogPart
    {
        /// <summary>
        /// Overrides the Title to display Modules Catalog Part by default
        /// </summary>
        public override string Title
        {
            get
            {
                string title = base.Title;
                return string.IsNullOrEmpty(title) ? "Moduli disponibili" : title;
            }
            set
            {
                base.Title = value;
            }
        }

        /// <summary>
        /// Creates a new instance of the class
        /// </summary>
        public WidgetsCatalog()
        {

        }

        /// <summary>
        /// Returns the WebPartDescriptions
        /// </summary>
        public override WebPartDescriptionCollection GetAvailableWebPartDescriptions()
        {
            if (this.DesignMode)
            {
                return new WebPartDescriptionCollection(new object[] {
                    new WebPartDescription("1", "Modulo 1", null, null),
                    new WebPartDescription("2", "Modulo 2", null, null),
                        new WebPartDescription("3", "Modulo 3", null, null)});
            }

            List<WebPartDescription> list = new List<WebPartDescription>();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                foreach (Widget module in db.WidgetSet)
                {                    
                    list.Add(new WebPartDescription(module.WidgetId, module.Name, module.Description, module.ImageUrl));
                }
            }
            return new WebPartDescriptionCollection(list);
        }

        /// <summary>
        /// Returns a new instance of the WebPart specified by the description
        /// </summary>
        public override WebPart GetWebPart(WebPartDescription description)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var widgetData = db.WidgetSet.Where(m => m.WidgetId == description.ID).SingleOrDefault();
                if (widgetData != null)
                {
                    WidgetWebPart wp = new WidgetWebPart(widgetData);
                    return wp;
                }
            }
            return null;
        }

    
    }
}
