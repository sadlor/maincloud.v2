﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using MainCloudFramework.UI.Containers;
using MainCloudFramework.UI.Utilities;

namespace MainCloudFramework.UI.Modules
{
    /// <summary>
    /// Summary description for WidgetControl
    /// </summary>
    public class WidgetControl<T> : BaseWidgetControl
    {
        public WidgetControl(Type widgetType) : base(widgetType) {}

        //protected T CustomSettings
        //{
        //    get
        //    {
        //        return (T)Convert.ChangeType(((WidgetWebPart)Parent).WidgetCustomConfiguration, typeof(T));
        //    }
        //    set
        //    {
        //        ((WidgetWebPart)Parent).WidgetCustomConfiguration = value;
        //    }
        //}

        protected DataCustomSettings CustomSettings
        {
            get
            {
                return ((WidgetWebPart)Parent).WidgetCustomConfiguration;
            }
        }
    }
}