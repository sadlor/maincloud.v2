using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.UI.Modules
{
    public class DropDownCatalogPartChrome : CatalogPartChrome
    {
        public DropDownCatalogPartChrome(DropDownCatalogZone zone)
            : base(zone)
        {
        }

        public override void RenderCatalogPart(HtmlTextWriter writer, CatalogPart catalogPart)
        {
            WebPartDescriptionCollection partDescriptions = catalogPart.GetAvailableWebPartDescriptions();
            DropDownList list = new DropDownList();
            list.Width = new Unit(85, UnitType.Percentage);
            list.ID = ((DropDownCatalogZone)Zone).ModuleSelectorControlID;

            foreach (WebPartDescription description in partDescriptions)
            {
                list.Items.Add(new ListItem(description.Title, description.ID));
            }
            writer.Write("Moduli:&nbsp;");
            list.RenderControl(writer);
        }
    }
}
