﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using MainCloudFramework.UI.Containers;
using MainCloudFramework.UI.Utilities;
using MainCloudFramework.UI.Communication.Attributes;
using System.Collections.Generic;
using MainCloudFramework.UI.Communication.Events;
using MainCloudFramework.Models;
using MainCloudFramework.Web.Multitenants;

namespace MainCloudFramework.UI.Modules
{
    /// <summary>
    /// Summary description for WidgetControl
    /// </summary>
    public abstract class BaseWidgetControl : UserControl
    {
        private Type widgetType;

        public BaseWidgetControl(Type t)
        {
            this.widgetType = t;
        }

        public string WidgetId { get; set; }
        public Widget WidgetData { get; set; }

        public virtual bool EnablePrint { get { return false; } }
        public virtual bool EnableExport { get { return false; } }

        public virtual void OnPrint() { }
        public virtual void OnExport() { }

        public HttpMultitenantsSessionState SessionMultitenant
        {
            get { return Session.SessionMultitenant(); }
        }

        public HttpMultitenantsApplicationState ApplicationMultitenant
        {
            get { return Application.ApplicationMultitenant(); }
        }

        protected object _dataProvider
        {
            get
            {
                return ViewState["dataProvider"];
            }
            set
            {
                ViewState["dataProvider"] = value;
            }
        }

        public virtual void ReceiveEvent(WidgetWebPart sender, SendObjectEventArgs e) //object sender
        {
            bool accept = false;
            
            //Read System.ComponentModel Description Attribute from method 'MyMethodName' in class 'MyClass'
            //List<string> Attribute = widgetType.GetAttribute("ReceiveEvent", (TypeAcceptedAttribute ta) => ta.TypeAccepted);
            //foreach (string typeName in Attribute)
            //{
            //    if (e.SenderData.GetType() == Type.GetType(typeName))
            //    {
            //        accept = true;
            //        break;
            //    }
            //}
            //if (!accept)
            //{
            //    //"Il tipo di oggetto passato non è un tipo valido, check provider"
            //}

            ViewState["dataProvider"] = e.SenderData;
            //this._dataProvider = provider;
        }

        public void SendEvent(object sender)
        {
            WidgetWebPartContainer.SendWidgetEvent(sender);
        }

        public WidgetWebPart WidgetWebPartContainer
        {
            get
            {
                return this.Parent as WidgetWebPart;
            }
        }
    }

    public static class AttributeExtensions
    {
        public static TValue GetAttribute<TAttribute, TValue>(this Type type, string MemberName, Func<TAttribute, TValue> valueSelector, bool inherit = false) where TAttribute : Attribute
        {
            var att = type.GetMember(MemberName).FirstOrDefault().GetCustomAttributes(typeof(TAttribute), inherit).FirstOrDefault() as TAttribute;
            if (att != null)
            {
                return valueSelector(att);
            }
            return default(TValue);
        }
    }
}