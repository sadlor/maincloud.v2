﻿using MainCloudFramework.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace MainCloudFramework.UI.Modules
{
    public abstract class WidgetSetting : UserControl
    {
        public abstract bool ApplyChanges(DataCustomSettings widgetCustomSettings);
        public abstract void SyncChanges(DataCustomSettings widgetCustomSettings);
    }
}
