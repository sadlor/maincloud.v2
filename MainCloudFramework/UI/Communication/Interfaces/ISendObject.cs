﻿using MainCloudFramework.UI.Communication.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.UI.Communication.Interfaces
{
    public interface ISendObject : IWidgetCommunication
    {
        SendObjectEventArgs e { get; }
    }
}