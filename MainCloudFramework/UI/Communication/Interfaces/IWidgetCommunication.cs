﻿using MainCloudFramework.UI.Communication.Events;
using MainCloudFramework.UI.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MainCloudFramework.UI.Communication.Interfaces
{
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    [AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    public interface IWidgetCommunication
    {
        WidgetWebPart Provider { get; }
    }
}