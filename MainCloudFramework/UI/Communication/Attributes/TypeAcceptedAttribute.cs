﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.UI.Communication.Attributes
{
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]  //AllowMultiple = true
    public class TypeAcceptedAttribute : Attribute
    {
        public List<string> TypeAccepted { get; private set; }

        public TypeAcceptedAttribute(params string[] type)
        {
            this.TypeAccepted = new List<string>(type);
        }

        public TypeAcceptedAttribute()
        {}
    }
}