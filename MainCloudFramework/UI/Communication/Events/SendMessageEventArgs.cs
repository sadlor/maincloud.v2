﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.UI.Communication.Events
{
    public class SendMessageEventArgs : EventArgs
    {
        public string Message { get; private set; }

        public SendMessageEventArgs(string message)
        {
            this.Message = message;
        }
    }
}