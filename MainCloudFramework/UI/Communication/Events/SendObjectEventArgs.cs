﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.UI.Communication.Events
{
    public class SendObjectEventArgs : EventArgs
    {
        public object SenderData { get; private set; }

        public SendObjectEventArgs(object obj)
        {
            this.SenderData = obj;
        }
    }
}