﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.UI.Utilities
{
    using System.Text;
    using System.Globalization;
    using System.Runtime.InteropServices;
    using Web;

    /*
     * Various string handling utilities 
     */
    internal static class StringUtil
    {
        internal static string CheckAndTrimString(string paramValue, string paramName)
        {
            return CheckAndTrimString(paramValue, paramName, true);
        }

        internal static string CheckAndTrimString(string paramValue, string paramName, bool throwIfNull)
        {
            return CheckAndTrimString(paramValue, paramName, throwIfNull, -1);
        }

        internal static string CheckAndTrimString(string paramValue, string paramName,
                                                  bool throwIfNull, int lengthToCheck)
        {
            if (paramValue == null)
            {
                if (throwIfNull)
                {
                    throw new ArgumentNullException(paramName);
                }
                return null;
            }
            string trimmedValue = paramValue.Trim();
            if (trimmedValue.Length == 0)
            {
                throw new ArgumentException(
                    SR.GetString(SR.PersonalizationProviderHelper_TrimmedEmptyString,
                                                     paramName));
            }
            if (lengthToCheck > -1 && trimmedValue.Length > lengthToCheck)
            {
                throw new ArgumentException(
                    SR.GetString(SR.StringUtil_Trimmed_String_Exceed_Maximum_Length,
                                                     paramValue, paramName, lengthToCheck.ToString(CultureInfo.InvariantCulture)));
            }
            return trimmedValue;
        }

        internal static bool Equals(string s1, string s2)
        {
            if (s1 == s2)
            {
                return true;
            }

            if (String.IsNullOrEmpty(s1) && String.IsNullOrEmpty(s2))
            {
                return true;
            }

            return false;
        }

        internal static string[] ObjectArrayToStringArray(object[] objectArray)
        {
            String[] stringKeys = new String[objectArray.Length];
            objectArray.CopyTo(stringKeys, 0);
            return stringKeys;
        }

        internal static byte[] GetBytes(string str)
        {
            byte[] bytes = str.Select(Convert.ToByte).ToArray();
            return bytes;
        }

        internal static string GetString(byte[] bytes)
        {
            string str = new string(bytes.Select(Convert.ToChar).ToArray());
            return str;
        }
    }
}
