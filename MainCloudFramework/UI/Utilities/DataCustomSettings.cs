﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.UI.Utilities
{
    public class DataCustomSettings
    {
        public Dictionary<string, object> Params { get; set; }

        public DataCustomSettings()
        {
            this.Params = new Dictionary<string,object>();
        }

        public void Add(string code, object data)
        {
            if (!Params.ContainsKey(code))
            {
                this.Params.Add(code, data);
            }
        }

        public void Add(Dictionary<string, object> _params)
        {
            foreach (KeyValuePair<string, object> param in _params)
            {
                Add(param.Key, param.Value);
            }
        }

        public void AddOrUpdate(string code, object data)
        {
            if (!Params.ContainsKey(code))
            {
                this.Params.Add(code, data);
            }
            else
            {
                Params[code] = data;
            }
        }

        public object Find(string code)
        {
            if (Params.ContainsKey(code))
            {
                return this.Params[code];
            }
            else
            { return null; }
        }
    }
}
