﻿using System.Web.UI.WebControls.WebParts;
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.ComponentModel;
using System.IO;
using MainCloudFramework.UI.Modules;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using MainCloudFramework.UI.Containers.Editor.EditorProperties;
using MainCloudFramework.UI.Utilities;
using System.Security.Permissions;
using MainCloudFramework.UI.Communication.Attributes;
using MainCloudFramework.UI.Communication.Interfaces;
using Newtonsoft.Json;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Web;
using MainCloudFramework.Services;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;

namespace MainCloudFramework.UI.Containers
{
/// <summary>
/// Summary description for WidgetWebPart
/// </summary>
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    [AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    public class WidgetWebPart : WebPart, IWebEditable, ISendObject
    {
        public const string PARAM_EDIT_CONTROL = "editcontrol";
        public const string PARAM_WIDGET_ID = "widgetid";
        public const string PARAM_RETURN_URL = "returnurl";

        protected WidgetService widgetService = new WidgetService();

        private BaseWidgetControl _module;
        private string _baseModulePath;
        private Widget widget;
        private string _widgetId;

        [Obsolete("Da eliminare al termine della migrazione.")]
        private string _userControlName;

        [Obsolete("Da eliminare al termine della migrazione.")]
        [WebBrowsable(false), Personalizable(true)]
        public string UserControlPath { get; set; }

        [Obsolete("Da eliminare al termine della migrazione.")]
        [WebBrowsable(false), Personalizable(true)]
        public string UserControlName
        {
            get
            {
                return _userControlName;
            }
            set
            {
                _userControlName = value;
                if (value != null && WidgetId == null)
                {
                    WidgetsRepository wr = new WidgetsRepository();
                    var tmpW = wr.FindByWidgetName(value);
                    WidgetId = tmpW.WidgetId;
                    SetPersonalizationDirty();
                }
            }
        }


        [WebBrowsable(false), Personalizable(true)]
        public string WidgetId
        {
            get
            {
                return _widgetId;
            }
            set
            {
                _widgetId = value;
                if (value != null)
                {
                    WidgetsRepository wr = new WidgetsRepository();
                    widget = wr.FindByWidgetId(value);
                }
            }
        }

        public string WidgetPath { get { return widget != null && widget.WidgetPath != null ? widget.WidgetPath : UserControlPath; } }

        public string WidgetName { get { return widget != null && widget.WidgetName != null ? widget.WidgetName : UserControlName; } }

        [WebBrowsable(false), Personalizable(true)]
        public string ContainerStyle
        {
            get { return CssClass; }
            set { CssClass = value; }
        }

        public string FullPath
        {
            get
            {
                string fullPath = Path.Combine(Path.Combine(this._baseModulePath, this.WidgetPath), WidgetName + "_View.ascx");
                return fullPath;
            }
        }

        public string FullPathEdit
        {
            get
            {
                string fullPath = Path.Combine(Path.Combine(this._baseModulePath, this.WidgetPath), WidgetName + "_Edit.ascx");
                return fullPath;
            }
        }

        public string FullPathSettings
        {
            get
            {
                string fullPath = Path.Combine(Path.Combine(this._baseModulePath, this.WidgetPath), WidgetName + "_Settings.ascx");
                return fullPath;
            }
        }

        public string EditPageUrl
        {
            get
            {
                string urlEditPage = MultiTenantsHelper.MountMultiTenantUrl("Edit") + "?";
                urlEditPage += string.Format("{0}={1}", PARAM_EDIT_CONTROL, this.WidgetPath + "/" + this.WidgetName + "_Edit");
                urlEditPage += "&";
                urlEditPage += string.Format("{0}={1}", PARAM_WIDGET_ID, WidgetId);
                urlEditPage += "&";
                urlEditPage += string.Format("{0}={1}", PARAM_RETURN_URL, HttpContext.Current.Request.Url.AbsolutePath);

                urlEditPage = this.ResolveUrl(urlEditPage);
                return urlEditPage;
            }
        }

        public string PrintPageUrl
        {
            get
            {
                string urlPage = MultiTenantsHelper.MountMultiTenantUrl("Print") + "?";
                urlPage += string.Format("{0}={1}", PARAM_EDIT_CONTROL, WidgetPath + "/" + WidgetName + "_Print");
                urlPage += "&";
                urlPage += string.Format("{0}={1}", PARAM_WIDGET_ID, WidgetId);
                urlPage += "&";
                urlPage += string.Format("{0}={1}", PARAM_RETURN_URL, HttpContext.Current.Request.Url.AbsolutePath);

                urlPage = ResolveUrl(urlPage);
                return urlPage;
            }
        }

        public string ExportPageUrl
        {
            get
            {
                string urlPage = MultiTenantsHelper.MountMultiTenantUrl("Export") + "?";
                urlPage += string.Format("{0}={1}", PARAM_EDIT_CONTROL, WidgetPath + "/" + WidgetName + "_Export");
                urlPage += "&";
                urlPage += string.Format("{0}={1}", PARAM_WIDGET_ID, WidgetId);
                urlPage += "&";
                urlPage += string.Format("{0}={1}", PARAM_RETURN_URL, HttpContext.Current.Request.Url.AbsolutePath);

                urlPage = ResolveUrl(urlPage);
                return urlPage;
            }
        }

        public bool ExistEditPage
        {
            get
            {
                return File.Exists(Page.MapPath(widgetService.GetModuleControlPath(WidgetPath + "/" + WidgetName + "_Edit.ascx")));
            }
        }

        public bool ExistSettingPage
        {
            get
            {
                return File.Exists(Page.MapPath(widgetService.GetModuleControlPath(WidgetPath + "/" + WidgetName + "_Settings.ascx")));
            }
        }

        public WidgetWebPart() : base()
        {
            _baseModulePath = widgetService.GetModuleControlPath("");
            _widgetCustomConfiguration = new DataCustomSettings();
        }

        public WidgetWebPart(Widget widgetData) : this()
        {
            WidgetId = widgetData.WidgetId; 
            Title = widgetData.Name;
        }

        protected internal void LoadUserControl()
        {
            string fullPath = this.FullPath;
            if (!string.IsNullOrEmpty(fullPath))
            {
                base.CreateChildControls();
                Controls.Clear();
                _module = (BaseWidgetControl)Page.LoadControl(fullPath);
                _module.ID = ID;
                _module.WidgetId = WidgetId;
                _module.WidgetData = widget;
                Controls.Add(_module);
            }
        }

        protected void EditContentVerbEventHandler(object sender, WebPartEventArgs e)
        {
            Page.Response.Redirect(EditPageUrl);
        }

        protected void PrintVerbEventHandler(object sender, WebPartEventArgs e)
        {
            Page.Response.Redirect(PrintPageUrl);
        }

        protected void ExportContentVerbEventHandler(object sender, WebPartEventArgs e)
        {
            //Page.Response.Redirect(ExportPageUrl);
            _module.OnExport();
        }

        public override WebPartVerbCollection Verbs
        {
            get
            {
                GrantService grantService = new GrantService();

                Collection<WebPartVerb> verbs = new Collection<WebPartVerb>();
                HttpContext ctx = HttpContext.Current;

                if (_module != null && _module.EnablePrint)
                {
                    WebPartVerb printVerb = new WebPartVerb("PrintVerb", new WebPartEventHandler(PrintVerbEventHandler));
                    printVerb.Text = "<i class=\"fa fa-print fa-fw\"></i>&nbsp;" + "Print";  //Stampa
                    printVerb.Description = "Stampa dei contenuti di {0}";
                    verbs.Add(printVerb);
                }

                if (_module != null && _module.EnableExport)
                {
                    WebPartVerb editVerb = new WebPartVerb("ExportContentVerb", new WebPartEventHandler(ExportContentVerbEventHandler));
                    editVerb.Text = "<i class=\"fa fa-file-text-o fa-fw\"></i>&nbsp;" + "Export widget content";  //Esporta contenuto widget
                    editVerb.Description = "Esportazione dei contenuti di {0}";
                    verbs.Add(editVerb);
                }

                if ((grantService.IsHostUser() || grantService.IsAdminUser()) &&
                    this.WebPartManager.Personalization.IsModifiable && this.WebPartManager.Personalization.CanEnterSharedScope)
                {
                    if (ctx.Request.IsAuthenticated && ExistEditPage)
                    {
                        //string callbackFunct = "function(){window.top.location.reload();}";
                        //string js = string.Format("return GB_showFullScreen('Modifica contenuti', '{0}', {1})", this.EditPageUrl, callbackFunct);
                        //WebPartVerb editVerb = new WebPartVerb("ModificaContenutoVerb", js);
                        //editVerb.Text = "Modifica contenuto widget";
                        //editVerb.Description = "Modifica il contenuto di {0}";
                        ////editVerb.ImageUrl = "~/App_Themes/FederagentiBlue/images_cms/EditVerb.gif";

                        //Server side
                        WebPartVerb editVerb = new WebPartVerb("ModificaContenutoVerb", new WebPartEventHandler(EditContentVerbEventHandler));
                        editVerb.Text = "<i class=\"fa fa-pencil fa-fw\"></i>&nbsp;" + "Edit widget content";  //Modifica contenuto widget
                        editVerb.Description = "Editazione e modifica dei contenuti di {0}";

                        verbs.Add(editVerb);
                    }
                }
                else
                {
                    string routePath = (string)Page.RouteData.Values["name"];
                    
                    bool isAreaOwner = grantService.IsAreaOwner(routePath);
                    if ((grantService.IsHostUser() ||
                            grantService.IsAdminUser()) ||
                            isAreaOwner)
                    {
                        // do nothing
                    }
                    else
                    {
                        AllowClose = false;
                        AllowConnect = false;
                        AllowEdit = false;
                        AllowHide = false;
                        AllowMinimize = false;
                        AllowZoneChange = false;
                    }
                }
                return new WebPartVerbCollection(base.Verbs, verbs);
            }
        }

        protected override void CreateChildControls()
        {
            try
            {
                LoadUserControl();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void SendWidgetEvent(object sender)
        {
            this.Data = sender;
            //this.WebPartManager.GetProviderConnectionPoints(this)[0];
        }

        [ConnectionProvider("Send object from provider", "SendWidgetProvider")]
        public ISendObject SendWidgetProvider()
        {
            return this;
        }

        [ConnectionConsumer("Consumer", "Consumer")]  //cambiare id
        public void GetWidgetProvider(ISendObject sender)
        {
            //this._provider = sender.Provider;
            this._module.ReceiveEvent(sender.Provider, sender.e);
        }

        #region IWebEditable Members

        EditorPartCollection IWebEditable.CreateEditorParts()
        {         
            List<EditorPart> editors = new List<EditorPart>();
            editors.Add(new StyleEditorPart());
            if (ExistSettingPage)
            {
                editors.Add(new SettingEditorPart());
            }
            return new EditorPartCollection(editors);
        }

        object IWebEditable.WebBrowsableObject
        {
            get { return this; }
        }

        #endregion

        private object _dataProvider;

        [Personalizable(true)]
        public object Data
        {
            get
            {
                return this._dataProvider;
            }
            set
            {
                this._dataProvider = value;
            }
        }

        private DataCustomSettings _widgetCustomConfiguration;

        [Personalizable(true), WebBrowsable(false)] //WebDisplayName("Configurazione"), WebDescription("Definisce configurazione personalizzata")
        public string PersistedWidgetCustomConfiguration
        {
            get
            {
                string _persistedConfig = "";
                try
                {
                    _persistedConfig = JsonConvert.SerializeObject(_widgetCustomConfiguration);
                }
                catch (Exception ex)
                {
                }
                return _persistedConfig;
            }
            set
            {
                try
                {
                    _widgetCustomConfiguration = JsonConvert.DeserializeObject<DataCustomSettings>(value);
                }
                catch (Exception ex)
                { }
            }
        }

        public DataCustomSettings WidgetCustomConfiguration
        {
            get
            {
                return _widgetCustomConfiguration;
            }
            set
            {
                _widgetCustomConfiguration = value;
            }
        }

        public WidgetWebPart Provider
        {
            get
            {
                return this;
            }
        }

        public Communication.Events.SendObjectEventArgs e
        {
            get
            {
                return new Communication.Events.SendObjectEventArgs(this.Data);
            }
        }
    }
}