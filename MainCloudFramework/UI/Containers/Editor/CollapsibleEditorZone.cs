namespace MainCloudFramework.UI.Containers.Editor
{
    using System;
	using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;

    public class CollapsibleEditorZone : EditorZone
    {
		[Description("Indicates what happens to other EditorParts when an EditorPart expands."),
		Category("Behavior"),
		DefaultValue(CollapseStyle.ContractAll)]
		public CollapseStyle CollapseStyle
		{
			get
			{
				CollapseStyle style = CollapseStyle.ContractAll;
				if (ViewState["CollapseStyle"] != null)
				{
					style = (CollapseStyle)ViewState["CollapseStyle"];
				}
				return style;
			}
			set { ViewState["CollapseStyle"] = value; }
		}

		[Description("Indicates whether the editor uses collapsing behavior for EditorParts."),
		Category("Behavior"),
		DefaultValue(CollapseBehavior.Collapse)]
		public CollapseBehavior CollapseBehavior
		{
			get
			{
				CollapseBehavior style = CollapseBehavior.Collapse;
				if (ViewState["CollapseBehavior"] != null)
				{
					style = (CollapseBehavior)ViewState["CollapseBehavior"];
				}
				return style;
			}
			set { ViewState["CollapseBehavior"] = value; }
		}

        protected override EditorPartChrome CreateEditorPartChrome()
        {
			return new CollapsibleEditorChrome(this);
        }

        protected override void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument.StartsWith("minimize:"))
            {
                string editorPartID = eventArgument.Substring("minimize:".Length);

                foreach (EditorPart part in EditorParts)
                {
                    if (part.ID == editorPartID)
                    {
                        if (part.ChromeState == PartChromeState.Normal)
                        {
                            part.ChromeState = PartChromeState.Minimized;
                        }
                        else
                        {
                            part.ChromeState = PartChromeState.Normal;
                        }
					}
					else if (this.CollapseStyle == CollapseStyle.ContractAll && 
						part.ChromeState == PartChromeState.Normal &&
						this.CollapseBehavior == CollapseBehavior.Collapse)
					{
						part.ChromeState = PartChromeState.Minimized;
					}
                }
            }
            base.RaisePostBackEvent(eventArgument);
        }
    }
}