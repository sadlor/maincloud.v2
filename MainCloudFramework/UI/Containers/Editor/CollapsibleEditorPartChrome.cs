using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using MainCloudFramework.UI.Utilities;

namespace MainCloudFramework.UI.Containers.Editor
{
    public class CollapsibleEditorChrome : EditorPartChrome
    {

        #region [===== Constructor =====]
        public CollapsibleEditorChrome(CollapsibleEditorZone zone)
            : base(zone)
        {
        }
        #endregion

        #region [===== Public instance methods =====]
        public override void RenderEditorPart(HtmlTextWriter writer, EditorPart editorPart)
        {
            if (editorPart == null)
            {
                throw new ArgumentNullException("editorPart");
            }

            PartChromeType partChromeType = Zone.GetEffectiveChromeType(editorPart);
            Style partChromeStyle = CreateEditorPartChromeStyle(editorPart, partChromeType);

            partChromeStyle.AddAttributesToRender(writer, Zone);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            if (partChromeType == PartChromeType.TitleAndBorder ||
                partChromeType == PartChromeType.TitleOnly)
            {
                RenderTitle(writer, editorPart);
            }

            if (editorPart.ChromeState != PartChromeState.Minimized)
            {
                Style partStyle = this.Zone.PartStyle;
                partStyle.AddAttributesToRender(writer, Zone);
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                RenderPartContents(writer, editorPart);
                writer.RenderEndTag(); // div
            }
            writer.RenderEndTag();// fieldset
        }
        #endregion

        #region [===== Private instance methods =====]
        private void RenderTitle(HtmlTextWriter writer, EditorPart editorPart)
        {
            string displayTitle = editorPart.DisplayTitle;
            if (!String.IsNullOrEmpty(displayTitle))
            {
                Style titleTextStyle;

                TableItemStyle partTitleStyle = this.Zone.PartTitleStyle;
                Style style = new Style();
                style.CopyFrom(partTitleStyle);
                titleTextStyle = style;

                titleTextStyle.AddAttributesToRender(writer, Zone);

                string description = editorPart.Description;
                if (!String.IsNullOrEmpty(description))
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Title, description);
                }

                string accessKey = editorPart.AccessKey;
                if (!String.IsNullOrEmpty(accessKey))
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Accesskey, accessKey);
                }

                writer.RenderBeginTag(HtmlTextWriterTag.Legend);

                if (((CollapsibleEditorZone)Zone).CollapseBehavior == CollapseBehavior.Collapse)
                {
                    string collapseIconTitle;
                    string collapseIconAltText;
                    string collapseIconResourceName;
                    if (editorPart.ChromeState == PartChromeState.Minimized)
                    {
                        collapseIconTitle = "Expand " + editorPart.Title;
                        collapseIconAltText = "Expands " + editorPart.Title;
                        //collapseIconResourceName = "InfoSupport.Demos.WebParts.Plus.gif";
                        collapseIconResourceName = ControlUtility.ResolveUrl("~/App_Themes/FederagentiBlue/images_cms/Minimize.gif");
                    }
                    else
                    {
                        collapseIconTitle = "Collapse " + editorPart.Title;
                        collapseIconAltText = "Collapses " + editorPart.Title;
                        //collapseIconResourceName = "InfoSupport.Demos.WebParts.Minus.gif";
                        collapseIconResourceName = ControlUtility.ResolveUrl("~/App_Themes/FederagentiBlue/images_cms/Maximize.gif");
                    }

                    writer.AddAttribute(HtmlTextWriterAttribute.Onclick,
                        Zone.Page.ClientScript.GetPostBackEventReference(
                            Zone, "minimize:" + editorPart.ID));
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");

                    writer.RenderBeginTag(HtmlTextWriterTag.A);


                    writer.AddAttribute(HtmlTextWriterAttribute.Src,
                        collapseIconResourceName);
                    /*Zone.Page.ClientScript.GetWebResourceUrl(
                        typeof(CollapsibleEditorZone), collapseIconResourceName));*/

                    writer.AddAttribute(HtmlTextWriterAttribute.Alt, collapseIconAltText);
                    writer.AddAttribute(HtmlTextWriterAttribute.Title, collapseIconTitle);
                    writer.AddAttribute(HtmlTextWriterAttribute.Width, "13px");

                    writer.AddAttribute(HtmlTextWriterAttribute.Height, "13px");
                    writer.AddAttribute(HtmlTextWriterAttribute.Align, "middle");
                    writer.AddStyleAttribute(HtmlTextWriterStyle.Cursor, "hand");
                    writer.RenderBeginTag(HtmlTextWriterTag.Img);
                    writer.RenderEndTag(); // img

                    writer.RenderEndTag(); // a
                }
                writer.Write(displayTitle);
                writer.RenderEndTag(); // legend
            }
        }
        #endregion
    }
}
