namespace MainCloudFramework.UI.Containers.Editor
{
	using System;

	public enum CollapseBehavior
	{
		NoCollapse,
		Collapse
	}

}
