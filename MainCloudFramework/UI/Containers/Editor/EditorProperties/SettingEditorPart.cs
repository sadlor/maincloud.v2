﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using MainCloudFramework.UI.Containers;
using MainCloudFramework.UI.Modules;

namespace MainCloudFramework.UI.Containers.Editor.EditorProperties
{
    /// <summary>
    /// Summary description for StyleEditorPart
    /// </summary>
    public class SettingEditorPart : EditorPart
    {
        private WidgetSetting widgetSetting;

        public SettingEditorPart()
        {
            this.Title = "Settings widget";
            this.ID = "SettingEditorPart";
        }

        public override bool ApplyChanges()
        {
            EnsureChildControls();
            WidgetWebPart control = WebPartToEdit as WidgetWebPart;
            if (control != null)
            {
                return widgetSetting.ApplyChanges(control.WidgetCustomConfiguration);
            }
            else
            {
                Console.WriteLine("ApplyChanges control is null");
            }
            return true;
        }

        public override void SyncChanges()
        {
            EnsureChildControls();
            WidgetWebPart control = WebPartToEdit as WidgetWebPart;
            if (control != null)
            {
                widgetSetting.SyncChanges(control.WidgetCustomConfiguration);
            }
            else
            {
                Console.WriteLine("SyncChanges control is null");
            }
        }

        protected override void CreateChildControls()
        {
            Controls.Clear();

            WidgetWebPart control = WebPartToEdit as WidgetWebPart;
            if (!control.ExistSettingPage)
            {
                Controls.Add(new Label() { Text = "Nessuna configurazione disponibile" });
            }
            else
            {
                string fullPath = control.FullPathSettings;
                if (!string.IsNullOrEmpty(fullPath))
                {
                    base.CreateChildControls();
                    widgetSetting = (WidgetSetting)Page.LoadControl(fullPath);
                    widgetSetting.ID = ID;
                }
            }

            Controls.Add(widgetSetting);
        }

        //protected override void RenderContents(HtmlTextWriter writer)
        //{
        //    widgetSetting.RenderControl(writer);
        //    //writer.Write("Selezione stile contenitori");
        //    //writer.Write("&nbsp;");
        //    //_ddlColor.RenderControl(writer);
        //    //writer.Write("<br/><br/><br/>");
        //    //writer.Write("Selezione comportamento");
        //    //writer.Write("&nbsp;");
        //    //_txtBehaviour.RenderControl(writer);
        //}
    }
}