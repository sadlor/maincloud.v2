﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using MainCloudFramework.UI.Containers;

namespace MainCloudFramework.UI.Containers.Editor.EditorProperties
{
    /// <summary>
    /// Summary description for StyleEditorPart
    /// </summary>
    public class StyleEditorPart : EditorPart
    {
        private DropDownList _ddlColor;
        private TextBox _txtBehaviour;

        public StyleEditorPart()
        {
            this.Title = "Settings contenitore";
            this.ID = "ContainerEditorPart";
        }

        public override bool ApplyChanges()
        {
            EnsureChildControls();
            WebPart oPart = WebPartToEdit as WebPart;
            if (oPart != null)
            {
                WidgetWebPart control = (WidgetWebPart)oPart;
                control.ContainerStyle = _ddlColor.SelectedItem.Value;
                control.WidgetCustomConfiguration.AddOrUpdate("Behaviour", _txtBehaviour.Text);
            }
            else
            {
                return false;
            }
            return true;
        }

        public override void SyncChanges()
        {
            EnsureChildControls();
            WebPart oPart = (WebPart)WebPartToEdit;
            //BaseUserControl control = (BaseUserControl)oPart.ChildControl;
            WidgetWebPart control = (WidgetWebPart)oPart;

            String currentColor = control.ContainerStyle;

            foreach (ListItem item in _ddlColor.Items)
            {
                if (item.Value == currentColor)
                {
                    item.Selected = true;
                    break;
                }
            }

            _txtBehaviour.Text = control.WidgetCustomConfiguration.Find("Behaviour") as string;
        }

        protected override void CreateChildControls()
        {
            Controls.Clear();

            _ddlColor = new DropDownList();
            _ddlColor.Items.Add(new ListItem("Default", "panel-default")); 
            _ddlColor.Items.Add(new ListItem("Primary", "panel-primary"));
            _ddlColor.Items.Add(new ListItem("Success", "panel-success"));
            _ddlColor.Items.Add(new ListItem("Warning", "panel-warning"));
            _ddlColor.Items.Add(new ListItem("Danger", "panel-danger"));
            _ddlColor.Items.Add(new ListItem("Info", "panel-info"));
            Controls.Add(_ddlColor);

            _txtBehaviour = new TextBox();
            Controls.Add(_txtBehaviour);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            writer.Write("Selezione stile contenitori");
            writer.Write("&nbsp;");
            _ddlColor.RenderControl(writer);
            writer.Write("<br/><br/><br/>");
            writer.Write("Selezione comportamento");
            writer.Write("&nbsp;");
            _txtBehaviour.RenderControl(writer);
        }
    }
}