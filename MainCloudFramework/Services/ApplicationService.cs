﻿using MainCloudFramework.Core.Multitenants.Identity;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web;
using MainCloudFramework.Web.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MainCloudFramework.Services
{
    public class ApplicationService : BaseService<ApplicationDbContext>
    {
        private ApplicationRepository applicationRepository = new ApplicationRepository();
        private MainCustomerRepository customerRepository = new MainCustomerRepository();
        private UserRepository userRepository = new UserRepository();

        private RoleRepository roleRepository = new RoleRepository();
        
        public ApplicationRole GetRoleGuest(string applicationId)
        {
            return new ApplicationRole(MCFConstants.ROLE_GUESTS, MCFConstants.ROLE_GUESTS) { ApplicationId = applicationId };
        }

        public List<AspNetApplications> GetApplications()
        {
            List<AspNetApplications> result = null;
            try
            {
                GrantService grantService = new GrantService();
                if (grantService.IsHostUser())
                {
                    result = DBContext.AspNetApplications.ToList();
                }
                else
                {
                    var userId = HttpContext.Current.User.Identity.GetUserId();
                    result = (from app in DBContext.AspNetApplications
                              where app.Users.Where(u => u.Id == userId).Any()
                              select app).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return result;
        }

        //public List<string> FindUserNamesStartWithExcludeCurrentApp(string userName)
        //{
        //    var appId = MultiTenantsHelper.GetApplicationId(DBContext);
        //    var usersFound = (from u in DBContext.Users
        //                      where u.UserName.StartsWith(userName) &&
        //                        !(from ua in u.AspNetUserApplication
        //                          where ua.UserId == u.Id && ua.ApplicationId == appId
        //                          select ua).Any()
        //                      select u.UserName).ToList();
        //    return usersFound;
        //}

        public ApplicationUser FindUserById(string id)
        {
            return userRepository.FindByID(id);
        }

        public void ChangeRoleAndUpdateUser(string newRoleId, ApplicationUser user)
        {
            if (!string.IsNullOrEmpty(newRoleId))
            {
                var currentUserRole = FindUserRoleCurrentApp(user);

                if (currentUserRole != null)
                {
                    user.Roles.Remove(user.Roles.Where(x => x.RoleId == currentUserRole.Id).SingleOrDefault());
                }

                user.Roles.Add(new IdentityUserRole()
                {
                    RoleId = newRoleId,
                    UserId = user.Id
                });
            }
            DBContext.SaveChanges();
        }

        public IdentityResult CreateUserCurrentApplication(ApplicationUser user, string password)
        {
            return CreateUser(user, password, MultiTenantsHelper.ApplicationId);
        }

        public IdentityResult CreateUser(ApplicationUser user, string password, string applicationId)
        {
            IdentityResult result = null;
            var role = roleRepository.FindGuestRole(applicationId);
            if (role == null)
            {
                role = GetRoleGuest(applicationId);
                roleRepository.Insert(role);
            }

            var userStore = new ApplicationUserStore<ApplicationUser>(DBContext) { ApplicationId = applicationId };
            var userManager = new UserManager<ApplicationUser, string>(userStore);
            result = userManager.Create(user, password);

            if (result.Succeeded)
            {
                user.Roles.Add(new IdentityUserRole()
                {
                    RoleId = role.Id,
                    UserId = user.Id
                });

                result = userManager.Update(user);
            }
            return result;
        }

        public object GetAllUsers()
        {
            var users = from user in DBContext.Users
                        select user;
            return users.ToList();
        }

        public List<ApplicationUser> GetUserForCurrentApplication()
        {
            string applicationId = MultiTenantsHelper.GetApplicationId(DBContext);
            if (applicationId != null)
            {
                var users = from user in DBContext.Users
                            where user.Applications.Any(x => x.Id == applicationId)
                            select user;
                return users.ToList();
            }
            return null;
        }

        /// <summary>
        /// Set customer based on multitenant application id contained into url
        /// </summary>
        public void SetCustomer(string customerId)
        {
            var c = customerRepository.FindByID(customerId);
            var appId = MultiTenantsHelper.GetApplicationId(DBContext);
            c.AspNetApplication = applicationRepository.FindByID(appId);
            //customerRepository.Update(c);
            customerRepository.SaveChanges();
        }

        /// <summary>
        /// Get customer Id based on multitenant application id contained into url
        /// </summary>
        /// <returns></returns>
        public string GetCustomerId()
        {
            var appId = MultiTenantsHelper.GetApplicationId(DBContext);
            var cid = (from c in DBContext.MainCustomer
                       where c.AspNetApplication != null && c.AspNetApplication.Id == appId
                       select c.Id).FirstOrDefault();
            return cid;
        }

        public void AddExistingUser(string userName)
        {
            var user = userRepository.FindUserByName(userName);
            AddExistingUserById(user.Id);
        }

        /// <summary>
        /// Add existing user by Id into current application
        /// Add the user into default GUEST role
        /// </summary>
        /// <param name="userId">User Id to add</param>
        public void AddExistingUserById(string userId)
        {
            var appId = MultiTenantsHelper.GetApplicationId(DBContext);
            var app = new AspNetApplications() { Id = appId };
            DBContext.AspNetApplications.Attach(app);

            var user = new ApplicationUser() { Id = userId };
            DBContext.Users.Attach(user);
            if (user.Applications == null)
            {
                user.Applications = new List<AspNetApplications>();
            }
            user.Applications.Add(app);

            // guset role
            var guestRole = DBContext.Roles.Where(r => r.ApplicationId == appId && r.Name == MCFConstants.ROLE_GUESTS).SingleOrDefault();

            if (guestRole == null)
            {
                // Fix add lost role
                guestRole = DBContext.Roles.Add(GetRoleGuest(appId));
            }

            guestRole.Users.Add(new IdentityUserRole()
            {
                RoleId = guestRole.Id,
                UserId = userId,
            });

            DBContext.SaveChanges();
        }

        /// <summary>
        /// Ricerca l'utente per id, identifica l'applicazione tramite application name presente nll'url.
        /// Rimuove l'utent SOLO dall'applicazione.
        /// Rimuove i riferimenti dell'utente dai gruppi dell'applicaione
        /// ATTENZIONE: L'utente non viene rimosso dal database.
        /// </summary>
        /// <param name="userId">Id del'utente rimosso dall'applicazione, se l'utente o l'applicazone non vengono trovati restituisce null</param>
        /// <returns>Utente rimosso</returns>
        public ApplicationUser RemoveUserCurrentAppAndGroup(string userId)
        {
            var applicationId = MultiTenantsHelper.GetApplicationId(DBContext);
            return RemoveUserCurrentAppAndGroup(userId, applicationId);
        }

        /// <summary>
        /// Rimuove l'utente SOLO dall'applicazione.
        /// Rimuove i riferimenti dell'utente dai gruppi dell'applicaione
        /// ATTENZIONE: L'utente non viene rimosso dal database.
        /// </summary>
        /// <param name="userId">Id del'utente rimosso dall'applicazione, se l'utente o l'applicazone non vengono trovati restituisce null</param>
        /// <returns>Utente rimosso</returns>
        public ApplicationUser RemoveUserCurrentAppAndGroup(string userId, string applicationId)
        {
            var user = userRepository.FindByID(userId);
            if (user != null)
            {
                // Rimuove tutti per fare eventuale autopulizia
                var application = applicationRepository.FindByID(applicationId);
                if (application != null)
                {
                    // Rimuove dall'applicazione
                    application.Users.Remove(user);
                    var role = FindUserRoleCurrentApp(user);
                    if (role != null)
                    {
                        var u = role.Users.Where(x => x.UserId == userId).SingleOrDefault();
                        if (u != null)
                        {
                            // Rimuove da gruppo collegato all'applicazione
                            role.Users.Remove(u);
                        }
                    }
                    DBContext.SaveChanges();
                }
            }
            return user;
        }

        /// <summary>
        /// Ricerca l'utente per id, identifica l'applicazione tramite application name presente nll'url.
        /// Ricerca il ruolo di un utente e restituisce la relativa entity
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public ApplicationRole FindUserRoleCurrentApp(string userId)
        {
            var user = userRepository.FindByID(userId);
            return FindUserRoleCurrentApp(user);
        }

        /// <summary>
        /// Ricerca l'utente, identifica l'applicazione tramite application name presente nll'url.
        /// Ricerca il ruolo di un utente e restituisce la relativa entity
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public ApplicationRole FindUserRoleCurrentApp(ApplicationUser user)
        {
            var appId = MultiTenantsHelper.ApplicationId;
            var appRoles = RolesInApplication(appId);
            var userRoles = user.Roles.Where(x => appRoles.Where(y => y.Users.Where(u => u.RoleId == x.RoleId).Any()).Any()).SingleOrDefault();
            if (userRoles != null)
            {
                var role = DBContext.Roles.Find(userRoles.RoleId);
                return role;
            }
            return null;
        }

        //public ApplicationRole GetUserRole(ApplicationUser user)
        //{
        //    using (var db = new DbContext())
        //    {

        //    }

        //    //var appId = MultiTenantsHelper.ApplicationId;
        //    //var appRoles = RolesInApplication(appId);
        //    //var userRoles = user.Roles.Where(x => appRoles.Where(y => y.Users.Where(u => u.RoleId == x.RoleId).Any()).Any()).SingleOrDefault();
        //    //if (userRoles != null)
        //    //{
        //    //    var role = DBContext.Roles.Find(userRoles.RoleId);
        //    //    return role;
        //    //}
        //    //return null;
        //}

        public ApplicationRole GetCurrentApplicationUserRole()
        {
            string userId = HttpContext.Current.User.Identity.GetUserId();
            var r = FindUserRoleCurrentApp(userId);
            return r;
        }

        public bool IsUserInApplication(string userId, string applicationTenantName)
        {
            var isInApplicationTenant = (from user in DBContext.Users
                                         where user.Id == userId && user.Applications.Any(x => x.Name == applicationTenantName)
                                         select user).Any();
            return isInApplicationTenant;
        }

        public int GetCurrentApplicationUsersCount()
        {
            var appId = MultiTenantsHelper.GetApplicationId(DBContext);
            return DBContext.AspNetApplications.Where(x => x.Id == appId).Count();
        }

        public void SaveRole(ApplicationRole role)
        {
            roleRepository.Update(role);
            DBContext.SaveChanges();
        }

        public ApplicationRole FindRoleByIdInCurrentApplication(string roleId)
        {
            var applicationId = MultiTenantsHelper.GetApplicationId(DBContext);
            return FindRoleById(applicationId, roleId);
        }

        public ApplicationRole FindRoleById(string applicationId, string roleId)
        {
            var res = (from r in DBContext.Roles
                       where r.ApplicationId == applicationId && r.Id == roleId
                       select r).SingleOrDefault();
            return res;
        }

        public List<ApplicationRole> RolesInCurrentApplication()
        {
            var applicationId = MultiTenantsHelper.GetApplicationId(DBContext);
            return RolesInApplication(applicationId);
        }

        public List<ApplicationRole> RolesInCurrentApplication(List<string> excludeRoles)
        {
            var roles = RolesInCurrentApplication().Where(x => !excludeRoles.Contains(x.Name)).ToList();
            return roles;
        }

        public List<ApplicationRole> RolesInApplication(string applicationId)
        {
            var res = (from r in DBContext.Roles
                       where r.ApplicationId == applicationId
                       select r).ToList();
            return res;
        }

        public string CreateApplication(string applicationName, string userName, string password)
        {
            return CreateApplication(applicationName, userName, password, null, false);
        }

        public string CreateApplication(string applicationName, string userName, string password, string applicationId)
        {
            return CreateApplication(applicationName, userName, password, applicationId, false);
        }

        public string CreateApplication(string applicationName, string userName, string password, string applicationId, bool isHostApplication)
        {
            // Verifica applicazione già esistente
            if (applicationRepository.ApplicationExists(applicationName))
            {
                throw new Exception("Nome applicazione già esistente.");
            }

            // Verifica utente già esistente
            if (userRepository.UserExists(userName))
            {
                throw new Exception("Nome utente già esistente.");
            }

            // Crea l'applicazione Demo, l'utente Admin e il gruppo Admin
            AspNetApplications application = new AspNetApplications()
            {
                URL = "/" + applicationName,
                Name = applicationName
            };
            if (!string.IsNullOrEmpty(applicationId))
            {
                application.Id = applicationId;
            }
            DBContext.AspNetApplications.Add(application);

            ApplicationRole newRoleUser;
            if (isHostApplication)
            {
                var roleStore = new ApplicationRoleStore<ApplicationRole>(application.Id, DBContext);
                var roleManager = new MFRoleManager<ApplicationRole>(roleStore);
                // Add Host
                newRoleUser = new ApplicationRole(MCFConstants.ROLE_HOST, MCFConstants.ROLE_HOST);
                newRoleUser.AspNetApplication = application;
                newRoleUser.AllowAddArea = newRoleUser.AllowAddWidget = newRoleUser.AllowDeleteArea = newRoleUser.AllowDeleteWidget = newRoleUser.AllowEditArea = newRoleUser.AllowEditWidget = newRoleUser.AllowPublicArea = true;
                newRoleUser.ShowWidgetAdvanced = newRoleUser.ShowWidgetAggregator = true;
                roleManager.Create(newRoleUser);
            }
            else
            {
                ApplicationRole newRole;
                // Add Guests
                newRole = new ApplicationRole(MCFConstants.ROLE_GUESTS, MCFConstants.ROLE_GUESTS);
                newRole.ApplicationId = application.Id;
                DBContext.Roles.Add(newRole);

                // Add Reseller
                newRole = new ApplicationRole(MCFConstants.ROLE_RESELLER, MCFConstants.ROLE_RESELLER);
                newRole.ApplicationId = application.Id;
                DBContext.Roles.Add(newRole);

                //Add Operator
                newRoleUser = new ApplicationRole(MCFConstants.ROLE_OPERATOR, MCFConstants.ROLE_OPERATOR);
                newRoleUser.ApplicationId = application.Id;
                DBContext.Roles.Add(newRoleUser);
                
                // Add admin
                newRoleUser = new ApplicationRole(MCFConstants.ROLE_ADMIN, MCFConstants.ROLE_ADMIN);
                newRoleUser.ApplicationId = application.Id;
                newRoleUser.AllowAddArea = newRoleUser.AllowAddWidget = newRoleUser.AllowDeleteArea = newRoleUser.AllowDeleteWidget = newRoleUser.AllowEditArea = newRoleUser.AllowEditWidget = newRoleUser.AllowPublicArea = true;
                DBContext.Roles.Add(newRoleUser);
            }

            // Create users
            var userStore = new ApplicationUserStore<ApplicationUser>(DBContext) { ApplicationId = applicationId };
            var userManager = new UserManager<ApplicationUser>(userStore);

            var newUser = new ApplicationUser()
            {
                UserName = userName,
                Applications = new List<AspNetApplications>()
            };
            // ERRORE IGNOTO DI VALIDAZIONE userManager.Create(newUser, "Password1!");

            DBContext.Users.Add(newUser);
            newUser.Applications.Add(application);
            newUser.Roles.Add(new IdentityUserRole() { UserId = newUser.Id, RoleId = newRoleUser.Id});
            DBContext.SaveChanges();

            // Set user attributes
            userManager.AddPassword(newUser.Id, password);
            userManager.SetLockoutEnabled(newUser.Id, false);

            if (!isHostApplication)
            {
                ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.LicenseMaxUser, 1, application.Id);
                ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.LicenseMaxAnalyzer, 10, application.Id);
                ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.LicenseMaxGateway, 1, application.Id);
                ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.LicenzaMaxPlant, 1, application.Id);
                ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.LicenseWidgetAdvanced, false, application.Id);
                ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.LicenseWidgetAggregator, false, application.Id);
                ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.EnergyParameterFilter, "", application.Id);
                //ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.OperatorDefaultApplicationUID, "", application.Id);

                ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.InstalledModuleEnabled, "", application.Id);
                ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.RemoteServerUrl, "", application.Id);
                ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.BasicAuthUser, "main_api_rest_user", application.Id);
                ApplicationSettingsHelper.UpdateSettings(ApplicationSettingsKey.BasicAuthPassword, "main_api_rest_password", application.Id);

                //Create default areas
                InitializeApplicationData(application.Id);
            }
            return application.Id;
        }

        // TODO: Rimuovere in favore della chiamata sincronizza widget
        public void InitializeWidgetCatalogue()
        {
            if (!DBContext.WidgetSet.Any())
            {
                DBContext.WidgetSet.Add(new Widget()
                {
                    Name = "AreaChart",
                    WidgetName = "AreaChart",
                    WidgetPath = "AreaChart",
                    Description = "AreaChart",
                    ImageUrl = "_.jpg",
                    Icon = "",
                    Category = "General",
                    Manifest = ""
                });
                DBContext.WidgetSet.Add(new Widget()
                {
                    Name = "AssetList",
                    WidgetName = "AssetList",
                    WidgetPath = "AssetList",
                    Description = "AssetList",
                    ImageUrl = "_.jpg",
                    Icon = "",
                    Category = "General",
                    Manifest = ""
                });
                DBContext.WidgetSet.Add(new Widget()
                {
                    Name = "AssetDetails",
                    WidgetName = "AssetDetails",
                    WidgetPath = "AssetDetails",
                    Description = "AssetDetails",
                    ImageUrl = "_.jpg",
                    Icon = "",
                    Category = "General",
                    Manifest = ""
                });
                DBContext.WidgetSet.Add(new Widget()
                {
                    Name = "CounterActive",
                    WidgetName = "CounterActive",
                    WidgetPath = "CounterActive",
                    Description = "CounterActive",
                    ImageUrl = "_.jpg",
                    Icon = "",
                    Category = "General",
                    Manifest = ""
                });
                DBContext.WidgetSet.Add(new Widget()
                {
                    Name = "CounterReactive",
                    WidgetName = "CounterReactive",
                    WidgetPath = "CounterReactive",
                    Description = "CounterReactive",
                    ImageUrl = "_.jpg",
                    Icon = "",
                    Category = "General",
                    Manifest = ""
                });
                DBContext.WidgetSet.Add(new Widget()
                {
                    Name = "RealPower",
                    WidgetName = "RealPower",
                    WidgetPath = "RealPower",
                    Description = "RealPower",
                    ImageUrl = "_.jpg",
                    Icon = "",
                    Category = "General",
                    Manifest = ""
                });
                DBContext.WidgetSet.Add(new Widget()
                {
                    Name = "RealTimeChart",
                    WidgetName = "RealTimeChart",
                    WidgetPath = "RealTimeChart",
                    Description = "RealTimeChart",
                    ImageUrl = "_.jpg",
                    Icon = "",
                    Category = "General",
                    Manifest = ""
                });
                DBContext.WidgetSet.Add(new Widget()
                {
                    Name = "History",
                    WidgetName = "History",
                    WidgetPath = "History",
                    Description = "History",
                    ImageUrl = "_.jpg",
                    Icon = "",
                    Category = "General",
                    Manifest = ""
                });
                DBContext.WidgetSet.Add(new Widget()
                {
                    Name = "Costi",
                    WidgetName = "Costi",
                    WidgetPath = "Costi",
                    Description = "Costi",
                    ImageUrl = "_.jpg",
                    Icon = "",
                    Category = "General",
                    Manifest = ""
                });
                DBContext.WidgetSet.Add(new Widget()
                {
                    Name = "Gantt",
                    WidgetName = "Gantt",
                    WidgetPath = "Gantt",
                    Description = "Gantt",
                    ImageUrl = "_.jpg",
                    Icon = "",
                    Category = "General",
                    Manifest = ""
                });
                DBContext.WidgetSet.Add(new Widget()
                {
                    Name = "MappaAssistenza",
                    WidgetName = "MappaAssistenza",
                    WidgetPath = "MappaAssistenza",
                    Description = "MappaAssistenza",
                    ImageUrl = "_.jpg",
                    Icon = "",
                    Category = "General",
                    Manifest = ""
                });

                DBContext.SaveChanges();
            }
        }

        public void InitializeApplicationData(string applicationId)
        {
            #region Aggiunta aree di default
            //if (!DBContext.WidgetAreaSet.Any())
            //{
            string pathId = string.Empty;
            DBContext.WidgetAreaSet.Add(new WidgetArea()
            {
                Description = "Elenco assets",
                Icon = "fa-align-justify",
                Title = "Assets",
                Visible = true,
                RoutePath = "Assets",
                Template = "Layout2Cols.ascx",
                Starred = true,
                Order = 0,
                ApplicationId = applicationId
            });
            pathId = Guid.NewGuid().ToString();
            DBContext.AspNetPaths.Add(new AspNetPaths()
            {
                Id = pathId,
                ApplicationId = applicationId,
                Path = "~/App/" + DBContext.AspNetApplications.Where(app => app.Id == applicationId).FirstOrDefault().Name + "/Area/Assets"
            });
            DBContext.AspNetPersonalizationAllUsers.Add(new Models.AspNetPersonalizationAllUsers()
            {
                PathId = pathId,
                PageSettings = "/wEUKwAkAgICAxkpjgFNYWluQ2xvdWRGcmFtZXdvcmsuV2ViLlVJLldlYkNvbnRyb2xzLldlYlBhcnRzLldpZGdldFdlYlBhcnRNYW5hZ2VyLCBNYWluQ2xvdWRGcmFtZXdvcmssIFZlcnNpb249MS4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsBQVfX3dwbWYCAh4SV2ViUGFydFN0YXRlU2hhcmVkFCsACAULd3A0Mjk5MDM1MTAFBnpvbmVfMWZoBQt3cDc5MDI4MzcyNwUGem9uZV8yZmhnaB4VRHluYW1pY1dlYlBhcnRzU2hhcmVkFCsACAULd3A0Mjk5MDM1MTAFLk1haW5DbG91ZEZyYW1ld29yay5VSS5Db250YWluZXJzLldpZGdldFdlYlBhcnRkZAULd3A3OTAyODM3MjcFLk1haW5DbG91ZEZyYW1ld29yay5VSS5Db250YWluZXJzLldpZGdldFdlYlBhcnRkZGdoBQt3cDQyOTkwMzUxMAIFHg9Vc2VyQ29udHJvbE5hbWUFCUFzc2V0TGlzdB4PVXNlckNvbnRyb2xQYXRoBQlBc3NldExpc3QeIlBlcnNpc3RlZFdpZGdldEN1c3RvbUNvbmZpZ3VyYXRpb24FI3siUGFyYW1zIjp7IkJlaGF2aW91ciI6IlJlZGlyZWN0In19HgVUaXRsZQUJQXNzZXRMaXN0Hg5Db250YWluZXJTdHlsZQUNcGFuZWwtZGVmYXVsdGYFC3dwNzkwMjgzNzI3AgMfAgUMQXNzZXREZXRhaWxzHwMFDEFzc2V0RGV0YWlscx8FBQxBc3NldERldGFpbHNm",
                LastUpdatedDate = DateTime.Now
            });

            DBContext.WidgetAreaSet.Add(new WidgetArea()
            {
                Description = "Situazione real time",
                Icon = "fa-area-chart",
                Title = "RealTime",
                Visible = true,
                RoutePath = "RealTime",
                Template = "Layout2Rows4Cols.ascx",
                Starred = true,
                Order = 1,
                ApplicationId = applicationId
            });
            pathId = Guid.NewGuid().ToString();
            DBContext.AspNetPaths.Add(new AspNetPaths()
            {
                Id = pathId,
                ApplicationId = applicationId,
                Path = "~/App/" + DBContext.AspNetApplications.Where(app => app.Id == applicationId).FirstOrDefault().Name + "/Area/RealTime"
            });
            DBContext.AspNetPersonalizationAllUsers.Add(new Models.AspNetPersonalizationAllUsers()
            {
                PathId = pathId,
                PageSettings = "/wEUKwA7AgICBhkpjgFNYWluQ2xvdWRGcmFtZXdvcmsuV2ViLlVJLldlYkNvbnRyb2xzLldlYlBhcnRzLldpZGdldFdlYlBhcnRNYW5hZ2VyLCBNYWluQ2xvdWRGcmFtZXdvcmssIFZlcnNpb249MS4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsBQVfX3dwbWYCAh4SV2ViUGFydFN0YXRlU2hhcmVkFCsAFAULd3AxMDYwOTY1ODYFBnpvbmVfMmZoBQx3cDE0MzI1ODkyNTQFBnpvbmVfM2ZoBQx3cDE2Nzk0MzI5MDcFBnpvbmVfNGZoBQp3cDY5ODgwNzU4BQZ6b25lXzECAWgFDHdwMTQ3Mjc1OTQ2MwUGem9uZV8xZmhnaB4VRHluYW1pY1dlYlBhcnRzU2hhcmVkFCsAFAULd3AxMDYwOTY1ODYFLk1haW5DbG91ZEZyYW1ld29yay5VSS5Db250YWluZXJzLldpZGdldFdlYlBhcnRkZAUMd3AxNDMyNTg5MjU0BS5NYWluQ2xvdWRGcmFtZXdvcmsuVUkuQ29udGFpbmVycy5XaWRnZXRXZWJQYXJ0ZGQFDHdwMTY3OTQzMjkwNwUuTWFpbkNsb3VkRnJhbWV3b3JrLlVJLkNvbnRhaW5lcnMuV2lkZ2V0V2ViUGFydGRkBQp3cDY5ODgwNzU4BS5NYWluQ2xvdWRGcmFtZXdvcmsuVUkuQ29udGFpbmVycy5XaWRnZXRXZWJQYXJ0ZGQFDHdwMTQ3Mjc1OTQ2MwUuTWFpbkNsb3VkRnJhbWV3b3JrLlVJLkNvbnRhaW5lcnMuV2lkZ2V0V2ViUGFydGRkZ2gFC3dwMTA2MDk2NTg2AgMeD1VzZXJDb250cm9sTmFtZQUJUmVhbFBvd2VyHg9Vc2VyQ29udHJvbFBhdGgFCVJlYWxQb3dlch4FVGl0bGUFCVJlYWxQb3dlcmYFDHdwMTQzMjU4OTI1NAIDHwIFBUNvc2ZpHwMFBUNvc2ZpHwQFBUNvc2ZpZgUMd3AxNjc5NDMyOTA3AgMfAgUNUmVhbFRpbWVDaGFydB8DBQ1SZWFsVGltZUNoYXJ0HwQFDVJlYWxUaW1lQ2hhcnRmBQp3cDY5ODgwNzU4AgMfAgUPQ291bnRlclJlYWN0aXZlHwMFD0NvdW50ZXJSZWFjdGl2ZR8EBQ9Db3VudGVyUmVhY3RpdmVmBQx3cDE0NzI3NTk0NjMCAx8CBQ1Db3VudGVyQWN0aXZlHwMFDUNvdW50ZXJBY3RpdmUfBAUNQ291bnRlckFjdGl2ZWY=",
                LastUpdatedDate = DateTime.Now
            });

            DBContext.WidgetAreaSet.Add(new WidgetArea()
            {
                Description = "Situazione storica",
                Icon = "fa-bar-chart",
                Title = "Storico",
                Visible = true,
                RoutePath = "Storico",
                Template = "Layout2Rows.ascx",
                Starred = true,
                Order = 2,
                ApplicationId = applicationId
            });
            pathId = Guid.NewGuid().ToString();
            DBContext.AspNetPaths.Add(new AspNetPaths()
            {
                Id = pathId,
                ApplicationId = applicationId,
                Path = "~/App/" + DBContext.AspNetApplications.Where(app => app.Id == applicationId).FirstOrDefault().Name + "/Area/Storico"
            });
            DBContext.AspNetPersonalizationAllUsers.Add(new Models.AspNetPersonalizationAllUsers()
            {
                PathId = pathId,
                PageSettings = "/wEUKwAoAgICAxkpjgFNYWluQ2xvdWRGcmFtZXdvcmsuV2ViLlVJLldlYkNvbnRyb2xzLldlYlBhcnRzLldpZGdldFdlYlBhcnRNYW5hZ2VyLCBNYWluQ2xvdWRGcmFtZXdvcmssIFZlcnNpb249MS4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsBQVfX3dwbWYCAh4SV2ViUGFydFN0YXRlU2hhcmVkFCsACAULd3AyMDIyNjUzNDYFBnpvbmVfMWZoBQt3cDc2MTY4NTAyOQUGem9uZV8yZmhnaB4VRHluYW1pY1dlYlBhcnRzU2hhcmVkFCsACAULd3AyMDIyNjUzNDYFLk1haW5DbG91ZEZyYW1ld29yay5VSS5Db250YWluZXJzLldpZGdldFdlYlBhcnRkZAULd3A3NjE2ODUwMjkFLk1haW5DbG91ZEZyYW1ld29yay5VSS5Db250YWluZXJzLldpZGdldFdlYlBhcnRkZGdoBQt3cDIwMjI2NTM0NgIFHg9Vc2VyQ29udHJvbE5hbWUFB0hpc3RvcnkeD1VzZXJDb250cm9sUGF0aAUHSGlzdG9yeR4OQ29udGFpbmVyU3R5bGUFDXBhbmVsLXN1Y2Nlc3MeBVRpdGxlBQdIaXN0b3J5HiJQZXJzaXN0ZWRXaWRnZXRDdXN0b21Db25maWd1cmF0aW9uBW97IlBhcmFtcyI6eyJQYXJhbWV0ZXJDb2RlIjoiVG90YWxBY3RpdmVQb3dlciIsIlR5cGVPZlRpbWUiOiJEYXkiLCJCZWhhdmlvdXIiOiJDaGFydCIsIkNoYXJ0VHlwZSI6IkFyZWFTZXJpZXMifX1mBQt3cDc2MTY4NTAyOQIFHwIFB0hpc3RvcnkfAwUHSGlzdG9yeR8EBQpwYW5lbC1pbmZvHwUFB0hpc3RvcnkfBgV1eyJQYXJhbXMiOnsiQmVoYXZpb3VyIjoiVGFibGUiLCJDaGFydFR5cGUiOiJMaW5lU2VyaWVzIiwiUGFyYW1ldGVyQ29kZSI6IlRvdGFsQWN0aXZlUG93ZXIiLCJUeXBlT2ZUaW1lIjoiTGFzdE1vbnRoIn19Zg==",
                LastUpdatedDate = DateTime.Now
            });

            DBContext.WidgetAreaSet.Add(new WidgetArea()
            {
                Description = "Analisi",
                Icon = "fa-line-chart",
                Title = "Analisi",
                Visible = true,
                RoutePath = "Analisi",
                Template = "Layout2Rows.ascx",
                Starred = true,
                Order = 3,
                ApplicationId = applicationId
            });
            pathId = Guid.NewGuid().ToString();
            DBContext.AspNetPaths.Add(new AspNetPaths()
            {
                Id = pathId,
                ApplicationId = applicationId,
                Path = "~/App/" + DBContext.AspNetApplications.Where(app => app.Id == applicationId).FirstOrDefault().Name + "/Area/Analisi"
            });
            DBContext.AspNetPersonalizationAllUsers.Add(new Models.AspNetPersonalizationAllUsers()
            {
                PathId = pathId,
                PageSettings = "/wEUKwAoAgICAxkpjgFNYWluQ2xvdWRGcmFtZXdvcmsuV2ViLlVJLldlYkNvbnRyb2xzLldlYlBhcnRzLldpZGdldFdlYlBhcnRNYW5hZ2VyLCBNYWluQ2xvdWRGcmFtZXdvcmssIFZlcnNpb249MS4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsBQVfX3dwbWYCAh4SV2ViUGFydFN0YXRlU2hhcmVkFCsACAULd3A4NTczMDcyODcFBnpvbmVfMWZoBQx3cDEyNzE5NDUwOTgFBnpvbmVfMmZoZ2geFUR5bmFtaWNXZWJQYXJ0c1NoYXJlZBQrAAgFC3dwODU3MzA3Mjg3BS5NYWluQ2xvdWRGcmFtZXdvcmsuVUkuQ29udGFpbmVycy5XaWRnZXRXZWJQYXJ0ZGQFDHdwMTI3MTk0NTA5OAUuTWFpbkNsb3VkRnJhbWV3b3JrLlVJLkNvbnRhaW5lcnMuV2lkZ2V0V2ViUGFydGRkZ2gFC3dwODU3MzA3Mjg3AgUeD1VzZXJDb250cm9sTmFtZQUIQW5hbHlzaXMeD1VzZXJDb250cm9sUGF0aAUIQW5hbHlzaXMeDkNvbnRhaW5lclN0eWxlBQ1wYW5lbC1wcmltYXJ5HgVUaXRsZQUIQW5hbHlzaXMeIlBlcnNpc3RlZFdpZGdldEN1c3RvbUNvbmZpZ3VyYXRpb24FUXsiUGFyYW1zIjp7IlR5cGVPZlRpbWUiOiJEYXkiLCJQYXJhbWV0ZXJDb2RlIjoiVG90YWxBY3RpdmVQb3dlciIsIkJlaGF2aW91ciI6IiJ9fWYFDHdwMTI3MTk0NTA5OAIFHwIFEUhpc3RvcnlDb21wYXJpc29uHwMFEUhpc3RvcnlDb21wYXJpc29uHwYFG3siUGFyYW1zIjp7IkJlaGF2aW91ciI6IiJ9fR8FBRFIaXN0b3J5Q29tcGFyaXNvbh8EBQpwYW5lbC1pbmZvZg==",
                LastUpdatedDate = DateTime.Now
            });

            DBContext.WidgetAreaSet.Add(new WidgetArea()
            {
                Description = "Costi",
                Icon = "fa-eur",
                Title = "Costi",
                Visible = true,
                RoutePath = "Costi",
                Template = "Layout2Rows.ascx",
                Starred = true,
                Order = 4,
                ApplicationId = applicationId
            });
            pathId = Guid.NewGuid().ToString();
            DBContext.AspNetPaths.Add(new AspNetPaths()
            {
                Id = pathId,
                ApplicationId = applicationId,
                Path = "~/App/" + DBContext.AspNetApplications.Where(app => app.Id == applicationId).FirstOrDefault().Name + "/Area/Costi"
            });
            DBContext.AspNetPersonalizationAllUsers.Add(new Models.AspNetPersonalizationAllUsers()
            {
                PathId = pathId,
                PageSettings = "/wEUKwA1AgICBBkpjgFNYWluQ2xvdWRGcmFtZXdvcmsuV2ViLlVJLldlYkNvbnRyb2xzLldlYlBhcnRzLldpZGdldFdlYlBhcnRNYW5hZ2VyLCBNYWluQ2xvdWRGcmFtZXdvcmssIFZlcnNpb249MS4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsBQVfX3dwbWYCAh4SV2ViUGFydFN0YXRlU2hhcmVkFCsADAUMd3AxNjIzMDI5Mjg3BQZ6b25lXzECAWgFC3dwNjc2ODgxNTI2BQZ6b25lXzJmaAULd3A5ODY5NTQwOTMFBnpvbmVfMWZoZ2geFUR5bmFtaWNXZWJQYXJ0c1NoYXJlZBQrAAwFDHdwMTYyMzAyOTI4NwUuTWFpbkNsb3VkRnJhbWV3b3JrLlVJLkNvbnRhaW5lcnMuV2lkZ2V0V2ViUGFydGRkBQt3cDY3Njg4MTUyNgUuTWFpbkNsb3VkRnJhbWV3b3JrLlVJLkNvbnRhaW5lcnMuV2lkZ2V0V2ViUGFydGRkBQt3cDk4Njk1NDA5MwUuTWFpbkNsb3VkRnJhbWV3b3JrLlVJLkNvbnRhaW5lcnMuV2lkZ2V0V2ViUGFydGRkZ2gFDHdwMTYyMzAyOTI4NwIFHg9Vc2VyQ29udHJvbE5hbWUFBUNvc3RpHg9Vc2VyQ29udHJvbFBhdGgFBUNvc3RpHg5Db250YWluZXJTdHlsZQUNcGFuZWwtZGVmYXVsdB4FVGl0bGUFFkdyYWZpY28gY29uc3VtaSBpbiBLV2geIlBlcnNpc3RlZFdpZGdldEN1c3RvbUNvbmZpZ3VyYXRpb24FHnsiUGFyYW1zIjp7IkJlaGF2aW91ciI6IktXaCJ9fWYFC3dwNjc2ODgxNTI2AgUfAgUFQ29zdGkfAwUFQ29zdGkfBgUgeyJQYXJhbXMiOnsiQmVoYXZpb3VyIjoiY29zdGkifX0fBQUFQ29zdGkfBAUNcGFuZWwtZGVmYXVsdGYFC3dwOTg2OTU0MDkzAgUfAgUKVGFibGVDb3N0aR8DBQVDb3N0aR8EBQ1wYW5lbC13YXJuaW5nHwUFFFRhYmxlIGNvbnN1bWkgaW4gS1doHwYFG3siUGFyYW1zIjp7IkJlaGF2aW91ciI6IiJ9fWY=",
                LastUpdatedDate = DateTime.Now
            });

            DBContext.WidgetAreaSet.Add(new WidgetArea()
            {
                Description = "Allarmi profili di carico",
                Icon = "fa-exclamation-triangle",
                Title = "Allarmi",
                Visible = true,
                RoutePath = "Allarmi",
                Template = "Layout2Cols.ascx",
                Starred = true,
                Order = 5,
                ApplicationId = applicationId
            });
            pathId = Guid.NewGuid().ToString();
            DBContext.AspNetPaths.Add(new AspNetPaths()
            {
                Id = pathId,
                ApplicationId = applicationId,
                Path = "~/App/" + DBContext.AspNetApplications.Where(app => app.Id == applicationId).FirstOrDefault().Name + "/Area/Allarmi"
            });
            DBContext.AspNetPersonalizationAllUsers.Add(new Models.AspNetPersonalizationAllUsers()
            {
                PathId = pathId,
                PageSettings = "/wEUKwAkAgICAxkpjgFNYWluQ2xvdWRGcmFtZXdvcmsuV2ViLlVJLldlYkNvbnRyb2xzLldlYlBhcnRzLldpZGdldFdlYlBhcnRNYW5hZ2VyLCBNYWluQ2xvdWRGcmFtZXdvcmssIFZlcnNpb249MS4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsBQVfX3dwbWYCAh4SV2ViUGFydFN0YXRlU2hhcmVkFCsACAUMd3AyMDc1MjUwOTM1BQZ6b25lXzFmaAUMd3AxNzMwNzcxOTYwBQZ6b25lXzJmaGdoHhVEeW5hbWljV2ViUGFydHNTaGFyZWQUKwAIBQx3cDIwNzUyNTA5MzUFLk1haW5DbG91ZEZyYW1ld29yay5VSS5Db250YWluZXJzLldpZGdldFdlYlBhcnRkZAUMd3AxNzMwNzcxOTYwBS5NYWluQ2xvdWRGcmFtZXdvcmsuVUkuQ29udGFpbmVycy5XaWRnZXRXZWJQYXJ0ZGRnaAUMd3AyMDc1MjUwOTM1AgMeD1VzZXJDb250cm9sTmFtZQUQTG9hZFByb2ZpbGVDaGFydB4PVXNlckNvbnRyb2xQYXRoBRBMb2FkUHJvZmlsZUNoYXJ0HgVUaXRsZQUQTG9hZFByb2ZpbGVDaGFydGYFDHdwMTczMDc3MTk2MAIFHwIFEExvYWRQcm9maWxlQWxhcm0fAwUQTG9hZFByb2ZpbGVDaGFydB4OQ29udGFpbmVyU3R5bGUFDHBhbmVsLWRhbmdlch8EBRBMb2FkUHJvZmlsZUFsYXJtHiJQZXJzaXN0ZWRXaWRnZXRDdXN0b21Db25maWd1cmF0aW9uBRt7IlBhcmFtcyI6eyJCZWhhdmlvdXIiOiIifX1m",
                LastUpdatedDate = DateTime.Now
            });

            DBContext.WidgetAreaSet.Add(new WidgetArea()
            {
                Description = "Schedulatore",
                Icon = "fa-calendar",
                Title = "Pianificazione",
                Visible = true,
                RoutePath = "Pianificazione",
                Template = "Layout2Rows.ascx",
                Starred = true,
                Order = 6,
                ApplicationId = applicationId
            });
            pathId = Guid.NewGuid().ToString();
            DBContext.AspNetPaths.Add(new AspNetPaths()
            {
                Id = pathId,
                ApplicationId = applicationId,
                Path = "~/App/" + DBContext.AspNetApplications.Where(app => app.Id == applicationId).FirstOrDefault().Name + "/Area/Pianificazione"
            });
            DBContext.AspNetPersonalizationAllUsers.Add(new Models.AspNetPersonalizationAllUsers()
            {
                PathId = pathId,
                PageSettings = "/wEUKwAgAgICAxkpjgFNYWluQ2xvdWRGcmFtZXdvcmsuV2ViLlVJLldlYkNvbnRyb2xzLldlYlBhcnRzLldpZGdldFdlYlBhcnRNYW5hZ2VyLCBNYWluQ2xvdWRGcmFtZXdvcmssIFZlcnNpb249MS4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsBQVfX3dwbWYCAh4SV2ViUGFydFN0YXRlU2hhcmVkFCsACAULd3A5ODA5ODAxNzIFBnpvbmVfMWZoBQx3cDIwNDM4NzI3NzYFBnpvbmVfMmZoZ2geFUR5bmFtaWNXZWJQYXJ0c1NoYXJlZBQrAAgFC3dwOTgwOTgwMTcyBS5NYWluQ2xvdWRGcmFtZXdvcmsuVUkuQ29udGFpbmVycy5XaWRnZXRXZWJQYXJ0ZGQFDHdwMjA0Mzg3Mjc3NgUuTWFpbkNsb3VkRnJhbWV3b3JrLlVJLkNvbnRhaW5lcnMuV2lkZ2V0V2ViUGFydGRkZ2gFC3dwOTgwOTgwMTcyAgMeD1VzZXJDb250cm9sTmFtZQUFR2FudHQeD1VzZXJDb250cm9sUGF0aAUFR2FudHQeBVRpdGxlBQVHYW50dGYFDHdwMjA0Mzg3Mjc3NgIDHwIFD01hcHBhQXNzaXN0ZW56YR8DBQ9NYXBwYUFzc2lzdGVuemEfBAUPTWFwcGFBc3Npc3RlbnphZg==",
                LastUpdatedDate = DateTime.Now
            });

            DBContext.WidgetAreaSet.Add(new WidgetArea()
            {
                Description = "Riepilogo",
                Icon = "fa-archive",
                Title = "Riepilogo",
                Visible = true,
                RoutePath = "Riepilogo",
                Template = "Default.ascx",
                Starred = true,
                Order = 7,
                ApplicationId = applicationId
            });
            pathId = Guid.NewGuid().ToString();
            DBContext.AspNetPaths.Add(new AspNetPaths()
            {
                Id = pathId,
                ApplicationId = applicationId,
                Path = "~/App/" + DBContext.AspNetApplications.Where(app => app.Id == applicationId).FirstOrDefault().Name + "/Area/Riepilogo"
            });
            DBContext.AspNetPersonalizationAllUsers.Add(new Models.AspNetPersonalizationAllUsers()
            {
                PathId = pathId,
                PageSettings = "/wEUKwAbAgICAhkpjgFNYWluQ2xvdWRGcmFtZXdvcmsuV2ViLlVJLldlYkNvbnRyb2xzLldlYlBhcnRzLldpZGdldFdlYlBhcnRNYW5hZ2VyLCBNYWluQ2xvdWRGcmFtZXdvcmssIFZlcnNpb249MS4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsBQVfX3dwbWYCAh4SV2ViUGFydFN0YXRlU2hhcmVkFCsABAUMd3AyMTI3OTQyNDQzBQZ6b25lXzFmaGdoHhVEeW5hbWljV2ViUGFydHNTaGFyZWQUKwAEBQx3cDIxMjc5NDI0NDMFLk1haW5DbG91ZEZyYW1ld29yay5VSS5Db250YWluZXJzLldpZGdldFdlYlBhcnRkZGdoBQx3cDIxMjc5NDI0NDMCBR4IV2lkZ2V0SWQFGGl0Lm15Y3Jvcy5tYWluLnRvdGFsaXplch4PVXNlckNvbnRyb2xQYXRoBQlUb3RhbGl6ZXIeD1VzZXJDb250cm9sTmFtZQUJVG90YWxpemVyHiJQZXJzaXN0ZWRXaWRnZXRDdXN0b21Db25maWd1cmF0aW9uBTR7IlBhcmFtcyI6eyJCZWhhdmlvdXIiOiJUb3RhbEFjdGl2ZUVuZXJneUNvbnN1bWVkIn19HgVUaXRsZQUJVG90YWxpemVyZg==",
                LastUpdatedDate = DateTime.Now
            });
            //}
            #endregion

            DBContext.SaveChanges();
        }
    }
}