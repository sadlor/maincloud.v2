﻿using MainCloudFramework.RequestDBContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MainCloudFramework.Services
{
    public class BaseService<D> where D : DbContext
    {
        protected D DBContext
        {
            get
            {
                return new DbContextLocator<D>().DbContext;
            }
        }
    }
}
