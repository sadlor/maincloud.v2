﻿using MainCloudFramework.Models;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web;

namespace MainCloudFramework.Services
{
    public class GrantService : BaseService<ApplicationDbContext>
    {
        private ApplicationService applicationService = new ApplicationService();
        private WidgetAreaRepository widgetAreaRepository = new WidgetAreaRepository();

        private bool CheckMaxUsersLicense()
        {
            try
            {
                var appId = MultiTenantsHelper.ApplicationId;
                if (appId != null)
                {
                    object maxUser = ApplicationSettingsHelper.GetConfiguration(ApplicationSettingsKey.LicenseMaxUser, appId);
                    long maxUserLimit = 0; // No user
                        
                    if (maxUser is string)
                    {
                        if (!long.TryParse(maxUser as string, out maxUserLimit))
                        {
                            maxUserLimit = -1;
                        }
                    }
                    else
                    {
                        maxUserLimit = (long)maxUser;
                    }
                    return applicationService.GetCurrentApplicationUsersCount() < maxUserLimit;
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool AllowCreateUser()
        {
            return CheckMaxUsersLicense();
        }

        public bool AllowAddUser()
        {
            return CheckMaxUsersLicense();
        }

        /// <summary>
        /// Current user is Host
        /// </summary>
        /// <returns></returns>
        public bool IsHostUser()
        {
            string userId = HttpContext.Current.User.Identity.GetUserId();
            return IsHostUser(userId);
        }

        public bool IsHostUser(string userId)
        {
            string hostRoleId = (from r in DBContext.Roles
                                 where r.Name == MCFConstants.ROLE_HOST
                                 select r.Id).Single();

            var IsInRole = (from u in DBContext.Users
                            where u.Id == userId &&
                            (from r in u.Roles
                             where r.RoleId == hostRoleId
                             select r).Any()
                            select u.Id).Any();
            return IsInRole;
        }

        /// <summary>
        /// Current user is Admin for current application
        /// </summary>
        /// <returns></returns>
        public bool IsAdminUser()
        {
            string userId = HttpContext.Current.User.Identity.GetUserId();
            return IsAdminUser(userId);
        }

        public bool IsAdminUser(string userId)
        {
            var appId = MultiTenantsHelper.ApplicationId;

            string adminRoleId = (from r in DBContext.Roles
                                  where r.Name == MCFConstants.ROLE_ADMIN && r.ApplicationId == appId
                                  select r.Id).Single();

            var IsInRole = (from u in DBContext.Users
                            where u.Id == userId && u.Roles.Where(x => x.RoleId == adminRoleId).Any()
                            select u).Any();

            return IsInRole;
        }

        public bool IsInRole(string role)
        {
            try
            {
                string userId = HttpContext.Current.User.Identity.GetUserId();
                var userRole = applicationService.FindUserRoleCurrentApp(userId);
                return userRole != null && userRole.Name == role;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        //public string GetRole()
        //{
        //    try
        //    {
        //        string userId = HttpContext.Current.User.Identity.GetUserId();
        //        return applicationService.GetUserRole(userId).Id;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}

        //public static bool IsReadOnly
        //{
        //    get
        //    {
        //        return IsInRole(MCFConstants.ROLE_GUESTS);
        //    }
        //}

        private ApplicationRole CurrentApplicationUserRole()
        {
            string currenUserId = HttpContext.Current.User.Identity.GetUserId();
            var r = applicationService.FindUserRoleCurrentApp(currenUserId);
            return r;
        }

        public bool AllowAddArea()
        {
            var r = applicationService.GetCurrentApplicationUserRole();
            return IsHostUser() || IsAdminUser() || (r != null && r.AllowAddArea);
        }

        public bool AllowEditArea()
        {
            var r = applicationService.GetCurrentApplicationUserRole();
            return IsHostUser() || IsAdminUser() || (r != null && r.AllowEditArea);
        }

        public bool AllowDeleteArea()
        {
            var r = applicationService.GetCurrentApplicationUserRole();
            return IsHostUser() || IsAdminUser() || (r != null && r.AllowDeleteArea);
        }

        public bool AllowAddWidget()
        {
            var r = applicationService.GetCurrentApplicationUserRole();
            return IsHostUser() || IsAdminUser() || (r != null && r.AllowAddWidget);
        }

        public bool AllowEditWidget()
        {
            var r = applicationService.GetCurrentApplicationUserRole();
            return IsHostUser() || IsAdminUser() || (r != null && r.AllowEditWidget);
        }

        public bool AllowDeleteWidget()
        {
            var r = applicationService.GetCurrentApplicationUserRole();
            return IsHostUser() || IsAdminUser() || (r != null && r.AllowDeleteWidget);
        }

        public bool AllowPublicArea()
        {
            var r = applicationService.GetCurrentApplicationUserRole();
            return IsHostUser() || IsAdminUser() || (r != null && r.AllowPublicArea);
        }

        public bool AllowModifyArea(string ownerUserId)
        {
            bool allow = false;

            if (IsHostUser() || IsAdminUser())
            {                    
                allow = true; // Sempre vero
            }
            else
            {
                if (ownerUserId == null) // Area pubblica
                {
                    allow = AllowEditArea();
                }
                else // area privata
                {
                    // Verifica se l'utente è il proprietario dell'area
                    string currenUserId = HttpContext.Current.User.Identity.GetUserId();
                    allow = (ownerUserId == currenUserId);
                }
            }
            return allow;
        }

        public bool AllowDeleteArea(string ownerUserId)
        {
            bool allow = false;

            if (IsHostUser() || IsAdminUser())
            {
                allow = true; // Sempre vero
            }
            else
            {
                if (ownerUserId == null) // Area pubblica
                {
                    allow = AllowDeleteArea();
                }
                else // area privata
                {
                    // Verifica se l'utente è il proprietario dell'area
                    string currenUserId = HttpContext.Current.User.Identity.GetUserId();
                    allow = (ownerUserId == currenUserId);
                }
            }
            return allow;
        }

        public bool AllowInsertWidgetLayout(string routePath)
        {
            bool allow = false;

            if (IsHostUser() || IsAdminUser())
            {
                allow = true; // Sempre vero
            }
            else
            {
                var area = widgetAreaRepository.FindWidgetAreaByRoutePath(routePath);
                if (area != null)
                {
                    if (area.OwnerUserId == null) // Area pubblica
                    {
                        allow = AllowAddWidget();
                    }
                    else // area privata
                    {
                        // Verifica se l'utente è il proprietario dell'area
                        string currenUserId = HttpContext.Current.User.Identity.GetUserId();
                        allow = (area.OwnerUserId == currenUserId);
                    }
                }
            }
            return allow;
        }

        public bool ShowWidgetAdvanced()
        {
            var r = applicationService.GetCurrentApplicationUserRole();
            return IsHostUser() || IsAdminUser() || (r != null && r.ShowWidgetAdvanced);
        }

        public bool ShowWidgetAggregator()
        {
            var r = applicationService.GetCurrentApplicationUserRole();
            return IsHostUser() || IsAdminUser() || (r != null && r.ShowWidgetAggregator);
        }

        public bool IsAreaOwner(string routePath)
        {
            WidgetArea page = DBContext.WidgetAreaSet.Where(p => p.RoutePath == routePath && p.ApplicationId == MultiTenantsHelper.ApplicationId).SingleOrDefault();
            if (page != null)
            {
                return page.OwnerUserId == HttpContext.Current.User.Identity.GetUserId();
            }
            else
            {
                return false;
            }
        }
    }
}
