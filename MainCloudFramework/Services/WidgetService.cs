﻿using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web;
using MainCloudFramework.Web.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MainCloudFramework.Services
{
    public class WidgetService : BaseService<ApplicationDbContext>
    {
        private WidgetsRepository widgetsRepository = new WidgetsRepository();

        public void SyncWidgetsList()
        {
            var path = HttpContext.Current.Server.MapPath(MCFConstants.BASE_MODULES_PATH);
            List<Widget> ls = SyncWidgetsList(path);
            widgetsRepository.UpdateInstalledWidgets(ls);
        }

        private List<Widget> SyncWidgetsList(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            var dirs = di.GetDirectories();

            List<Widget> ls = new List<Widget>();

            foreach (var dir in dirs)
            {
                var files = dir.GetFiles("Manifest.json");
                if (files.Length == 1)
                {
                    var file = files[0];
                    JObject json = JObject.Parse(File.ReadAllText(file.FullName));
                    string rootWidgetPath = HttpContext.Current.Server.MapPath(MCFConstants.BASE_MODULES_PATH);
                    
                    Widget w = new Widget();
                    w.WidgetName = dir.Name; // Widget ascx base name same as folder
                    w.WidgetPath = dir.FullName.Replace(rootWidgetPath, ""); // Relative path

                    w.WidgetId = json.Value<string>("id");
                    w.Name = json.Value<string>("name"); // Title
                    w.Description = json.Value<string>("description");
                    w.Icon = json.Value<string>("iconClass");
                    w.ImageUrl = json.Value<string>("iconImageUrl");
                    w.Category = json.Value<string>("group");

                    w.Manifest = json.ToString();
                    ls.Add(w);
                }
                else
                {
                    ls.AddRange(SyncWidgetsList(dir.FullName));
                }
            }
            return ls;
        }

        public string GetModuleControlPath(string controlPath)
        {
            string urlPath = Path.Combine(MCFConstants.BASE_MODULES_PATH, controlPath);
            return urlPath;
        }
    }
}
