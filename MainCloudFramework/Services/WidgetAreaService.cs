﻿using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MainCloudFramework.Repositories.WidgetAreaRepository;

namespace MainCloudFramework.Services
{
    public class WidgetAreaService : BaseService<ApplicationDbContext>
    {
        private WidgetAreaRepository widgetAreaRepository = new WidgetAreaRepository();
        private GrantService grantService = new GrantService();

        public List<WidgetArea> FindAllUngrupedActiveAreaByApplicationAndUserId(string applicationId, string userId, string sortBy, bool sortAscending)
        {
            return widgetAreaRepository.FindAllActiveAreaByApplicationAndUserId(applicationId, userId, false, sortBy, sortAscending, grantService.IsHostUser(userId), true, FilterGroupAreaMode.ExludeGroupArea);
        }

        public List<WidgetArea> FindAllActiveAreaByApplicationAndUserId(string applicationId, string userId, string sortBy, bool sortAscending)
        {
            return widgetAreaRepository.FindAllActiveAreaByApplicationAndUserId(applicationId, userId, false, sortBy, sortAscending, grantService.IsHostUser(userId), false, FilterGroupAreaMode.All);
        }

        public List<WidgetArea> FindAllGroupedActiveAreaByApplicationAndUserId(string applicationId, string userId, string sortBy, bool sortAscending)
        {
            return widgetAreaRepository.FindAllActiveAreaByApplicationAndUserId(applicationId, userId, false, sortBy, sortAscending, grantService.IsHostUser(userId), true, FilterGroupAreaMode.OnlyGroupArea);
        }

        public List<WidgetArea> FindAllGroupedActiveStarredAreaByApplicationAndUserId(string applicationId, string userId, string sortBy, bool sortAscending)
        {
            return widgetAreaRepository.FindAllActiveAreaByApplicationAndUserId(applicationId, userId, true, sortBy, sortAscending, grantService.IsHostUser(userId), true, FilterGroupAreaMode.All);
        }

        public void DeleteArea(string id, string userId)
        {
            var area = widgetAreaRepository.FindByID(id);

            if (area != null)
            {
                string urlPath = MultiTenantsHelper.MountMultiTenantAreaUrl(area.RoutePath);

                var pathToDelete = (from p in DBContext.AspNetPaths
                                    where p.Path == urlPath
                                    select p).SingleOrDefault();

                if (pathToDelete != null)
                {
                    // Rimuove tuitti i widget per le aree shared
                    var allUser = from au in DBContext.AspNetPersonalizationAllUsers
                                    where au.PathId == pathToDelete.Id
                                    select au;
                    DBContext.AspNetPersonalizationAllUsers.RemoveRange(allUser);

                    // Rimuove tuitti i widget per le aree personali dell'utente
                    var perUser = from au in DBContext.AspNetPersonalizationPerUser
                                    where au.PathId == pathToDelete.Id && au.UserId == userId
                                    select au;
                    DBContext.AspNetPersonalizationPerUser.RemoveRange(perUser);

                    // Remove path
                    DBContext.AspNetPaths.Remove(pathToDelete);
                }

                DBContext.WidgetAreaSet.Remove(area);

                DBContext.SaveChanges();
            }
        }

        public string ResolveRedirectAreaUrl(string uid)
        {
            return MultiTenantsHelper.MountMultiTenantAreaUrl(widgetAreaRepository.FindByID(uid).RoutePath);
        }
    }
}