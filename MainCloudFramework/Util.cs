﻿// Decompiled with JetBrains decompiler
// Type: MainCloudFramework.Web.Util
// Assembly: MainCloudFramework.Web, Version=1.0.4095.40444, Culture=neutral, PublicKeyToken=null
// MVID: D9DAFA03-5B6A-48D2-AD01-B14628D5693C
// Assembly location: C:\Development\Clienti\mycros\MainProject\ExternalLib\MainCloudFramework.Web.dll

using System.Diagnostics;
using System.Text;

namespace MainCloudFramework.Web
{
  public class Util
  {
    [DebuggerNonUserCode]
    public Util()
    {
    }

    public static string QuoteJScriptString(string value)
    {
      return Util.QuoteJScriptString(value, false);
    }

    internal static string QuoteJScriptString(string value, bool forUrl)
    {
      StringBuilder stringBuilder = (StringBuilder) null;
      if (string.IsNullOrEmpty(value))
        return string.Empty;
      int startIndex = 0;
      int count = 0;
      int num1 = 0;
      int num2 = checked (value.Length - 1);
      int index = num1;
      while (index <= num2)
      {
        switch (value[index])
        {
          case '%':
            if (forUrl)
            {
              if (stringBuilder == null)
                stringBuilder = new StringBuilder(checked (value.Length + 6));
              if (count > 0)
                stringBuilder.Append(value, startIndex, count);
              stringBuilder.Append("%25");
              startIndex = checked (index + 1);
              count = 0;
              break;
            }
            goto default;
          case '\'':
            if (stringBuilder == null)
              stringBuilder = new StringBuilder(checked (value.Length + 5));
            if (count > 0)
              stringBuilder.Append(value, startIndex, count);
            stringBuilder.Append("\\'");
            startIndex = checked (index + 1);
            count = 0;
            break;
          case '\\':
            if (stringBuilder == null)
              stringBuilder = new StringBuilder(checked (value.Length + 5));
            if (count > 0)
              stringBuilder.Append(value, startIndex, count);
            stringBuilder.Append("\\\\");
            startIndex = checked (index + 1);
            count = 0;
            break;
          case '\t':
            if (stringBuilder == null)
              stringBuilder = new StringBuilder(checked (value.Length + 5));
            if (count > 0)
              stringBuilder.Append(value, startIndex, count);
            stringBuilder.Append("\\t");
            startIndex = checked (index + 1);
            count = 0;
            break;
          case '\n':
            if (stringBuilder == null)
              stringBuilder = new StringBuilder(checked (value.Length + 5));
            if (count > 0)
              stringBuilder.Append(value, startIndex, count);
            stringBuilder.Append("\\n");
            startIndex = checked (index + 1);
            count = 0;
            break;
          case '\r':
            if (stringBuilder == null)
              stringBuilder = new StringBuilder(checked (value.Length + 5));
            if (count > 0)
              stringBuilder.Append(value, startIndex, count);
            stringBuilder.Append("\\r");
            startIndex = checked (index + 1);
            count = 0;
            break;
          case '"':
            if (stringBuilder == null)
              stringBuilder = new StringBuilder(checked (value.Length + 5));
            if (count > 0)
              stringBuilder.Append(value, startIndex, count);
            stringBuilder.Append("\\\"");
            startIndex = checked (index + 1);
            count = 0;
            break;
          default:
            checked { ++count; }
            break;
        }
        checked { ++index; }
      }
      if (stringBuilder == null)
        return value;
      if (count > 0)
        stringBuilder.Append(value, startIndex, count);
      return stringBuilder.ToString();
    }
  }
}
