﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MainCloudFramework.RequestDBContext
{
    public class DbContextLocator<D> where D : DbContext
    {
        internal static D DbContextForce { get; set; }

        public D DbContext
        {
            get
            {
                if (DbContextForce != null)
                {
                    return DbContextForce;
                }
                else
                {
                    D dbContext = HttpContext.Current.Items[typeof(D).FullName] as D;
                    if (dbContext == null)
                    {
                        throw new InvalidOperationException("No HttpContext.Current.Items DbContext of type " + typeof(D).FullName + " found. You must register the HttpModule to use per - Request impelementation.");
                    }
                    return dbContext;
                }
            }
        }
    }
}
