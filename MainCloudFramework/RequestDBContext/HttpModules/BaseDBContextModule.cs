﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MainCloudFramework.RequestDBContext.HttpModules
{
    public abstract class BaseDBContextModule : IHttpModule
    {
        // Lista dei db context da rendere disponibili
        abstract protected Type[] DbContextTypesList
        {
            get;
        }

        public void Init(HttpApplication context)
        {
            // ad ogni richiesta creiamo un'istanza
            context.BeginRequest += (sender, e) =>
            {
                foreach (Type t in DbContextTypesList)
                {
                    HttpContext.Current.Items[t.FullName] = Activator.CreateInstance(t);
                }
            };

            // alla fine di ogni richiesta, se attiva, la distruggiamo
            context.EndRequest += (sender, e) =>
            {
                foreach (Type t in DbContextTypesList)
                {
                    var ctx = HttpContext.Current.Items[t.FullName] as DbContext;
                    if (ctx != null)
                    {
                        ctx.Dispose();
                        HttpContext.Current.Items.Remove(t.FullName);
                    }
                }
            };
        }

        public void Dispose() { }
    }
}