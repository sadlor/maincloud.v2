﻿using MainCloudFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.Repositories
{
    public class ApplicationRepository : BaseRepository<AspNetApplications, ApplicationDbContext>
    {
        public bool ApplicationExists(string applicationName)
        {
            var appExists = (from app in DBContext.AspNetApplications
                             where app.Name == applicationName
                             select app).Any();
            return appExists;
        }
    }
}
