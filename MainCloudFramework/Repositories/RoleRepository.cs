﻿using MainCloudFramework.Models;
using MainCloudFramework.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.Repositories
{
    public class RoleRepository : BaseRepository<ApplicationRole, ApplicationDbContext>
    {
        public ApplicationRole FindByName(string roleName, string applicationId)
        {
            var item = (from r in DBContext.Roles
                             where r.Name.ToUpper() == roleName.ToUpper() && r.ApplicationId == applicationId
                             select r).SingleOrDefault();
            return item;
        }

        public ApplicationRole FindGuestRole(string applicationId)
        {
            return FindByName(MCFConstants.ROLE_GUESTS, applicationId);
        }

        //public bool FindUserFromRole(string roleId, string userId)
        //{
        //    var role = FindByID(roleId);
        //    if (role != null)
        //    {
        //        var user = role.Users.Where(x => x.UserId == userId).SingleOrDefault();
        //        if (user != null)
        //        {
        //            role.Users.Remove(user);
        //        }
        //    }
        //    return false;
        //}
    }
}
