﻿using MainCloudFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using MainCloudFramework.Web.Helpers;

namespace MainCloudFramework.Repositories
{
    public class WidgetAreaRepository : BaseRepository<WidgetArea, ApplicationDbContext>
    {
        public enum FilterGroupAreaMode
        {
            OnlyGroupArea,
            OnlyGroupAreaNotEmpty,
            ExludeGroupArea,
            All
        }
        public WidgetArea FindWidgetAreaByRoutePath(string routePath)
        {
            WidgetArea page = DBContext.WidgetAreaSet.Where(p => p.RoutePath == routePath && p.ApplicationId == MultiTenantsHelper.ApplicationId).SingleOrDefault();
            return page;
        }

        public List<WidgetArea> FindAllActiveAreaByApplicationAndUserId(string applicationId, string userId, bool onlyStarred, string orderBy, bool sortAscending, bool IsHostUser, bool onlyFirstLevel, FilterGroupAreaMode filterGroupAreaMode)
        {
            var allAppAreas = DBContext.WidgetAreaSet.Where(a => a.ApplicationId.Equals(applicationId) && a.DeletedAt == null && a.Visible);

            if (onlyStarred)
            {
                allAppAreas = allAppAreas.Where(x => x.Starred == true);
            }

            if (sortAscending)
            {
                allAppAreas = allAppAreas.OrderBy(orderBy);
            }
            else
            {
                allAppAreas = allAppAreas.OrderBy(orderBy + " descending");
            }

            if (onlyFirstLevel)
            {
                allAppAreas = allAppAreas.Where(x => x.WidgetAreaParentId == null);
            }

            if (filterGroupAreaMode == FilterGroupAreaMode.ExludeGroupArea)
            {
                allAppAreas = allAppAreas.Where(x => !x.WidgetGroupArea);
            }
            else if (filterGroupAreaMode == FilterGroupAreaMode.OnlyGroupArea)
            {
                allAppAreas = allAppAreas.Where(x => x.WidgetGroupArea);
            }
            else if (filterGroupAreaMode == FilterGroupAreaMode.OnlyGroupAreaNotEmpty)
            {
                allAppAreas = allAppAreas.Where(x => x.WidgetGroupArea && x.WidgetAreaChildren.Count > 0);
            }

            //if (IsHostUser)
            //{
                return allAppAreas.ToList();
            //}
            //else
            //{
            //    var areas = from a in allAppAreas
            //                where a.OwnerUserId == null || a.OwnerUserId == userId // Se pubblico o SOLO se di proprietà dell'utente
            //                select a;
            //    return areas.ToList();
            //}
        }

        public void DeleteArea(string id, string userId)
        {
            var area = (from a in DBContext.WidgetAreaSet
                        where a.Id == id
                        select a).SingleOrDefault();

            if (area != null)
            {
                string urlPath = MultiTenantsHelper.MountMultiTenantAreaUrl(area.RoutePath);
                var pathToDelete = (from p in DBContext.AspNetPaths
                                    where p.Path == urlPath
                                    select p).SingleOrDefault();

                if (pathToDelete != null)
                {
                    // Rimuove tuitti i widget per le aree shared
                    var allUser = from au in DBContext.AspNetPersonalizationAllUsers
                                  where au.PathId == pathToDelete.Id
                                  select au;
                    DBContext.AspNetPersonalizationAllUsers.RemoveRange(allUser);

                    // Rimuove tuitti i widget per le aree personali dell'utente
                    var perUser = from au in DBContext.AspNetPersonalizationPerUser
                                  where au.PathId == pathToDelete.Id && au.UserId == userId
                                  select au;
                    DBContext.AspNetPersonalizationPerUser.RemoveRange(perUser);

                    // Remove path
                    DBContext.AspNetPaths.Remove(pathToDelete);
                }

                DBContext.WidgetAreaSet.Remove(area);

                DBContext.SaveChanges();
            }
        }
    }
}
