﻿using MainCloudFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.Repositories
{
    public class UserRepository : BaseRepository<ApplicationUser, ApplicationDbContext>
    {
        public ApplicationUser FindUserByPartialUserName(string userName)
        {
            var userFound = (from u in DBContext.Users
                              where u.UserName.StartsWith(userName)
                              select u).SingleOrDefault();
            return userFound;
        }

        public bool UserExists(string userName)
        {
            return FindUserByName(userName) != null;
        }

        public ApplicationUser FindUserByName(string userName)
        {
            var userFound = (from u in DBContext.Users
                             where u.UserName == userName
                             select u).SingleOrDefault();
            return userFound;
        }

        /// <summary>
        /// Esclude gli utenti già associati all'app specificata
        /// </summary>
        /// <param name="userName">Utente da ricercare</param>
        /// <param name="excludeUserInAppId">Id dell'app con gli utenti da non considerare</param>
        /// <returns></returns>
        public ApplicationUser FindUserByName(string userName, string excludeUserInAppId)
        {
            var userFound = (from u in DBContext.Users
                             where u.UserName == userName && !u.Applications.Any(x => x.Id == excludeUserInAppId)
                             select u).SingleOrDefault();
            return userFound;
        }

    }
}
