﻿using MainCloudFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.Repositories
{
    public class WidgetsRepository : BaseRepository<Widget, ApplicationDbContext>
    {
        public IList<Widget> GetInstalledWidgets()
        {
            return DBContext.WidgetSet.ToList();
        }

        public void UpdateInstalledWidgets(IList<Widget> widgets)
        {
            DeleteAll();
            foreach(var w in widgets)
            {
                // TODO Rimuovere legacy vecchia modalità widget
                //var dbW = DBContext.WidgetSet.Where(m => m.WidgetPath.Equals(w.WidgetPath, StringComparison.OrdinalIgnoreCase) && m.WidgetId == null).FirstOrDefault();
                //if (dbW != null)
                //{
                //    DBContext.WidgetSet.Remove(dbW);
                //}

                // Nuovo formato widget, ricerca per WidgetId GUID
                //dbW = DBContext.WidgetSet.Where(m => m.WidgetId == w.WidgetId).FirstOrDefault(); // TODO prevedere multi widget per modulo????
                // rimuove se esiste
                //if (dbW != null)
                //{
                //    DBContext.WidgetSet.Remove(dbW);
                //}
                // carica nuovo widget
                DBContext.WidgetSet.Add(w);
            }
            DBContext.SaveChanges();
        }

        public void DeleteInstalledWidgets(string widgetId)
        {
            DBContext.WidgetSet.Remove(DBContext.WidgetSet.Where(m => m.WidgetId == widgetId).SingleOrDefault());
            DBContext.SaveChanges();
        }

        public Widget FindByWidgetId(string widgetId)
        {
            var w = DBContext.WidgetSet.Where(x => x.WidgetId == widgetId).SingleOrDefault();
            return w;
        }

        public Widget FindByWidgetName(string widgetName)
        {
            var w = DBContext.WidgetSet.Where(x => x.WidgetName == widgetName).FirstOrDefault();
            return w;
        }
    }
}
