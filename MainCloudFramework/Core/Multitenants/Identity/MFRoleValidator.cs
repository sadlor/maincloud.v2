﻿// Copyright (c) Microsoft Corporation, Inc. All rights reserved.
// Licensed under the MIT License, Version 2.0. See License.txt in the project root for license information.

using MainCloudFramework.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace MainCloudFramework.Core.Multitenants.Identity
{
    /// <summary>
    ///     Validates roles before they are saved
    /// </summary>
    /// <typeparam name="TRole"></typeparam>
    public class MFRoleValidator<TRole> : RoleValidator<TRole, string> where TRole : ApplicationRole, IRole<string>
    {
        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="manager"></param>
        public MFRoleValidator(MFRoleManager<TRole, string> manager)
            : base(manager)
        {
        }
    }

    /// <summary>
    ///     Validates roles before they are saved
    /// </summary>
    /// <typeparam name="TRole"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public class MFRoleValidator<TRole, TKey> : IIdentityValidator<TRole>
        where TRole : ApplicationRole, IRole<TKey>
        where TKey : IEquatable<TKey>
    {
        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="manager"></param>
        public MFRoleValidator(MFRoleManager<TRole, TKey> manager)
        {
            if (manager == null)
            {
                throw new ArgumentNullException("manager");
            }
            Manager = manager;
        }

        private MFRoleManager<TRole, TKey> Manager { get; set; }

        /// <summary>
        ///     Validates a role before saving
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public virtual async Task<IdentityResult> ValidateAsync(TRole item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            var errors = new List<string>();
            await ValidateRoleName(item, errors).WithCurrentCulture();
            if (errors.Count > 0)
            {
                return IdentityResult.Failed(errors.ToArray());
            }
            return IdentityResult.Success;
        }

        private async Task ValidateRoleName(TRole role, List<string> errors)
        {
            if (string.IsNullOrWhiteSpace(role.Name))
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, "{0} cannot be null or empty.", "Name"));
            }
            else
            {
                var owner = await Manager.FindByNameAsync(role.Name).WithCurrentCulture();            
                if (owner != null && !EqualityComparer<string>.Default.Equals(owner.Id, role.Id) && EqualityComparer<string>.Default.Equals(owner.ApplicationId, role.ApplicationId))
                {
                    errors.Add(String.Format(CultureInfo.CurrentCulture, "Name {0} is already taken.", role.Name));
                }
                
            }
        }
    }
}