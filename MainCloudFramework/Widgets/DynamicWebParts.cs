﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;

/// Use an alias for the WebPartZone
/// 


namespace CMS
{
    public class DynamicWebParts
    {
        /// <summary>
        /// the page to which components will be added
        /// </summary>
        private Page itsPage;

        public DynamicWebParts(Page aPage)
        {
            itsPage = aPage;
        }

        private WebPartManager itsManager;
        public WebPartManager Manager
        {
            set { itsManager = value; }
        }


        /// <summary>
        /// Create a web part from the specified control and add it to the specified zone.
        /// </summary>
        /// <param name="aZoneName">the zone to which the control will be added</param>
        /// <param name="aControlName">the virtual path to the control file</param>
        /// <param name="aWebPartTitle">the title to give the newly created Web Part</param>
        /// <returns></returns>
        public WebPart AddWebPartToZone(string aZoneName, string aControlPath, string aWebPartTitle)
        {
            // load the control from the supplied path
            Control control = itsPage.LoadControl(aControlPath);

            // remove spaces from the control's path to form the ID
            control.ID = aWebPartTitle.Replace(" ", "");

            // find the specified web part zone in the manager's list of zones
            WebPartZone zone = itsManager.Zones[aZoneName] as WebPartZone;
            if (zone != null)
            {
                GenericWebPart genericWebPart = itsManager.CreateWebPart(control);
                genericWebPart.Title = aWebPartTitle;

                // add the web part to the zone
                return itsManager.AddWebPart(genericWebPart, zone, 0);
            }

            // the web part could not be added to the zone
            return null;
        }

    }
}