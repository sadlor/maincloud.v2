namespace MainCloudFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class widgetAreaRoles : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("WidgetAreaRoles", "ApplicationRole_Id", "AspNetRoles");
        }
        
        public override void Down()
        {
        }
    }
}
