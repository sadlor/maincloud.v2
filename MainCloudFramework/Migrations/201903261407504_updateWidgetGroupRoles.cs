namespace MainCloudFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateWidgetGroupRoles : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.WidgetGroupRoles", "ApplicationRole_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.WidgetGroupRoles", "WidgetArea_Id", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
        }
    }
}
