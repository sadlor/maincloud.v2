namespace MainCloudFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateWidgetAreaRoles : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.WidgetAreaRoles", "IX_ApplicationRole_Id");
        }
        
        public override void Down()
        {
        }
    }
}
