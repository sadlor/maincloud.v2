namespace MainCloudFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateRoles : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetRoles", "ShowWidgetAdvanced");
            DropColumn("dbo.AspNetRoles", "ShowWidgetAggregator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetRoles", "ShowWidgetAggregator", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetRoles", "ShowWidgetAdvanced", c => c.Boolean(nullable: false));
        }
    }
}
