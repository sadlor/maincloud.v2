namespace MainCloudFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WidgetGroupRoles : DbMigration
    {
        public override void Up()
        {
            CreateTable(
            "dbo.WidgetGroupRoles",
            c => new
            {
                Id = c.String(nullable: false, maxLength: 128),
                View = c.Boolean(nullable: false),
                Edit = c.Boolean(nullable: false),
                WidgetArea_Id = c.String(),
                ApplicationRole_Id = c.String()
            })
            .PrimaryKey(t => t.Id);
            //.ForeignKey("dbo.WidgetAreas_Id", t => t.WidgetArea_Id)
            //.ForeignKey("dbo.AspNetRoles_Id", t => t.ApplicationRole_Id);
        }
        
        public override void Down()
        {
            //DropForeignKey("dbo.WidgetGroupRoles", "WidgetArea_Id", "dbo.WidgetAreas");
            //DropForeignKey("dbo.WidgetGroupRoles", "ApplicationRole_Id", "dbo.AspNetRoles");
            DropTable("dbo.WidgetGroupRoles");
        }
    }
}
