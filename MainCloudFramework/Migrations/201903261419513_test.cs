namespace MainCloudFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.WidgetGroupRoles",
            //    c => new
            //        {
            //            Id = c.String(nullable: false, maxLength: 128),
            //            View = c.Boolean(nullable: false),
            //            Edit = c.Boolean(nullable: false),
            //            ApplicationRole_Id = c.String(maxLength: 128),
            //            WidgetArea_Id = c.String(maxLength: 128),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.AspNetRoles", t => t.ApplicationRole_Id)
            //    .ForeignKey("dbo.WidgetAreas", t => t.WidgetArea_Id)
            //    .Index(t => t.ApplicationRole_Id)
            //    .Index(t => t.WidgetArea_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WidgetGroupRoles", "WidgetArea_Id", "dbo.WidgetAreas");
            DropForeignKey("dbo.WidgetGroupRoles", "ApplicationRole_Id", "dbo.AspNetRoles");
            DropIndex("dbo.WidgetGroupRoles", new[] { "WidgetArea_Id" });
            DropIndex("dbo.WidgetGroupRoles", new[] { "ApplicationRole_Id" });
            DropTable("dbo.WidgetGroupRoles");
        }
    }
}
