// <auto-generated />
namespace MainCloudFramework.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class updateWidgetAreaRoles : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(updateWidgetAreaRoles));
        
        string IMigrationMetadata.Id
        {
            get { return "201903271202115_updateWidgetAreaRoles"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
