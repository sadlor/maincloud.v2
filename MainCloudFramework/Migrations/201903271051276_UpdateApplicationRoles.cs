namespace MainCloudFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateApplicationRoles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetRoles", "CanViewAllWidgets", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetRoles", "CanViewAllWidgets");
        }
    }
}
