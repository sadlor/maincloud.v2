namespace MainCloudFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_widgetGroupRoles : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("dbo.WidgetGroupRoles", "ApplicationRole_Id","dbo.AspNetRoles", "Id");
            AddForeignKey("dbo.WidgetGroupRoles", "WidgetArea_Id", "dbo.WidgetAreas", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WidgetGroupRoles", "ApplicationRole_Id", "dbo.AspNetRoles");
            DropForeignKey("dbo.WidgetGroupRoles", "WidgetArea_Id", "dbo.WidgetAreas");
        }
    }
}
