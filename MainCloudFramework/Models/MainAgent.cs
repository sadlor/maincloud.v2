﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MainCloudFramework.Models
{
    [Table("MainAgent")]
    public class MainAgent
    {
        public MainAgent()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        public string Id { get; set; }

        [Required]
        [MaxLength(256)]
        public virtual string Name { get; set; }

        [Required]
        [MaxLength(256)]
        public virtual string Surname { get; set; }

        [NotMapped]
        public string FullName
        {
            get { return Surname + " " + Name; }
        }

        public virtual string Description { get; set; }
    }
}