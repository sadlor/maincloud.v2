﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MainCloudFramework.Models
{
    [Table("AspNetApplications")]
    public class AspNetApplications
    {
        public AspNetApplications()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        public string Id { get; set; }

        [MaxLength(128)]
        public string Code { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("IX_ApplicationCode", 1, IsUnique = true)]
        public string Name { get; set; }

        [Required]
        [MaxLength(128)]
        public string URL { get; set; }

        public string Settings { get; set; }

        public virtual ICollection<ApplicationUser> Users { get; set; }
    }
}