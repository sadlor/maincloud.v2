using MainCloudFramework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MainCloudFramework.Models
{
    public class WidgetArea : BaseModel
    {
        public WidgetArea() : base()
        {
            Id = Guid.NewGuid().ToString();
            Order = 0;
            Visible = false;
            Starred = false;
            //this.WidgetAreaChildren = new HashSet<WidgetArea>();
        }
    
        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        [MaxLength(128)]
        [Column(Order = 2)]
        public string ApplicationId { get; set; }

        [Column(Order = 3)]
        public string OwnerUserId { get; set;}

        [MaxLength(128)]
        [Column(Order = 4)]
        public string WidgetAreaParentId { get; set; }

        public string RoutePath { get; set; }
        public string Template { get; set; }
        public int Order { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Visible { get; set; }
        public string Properties { get; set; }
        public bool Starred { get; set; }
        public string Icon { get; set; }

        [NotMapped]
        public bool PublicArea {
            get
            {
                return OwnerUserId == null;   
            }
        }

        public DateTime? PublishDate { get; set; }
        public DateTime? ExpirationDate { get; set; }

        [ForeignKey("ApplicationId")]
        public virtual AspNetApplications AspNetApplication { get; set; }

        [ForeignKey("WidgetAreaParentId")]
        public virtual WidgetArea WidgetAreaParent { get; set; }

        [InverseProperty("WidgetAreaParent")]
        public virtual ICollection<WidgetArea> WidgetAreaChildren { get; set; }

        public virtual ICollection<WidgetAreaRoles> WidgetAreaRoles { get; set; }

        public virtual ICollection<WidgetGroupRoles> WidgetGroupRoles { get; set; }

        public bool WidgetGroupArea { get; set; }
    }
}