﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MainCloudFramework.Models
{
    [Table("AspNetPersonalizationAllUsers")]
    public class AspNetPersonalizationAllUsers
    {
        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string PathId { get; set; }

        [ForeignKey("PathId")]
        public virtual AspNetPaths AspNetPaths { get; set; }

        [Required]
        public string PageSettings { get; set; }

        [Required]
        public DateTime LastUpdatedDate { get; set; }
    }
}