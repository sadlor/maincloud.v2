﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MainCloudFramework.Models
{
    [Table("AspNetPersonalizationPerUser")]
    public class AspNetPersonalizationPerUser
    {
        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 2)]
        public string PathId { get; set; }

        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 3)]
        public string UserId { get; set; }

        [ForeignKey("PathId")]
        public virtual AspNetPaths AspNetPaths { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser AspNetUsers { get; set; }

        [Required]
        public string PageSettings { get; set; }

        [Required]
        public DateTime LastUpdatedDate { get; set; }
    }
}