using System;
using System.Collections.Generic;
    
namespace MainCloudFramework.Models
{
    public partial class WidgetTextHtml
    {
        public string Id { get; set; }
        public string Body { get; set; }
    }
}
