﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MainCloudFramework.Models
{
    [Table("MainReseller")]
    public class MainReseller
    {
        public MainReseller()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [MaxLength(128)]
        public string Id { get; set; }

        [MaxLength(256)]
        public virtual string Code { get; set; }

        [Required]
        [MaxLength(256)]
        public virtual string Name { get; set; }

        [MaxLength(10)]
        public virtual string PIVA { get; set; }

        [MaxLength(16)]
        public virtual string FiscalCode { get; set; }

        [MaxLength(255)]
        public virtual string Address { get; set; }

        [MaxLength(255)]
        public virtual string City { get; set; }

        [MaxLength(20)]
        public virtual string ZipCode { get; set; }

        public virtual string Description { get; set; }
    }
}