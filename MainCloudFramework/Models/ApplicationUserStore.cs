﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Reflection;
using System.Collections.Generic;

namespace MainCloudFramework.Models
{
    public class ApplicationUserStore<TUser> : UserStore<TUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>, IUserStore<TUser>, IUserStore<TUser, string>, IDisposable 
      where TUser : ApplicationUser
    {
        public ApplicationUserStore(DbContext context)
            : base(context)
        {
        }
         
        public ApplicationUserStore()
            : this(new ApplicationDbContext())
        {
            this.DisposeContext = true;
        }

        public string ApplicationId { get; set; }

        private void ParentThrowIfDisposed()
        {
            // Call private
            MethodInfo dynMethod = base.GetType().GetMethod("ThrowIfDisposed",
                BindingFlags.NonPublic | BindingFlags.Instance);
            if (dynMethod != null)
            {
                dynMethod.Invoke(this, new object[] {});
            }
        }


        public override Task CreateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.Applications = new List<AspNetApplications>();
            var app = new AspNetApplications() { Id = ApplicationId };
            Context.Set<AspNetApplications>().Attach(app);
            user.Applications.Add(app);

            return base.CreateAsync(user);
        }

        public override Task<TUser> FindByEmailAsync(string email)
        {
            if (ApplicationId != null)
            {

                ParentThrowIfDisposed();
                return this.GetUserAggregateAsync(u => u.Email.ToUpper() == email.ToUpper()
                    && u.Applications.FirstOrDefault(x => x.Id == ApplicationId) != null);
            }
            else
            {
                return base.FindByEmailAsync(email);
            }
        }

        public override Task<TUser> FindByNameAsync(string userName)
        {
            if (ApplicationId != null)
            {
                ParentThrowIfDisposed();
                return this.GetUserAggregateAsync(u => u.UserName.ToUpper() == userName.ToUpper()
                    && u.Applications.Any(x => x.Id == ApplicationId));
            }
            else
            {
                return base.FindByNameAsync(userName);
            }
        }
    }
}
