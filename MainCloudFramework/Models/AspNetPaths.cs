﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MainCloudFramework.Models
{
    [Table("AspNetPaths")]
    public class AspNetPaths
    {
        [Key]
        [Required]
        [MaxLength(128)]
        [Column(Order = 1)]
        public string Id { get; set; }

        [Required]
        [MaxLength(128)]
        [Column(Order = 2)]
        public string ApplicationId { get; set; }

        [Required]
        [MaxLength(256)]
        public string Path { get; set; }

        [ForeignKey("ApplicationId")]
        public virtual AspNetApplications AspNetApplications { get; set; }
    }
}