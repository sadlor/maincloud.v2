using MainCloudFramework.Models;
using System;
using System.Collections.Generic;

namespace MainCloudFramework.Models
{   
    public class Widget
    {
        public Widget()
        {
            this.Category = "General";
            this.WidgetUserRoles = new HashSet<WidgetUserRole>();
        }
    
        public int Id { get; set; }
        public string WidgetId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public string WidgetName { get; set; }
        public string WidgetPath { get; set; }
        public string Icon { get; set; }
        public string Category { get; set; }
        public string Manifest { get; set; }
    
        public virtual ICollection<WidgetUserRole> WidgetUserRoles { get; set; }
    }
}