﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.Models
{
    public class WidgetGroupRoles
    {
        [Key, Column(Order = 0)]
        public string Id { get; set; }

        public virtual WidgetArea WidgetArea { get; set; }
        public virtual ApplicationRole ApplicationRole { get; set; }

        public bool View { get; set; }
        public bool Edit { get; set; }
    }
}
