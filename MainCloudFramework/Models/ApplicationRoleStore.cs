﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.Models
{
    public class ApplicationRoleStore<TRole> : RoleStore<TRole>
        where TRole : ApplicationRole, new()
    {
        //
        // Summary:
        //     Constructor
        public ApplicationRoleStore(string applicationId) : base()
        {
            ApplicationId = applicationId;
        }

        //
        // Summary:
        //     Constructor
        //
        // Parameters:
        //   context:
        public ApplicationRoleStore(string applicationId, DbContext context) : base(context)
        {
            ApplicationId = applicationId;
        }

        public string ApplicationId { get; set; }

        public override Task CreateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            role.ApplicationId = ApplicationId;
            return base.CreateAsync(role);
        }

        public override Task UpdateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            role.ApplicationId = ApplicationId;
            return base.UpdateAsync(role);
        }
    }
}
