﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MainCloudFramework.Models
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base()
        {
        }

        public ApplicationRole(string roleName) : base(roleName)
        {
        }

        public ApplicationRole(string roleName, string description) : this(roleName)
        {
            Description = description;
        }

        public string Description { get; set; }

        //[Key]
        [Required]
        [MaxLength(128)]
        [Index("RoleMultiTenantNameIndex", IsUnique = true, Order = 2)]
        public string ApplicationId { get; set; }

        [ForeignKey("ApplicationId")]
        public virtual AspNetApplications AspNetApplication { get; set; }

        public virtual ICollection<WidgetArea> WidgetAreas { get; set; }

        public bool AllowAddArea { get; set; }
        public bool AllowEditArea { get; set; }
        public bool AllowDeleteArea { get; set; }

        public bool AllowAddWidget { get; set; }
        public bool AllowEditWidget { get; set; }
        public bool AllowDeleteWidget { get; set; }
        public bool AllowPublicArea { get; set; }
        public bool ShowWidgetAdvanced { get; set; }
        public bool ShowWidgetAggregator { get; set; }
        public bool CanViewAllWidgets { get; set; }
    }
}