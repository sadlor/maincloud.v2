﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

namespace MainCloudFramework.Models
{
    public class ApplicationDbContext : MFIdentityDbContext<ApplicationUser, ApplicationRole>
    {
        //public DbSet<ApplicationRole> Roles { get; set; }
        public DbSet<AspNetApplications> AspNetApplications { get; set; }
        public DbSet<AspNetPaths> AspNetPaths { get; set; }
        public DbSet<AspNetPersonalizationPerUser> AspNetPersonalizationPerUser { get; set; }
        public DbSet<AspNetPersonalizationAllUsers> AspNetPersonalizationAllUsers { get; set; }

        public DbSet<MainReseller> MainReseller { get; set; }
        public DbSet<MainCustomer> MainCustomer { get; set; }
        public DbSet<MainAgent> MainAgent { get; set; }
        public DbSet<Widget> WidgetSet { get; set; }
        public DbSet<WidgetTextHtml> WidgetTextHtmlSet { get; set; }
        public DbSet<WidgetArea> WidgetAreaSet { get; set; }
        public DbSet<WidgetUserRole> WidgetUserRoleSet { get; set; }
        public DbSet<WidgetAreaRoles> WidgetAreaRoleSet { get; set; }
        public DbSet<WidgetGroupRoles> WidgetGroupRoleSet { get; set; }
        //public DbSet<ApplicationRole> ApplicationRolesSet { get; set; }
        public DbSet<Log> Logs { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new ApplicationDbInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            var currentUsername = HttpContext.Current != null && HttpContext.Current.User != null
           ? HttpContext.Current.User.Identity.Name
           : "Anonymous";

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Added && x.Entity.CreatedAt == default(DateTime)))
            {
                entry.Entity.CreatedAt = DateTime.Now;
            }

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Added && x.Entity.CreatedBy == null))
            {
                entry.Entity.CreatedBy = currentUsername;
            }

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Modified))
            {
                //        string name = ReflectionUtility.GetPropertyName(() => model.CreatedAt);
                //        entry.Property(name).IsModified = false;

                entry.Entity.ModifiedAt = DateTime.Now;
                entry.Entity.ModifiedBy = currentUsername;
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges()
        {
            var currentUsername = HttpContext.Current != null && HttpContext.Current.User != null
                       ? HttpContext.Current.User.Identity.Name
                       : "Anonymous";

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Added && x.Entity.CreatedAt == default(DateTime)))
            {
                entry.Entity.CreatedAt = DateTime.Now;
            }

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Added && x.Entity.CreatedBy == null))
            {
                entry.Entity.CreatedBy = currentUsername;
            }

            foreach (var entry in ChangeTracker.Entries<BaseModel>().Where(x => x.State == System.Data.Entity.EntityState.Modified))
            {
                //        string name = ReflectionUtility.GetPropertyName(() => model.CreatedAt);
                //        entry.Property(name).IsModified = false;

                entry.Entity.ModifiedAt = DateTime.Now;
                entry.Entity.ModifiedBy = currentUsername;
            }
            return base.SaveChanges();
        }

    }
}