﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;

namespace MainCloudFramework.Models
{
    // You can add User data for the user by adding more properties to your User class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        //[Required]
        //[MaxLength(256)]
        //public string FirstName { get; set; }

        //[Required]
        //[MaxLength(256)]
        //public string LastName { get; set; }

        public bool AllowUnlimitedSimultaneousLogin { get; set; }

        public ClaimsIdentity GenerateUserIdentity(ApplicationUserManager manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = manager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            return Task.FromResult(GenerateUserIdentity(manager));
        }

        // [InverseProperty("ApplicationUser")]
        public virtual ICollection<AspNetApplications> Applications { get; set; }

        public bool IsInApplication(string applicationId)
        {
            return Applications.Any(x => x.Id == applicationId);
        }

        //public ICollection<ApplicationUserRole> UserRoles { get; set; }
    }
}