﻿using MainCloudFramework.Core.Multitenants.Identity;
using MainCloudFramework.RequestDBContext;
using MainCloudFramework.Services;
using MainCloudFramework.Web;
using MainCloudFramework.Web.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MainCloudFramework.Models
{
    public class ApplicationDbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            InitializeIdentityForEF(context);
            base.Seed(context);
        }

        public void InitializeIdentityForEF(ApplicationDbContext db)
        {
            // Se non è presente nessun utente inizializza i dati nel DB
            if (!db.Users.Any())
            {
                DbContextLocator<ApplicationDbContext>.DbContextForce = db;
                ApplicationService applicationService = new ApplicationService();
                string newUserName = "host@host.it";
                string applicationName = "Host";
                // Create Host application
                applicationService.CreateApplication(applicationName, newUserName, "Password1!", "aee44922-d28f-415c-b4f7-e98c1c6a0af7", true);

                // Create Demo application
                applicationName = "Demo";
                newUserName = string.Format(MCFConstants.APPLICATION_ADMIN_EMAIL_TEMPLATE, MCFConstants.USER_ADMIN.ToLower(), applicationName);
                var demoAppId = applicationService.CreateApplication(applicationName, newUserName, "Password1!", "bc6058d2-4f74-4c62-b023-6ab4132e6f2f");

                applicationService.InitializeWidgetCatalogue();
            }
        }
    }
}