using System;
using System.Collections.Generic;
    
namespace MainCloudFramework.Models
{
    public partial class WidgetUserRole
    {
        public int Id { get; set; }
        public string ApplicationId { get; set; }
        public string UserRoleId { get; set; }
    
        public virtual Widget Widget { get; set; }
    }
}
