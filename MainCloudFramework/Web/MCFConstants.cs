﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.Web
{
    public class MCFConstants
    {
        public const string ROLE_HOST = "Host";

        public const string ROLE_ADMIN = "Administrators";
        public const string USER_ADMIN = "admin";
        public const string APPLICATION_ADMIN_EMAIL_TEMPLATE = "{0}{1}@mainenergy.it";

        public const string ROLE_GUESTS = "Guests";

        public const string ROLE_RESELLER = "Reseller";

        public const string ROLE_OPERATOR = "Operatore";

        public const string BASE_MODULES_PATH = "~/Dashboard/Modules/";
        public const string BASE_PAGES_TEMPLATE_PATH = "Templates/";
    }
}
