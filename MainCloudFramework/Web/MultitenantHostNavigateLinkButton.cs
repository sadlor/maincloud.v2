﻿using MainCloudFramework.UI.Utilities;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;

namespace MainCloudFramework.Web
{
    public class MultitenantHostNavigateLinkButton : HyperLink
    {
        public string RouthPath
        {
            set
            {
                NavigateUrl = string.Format("~/{0}/{1}", MultiTenantsHelper.HostAdministrationUrl, value.TrimStart('/'));
                NavigateUrl += "?ReturnUrl=~" + HttpContext.Current.Request.Url.AbsolutePath;
            }
        }

        public override bool Visible
        {
            get
            {
                if (Page.User.IsInRole(MCFConstants.ROLE_HOST))
                {
                    return base.Visible;
                }
                else
                {
                    return false;
                }
            }

            set
            {
                base.Visible = value;
            }
        }
    }
}
