﻿/// <reference name="MicrosoftAjax.js"/>
function IsNull ( obj )
{
    if ( obj == null || obj == "undefined" ) return true;
    
    return false;
};

function getFirstChild ( element )
{
    if ( IsNull ( element ) ) throw Error.argumentNull("element");
    if ( IsNull ( element.childNodes ) ) throw Error.argumentNull("element.childNodes");
    
    if ( element.childNodes.length < 1 ) {
        return null;
    };
    
    var count = element.childNodes.length;
    var childNodes = element.childNodes;
    
    for ( var index = 0; index < count; index++ ) {
        var child = childNodes[index];
        
        if ( child.nodeType == 1 ) {
            // Sys.Debug.trace("child = " + child + ", child.nodeType = " + child.nodeType);
            return child;
        };
    };
    
    return null;
};

function getNextSibling ( element )
{
    if ( IsNull ( element ) ) throw Error.argumentNull("element");
    
    var sibling = element.nextSibling;
    
    while ( !IsNull(sibling) && sibling.nodeType != 1 )
    {
        sibling = sibling.nextSibling;
    };
    
    return sibling;
};

//function disableSelection(target){
//    if (typeof target.onselectstart!="undefined") //IE route
//	    target.onselectstart=function(){return false}
//    else if (typeof target.style.MozUserSelect!="undefined") //Firefox route
//	    target.style.MozUserSelect="none"
//    else //All other route (ie: Opera)
//	    target.onmousedown=function(){return false}
//    target.style.cursor = "default"
//}

//disableSelection(document.body);

Type.registerNamespace("MainCloudFramework.Web");

MainCloudFramework.Web.WebPartBehavior = function ( element ) 
{
    MainCloudFramework.Web.WebPartBehavior.initializeBase(this, [element]);

    this._titleBar$delegates = 
    {
        mousedown : Function.createDelegate(this, this._titleBar_onMouseDown)
    };
    
    this._selectStartHandler = Function.createDelegate(this, this._onSelectStart);
    
    this._zone = null;
    this._titleBarElement = null;
};

MainCloudFramework.Web.WebPartBehavior.prototype = 
{
    initialize: function () 
    {
        MainCloudFramework.Web.WebPartBehavior.callBaseMethod(this, 'initialize');
        
//        <div id="wp1560555101" class="webpart">
//		    <div class="webpart_chrome">
//			    <div id="WebPartManager_wp1560555101_titlebar" class="webpart_title">
//				    <span>Untitled</span>
//				    <a title="Minimizes 'Untitled [3]'" href="javascript:__doPostBack('WebPartZone2','minimize:wp1560555101')">
//				        <img title="Minimizes 'Untitled [3]'" src="Images/MinimizeVerb.gif" alt="Minimize" style="border-width:0px;" />
//				    </a>
//				    <a title="Closes 'Untitled [3]'" href="javascript:__doPostBack('WebPartZone2','close:wp1560555101')">
//				        <img title="Closes 'Untitled [3]'" src="Images/CloseVerb.gif" alt="Close" style="border-width:0px;" />
//                    </a>
//			    </div><div class="webpart_body">
//				    <div class="webpart_data">
//					    Hello
//				    </div>
//			    </div>
//		    </div>
//	    </div>

        var webPart = getFirstChild(this.get_element());
        if (!webPart) {
            Sys.Debug.trace(this.get_element());
            Sys.Debug.trace("No valid element in Webpart: Check if is a WebPart Element");
            return;
        }
        else {
            var chrome = getFirstChild(webPart);
            this._titleBarElement = getFirstChild(chrome);
    //        alert(this._titleBarElement);                
    //        this._titleBarElement = this.get_element().childNodes[0].childNodes[0].childNodes[0];
            if (IsNull(this._titleBarElement)) {
                alert("Title bar element not found in WebPart, exiting");
                return;
            };
            this._titleBarElement.style.cursor = "move";
            $addHandlers(this._titleBarElement, this._titleBar$delegates);
        }
    },
    dispose: function () 
    {        
        if ( !IsNull(this._titleBarElement) )
            $common.removeHandlers(this._titleBarElement, this._titleBar$delegates);

        this._zone = null;
        
        MainCloudFramework.Web.WebPartBehavior.callBaseMethod(this, 'dispose');
    },
    get_zone: function ()
    {
        return this._zone;
    },
    set_zone: function ( value )
    {
        this._zone = value;
    },
    _titleBar_onMouseDown: function ( e )
    {
        window._event = e; // Needed internally by _DragDropManager
        
        var element = this.get_element();
        
        this._webParSize = $common.getSize(element); // used later to create an empty div in the drop target where the mouse is
        
        // this cloneNode isn't needed but for some reason it's not playing nice when i remove it and replace it with element
        this._visual = null;
        
        if ( Sys.Browser.name == "Safari" ) // cloneNode doesn't play nice in Safari, this is a temporary work around
            this._visual = document.createElement("div");
        else
            this._visual = element.cloneNode(true);
            
        this._visual.className = "movingWebPart";
                 
        document.body.appendChild(this._visual);
        
        $common.setElementOpacity(this._visual, 0.8);
        $common.setSize(this._visual, this._webParSize);
        $common.setLocation(this._visual, $common.getLocation(this.get_element()));
        
        if ( Sys.Browser.agent === Sys.Browser.InternetExplorer )
        {
            this._selectStartPending = true;
        
            $addHandler(document, 'selectstart', this._selectStartHandler);
        }
        else if ( Sys.Browser.agent === Sys.Browser.Firefox )
        {
            document.body.style.MozUserSelect = "none";
        }
        else
        {
            document.body.onmousedown = function()
            {
                return false;
            };
        };
        
        Sys.Extended.UI.DragDropManager.startDragDrop(this, this._visual, null);
        
        var zone = this.get_zone();
        var tempWebPartElement = zone._createTempElement(this._webParSize);
        var zoneElement = zone.get_element();
        
        zone._attachElementToZone(zoneElement, tempWebPartElement);
        zone._tempWebPartElement = tempWebPartElement;
        
        element.style.display = "none";
        
//        e.stopPropagation();
//        e.cancelBubble = true;
    },
    _onSelectStart: function(e) {
        /// <summary>
        /// Handler for when the parent element is selected.
        /// </summary>
        /// <param name="e" type="Sys.UI.DomEvent">Event info.</param>
        /// <returns></returns>
        
        e.preventDefault();
        
        return false;
    },
    // Sys.Extended.UI.IDragSource section start
    get_dragDataType: function() 
    {
        return "WebPart";
    },
    getDragData: function () 
    { 
        return this.get_element();
    },
    get_dragMode: function () 
    { 
        return Sys.Extended.UI.DragMode.Move;
    },
    onDragStart: function () 
    { 
        // do nothing
    },
    onDrag: function () 
    { 
        // do nothing
    },
    onDragEnd: function ( canceled ) 
    { 
        document.body.removeChild(this._visual);
        
        this.get_element().style.display = "";
        
        if ( Sys.Browser.agent == Sys.Browser.InternetExplorer )
        {
            if ( this._selectStartPending ) 
            {
                $removeHandler(document, 'selectstart', this._selectStartHandler);
                this._selectStartPending = false;
            };
        }
        else if ( Sys.Browser.agent == Sys.Browser.Firefox )
        {
            document.body.style.MozUserSelect = "";
        }
        else
        {
            document.body.onmousedown = null;
        };
    }
};

MainCloudFramework.Web.WebPartBehavior.registerClass('MainCloudFramework.Web.WebPartBehavior', Sys.Extended.UI.BehaviorBase, Sys.Extended.UI.IDragSource);

MainCloudFramework.Web.WebPartZoneBehavior = function(element) 
{
    MainCloudFramework.Web.WebPartZoneBehavior.initializeBase(this, [element]);
    
    this._manager = null;
    this._webParts = new Array();
};

MainCloudFramework.Web.WebPartZoneBehavior.prototype = 
{
    initialize: function() 
    {
        MainCloudFramework.Web.WebPartZoneBehavior.callBaseMethod(this, 'initialize');
        
        Sys.Extended.UI.DragDropManager.registerDropTarget(this);
        
        // all child elements are web parts, so here we wire up the web part dom elements with WebPartBehavior's
        var zoneElement = this.get_element();
        
        if ( zoneElement.childNodes.length < 1 ) return;
        
        var childElement = getFirstChild(zoneElement);

        if ( IsNull( childElement ) )
        {
            Sys.Debug.trace("No WebParts found, exiting");
            
            return;
        };
        
        while( !IsNull(childElement) )
        {
            var webPart = $create(MainCloudFramework.Web.WebPartBehavior, null, null, null, childElement);

            webPart.set_zone(this);

            Array.add(this._webParts, webPart);

            childElement = getNextSibling(childElement);
        };
        
    },
    dispose: function() 
    {        
        Sys.Extended.UI.DragDropManager.unregisterDropTarget(this);

        MainCloudFramework.Web.WebPartZoneBehavior.callBaseMethod(this, 'dispose');
    },
    get_manager: function ()
    {
        return this._manager;
    },
    set_manager: function ( value )
    {
        this._manager = value;
    },
    _getMouseLocation : function ()
    {
        var e = window._event;
        var x = e.clientX + ( document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft );
		var y = e.clientY + ( document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop );

        return new Sys.UI.Point(x, y);
    },
    _getSiblingByPosition : function ( position )
    {
        var zoneElement = this.get_element();
        var child = getFirstChild(zoneElement);
        
        while ( child != null )
        {
            var childBounds = $common.getBounds(child);
            
//            Sys.Debug.trace("   positino.y = " + position.y + ", childBounds.y = " + childBounds.y);
            
            if ( position.y >= childBounds.y && position.y <= (childBounds.y + childBounds.height)) return child;
            
            child = getNextSibling(child);
        };
        
        return null;
    },
    _attachElementToZone : function ( zoneElement, element )
    {
        if ( this._tempWebPartElement != null )
        {
            zoneElement.removeChild(this._tempWebPartElement);
            
            this._tempWebPartElement = null;
        };
        
        var position = this._getMouseLocation();
        var sibling = this._getSiblingByPosition(position);
        
        if ( sibling != null )
        {
            zoneElement.insertBefore(element, sibling);
        }
        else
        {
            zoneElement.appendChild(element);
        };
    },
    _createTempElement: function ( size )
    {
        var tempWebPartElement = document.createElement("div");
        
        tempWebPartElement.className = "targetWebPartBox";
        tempWebPartElement.style.height = size.height + "px";
        
        return tempWebPartElement;
    },
    _getWebPartIndex : function ( webPartElement ) 
    {
        var zoneElement = webPartElement.parentNode;
        var webPartIndex = 0;
        var child = getFirstChild(zoneElement);
        
        while ( child.id != webPartElement.id )
        {
            webPartIndex += 1;
            child = getNextSibling(child);
        };
        
        return webPartIndex;
    },
    // Sys.Extended.UI.IDropTarget
    get_dropTargetElement: function() 
    { 
        return this.get_element();
    },
    canDrop: function(dragMode, dataType, data) 
    { 
        return (dataType == 'WebPart' && data);
    },
    drop: function(dragMode, dataType, data) 
    { 
        var dragDropManager = Sys.Extended.UI.DragDropManager._getInstance();
        var webPart = dragDropManager._activeDragSource;
        
        webPart.set_zone(this);
        
        var zoneElement = this.get_element();
        var startingZoneElement = webPart.get_element().parentNode;
        var webPartElement = webPart.get_element();
        var oldWebPartIndex = this._getWebPartIndex(webPartElement);
        
        this._attachElementToZone(zoneElement, webPartElement);
        
        var newWebPartIndex = this._getWebPartIndex(webPartElement);
        
        if ( startingZoneElement == zoneElement && newWebPartIndex == oldWebPartIndex ) return; // no change
        
        if ( startingZoneElement == zoneElement )
        {
            if ( oldWebPartIndex < newWebPartIndex ) 
            {
                newWebPartIndex += 1;
            };
        };
        
//        alert("oldWebPartIndex = " + oldWebPartIndex + ", newWebPartIndex = " + newWebPartIndex);
        
        Sys.Debug.trace("oldWebPartIndex = " + oldWebPartIndex + ", newWebPartIndex = " + newWebPartIndex);
        
        var eventTarget = zoneElement.getAttribute("UniqueID"); // zoneElement.id;
        var eventArgument = "Drag:" + webPart.get_element().id + ":" + newWebPartIndex;
        
        Sys.Debug.trace("eventTarget = " + eventTarget + ", eventArgument = " + eventArgument);
        
        __doPostBack(eventTarget, eventArgument);
    },
    onDragEnterTarget: function(dragMode, dataType, data) 
    { 
        // do nothing
    },
    onDragLeaveTarget: function(dragMode, dataType, data) 
    { 
        if ( this._tempWebPartElement != null )
        {
            var zoneElement = this.get_element();
            
            zoneElement.removeChild(this._tempWebPartElement);
            
            this._tempWebPartElement = null;
        };
    },
    onDragInTarget: function(dragMode, dataType, data) 
    { 
        var dragDropManager = Sys.Extended.UI.DragDropManager._getInstance();
        var webPart = dragDropManager._activeDragSource;
        var size = webPart._webParSize
        var tempWebPartElement = this._createTempElement(size);
        var zoneElement = this.get_element();
        
        this._attachElementToZone(zoneElement, tempWebPartElement);
        this._tempWebPartElement = tempWebPartElement;
    }
};

MainCloudFramework.Web.WebPartZoneBehavior.registerClass('MainCloudFramework.Web.WebPartZoneBehavior', Sys.Extended.UI.BehaviorBase, Sys.Extended.UI.IDropTarget);

MainCloudFramework.Web.WebPartManagerBehavior = function(element) 
{
    MainCloudFramework.Web.WebPartManagerBehavior.initializeBase(this, [element]);
    
    this._zones = new Array();
    this._enableDragDrop = false;
    this._dragDropEnabled = false;
};

MainCloudFramework.Web.WebPartManagerBehavior.prototype = 
{
    initialize: function() 
    {
        MainCloudFramework.Web.WebPartManagerBehavior.callBaseMethod(this, 'initialize');
        
        if ( !this.get_enableDragDrop() ) 
        {
            return;
        };
        
        for ( var zoneIndex = 0; zoneIndex < this._zonesIds.length; zoneIndex++ )
        {
            var id = this._zonesIds[zoneIndex];
            var zoneElement = $get(id);
            
            if ( zoneElement == null || zoneElement == "undefined" ) 
            {
                alert("zoneElement with id [" + id + "] not found");
                
                continue;
            };
            
            var zone = $create(MainCloudFramework.Web.WebPartZoneBehavior, null, null, null, zoneElement);
            
            zone.set_manager(this);
            
            Array.add(this._zones, zone);
        };
        
        this._dragDropEnabled = true;
    },
    dispose: function() 
    {        
        MainCloudFramework.Web.WebPartManagerBehavior.callBaseMethod(this, 'dispose');
    },
    get_zonesIds: function ()
    {
        return this._zonesIds;
    },
    set_zonesIds: function ( value )
    {
        this._zonesIds = value;
    },
    get_enableDragDrop: function ()
    {
        return this._enableDragDrop;
    },
    set_enableDragDrop: function ( value )
    {
        this._enableDragDrop = value;
    }
};

MainCloudFramework.Web.WebPartManagerBehavior.registerClass('MainCloudFramework.Web.WebPartManagerBehavior', Sys.Extended.UI.BehaviorBase);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
