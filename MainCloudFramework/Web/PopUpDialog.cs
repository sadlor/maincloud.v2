﻿using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

// https://msdn.microsoft.com/en-us/library/aa478964.aspx

namespace MainCloudFramework.Web
{
    [ToolboxData("<{0}:PopUpDialog runat=server></{0}:PopUpDialog>")]
    [ParseChildren(true, "Body")]
    public class PopUpDialog : WebControl, INamingContainer
    {

//        private string templateFooter = @"
//<div class=""modal-footer"">
//    <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Close</button>
//    <button type=""button"" class=""btn btn-primary"">{0}</button>
//</div>";

        [Bindable(true), Category("Appearance"), DefaultValue("PopUp Dialog Title")]
        public string Title
        {
            get
            {
                object o = ViewState["PopUpDialogTitle"];
                if (o == null)
                    return "PopUp Dialog Title";   // return the default value
                else
                    return (string)o;
            }
            set
            {
                ViewState["PopUpDialogTitle"] = value;
            }
        }

        [Bindable(true), Category("Layout"), DefaultValue("60%")]
        override public Unit Width
        {
            get
            {
                object o = ViewState["PopUpDialogWidth"];
                if (o == null)
                    return Unit.Percentage(60);   // return the default value
                else
                    return (Unit)o;
            }
            set
            {
                ViewState["PopUpDialogWidth"] = value;
            }
        }

        [Bindable(true), Category("Layout"), DefaultValue("20%")]
        public Unit MarginLeft
        {
            get
            {
                object o = ViewState["PopUpDialogMarginLeft"];
                if (o == null)
                    return Unit.Percentage(20);   // return the default value
                else
                    return (Unit)o;
            }
            set
            {
                ViewState["PopUpDialogMarginLeft"] = value;
            }
        }

        [Bindable(true), Category("Appearance"), DefaultValue(false)]
        protected bool Open
        {
            get
            {
                object o = ViewState["PopUpDialogIsOpen"];
                if (o == null)
                    return false;   // return the default value
                else
                    return (bool)o;
            }
            set
            {
                ViewState["PopUpDialogIsOpen"] = value;
            }
        }

        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(PopUpDialog))]
        [TemplateInstance(TemplateInstance.Single)]
        public virtual ITemplate Body { get; set; }

        //[PersistenceMode(PersistenceMode.InnerProperty)]
        //[TemplateContainer(typeof(PopUpDialog))]
        //[TemplateInstance(TemplateInstance.Single)]
        //public virtual ITemplate Footer { get; set; }

        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            // Popup
            string tmpl = @"<div id=""{0}"" class=""modal fade"" tabindex=""-1"" role=""dialog"" data-keyboard=""false"" data-backdrop=""static"" aria-labelledby=""{0}"" aria-hidden=""true""><div class=""modal-dialog"" style=""width:{2}; margin-left:{3}"" role=""document""><div class=""modal-content"">";
            writer.Write(string.Format(tmpl, ClientID, ClientID + "_myModalLabel", Width, MarginLeft));

            // Header and title
            //tmpl = @"<div class=""modal-header""><button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close""><span aria-hidden=""true"">&times;</span></button><h4 class=""modal-title"" id=""{0}"">";
            tmpl = @"<div class=""modal-header""><h4 class=""modal-title"" id=""{0}"">";
            writer.Write(string.Format(tmpl, ClientID + "_myModalLabel"));
            writer.Write(Title);
            writer.Write(@"</h4></div>");

            writer.Write(@"<div class=""modal-body"">");  // modal-body
        }

        public override void RenderEndTag(HtmlTextWriter writer)
        {
            writer.Write(@"</div>"); // modal-body
            writer.Write("</div></div></div>"); // Popup
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            RenderChildren(writer);
        }

        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);

            // Initialize all child controls.
            CreateChildControls();
            ChildControlsCreated = true;
        }

        protected override void CreateChildControls()
        {
            // Remove any controls
            Controls.Clear();

            // Add all content to a container.
            var container = new Control();
            Body.InstantiateIn(container);

            // this.Footer.InstantiateIn(container);

            // Add container to the control collection.
            Controls.Add(container);
        }

        /// <summary>
        /// Register script to open dialog
        /// </summary>
        public void OpenDialog()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append(@"$(function(){");
            sb.Append(string.Format("$('#{0}').modal('show');", ClientID));
            sb.Append(@"});");
            sb.Append(@"</script>");
            ScriptManager.RegisterStartupScript(Page, GetType(), "CommandButtonOpentModalScript", sb.ToString(), false);
            Open = true;
        }

        /// <summary>
        /// Register script to close dialog
        /// </summary>
        public void CloseDialog()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append(@"$(function(){");
            sb.Append(string.Format("$('#{0}').modal('hide');", ClientID));
            sb.Append(@"});");
            sb.Append(@"</script>");
            ScriptManager.RegisterStartupScript(Page, GetType(), "CommandButtonClosetModalScript", sb.ToString(), false);
            Open = false;
        }

        public bool IsOpen()
        {
            return Open;
        }
    }
}