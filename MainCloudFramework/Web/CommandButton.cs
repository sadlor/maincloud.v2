﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloudFramework.Web
{
    public class CommandButton : LinkButton
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (CommandName == "Delete")
            {
                OnClientClick = Helpers.ClientDialog.ConfirmDialog("Conferma eliminazione", this);
            }
        }

        protected override void OnCommand(CommandEventArgs e)
        {
            base.OnCommand(e);
        }
    }
}