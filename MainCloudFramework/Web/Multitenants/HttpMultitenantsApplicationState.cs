﻿using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MainCloudFramework.Web.Multitenants
{
    public static class HttpApplicationStateExtensions
    {
        public static HttpMultitenantsApplicationState ApplicationMultitenant(this HttpApplicationState applicationState)
        {
            string appName = MultiTenantsHelper.ApplicationName;
            if (applicationState[appName] == null)
            {
                applicationState[appName] = new HttpMultitenantsApplicationState();
            }
            return applicationState[appName] as HttpMultitenantsApplicationState;
        }
    }

    public class HttpMultitenantsApplicationState : ConcurrentDictionary<string, object>
    {
    }

}
