﻿using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace MainCloudFramework.Web.Multitenants
{
    public static class HttpSessionStateExtensions
    {
        /// <summary>
        /// Session del singolo utente su una singola applicazione multitenant
        /// </summary>
        /// <param name="sessionState"></param>
        /// <returns></returns>
        public static HttpMultitenantsSessionState SessionMultitenant(this HttpSessionState sessionState)
        {
            string appName = MultiTenantsHelper.ApplicationName;
            if (sessionState[appName] == null)
            {
                sessionState[appName] = new HttpMultitenantsSessionState();
            }
            return sessionState[appName] as HttpMultitenantsSessionState;
        }

        /// <summary>
        /// Session del singolo utente su una singola applicazione multitenant
        /// </summary>
        /// <param name="sessionState"></param>
        /// <param name="appName">Nome dell'application multitenant</param>
        /// <returns></returns>
        public static HttpMultitenantsSessionState SessionMultitenant(this HttpSessionState sessionState, string appName)
        {
            if (sessionState[appName] == null)
            {
                sessionState[appName] = new HttpMultitenantsSessionState();
            }
            return sessionState[appName] as HttpMultitenantsSessionState;
        }
    }

    public class HttpMultitenantsSessionState : ConcurrentDictionary<string, object>
    {
    }

}
