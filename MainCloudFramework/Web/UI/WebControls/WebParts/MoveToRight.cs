﻿// Decompiled with JetBrains decompiler
// Type: MainCloudFramework.Web.UI.WebControls.WebParts.MoveToRight
// Assembly: MainCloudFramework.Web, Version=1.0.4095.40444, Culture=neutral, PublicKeyToken=null
// MVID: D9DAFA03-5B6A-48D2-AD01-B14628D5693C
// Assembly location: C:\Development\Clienti\mycros\MainProject\ExternalLib\MainCloudFramework.Web.dll

using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.Web.UI.WebControls.WebParts
{
  internal class MoveToRight : WebPartVerb
  {
    private const string _copyWebPartImageUrl = "~/Images/right_arrow.png";

    public override string Text
    {
      get
      {
        return "Move Right";
      }
      set
      {
      }
    }

    public override string Description
    {
      get
      {
        return "Allows you to move webparts to the right zone ";
      }
      set
      {
      }
    }

    public override bool Enabled
    {
      get
      {
        return base.Enabled;
      }
      set
      {
        base.Enabled = value;
      }
    }

    public override string ImageUrl
    {
      get
      {
        return "~/Images/right_arrow.png";
      }
      set
      {
      }
    }

    internal MoveToRight(WebPartEventHandler serverClickHandler)
      : base("MyVerbRight", serverClickHandler)
    {
    }
  }
}
