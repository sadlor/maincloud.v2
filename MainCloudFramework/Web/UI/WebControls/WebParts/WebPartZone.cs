﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.Web.UI.WebControls.WebParts
{
    public class WidgetWebPartZone : WebPartZone
    {
        [DebuggerNonUserCode]
        public WidgetWebPartZone()
        {
        }

        protected void AddWatermarkZoneName(HtmlTextWriter writer)
        {
            if (this.DesignMode || this.WebPartManager != null && this.WebPartManager.DisplayMode.AllowPageDesign)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "widget_zone_watermark");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.Write(ID);
                writer.RenderEndTag();
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                base.AddAttributesToRender(writer);
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
                writer.AddAttribute("UniqueID", this.UniqueID);
                if (this.DesignMode || this.WebPartManager != null && this.WebPartManager.DisplayMode.AllowPageDesign)
                    new Style()
                    {
                        BorderColor = Color.Blue,
                        BorderStyle = BorderStyle.Dotted,
                        BorderWidth = Unit.Pixel(1)
                    }.AddAttributesToRender(writer, (WebControl)this);
                if (!string.IsNullOrEmpty(this.CssClass))
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CssClass);
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                base.Render(writer);
            }
            else
            {
                if (this.Page != null)
                    this.Page.VerifyRenderingInServerForm((Control)this);
                this.RenderBeginTag(writer);
                this.RenderContents(writer);
                this.RenderEndTag(writer);
            }
        }

        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                base.RenderBeginTag(writer);
            }
            else
            {
                this.AddAttributesToRender(writer);
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                AddWatermarkZoneName(writer);
            }
        }

        public override void RenderEndTag(HtmlTextWriter writer)
        {
            if (this.DesignMode)
                base.RenderEndTag(writer);
            else
                writer.RenderEndTag();
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                base.RenderContents(writer);
            }
            else
            {
                WebPartCollection webParts = this.WebParts;
                if (webParts == null || webParts.Count == 0)
                {
                    this.RenderEmptyZoneBody(writer);
                }
                else
                {
                    WebPartChrome webPartChrome = (WebPartChrome)this.WebPartChrome;

                    foreach (WebPart webPart in (ReadOnlyCollectionBase)webParts)
                    {
                        if (webPart.ChromeState == PartChromeState.Minimized)
                        {
                            switch (this.GetEffectiveChromeType((Part)webPart))
                            {
                                case PartChromeType.None:
                                case PartChromeType.BorderOnly:
                                    writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
                                    break;
                            }
                        }

                        webPartChrome.RenderWebPart(writer, webPart);

                    }
                }
            }
        }

        private void RenderEmptyZoneBody(HtmlTextWriter writer)
        {
            //bool flag1 = this.LayoutOrientation == Orientation.Vertical;
            //bool flag2 = !flag1;
            string emptyZoneText = this.EmptyZoneText;
            bool flag3 = !this.DesignMode && this.AllowLayoutChange && (this.WebPartManager != null && this.WebPartManager.DisplayMode.AllowPageDesign) && !string.IsNullOrEmpty(emptyZoneText);
            //if (flag1)
            //    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            //if (flag3)
            //    writer.AddAttribute(HtmlTextWriterAttribute.Valign, "top");
            //if (flag2)
            //    writer.AddStyleAttribute(HtmlTextWriterStyle.Width, "100%");
            //else
            //    writer.AddStyleAttribute(HtmlTextWriterStyle.Height, "100%");
            //writer.RenderBeginTag(HtmlTextWriterTag.Td);
            if (flag3)
            {
                Style emptyZoneTextStyle = this.EmptyZoneTextStyle;
                if (!emptyZoneTextStyle.IsEmpty)
                    emptyZoneTextStyle.AddAttributesToRender(writer, (WebControl)this);
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.Write(emptyZoneText);
                writer.RenderEndTag();
            }
            //writer.RenderEndTag();
            //if (flag1)
            //    writer.RenderEndTag();
            if ((!flag3 || !this.DragDropEnabled) && !false)
                return;
            this.RenderDropCue(writer);
        }

        protected override void RenderHeader(HtmlTextWriter writer)
        {
            if (!this.DesignMode)
                throw new NotImplementedException();
            base.RenderHeader(writer);
        }

        protected override void RenderFooter(HtmlTextWriter writer)
        {
            if (!this.DesignMode)
                throw new NotImplementedException();
            base.RenderFooter(writer);
        }

        protected override System.Web.UI.WebControls.WebParts.WebPartChrome CreateWebPartChrome()
        {
            if (this.DesignMode)
                return base.CreateWebPartChrome();
            return (System.Web.UI.WebControls.WebParts.WebPartChrome)new WebPartChrome(this, (WidgetWebPartManager)this.WebPartManager);
        }

        private void OnMoveRight(object sender, WebPartEventArgs e)
        {
            int count = e.WebPart.Zone.WebParts.Count;
            int zoneIndex = e.WebPart.ZoneIndex;
            int index = int.Parse(e.WebPart.Zone.ID.Split("e".ToCharArray())[1].ToString());
            WidgetWebPartManager webPartManager = (WidgetWebPartManager)System.Web.UI.WebControls.WebParts.WebPartManager.GetCurrentWebPartManager(this.Page);
            if (index >= 3)
                return;
            GenericWebPart genericWebPart = (GenericWebPart)webPartManager.Zones[checked(index - 1)].WebParts[zoneIndex];
            webPartManager.MoveWebPart((WebPart)genericWebPart, webPartManager.Zones[index], zoneIndex);
        }

        private void OnMoveLeft(object sender, WebPartEventArgs e)
        {
            HttpContext.Current.Session["CustomZone.Count"] = (object)0;
            int count = e.WebPart.Zone.WebParts.Count;
            int zoneIndex = e.WebPart.ZoneIndex;
            int num = int.Parse(e.WebPart.Zone.ID.Split("e".ToCharArray())[1].ToString());
            HttpContext.Current.Session["CustomZone.Count"] = (object)checked(num - 1);
            WidgetWebPartManager webPartManager = (WidgetWebPartManager)System.Web.UI.WebControls.WebParts.WebPartManager.GetCurrentWebPartManager(this.Page);
            if (num == 1)
                return;
            GenericWebPart genericWebPart = (GenericWebPart)webPartManager.Zones[checked(num - 1)].WebParts[zoneIndex];
            webPartManager.MoveWebPart((WebPart)genericWebPart, webPartManager.Zones[checked(num - 2)], zoneIndex);
        }

        private void OnMoveDown(object sender, WebPartEventArgs e)
        {
            int count = e.WebPart.Zone.WebParts.Count;
            int zoneIndex = e.WebPart.ZoneIndex;
            int num = int.Parse(e.WebPart.Zone.ID.Split("e".ToCharArray())[1].ToString());
            WidgetWebPartManager webPartManager = (WidgetWebPartManager)System.Web.UI.WebControls.WebParts.WebPartManager.GetCurrentWebPartManager(this.Page);
            if (zoneIndex >= checked(count - 1))
                return;
            GenericWebPart genericWebPart = (GenericWebPart)webPartManager.Zones[checked(num - 1)].WebParts[zoneIndex];
            webPartManager.MoveWebPart((WebPart)genericWebPart, webPartManager.Zones[checked(num - 1)], checked(zoneIndex + 1));
        }

        private void OnMoveUp(object sender, WebPartEventArgs e)
        {
            int count = e.WebPart.Zone.WebParts.Count;
            int zoneIndex = e.WebPart.ZoneIndex;
            int num = int.Parse(e.WebPart.Zone.ID.Split("e".ToCharArray())[1].ToString());
            WidgetWebPartManager webPartManager = (WidgetWebPartManager)System.Web.UI.WebControls.WebParts.WebPartManager.GetCurrentWebPartManager(this.Page);
            if (zoneIndex == 0)
                return;
            HttpContext.Current.Session["zoneindex"] = (object)null;
            GenericWebPart genericWebPart = (GenericWebPart)webPartManager.Zones[checked(num - 1)].WebParts[zoneIndex];
            webPartManager.MoveWebPart((WebPart)genericWebPart, webPartManager.Zones[checked(num - 1)], checked(zoneIndex - 1));
        }
    }
}
