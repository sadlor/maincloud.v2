﻿using System;
using System.Collections;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.Web.UI.WebControls.WebParts
{
    public class WebPartChrome : System.Web.UI.WebControls.WebParts.WebPartChrome
    {
        protected WidgetWebPartZone _zone;
        protected WidgetWebPartManager _manager;
        protected PersonalizationScope _personalizationScope;

        public WebPartChrome(WidgetWebPartZone zone, WidgetWebPartManager manager)
            : base((WebPartZoneBase)zone, (System.Web.UI.WebControls.WebParts.WebPartManager)manager)
        {
            this._zone = (WidgetWebPartZone)null;
            this._manager = (WidgetWebPartManager)null;
            this._personalizationScope = PersonalizationScope.Shared;
            this._zone = zone;
            this._manager = manager;
            if (manager == null)
                return;
            this._personalizationScope = manager.Personalization.Scope;
        }

        public override void PerformPreRender()
        {
            // We override the PerformPreRender method to avoid the original implementation (table styles in html header).
            // In SNWebPartChrome the different chrometypes (border only, title and border, etc.) are generated with css classes.
        }

        protected virtual string GetEventArgument(int verbIndex, WebPartVerb verb, WebPart webPart)
        {
            string str = string.Empty;
            string name = verb.GetType().Name;
            if (name == "WebPartMinimizeVerb")
            {
                if (webPart.ChromeState == PartChromeState.Normal)
                    str = "minimize:";
            }
            else if (name == "WebPartRestoreVerb")
            {
                if (webPart.ChromeState == PartChromeState.Minimized)
                    str = "restore:";
            }
            else if (name == "WebPartCloseVerb")
            {
                str = "close:";
            }
            else if (name == "WebPartDeleteVerb")
            {
                str = "delete:";
            }
            else if (name == "WebPartEditVerb")
            {
                str = "edit:";
            }
            else if (name == "WebPartConnectVerb")
            {
                str = "connect:";
            }
            else
            {
                if (verb.ID == null)
                {
                    throw new NotSupportedException();
                }
                str = string.Format("partverb:{0}:", verb.ID);
            }
            return str + webPart.ID;
        }

        protected void RenderVerb(HtmlTextWriter writer, int verbIndex, WebPartVerb verb, WebPart webPart)
        {
            bool flag = this.Zone.Enabled && verb.Enabled;
            ButtonType barVerbButtonType = this.Zone.TitleBarVerbButtonType;
            WebControl webControl;
            if (verb == this.Zone.HelpVerb)
            {
                string str = this.Zone.ResolveClientUrl(webPart.HelpUrl);
                if (barVerbButtonType == ButtonType.Button)
                {
                    ZoneButton zoneButton = new ZoneButton((WebZone)this.Zone, (string)null);
                    if (flag)
                        zoneButton.OnClientClick = "__wpm.ShowHelp('" + MainCloudFramework.Web.Util.QuoteJScriptString(str) + "', " + ((int)webPart.HelpMode).ToString((IFormatProvider)CultureInfo.InvariantCulture) + ");return;";
                    zoneButton.Text = verb.Text;
                    webControl = (WebControl)zoneButton;
                }
                else
                {
                    HyperLink hyperLink = new HyperLink();
                    switch (webPart.HelpMode)
                    {
                        case WebPartHelpMode.Modal:
                        case WebPartHelpMode.Modeless:
                            hyperLink.NavigateUrl = str;
                            hyperLink.Target = "_blank";
                            break;
                        case WebPartHelpMode.Navigate:
                            hyperLink.NavigateUrl = str;
                            break;
                    }
                    hyperLink.Text = verb.Text;
                    if (barVerbButtonType == ButtonType.Image)
                        hyperLink.ImageUrl = verb.ImageUrl;
                    webControl = (WebControl)hyperLink;
                }
            }
            else if (verb == this.Zone.ExportVerb)
            {
                string exportUrl = this._manager.GetExportUrl(webPart);
                if (barVerbButtonType == ButtonType.Button)
                {
                    ZoneButton zoneButton = new ZoneButton((WebZone)this.Zone, string.Empty);
                    zoneButton.Text = verb.Text;
                    if (flag)
                    {
                        if (webPart.ExportMode == WebPartExportMode.All && this._personalizationScope == PersonalizationScope.User)
                            zoneButton.OnClientClick = "__wpm.ExportWebPart('" + MainCloudFramework.Web.Util.QuoteJScriptString(exportUrl) + "', true, false);return false;";
                        else
                            zoneButton.OnClientClick = "window.location='" + MainCloudFramework.Web.Util.QuoteJScriptString(exportUrl) + "';return false;";
                    }
                    webControl = (WebControl)zoneButton;
                }
                else
                {
                    HyperLink hyperLink = new HyperLink();
                    hyperLink.Text = verb.Text;
                    if (barVerbButtonType == ButtonType.Image)
                        hyperLink.ImageUrl = verb.ImageUrl;
                    hyperLink.NavigateUrl = exportUrl;
                    if (webPart.ExportMode == WebPartExportMode.All)
                        hyperLink.Attributes.Add("onclick", "return __wpm.ExportWebPart('', true, true)");
                    webControl = (WebControl)hyperLink;
                }
            }
            else
            {
                string eventArgument = this.GetEventArgument(verbIndex, verb, webPart);
                string clientClickHandler = verb.ClientClickHandler;
                string iconTag = "";
                
                if (verb == Zone.CloseVerb)
                {
                    iconTag = "<span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span> ";

                }
                else if (verb == Zone.DeleteVerb)
                {
                    iconTag = "<span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span> ";

                }
                else if (verb == Zone.MinimizeVerb)
                {
                    iconTag = "<span class=\"glyphicon glyphicon-resize-small\" aria-hidden=\"true\"></span> ";

                }                
                else if (verb == Zone.RestoreVerb)
                {
                    iconTag = "<span class=\"glyphicon glyphicon-resize-full\" aria-hidden=\"true\"></span> ";
                }
                else if (verb == Zone.EditVerb)
                {
                    iconTag = "<span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span> ";
                }

                if (barVerbButtonType == ButtonType.Button)
                {
                    ZoneButton zoneButton = new ZoneButton((WebZone)this.Zone, eventArgument);
                    zoneButton.Text = iconTag + verb.Text;
                    if (!string.IsNullOrEmpty(clientClickHandler) && flag)
                        zoneButton.OnClientClick = clientClickHandler;
                    webControl = (WebControl)zoneButton;
                }
                else
                {
                    ZoneLinkButton zoneLinkButton = new ZoneLinkButton((WebZone)this.Zone, eventArgument);
                    zoneLinkButton.Text = iconTag + verb.Text;
                    if (barVerbButtonType == ButtonType.Image)
                        zoneLinkButton.ImageUrl = verb.ImageUrl;
                    if (!string.IsNullOrEmpty(clientClickHandler) && flag)
                        zoneLinkButton.OnClientClick = clientClickHandler;
                    webControl = (WebControl)zoneLinkButton;
                }
                if (this._manager != null && flag)
                {
                    if (verb == this.Zone.CloseVerb)
                    {
                        ProviderConnectionPointCollection connectionPoints = this._manager.GetProviderConnectionPoints(webPart);
                        if (connectionPoints != null && connectionPoints.Count > 0 && this.ContainsProvider(webPart, this._manager.Connections))
                        {
                            string str = "if (__wpmCloseProviderWarning.length >= 0 && !confirm(__wpmCloseProviderWarning)) { return false; }";
                            webControl.Attributes.Add("onclick", str);
                        }
                    }
                    else if (verb == this.Zone.DeleteVerb)
                    {
                        string str = "if (__wpmDeleteWarning.length >= 0 && !confirm(__wpmDeleteWarning)) { return false; }";
                        webControl.Attributes.Add("onclick", str);
                    }
                }
            }
            webControl.ApplyStyle(this.Zone.TitleBarVerbStyle);
            webControl.ToolTip = string.Format((IFormatProvider)CultureInfo.CurrentCulture, verb.Description, new object[1]
      {
        (object) webPart.DisplayTitle
      });
            webControl.Enabled = verb.Enabled;
            webControl.Page = this._manager.Page;
            webControl.RenderControl(writer);
        }

        protected bool ContainsProvider(WebPart provider, WebPartConnectionCollection list)
        {
            try
            {
                foreach (WebPartConnection webPartConnection in (CollectionBase)list)
                {
                    if (webPartConnection.Provider == provider)
                        return true;
                }
            }
            finally
            {
                //IEnumerator enumerator;
                //if (enumerator is IDisposable)
                //  (enumerator as IDisposable).Dispose();
            }
            return false;
        }

        protected void RenderVerbsInTitle(HtmlTextWriter writer, WebPart webPart)
        {
            WebPartVerbCollection partVerbCollection = this.FilterWebPartVerbs(this.GetWebPartVerbs(webPart), webPart);
            if ((partVerbCollection == null || partVerbCollection.Count <= 0) && !false)
            {
                // Decidere cosa disegnare in caso non sia visibile il menu
                return;
            }
            else 
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "pull-right");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "btn-group");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                writer.Write("<button type=\"button\" class=\"btn btn-default btn-xs dropdown-toggle\" data-toggle=\"dropdown\">Actions<span class=\"caret\"></span></button>");

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown-menu pull-right");
                writer.AddAttribute("role", "menu");
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                int num2 = checked(partVerbCollection.Count - 1);
                int verbIndex = 0;
                while (verbIndex <= num2)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);
                    WebPartVerb verb = partVerbCollection[verbIndex];
                    this.RenderVerb(writer, verbIndex, verb, webPart);
                    checked { ++verbIndex; }
                    writer.RenderEndTag(); // li
                }
                writer.RenderEndTag(); // ul dropdown-menu pull-right
                writer.RenderEndTag(); // div btn-group
                writer.RenderEndTag(); // div pull-right
            }
        }

        public override void RenderWebPart(HtmlTextWriter writer, WebPart webPart)
        {
            if (webPart == null)
                throw new ArgumentNullException("webPart");

            // PartChromeType effectiveChromeType = this.Zone.GetEffectiveChromeType((Part)webPart);

            writer.AddAttribute(HtmlTextWriterAttribute.Id,  this.GetWebPartChromeClientID(webPart));
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "webpart");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, webPart.ID);
            if (string.IsNullOrEmpty(webPart.CssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel panel-default webpart_chrome");
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, webPart.CssClass + " webpart_chrome");
            }            
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel-heading webpart_title");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            
            Page page = webPart.Page;
            if (!string.IsNullOrEmpty(webPart.TitleIconImageUrl))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Src, page.ResolveClientUrl(webPart.TitleIconImageUrl));
                writer.RenderBeginTag(HtmlTextWriterTag.Img);
                writer.RenderEndTag();
            }
            
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel-title");
            writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "inline");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            //writer.Write("<i class=\"fa fa-bar-chart-o fa-fw\"></i>");
            if (string.IsNullOrEmpty(webPart.Title))
                writer.Write("Untitled");
            else
                writer.Write(webPart.Title);
            writer.RenderEndTag();

            this.RenderVerbsInTitle(writer, webPart);

            writer.RenderEndTag();
            if (webPart.ChromeState == PartChromeState.Minimized)
            {
                writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
            }
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "panel-body webpart_body");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            if (!string.IsNullOrEmpty(webPart.Subtitle))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "webpart_subtitle");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.Write(webPart.Subtitle);
                // webpart_subtitle
                writer.RenderEndTag();
            }
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "webpart_data");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            this.RenderPartContents(writer, webPart);
            // webpart_data
            writer.RenderEndTag();
            // webpart_body
            writer.RenderEndTag();
            // webpart_chrome
            writer.RenderEndTag();
            // webpart
            writer.RenderEndTag();
        }
    }
}