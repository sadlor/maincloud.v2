﻿// Decompiled with JetBrains decompiler
// Type: MainCloudFramework.Web.UI.WebControls.WebParts.WebPartManagerExtender
// Assembly: MainCloudFramework.Web, Version=1.0.4095.40444, Culture=neutral, PublicKeyToken=null
// MVID: D9DAFA03-5B6A-48D2-AD01-B14628D5693C
// Assembly location: C:\Development\Clienti\mycros\MainProject\ExternalLib\MainCloudFramework.Web.dll

using AjaxControlToolkit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.Web.UI.WebControls.WebParts
{
  [RequiredScript(typeof (DragDropScripts))]
  [RequiredScript(typeof (CommonToolkitScripts))]
  [ClientScriptResource("MainCloudFramework.Web.WebPartManagerBehavior", "MainCloudFramework.Web.WebPartManagerBehavior")]
  [RequiredScript(typeof (TimerScript))]
  [RequiredScript(typeof (AnimationScripts))]
  [TargetControlType(typeof (Control))]
  public class WebPartManagerExtender : ExtenderControlBase
  {
    [ClientPropertyName("enableDragDrop")]
    [ExtenderControlProperty]
    public bool EnableDragDrop
    {
      get
      {
        return (bool) this.GetPropertyValue<bool>("enableDragDrop", true);
      }
      set
      {
        this.SetPropertyValue<bool>("enableDragDrop", (value ? true : false));
      }
    }

    [ClientPropertyName("zonesIds")]
    [ExtenderControlProperty(true, true)]
    public List<string> ZonesIds
    {
      get
      {
        return (List<string>) this.GetPropertyValue<List<string>>("zones", null);
      }
      set
      {
        this.SetPropertyValue<List<string>>("zones", value);
      }
    }

    //[DebuggerNonUserCode]
    //public WebPartManagerExtender()
    //{
    //  base.\u002Ector();
    //}

    protected override void OnPreRender(EventArgs e)
    {
      WidgetWebPartManager webPartManager = (WidgetWebPartManager) System.Web.UI.WebControls.WebParts.WebPartManager.GetCurrentWebPartManager(((Control) this).Page);
      WebPartZoneCollection zones = webPartManager.Zones;
      List<string> list = new List<string>();
      int num1 = 0;
      int num2 = checked (zones.Count - 1);
      int index = num1;
      while (index <= num2)
      {
        list.Add(zones[index].ClientID);
        checked { ++index; }
      }
      this.ZonesIds = list;
      this.EnableDragDrop = zones.Count >= 1 && webPartManager.DisplayMode != System.Web.UI.WebControls.WebParts.WebPartManager.BrowseDisplayMode || !true;
      base.OnPreRender(e);
    }
  }
}
