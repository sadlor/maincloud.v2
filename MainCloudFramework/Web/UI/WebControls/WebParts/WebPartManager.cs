﻿// Decompiled with JetBrains decompiler
// Type: MainCloudFramework.Web.UI.WebControls.WebParts.WidgetWebPartManager
// Assembly: MainCloudFramework.Web, Version=1.0.4095.40444, Culture=neutral, PublicKeyToken=null
// MVID: D9DAFA03-5B6A-48D2-AD01-B14628D5693C
// Assembly location: C:\Development\Clienti\mycros\MainProject\ExternalLib\MainCloudFramework.Web.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.UI;

namespace MainCloudFramework.Web.UI.WebControls.WebParts
{
  public class WidgetWebPartManager : System.Web.UI.WebControls.WebParts.WebPartManager
  {
    private static List<WeakReference> __ENCList = new List<WeakReference>();

    [DebuggerNonUserCode]
    static WidgetWebPartManager()
    {
    }

    [DebuggerNonUserCode]
    public WidgetWebPartManager()
    {
      lock (WidgetWebPartManager.__ENCList)
        WidgetWebPartManager.__ENCList.Add(new WeakReference((object) this));
    }

    protected override void Render(HtmlTextWriter writer)
    {
      base.Render(writer);
      writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
      writer.RenderBeginTag(HtmlTextWriterTag.Span);
      writer.RenderEndTag();
    }

    protected override void RegisterClientScript()
    {
      this.Page.ClientScript.RegisterClientScriptInclude(typeof (WidgetWebPartManager), "WidgetWebPartManager", this.Page.ResolveClientUrl("~/scripts/webpartmanager.js"));
    }
  }
}
