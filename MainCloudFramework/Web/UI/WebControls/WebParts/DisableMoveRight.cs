﻿// Decompiled with JetBrains decompiler
// Type: MainCloudFramework.Web.UI.WebControls.WebParts.DisableMoveRight
// Assembly: MainCloudFramework.Web, Version=1.0.4095.40444, Culture=neutral, PublicKeyToken=null
// MVID: D9DAFA03-5B6A-48D2-AD01-B14628D5693C
// Assembly location: C:\Development\Clienti\mycros\MainProject\ExternalLib\MainCloudFramework.Web.dll

using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.Web.UI.WebControls.WebParts
{
  internal class DisableMoveRight : WebPartVerb
  {
    private const string _copyWebPartImageUrl = "~/Images/disabled_right_arrow.png";

    public override string Text
    {
      get
      {
        return "Move Right";
      }
      set
      {
      }
    }

    public override string Description
    {
      get
      {
        return "Unable to move ";
      }
      set
      {
      }
    }

    public override bool Enabled
    {
      get
      {
        bool enabled = base.Enabled;
        int num = DisableMoveRight.InlineAssignHelper<bool>(ref enabled, false) ? 1 : 0;
        base.Enabled = enabled;
        return num != 0;
      }
      set
      {
        base.Enabled = value;
      }
    }

    public override string ImageUrl
    {
      get
      {
        return "~/Images/disabled_right_arrow.png";
      }
      set
      {
      }
    }

    internal DisableMoveRight(WebPartEventHandler serverClickHandler)
      : base("Right", serverClickHandler)
    {
    }

    private static T InlineAssignHelper<T>(ref T target, T value)
    {
      target = value;
      return value;
    }
  }
}
