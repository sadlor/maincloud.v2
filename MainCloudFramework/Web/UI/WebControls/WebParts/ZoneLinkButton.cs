﻿// Decompiled with JetBrains decompiler
// Type: MainCloudFramework.Web.UI.WebControls.WebParts.ZoneLinkButton
// Assembly: MainCloudFramework.Web, Version=1.0.4095.40444, Culture=neutral, PublicKeyToken=null
// MVID: D9DAFA03-5B6A-48D2-AD01-B14628D5693C
// Assembly location: C:\Development\Clienti\mycros\MainProject\ExternalLib\MainCloudFramework.Web.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.Web.UI.WebControls.WebParts
{
  [SupportsEventValidation]
  internal sealed class ZoneLinkButton : LinkButton
  {
    private static List<WeakReference> __ENCList = new List<WeakReference>();
    private WebZone _owner;
    private string _eventArgument;
    private string _imageUrl;

    public string ImageUrl
    {
      get
      {
        return this._imageUrl != null ? this._imageUrl : string.Empty;
      }
      set
      {
        this._imageUrl = value;
      }
    }

    [DebuggerNonUserCode]
    static ZoneLinkButton()
    {
    }

    public ZoneLinkButton(WebZone owner, string eventArgument)
    {
      lock (ZoneLinkButton.__ENCList)
        ZoneLinkButton.__ENCList.Add(new WeakReference((object) this));
      if (owner == null)
        throw new ArgumentNullException("owner");
      this._owner = owner;
      this._eventArgument = eventArgument;
    }

    protected override PostBackOptions GetPostBackOptions()
    {
      if ((string.IsNullOrEmpty(this._eventArgument) || this._owner.Page == null) && !false)
        return base.GetPostBackOptions();
      return new PostBackOptions((Control) this._owner, this._eventArgument)
      {
        RequiresJavaScriptProtocol = true
      };
    }

    protected override void RenderContents(HtmlTextWriter writer)
    {
      string imageUrl = this.ImageUrl;
      if (!string.IsNullOrEmpty(imageUrl))
      {
        Image image = new Image();
        image.ImageUrl = this.ResolveClientUrl(imageUrl);
        string toolTip = this.ToolTip;
        if (!string.IsNullOrEmpty(toolTip))
          image.ToolTip = toolTip;
        string text = this.Text;
        if (!string.IsNullOrEmpty(text))
          image.AlternateText = text;
        image.Page = this.Page;
        image.RenderControl(writer);
      }
      else
        base.RenderContents(writer);
    }
  }
}
