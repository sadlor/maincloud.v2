﻿// Decompiled with JetBrains decompiler
// Type: MainCloudFramework.Web.UI.WebControls.WebParts.ZoneButton
// Assembly: MainCloudFramework.Web, Version=1.0.4095.40444, Culture=neutral, PublicKeyToken=null
// MVID: D9DAFA03-5B6A-48D2-AD01-B14628D5693C
// Assembly location: C:\Development\Clienti\mycros\MainProject\ExternalLib\MainCloudFramework.Web.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace MainCloudFramework.Web.UI.WebControls.WebParts
{
  [SupportsEventValidation]
  internal sealed class ZoneButton : Button
  {
    private static List<WeakReference> __ENCList = new List<WeakReference>();
    private WebZone _owner;
    private string _eventArgument;

    [DefaultValue(false)]
    public override bool UseSubmitBehavior
    {
      get
      {
        return false;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    [DebuggerNonUserCode]
    static ZoneButton()
    {
    }

    public ZoneButton(WebZone owner, string eventArgument)
    {
      lock (ZoneButton.__ENCList)
        ZoneButton.__ENCList.Add(new WeakReference((object) this));
      if (owner == null)
        throw new ArgumentNullException("owner");
      this._owner = owner;
      this._eventArgument = eventArgument;
    }

    protected override PostBackOptions GetPostBackOptions()
    {
      if ((string.IsNullOrEmpty(this._eventArgument) || this._owner.Page == null) && !false)
        return base.GetPostBackOptions();
      return new PostBackOptions((Control) this._owner, this._eventArgument)
      {
        ClientSubmit = true
      };
    }
  }
}
