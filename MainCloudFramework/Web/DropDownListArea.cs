﻿using MainCloudFramework.Core.Utilities;
using MainCloudFramework.Models;
using MainCloudFramework.Services;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;

namespace MainCloudFramework.Web
{
    public class DropDownListArea : DropDownList
    {
        public DropDownListArea()
        {
        }

        public void LoadDefualtAreaData()
        {
            string userId = Context.User.Identity.GetUserId();
            string applicationId = MultiTenantsHelper.ApplicationId;
            WidgetAreaService widgetAreaService = new WidgetAreaService();
            DataTextField = "Title";
            DataValueField = "Id";
            DataSource = widgetAreaService.FindAllActiveAreaByApplicationAndUserId(applicationId, userId, ReflectionUtility.GetPropertyName(() => new WidgetArea().Title), true);
        }
    }
}
