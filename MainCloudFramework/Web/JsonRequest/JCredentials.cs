﻿namespace JsonRequest
{
    /// <summary>
    /// This class support JsonRequest on sending credentials for Basic Authentication on HTTP server
    /// </summary>
    public class JCredentials
    {
        public string UserName { get; set; }
        public string Password { get; set; }

        public JCredentials(string userName, string password)
        {
            this.UserName = userName;
            this.Password = password;
        }
    }
}