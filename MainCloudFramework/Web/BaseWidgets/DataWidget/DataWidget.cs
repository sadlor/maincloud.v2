﻿using MainCloudFramework.UI.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace MainCloudFramework.Web.BaseWidgets.DataWidget
{
    public abstract class DataWidget<T> : WidgetControl<T>
    {
        ContentPlaceHolder CphFilter { get; set; }
        ContentPlaceHolder CphMainContent { get; set; }

        public DataWidget(Type widgetType) : base(widgetType)
        {
        }


        public void BuildTemplate()
        {
            CphFilter = new ContentPlaceHolder();
            Controls.Add(CphFilter);
            CphFilter.ID = "CphFilter";

            CphMainContent = new ContentPlaceHolder();
            Controls.Add(CphMainContent);
            CphMainContent.ID = "MainContent";
        }
    }
}