﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace MainCloudFramework.Web.BasePages
{
    public class BaseAdminPage : BasePage
    {
        protected virtual void btnCancel_Click(object sender, EventArgs e)
        {
            IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
        }

        protected string GetValidationMessageException(DbEntityValidationException exdb)
        {
            string msg = "";
            foreach (var e1 in exdb.EntityValidationErrors)
            {
                foreach (var error in e1.ValidationErrors)
                {
                    msg = string.Join(",", error.ErrorMessage);
                }
            }
            return msg;
        }
    }
}
