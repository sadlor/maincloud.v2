﻿using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using MainCloudFramework.Services;

namespace MainCloudFramework.Web.BasePages
{
    public class BasePage : Page
    {
        private GrantService grantService = new GrantService();

        public string CurrentUserId
        {
            get
            {
                return User.Identity != null ? User.Identity.GetUserId() : null;
            }
        }

        public string CurrentUsername
        {
            get
            {
                return User.Identity != null ? User.Identity.Name : "Anonymous";
            }
        }

        public bool IsHostUser
        {
            get
            {
                string userId = User.Identity.GetUserId();
                return grantService.IsHostUser(userId);
            }
        }

        public bool IsAdminUser
        {
            get
            {
                string userId = User.Identity.GetUserId();
                return grantService.IsAdminUser(userId);
            }
        }

        public bool AllowInsertWidgetLayout
        {
            get
            {
                string routePath = (string)Page.RouteData.Values["name"];
                return grantService.AllowInsertWidgetLayout(routePath);
            }
        }
    }
}
