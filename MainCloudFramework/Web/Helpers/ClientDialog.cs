﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MainCloudFramework.Web.Helpers
{
    public static class ClientDialog
    {
        public static String ConfirmDialog(String message, WebControl ctrl) {
            String script = @"javascript:if(BootstrapDialog.confirm('{0}', function(result) {{ if (result) {{ __doPostBack('{1}','') }} }} )) return false;";
            return String.Format(script, message.Replace("'", "\'"), ctrl.UniqueID);
        }
    }
}
