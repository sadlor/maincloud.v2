﻿using JsonRequest;
using MainCloudFramework.Models;
using MainCloudFramework.Repositories;
using MainCloudFramework.RequestDBContext;
using MainCloudFramework.Services;
using MainCloudFramework.UI.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MainCloudFramework.Web.Helpers
{
    public enum ApplicationSettingsKey
    {
        LicenseMaxUser,
        EnergyParameterFilter,
        LicenseMaxAnalyzer,
        LicenseMaxGateway,
        LicenzaMaxPlant,
        LicenseWidgetAdvanced,
        LicenseWidgetAggregator,
        //OperatorDefaultApplicationUID,

        ApplicationId,

        InstalledModuleEnabled,
        RemoteServerUrl,
        BasicAuthUser,
        BasicAuthPassword,

        DaysWaitingBeforeLicenseLock,
        DaysLicenseDuration,
        LicenseCheckIntervalMinutes,
        DataSyncronizeIntervalMinutes,
        LicenseExpirationDate,
        ApplicationRegistrationDate,
        LicenseExpirationContactEmails
    }

    public class SyncronizeSettingsTask
    {
        public SyncronizeSettingsTask(CancellationTokenSource cancellationTokenSource, Task task)
        {
            CancellationTokenSource = cancellationTokenSource;
            Task = task;
        }
        public CancellationTokenSource CancellationTokenSource { get; }
        public Task Task { get; }
    }

    public static class ApplicationSettingsHelper
    {
        private static string SYNCRONIZE_SETTINGS_TASK_APP_KEY = "SYNCRONIZE_SETTINGS_TASK_LIST";

        public static bool IsLicenseExpired()
        {
            DateTime expDate = GetConfiguration<DateTime>(ApplicationSettingsKey.LicenseExpirationDate, MultiTenantsHelper.ApplicationId);
            int dayWaiting = GetConfiguration<int>(ApplicationSettingsKey.DaysWaitingBeforeLicenseLock, MultiTenantsHelper.ApplicationId);
            return expDate.AddDays(dayWaiting) <= DateTime.Now;
        }

        public static IDictionary<string, SyncronizeSettingsTask> StartAllSyncronizationBackgroundService()
        {
            var result = new Dictionary<string, SyncronizeSettingsTask>();
            DbContextLocator<ApplicationDbContext>.DbContextForce = new ApplicationDbContext();
            ApplicationRepository appService = new ApplicationRepository();
            appService.FindAll().ToList().ForEach(app =>
            {
                var started = StartSyncronizationBackgroundService(app.Id);
                if (started != null)
                {
                    result.Add(app.Id, started);
                }
            });
            return result;
        }

        internal static bool IsModuleEnable(string category)
        {
            JArray modules = GetConfiguration(ApplicationSettingsKey.InstalledModuleEnabled) as JArray;
            if (modules == null || modules.Count == 0)
            {
                return false;
            }
            return modules.ToObject<List<string>>().Contains(category);
        }

        public static bool IsRunningSyncronizationBackgroundService(string applicationId)
        {
            var syncronizeSettingsTaskList = HttpContext.Current.Application.Contents[SYNCRONIZE_SETTINGS_TASK_APP_KEY] as IDictionary<string, SyncronizeSettingsTask>;
            if (syncronizeSettingsTaskList != null && syncronizeSettingsTaskList.ContainsKey(applicationId))
            {
                return true;
            }
            return false;
        }

        public static void StopSyncronizationBackgroundService(string applicationId)
        {
            var syncronizeSettingsTaskList = HttpContext.Current.Application.Contents[SYNCRONIZE_SETTINGS_TASK_APP_KEY] as IDictionary<string, SyncronizeSettingsTask>;
            if (syncronizeSettingsTaskList != null && syncronizeSettingsTaskList.ContainsKey(applicationId))
            {
                syncronizeSettingsTaskList[applicationId].CancellationTokenSource.Cancel();
                syncronizeSettingsTaskList.Remove(applicationId);
            }
        }

        public static SyncronizeSettingsTask StartSyncronizationBackgroundService(string applicationId)
        {
            if (GetConfiguration(ApplicationSettingsKey.ApplicationRegistrationDate, applicationId) == null) {
                return null;
            }

            var syncronizeSettingsTaskList = HttpContext.Current.Application.Contents[SYNCRONIZE_SETTINGS_TASK_APP_KEY] as IDictionary<string, SyncronizeSettingsTask> ?? new Dictionary<string, SyncronizeSettingsTask>();
            HttpContext.Current.Application.Contents[SYNCRONIZE_SETTINGS_TASK_APP_KEY] = syncronizeSettingsTaskList;

            StopSyncronizationBackgroundService(applicationId);

            var cancellationTokenSource = new CancellationTokenSource();
            int interval = 0;
            int.TryParse("" + GetConfiguration(ApplicationSettingsKey.LicenseCheckIntervalMinutes, applicationId), out interval);
            interval = interval * 60 * 1000;
            if (interval > 0)
            {
                Task perdiodicTask = PeriodicTaskFactory.Start(() =>
                {
                    Console.WriteLine("StartSyncronizationBackgroundService exec:" + DateTime.Now);
                    SyncronizeSettings(applicationId);
                }, intervalInMilliseconds: interval, delayInMilliseconds:1000, duration: 10, cancelToken: cancellationTokenSource.Token);

                var syncronizeSettingsTask = new SyncronizeSettingsTask(cancellationTokenSource, perdiodicTask);
                syncronizeSettingsTaskList.Add(applicationId, syncronizeSettingsTask);
                return syncronizeSettingsTask;
            }
            return null;
        }

        public static void SyncronizeSettings(string applicationId)
        {
            DbContextLocator<ApplicationDbContext>.DbContextForce = new ApplicationDbContext();
            var request = new JRequest();
            Dictionary<string, object> settings = GetConfigurations(applicationId);
            if (!settings.ContainsKey(ApplicationSettingsKey.ApplicationId.ToString()))
            {
                settings.Add(ApplicationSettingsKey.ApplicationId.ToString(), applicationId);
            }
            string url = settings[ApplicationSettingsKey.RemoteServerUrl.ToString()] as string;
            string basicAuthName = settings[ApplicationSettingsKey.BasicAuthUser.ToString()] as string;
            string basicAuthPassword = settings[ApplicationSettingsKey.BasicAuthPassword.ToString()] as string;
            request.Credentials = new JCredentials(basicAuthName, basicAuthPassword);
            Dictionary<string, object> response = request.Execute<Dictionary<string, object>>(url + "/api/SyncronyzeApplicationSettings", settings, JRequest.VerbPost) as Dictionary<string, object>;
            if (response != null)
            {
                UpdateSettings(response, applicationId);
            }
        }

        public static Dictionary<string, object> UpdateSettingsKeys(Dictionary<string, object> settings)
        {
            foreach (ApplicationSettingsKey applicationSettingsKey in Enum.GetValues(typeof(ApplicationSettingsKey)))
            {
                if (!settings.ContainsKey(applicationSettingsKey.ToString()))
                {
                    settings.Add(applicationSettingsKey.ToString(), null);
                }
            }
            return settings;
        }

        public static Dictionary<string, object> GetConfigurations()
        {
            return GetConfigurations(MultiTenantsHelper.ApplicationId);
        }

        public static Dictionary<string, object> GetConfigurations(string applicationId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                try
                {
                    string settings = db.AspNetApplications.Where(x => x.Id == applicationId).Select(x => x.Settings).FirstOrDefault();
                    if (settings != null || settings != string.Empty)
                    {
                        Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(settings);
                        return config;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static T GetConfiguration<T>(ApplicationSettingsKey key, string applicationId)
        {
            return GetConfiguration<T>(key.ToString(), applicationId);
        }

        public static object GetConfiguration(ApplicationSettingsKey key, string applicationId)
        {
            return GetConfiguration(key.ToString(), applicationId);
        }

        public static object GetConfiguration(ApplicationSettingsKey key)
        {
            return GetConfiguration(key.ToString());
        }

        public static object GetConfiguration(string key)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                try
                {
                    string settings = db.AspNetApplications.Where(x => x.Id == MultiTenantsHelper.ApplicationId).Select(x => x.Settings).FirstOrDefault();
                    if (settings != null && settings != string.Empty)
                    {
                        Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(settings);
                        return config[key];
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static T GetConfiguration<T>(string key, string applicationId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                try
                {
                    string settings = db.AspNetApplications.Where(x => x.Id == applicationId).Select(x => x.Settings).FirstOrDefault();
                    if (settings != null && settings != string.Empty)
                    {
                        Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(settings);
                        return (T)config[key];
                    }
                    else
                    {
                        return default(T);
                    }
                }
                catch (Exception ex)
                {
                    return default(T);
                }
            }
        }

        public static object GetConfiguration(string key, string applicationId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                try
                {
                    string settings = db.AspNetApplications.Where(x => x.Id == applicationId).Select(x => x.Settings).FirstOrDefault();
                    if (settings != null || settings != string.Empty)
                    {
                        Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(settings);
                        return config[key];
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static void UpdateSettings(ApplicationSettingsKey key, object obj, string applicationId)
        {
            UpdateSettings(key.ToString(), obj, applicationId);
        }

        public static void UpdateSettings(ApplicationSettingsKey key, object obj)
        {
            UpdateSettings(key.ToString(), obj);
        }

        public static void UpdateSettings(string key, object obj)
        {
            UpdateSettings(key, obj, MultiTenantsHelper.ApplicationId);
        }

        public static void UpdateSettings(string key, object obj, string applicationId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var application = db.AspNetApplications.Where(x => x.Id == applicationId).FirstOrDefault();
                if (!string.IsNullOrEmpty(application.Settings))
                {
                    Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(application.Settings);
                    if (config.ContainsKey(key))
                    {
                        config[key] = obj;
                        application.Settings = JsonConvert.SerializeObject(config);
                    }
                    else
                    {
                        config.Add(key, obj);
                        application.Settings = JsonConvert.SerializeObject(config);
                    }
                }
                else
                {
                    Dictionary<string, object> config = new Dictionary<string, object>();
                    config.Add(key, obj);
                    application.Settings = JsonConvert.SerializeObject(config);
                }
                db.SaveChanges();
            }
        }

        public static void UpdateSettings(Dictionary<string, object> _params)
        {
            UpdateSettings(_params, MultiTenantsHelper.ApplicationId);
        }

        public static void UpdateSettings(Dictionary<string, object> _params, string applicationId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var settings = db.AspNetApplications.Where(x => x.Id == applicationId).FirstOrDefault();
                if (settings.Settings != null && settings.Settings != string.Empty)
                {
                    Dictionary<string, object> config = JsonConvert.DeserializeObject<Dictionary<string, object>>(settings.Settings);
                    foreach (var param in _params)
                    {
                        if (config.ContainsKey(param.Key))
                        {
                            config[param.Key] = param.Value;
                        }
                        else
                        {
                            config.Add(param.Key, param.Value);
                        }
                    }
                    settings.Settings = JsonConvert.SerializeObject(config);
                }
                else
                {
                    settings.Settings = JsonConvert.SerializeObject(_params);
                }
                db.SaveChanges();
            }
        }
    }
}