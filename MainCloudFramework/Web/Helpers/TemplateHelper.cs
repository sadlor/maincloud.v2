﻿using MainCloudFramework.Web.Templates;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace MainCloudFramework.Web.Helpers
{
    public class TemplateHelper
    {
        public const string RelativePath = "~/Dashboard/Templates";

        public static List<TemplateData> ListLayout
        {
            get
            {
                var layoutPath = Path.Combine(HttpContext.Current.Server.MapPath(RelativePath), "Manifest.json");
                var manifest = File.ReadAllText(layoutPath);
                List <TemplateData> layouts = JsonConvert.DeserializeObject<List<TemplateData>>(manifest);
                return layouts;
            }
        }

        public static string TemplatePath
        {
            get
            {
                return VirtualPathUtility.ToAbsolute(RelativePath);
            }
        }

        public static string GetAbsoluteResourcePath(string resourceFile)
        {
            return TemplatePath + "/" + resourceFile;
        }
    }
}
