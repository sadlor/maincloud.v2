﻿using MainCloudFramework.Models;
using MainCloudFramework.UI.Modules;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MainCloudFramework.Web.Helpers
{
    public class MultiTenantsHelper
    {
        public static string MULTI_TENANT_ROUTE_BASE_URL = "App/{tenant}";
        public static string MULTI_TENANT_ROUTE_ADMIN_BASE_URL = "ApplicationAdmin/{tenant}";
        public static string MULTI_TENANT_ROUTE_HOST_BASE_URL = "Host/{tenant}";
        public static string MULTI_TENANT_ROUTE_AREA_BASE_URL = "App/{tenant}/Area";

        /// <summary>
        /// Restituisce un url nel formato App/{tenant} dove {tenant} è il valore dell'argomento ApplicationName
        /// </summary>
        public static string MountApplicationUrl(string ApplicationName)
        {
            return MULTI_TENANT_ROUTE_BASE_URL.Replace("{tenant}", ApplicationName);
        }

        /// <summary>
        /// Restituisce un url nel formato Host/{tenant} dove {tenant} è il valore dell'argomento ApplicationName
        /// </summary>
        public static string MountHostAdministrationUrl(string ApplicationName)
        {
            return MULTI_TENANT_ROUTE_HOST_BASE_URL.Replace("{tenant}", ApplicationName);
        }

        /// <summary>
        /// Restituisce un url nel formato ApplicationAdmin/{tenant} dove {tenant} è il valore dell'argomento ApplicationName
        /// </summary>
        public static string MountAdministrationUrl(string ApplicationName)
        {
            return MULTI_TENANT_ROUTE_ADMIN_BASE_URL.Replace("{tenant}", ApplicationName);
        }

        /// <summary>
        /// Restituisce un url nel formato ApplicationAdmin/{tenant}/Area dove {tenant} è il valore dell'argomento ApplicationName
        /// </summary>
        public static string MountAreaUrl(string ApplicationName)
        {
            return MULTI_TENANT_ROUTE_AREA_BASE_URL.Replace("{tenant}", ApplicationName);
        }

        /// <summary>
        /// Restituisce un url nel formato ApplicationAdmin/{tenant} dove {tenant} viene sostituito con il tenant nell'url corrente
        /// </summary>
        public static string AdministrationUrl
        {
            get
            {
                return MountAdministrationUrl(ApplicationName as string);
            }
        }

        /// <summary>
        /// Restituisce un url nel formato ApplicationAdmin/{tenant} dove {tenant} viene sostituito con il tenant nell'url corrente
        /// </summary>
        public static string HostAdministrationUrl
        {
            get
            {
                return MountHostAdministrationUrl(ApplicationName as string);
            }
        }

        /// <summary>
        /// Restituisce un url nel formato App/{tenant} dove {tenant} viene sostituito con il tenant nell'url corrente
        /// </summary>
        public static string ApplicationUrl
        {
            get
            {
                return MountApplicationUrl(ApplicationName as string);
            }
        }

        /// <summary>
        /// Restituisce un url nel formato App/{tenant}/Area dove {tenant} viene sostituito con il tenant nell'url corrente
        /// </summary>
        public static string AreaUrl
        {
            get
            {
                return MountAreaUrl(ApplicationName as string);
            }
        }

        /// <summary>
        /// Restituisce il nome dell'application (tenant) correntemente presente nell'url
        /// </summary>
        public static string ApplicationName
        {
            get
            {
                string appName = HttpContext.Current.Request.RequestContext.RouteData.Values["tenant"] as string;
                return appName ?? HttpContext.Current.Request.QueryString["MultiTenant"] as string;
            }
        }

        /// <summary>
        /// Restituisce l'Id dell'application corrente prendendo come nome dell'application il valroe del tenant presente nell'Url
        /// </summary>
        public static string ApplicationId
        {
            get
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return GetApplicationId(db);
                }
            }
        }

        public static string GetApplicationId(ApplicationDbContext db)
        {
            var appId = from application in db.AspNetApplications
                        where application.Name == ApplicationName
                        select application.Id;
            return appId.SingleOrDefault();
        }

        /// <summary>
        /// Restituisce un url nel formato App/{tenant}/Area/<code>areaName</code> dove {tenant} viene sostituito 
        /// con il tenant nell'url corrente e urlToken è il valore dell'argomento value
        /// </summary>
        public static string MountMultiTenantAreaUrl(string areaName)
        {
            return string.Format("~/{0}/{1}", AreaUrl, areaName.TrimStart('/'));
        }

        /// <summary>
        /// Restituisce un url nel formato App/{tenant}/<code>urlToken</code> dove {tenant} viene sostituito 
        /// con il tenant nell'url corrente e urlToken è il valore dell'argomento value
        /// </summary>
        public static string MountMultiTenantUrl(string urlToken)
        {
            return string.Format("~/{0}/{1}", ApplicationUrl, urlToken.TrimStart('/'));
        }

        public static string MountAndResolveMultiTenantUrl(string baseUrl)
        {
            return VirtualPathUtility.ToAbsolute(baseUrl + "?MultiTenant=" + ApplicationName);
        }

        public static string CreateApplication()
        {
            string applicationId = ApplicationId;
            if (applicationId == null)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    CreateApplication(db);
                }
            }
            return applicationId;
        }

        /// <summary>
        /// Utilizza il nome dell'applicazione contenuto nell'url, se non esiste crea una nuova entità applicaizone con tale nome
        /// </summary>
        /// <returns>ApplicationId</returns>
        public static string CreateApplication(ApplicationDbContext db)
        {
            string applicationId = ApplicationId;
            if (applicationId == null)
            {
                applicationId = db.AspNetApplications.Add(new AspNetApplications()
                {
                    Name = ApplicationName,
                    URL = "/" + ApplicationName
                }).Id;
                db.SaveChanges();
            }
            return applicationId;
        }

        public static string UploadApplicationWidgetRootPath()
        {
            string root = HttpContext.Current.Server.MapPath("~");
            return Path.Combine(root, "Uploads", ApplicationId, "Widgets");
        }

        public static string UploadApplicationPathShared()
        {
            string path = null;
            path = Path.Combine(UploadApplicationWidgetRootPath(), "Shared");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return path;
        }

        public static string UploadApplicationWidgetPath(WidgetControl<object> widget)
        {
            string path = null;
            if (widget != null)
            {
                path = Path.Combine(UploadApplicationWidgetRootPath(), widget.WidgetId);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            return path;
        }

        public static string UploadApplicationWidgetResourcePath(string resourceFileName, WidgetControl<object> widget)
        {
            string relativePath = null;
            if (resourceFileName != null && widget != null)
            {
                string resourceFullPath = Path.Combine(UploadApplicationWidgetPath(widget), resourceFileName);
                relativePath = resourceFullPath.Replace(HttpContext.Current.Server.MapPath("~/"), "~/").Replace(@"\", "/");
            }
            return relativePath;
        }

        public static string UploadApplicationWidgetResourcePathShared(string resourceFileName)
        {
            string relativePath = null;
            if (resourceFileName != null)
            {
                string resourceFullPath = Path.Combine(UploadApplicationPathShared(), resourceFileName);
                relativePath = resourceFullPath.Replace(HttpContext.Current.Server.MapPath("~/"), "~/").Replace(@"\", "/");
            }
            return relativePath;
        }
        
    }
}
