﻿using MainCloudFramework.Models;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using MainCloudFramework.Services;
using System.Web;
using Microsoft.AspNet.Identity;

namespace MainCloudFramework.Repository
{
    public class AreaHelper
    {
        public static List<WidgetArea> FindAllActiveAreaByApplicationAndUserId(ApplicationDbContext db, string applicationId, string userId, string sortBy, bool sortAscending)
        {
            return FindAllActiveAreaByApplicationAndUserId(db, applicationId, userId, false, sortBy, sortAscending);
        }

        public static object FindAllActiveStarredAreaByApplicationAndUserId(ApplicationDbContext db, string applicationId, string userId, string sortBy, bool sortAscending) 
        {
            return FindAllActiveAreaByApplicationAndUserId(db, applicationId, userId, true, sortBy, sortAscending);
        }

        private static List<WidgetArea> FindAllActiveAreaByApplicationAndUserId(ApplicationDbContext db, string applicationId, string userId, bool onlyStarred, string orderBy, bool sortAscending)
        {
                    GrantService grantService = new GrantService();

        var allAppAreas = from a in db.WidgetAreaSet
                              where a.ApplicationId == applicationId &&
                              a.DeletedAt == null && a.Visible // Non eliminata e visibile
                              select a;
            if (onlyStarred)
            {
                allAppAreas = allAppAreas.Where(x => x.Starred == true);
            }

            if (sortAscending)
            {
                allAppAreas = allAppAreas.OrderBy(orderBy);
            }
            else
            {
                allAppAreas = allAppAreas.OrderBy(orderBy + " descending");
            }

            if (grantService.IsHostUser(HttpContext.Current.User.Identity.GetUserId()))
            {
                return allAppAreas.ToList();
            }
            else
            {
                var areas = from a in allAppAreas
                        where a.OwnerUserId == null || a.OwnerUserId == userId // Se pubblico o SOLO se di proprietà dell'utente
                        select a;
                return areas.ToList();
            }
        }
    }
}
