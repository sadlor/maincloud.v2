﻿using MainCloudFramework.UI.Utilities;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using MainCloudFramework.Services;

namespace MainCloudFramework.Web
{
    public class MultitenantAdminNavigateLinkButton : HyperLink
    {
        private GrantService grantService = new GrantService();

        public string RouthPath
        {
            set
            {
                NavigateUrl = string.Format("~/{0}/{1}", MultiTenantsHelper.AdministrationUrl, value.TrimStart('/'));
                NavigateUrl += "?ReturnUrl=~" + HttpContext.Current.Request.Url.AbsolutePath;
            }
        }

        public override bool Visible
        {
            get
            {
                if (grantService.IsHostUser() || grantService.IsAdminUser())
                {
                    return base.Visible;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                base.Visible = value;
            }
        }
    }
}
