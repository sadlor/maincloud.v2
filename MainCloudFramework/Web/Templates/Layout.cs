﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainCloudFramework.Web.Templates
{
    public class TemplateData
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Template { get; set; }
        public string TemplatePreView { get; set; }
    }
}
