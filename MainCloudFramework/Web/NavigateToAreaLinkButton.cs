﻿using MainCloudFramework.UI.Utilities;
using MainCloudFramework.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace MainCloudFramework.Web
{
    public class NavigateToAreaLinkButton : HyperLink
    {
        public string RouthPath
        {
            set
            {
                NavigateUrl = MultiTenantsHelper.MountMultiTenantAreaUrl(value);
            }
        }
    }
}
