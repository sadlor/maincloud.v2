﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.ComponentModel;
using System.Globalization;
using System.Web;
using System.Web.DataAccess;
using System.Web.Util;
using System.Web.UI.WebControls.WebParts;
using MainCloudFramework.Web;
using System.Diagnostics;
using MainCloudFramework.Web.Helpers;
using MainCloudFramework.Models;
using System.Linq;
using MainCloudFramework.UI.Utilities;
using System.Reflection;
using System.Web.UI.WebControls.WebParts;
using Newtonsoft.Json;
using System.Web.UI;
using System.IO;

namespace MainCloudFramework.Personalization
{
    /// <devdoc>
    /// The provider used to access the personalization store for Widgets pages from a EntityFramework. 
    /// </devdoc>
    public class WidgetPersonalizationProvider : PersonalizationProvider
    {
        internal static readonly DateTime DefaultInactiveSinceDate = DateTime.MaxValue;

        private enum ResetUserStateMode
        {
            PerInactiveDate,
            PerPaths,
            PerUsers
        }

        private const int maxStringLength = 256;

        private string _applicationName;

        /// <devdoc>
        /// Initializes an instance of SqlPersonalizationProvider. 
        /// </devdoc>
        public WidgetPersonalizationProvider()
        {
        }

        public override string ApplicationName
        {
            get
            {
                if (string.IsNullOrEmpty(_applicationName))
                {
                    //_applicationName = SecUtility.GetDefaultAppName();
                    _applicationName = MultiTenantsHelper.ApplicationName;
                }
                return _applicationName;
            }
            set
            {
                if (value != null && value.Length > maxStringLength)
                {
                    throw new ProviderException(SR.GetString(
                        SR.PersonalizationProvider_ApplicationNameExceedMaxLength, maxStringLength.ToString(CultureInfo.CurrentCulture)));
                }
                _applicationName = value;
            }
        }

        private PersonalizationStateInfoCollection FindSharedState(string path,
                                                                   int pageIndex,
                                                                   int pageSize,
                                                                   out int totalRecords)
        {
            totalRecords = 0;

            // Extra try-catch block to prevent elevation of privilege attack via exception filter 
            try
            {
                try
                {
                    //                    Debug.Assert(connection != null);

                    Nullable<int> returnValue = null;

                    PersonalizationStateInfoCollection sharedStateInfoCollection = new PersonalizationStateInfoCollection();
                    string applicationId = MultiTenantsHelper.ApplicationId;
                    if (applicationId != null)
                    {
                        int pageLowerBound = pageSize * pageIndex;
                        int pageUpperBound = pageSize - 1 + pageLowerBound;

                        using (ApplicationDbContext db = new ApplicationDbContext())
                        {
                            var reader = from p in db.AspNetPaths
                                         from sharedDataPerPath in (from paths in db.AspNetPaths
                                                                    from allUsers in db.AspNetPersonalizationAllUsers
                                                                    where paths.ApplicationId == applicationId && allUsers.PathId == paths.Id && (path == null || paths.Path.ToLower().Contains(path))
                                                                    select paths.Id)
                                         from userDataPerPath in (from paths in db.AspNetPaths
                                                                  from perUser in db.AspNetPersonalizationPerUser
                                                                  where paths.ApplicationId == applicationId && perUser.PathId == paths.Id && (path == null || paths.Path.ToLower().Contains(path))
                                                                  select paths.Id)
                                         where p.Id == sharedDataPerPath || p.Id == userDataPerPath
                                         orderby p.Path
                                         select p;

                            if (reader != null)
                            {
                                foreach (var returnedPath in reader)
                                {
                                    // Data can be null if there is no data associated with the path
                                    //DateTime lastUpdatedDate = (reader.IsDBNull(1)) ? DateTime.MinValue :
                                    //                                DateTime.SpecifyKind(reader.GetDateTime(1), DateTimeKind.Utc);
                                    //int size = (reader.IsDBNull(2)) ? 0 : reader.GetInt32(2);
                                    //int userDataSize = (reader.IsDBNull(3)) ? 0 : reader.GetInt32(3);
                                    //int userCount = (reader.IsDBNull(4)) ? 0 : reader.GetInt32(4);
                                    //sharedStateInfoCollection.Add(new SharedPersonalizationStateInfo(
                                    //    returnedPath, lastUpdatedDate, size, userDataSize, userCount));
                                }
                            }
                        }
                    }

                    // Set the total count at the end after all operations pass
                    if (returnValue.Value != null && returnValue.Value is int)
                    {
                        totalRecords = (int)returnValue.Value;
                    }

                    return sharedStateInfoCollection;
                }
                finally { }
            }
            catch
            {
                throw;
            }
        }

        public override PersonalizationStateInfoCollection FindState(PersonalizationScope scope,
                                                                     PersonalizationStateQuery query,
                                                                     int pageIndex, int pageSize,
                                                                     out int totalRecords)
        {
            PersonalizationProviderHelper.CheckPersonalizationScope(scope);
            PersonalizationProviderHelper.CheckPageIndexAndSize(pageIndex, pageSize);

            if (scope == PersonalizationScope.Shared)
            {
                string pathToMatch = null;
                if (query != null)
                {
                    pathToMatch = StringUtil.CheckAndTrimString(query.PathToMatch, "query.PathToMatch", false, maxStringLength);
                }
                return FindSharedState(pathToMatch, pageIndex, pageSize, out totalRecords);
            }
            else
            {
                string pathToMatch = null;
                DateTime inactiveSinceDate = DefaultInactiveSinceDate;
                string usernameToMatch = null;
                if (query != null)
                {
                    pathToMatch = StringUtil.CheckAndTrimString(query.PathToMatch, "query.PathToMatch", false, maxStringLength);
                    inactiveSinceDate = query.UserInactiveSinceDate;
                    usernameToMatch = StringUtil.CheckAndTrimString(query.UsernameToMatch, "query.UsernameToMatch", false, maxStringLength);
                }

                return FindUserState(pathToMatch, inactiveSinceDate, usernameToMatch,
                                     pageIndex, pageSize, out totalRecords);
            }
        }

        private PersonalizationStateInfoCollection FindUserState(string path,
                                                                 DateTime inactiveSinceDate,
                                                                 string username,
                                                                 int pageIndex,
                                                                 int pageSize,
                                                                 out int totalRecords)
        {
            //SqlConnectionHolder connectionHolder = null;
            //SqlConnection connection = null;
            //SqlDataReader reader = null;
            totalRecords = 0;

            //// Extra try-catch block to prevent elevation of privilege attack via exception filter
            //try
            //{
            //    try
            //    {
            //        connectionHolder = GetConnectionHolder();
            //        connection = connectionHolder.Connection;
            //        Debug.Assert(connection != null);



            //        SqlCommand command = new SqlCommand("dbo.aspnet_PersonalizationAdministration_FindState", connection);
            //        SetCommandTypeAndTimeout(command);
            //        SqlParameterCollection parameters = command.Parameters;

            //        SqlParameter parameter = parameters.Add(new SqlParameter("AllUsersScope", SqlDbType.Bit));
            //        parameter.Value = false;

            //        parameters.AddWithValue("ApplicationName", ApplicationName);
            //        parameters.AddWithValue("PageIndex", pageIndex);
            //        parameters.AddWithValue("PageSize", pageSize);

            //        SqlParameter returnValue = new SqlParameter("@ReturnValue", SqlDbType.Int);
            //        returnValue.Direction = ParameterDirection.ReturnValue;
            //        parameters.Add(returnValue);

            //        parameter = parameters.Add("Path", SqlDbType.NVarChar);
            //        if (path != null)
            //        {
            //            parameter.Value = path;
            //        }

            //        parameter = parameters.Add("UserName", SqlDbType.NVarChar);
            //        if (username != null)
            //        {
            //            parameter.Value = username;
            //        }

            //        parameter = parameters.Add("InactiveSinceDate", SqlDbType.DateTime);
            //        if (inactiveSinceDate != PersonalizationAdministration.DefaultInactiveSinceDate)
            //        {
            //            parameter.Value = inactiveSinceDate.ToUniversalTime();
            //        }

            //        reader = command.ExecuteReader(CommandBehavior.SequentialAccess);
            PersonalizationStateInfoCollection stateInfoCollection = new PersonalizationStateInfoCollection();

            //        if (reader != null)
            //        {
            //            if (reader.HasRows)
            //            {
            //                while (reader.Read())
            //                {
            //                    string returnedPath = reader.GetString(0);
            //                    DateTime lastUpdatedDate = DateTime.SpecifyKind(reader.GetDateTime(1), DateTimeKind.Utc);
            //                    int size = reader.GetInt32(2);
            //                    string returnedUsername = reader.GetString(3);
            //                    DateTime lastActivityDate = DateTime.SpecifyKind(reader.GetDateTime(4), DateTimeKind.Utc);
            //                    stateInfoCollection.Add(new UserPersonalizationStateInfo(
            //                                                    returnedPath, lastUpdatedDate,
            //                                                    size, returnedUsername, lastActivityDate));
            //                }
            //            }

            //            // The reader needs to be closed so return value can be accessed
            //            // See MSDN doc for SqlParameter.Direction for details. 
            //            reader.Close();
            //            reader = null;
            //        }

            //        // Set the total count at the end after all operations pass 
            //        if (returnValue.Value != null && returnValue.Value is int)
            //        {
            //            totalRecords = (int)returnValue.Value;
            //        }

            return stateInfoCollection;
            //    }
            //    finally
            //    {
            //        if (reader != null)
            //        {
            //            reader.Close();
            //        }

            //        if (connectionHolder != null)
            //        {
            //            connectionHolder.Close();
            //            connectionHolder = null;
            //        }
            //    }
            //}
            //catch
            //{
            //    throw;
            //}
        }

        private int GetCountOfSharedState(string path)
        {
            int count = 0;

            // Extra try-catch block to prevent elevation of privilege attack via exception filter
            try
            {
                try
                {
                    string applicationId = MultiTenantsHelper.ApplicationId;

                    if (applicationId != null)
                    {

                        // dbo.aspnet_PersonalizationAdministration_GetCountOfState
                        // AllUsersScope = true
                        ApplicationDbContext db = new ApplicationDbContext();

                        count = (from allUsers in db.AspNetPersonalizationAllUsers
                                 from paths in db.AspNetPaths
                                 where paths.ApplicationId == applicationId && allUsers.PathId == paths.Id && (path == null || paths.Path.ToLower().Contains(path))
                                 select allUsers).Count();
                    }
                }
                finally
                {
                }
            }
            catch
            {
                throw;
            }

            return count;
        }

        public override int GetCountOfState(PersonalizationScope scope, PersonalizationStateQuery query)
        {
            PersonalizationProviderHelper.CheckPersonalizationScope(scope);
            if (scope == PersonalizationScope.Shared)
            {
                string pathToMatch = null;
                if (query != null)
                {
                    pathToMatch = StringUtil.CheckAndTrimString(query.PathToMatch, "query.PathToMatch", false, maxStringLength);
                }
                return GetCountOfSharedState(pathToMatch);
            }
            else
            {
                string pathToMatch = null;
                DateTime userInactiveSinceDate = DefaultInactiveSinceDate;
                string usernameToMatch = null;
                if (query != null)
                {
                    pathToMatch = StringUtil.CheckAndTrimString(query.PathToMatch, "query.PathToMatch", false, maxStringLength);
                    userInactiveSinceDate = query.UserInactiveSinceDate;
                    usernameToMatch = StringUtil.CheckAndTrimString(query.UsernameToMatch, "query.UsernameToMatch", false, maxStringLength);
                }
                return GetCountOfUserState(pathToMatch, userInactiveSinceDate, usernameToMatch);
            }
        }

        private int GetCountOfUserState(string path, DateTime inactiveSinceDate, string username)
        {
            int count = 0;

            // Extra try-catch block to prevent elevation of privilege attack via exception filter
            try
            {
                try
                {
                    string applicationId = MultiTenantsHelper.ApplicationId;

                    if (applicationId != null)
                    {

                        // dbo.aspnet_PersonalizationAdministration_GetCountOfState
                        // AllUsersScope = false
                        ApplicationDbContext db = new ApplicationDbContext();

                        count = (from perUser in db.AspNetPersonalizationPerUser
                                 from users in db.Users
                                 from paths in db.AspNetPaths
                                 where paths.ApplicationId == applicationId &&
                                 perUser.UserId == users.Id &&
                                 perUser.PathId == paths.Id &&
                                (path == null || paths.Path.ToLower().Contains(path)) &&
                                (username == null || users.UserName.ToLower().Contains(username))
                                 // TODO: IMPORTANT                           &&     (inactiveSinceDate == null || users.LastActivityDate < inactiveSinceDate)
                                 select perUser).Count();
                    }
                }
                finally
                {
                }
            }
            catch
            {
                throw;
            }

            return count;
        }

        public override void Initialize(string name, NameValueCollection configSettings)
        {
            //HttpRuntime.CheckAspNetHostingPermission(AspNetHostingPermissionLevel.Low, SR.Feature_not_supported_at_this_level);

            // configSettings cannot be null because there are required settings needed below
            if (configSettings == null)
            {
                throw new ArgumentNullException("configSettings");
            }

            if (String.IsNullOrEmpty(name))
            {
                name = "SqlPersonalizationProvider";
            }

            // description will be set from the base class' Initialize method
            if (string.IsNullOrEmpty(configSettings["description"]))
            {
                configSettings.Remove("description");
                //var d = SR.GetString(SR.SqlPersonalizationProvider_Description);
                configSettings.Add("description", "WidgetPersonalizationProvider");
            }
            base.Initialize(name, configSettings);

            // If not available, the default value is set in the get accessor of ApplicationName
            _applicationName = configSettings["applicationName"];
            if (_applicationName != null)
            {
                configSettings.Remove("applicationName");

                if (_applicationName.Length > maxStringLength)
                {
                    throw new ProviderException(SR.GetString(
                        SR.PersonalizationProvider_ApplicationNameExceedMaxLength, maxStringLength.ToString(CultureInfo.CurrentCulture)));
                }
            }

            //string connectionStringName = configSettings["connectionStringName"];
            //if (String.IsNullOrEmpty(connectionStringName))
            //{
            //    throw new ProviderException(SR.GetString(SR.PersonalizationProvider_NoConnection));
            //}
            //configSettings.Remove("connectionStringName");

            //string connectionString = SqlConnectionHelper.GetConnectionString(connectionStringName, true, true);
            //if (String.IsNullOrEmpty(connectionString))
            //{
            //    throw new ProviderException(SR.GetString(SR.PersonalizationProvider_BadConnection, connectionStringName));
            //}
            //_connectionString = connectionString;

            //_commandTimeout = SecUtility.GetIntValue(configSettings, "commandTimeout", -1, true, 0);
            //configSettings.Remove("commandTimeout");

            if (configSettings.Count > 0)
            {
                string invalidAttributeName = configSettings.GetKey(0);
                throw new ProviderException(SR.GetString(SR.PersonalizationProvider_UnknownProp, invalidAttributeName, name));
            }
        }

        /// <devdoc> 
        /// </devdoc>
        private byte[] LoadPersonalizationBlob(ApplicationDbContext db, string path, string userName)
        {
            Debug.Assert(db != null);
            Debug.Assert(!string.IsNullOrEmpty(path));

            string applicationId = MultiTenantsHelper.ApplicationId;
            if (applicationId == null) return null;

            string pathId = db.AspNetPaths.Where(u => u.ApplicationId == applicationId && u.Path.ToLower() == path.ToLower()).Select(u => u.Id).SingleOrDefault();
            if (pathId == null) return null;

            string reader = null;

            // TODO: aggiornamneto ultima attività
            //          UPDATE dbo.aspnet_Users WITH (ROWLOCK)
            //SET      LastActivityDate = @CurrentTimeUtc
            //  WHERE    UserId = @UserId
            //  IF(@@ROWCOUNT = 0)-- Username not found
            //      RETURN

            if (userName != null)
            {
                // dbo.aspnet_PersonalizationPerUser_GetPageSettings
                string userId = db.Users.Where(u => u.Applications.Any(x => x.Id == applicationId) && u.UserName.ToLower() == userName.ToLower()).Select(u => u.Id).SingleOrDefault();
                if (userId == null) return null;

                reader = (from p in db.AspNetPersonalizationPerUser
                          where p.PathId == pathId && p.UserId == userId
                          select p.PageSettings).SingleOrDefault(); // command.ExecuteReader(CommandBehavior.SingleRow);
            }
            else
            {
                // dbo.aspnet_PersonalizationAllUsers_GetPageSettings

                reader = (from p in db.AspNetPersonalizationAllUsers
                          where p.PathId == pathId
                          select p.PageSettings).SingleOrDefault(); // command.ExecuteReader(CommandBehavior.SingleRow);
            }

            try
            {
                if (reader != null)
                {
                    byte[] state = Convert.FromBase64String(reader);
                    return state;
                }
            }
            finally
            {
            }

            return null;
        }

        /// <internalonly />
        protected override void LoadPersonalizationBlobs(WebPartManager webPartManager, string path, string userName, ref byte[] sharedDataBlob, ref byte[] userDataBlob)
        {
            sharedDataBlob = null;
            userDataBlob = null;

            // Extra try-catch block to prevent elevation of privilege attack via exception filter 
            try
            {
                try
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        sharedDataBlob = LoadPersonalizationBlob(db, path, null);
                        if (!string.IsNullOrEmpty(userName))
                        {
                            userDataBlob = LoadPersonalizationBlob(db, path, userName);
                        }
                    }
                }
                finally
                {
                }
            }
            catch
            {
                throw;
            }
        }

        /// <devdoc>
        /// </devdoc>
        private void ResetPersonalizationState(ApplicationDbContext db, string path, string userName)
        {
            Debug.Assert(db != null);
            Debug.Assert(!string.IsNullOrEmpty(path));

            string applicationId = MultiTenantsHelper.ApplicationId;
            if (applicationId == null) return;

            string pathId = db.AspNetPaths.Where(u => u.ApplicationId == applicationId && u.Path.ToLower() == path.ToLower()).Select(u => u.Id).SingleOrDefault();
            if (pathId == null) return;

            if (userName != null)
            {
                string userId = db.Users.Where(u => u.Applications.Any(x => x.Id == applicationId) && u.UserName.ToLower() == userName.ToLower()).Select(u => u.Id).SingleOrDefault();
                if (pathId == null) return;
                // TODO Update user last activity statistics
                var toRemove = db.AspNetPersonalizationPerUser.Where(p => p.PathId == pathId && p.UserId == userId);
                db.AspNetPersonalizationPerUser.RemoveRange(toRemove);
            }
            else
            {
                var toRemove = db.AspNetPersonalizationAllUsers.Where(p => p.PathId == pathId);
                db.AspNetPersonalizationAllUsers.RemoveRange(toRemove);

            }

            db.SaveChanges();
        }

        /// <internalonly />
        protected override void ResetPersonalizationBlob(WebPartManager webPartManager, string path, string userName)
        {
            // Extra try-catch block to prevent elevation of privilege attack via exception filter 
            try
            {
                try
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        ResetPersonalizationState(db, path, userName);
                    }
                }
                finally
                {
                }
            }
            catch
            {
                throw;
            }
        }

        private int ResetAllState(PersonalizationScope scope)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                int count = 0;

                // Extra try-catch block to prevent elevation of privilege attack via exception filter 
                try
                {
                    try
                    {
                        Debug.Assert(db != null);

                        string applicationId = MultiTenantsHelper.ApplicationId;
                        if (applicationId == null) return 0;

                        if (scope == PersonalizationScope.Shared)
                        {
                            var paths = db.AspNetPaths.Where(p => p.ApplicationId == applicationId).Select(p => p.Id);
                            var rowsToDelete = db.AspNetPersonalizationAllUsers.Where(p => paths.Contains(p.PathId));
                            count = db.AspNetPersonalizationAllUsers.RemoveRange(rowsToDelete).Count();
                        }
                        else
                        {
                            var paths = db.AspNetPaths.Where(p => p.ApplicationId == applicationId).Select(p => p.Id);
                            var rowsToDelete = db.AspNetPersonalizationPerUser.Where(p => paths.Contains(p.PathId));
                            count = db.AspNetPersonalizationPerUser.RemoveRange(rowsToDelete).Count();
                        }

                        db.SaveChanges();
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }

                return count;
            }
        }

        private int ResetSharedState(string[] paths)
        {
            int resultCount = 0;

            if (paths == null)
            {
                resultCount = ResetAllState(PersonalizationScope.Shared);
            }
            else
            {
                // Extra try-catch block to prevent elevation of privilege attack via exception filter
                try
                {
                    try
                    {
                        using (ApplicationDbContext db = new ApplicationDbContext())
                        {
                            Debug.Assert(db != null);

                            string applicationId = MultiTenantsHelper.ApplicationId;
                            if (applicationId == null) return 0;

                            var lowerPath = paths.Select(p => p.ToLower());
                            var allUsers = from allUsersTbl in db.AspNetPersonalizationAllUsers
                                           from pathsTbl in db.AspNetPaths
                                           where pathsTbl.ApplicationId == applicationId
                                           && allUsersTbl.PathId == pathsTbl.Id
                                           && lowerPath.Contains(pathsTbl.Path.ToLower())
                                           select allUsersTbl.PathId;
                            var rowsToDelete = db.AspNetPersonalizationAllUsers.Where(p => allUsers.Contains(p.PathId));
                            resultCount = db.AspNetPersonalizationAllUsers.RemoveRange(rowsToDelete).Count();
                            db.SaveChanges();
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
            }

            return resultCount;
        }

        public override int ResetUserState(string path, DateTime userInactiveSinceDate)
        {
            path = StringUtil.CheckAndTrimString(path, "path", false, maxStringLength);
            string[] paths = (path == null) ? null : new string[] { path };
            return ResetUserState(ResetUserStateMode.PerInactiveDate,
                                  userInactiveSinceDate, paths, null);
        }

        public override int ResetState(PersonalizationScope scope, string[] paths, string[] usernames)
        {
            PersonalizationProviderHelper.CheckPersonalizationScope(scope);
            paths = PersonalizationProviderHelper.CheckAndTrimNonEmptyStringEntries(paths, "paths", false, false, maxStringLength);
            usernames = PersonalizationProviderHelper.CheckAndTrimNonEmptyStringEntries(usernames, "usernames", false, true, maxStringLength);

            if (scope == PersonalizationScope.Shared)
            {
                PersonalizationProviderHelper.CheckUsernamesInSharedScope(usernames);
                return ResetSharedState(paths);
            }
            else
            {
                PersonalizationProviderHelper.CheckOnlyOnePathWithUsers(paths, usernames);
                return ResetUserState(paths, usernames);
            }
        }

        private int ResetUserState(string[] paths, string[] usernames)
        {
            int count = 0;
            bool hasPaths = !(paths == null || paths.Length == 0);
            bool hasUsernames = !(usernames == null || usernames.Length == 0);

            if (!hasPaths && !hasUsernames)
            {
                count = ResetAllState(PersonalizationScope.User);
            }
            else if (!hasUsernames)
            {
                count = ResetUserState(ResetUserStateMode.PerPaths,
                                       DefaultInactiveSinceDate,
                                       paths, usernames);
            }
            else
            {
                count = ResetUserState(ResetUserStateMode.PerUsers,
                                       DefaultInactiveSinceDate,
                                       paths, usernames);
            }

            return count;
        }

        private int ResetUserState(ResetUserStateMode mode,
                                   DateTime userInactiveSinceDate,
                                   string[] paths,
                                   string[] usernames)
        {
            int count = 0;

            // Extra try-catch block to prevent elevation of privilege attack via exception filter
            try
            {
                try
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        Debug.Assert(db != null);

                        string applicationId = MultiTenantsHelper.ApplicationId;
                        if (applicationId == null) return 0;

                        string firstPath = (paths != null && paths.Length > 0) ? paths[0] : null;

                        string[] lowerPaths = null;
                        string[] lowerUserName = null;


                        if (mode == ResetUserStateMode.PerInactiveDate)
                        {
                            //if (userInactiveSinceDate != PersonalizationAdministration.DefaultInactiveSinceDate)
                            //{
                            //    // Special note: DateTime object cannot be added to collection 
                            //    // via AddWithValue for some reason.
                            //    parameter = parameters.Add("InactiveSinceDate", SqlDbType.DateTime);
                            //    parameter.Value = userInactiveSinceDate.ToUniversalTime();
                            //}

                            lowerPaths = firstPath != null ? new string[] { firstPath.ToLower() } : null;
                        }
                        else if (mode == ResetUserStateMode.PerPaths)
                        {
                            Debug.Assert(paths != null);

                            lowerPaths = paths == null ? null : paths.Select(p => p.ToLower()).ToArray();
                        }
                        else
                        {
                            Debug.Assert(mode == ResetUserStateMode.PerUsers);

                            lowerPaths = firstPath != null ? new string[] { firstPath.ToLower() } : null;
                            lowerUserName = usernames == null ? null : usernames.Select(u => u.ToLower()).ToArray();
                        }

                        var perUserIds = from perUser in db.AspNetPersonalizationPerUser
                                         from usersTbl in db.Users
                                         from pathsTbl in db.AspNetPaths
                                         where pathsTbl.ApplicationId == applicationId
                                         && perUser.UserId == usersTbl.Id
                                         && perUser.PathId == pathsTbl.Id
                                         //  && (inactiveSinceDate == null || usersTbl.LastActivityDate <= inactiveSinceDate)
                                         && (lowerUserName == null || lowerUserName.Contains(usersTbl.UserName.ToLower()))
                                         && (lowerPaths == null || lowerPaths.Contains(pathsTbl.Path.ToLower()))
                                         select perUser.Id;

                        var rowsToDelete = db.AspNetPersonalizationPerUser.Where(p => perUserIds.Contains(p.PathId));
                        count = db.AspNetPersonalizationPerUser.RemoveRange(rowsToDelete).Count();

                        db.SaveChanges();
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                }
            }
            catch
            {
                throw;
            }

            return count;
        }

        private string CreatePath(ApplicationDbContext db, string path, string applicationId)
        {
            string pathId = db.AspNetPaths.Where(x => x.Path.ToLower() == path.ToLower() && x.ApplicationId == applicationId).Select(p => p.Id).SingleOrDefault();
            if (string.IsNullOrEmpty(pathId))
            {
                pathId = db.AspNetPaths.Add(new AspNetPaths()
                {
                    Id = Guid.NewGuid().ToString(),
                    ApplicationId = applicationId,
                    Path = path
                }).Id;
                db.SaveChanges();
            }
            return pathId;
        }


        /// <devdoc> 
        /// </devdoc>
        private void SavePersonalizationState(ApplicationDbContext db, string path, string userName, byte[] state)
        {
            Debug.Assert(db != null);
            Debug.Assert(!string.IsNullOrEmpty(path));
            Debug.Assert((state != null) && (state.Length != 0));

            string applicationId = MultiTenantsHelper.CreateApplication(db);
            string pathId = CreatePath(db, path, applicationId);

            string base64State = Convert.ToBase64String(state);

            if (userName != null)
            {
                //command = new SqlCommand("dbo.aspnet_PersonalizationPerUser_SetPageSettings", connection);

                string userId = db.Users.Where(u => u.Applications.Any(x => x.Id == applicationId) && u.UserName.ToLower() == userName.ToLower()).Select(u => u.Id).SingleOrDefault();
                //TODO: IF(@UserId IS NULL)
                //BEGIN
                //    EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CurrentTimeUtc, @UserId OUTPUT
                //END

                // TODO: update user LastActivityDate
                if (string.IsNullOrEmpty(userId))
                    return;

                var personalization = db.AspNetPersonalizationPerUser.Where(p => p.UserId == userId && p.PathId == pathId).SingleOrDefault();
                if (personalization != null)
                {
                    personalization.PageSettings = base64State;
                    personalization.LastUpdatedDate = DateTime.Now;
                }
                else
                {
                    db.AspNetPersonalizationPerUser.Add(new AspNetPersonalizationPerUser()
                    {
                        Id = Guid.NewGuid().ToString(),
                        UserId = userId,
                        PathId = pathId,
                        PageSettings = base64State,
                        LastUpdatedDate = DateTime.Now
                    });
                }
            }
            else
            {
                //command = new SqlCommand("dbo.aspnet_PersonalizationAllUsers_SetPageSettings", connection);
                var stateString = base64State;
                var personalization = db.AspNetPersonalizationAllUsers.Where(p => p.PathId == pathId).SingleOrDefault();
                if (personalization != null)
                {
                    personalization.PageSettings = stateString;
                    personalization.LastUpdatedDate = DateTime.Now;
                }
                else
                {
                    db.AspNetPersonalizationAllUsers.Add(new AspNetPersonalizationAllUsers()
                    {
                        PathId = pathId,
                        PageSettings = stateString,
                        LastUpdatedDate = DateTime.Now
                    });
                }
            }

            db.SaveChanges();
        }

        /// <internalonly /> 
        protected override void SavePersonalizationBlob(WebPartManager webPartManager, string path, string userName, byte[] dataBlob)
        {
            // Extra try-catch block to prevent elevation of privilege attack via exception filter 
            try
            {
                try
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        SavePersonalizationState(db, path, userName, dataBlob);
                    }
                }
                catch (Exception ex)
                {
                    // Check if it failed due to duplicate user name
                    //if (userName != null && (sqlEx.Number == 2627 || sqlEx.Number == 2601 || sqlEx.Number == 2512))
                    //{
                    //    // Try again, because it failed first time with duplicate user name
                    //    SavePersonalizationState(connection, path, userName, dataBlob);
                    //}
                    //else
                    {
                        throw;
                    }
                }
                finally
                {
                }
            }
            catch
            {
                throw;
            }
        }
        

        public static IDictionary DeserializeBlob(byte[] state)
        {
            Type typeBlobPersonalizationState = typeof(PersonalizationState).Assembly.GetType("System.Web.UI.WebControls.WebParts.BlobPersonalizationState");
            var method = typeBlobPersonalizationState.GetMethod("DeserializeData", BindingFlags.Static | BindingFlags.NonPublic);
            IDictionary r = (IDictionary)method.Invoke(obj: null, parameters: new object[] { state });
            return r;
        }

        public static byte[] SerializeBlob(IDictionary state)
        {
            Type typeBlobPersonalizationState = typeof(PersonalizationState).Assembly.GetType("System.Web.UI.WebControls.WebParts.BlobPersonalizationState");
            var method = typeBlobPersonalizationState.GetMethod("SerializeData", BindingFlags.Static | BindingFlags.NonPublic);
            byte[] r = (byte[])method.Invoke(obj: null, parameters: new object[] { state });
            return r;
        }

        public static string FromBlobByteArray2Json(byte[] dataBlob)
        {
            ObjectStateFormatter formatter = new ObjectStateFormatter();
            object[]  items = (object[])formatter.Deserialize(new MemoryStream(dataBlob));

            var jsonSerializerSettings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All
            };
            string json = JsonConvert.SerializeObject(items, jsonSerializerSettings);
            return json;
        }

        public static byte[] FromJson2BlobByteArray(string dataJson)
        {
            var jsonSerializerSettings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All,
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                
            };
            object[] state = JsonConvert.DeserializeObject<object[]>(dataJson, jsonSerializerSettings);

            ObjectStateFormatter formatter = new ObjectStateFormatter();
            MemoryStream ms = new MemoryStream(1024);
            formatter.Serialize(ms, state);

            byte[] serializedData = ms.ToArray();

            return serializedData;
        }

    }
}
