﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTPModule
{
    public abstract class WatcherJob : FileSystemWatcher
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //private static readonly log4net.ILog emailLog = log4net.LogManager.GetLogger("EmailLogger");

        //private const string HeaderRow = "deviceId;deviceName;timestamp;quality;value;alarm;time;";

        public string IdApplication { get; set; }

        protected string BasePath { get; set; }

        protected DateTime LastFileReceived { get; set; }
        protected DateTime LastFileProcess { get; set; }

        protected List<string> FileUploadToDBList { get; set; } //lista dei file che devo elaborare per salvarli nel DB

        public WatcherJob(string applicationId, string path)
        {
            BasePath = path;
            IdApplication = applicationId;
            Path = System.IO.Path.Combine(BasePath, "Input");

            FileUploadToDBList = new List<string>();

            LastFileProcess = DateTime.Now;

            // Watch for changes in LastAccess and LastWrite times.
            NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.Size | NotifyFilters.Attributes;
            // Only watch csv files.
            //Filter = "*.csv";

            // Add event handlers.
            Changed += new FileSystemEventHandler(OnChanged);
            Error += new ErrorEventHandler(OnError);

            // Begin watching.
            EnableRaisingEvents = true;

            //HttpContext.Current = currentContext;
        }

        public abstract void OnChanged(object source, FileSystemEventArgs e);
        public abstract void OnError(object source, ErrorEventArgs e);
    }
}