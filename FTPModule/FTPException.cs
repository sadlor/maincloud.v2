﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTPModule
{
    public class FTPException : Exception
    {
        public FTPException()
        {
        }

        public FTPException(string message) : base(message)
        {
        }

        public FTPException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}