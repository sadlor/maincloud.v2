﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTPModule
{
    public abstract class AbsWatcherParser<T, TMap> where TMap : CSVStructure<T>
    {
        public List<T> Read(string fileName)
        {
            try
            {
                using (StreamReader sr = new StreamReader(fileName))
                {
                    var csv = new CsvReader(sr);
                    csv.Configuration.Delimiter = ";";
                    csv.Configuration.CultureInfo = CultureInfo.InvariantCulture;
                    csv.Configuration.RegisterClassMap<TMap>();

                    //try
                    //{
                    return csv.GetRecords<T>().ToList();
                    //}
                    //catch (Exception ex) //se è presente una riga scritta in un formato errato
                    //{
                    //    List<CSVStructure> recordList = new List<CSVStructure>();
                    //    while (csv.Read())
                    //    {
                    //        try
                    //        {
                    //            recordList.Add(csv.GetRecord<CSVStructure>());
                    //        }
                    //        catch (Exception e)
                    //        {
                    //        }
                    //    }
                    //    return recordList;
                    //}
                }
            }
            catch (FileNotFoundException ex)
            {
                throw new FTPException(string.Format("Non esiste il file {0}. {1}", fileName, ex.Message));
            }
        }
    }
}