﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FTPModule.ReadCSV
{
    public class CSVStructure : List<List<CSVData>>
    {
        public CSVStructure(string fileName)
        {
            GetStructure(fileName);
        }

        public void GetStructure(string fileName)
        {
            try
            {
                // Read sample data from CSV file
                using (CsvFileReader reader = new CsvFileReader(fileName))
                {
                    CsvRow row = new CsvRow();

                    reader.ReadRow(row); //prima riga con nomi colonne
                    
                    List<string> columnName = new List<string>();
                    columnName.AddRange(row);

                    while (reader.ReadRow(row))
                    {
                        int i = 0;
                        List<CSVData> dataRow = new List<CSVData>();
                        foreach (string s in row)
                        {
                            //leggo i valori di ogni cella per tutta la riga
                            CSVData column = new CSVData(columnName[i], s);
                            dataRow.Add(column);
                            i++;
                        }
                        this.Add(dataRow);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                //ex.Message;
            }
        }
    }
}
