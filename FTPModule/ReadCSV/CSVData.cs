﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTPModule.ReadCSV
{
    public struct CSVData
    {
        public string Key;
        public string Value;

        public CSVData(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }
    }
}