﻿using MainCloudFramework.Models;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FTPModule
{
    public class SchedulerFtp<T> where T : WatcherJob
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        IScheduler Scheduler;

        public SchedulerFtp(HttpContext currentContext, int frequency = 1)
        {
            try
            {
                // Grab the Scheduler instance from the Factory 
                Scheduler = StdSchedulerFactory.GetDefaultScheduler();
                JobDataMap map = new JobDataMap();
                map.Add("HttpContext", currentContext);

                // define the job and tie it to our HelloJob class
                IJobDetail job = JobBuilder.Create<WatchFtpJob<T>>()
                    .WithIdentity("WatchDirectory", "MonitorFTP")
                    .UsingJobData(map)
                    .Build();

                // Trigger the job to run now, and then repeat every 10 seconds
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("WatchDirectoryTrigger", "MonitorFTP")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(frequency)
                        .RepeatForever())
                    .Build();

                // Tell quartz to schedule the job using our trigger
                Scheduler.ScheduleJob(job, trigger);
            }
            catch (SchedulerException se)
            {
                //Console.WriteLine(se);
            }
        }

        public void Start()
        {
            try
            {
                Scheduler.Start();
                log.Info("Monitor FTP start");
            }
            catch (SchedulerException se)
            {
                log.Error("Monitor FTP start error", se);
            }
        }

        public void Stop()
        {
            try
            {
                Scheduler.Shutdown();
                log.Info("Monitor FTP stop");
            }
            catch (SchedulerException se)
            {
                log.Error("Monitor FTP stop error", se);
            }
        }

        private class WatchFtpJob<T> : IJob where T : WatcherJob
        {
            public void Execute(IJobExecutionContext context)
            {
                try
                {
                    HttpContext currentContext = (HttpContext)context.JobDetail.JobDataMap["HttpContext"];

                    if (currentContext.Application["WatcherList"] == null)
                    {
                        currentContext.Application["WatcherList"] = new List<T>();
                    }
                    List<T> watcherList = (List<T>)currentContext.Application["WatcherList"];

                    List<AspNetApplications> applications;
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        applications = db.AspNetApplications.ToList(); //le tabella delle applications potrebbe subire modifiche in corso d'opera, tenere controllata
                    }
                    foreach (AspNetApplications application in applications) //schedulo tutte le application
                    {
                        if (!watcherList.Exists(x => x.IdApplication == application.Id))
                        {
                            //le directory sono nominate come l'application
                            try
                            {
                                //string path = currentContext.Server.MapPath("~/Ftp/" + application.Name);
                                string path = @"C:\FTP\" + application.Name;
                                watcherList.Add((T)Activator.CreateInstance(typeof(T).GetType(), application.Id, path));  //istanzio un watcher per ogni application
                            }
                            catch (Exception ex)
                            {
                                log.Error("Watcher not created", ex);
                            }
                        }
                    }
                    foreach (T watcher in watcherList) //verifico che non esistano watcher associati ad una application non esistente
                    {
                        if (!applications.Exists(x => x.Id == watcher.IdApplication))
                        {
                            try
                            {
                                watcher.EnableRaisingEvents = false;
                                watcher.Dispose();
                            }
                            catch (Exception ex)
                            {
                                log.Error("Dispose watcher error", ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("This is my error", ex);
                }
            }
        }
    }
}